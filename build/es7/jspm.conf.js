System.config({
  baseURL: "./",
  defaultJSExtensions: true,
  transpiler: "update",
  babelOptions: {
    "optional": [
      "runtime"
    ],
    "stage": 1
  },
  paths: {
    "github:*": "jspm_packages/github/*",
    "npm:*": "jspm_packages/npm/*"
  },

  map: {
    "ENV": "src/app/core/config/env.conditions.js",
    "angular": "github:angular/bower-angular@1.4.9",
    "angular-animate": "github:angular/bower-angular-animate@1.4.9",
    "angular-bootstrap": "github:angular-ui/bootstrap-bower@1.1.1",
    "angular-dnd-lists": "github:marceljuenemann/angular-drag-and-drop-lists@1.3.0",
    "angular-file-upload": "github:danialfarid/ng-file-upload-bower@11.2.3",
    "angular-formly": "github:formly-js/angular-formly@7.3.9",
    "angular-formly-templates-bootstrap": "github:formly-js/angular-formly-templates-bootstrap@6.2.0",
    "angular-input-masks": "github:assisrafael/bower-angular-input-masks@2.1.1",
    "angular-local-storage": "npm:angular-local-storage@0.2.2",
    "angular-mocks": "github:angular/bower-angular-mocks@1.4.9",
    "angular-sanitize": "github:angular/bower-angular-sanitize@1.4.9",
    "angular-ui-grid": "github:angular-ui/bower-ui-grid@3.0.7",
    "angular-ui-router": "github:angular-ui/ui-router@0.2.17",
    "angular-ui-select": "github:angular-ui/ui-select@0.14.2",
    "angular-validator": "github:turinggroup/angular-validator@1.2.8",
    "angular-xeditable": "github:vitalets/angular-xeditable@0.1.9",
    "api-check": "npm:api-check@7.5.5",
    "babel": "npm:babel-core@5.8.35",
    "babel-runtime": "npm:babel-runtime@5.8.35",
    "bootstrap": "github:twbs/bootstrap@3.3.5",
    "bootstrap-css-only": "github:fyockm/bootstrap-css-only@3.3.5",
    "bootstrap-sass": "github:twbs/bootstrap-sass@3.3.5",
    "cgBusy": "github:cgross/angular-busy@4.1.3",
    "clean-css": "npm:clean-css@3.4.6",
    "core-js": "npm:core-js@1.2.5",
    "css": "github:systemjs/plugin-css@0.1.20",
    "dropzone": "github:enyo/dropzone@4.2.0",
    "eriktufvesson/ngBootbox": "github:eriktufvesson/ngBootbox@0.1.2",
    "flatlogic/awesome-bootstrap-checkbox": "github:flatlogic/awesome-bootstrap-checkbox@0.3.5",
    "font-awesome": "npm:font-awesome@4.4.0",
    "fraywing/textAngular": "github:fraywing/textAngular@1.4.6",
    "greensock": "github:greensock/GreenSock-JS@1.18.0",
    "imgCrop": "github:alexk111/ngImgCrop@0.3.2",
    "jquery": "npm:jquery@2.1.4",
    "json": "github:systemjs/plugin-json@0.1.0",
    "lodash": "npm:lodash@3.10.1",
    "moment": "github:moment/moment@2.10.6",
    "moment-range": "npm:moment-range@2.0.3",
    "moment-timezone": "npm:moment-timezone@0.4.1",
    "multiSelect": "github:isteven/angular-multi-select@4.0.0",
    "ng-bs-animated-button": "github:jeremypeters/ng-bs-animated-button@2.0.3",
    "ng-file-upload": "npm:ng-file-upload@7.2.0",
    "ng-sortable": "github:a5hik/ng-sortable@1.3.2",
    "normalize.css": "github:necolas/normalize.css@3.0.3",
    "open-sans-fontface": "github:FontFaceKit/open-sans@1.4.2",
    "pdfmake": "github:bpampuch/pdfmake@0.1.20",
    "rangy": "github:timdown/rangy-release@1.3.0",
    "rx": "npm:rx@3.1.2",
    "sparkalow/angular-truncate": "github:sparkalow/angular-truncate@master",
    "systemjs/plugin-css": "github:systemjs/plugin-css@0.1.20",
    "text": "github:systemjs/plugin-text@0.0.2",
    "update": "npm:babel-core@5.8.35",
    "update-runtime": "npm:babel-runtime@5.8.35",
    "github:FontFaceKit/open-sans@1.4.2": {
      "css": "github:systemjs/plugin-css@0.1.20"
    },
    "github:a5hik/ng-sortable@1.3.2": {
      "angular": "github:angular/bower-angular@1.4.9",
      "css": "github:systemjs/plugin-css@0.1.20"
    },
    "github:angular-ui/ui-router@0.2.17": {
      "angular": "github:angular/bower-angular@1.4.9"
    },
    "github:angular/bower-angular-animate@1.4.9": {
      "angular": "github:angular/bower-angular@1.4.9"
    },
    "github:angular/bower-angular-mocks@1.4.9": {
      "angular": "github:angular/bower-angular@1.4.9"
    },
    "github:angular/bower-angular-sanitize@1.4.9": {
      "angular": "github:angular/bower-angular@1.4.9"
    },
    "github:assisrafael/bower-angular-input-masks@2.1.1": {
      "angular": "github:angular/bower-angular@1.4.9"
    },
    "github:cgross/angular-busy@4.1.3": {
      "angular": "github:angular/bower-angular@1.4.9"
    },
    "github:fraywing/textAngular@1.4.6": {
      "angular": "github:angular/bower-angular@1.4.9",
      "css": "github:systemjs/plugin-css@0.1.20",
      "font-awesome": "npm:font-awesome@4.4.0",
      "rangy": "github:timdown/rangy-release@1.3.0"
    },
    "github:jspm/nodelibs-assert@0.1.0": {
      "assert": "npm:assert@1.3.0"
    },
    "github:jspm/nodelibs-buffer@0.1.0": {
      "buffer": "npm:buffer@3.5.1"
    },
    "github:jspm/nodelibs-events@0.1.1": {
      "events": "npm:events@1.0.2"
    },
    "github:jspm/nodelibs-http@1.7.1": {
      "Base64": "npm:Base64@0.2.1",
      "events": "github:jspm/nodelibs-events@0.1.1",
      "inherits": "npm:inherits@2.0.1",
      "stream": "github:jspm/nodelibs-stream@0.1.0",
      "url": "github:jspm/nodelibs-url@0.1.0",
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "github:jspm/nodelibs-https@0.1.0": {
      "https-browserify": "npm:https-browserify@0.0.0"
    },
    "github:jspm/nodelibs-os@0.1.0": {
      "os-browserify": "npm:os-browserify@0.1.2"
    },
    "github:jspm/nodelibs-path@0.1.0": {
      "path-browserify": "npm:path-browserify@0.0.0"
    },
    "github:jspm/nodelibs-process@0.1.2": {
      "process": "npm:process@0.11.2"
    },
    "github:jspm/nodelibs-stream@0.1.0": {
      "stream-browserify": "npm:stream-browserify@1.0.0"
    },
    "github:jspm/nodelibs-url@0.1.0": {
      "url": "npm:url@0.10.3"
    },
    "github:jspm/nodelibs-util@0.1.0": {
      "util": "npm:util@0.10.3"
    },
    "github:jspm/nodelibs-zlib@0.1.0": {
      "browserify-zlib": "npm:browserify-zlib@0.1.4"
    },
    "github:necolas/normalize.css@3.0.3": {
      "css": "github:systemjs/plugin-css@0.1.20"
    },
    "github:turinggroup/angular-validator@1.2.8": {
      "angular": "github:angular/bower-angular@1.4.9"
    },
    "github:twbs/bootstrap@3.3.5": {
      "jquery": "github:components/jquery@2.1.4"
    },
    "github:vitalets/angular-xeditable@0.1.9": {
      "angular": "github:angular/bower-angular@1.4.9",
      "css": "github:systemjs/plugin-css@0.1.20"
    },
    "npm:amdefine@1.0.0": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "module": "github:jspm/nodelibs-module@0.1.0",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:assert@1.3.0": {
      "util": "npm:util@0.10.3"
    },
    "npm:babel-runtime@5.8.35": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:browserify-zlib@0.1.4": {
      "assert": "github:jspm/nodelibs-assert@0.1.0",
      "buffer": "github:jspm/nodelibs-buffer@0.1.0",
      "pako": "npm:pako@0.2.8",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "readable-stream": "npm:readable-stream@1.1.13",
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:buffer@3.5.1": {
      "base64-js": "npm:base64-js@0.0.8",
      "ieee754": "npm:ieee754@1.1.6",
      "is-array": "npm:is-array@1.0.1"
    },
    "npm:clean-css@3.4.6": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0",
      "commander": "npm:commander@2.8.1",
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "http": "github:jspm/nodelibs-http@1.7.1",
      "https": "github:jspm/nodelibs-https@0.1.0",
      "os": "github:jspm/nodelibs-os@0.1.0",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "source-map": "npm:source-map@0.4.4",
      "url": "github:jspm/nodelibs-url@0.1.0",
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:commander@2.8.1": {
      "child_process": "github:jspm/nodelibs-child_process@0.1.0",
      "events": "github:jspm/nodelibs-events@0.1.1",
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "graceful-readlink": "npm:graceful-readlink@1.0.1",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:core-js@1.2.5": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "systemjs-json": "github:systemjs/plugin-json@0.1.0"
    },
    "npm:core-util-is@1.0.1": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0"
    },
    "npm:font-awesome@4.4.0": {
      "css": "github:systemjs/plugin-css@0.1.20"
    },
    "npm:graceful-readlink@1.0.1": {
      "fs": "github:jspm/nodelibs-fs@0.1.2"
    },
    "npm:https-browserify@0.0.0": {
      "http": "github:jspm/nodelibs-http@1.7.1"
    },
    "npm:inherits@2.0.1": {
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:jquery@2.1.4": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:lodash@3.10.1": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:moment-range@2.0.3": {
      "moment": "npm:moment@2.6.0"
    },
    "npm:moment-timezone@0.4.1": {
      "moment": "npm:moment@2.6.0",
      "systemjs-json": "github:systemjs/plugin-json@0.1.0"
    },
    "npm:moment@2.6.0": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "https": "github:jspm/nodelibs-https@0.1.0",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "zlib": "github:jspm/nodelibs-zlib@0.1.0"
    },
    "npm:ng-file-upload@7.2.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:os-browserify@0.1.2": {
      "os": "github:jspm/nodelibs-os@0.1.0"
    },
    "npm:pako@0.2.8": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:path-browserify@0.0.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:process@0.11.2": {
      "assert": "github:jspm/nodelibs-assert@0.1.0"
    },
    "npm:punycode@1.3.2": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:readable-stream@1.1.13": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0",
      "core-util-is": "npm:core-util-is@1.0.1",
      "events": "github:jspm/nodelibs-events@0.1.1",
      "inherits": "npm:inherits@2.0.1",
      "isarray": "npm:isarray@0.0.1",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "stream-browserify": "npm:stream-browserify@1.0.0",
      "string_decoder": "npm:string_decoder@0.10.31"
    },
    "npm:rx@3.1.2": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:source-map@0.4.4": {
      "amdefine": "npm:amdefine@1.0.0",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:stream-browserify@1.0.0": {
      "events": "github:jspm/nodelibs-events@0.1.1",
      "inherits": "npm:inherits@2.0.1",
      "readable-stream": "npm:readable-stream@1.1.13"
    },
    "npm:string_decoder@0.10.31": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.0"
    },
    "npm:url@0.10.3": {
      "assert": "github:jspm/nodelibs-assert@0.1.0",
      "punycode": "npm:punycode@1.3.2",
      "querystring": "npm:querystring@0.2.0",
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:util@0.10.3": {
      "inherits": "npm:inherits@2.0.1",
      "process": "github:jspm/nodelibs-process@0.1.2"
    }
  }
});
