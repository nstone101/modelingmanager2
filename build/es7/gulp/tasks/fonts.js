'use strict';

import es from 'event-stream';
import gulp from 'gulp';
import size from 'gulp-size';
import flatten from 'gulp-flatten';
import paths from '../paths';
import rename from 'gulp-rename';

/**
 * The 'fonts' task copies fonts to `build/dist` directory.
 *
 * @return {Stream}
 */
gulp.task('fonts', () => {
    return es.concat(
        gulp.src(paths.app.fonts)
            .pipe(flatten())
            .pipe(gulp.dest(paths.build.dist.fonts))
            .pipe(gulp.dest(paths.build.baseFonts)),
        gulp.src(paths.app.bsFonts)
            .pipe(flatten())
            .pipe(gulp.dest(paths.build.dist.bsFonts))
            .pipe(gulp.dest(paths.build.baseFonts+'/bootstrap/')),
        gulp.src(paths.app.openSansFonts)
            .pipe(rename((path) => {
                path.dirname = path.dirname.replace(/open-sans@1.4.2\/fonts\//, '');
            }))
            .pipe(gulp.dest(paths.build.dist.fonts))
            .pipe(gulp.dest(paths.build.baseFonts))
    )
    .pipe(size({title: 'fonts'}));
});
