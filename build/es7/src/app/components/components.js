'use strict';

import './aboutUs/aboutUs';
import './header/header';
import './sideMenu/sideMenu';
import './footer/footer';
import './modal-error/modal-error';
import './alert-danger/alert-danger';
import './nodeBreadcrumb/nodeBreadcrumb';
import './nodeDocContainer/nodeDocContainer';
import './file-container/file-container';