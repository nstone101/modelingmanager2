'use strict';

import template from './aboutUs.html!text';
import {RouteConfig, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('aboutUs', {
    url: '/common/aboutUs',
    onEnter: ['$uibModal', 'ModalService', ($modal, ModalService) => {
        $modal.open({
            template: template,
            controller: AboutUs,
            controllerAs: 'vm',
            size: 'about'
        })
.result.finally(ModalService.onFinal('portal.nodes.node', {nodeName: $stateParams.nodeName}));
    }]
})
@Inject('$state', '$rootScope', '$uibModalInstance')
//end-non-standard
class AboutUs {
    constructor($state, $rootScope, $modalInstance) {
        this.modal = $modalInstance;
        this.router = $state;
        this.user = $rootScope.currentUser;
    }

    cancel() {
        this.modal.dismiss('cancel');
    }
}

export default AboutUs;
