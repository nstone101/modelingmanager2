'use strict';

import {View, Component, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@Component({
    selector: 'node-breadcrumb'
})
@View({
    template: `<div class="crumb">
                    <a href="/portal/nodes/browser/">Node Browser</a>
                    <a href="/portal/nodes/browser/{{vm.node.node.SiteGroup}}">{{vm.node.node.SiteGroup}}</a>
                    <a href="/portal/nodes/node/{{vm.node.node.SiteName}}">{{vm.node.node.SiteName}}</a>
                    <a href="" class="active">{{vm.Area}}</a>
                </div>`,
    replace: true
})
@Inject('$rootScope')
//end-non-standard
class Breadcrumb {
    constructor($rootScope) {
        // get user session information
        this.node = $rootScope.node;
        this.node.node = $rootScope.node.node;
        this.Area = 'Node Info';
        this.$rootScope = $rootScope;
        this.init();
    }

    init() {
        this.$rootScope.$bus.subscribe('NODE_AREA_UPDATE', (data) => {
            this.Area = data.area;
        });
    }
}
