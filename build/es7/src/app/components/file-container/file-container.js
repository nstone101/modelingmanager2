'use strict';

import template from './file-container.html!text';
import {Directive, Component, Inject} from '../../decorators'; // jshint unused: false

const WINDOW = new WeakMap();

//start-non-standard
@Directive({
    selector: 'file-container'
})
//end-non-standard
class NodeDocContainer {
    //static id = 0;
    constructor($rootScope) {
        this.restrict = 'E';
        this.replace = true;
        this.template = template;
        //this.id = id;
        this.scope = {
            isAdmin: '=',
            config: '=',
            files: '=',
            path: '=',
            host: '='
        };

    }

    link(scope, element, attrs) {

/*        console.log('-=-=-=-=-=-=-=-=-=-=-==-');
        console.log(NodeDocContainer.id++);
        console.log('-=-=-=-=-=-=-=-=-=-=-==-');
        console.log(scope.files);
        console.log(scope.config);
        console.log(scope.path);
        console.log(scope.isAdmin);*/

        scope.files = scope.files[0];
        scope.counts = {total: 0};
        _.map(scope.files, (item, key) => {
            scope.counts.total += item.length;
            scope.counts[key] = item.length;
        });
        scope.$watch('files', (v1, v2) => {
            scope.files = (v1 === undefined)? v2 : v1;
            if(angular.isArray(scope.files)){
                scope.files = scope.files[0];
            }
            scope.counts = {total: 0};
            _.map(scope.files, (item, key) => {
                scope.counts.total += item.length;
                scope.counts[key] = item.length;
            });
        });

        scope.collapse = {};
        scope.collapse[scope.config.type] = {
            isExpanded: false,
            gens: {}
        };
        _.map(scope.config.gens, (item) => {
            scope.collapse[scope.config.type].gens[item] = {
                isExpanded: false
            };

        });

        scope.deleteFile = (filename) => {
            filename = filename.replace(/\//g, '^');
            console.log(scope);
            scope.$bus.publish('FILE_CONTAINER_DELETE', {'filename': filename});
        };

        scope.deleteTitle = (filename) => {
            return 'Delete ' + filename;
        };

        scope.deleteMessage = (filename) => {
            return 'Are you sure you want to delete ' + filename;
        };

    }

    //start-non-standard
    @Inject('$rootScope', '$window')
    //end-non-standard
    static directiveFactory($rootScope, $window){
        NodeDocContainer.instance = new NodeDocContainer($rootScope, $window);
        return NodeDocContainer.instance;
    }
}
