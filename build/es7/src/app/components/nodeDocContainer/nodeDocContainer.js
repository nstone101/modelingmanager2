'use strict';

import template from './nodeDocContainer.html!text';
import {Directive, Component, Inject} from '../../decorators'; // jshint unused: false

const WINDOW = new WeakMap();

//start-non-standard
@Directive({
    selector: 'node-doc-container'
})
//end-non-standard
class NodeDocContainer {
    constructor($rootScope) {
        this.restrict = 'E';
        this.replace = true;
        this.template = template;

        this.scope = {
            siteName: '=',
            isAdmin: '=',
            docConfig: '=',
            docData: '=',
            docPath: '=',
            docHost: '='
        };
    }

    link(scope, element, attrs) {
        scope.collapse = {};
        scope.collapse[scope.docConfig.docType] = false;
    }

    //start-non-standard
    @Inject('$rootScope', '$window')
    //end-non-standard
    static directiveFactory($rootScope, $window){
        NodeDocContainer.instance = new NodeDocContainer($rootScope, $window);
        return NodeDocContainer.instance;
    }
}
