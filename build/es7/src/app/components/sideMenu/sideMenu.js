'use strict';

import template from './sideMenu.html!text';
import {View, Component, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@Component({
    selector: 'side-menu'
})
@View({
    template: template,
    replace: true
})
@Inject('$rootScope')
//end-non-standard
class SideMenu {
    constructor($rootScope, $location) {
        // get user session information
        this.user = $rootScope.currentUser;


        /*this.location = $location;
        console.log(this.location);*/

        this.isCollapsed = false;
        $rootScope.$bus.subscribe('SIDEBAR_TOGGLE_COLLAPSE', (data)=>{
            this.isCollapsed = data.isCollapsed;
        });

        /*$rootScope.$on('$locationChangeSuccess', function (event, next, current) {
            var sPath = window.location.pathname;
            var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);

            this.activeNav = sPage;

        });*/
//router.current.url

    }
    getClass(path) {

        //console.log(this.location);

        //console.log(path, this.location.path().indexOf(path));

        //if ($location.path().substr(0, path.length) === path) {
        /*if (this.location.path().indexOf(path)) {
            return 'active';
        } else {
            return '';
        }*/
    }

    /*setActive($index){
        this.activeNav = $index;
        console.log('setActive: ' + this.activeNav);


       /!* var sPath = window.location.pathname;
        var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);

        console.log(sPath, sPage);*!/
    }*/

    toggleSubMenu($event){
        $event.preventDefault();
        var submenu = $($event.currentTarget).siblings('ul');
            var li = $($event.currentTarget).parents('li');
            var submenus = $('#sidebar li.submenu ul');
            var submenusParents = $('#sidebar li.submenu');
            if(li.hasClass('open'))
            {
                if(($(window).width() > 768) || ($(window).width() < 479)) {
                    submenu.slideUp();
                } else {
                    submenu.fadeOut(250);
                }
                li.removeClass('open');
            } else 


            {
                if(($(window).width() > 768) || ($(window).width() < 479)) {
                    submenus.slideUp();
                    submenu.slideDown();
                } else {
                    submenus.fadeOut(250);
                    submenu.fadeIn(250);
                }
                submenusParents.removeClass('open');
                li.addClass('open');
            }
    }


    /*toggleSideMenu() {
        // This checks if the bar is extended, and if so, collapses it.
        if (localStorage.menuCollapsed === '0') {
            // removes the active class to the link in the menu for the open url
            //menuLink.parent().removeClass('active');

            // activate the tooltips on the menu icons
            // TODO: Someone bropke this :(
            //$('.menu-icon').tooltip();


        }
        // if the bar is already collapsed, it will be expanded:
        else {
            //sidebar.addClass('open');

            // adds the active class to the link in the menu for the open url
            //menuLink.parent().addClass('active');

            // remove the tooltips on the menu icons
            // TODO: Someone bropke this :(
            //$('.menu-icon').tooltip('destroy');


        }
    }*/

}
/*
 init() {
 $('#sidebar > a').click(function(e)
 {
 e.preventDefault();
 var sidebar = $('#sidebar');
 if(sidebar.hasClass('open'))
 {
 sidebar.removeClass('open');
 ul.slideUp(250);
 } else
 {
 sidebar.addClass('open');
 ul.slideDown(250);
 }
 });
 }*/

/*
    var sPath = window.location.pathname;
    var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);
    var menuLink = $('#sidebar a[href="'+ sPath +'"]');
*/

