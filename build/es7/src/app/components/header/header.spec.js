
'use strict';

import './header.js';

describe('Header', () => {
    let component = '<header></header>';

    beforeEach(angular.mock.module('ngDecorator'));

    describe('Component', () => {
        let $compile, $rootScope, scope, render, element;

        beforeEach(inject((_$compile_, _$rootScope_) => {
            $compile = _$compile_;
            $rootScope = _$rootScope_;
            scope = $rootScope.$new();

            render = () => {
                let element = angular.element(component);
                let compiledElement = $compile(element)(scope);
                $rootScope.$digest();

                return compiledElement;
            };
        }));

        it('should contain header component', () => {
            element = render();

            expect(element.controller('header')).toBeDefined();
            expect(element['0']).not.toEqual(component);
        });
    });
});
