'use strict';

import template from './header.html!text';
import {View, Component, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@Component({
    selector: 'header'
})
@View({
    template: template,
    replace: true
})
@Inject('$rootScope')
//end-non-standard
class Header {
    constructor($rootScope) {
        this.rootScope = $rootScope;
        // get user session information
        this.user = $rootScope.currentUser;
        this.isCollapsed = false;
        this.headerCollapsed = false;
        this.init();
    }

    init() {
        var ul = $('#sidebar > ul');
        // === Resize window related === //
        $(window).resize(function()
        {
            if($(window).width() > 479)
            {
                ul.css({'display':'block'});    
                //$('#content-header .btn-group').css({width:'auto'});      
            }
            if($(window).width() < 479)
            {
                ul.css({'display':'none'});
                /* jshint ignore:start */
                fix_position();
                /* jshint ignore:end */
            }
            if($(window).width() > 768)
            {
                $('#user-nav > ul').css({width:'auto',margin:'0'});
                //$('#content-header .btn-group').css({width:'auto'});
            }
        });

        // === Tooltips === //
/*        $('.tip').tooltip();
        $('.tip-left').tooltip({ placement: 'left' });  
        $('.tip-right').tooltip({ placement: 'right' });    
        $('.tip-top').tooltip({ placement: 'top' });    
        $('.tip-bottom').tooltip({ placement: 'bottom' });  */
    }

	toggleSideMenu() {
        // This checks if the bar is extended, and if so, collapses it.
        if (localStorage.menuCollapsed === '0') {
            this.isCollapsed = true;
            if(typeof(Storage) !== undefined) {
                localStorage.menuCollapsed = 1;
            }
        }
        // if the bar is already collapsed, it will be expanded:
        else {
            this.isCollapsed = false;
            if(typeof(Storage)!== 'undefined') {
                localStorage.menuCollapsed = '0';
            }
        }
        this.rootScope.$bus.publish('SIDEBAR_TOGGLE_COLLAPSE', { 'isCollapsed': this.isCollapsed});
	}

    toggleHeader() {
        this.headerCollapsed = !this.headerCollapsed;
        this.rootScope.$bus.publish('HEADER_TOGGLE_COLLAPSE', { 'headerCollapsed': this.headerCollapsed});

        if(this.headerCollapsed){
            this.isCollapsed = true;
        }else{
            this.isCollapsed = false;
        }
        this.rootScope.$bus.publish('SIDEBAR_TOGGLE_COLLAPSE', { 'isCollapsed': this.isCollapsed});
    }
}
