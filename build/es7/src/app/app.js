'use strict';

// Sometimes transpiled code isn't always supported
import 'babel/polyfill';

// core libraries
import _ from 'lodash'; window._ = _;
import $ from 'jquery'; window.$ = $; window.jquery = $;
import TweenMax from 'greensock/TweenMax'; window.TweenMax = TweenMax;
import angular from 'angular'; window.angular = angular;
import 'angular-animate';
import 'angular-sanitize';
import 'angular-ui-router';

// vendor
import 'angular-xeditable';
import 'angular-ui-select/dist/select';
import 'angular-local-storage';
import 'angular-file-upload';
import 'ng-sortable';
import 'angular-formly';
import 'angular-input-masks';
import 'angular-ui-select/dist/select';

import 'angular-bootstrap/ui-bootstrap-tpls';
import 'ng-bs-animated-button';
import 'angular-validator';
import 'angular-dnd-lists';

import 'multiSelect/isteven-multi-select';

import 'angular-ui-grid';
import 'angular-formly-templates-bootstrap';

// css
/*import 'normalize.css';*/
import 'multiSelect/isteven-multi-select.css!css';
import 'dropzone/dropzone.css!css';
import 'ng-sortable/ng-sortable.css!css';
import 'angular-ui-select/dist/select.css!css';

// js app files
import './core/core';
import './components/components';
import './routes/routes';

import mainModule from './decorators';

angular.element(document).ready(function() {
    angular.bootstrap(document, [mainModule.name], {
    });
});
