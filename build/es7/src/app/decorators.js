'use strict';
import 'cgBusy';
const portal = angular.module('ngDecorator', [
    // angular modules
    'ngAnimate',
    'angularValidator',

    // 3rd party modules
    'ngSanitize',
    'LocalStorageModule',
    'ui.bootstrap',
    'ui.select',
    'cgBusy',
    'ui.grid',
    'ui.grid.exporter',
    'ui.grid.pagination',
    'ui.grid.cellNav',
    'ui.grid.edit',
    'ui.grid.resizeColumns',
    'ui.grid.pinning',
    'ui.grid.selection',
    'ui.grid.moveColumns',
    'ui.grid.exporter',
    'ui.grid.grouping',
    'ui.grid.autoResize',
    'isteven-multi-select',
    'ui.utils.masks',
    'ui.router',

    'jp.ng-bs-animated-button',
    'ngFileUpload',
    'as.sortable',
    'formly',
    'formlyBootstrap',
    'xeditable'
]);

function Constant(key, value) {
    return function decorator(target){ // jshint unused:false
        portal.constant(key, value);
    };
}

function Run() {
    return function decorator(target, key, descriptor) {
        portal.run(descriptor.value);
    };
}

function Config() {
    return function decorator(target, key, descriptor) {
        portal.config(descriptor.value);
    };
}

function Service(options) {
    return function decorator(target) {
        options = options ? options : {};
        if (!options.serviceName) {
            throw new Error('@Service() must contains serviceName property!');
        }
        portal.service(options.serviceName, target);
    };
}

function Filter(filter) {
    return function decorator(target, key, descriptor) {
        filter = filter ? filter : {};
        if (!filter.filterName) {
            throw new Error('@Filter() must contains filterName property!');
        }
        portal.filter(filter.filterName, descriptor.value);
    };
}

function Inject(...dependencies) {
    return function decorator(target, key, descriptor) {
        // if it's true then we injecting dependencies into function and not Class constructor
        if(descriptor) {
            const fn = descriptor.value;
            fn.$inject = dependencies;
        } else {
            target.$inject = dependencies;
        }
    };
}

function Component(component) {
    return function decorator(target) {
        component = component ? component : {};
        if (!component.selector) {
            throw new Error('@Component() must contains selector property!');
        }

        if (target.$initView) {
            target.$initView(component.selector);
        }

        target.$isComponent = true;
    };
}

function View(view) {
    let options = view ? view : {};
    const defaults = {
        template: options.template,
        restrict: 'E',
        scope: {},
        bindToController: true,
        controllerAs: 'vm'
    };
    return function decorator(target) {
        if (target.$isComponent) {
            throw new Error('@View() must be placed after @Component()!');
        }

        target.$initView = function(directiveName) {
            directiveName = pascalCaseToCamelCase(directiveName);
            directiveName = dashCaseToCamelCase(directiveName);

            options.bindToController = options.bindToController || options.bind || {};

            portal.directive(directiveName, function () {
                return Object.assign(defaults, { controller: target }, options);
            });
        };

        target.$isView = true;
    };
}

function Directive(options) {
    return function decorator(target) {
        const directiveName = dashCaseToCamelCase(options.selector);
        portal.directive(directiveName, target.directiveFactory);
    };
}

function RouteConfig(stateName, options) {
    return function decorator(target) {
        portal.config(['$stateProvider', ($stateProvider) => {
            $stateProvider.state(stateName, Object.assign({
                controller: target,
                controllerAs: 'vm'
            }, options));
        }]);
        portal.controller(target.name, target);
    };
}

// Utils functions
function pascalCaseToCamelCase(str) {
    return str.charAt(0).toLowerCase() + str.substring(1);
}

function dashCaseToCamelCase(string) {
    return string.replace( /-([a-z])/ig, function( all, letter ) {
        return letter.toUpperCase();
    });
}

/**
 * this is not a smart thing to do. we are trying to get away from globals
 * but this seems to fit for now.
 */
// use during gulp build
/*window.PORTAL_URLS = {
    BASE: window.location.protocol + '//' + window.location.host,
    API: window.location.protocol + '//' + window.location.host + '/api'
};*/

// gulp serve
window.PORTAL_URLS = {
    BASE: `http://localhost`,
    API: `http://localhost`
};

export default portal;
export {Component, View, RouteConfig, Inject, Run, Config, Service, Filter, Directive, Constant};
