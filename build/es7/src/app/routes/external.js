'use strict';

import {RouteConfig, Inject} from '../decorators';  // jshint unused: false

//start-non-standard
@RouteConfig('external', {
    url: window.PORTAL_URLS.API,
    params: {
        destUrl: null
    }
})
@Inject()
//end-non-standard
class External {
    constructor() {
        console.log('external');
    }
}

export default External;
