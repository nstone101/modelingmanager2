'use strict';

import './users/users';
import './reports/reports';
import './nodes/nodes';
import './admin-tools/admin-tools';
import './modeling/modeling';
import './modeling2/modeling2';


import template from './layout-portal.html!text';
import {RouteConfig, Inject} from '../../decorators';  // jshint unused: false

//start-non-standard
@RouteConfig('portal', {
    url: '/portal',
    abstract: true,
    replace: true,
    template: template
})
@Inject('$rootScope', 'TokenModel')
//end-non-standard
class LayoutPortal {
    constructor($rootScope, TokenModel) {
        this.isCollapsed = false;
        if($rootScope.currentUser === undefined) {
            $rootScope.currentUser = TokenModel.getCurrentUser();
        }

        $rootScope.$bus.subscribe('SIDEBAR_TOGGLE_COLLAPSE', (data) => {
            this.isCollapsed = data.isCollapsed;
        });
    }
}

export default LayoutPortal;


/*
_.map(nodes.data, (item, key)=>{
    if(item === 'all') { return; }
    this.browser.push(item);
    this.collapse[key] = true;
});*/
