/**
 * Created by bpowell on 2/1/16.
 */
'use strict';
// special set of javascript
/*import '../../../../core/libs/jquery-ui.min';
import '../../../../core/libs/report-builder-lib';*/
import template from './report-builder.html!text';

import {RouteConfig, Component, Inject} from '../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.reports.reportBuilder', {
    url: '/reportBuilder',
    template: template
})
@Component({
    selector: 'report-builder'
})
@Inject()
//end-non-standard
class ReportBuilder {
    constructor(){

    }
}

export default ReportBuilder;

