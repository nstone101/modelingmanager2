'use strict';

import template from './node.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.reports.node', {
    url: '/node',
    template: template
})
@Component({
    selector: 'node-reports'
})
@Inject('$scope', '$rootScope', '$state', 'GridService', 'ReportModelService')
//end-non-standard
class NodeReports {
    constructor($scope, $rootScope, $state, GridService, ReportModelService) {
        this.dropDownTitle = 'MX960 Physical Ports that Require Scanning';
        this.GridService = GridService;
        this.reportModelService = ReportModelService;
        this.rootScope = $rootScope;
        this.scope = $scope;
        this.router = $state;

        this.updatePageView('Mx960PortsRequiringScanning', 'MX960 Physical Ports that Require Scanning');

        this.headerCollapsed = false;
        $rootScope.$bus.subscribe('HEADER_TOGGLE_COLLAPSE', (data)=>{
            this.headerCollapsed = data.headerCollapsed;
        });
    }

    updatePageView(id, title){
        this.rootScope.$bus.publish('REPORT_TITLE_UPDATE', {
            title: title
        });

        this.rootScope.$bus.publish('REPORT_AREA_UPDATE', {
            area: title
        });

        this.dropDownTitle = title;
        this.updateGridView(id);
    }

    updateGridView(id) {
        this.model = this.reportModelService.getReportModel(id);
        this.multiSelectColumns = this.model.getColumns();
        this.GridService.init({
            columnDefs: this.multiSelectColumns
        }, this.scope);

        // request the data
        this.model
            .initCollection()
            .then((resp) => {
                this.GridService.setData(this.model.getCollection());
                this.GridService.gridApi.core.handleWindowResize();
                this.rootScope.currentPageModel = this.model;
            });
    }

    toggleGridColumns(data) {
        if(this.GridService) {
            this.GridService.toggleGridColumns(data);
        }
    }

    redirect(report){
        this.router.go(report);
    }
}

export default NodeReports;
