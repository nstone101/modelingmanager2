'use strict';

import template from './changelogs.html!text';
import {RouteConfig, Component, Inject} from '../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.reports.changelogs', {
    url: '/changelogs',
    template: template
})
@Component({
    selector: 'changelogs-report'
})
@Inject('$scope', '$rootScope', 'GridService', 'NodePageModelService')
//end-non-standard
class ChangelogsReport {
    constructor($scope, $rootScope, GridService, NodePageModelService) {
        this.GridService = GridService;
        this.nodePageModelService = NodePageModelService;
        this.user = $rootScope.currentUser;
        this.rootScope = $rootScope;
        this.scope = $scope;

        this.tooltipTemplate =
            `<div style="z-index:999">
                <span>Edited By: %editBy%</span>
                <br/>
                <p>%updateValues%</p>
                <br/>
                <span>Comment:</span>
                <p>%comment%</p>
            </div>`;

        this.updatePageView('NodeDeviceNames', 'Node Device Names');

        this.headerCollapsed = false;
        $rootScope.$bus.subscribe('HEADER_TOGGLE_COLLAPSE', (data)=>{
            this.headerCollapsed = data.headerCollapsed;
        });
    }

    updatePageView(id, title){
        this.rootScope.$bus.publish('REPORT_PAGE_TITLE_UPDATE', {
            title: title
        });

        this.rootScope.$bus.publish('REPORT_AREA_UPDATE', {
            area: title
        });

        this.dropDownTitle = title;
        this.updateGridView(id);
    }

    updateGridView(id) {
        this.model = this.nodePageModelService.getNodePageModel(id, true);
        this.multiSelectColumns = this.model.getColumns('audit');
        this.GridService.init({
            columnDefs: this.multiSelectColumns
        }, this.scope);

        // request the data
        this.model
            .initCollection()
            .then((resp) => {
                // for the changelog report,
                // need to add tooltips
                var list = this.model.getCollection();
                _.map(list, (row, index) => {
                    var curId = 0, prevId = 0;
                    var updateVals = '';
                    // these are the only ones we care about.
                    if(row.Action === 'update') {
                        console.log(row);
                        console.log(index);
                        curId = row.RowID;
                        prevId = (list[index-1]) ? list[index-1].RowID : 0;
                        // TODO: get some time to actually think about doing this....
                        // ugh, need to do a field by field comparision...
                        if(prevId === curId) {
                            // already processed, move along.
                            // we only care about the first update of the group.
                            delete(list[index-1]);
                            return;
                        }


                        if(list[index+1]) {
                            _.map(row, function (field, key) {
                                if (key === 'id' || key === 'ToolTip') {return;}

                                if (field !== list[index + 1][key]) {
                                    if(field !== null) {
                                        updateVals += 'Field: ' + key + '<br/>' + 'Previous Value: ' + field + '<br/>';
                                    }
                                }
                            });
                            list[index + 1].ToolTip = this.tooltipTemplate.replace('%editBy%', row.EditedBy).replace('%updateValues%', updateVals).replace('%comment%', row.Comment);
                            updateVals = '';
                        }
                        else {
                            list.ToolTip = this.tooltipTemplate.replace('%editBy%', row.EditedBy).replace('%updateValues%', updateVals).replace('%comment%', row.Comment);
                        }
                    }
                    else {
                        list.ToolTip = this.tooltipTemplate.replace('%editBy%', row.EditedBy).replace('%updateValues%', updateVals).replace('%comment%', row.Comment);
                    }
                });
                console.log(list[list.length-1]);
                this.GridService.setData(list);
                this.GridService.gridApi.core.handleWindowResize();
                this.rootScope.currentPageModel = this.model;
            });

    }

    toggleGridColumns(data) {
        if(this.GridService) {
            this.GridService.toggleGridColumns(data);
        }
    }
}

export default ChangelogsReport;
