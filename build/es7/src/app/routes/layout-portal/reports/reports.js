'use strict';

import './changelogs/changelogs';
import './node/node';
import './report-builder/report-builder';

import {RouteConfig, Component} from '../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.reports', {
    url: '/reports',
    template: '<ui-view/>'
})
@Component({
    selector: 'reports'
})
//end-non-standard
class Reports {
    constructor() {

    }
}

export default Reports;
