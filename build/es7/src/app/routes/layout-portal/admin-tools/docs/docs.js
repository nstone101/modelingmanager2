'use strict';

import './download/downloadDoc';
import './delete/deleteDoc';
import './upload/uploadAdminDoc';

import template from './docs.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../decorators'; // jshint unused: false

//start-non-standard
@Component({
    selector: 'admin-tools-docs'
})
@View({
    template: template,
    replace: true
})
@Inject('$rootScope', '$timeout', 'FileModel')
//end-non-standard
class AdminDocs {
    constructor($rootScope, $timeout, FileModel) {
        this.timeout = $timeout;
        this.fileModel = FileModel;
        this.host = window.location.protocol + '//' + window.location.host;
        if(window.location.host.indexOf('localhost:8000') > -1) {
            /* jshint ignore:start */
            this.host = '';
            this.host =  window.PORTAL_URLS.BASE;
            /* jshint ignore:end */
        }

        this.fileModel.initCollection()
            .then((resp) => {
                    this.timeout(()=>{
                        this.docConfig = resp.configs;
                        this.paths = resp.paths;
                        this.docData = resp.files;
                    },0);
                });

        $rootScope.$bus.subscribe('DZ_FILE_UPLOADED', () => {
            this.fileModel.initCollection()
                .then((resp) => {
                    this.timeout(()=>{
                        this.docConfig = resp.configs;
                        this.paths = resp.paths;
                        this.docData = resp.files;
                    },0);
                });
        });

        $rootScope.$bus.subscribe('FILE_CONTAINER_DELETE', (params) => {
            console.log(params.filename);
            this.fileModel.resource.delete(params.filename)
                .then((resp) => {
                    this.fileModel.initCollection()
                        .then((resp) => {
                            this.timeout(()=>{
                                this.docConfig = resp.configs;
                                this.paths = resp.paths;
                                this.docData = resp.files;
                            },0);
                        });
                });
        });

        $rootScope.$bus.subscribe('DZ_DELETED_FILE', () => {
            this.fileModel.initCollection()
                .then((resp) => {
                    this.timeout(()=>{
                        this.docConfig = resp.configs;
                        this.paths = resp.paths;
                        this.docData = resp.files;
                    },0);
                });
        });
    }
}

export default AdminDocs;
