'use strict';

import template from './deleteDoc.html!text';
import {RouteConfig, Inject} from '../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.adminTools.deleteAdminDoc', {
    url: '/docs/deleteAdminDoc',
    params: {docConfig: null, doc: null, index: null},
    onEnter: ['$stateParams', '$uibModal', 'ModalService', ($stateParams, $modal, ModalService) => {
        $modal.open({
            template: template,
            controller: DeleteAdminDoc,
            controllerAs: 'vm',
            size: 'lg'
        }).result.finally(ModalService.onFinal('portal.adminTools'));
    }]
})
@Inject('$rootScope', '$state', '$stateParams', '$uibModalInstance', 'NodeResource')
//end-non-standard
class DeleteAdminDoc {
    constructor($rootScope, $state, $stateParams, $modalInstance, NodeResource) {
        this.modal = $modalInstance;
        this.params = $stateParams;
        if(this.params.doc === undefined){
            this.modal.dismiss('cancel');
        }
        this.params.filename = this.params.doc.replace(/^.*[\\\/]/, '');
        
        this.NodeResource = NodeResource;
        this.rootScope = $rootScope;
    }

    deleteNodeDoc(){
        this.NodeResource.deleteNodeDoc({
            name: this.params.docConfig.name,
            docType: this.params.docConfig.docType,
            directory: this.params.docConfig.directory,
            logFile: this.params.docConfig.logFile,
            doc: this.params.doc,
            siteName: this.params.nodeName
            }).then((resp) => {
                console.log(resp);
                this.rootScope.$bus.publish('DZ_DELETED_FILE');
                this.modal.dismiss('cancel');
            });
    }

    cancel() {
        this.modal.dismiss('cancel');
    }
}

export default DeleteAdminDoc;
