'use strict';

import template from './uploadAdminDoc.html!text';
import {RouteConfig, Inject} from '../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.adminTools.uploadAdminDoc', {
    url: '/docs/uploadAdminDoc',
    params: {config: null},
    onEnter: ['$stateParams', '$uibModal', 'ModalService', ($stateParams, $modal, ModalService) => {
        $modal.open({
            template: template,
            controller: UploadAdminDoc,
            controllerAs: 'vm',
            size: 'md'
        }).result.finally(ModalService.onFinal('portal.adminTools'));
    }]
})
@Inject('$rootScope', '$scope', '$state', '$stateParams', '$uibModalInstance')
//end-non-standard
class UploadAdminDoc {
    constructor($rootScope, $scope, $state, $stateParams, $modalInstance) {
        this.modal = $modalInstance;
        if($stateParams.config === undefined){
            this.modal.dismiss('cancel');
        }
        this.router = $state;
        this.result = null;
        this.config = $stateParams.config;

        this.dropzoneApi = {};

        this.dzOptions = {
            url: window.PORTAL_URLS.BASE + '/api/files/',
            onRegisterApi: (api) => {
                this.dropzoneApi = api;
                this.itemsInQueue = api.itemsInQueue;
            }
        };

        $rootScope.$bus.subscribe('DZ_ADDED_FILE', (params) => {this.itemsInQueue = params.itemsInQueue;});
        $rootScope.$bus.subscribe('DZ_REMOVED_FILE', (params) => {this.itemsInQueue = params.itemsInQueue;});
        $rootScope.$bus.subscribe('DZ_QUEUE_COMPLETE', (params) => {this.itemsInQueue = params.itemsInQueue;});

        window.addEventListener('dragover',function(e){
            e = e || event;
            console.log(e);
            console.log(e.target);
            console.log(e.target.parentNode.id);
            e.preventDefault();

        },false);

        window.addEventListener('drop',function(e){
            e = e || event;
            e.preventDefault();
        },false);

    }

    cancel() {
        this.modal.dismiss('cancel');
    }

    processDropzone() {
        this.dropzoneApi.processDropzone();
    }

    resetDropzone(){
        this.dropzoneApi.resetDropzone();
    }
}

export default UploadAdminDoc;
