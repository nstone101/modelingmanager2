'use strict';

import './docs/docs';
import template from './admin-tools.html!text';

import {RouteConfig, Inject} from '../../../decorators';  // jshint unused: false

//start-non-standard
@RouteConfig('portal.adminTools', {
    url: '/admin-tools',
    template: template
})
@Inject()
//end-non-standard
class AdminTools { }

export default AdminTools;