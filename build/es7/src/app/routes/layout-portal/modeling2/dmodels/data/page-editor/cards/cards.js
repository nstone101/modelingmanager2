'use strict';

//import './cards-add';
//import './cards-edit';'use strict';

import template from './cards.html!text';
import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false


//start-non-standard
@RouteConfig('portal.modeling2.dmodels.page-editor.cards', {
    url: '/cards',
    template: template
})
@Component({
    selector: 'cards-editor'
})
//end-non-standard
class CardsEditor {
    constructor($rootScope, $state, $timeout, $scope) {
        this.collapse = {};
        this.browser = [];
        this.$state = $state;
        this.$scope = $scope;

  $rootScope.deleteRow = function(row) {
    let index = $rootScope.gridOptionsCards.data.indexOf(row.entity);
    $rootScope.gridOptionsCards.data.splice(index, 1);
  };
  $rootScope.addData = function() {
    let n = $rootScope.gridOptionsCards.data.length + 1;
    $rootScope.gridOptionsCards.data.push({
                'Size': 'New ' + n,
                'ICard': 'ICard' + n,
                'IType': 'abc',
                'Purpose': 'abc',
                'UpdatedTime': '',
                'EditedBy': '',
                'OptionBin': '' 
              });
  }; 



$rootScope.gridOptionsCards = {
        enableFiltering: true,
        showGridFooter: true,
        showColumnFooter: true,
        exporterMenuCsv: true,
        enableGridMenu: true,
        columnDefs: [
          { name: 'id', width: 100, enableHiding:true, visible:false },
          { name: 'Size', width: 100 },
          { name: 'ICard', width: 450},
          { name: 'IType', width: 100 },
          { name: 'Purpose', enableHiding: true, visible:false },
          { name: 'UpdatedTime', enableHiding: true, visible:false },
          { name: 'EditedBy', enableHiding: true, visible: false },
          { name: 'OptionBin', enableCellEdit: false,
             cellTemplate:'<button class="btn primary" style="margin:3px;" ng-click="grid.appScope.showMe()"><i class="fa fa-eye" style="color:black;"></i></button><button class="btn btn-danger" style="margin:3px;" ng-click="grid.appScope.deleteRow(row)"><i class="fa fa-trash"></i></button>'}
             //{ name: 'UpdatedTime', width: 500 }
           /*cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
              if (grid.getCellValue(row,col) === 'xe') {
                return 'blue';
              } else if (grid.getCellValue(row,col) === 'ge') {
                return 'pink';
              } else if (grid.getCellValue(row,col) === 'pa') {
                return 'white';
            }
          },*/
        ],
        //getMicData(){};
        //$http.get(/api/{this.base}/{this.route}/data) };
        data: [
          {
         'id': 212,
         'Size': 8,
         'ICard': '8 Port 10G Interface Card ( MPC2 + 2x 4x 10G MIC)',
         'IType': 'xe',
         'Purpose': '',
         'UpdatedTime': '2016-02-13 05:18:47',
         'EditedBy': null
         //'Comment': null
         },
         {
         'id': 214,
         'Size': 40,
         'ICard': '40 Port 100m/1G baseT ( MPC1 + 1x 40x 1G MIC )',
         'IType': 'ge',
         'Purpose': '',
         'UpdatedTime': '2016-02-13 05:18:41',
         'EditedBy': null
         //'Comment': null
         },
         {
         'id': 213,
         'Size': 4,
         'ICard': '4 Port 10G Interface Card ( MPC2 + 1x 4x 10G MIC)',
         'IType': 'xe',
         'Purpose': '',
         'UpdatedTime': '2016-02-13 05:18:56',
         'EditedBy': null
         //'Comment': null
         },
         {
         'id': 215,
         'Size': 20,
         'ICard': '20 Port 1G baseT ( SRX5K-MPC + 1x SRX-MIC-20GE-SFP )',
         'IType': 'ge',
         'Purpose': '',
         'UpdatedTime': '2016-02-13 05:19:01',
         'EditedBy': null
         //'Comment': null
         },
         {
         'id': 216,
         'Size': 20,
         'ICard': '20 Port IXIA Anue NTO 5293',
         'IType': 'pa',
         'Purpose': '',
         'UpdatedTime': '2016-02-13 05:19:10',
         'EditedBy': null
         //'Comment': null
         }
        ],
        /*padata: [
         {
            'id': 25,
            'Size': 20,
            'IType': 'xe'
         },
         {
            'id': 1000,
            'Size': 40,
            'IType': 'xe',
         }
        ]*/
      };
    }

}

export default CardsEditor;
