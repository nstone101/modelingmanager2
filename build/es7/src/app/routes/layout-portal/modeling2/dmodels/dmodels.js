'use strict';

import './data/dmodels-data';

import template from './dmodels.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.modeling2.dmodels', {
    url: '/dmodels',
    template: template
    //resolve: {}  //was resolve node: {stateParams, NodeResource}  that's where the below where 'dmodels' is used to be 'node'
}
)
@Component({
    selector: 'dmodels'
})
@Inject('$rootScope', '$stateParams', 'dmodels')
//end-non-standard
class DModel2 {
    constructor($rootScope, $stateParams, dmodels) {
        console.log('dmodels.js here');
        
        this.user = $rootScope.currentUser;
        this.rootScope = $rootScope;
        $rootScope.dmodels = this.dmodels = dmodels.data;
        this.displayDmodelsData = true; //this.displayNodeData was 
        this.title = '';

        $rootScope.$bus.subscribe('DMODEL_PAGE_TITLE_UPDATE', (data) => {
            this.updateTitle(data.title);
    });

        window.addEventListener('dragover',function(e){
            e = e || event;
            e.preventDefault();

        },false);

        window.addEventListener('drop', function(e){
            e = e || event;
            e.preventDefault();
        },false);
    }

    updateTitle(title){
        this.title = title;
        this.rootScope.$bus.publish('DMODEL_AREA_UPDATE', {
            area: this.title
        });
    }
}

export default DModel2;
