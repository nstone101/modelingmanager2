'use strict';

import './cards/cards';

import {RouteConfig, Component} from '../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.modeling.dmodels.page-editor', {
    url: '/editor',
    template: '<ui-view/>'
})
@Component({
    selector: 'page-editor'
})
//end-non-standard
class DModelsPageEditor {
    constructor() {
        console.log('dmodels page editore loaded');
    }
}

export default DModelsPageEditor;
