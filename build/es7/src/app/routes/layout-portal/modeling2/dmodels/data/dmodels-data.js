'use strict';

import './page-editor/page-editor';

import template from './dmodels-data.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../../decorators'; // jshint unused: false

//start-non-standard
@Component({
    selector: 'dmodels-data'
})
@View({
    template: '<ui-view/>'
    //template: template,
    //replace: true
})
@Inject('$scope', '$rootScope', '$timeout', '$window', '$state', 'GridService', 'DModelsPageModelService')
//end-non-standard
class DModelsData {
    constructor($scope, $rootScope, $timeout, $window, $state, GridService, DModelsPageModelService) {
        this.GridService = GridService;
        this.loadingData = {};
        this.dmodelsPageModelService = DModelsPageModelService;
        //this.node = $rootScope.node;
        this.user = $rootScope.currentUser;
        this.$timeout = $timeout;
        this.rootScope = $rootScope;
        this.$scope = $scope;
        this.router = $state;

        //this.updatePageView('NodeDeviceNames', 'Node Device Names');


        this.headerCollapsed = false;
        $rootScope.$bus.subscribe('HEADER_TOGGLE_COLLAPSE', (data)=>{
            this.headerCollapsed = data.headerCollapsed;
        });
    }

    updatePageView(id, title){
        this.rootScope.$bus.publish('NODE_PAGE_TITLE_UPDATE', {
            title: title
        });

        this.rootScope.$bus.publish('NODE_AREA_UPDATE', {
            area: title
        });

        this.dropDownTitle = title;
        this.updateGridView(id);
    }

    updateGridView(id) {
        this.model = this.dmodelsPageModelService.getDModelsPageModel(id);
        this.editorLink = this.dmodelsPageModelService.getEditorLink(id);
        this.multiSelectColumns = this.model.getColumns();
        this.GridService.init({
            columnDefs: this.multiSelectColumns
        }, this.$scope);

        // request the data
        this.model
            .initCollection({})
            .then((resp) => {
                this.GridService.setData(this.model.getCollection());
                this.GridService.gridApi.core.handleWindowResize();
                this.rootScope.currentPageModel = this.model;
            });

    }

    toggleGridColumns(data) {
        if(this.GridService) {
            this.GridService.toggleGridColumns(data);
        }
    }

    addEntry() {
        this.router.go(this.editorLink + '.add');
    }

    editEntry(){
        this.router.go(this.editorLink + '.edit', {id: this.GridService.selectedRows[0].id});
    }

    deleteEntries() {
        if(this.GridService.selectedRows.length > 1){
            this.model.batchDelete(this.GridService.selectedRows);
        }
        else {
            this.model.delete(this.GridService.selectedRows[0]);
        }

    }
}

export default DModelsData;