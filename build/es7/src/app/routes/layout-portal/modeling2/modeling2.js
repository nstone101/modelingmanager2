'use strict';

import './dmodels/dmodels';//problem

import {RouteConfig, Inject} from '../../../decorators';  // jshint unused: false

//start-non-standard
@RouteConfig('portal.modeling2', {
    url: '/modeling2',
    abstract: true,
    template: '<ui-view/>'
})
@Inject()
//end-non-standard
class Modeling2 { }
