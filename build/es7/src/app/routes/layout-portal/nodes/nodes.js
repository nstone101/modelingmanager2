'use strict';

//import './models-manager/models-manager';
import './node/node';
import './browser/browser';
import './newnodesbrowser/newnodesbrowser';

import {RouteConfig, Inject} from '../../../decorators';  // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes', {
    url: '/nodes',
    abstract: true,
    template: '<ui-view/>'
})
@Inject()
//end-non-standard
class Nodes { }
