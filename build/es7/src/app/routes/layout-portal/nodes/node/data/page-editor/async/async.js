'use strict';

import './async-add';
import './async-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.async', {
    url: '/async',
    template: '<ui-view/>'
})
@Component({
    selector: 'async-editor'
})
//end-non-standard
class AsyncEditor {
    constructor() {
    }
}

export default AsyncEditor;
