'use strict';

import './customer-addressing-add';
import './customer-addressing-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.customer-addressing', {
    url: '/customer-addressing',
    template: '<ui-view/>'
})
@Component({
    selector: 'customer-addressing-editor'
})
//end-non-standard
class CustomerAddressingEditor {
    constructor() {
    }
}

export default CustomerAddressingEditor;