'use strict';

import './info/info';
import './docs/docs';
import './photos/photos';
import './diagrams/diagrams';
import './data/node-data';

import template from './node.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node', {
    url: '/node/:nodeName',
    template: template,
    resolve: {
        node: ($stateParams, NodeResource) => {
            return NodeResource.getNodeByName($stateParams.nodeName);
        }
    }
})
@Component({
    selector: 'node'
})
@Inject('$rootScope', '$stateParams', 'node')
//end-non-standard
class Node {
    constructor($rootScope, $stateParams, node) {
        this.user = $rootScope.currentUser;
        this.rootScope = $rootScope;
        $rootScope.node = this.node = node.data;
        this.displayNodeData = true;
        this.title = '';

        $rootScope.$bus.subscribe('NODE_PAGE_TITLE_UPDATE', (data) => {
            this.updateTitle(data.title);
        });

        window.addEventListener('dragover',function(e){
            e = e || event;
            console.log(e);
            console.log(e.target);
            console.log(e.target.parentNode.id);
            e.preventDefault();

        },false);

        window.addEventListener('drop', function(e){
            e = e || event;
            e.preventDefault();
        },false);

        this.headerCollapsed = false;
        $rootScope.$bus.subscribe('HEADER_TOGGLE_COLLAPSE', (data)=>{
            this.headerCollapsed = data.headerCollapsed;
        });
    }

    updateTitle(title){
        this.title = title;
        this.rootScope.$bus.publish('NODE_AREA_UPDATE', {
            area: this.title
        });
    }
}

export default Node;
