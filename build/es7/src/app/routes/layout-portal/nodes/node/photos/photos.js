'use strict';

import './upload/uploadPhoto';
import './archive/archive';

import template from './photos.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../../decorators'; // jshint unused: false

//start-non-standard
@Component({
    selector: 'photos'
})
@View({
    template: template,
    replace: true
})
@Inject('$rootScope', '$timeout', 'NodeModel')
//end-non-standard
class Photos {
    constructor($rootScope, $timeout, NodeModel) {
        this.node = $rootScope.node;
        NodeModel.getNodePhotos(this.node.siteName)
            .then((resp)=>{
                this.photos = resp.photos;
            });
        $rootScope.$bus.subscribe('DZ_FILE_UPLOADED', () => {
            NodeModel.getNodePhotos(this.node.siteName)
            .then((resp)=>{
                    $timeout(()=>{
                        this.photos = resp.photos;
                    }, 0);
            });
        });

        $rootScope.$bus.subscribe('DZ_DELETED_FILE', () => {
            NodeModel.getNodePhotos(this.node.siteName)
            .then((resp)=>{
                    $timeout(()=>{
                        this.photos = resp.photos;
                    }, 0);
            });
        });
    }
}

export default Photos;