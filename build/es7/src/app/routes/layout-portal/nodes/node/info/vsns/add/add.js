'use strict';

import template from './add.html!text';
import {RouteConfig, Inject} from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('nodes.node.info.vsnAdd', {
    url: '/vsn/add',
    onEnter: ['$uibModal', 'ModalService', ($modal, ModalService) => {
        $modal.open({
            template: template,
            controller: VsnAdd,
            controllerAs: 'vm',
            size: 'lg'
        }).result.finally(ModalService.onFinal('nodes.node'));
    }]
})
@Inject('$rootScope', '$state', 'FormService', '$uibModalInstance')
//end-non-standard
class VsnAdd {
    constructor($rootScope, $state, FormService, $modalInstance) {
        this.modal = $modalInstance;
        this.router = $state;
        this.result = null;
        this.node = $rootScope.node;
        this.FormService = FormService;
        this.isSubmitting = null;
        this.saveButtonOptions = Object.assign({}, FormService.getModalSaveButtonOptions()); // clone the modal save button options so we don't overwrite default one
        this.saveButtonOptions.buttonDefaultText = 'Add a VSN';
        this.saveButtonOptions.buttonSuccessText = 'Added a VSN';
        this.saveButtonOptions.buttonSubmittingText = 'Adding a VSN';
    }

    cancel() {
        this.modal.dismiss('cancel');
    }

    ok(form) {
        this.isSubmitting = true;
        //this.FormService.save(this.EmployeeModel, this.employee, this, form);
    }
}

export default VsnAdd;
