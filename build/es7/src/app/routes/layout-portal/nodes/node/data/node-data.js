'use strict';

import './page-editor/page-editor';

import template from './node-data.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../../decorators'; // jshint unused: false

//start-non-standard
@Component({
    selector: 'node-data'
})
@View({
    template: template,
    replace: true
})
@Inject('$scope', '$rootScope', '$timeout', '$window', '$state', 'GridService', 'NodePageModelService')
//end-non-standard
class NodeData {
    constructor($scope, $rootScope, $timeout, $window, $state, GridService, NodePageModelService) {
        this.dropDownTitle = 'Customer Addressing';
        this.GridService = GridService;
        this.loadingData = {};
        this.nodePageModelService = NodePageModelService;
        this.node = $rootScope.node;
        this.user = $rootScope.currentUser;
        this.$timeout = $timeout;
        this.rootScope = $rootScope;
        this.$scope = $scope;
        this.router = $state;

        this.updatePageView('NodeDeviceNames', 'Node Device Names');


        this.headerCollapsed = false;
        $rootScope.$bus.subscribe('HEADER_TOGGLE_COLLAPSE', (data)=>{
            this.headerCollapsed = data.headerCollapsed;
        });
    }

    updatePageView(id, title){
        this.rootScope.$bus.publish('NODE_PAGE_TITLE_UPDATE', {
            title: title
        });

        this.rootScope.$bus.publish('NODE_AREA_UPDATE', {
            area: title
        });
        
        this.dropDownTitle = title;
        this.updateGridView(id);
    }

    updateGridView(id) {
        this.model = this.nodePageModelService.getNodePageModel(id);
        this.editorLink = this.nodePageModelService.getEditorLink(id);
        this.multiSelectColumns = this.model.getColumns();
        this.GridService.init({
            columnDefs: this.multiSelectColumns
        }, this.$scope);

        // request the data
        this.model
            .initCollection({siteName: this.node.node.SiteName, vsn: 'all'})
            .then((resp) => {
                this.GridService.setData(this.model.getCollection());
                this.GridService.gridApi.core.handleWindowResize();
                this.rootScope.currentPageModel = this.model;
            });

    }

    toggleGridColumns(data) {
        if(this.GridService) {
            this.GridService.toggleGridColumns(data);
        }
    }

    addEntry() {
        this.router.go(this.editorLink + '.add');
    }

    editEntry(){
        this.router.go(this.editorLink + '.edit', {id: this.GridService.selectedRows[0].id});
    }

    deleteEntries() {
        if(this.GridService.selectedRows.length > 1){
            this.model.batchDelete(this.GridService.selectedRows);
        }
        else {
            this.model.delete(this.GridService.selectedRows[0]);
        }

    }
}

export default NodeData;