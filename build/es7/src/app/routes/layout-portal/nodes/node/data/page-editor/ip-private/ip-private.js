'use strict';

import './ip-private-add';
import './ip-private-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.ip-private', {
    url: '/ip-private',
    template: '<ui-view/>'
})
@Component({
    selector: 'ip-private-editor'
})
//end-non-standard
class IpPrivateEditor {
    constructor() {
    }
}

export default IpPrivateEditor;
