'use strict';

import {Component, View, Inject} from '../../../../../../decorators'; // jshint unused: false
const formStates = { 'view': 'view', 'edit': 'edit'};

//start-non-standard
@Component({
    selector: 'vsns'
})
@View({
    template: `
        <div class="vsns">
            <div class="form-group">
                <label class="control-label col-sm-2" for="VSNDisplay">VSNs</label>
                <div class="vsn-list">
                    <span style='color: #333333' ng-repeat="vsn in vm.vsns track by $index" class="badge {{vsn.replace(' ', '-').toLowerCase()}}">{{vsn}}</span>
                </div>
            </div>
        </div>`,
    scope: {
        data: '=',
        state: '='
    }
})
@Inject('$scope')
//end-non-standard
class Vsns {
    constructor($scope) {
        if($scope.data === undefined) { return; }
        this.vsns = $scope.data.split(',');
    }
}

export default Vsns;