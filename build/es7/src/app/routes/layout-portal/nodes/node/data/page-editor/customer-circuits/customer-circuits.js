'use strict';

import './customer-circuits-add';
import './customer-circuits-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.customer-circuits', {
    url: '/customer-circuits',
    template: '<ui-view/>'
})
@Component({
    selector: 'customer-circuits-editor'
})
//end-non-standard
class CustomerCircuitsEditor {
    constructor() {
    }
}

export default CustomerCircuitsEditor;
