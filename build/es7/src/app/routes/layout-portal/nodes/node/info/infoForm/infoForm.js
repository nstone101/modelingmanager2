'use strict';
import moment from 'moment';
import momentTz from 'moment-timezone';

import template from './infoForm.html!text';
import {Component, View, Inject} from '../../../../../../decorators'; // jshint unused: false
const formStates = { 'view': 'view', 'edit': 'edit'};
const SERVICE = new WeakMap();

//start-non-standard
@Component({
    selector: 'info-form'
})
@View({
    template: template,
    scope: {
        data: '='
    }
})
@Inject('$rootScope', '$scope', 'NodeModel', 'NodeResource')
//end-non-standard
class InfoForm {
    constructor($rootScope, $scope, NodeModel, NodeResource) {
        SERVICE.set(this, NodeModel);

        SERVICE.get(this).getDesignDocs($scope.data.node.GenType);

        this.isEditing = 'false';
        this.vsns = [];
        // Formly
        this.loadingPromise = {};
        this.author = {};
        this.env = {};
        this.node = $scope.data.node;
        this.model = $scope.data.node;

        this.NodeResource = NodeResource;
        this.rootScope = $rootScope;

        this.options = {
            formState: {
                horizontalLabelClass: 'col-sm-2',
                horizontalFieldClass: 'col-sm-10',
                readOnly: true
            }
        };
        /* jshint ignore:start */


        this.editState = {
            'templateOptions.disabled': 'formState.readOnly',
            'templateOptions.readOnly': 'formState.readOnly'
        };


        this.fields = [
            {"className": "col-md-6", "fieldGroup":[
                {key: 'SiteName', type: 'input', templateOptions: {label: 'Node Name', placeholder: ''}, ngModelElAttrs: {readOnly: '', disabled: ''}},
                {key: 'LongName', type: 'input', templateOptions: {label: 'Long Name', placeholder: '', maxlength: 256}, expressionProperties:this.editState},
                {key: 'GenType', type: 'input', templateOptions: {label: 'Type', placeholder: ''}, ngModelElAttrs: {readOnly: '', disabled: ''}},
                {
                    key: 'SiteGroup',
                    type: 'select',
                    templateOptions: {
                        label: 'Group',
                        valueProp: 'value',
                        labelProp: 'SiteGroup',
                        placeholder: 'Select Node Group',
                        options: [
                            {"SiteGroup":"Production (processing live traffic)","value":"Production"},
                            {"SiteGroup":"Production (adding a VSN)","value":"Production-Add-VSN"},
                            {"SiteGroup":"Production (no live traffic)","value":"Pre-Production"},
                            {"SiteGroup":"Lab","value":"Lab"},
                            {"SiteGroup":"Decommissioned","value":"Deactivated"},
                            {"SiteGroup":"New Node","value":"NewNode"}
                        ]
                    },
                    expressionProperties:this.editState
                },
                {
                    key: 'SiteState',
                    type: 'select',
                    templateOptions: {
                        label: 'State',
                        valueProp: 'value',
                        labelProp: 'SiteState',
                        placeholder: 'Select Node State',
                        options: [
                            {"SiteState":"Staging equipment","value":"Staging-Equipment"},
                            {"SiteState":"Install/Async Verification","value":"Install-ASync-Verification"},
                            {"SiteState":"Site Visit/Base Config","value":"Site-Visit-Base-Config"},
                            {"SiteState":"Node Device Config","value":"Node-Device-Config"},
                            {"SiteState":"Network Verification","value":"Network-Verification"},
                            {"SiteState":"Failover Pre-Verification: 25% Complete","value":"Failover-Complete-25"},
                            {"SiteState":"Failover Pre-Verification: 50% Complete","value":"Failover-Complete-50"},
                            {"SiteState":"Failover Pre-Verification: 75% Complete","value":"Failover-Complete-75"},
                            {"SiteState":"Failover Pre-Verification: 100% Complete","value":"Failover-Complete-100"},
                            {"SiteState":"Call Through Testing","value":"Released-ISNTS"},
                            {"SiteState":"Accepted by ISNTS","value":"Accepted-ISNTS"}
                        ]
                    },
                    expressionProperties:this.editState
                },
                {key: 'EditedBy',type: 'input', templateOptions: {label: 'Updated By', placeholder: '', ngReadonly: true}, ngModelElAttrs: {readOnly: '', disabled: ''}},
                {key: 'UpdatedTime', type: 'input', templateOptions: {label: 'Last Updated', placeholder: ''}, ngModelElAttrs: {readOnly: '', disabled: ''}},
                {key: 'Comment', type: 'textarea', templateOptions: { label: 'Comment', required: true, "rows": 4}, expressionProperties:this.editState}
            ]},

            {"className": "col-md-6", "fieldGroup":[
                //TODO: This will be replaced with a chip list Will be added to modal. Not the same as above.
                {key: 'VSN', type: 'input', templateOptions: {label: 'VSNs', placeholder: ''}, expressionProperties:this.editState},

                /*{
                 key: 'VSN',
                 type: 'ui-select-multiple',
                 templateOptions: {
                 required: true,
                 type: 'email',
                 optionsAttr: 'bs-options',
                 ngOptions: 'option[to.valueProp] as option in to.options track by $index',
                 label: 'VSNs',
                 valueProp: 'vsn',
                 labelProp: 'vsn',
                 placeholder: 'Select VSNs',
                 options: vsnTypesService
                 /!*options: function(){
                 return '{[{"vsn": "Car"},{"vsn2": "Helicopter"}]}'
                 }*!/
                 },
                 //parsers: [parseVSN]
                 },*/

                //TODO: Will be added to modal.
                /*{
                 key: 'Title',
                 type: 'ui-select-multiple',
                 templateOptions: {
                 optionsAttr: 'bs-options',

                 ngOptions: 'option[to.valueProp] as option in to.options track by $index',
                 label: 'Title(s)',
                 valueProp: 'value',
                 labelProp: 'title',
                 placeholder: 'Select Title',
                 options: contactTitlesService
                 }
                 },*/

                {key: 'SiteCode', type: 'input', templateOptions: {label: 'Code', placeholder: ''}, expressionProperties:this.editState},
                {key: 'SiteMux', type: 'input', templateOptions: {label: 'Mux', placeholder: ''}, expressionProperties:this.editState},
                {key: 'ClliCode', type: 'input', templateOptions: {label: 'CLLI Code', placeholder: ''}, expressionProperties:this.editState},
                {key: 'EMTS', type: 'input', templateOptions: {label: 'ETMS', placeholder: ''}, expressionProperties:this.editState},


                // TODO: Need add a link to the file for download.
                {
                    key: 'MOP',
                    type: 'select',
                    templateOptions: {
                        label: 'Base MOP',
                        valueProp: 'path',
                        labelProp: 'doc',
                        required: false,
                        placeholder: 'Select MOP',
                        options: []
                    },
                    expressionProperties:this.editState,

                    controller: /* @ngInject */ ($scope) => {
                                $scope.to.options = SERVICE.get(this).getMops('mops');
                    }
                },
                {
                    key: 'VSNMOP1',
                    type: 'select',
                    templateOptions: {
                        label: 'VSN MOP',
                        valueProp: 'path',
                        labelProp: 'doc',
                        required: false,
                        placeholder: 'Select MOP',
                        options: []
                    },
                    expressionProperties:this.editState,
                    controller: /* @ngInject */ ($scope) => {
                                $scope.to.options = SERVICE.get(this).getMops('vsnmops');
                    }
                },
                {
                    key: 'VSNMOP2',
                    type: 'select',
                    templateOptions: {
                        label: 'VSN MOP',
                        valueProp: 'path',
                        labelProp: 'doc',
                        required: false,
                        placeholder: 'Select MOP',
                        options: []
                    },
                    expressionProperties:this.editState,
                    controller: /* @ngInject */ ($scope) => {
                                $scope.to.options = SERVICE.get(this).getMops('vsnmops');
                    }
                },
                {
                    key: 'LLD',
                    type: 'select',
                    templateOptions: {
                        label: 'LLD',
                        valueProp: 'path',
                        labelProp: 'doc',
                        required: false,
                        placeholder: 'Select LLD',
                        options: []
                    },
                    expressionProperties:this.editState,
                    controller: /* @ngInject */ ($scope) => {
                                $scope.to.options = SERVICE.get(this).getMops('lld');
                    }
                }
            ]},

            {template: '<hr />'},

            {"className": "col-md-6", "fieldGroup":[

                {key: 'ContactName', type: 'input', templateOptions: {label: 'Contact Name', placeholder: ''}, expressionProperties:this.editState},
                {key: 'ContactPhone', type: 'input', templateOptions: {label: 'Contact Phone', placeholder: ''}, expressionProperties:this.editState},
                {key: 'ContactEmail', type: 'input', templateOptions: {label: 'Contact Email', placeholder: ''}, expressionProperties:this.editState},
            ]},

            {"className": "col-md-6", "fieldGroup":[

                {key: 'Address1', type: 'input', templateOptions: {label: 'Site Address 1', placeholder: ''}, expressionProperties:this.editState},
                {key: 'Address2', type: 'input', templateOptions: {label: 'Site Address 2', placeholder: ''}, expressionProperties:this.editState},
                {key: 'City', type: 'input', templateOptions: {label: 'City', placeholder: ''}, expressionProperties:this.editState},
                {
                    key: 'State',
                    type: 'select',
                    templateOptions: {
                        label: 'State',
                        valueProp: 'value',
                        labelProp: 'name',
                        placeholder: 'Select State',
                        options: [
                            {"name":"Alabama","value":"AL"},
                            {"name":"Alaska","value":"AK"},
                            {"name":"Arizona","value":"AZ"},
                            {"name":"Arkansas","value":"AR"},
                            {"name":"California","value":"CA"},
                            {"name":"Colorado","value":"CO"},
                            {"name":"Connecticut","value":"CT"},
                            {"name":"Delaware","value":"DE"},
                            {"name":"District of Columbia","value":"DC"},
                            {"name":"Florida","value":"FL"},
                            {"name":"Georgia","value":"GA"},
                            {"name":"Hawaii","value":"HI"},
                            {"name":"Idaho","value":"ID"},
                            {"name":"Illinois","value":"IL"},
                            {"name":"Indiana","value":"IN"},
                            {"name":"Iowa","value":"IA"},
                            {"name":"Kansa","value":"KS"},
                            {"name":"Kentucky","value":"KY"},
                            {"name":"Lousiana","value":"LA"},
                            {"name":"Maine","value":"ME"},
                            {"name":"Maryland","value":"MD"},
                            {"name":"Massachusetts","value":"MA"},
                            {"name":"Michigan","value":"MI"},
                            {"name":"Minnesota","value":"MN"},
                            {"name":"Mississippi","value":"MS"},
                            {"name":"Missouri","value":"MO"},
                            {"name":"Montana","value":"MT"},
                            {"name":"Nebraska","value":"NE"},
                            {"name":"Nevada","value":"NV"},
                            {"name":"New Hampshire","value":"NH"},
                            {"name":"New Jersey","value":"NJ"},
                            {"name":"New Mexico","value":"NM"},
                            {"name":"New York","value":"NY"},
                            {"name":"North Carolina","value":"NC"},
                            {"name":"North Dakota","value":"ND"},
                            {"name":"Ohio","value":"OH"},
                            {"name":"Oklahoma","value":"OK"},
                            {"name":"Oregon","value":"OR"},
                            {"name":"Pennsylvania","value":"PA"},
                            {"name":"Rhode Island","value":"RI"},
                            {"name":"South Carolina","value":"SC"},
                            {"name":"South Dakota","value":"SD"},
                            {"name":"Tennessee","value":"TN"},
                            {"name":"Texas","value":"TX"},
                            {"name":"Utah","value":"UT"},
                            {"name":"Vermont","value":"VT"},
                            {"name":"Virginia","value":"VA"},
                            {"name":"Washington","value":"WA"},
                            {"name":"West Virginia","value":"WV"},
                            {"name":"Wisconsin","value":"WI"},
                            {"name":"Wyoming","value":"WY"}
                        ]
                    },
                    expressionProperties:this.editState
                },
                {key: 'Zip', type: 'input', templateOptions: {label: 'Zip', placeholder: ''}, expressionProperties:this.editState},
                {key: 'Country', type: 'input', templateOptions: {label: 'Country', placeholder: ''}, expressionProperties:this.editState}
            ]}

            /*{template: '<hr />'}*/
        ];
        /* jshint ignore:end */

    }

    onSubmit(){
        this.updateInfo();
    }


    dumpCompleteNode () {

        this.NodeResource.dumpCompleteNode({
               siteName: this.node.SiteName
            }).then((resp) => {
               console.log('dump node csvs done, now reload NodeDocs for ' + this.node.SiteName);
                 this.rootScope.$bus.publish('DZ_FILE_UPLOADED');
            });
    }

    dumpNodeHeader () {
      return 'Dump Node Data' ;
    }

    dumpNodeMessage () {
      return '<p>Node Data CSVs are generated and available on the Node Docs Miscellaneous page.</p>';
    }



    completeInfo(){
        this.editInfo();
        this.model.SiteGroup = 'Pre-Production';
    }

    editInfo(){
        this.options.formState.readOnly = false;
        this.isEditing = true;
        this.options.updateInitialValue();
        this.model.Comment = '';
    }

    cancelEdit(){
        this.isEditing = false;
        this.options.formState.readOnly = true;
        //infoVM.fields = infoVM.originalFields;
        this.options.resetModel();
    }

    getInfo() {
        /*let nodeResource = SERVICE.get(this);
        this.loadingPromise = nodeResource.get(this.model.id)
            .then( (resp) => {
                var genType = resp.data[0].GenType;
                nodeResource.getDesignDocs(resp.data[0].GenType)
                    .then((resp) => {
                        this.model = resp.data
                    });
/!*                designDocsService.getDesignDocs('/api/designDocs/', genType)
                    .then(function(response){
                        infoVM.model = resp.data[0];
                        return response;
                    });*!/

                this.originalFields = angular.copy(this.fields);
                return resp.data[0];
            });*/
    }

    updateInfo() {
        this.model.id = this.node.SiteName;
        console.log(this.model);

        let nodeResource = SERVICE.get(this);
        this.loadingPromise = nodeResource.save(this.model)
            .then( (resp) => {
                this.options.formState.readOnly = true;
                this.isEditing = false;
            });
    }

    parseVSNs(data){
        //return JSON.stringify(infoVM.model.VSN);
        //console.log(infoVM.model.VSN);
        //console.log(JSON.stringify(infoVM.model.VSN));

        var items = data.split(',');
        //return items;

        var vsns = items.map((e)=> {
            return { vsn: e };
        });
        //console.log(vsns);
        return vsns;
    }
}

export default InfoForm;
