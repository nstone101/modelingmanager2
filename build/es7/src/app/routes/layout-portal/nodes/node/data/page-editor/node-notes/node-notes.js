'use strict';

import './node-notes-add';
import './node-notes-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.node-notes', {
    url: '/node-notes',
    template: '<ui-view/>'
})
@Component({
    selector: 'node-notes-editor'
})
//end-non-standard
class NodeNotesEditor {
    constructor() {
    }
}

export default NodeNotesEditor;
