'use strict';

import './upload/upload-diagram';

import template from './diagrams.html!text';
import {Component, View, Inject} from '../../../../../decorators'; // jshint unused: false

//start-non-standard
@Component({
    selector: 'node-diagram'
})
@View({
    template: template,
    replace: true
})
@Inject('$rootScope', '$timeout', 'NodeModel', '$window')
//end-non-standard
class Diagrams {
    constructor($rootScope, $timeout, NodeModel, $window) {
        this.node = $rootScope.node;
        this.user = $rootScope.currentUser;
        this.img = null;
        this.window = $window;
        this.config = {
            host: ''
        };
        if($window.location.host.indexOf('localhost:8000') > -1) {
            this.config.host = window.PORTAL_URLS.BASE;
        }

        NodeModel.getNodeDiagram(this.node.siteName)
            .then((resp)=>{
                this.getImageSrc(resp);
            });

        $rootScope.$bus.subscribe('DZ_FILE_UPLOADED', () => {
            NodeModel.getNodeDiagram(this.node.siteName)
                .then((resp)=>{
                    this.getImageSrc(resp);
                });
        });

        $rootScope.$bus.subscribe('DZ_DELETED_FILE', () => {
            NodeModel.getNodeDiagram(this.node.siteName)
                .then((resp)=>{
                    this.getImageSrc(resp);
                });
        });
    }

    getImageSrc(data){
        if(data !== undefined) {
            this.img = this.config.host + '/files/node/' + this.node.node.SiteName + '/diagram/diagram.png?'+ new Date().getTime();
        }
        else {
            this.img = undefined;
        }
    }
}

export default Diagrams;