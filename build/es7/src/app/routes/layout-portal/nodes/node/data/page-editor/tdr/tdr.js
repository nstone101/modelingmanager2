'use strict';

import './tdr-add';
import './tdr-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.tdr', {
    url: '/tdr',
    template: '<ui-view/>'
})
@Component({
    selector: 'tdr-editor'
})
//end-non-standard
class TdrEditor {
    constructor() {
    }
}

export default TdrEditor;
