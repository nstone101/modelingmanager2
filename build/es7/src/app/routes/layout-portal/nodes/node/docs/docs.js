'use strict';

import './download/downloadDoc';
import './delete/deleteDoc';
import './upload/uploadDoc';

import template from './docs.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../../decorators'; // jshint unused: false

//start-non-standard
@Component({
    selector: 'node-docs'
})
@View({
    template: template,
    replace: true
})
@Inject('$rootScope', '$timeout', 'NodeModel')
//end-non-standard
class Docs {
    constructor($rootScope, $timeout, NodeModel) {
        this.node = $rootScope.node;
        this.host = window.location.protocol + '//' + window.location.host;
        if(window.location.host.indexOf('localhost:8000') > -1) {
            /* jshint ignore:start */
            this.host = '';
            this.host =  window.PORTAL_URLS.BASE;
            /* jshint ignore:end */
        }
        NodeModel.getNodeDocs(this.node.siteName)
            .then((resp)=>{
                this.docData = resp.docs;
                this.paths = resp.paths;
                this.docConfig = NodeModel.getDocConfig();
            });
        $rootScope.$bus.subscribe('DZ_FILE_UPLOADED', () => {
            NodeModel.getNodeDocs(this.node.siteName)
            .then((resp)=>{
                this.docData = resp.docs;
                this.paths = resp.paths;
                this.docConfig = NodeModel.getDocConfig();
            });
        });

        $rootScope.$bus.subscribe('DZ_DELETED_FILE', () => {
            NodeModel.getNodeDocs(this.node.siteName)
            .then((resp)=>{
                this.docData = resp.docs;
                this.paths = resp.paths;
                this.docConfig = NodeModel.getDocConfig();
            });
        });
    }
}

export default Docs;
