'use strict';

import './dns-trap-log-add';
import './dns-trap-log-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.dns-trap-log', {
    url: '/dns-trap-log',
    template: '<ui-view/>'
})
@Component({
    selector: 'dns-trap-log-editor'
})
//end-non-standard
class DnsEditor {
    constructor() {
    }
}

export default DnsEditor;
