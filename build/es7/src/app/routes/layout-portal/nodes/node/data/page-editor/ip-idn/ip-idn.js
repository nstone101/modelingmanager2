'use strict';

import './ip-idn-add';
import './ip-idn-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.ip-idn', {
    url: '/ip-idn',
    template: '<ui-view/>'
})
@Component({
    selector: 'ip-idn-editor'
})
//end-non-standard
class IpIDNEditor {
    constructor() {
    }
}

export default IpIDNEditor;
