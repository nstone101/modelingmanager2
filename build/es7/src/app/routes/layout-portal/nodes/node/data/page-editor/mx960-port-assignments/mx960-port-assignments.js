'use strict';

import './mx960-port-assignments-add';
import './mx960-port-assignments-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.mx960-port-assignments', {
    url: '/mx960-port-assignments',
    template: '<ui-view/>'
})
@Component({
    selector: 'mx960-port-assignments-editor'
})
//end-non-standard
class Mx960PortAssignmentsEditor {
    constructor() {
    }
}

export default Mx960PortAssignmentsEditor;
