'use strict';

import './card-slotting-add';
import './card-slotting-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.card-slotting', {
    url: '/card-addressing',
    template: '<ui-view/>'
})
@Component({
    selector: 'card-slotting-editor'
})
//end-non-standard
class CardSlottingEditor {
    constructor() {
    }
}

export default CardSlottingEditor;
