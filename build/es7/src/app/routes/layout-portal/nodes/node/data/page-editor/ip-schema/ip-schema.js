'use strict';

import './ip-schema-add';
import './ip-schema-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.ip-schema', {
    url: '/ip-schema',
    template: '<ui-view/>'
})
@Component({
    selector: 'ip-schema-editor'
})
//end-non-standard
class IpSchemaEditor {
    constructor() {
    }
}

export default IpSchemaEditor;
