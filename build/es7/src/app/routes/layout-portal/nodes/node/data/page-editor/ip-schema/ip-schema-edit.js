'use strict';

import template from './ip-schema-edit.html!text';
import {RouteConfig, Inject} from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.ip-schema.edit', {
    url: '/:id/edit',
    onEnter: ['$stateParams', '$uibModal', 'ModalService', ($stateParams, $modal, ModalService) => {
        const id = $stateParams.id;
        $modal.open({
            template: template,
            resolve: {
                init: ['$rootScope', ($rootScope) => Promise.all([$rootScope.currentPageModel.initItem(id)])]
            },
            controller: IpSchemaEditEditor,
            controllerAs: 'vm',
            size: 'lg'
        }).result.finally(ModalService.onFinal('portal.nodes.node', {nodeName: $stateParams.nodeName}));
    }]
})
@Inject('$rootScope', '$scope', '$state', '$stateParams', '$uibModalInstance', 'FormService')
//end-non-standard
class IpSchemaEditEditor {
    constructor($rootScope, $scope, $state, $stateParams, $modalInstance, FormService) {
        this.result = null;
        this.isSubmitting = null;

        this.modal = $modalInstance;
        this.Model = $rootScope.currentPageModel;

        this.user = $rootScope.currentUser;
        this.node = $rootScope.node;
        this.entry = this.Model.getItem($stateParams.id);
        this.entry.EditedBy = this.user.username;
        this.entry.SiteName = this.node.node.SiteName;
        this.router = $state;

        this.FormService = FormService;
        this.saveButtonOptions = Object.assign({}, FormService.getModalSaveButtonOptions()); // clone the modal save button options so we don't overwrite default one
        this.saveButtonOptions.buttonDefaultText = 'Update';
        this.saveButtonOptions.buttonSuccessText = 'Updated';
        this.saveButtonOptions.buttonSubmittingText = 'Updating';
        this.saveButtonOptions.buttonInitialIcon = 'fa fa-plus';

        // node setup

        this.vsnOptions = this.node.nodeInfo.vsns;
    }

    cancel() {
        this.modal.dismiss('cancel');
    }

    save(form) {
        if(!form.$valid) {return;}

        this.isSubmitting = true;
        this.FormService.save(this.Model, this.entry, this, form);
    }
}

export default IpSchemaEditEditor;
