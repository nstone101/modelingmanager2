'use strict';

import './customer-addressing/customer-addressing';
import './customer-circuits/customer-circuits';
import './async/async';
import './ping-poller/ping-poller';
import './node-notes/node-notes';
import './tdr/tdr';
import './mx960-port-assignments/mx960-port-assignments';
import './ns5400-port-assignments/ns5400-port-assignments';
import './vpn-routing/vpn-routing';
import './node-device-names/node-device-names';
import './network-connections/network-connections';
import './ip-schema/ip-schema';
import './ip-public/ip-public';
import './ip-idn/ip-idn';
import './ip-private/ip-private';
import './fw-golden-config/fw-golden-config';
import './fw-setup/fw-setup';
import './dns-trap-log/dns-trap-log';
import './card-slotting/card-slotting';


import {RouteConfig, Component} from '../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor', {
    url: '/editor',
    template: '<ui-view/>'
})
@Component({
    selector: 'page-editor'
})
//end-non-standard
class PageEditor {
    constructor() {
        console.log('page editore loaded');
    }
}

export default PageEditor;
