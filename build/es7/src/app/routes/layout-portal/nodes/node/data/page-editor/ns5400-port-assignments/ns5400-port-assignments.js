'use strict';

import './ns5400-port-assignments-add';
import './ns5400-port-assignments-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.ns5400-port-assignments', {
    url: '/ns5400-port-assignments',
    template: '<ui-view/>'
})
@Component({
    selector: 'ns5400-port-assignments-editor'
})
//end-non-standard
class Ns5400PortAssignmentsEditor {
    constructor() {
    }
}

export default Ns5400PortAssignmentsEditor;
