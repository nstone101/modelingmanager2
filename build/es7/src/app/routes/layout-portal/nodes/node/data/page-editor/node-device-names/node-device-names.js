'use strict';

import './node-device-names-add';
import './node-device-names-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.node-device-names', {
    url: '/node-device-names',
    template: '<ui-view/>'
})
@Component({
    selector: 'node-device-names-editor'
})
//end-non-standard
class NodeDeviceNamesEditor {
    constructor() {
    }
}

export default NodeDeviceNamesEditor;
