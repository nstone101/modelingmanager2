'use strict';

import template from './uploadDoc.html!text';
import {RouteConfig, Inject} from '../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.uploadDoc', {
    url: '/docs/uploadDoc',
    params: {docConfig: null},
    onEnter: ['$stateParams', '$uibModal', 'ModalService', ($stateParams, $modal, ModalService) => {

        $modal.open({
            template: template,
            controller: UploadDoc,
            controllerAs: 'vm',
            size: 'md'
        }).result.finally(ModalService.onFinal('portal.nodes.node', {nodeName: $stateParams.nodeName}));
    }]
})
@Inject('$rootScope', '$scope', '$state', '$stateParams', '$uibModalInstance')
//end-non-standard
class UploadDoc {
    constructor($rootScope, $scope, $state, $stateParams, $modalInstance) {
        this.modal = $modalInstance;
        if($stateParams.docConfig === undefined){
            this.modal.dismiss('cancel');
        }
        this.router = $state;
        this.result = null;
        this.node = $rootScope.node;
        this.docConfig = $stateParams.docConfig;

        console.log(this.node);

        this.dropzoneApi = {};

        this.dzOptions = {
            url: window.PORTAL_URLS.BASE + '/api/nodes/docs/upload',
            onRegisterApi: (api) => {
                this.dropzoneApi = api;
                this.itemsInQueue = api.itemsInQueue;
            }
        };

        $rootScope.$bus.subscribe('DZ_ADDED_FILE', (params) => {this.itemsInQueue = params.itemsInQueue;});
        $rootScope.$bus.subscribe('DZ_REMOVED_FILE', (params) => {this.itemsInQueue = params.itemsInQueue;});
        $rootScope.$bus.subscribe('DZ_QUEUE_COMPLETE', (params) => {this.itemsInQueue = params.itemsInQueue;});

        window.addEventListener('dragover',function(e){
            e = e || event;
            console.log(e);
            console.log(e.target);
            console.log(e.target.parentNode.id);
            e.preventDefault();

        },false);

        window.addEventListener('drop',function(e){
            e = e || event;
            e.preventDefault();
        },false);

    }


    cancel() {
        this.modal.dismiss('cancel');
    }

    processDropzone() {
        this.dropzoneApi.processDropzone();
    }

    resetDropzone(){
        this.dropzoneApi.resetDropzone();
    }
}

export default UploadDoc;
