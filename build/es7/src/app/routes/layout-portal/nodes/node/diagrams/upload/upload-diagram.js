'use strict';

import template from './upload-diagram.html!text';
import {RouteConfig, Inject} from '../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.uploadDiagram', {
    url: '/diagram/uploadDiagram',
    onEnter: ['$stateParams', '$uibModal', 'ModalService', ($stateParams, $modal, ModalService) => {
        $modal.open({
            template: template,
            controller: UploadDiagram,
            controllerAs: 'vm',
            size: 'md'
        }).result.finally(ModalService.onFinal('portal.nodes.node', {nodeName: $stateParams.nodeName}));
    }]
})
@Inject('$rootScope', '$scope', '$state', '$stateParams', '$uibModalInstance')
//end-non-standard
class UploadDiagram {
    constructor($rootScope, $scope, $state, $stateParams, $modalInstance) {
        this.modal = $modalInstance;
        this.router = $state;
        this.result = null;
        this.node = $rootScope.node;
        this.user = $rootScope.currentUser;
        this.dropzoneApi = {};

        this.dzOptions = {
            maxFiles: 1,
            parallelUploads: 1,
            autoProcessQueue: false,
            uploadMultiple: false,
            url: window.PORTAL_URLS.BASE + '/api/nodes/diagrams/upload',
            onRegisterApi: (api) => {
                this.dropzoneApi = api;
                this.itemsInQueue = api.itemsInQueue;
            },
            accept: function(file, done) {
                var re = /(?:\.([^.]+))?$/;
                var ext = re.exec(file.name)[1];
                ext = ext.toUpperCase();
                if ( ext === 'JPEG' || ext === 'GIF' ||  ext === 'JPG' ||  ext === 'PNG')
                {
                    done();
                }
                else { done('Please select jpeg or gif or png files.'); }
            }
        };

        $rootScope.$bus.subscribe('DZ_ADDED_FILE', (params) => {this.itemsInQueue = params.itemsInQueue;});
        $rootScope.$bus.subscribe('DZ_REMOVED_FILE', (params) => {this.itemsInQueue = params.itemsInQueue;});
        $rootScope.$bus.subscribe('DZ_QUEUE_COMPLETE', (params) => {this.itemsInQueue = params.itemsInQueue;});

        window.addEventListener('dragover',function(e){
            e = e || event;
            e.preventDefault();

        },false);

        window.addEventListener('drop',function(e){
            e = e || event;
            e.preventDefault();
        },false);
    }

    cancel() {
        this.modal.dismiss('cancel');
    }

    processDropzone() {
        this.dropzoneApi.processDropzone();
    }

    resetDropzone(){
        this.dropzoneApi.resetDropzone();
    }
}

export default UploadDiagram;
