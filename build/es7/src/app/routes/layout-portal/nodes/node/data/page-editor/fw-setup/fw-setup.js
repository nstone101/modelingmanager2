'use strict';

import './fw-setup-add';
import './fw-setup-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.fw-setup', {
    url: '/fw-setup',
    template: '<ui-view/>'
})
@Component({
    selector: 'fw-setup-editor'
})
//end-non-standard
class FwSetupEditor {
    constructor() {
    }
}

export default FwSetupEditor;
