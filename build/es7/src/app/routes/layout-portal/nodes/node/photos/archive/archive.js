'use strict';

import template from './archive.html!text';
import {RouteConfig, Inject} from '../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.photoArchive', {
    url: '/photos/photoArchive',
    onEnter: ['$stateParams', '$uibModal', 'ModalService', ($stateParams, $modal, ModalService) => {
        $modal.open({
            template: template,
            controller: PhotoArchive,
            controllerAs: 'vm',
            size: 'lg'
        }).result.finally(ModalService.onFinal('portal.nodes.node', {nodeName: $stateParams.nodeName}));
    }]
})
@Inject('$rootScope', '$scope', '$state', '$interval', '$stateParams', '$uibModalInstance', 'NodeResource')
//end-non-standard
class PhotoArchive {
    constructor($rootScope, $scope, $state, $interval, $stateParams, $modalInstance, NodeResource) {
        this.modal = $modalInstance;
        this.router = $state;
        this.result = null;
        this.node = $rootScope.node;
        this.progressValue = 10;
/*
        NodeResource.getPhotoArchive()
            .then((resp) => {
                
            });*/
    }

    cancel() {
        this.modal.dismiss('cancel');
    }

    processDropzone() {
        this.dropzoneApi.processDropzone();
    }

    resetDropzone(){
        this.dropzoneApi.resetDropzone();
    }
}

export default PhotoArchive;
