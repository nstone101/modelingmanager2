'use strict';

import './infoForm/infoForm';
import './vsns/vsns';
import './vsns/add/add';

import template from './info.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../../decorators'; // jshint unused: false
const nodeStates = [{label: 'Node Info', value: 'NodeInfo'}, {label: 'Revision', value: 'Revision'}];

//start-non-standard
@Component({
    selector: 'node-info'
})
@View({
    template: template,
    replace: true
})
@Inject('$rootScope', '$timeout')
//end-non-standard
class Info {
    constructor($rootScope, $timeout) {
        this.node = $rootScope.node;
        this.nodeStates = nodeStates;
        this.nodeState = nodeStates.NodeInfo;
        this.$timeout = $timeout;
    }
}

export default Info;