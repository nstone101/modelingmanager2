'use strict';

import './network-connections-add';
import './network-connections-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.network-connections', {
    url: '/network-connections',
    template: '<ui-view/>'
})
@Component({
    selector: 'network-connections-editor'
})
//end-non-standard
class NetworkConnectionsEditor {
    constructor() {
    }
}

export default NetworkConnectionsEditor;
