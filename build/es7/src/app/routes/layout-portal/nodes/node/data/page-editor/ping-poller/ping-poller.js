'use strict';

import './ping-poller-add';
import './ping-poller-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.ping-poller', {
    url: '/ping-poller',
    template: '<ui-view/>'
})
@Component({
    selector: 'ping-poller-editor'
})
//end-non-standard
class PingPollerEditor {
    constructor() {
    }
}

export default PingPollerEditor;
