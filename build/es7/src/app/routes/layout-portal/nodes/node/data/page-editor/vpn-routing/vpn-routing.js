'use strict';

import './vpn-routing-add';
import './vpn-routing-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.vpn-routing', {
    url: '/vpn-routing',
    template: '<ui-view/>'
})
@Component({
    selector: 'vpn-routing-editor'
})
//end-non-standard
class VpnRoutingEditor {
    constructor() {
    }
}

export default VpnRoutingEditor;
