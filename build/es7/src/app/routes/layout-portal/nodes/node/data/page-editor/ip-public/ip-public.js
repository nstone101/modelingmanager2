'use strict';

import './ip-public-add';
import './ip-public-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.ip-public', {
    url: '/ip-public',
    template: '<ui-view/>'
})
@Component({
    selector: 'ip-public-editor'
})
//end-non-standard
class IpPublicEditor {
    constructor() {
    }
}

export default IpPublicEditor;
