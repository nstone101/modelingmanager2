'use strict';

import './fw-golden-config-add';
import './fw-golden-config-edit';

import {RouteConfig, Component}  from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.nodes.node.page-editor.fw-golden-config', {
    url: '/fw-golden-config',
    template: '<ui-view/>'
})
@Component({
    selector: 'fw-golden-config-editor'
})
//end-non-standard
class FwGoldenConfigEditor {
    constructor() {
    }
}

export default FwGoldenConfigEditor;
