'use strict';

import template from './browser.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../decorators'; // jshint unused: false

window._collapse = {};

//start-non-standard
@RouteConfig('portal.nodes.browser', {
    url: '/browser/:filter',
    template: template,
    resolve: {
        nodes: ($stateParams, NodeResource) => { return NodeResource.getNodeBrowser($stateParams.filter)}
    }
})
@Component({
    selector: 'nodes-browser'
})
@Inject('$timeout', 'nodes')
//end-non-standard
class Browser {
    constructor($timeout, nodes) {
        this.collapse = {};
        this.browser = [];

        this.sortableOptions = {
            containment: '#sortable-container'
        };

        _.map(nodes.data, (item, key)=>{
            if(item === 'all') { return; }
            this.browser.push(item);
            this.collapse[key] = true;
        });

        this.$timeout = $timeout;
    }
}

export default Browser;