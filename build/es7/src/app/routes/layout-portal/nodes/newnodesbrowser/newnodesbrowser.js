'use strict';

import template from './newnodesbrowser.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../decorators'; // jshint unused: false

window._collapse = {};

//start-non-standard
@RouteConfig('portal.nodes.newnodesbrowser', {
    url: '/newnodesbrowser/:filter',
    template: template,
    resolve: {
        nodes: ($stateParams, NodeResource) => { return NodeResource.getNewNodesBrowser($stateParams.filter)}
    }
})
@Component({
    selector: 'nodes-newnodesbrowser'
})
@Inject('$state', '$timeout', 'nodes', 'NodeResource')
//end-non-standard
class Browser {
    constructor($state, $timeout, nodes, NodeResource) {
        this.collapse = {};
        this.browser = [];
        this.NodeResource = NodeResource;
        this.$state = $state;


        this.sortableOptions = {
            containment: '#sortable-container'
        };

        _.map(nodes.data, (item, key)=>{
            if(item === 'all') { return; }
            this.browser.push(item);
            this.collapse[key] = true;
        });

        this.$timeout = $timeout;
    }
    createNewNode(wizr) {

        this.NodeResource.createNewNode({
               name: wizr.name,
               type: wizr.type
            }).then((resp) => {

                this.$state.go(this.$state.current, {}, {reload: true});

                console.log('got here, made a call, got this back:' + resp);
            });
    }

}

export default Browser;
