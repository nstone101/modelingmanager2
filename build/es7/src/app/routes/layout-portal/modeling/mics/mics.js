'use strict';


import template from './mics.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../decorators'; // jshint unused: false

window._collapse = {};

//start-non-standard
@RouteConfig('portal.modeling.mics', {
    url: '/mics/:filter',
    template: template
})
@Component({
    selector: 'mics-base'
})
@Inject('$rootScope', '$state', '$timeout', 'ModelingResource')
//end-non-standard
class MICsBrowser {
    constructor($rootScope, $state, $timeout, ModelingResource) {
        this.collapse = {};
        this.browser = [];
        this.ModelingResource = ModelingResource; 
        this.$state = $state;
        this.rootScope = $rootScope; //
        
  /*this.ModelingResource.getMICCards({
  }).this((resp) => {
    this.$state.go(this.$state.current, {}, {reload: true});
    console.log('MODELING - MICData has got here, made a call and got this back:' + angular.toJson(resp));
    console.log('Modeling - MICDATA : ' + angular.toJson(resp.data));
  });*/


  
//$rootScope.gridOptionsMIC.data = resp.data;//
//        console.log('resp made it to MIC past model');

  //DELETE ROW 
  $rootScope.deleteRow = function(row) {
    let index = $rootScope.gridOptionsMIC.data.indexOf(row.entity);
    $rootScope.gridOptionsMIC.data.splice(index, 1);
  };

  //ADD ROW + DEFAULT MODEL DATA
  $rootScope.addData = function() {
    let n = $rootScope.gridOptionsMIC.data.length + 1;
    $rootScope.gridOptionsMIC.data.push({
                'Size': 'New ' + n,
                'ICard': 'ICard' + n,
                'IType': 'abc',
                'Purpose': 'abc',
                'UpdatedTime': '',
                'EditedBy': '',
                'Actions': '' 
              });
  };

//getMicsDataIds(id){
//$http.get('api/modeling/{this.route}/:id')
//}; 


//resp not defined
//$rootScope.gridOptionsMIC.data = resp.data;
//.log('resp');


this.ModelingResource.getMICCards({
}).this((resp) => {$state.go($state.current, {}, {reload: true});
        console.log('MODELING - MICData has got here, made a call and got this back:' + angular.toJson(resp));
        console.log('Modeling - MICDATA : ' + angular.toJson(resp.data));
  });


$rootScope.gridOptionsMIC = {
        enableFiltering: true,
        showGridFooter: true,
        showColumnFooter: true,
        exporterMenuCsv: true,
        enableGridMenu: true,
        columnDefs: [
          { name: 'id', enableHiding:true, visible:false },
          { name: 'Size', width: 100 },
          { name: 'MICCard', width: 450},
          { name: 'MICType', width: 100 },
          { name: 'Purpose', enableHiding:true, visible:false },
          { name: 'UpdatedTime', enableHiding: true, visible:false },
          { name: 'EditedBy', enableHiding: true, visible:false },
          { name: 'Comment', visible:false },
          { name: 'Data1', enableHiding: true, visible: false },
          { name: 'Data2', enableHiding: true, visible: false },
          { name: 'Data3', enableHiding: true, visible: false },
          //Details and Row Delete
          { name: 'Actions',
             cellTemplate:'<button class="btn primary" style="margin:3px;" ng-click="grid.appScope.showMe()"><i class="fa fa-eye" style="color:black;"></i></button><button class="btn btn-danger" style="margin:3px;" ng-click="grid.appScope.deleteRow(row)"><i class="fa fa-trash"></i></button>'}
             //{ name: 'UpdatedTime', width: 500 }
           /*cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
              if (grid.getCellValue(row,col) === 'xe') {
                return 'blue';
              } else if (grid.getCellValue(row,col) === 'ge') {
                return 'pink';
              } else if (grid.getCellValue(row,col) === 'pa') {
                return 'white';
            }
          },*/
        ]
      
        //getMICColumnData{
        //$http.get(/api/{this.base}/{this.route}/data) };
        /*mic1data: [
          {
         //'id': 212,
         'Size': '8',
         'ICard': 'Port 10G Interface Card ( MPC2 + 2x 4x 10G MIC)',
         'IType': 'xe'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:18:47'
         //'EditedBy': null,
         //'Comment': null
         },
         {
         //'id': 214,
         'Size': '40',
         'ICard': 'Port 100m/1G baseT ( MPC1 + 1x 40x 1G MIC )',
         'IType': 'ge'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:18:41'
         //'EditedBy': null,
         //'Comment': null
         },
         {
         //'id': 213,
         'Size': '4',
         'ICard': 'Port 10G Interface Card ( MPC2 + 1x 4x 10G MIC)',
         'IType': 'xe'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:18:56'
         //'EditedBy': null,
         //'Comment': null
         },
         {
         //'id': 215,
         'Size': '20',
         'ICard': 'Port 1G baseT ( SRX5K-MPC + 1x SRX-MIC-20GE-SFP )',
         'IType': 'ge'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:19:01'
         //'EditedBy': null,
         //'Comment': null
         },
         {
         //'id': 216,
         'Size': '20',
         'ICard': 'Port IXIA Anue NTO 5293',
         'IType': 'pa'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:19:10'
         //'EditedBy': null,
         //'Comment': null
         }
        ]*/
      };



    }




}

export default MICsBrowser;