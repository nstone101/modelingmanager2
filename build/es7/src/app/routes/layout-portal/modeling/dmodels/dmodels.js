'use strict';

import './mic_card/mic_card';

import template from './dmodels.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../decorators'; // jshint unused: false

window._collapse = {};

//start-non-standard
@RouteConfig('portal.modeling.dmodels', {
    url: '/dmodels/:filter',
    template: template
})
@Component({
    selector: 'dmodels'
})
@Inject('$rootScope', '$state', '$timeout', 'ModelingResource', '$scope')
//end-non-standard
class DModels {
    constructor($rootScope, $state, $timeout, $scope) {
            this.collapse = {};
            this.browser = [];
            //$rootScope.dmodels = this.dmodels = dmodels.data;
            this.$state = $state;
            this.$scope = $scope;
            this.rootScope = $rootScope;

  $rootScope.deleteRow = function(row) {
    let index = $rootScope.gridOptionsComplex.data.indexOf(row.entity);
    $rootScope.gridOptionsComplex.data.splice(index, 1);
  };
  $rootScope.addData = function() {
    let n = $rootScope.gridOptionsComplex.data.length + 1;
    $rootScope.gridOptionsComplex.data.push({
                'Size': 'New ' + n,
                'ICard': 'ICard' + n,
                'IType': 'abc',
                'Purpose': 'abc',
                'UpdatedTime': '',
                'EditedBy': '',
                'OptionBin': '' 
              });
  }; 


//Going to be put into core/models/dmodelsModel as this.columns = [ //define column fields in here ]
$rootScope.gridOptionsComplex = {
        enableFiltering: true,
        showGridFooter: true,
        showColumnFooter: true,
        exporterMenuCsv: true,
        enableGridMenu: true,
        columnDefs: [
          { name: 'id', width: 100, enableHiding:true, visible:false },
          { name: 'Size', width: 250 },
          { name: 'ICard' },
          { name: 'IType' },
          { name: 'Purpose', enableHiding: true, visible:false },
          { name: 'UpdatedTime', enableHiding: true, visible:false },
          { name: 'EditedBy', enableHiding: true, visible: false },
          { name: 'OptionBin', width: 100, enableCellEdit: false,
             cellTemplate:'<button class="btn primary" style="margin:3px;" ng-click="grid.appScope.showMe()"><i class="fa fa-eye" style="color:black;"></i></button><button class="btn btn-danger" style="margin:3px;" ng-click="grid.appScope.deleteRow(row)"><i class="fa fa-trash"></i></button>'}
             //{ name: 'UpdatedTime', width: 500 }
           /*cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
              if (grid.getCellValue(row,col) === 'xe') {
                return 'blue';
              } else if (grid.getCellValue(row,col) === 'ge') {
                return 'pink';
              } else if (grid.getCellValue(row,col) === 'pa') {
                return 'white';
            }
          },*/
        ],
        //getMicData(){};
        //$http.get(/api/{this.base}/{this.route}/data) };
        data: [
          {
         'id': 212,
         'Size': 8,
         'ICard': '8 Port 10G Interface Card ( MPC2 + 2x 4x 10G MIC)',
         'IType': 'xe',
         'Purpose': '',
         'UpdatedTime': '2016-02-13 05:18:47',
         'EditedBy': null
         //'Comment': null
         },
         {
         'id': 214,
         'Size': 40,
         'ICard': '40 Port 100m/1G baseT ( MPC1 + 1x 40x 1G MIC )',
         'IType': 'ge',
         'Purpose': '',
         'UpdatedTime': '2016-02-13 05:18:41',
         'EditedBy': null
         //'Comment': null
         },
         {
         'id': 213,
         'Size': 4,
         'ICard': '4 Port 10G Interface Card ( MPC2 + 1x 4x 10G MIC)',
         'IType': 'xe',
         'Purpose': '',
         'UpdatedTime': '2016-02-13 05:18:56',
         'EditedBy': null
         //'Comment': null
         },
         {
         'id': 215,
         'Size': 20,
         'ICard': '20 Port 1G baseT ( SRX5K-MPC + 1x SRX-MIC-20GE-SFP )',
         'IType': 'ge',
         'Purpose': '',
         'UpdatedTime': '2016-02-13 05:19:01',
         'EditedBy': null
         //'Comment': null
         },
         {
         'id': 216,
         'Size': 20,
         'ICard': '20 Port IXIA Anue NTO 5293',
         'IType': 'pa',
         'Purpose': '',
         'UpdatedTime': '2016-02-13 05:19:10',
         'EditedBy': null
         //'Comment': null
         }
        ]

      };
    }

}

export default DModels;
