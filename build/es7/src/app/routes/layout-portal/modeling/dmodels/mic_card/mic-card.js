'use strict';

import template from './mic_card.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../decorators'; // jshint unused: false

window._collapse = {};

//start-non-standard
@RouteConfig('portal.modeling.dmodels/mic_card', {
    url: '/mic-card/:filter',
    template: template,
    replace: true
})
@Component({
    selector: 'mic-card'
})
@Inject('$rootScope', '$state', '$timeout')
//end-non-standard
class MICBrowser {
    constructor($rootScope, $state, $timeout, ModelingResource) {
        this.collapse = {};
        this.browser = []; //
        this.$state = $state;
        this.rootScope = $rootScope; //
        this.ModelingResource = ModelingResource; //

//Going to core/models/modeling/**dirname**
$rootScope.gridOptions5 = {
        enableFiltering: true,
        showGridFooter: true,
        showColumnFooter: true,
        columnDefs: [
          { name: 'Size', width: 100 },
          { name: 'ICard', width: 450},
          { name: 'IType', width: 100 }
          //{ name: 'UpdatedTime', width: 500 }
           /*cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
              if (grid.getCellValue(row,col) === 'xe') {
                return 'blue';
              } else if (grid.getCellValue(row,col) === 'ge') {
                return 'pink';
              } else if (grid.getCellValue(row,col) === 'pa') {
                return 'white';
            }
          },*/
        ],
        data: [
          {
         //'id': 212,
         'Size': '8',
         'ICard': 'Port 10G Interface Card ( MPC2 + 2x 4x 10G MIC)',
         'IType': 'xe'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:18:47'
         //'EditedBy': null,
         //'Comment': null*/
         },
         {
         //'id': 214,
         'Size': '40',
         'ICard': 'Port 100m/1G baseT ( MPC1 + 1x 40x 1G MIC )',
         'IType': 'ge'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:18:41'
         //'EditedBy': null,
         //'Comment': null
         },
         {
         //'id': 213,
         'Size': '4',
         'ICard': 'Port 10G Interface Card ( MPC2 + 1x 4x 10G MIC)',
         'IType': 'xe'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:18:56'
         //'EditedBy': null,
         //'Comment': null
         },
         {
         //'id': 215,
         'Size': '20',
         'ICard': 'Port 1G baseT ( SRX5K-MPC + 1x SRX-MIC-20GE-SFP )',
         'IType': 'ge'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:19:01'
         //'EditedBy': null,
         //'Comment': null
         },
         {
         //'id': 216,
         'Size': '20',
         'ICard': 'Port IXIA Anue NTO 5293',
         'IType': 'pa'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:19:10'
         //'EditedBy': null,
         //'Comment': null
         },
        ]
      };



    }

}

export default MICBrowser;
