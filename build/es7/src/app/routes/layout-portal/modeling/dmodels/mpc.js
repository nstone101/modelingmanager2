'use strict';

//import './mic_card/mic_card';

import template from './mpc.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../decorators'; // jshint unused: false

window._collapse = {};

//start-non-standard
@RouteConfig('portal.modeling.dmodels', {
    url: '/mpc/:filter',
    template: template
})
@Component({
    selector: 'mpc-selector'
})
@Inject('$rootScope', '$state', '$timeout', '$scope')
//end-non-standard
class Mpc {
    constructor($rootScope, $state, $timeout, $scope) {
        this.collapse = {};
        this.mpc = [];
        this.$state = $state;
        this.$rootScope = $rootScope;
        //this.$rootScope = $rootScope.mpc = mpc.data; 

        $scope.deleteRow = function(row) {
        let index = $rootScope.gridOptionsMPC.data2.indexOf(row.entity);
        $rootScope.gridOptionsMPC.data.splice(index, 1);
        };
        $rootScope.addData = function() {
        let n = $rootScope.gridOptionsMPC.data2.length + 1;
        $rootScope.gridOptionsMPC.data.push({
                'Size': 'New ' + n,
                'ICard': 'ICard' + n,
                'IType': 'abc',
                'Purpose': 'abc',
                'UpdatedTime': '',
                'EditedBy': '',
                'ShowScope': '' 
              });
        }; 


        $rootScope.gridOptionsMPC = {
                enableFiltering: true,
                showGridFooter: true,
                showColumnFooter: true,
                exporterMenuCsv: true,
                enableGridMenu: true,
                columnDefs: [
                    { name: 'id', enableHiding:true, visible: false },
                    { name: 'MPCCard', enableHiding:true, visible:false },
                    { name: 'MPCType', enableHiding:true, visible:false },
                    { name: 'Size', width: 100 },
                    //{ name: 'Size'},
                    { name: 'MICCard1' },
                    { name: 'MICCard2'},
                    { name: 'Purpose', enableHiding:true, visible: false },
                    { name: 'UpdatedTime', enableHiding: true, visible: false },
                    { name: 'EditedBy', enableHiding: true, visible: false },
                    //{ name: 'Comment' },
                    //{ name: 'Data1'},
                    //{ name: 'Data2'},
                    //{ name: 'Data3'},
                    { name: 'Actions',
                        cellTemplate:'<button class="btn primary" style="margin:3px;" ng-click="grid.appScope.showMe()"><i class="fa fa-eye" style="color:black;"></i></button><button class="btn btn-danger" style="margin:3px;" ng-click="grid.appScope.deleteRow(row)"><i class="fa fa-trash"></i></button>'},
            
                    { name: 'ICard', width: 450},
                    { name: 'IType', width: 100 }
           /*cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
              if (grid.getCellValue(row,col) === 'xe') {
                return 'blue';
              } else if (grid.getCellValue(row,col) === 'ge') {
                return 'pink';
              } else if (grid.getCellValue(row,col) === 'pa') {
                return 'white';
            }
          },*/
        ],
        //getMICColumnData{
        //$http.get(/api/{this.base}/{this.route}/data) };
        data: [
          {
         //'id': 212,
         'Size': '8',
         'ICard': '8 Port 10G Interface Card ( MPC2 + 2x 4x 10G MIC)',
         'IType': 'xe'
         //'NoCardSlots': 1,
         //'CardSizePer': 4
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:18:47'
         //'EditedBy': null,
         //'Comment': null*/
         },
         {
         //'id': 214,
         'Size': '40',
         'ICard': '40 Port 100m/1G baseT ( MPC1 + 1x 40x 1G MIC )',
         'IType': 'ge'
         //'NoCardSlots': 1,
         //'CardSizePer': 40

         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:18:41'
         //'EditedBy': null,
         //'Comment': null
         },
         {
         //'id': 213,
         'Size': '4',
         'ICard': '4 Port 10G Interface Card ( MPC2 + 1x 4x 10G MIC)',
         'IType': 'xe'
         //'NoCardSlots': 2,
         //'CardSizePer': 4
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:18:56'
         //'EditedBy': null,
         //'Comment': null
         },
         {
         //'id': 215,
         'Size': '20',
         'ICard': '20 Port 1G baseT ( SRX5K-MPC + 1x SRX-MIC-20GE-SFP )',
         'IType': 'ge'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:19:01'
         //'EditedBy': null,
         //'Comment': null
         },
         {
         //'id': 216,
         'Size': '20',
         'ICard': 'Port IXIA Anue NTO 5293',
         'IType': 'pa'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:19:10'
         //'EditedBy': null,
         //'Comment': null
         }
        ]
    };

    /*data2:[
        {
        'id': 1,
        'MPCCard': 'MPC2-2x4',
        'MPCType': 'xe',
        'Size': '8',
        'MICCard1': 2,
        'MICCard2': 2,
        'Purpose': '',
        'UpdatedTime': '2016-02-17 08:40:10',
        'EditedBy': null,
        'Comment': null,
        'Data1': null,
        'Data2': null,
        'Data3': null
        }       
    ]*/
 }

}

export default Mpc;