'use strict';

import template from './mic_card.html!text';
import {RouteConfig, Component, View, Inject} from '../../../../../decorators'; // jshint unused: false

window._collapse = {};

//start-non-standard
@RouteConfig('portal.modeling.dmodels.mic_card', {
    url: '/mic_card/:filter',
    template: template,
    replace: true
})
@Component({
    selector: 'mic_card-browser'
})
@Inject('$timeout', 'dmodels')
//end-non-standard
class MICBrowser {
    constructor($rootScope, $timeout, dmodels, $state) {
        this.collapse = {};
        this.micBrowser = [];
        this.$state = $state;
        this.$timeout = $timeout;

        $rootScope.gridOptions5 = {
            enableFiltering: true,
            showGridFooter: true,
            showColumnFooter: true,
            exporterMenuCsv: true,
            enableGridMenu: true,
                columnDefs: [
                    { name: 'Size', width: 100 },
                    { name: 'ICard', width: 450},
                    { name: 'IType', width: 100 },
                    { name: 'Purpose', enableHiding:true },
                    { name: 'UpdatedTime', enableHiding: true },
                    { name: 'EditedBy', enableHiding: true },
                    { name: 'ShowScope',
                        cellTemplate:'<button class="btn primary" style="margin:3px;" ng-click="grid.appScope.showMe()"><i class="fa fa-eye" style="color:black;"></i></button><button class="btn btn-danger" style="margin:3px;" ng-click="grid.appScope.deleteRow(row)"><i class="fa fa-trash"></i></button>'}
            ],
        data: [
          {
         //'id': 212,
         'Size': '8',
         'ICard': 'Port 10G Interface Card ( MPC2 + 2x 4x 10G MIC)',
         'IType': 'xe'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:18:47'
         //'EditedBy': null,
         //'Comment': null*/
         },
         {
         //'id': 214,
         'Size': '40',
         'ICard': 'Port 100m/1G baseT ( MPC1 + 1x 40x 1G MIC )',
         'IType': 'ge'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:18:41'
         //'EditedBy': null,
         //'Comment': null
         },
         {
         //'id': 213,
         'Size': '4',
         'ICard': 'Port 10G Interface Card ( MPC2 + 1x 4x 10G MIC)',
         'IType': 'xe'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:18:56'
         //'EditedBy': null,
         //'Comment': null
         },
         {
         //'id': 215,
         'Size': '20',
         'ICard': 'Port 1G baseT ( SRX5K-MPC + 1x SRX-MIC-20GE-SFP )',
         'IType': 'ge'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:19:01'
         //'EditedBy': null,
         //'Comment': null
         },
         {
         //'id': 216,
         'Size': '20',
         'ICard': 'Port IXIA Anue NTO 5293',
         'IType': 'pa'
         //'Purpose': '',
         //'UpdatedTime': '2016-02-13 05:19:10'
         //'EditedBy': null,
         //'Comment': null
         },
        ]
      };



    }

}

export default MICBrowser;
