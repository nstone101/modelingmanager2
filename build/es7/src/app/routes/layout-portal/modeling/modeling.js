'use strict';

import './dmodels/dmodels';
import './mics/mics';
import './mpcs/mpcs';
//import './models/Model_MicModel';

import {RouteConfig, Inject} from '../../../decorators';  // jshint unused: false

//start-non-standard
@RouteConfig('portal.modeling', {
    url: '/modeling',
    abstract: true,
    template: '<ui-view/>'
})
@Inject()
//end-non-standard
class Modeling { }
