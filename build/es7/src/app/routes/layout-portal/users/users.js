'use strict';
import './user/user';
import './userProfile/userProfile';
import './changePassword/changePassword';


import {RouteConfig, Inject} from '../../../decorators';  // jshint unused: false

//start-non-standard
@RouteConfig('portal.users', {
    url: '/users',
    abstract: true,
    template: '<ui-view/>'
})
@Inject()
//end-non-standard
class Users {
    constructor() {
        console.log('users loaded');
    }
}

export default Users;
