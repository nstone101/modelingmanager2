'use strict';

import template from './userProfile.html!text';
import {RouteConfig, Component, Inject} from '../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.users.userProfile', {
    url: 'userProfile',
    onEnter: ['$uibModal', 'ModalService', ($modal, ModalService) => {
        $modal.open({
            template: template,
            controller: UserProfile,
            controllerAs: 'vm',
            size: 'about'
        })
.result.finally(ModalService.onFinal('portal.nodes.node', {nodeName: $stateParams.nodeName}));

    }]
})
@Inject('$state', '$rootScope', '$uibModalInstance', 'UserResource')
//end-non-standard
class UserProfile {
    constructor($state, $rootScope, $modalInstance, UserResource) {
        this.modal = $modalInstance;
        this.router = $state;
        this.user = $rootScope.currentUser;
        this.UserResource = UserResource;

    }

    updateUserProfile(user) {

           this.UserResource.updateUserProfile({
                  id        : user.id,
                  firstname : user.firstname,
                  lastname  : user.lastname,
                  email     : user.email,
                  workphone : user.workphone,
                  cellphone : user.cellphone,
                  addr1     : user.Address1,
                  addr2     : user.Address2,
                  city      : user.City,
                  state     : user.State,
                  zip       : user.Zip
            }).then((resp) => {
                this.modal.dismiss('cancel');
            });
 

    }

    cancel() {
        this.modal.dismiss('cancel');
    }
}

export default UserProfile;
