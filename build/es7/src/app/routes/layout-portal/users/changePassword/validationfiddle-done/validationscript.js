'use strict'; 

(function() {

  var app = angular.module('validation', ['ngMessages']);

  var ChangePasswordController = function() {
    var model = this;

    model.message = '';

    model.user = {
      
      currentPassword: '',
      newPassword: '',
      confirmPassword: ''
    };

    model.submit = function(isValid) {
      console.log('h');
      if (isValid) {
        model.message = 'Submitted ' + model.user.currentPassword;
      } else {
        model.message = 'There are Password fields that do not match or do not meet criteria.';
      }
    };

  };

  var compareTo = function() {
    return {
      require: 'ngModel',
      scope: {
        otherModelValue: '=compareTo'
      },
      link: function(scope, element, attributes, ngModel) {

        ngModel.$validators.compareTo = function(modelValue) {
          return modelValue === scope.otherModelValue;
        };

        scope.$watch('otherModelValue', function() {
          ngModel.$validate();
        });
      }
    };
  };

  app.directive('compareTo', compareTo);
  app.controller('ChangePasswordController', ChangePasswordController);

}());