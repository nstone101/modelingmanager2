'use strict';

import template from './changePassword.html!text';
import {RouteConfig, Component, Inject} from '../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.users.changePassword', {
    url: 'changePassword',
    onEnter: ['$modal', 'ModalService', ($modal, ModalService) => {
        $modal.open({
            template: template,
            controller: ChangePassword,
            controllerAs: 'vm',
            size: 'about'
        })
.result.finally(ModalService.onFinal('portal.nodes.node', {nodeName: $stateParams.nodeName}));

    }]
})
@Inject('$state', '$rootScope', '$modalInstance', 'UserResource')
//end-non-standard
class ChangePassword {
    constructor($state, $rootScope, $modalInstance, UserResource) {
        this.modal = $modalInstance;
        this.router = $state;
        this.user = $rootScope.currentUser;
        this.UserResource = UserResource;

    }

    updateChangePassword(user) {

           this.UserResource.updateUserPassword({
                  id : user.id,
                  currentPassword : user.password,
                  updatedPassword  : user.newPassword,
                  confirmPassword : user.confirmPassword,
            }).then((resp) => {

                //this.modal.dismiss('cancel');
              // if(resp) { 500 response, set form message to updatepassword failed please updatepasswrd_msg, else if resp 200 then dismiss^ },

           });


    }
    newPwdConfirmd(user){
        //twoStrings value = to t or f ,  enabler button 'grey' disabled, false, returns false

    }
    login(user) {


    }


    cancel() {
        this.modal.dismiss('cancel');
    }
}

export default ChangePassword;
