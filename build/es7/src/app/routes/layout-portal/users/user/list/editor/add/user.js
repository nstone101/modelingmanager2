'use strict';

import template from './user.html!text';
import {RouteConfig, Inject} from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.users.user.user-editor.add', {
    url: '/add',
    onEnter: ['$uibModal', 'ModalService', ($modal, ModalService) => {
        $modal.open({
            template: template,
            resolve: {
                init: ['UserModel', 'GroupModel', 'RoleModel', (UserModel, GroupModel, RoleModel) => Promise.all([UserModel.initItem(), GroupModel.initCollection(), RoleModel.initCollection()])]
            },
            controller: UserAddEditor,
            controllerAs: 'vm',
            size: 'lg'
        }).result.finally(ModalService.onFinal('portal.users.user.list',{}, {notify:false}));
    }]
})
@Inject('$rootScope', '$state', 'UserModel', 'GroupModel', 'RoleModel', 'FormService', '$uibModalInstance')
//end-non-standard
class UserAddEditor {
    constructor($rootScope, $state, UserModel, GroupModel, RoleModel, FormService, $modalInstance) {
        console.log('user add editor');
        this.result = null;
        this.isSubmitting = null;
 
        this.rootScope = $rootScope;

        this.modal = $modalInstance;
        this.Model = UserModel;
        this.groups = GroupModel.getCollection();
        this.roles = RoleModel.getCollection();
        this.states = ['AL','AK','AZ','CA','CO','CT','DE','DC','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY'];

        this.user = $rootScope.currentUser;
        this.entry = this.Model.getItem();
        this.router = $state;

        this.FormService = FormService;
        this.saveButtonOptions = Object.assign({}, FormService.getModalSaveButtonOptions()); // clone the modal save button options so we don't overwrite default one
        this.saveButtonOptions.buttonDefaultText = 'Create';
        this.saveButtonOptions.buttonSuccessText = 'Created';
        this.saveButtonOptions.buttonSubmittingText = 'Creating';
        this.saveButtonOptions.buttonInitialIcon = 'fa fa-plus';
    }

    cancel() {
        this.modal.dismiss('cancel');
    }

    save(form) {
        if(!form.$valid) {return;}
        this.isSubmitting = true;
        this.rootScope.$bus.publish('USER_DATA_UPDATED');
        this.FormService.save(this.Model, this.entry, this, form).then(()=>{this.entry = null;});
    }
}

export default UserAddEditor;
