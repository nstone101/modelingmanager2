'use strict';

import './list/list';
import './groups/groups';

import template from './user.html!text';
import {RouteConfig, Component, Inject} from '../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.users.user', {
    url: '/user',
    abstract: true,
    template: template
})
@Component({
    selector: 'user'
})
@Inject('$rootScope', 'UserModel')
//end-non-standard
class User {
    constructor($rootScope, UserModel) {
        console.log('user loaded');
        this.user = $rootScope.currentUser;
        UserModel.setItem(this.user);
        $rootScope.currentUserModel = UserModel.getItem();
        this.rootScope = $rootScope;
        this.title = 'User Management';
        this.area = 'User Management';
        $rootScope.$bus.subscribe('USER_TITLE_UPDATE', (data) => {
            this.title = data.title;
        });
    }
}

export default User;