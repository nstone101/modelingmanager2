'use strict';

import './add/user';
import './edit/user';

import {RouteConfig, Component} from '../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.users.user.user-editor', {
    url: '/user-editor',
    template: '<ui-view data-test="editor"/>'
})
@Component({
    selector: 'user-editor'
})
//end-non-standard
class UserEditor {
    constructor() {
        console.log('user editor loaded');
    }
}

export default UserEditor;