'use strict';

import template from './groups.html!text';
import {RouteConfig, Component, Inject} from '../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.users.user.groups', {
    url: '/list',
    template: template,
    resolve: {
        init: ['GroupModel', (GroupModel) => Promise.all([GroupModel.initCollection()])]
    }
})
@Component({
    selector: 'user-groups'
})
@Inject('$scope', '$rootScope', '$state', 'GroupModel')
//end-non-standard
class UserGroups {
    constructor($scope, $rootScope, $state, GroupModel) {
        this.loadingData = {};
        this.model = GroupModel;
        /* jshint ignore:start */
        this.group = {"group_name": "", "group_description": ""};
        /* jshint ignore:end */
        this.user = $rootScope.currentUser;
        this.rootScope = $rootScope;
        this.scope = $scope;
        this.router = $state;


        this.editorLink = 'portal.users.user.groups';

        this.updatePageView('Group Management');

        this.rootScope.$bus.subscribe('USER_DATA_UPDATED', () => {

        });
    }

    updatePageView(title){
        this.rootScope.$bus.publish('USER_TITLE_UPDATE', {
            title: title
        });
    }

    addGroup() {
        this.model.save(this.group).then((resp)=>{
            /* jshint ignore:start */
            this.group = {"group_name": "", "group_description": ""};
            /* jshint ignore:end */
        });
    }

    save(data){
        this.model.save(data).then((resp) => {console.log(resp);});
    }

    deleteUserTitle(data){
        /* jshint ignore:start */
        return 'Delete Group - ' + data.group_name;
        /* jshint ignore:end */
    }

    deleteUserMessage(data){
       return '<p>Are you sure you want to delete this group?</p>';
    }

    delete(data) {
        this.model.delete(data).then(()=>{

        });
    }
}

export default UserGroups;