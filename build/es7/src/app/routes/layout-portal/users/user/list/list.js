'use strict';

import './editor/editor';

import template from './list.html!text';
import {RouteConfig, Component, Inject} from '../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.users.user.list', {
    url: '/list',
    template: template,
    resolve: {
        init: ['UserModel', (UserModel) => Promise.all([UserModel.initCollection()])]
    }
})
@Component({
    selector: 'user-list'
})
@Inject('$scope', '$rootScope', '$timeout', '$window', '$state', 'GridService', 'UserModel')
//end-non-standard
class UserList {
    constructor($scope, $rootScope, $timeout, $window, $state, GridService, UserModel) {
        console.log('user list loaded');
        this.GridService = GridService;
        this.loadingData = {};
        this.model = UserModel;
        this.user = $rootScope.currentUser;
        this.$timeout = $timeout;
        this.rootScope = $rootScope;
        this.scope = $scope;
        this.router = $state;

        this.editorLink = 'portal.users.user.user-editor';

        this.updatePageView('User Management');

        this.rootScope.$bus.subscribe('USER_DATA_UPDATED', () => {
            this.GridService.updateEditDeleteButtons(0);
        });
    }

    updatePageView(title){
        this.rootScope.$bus.publish('USER_TITLE_UPDATE', {
            title: title
        });

        this.updateGridView();
    }

    updateGridView() {
        this.multiSelectColumns = this.model.getColumns();

        this.rootScope.currentModel = this.model;
        this.GridService.init({
            rowHeight: 60,
            enableSorting: true,
            paginationPageSizes: [25, 50, 75],
            paginationPageSize: 25,
            displayDeleteBtn: false,
            displayEditBtn: false,
            columnDefs: this.multiSelectColumns,
            data: this.model.getCollection(),
            rowTemplate: `<div ng-class="{'inactiveUser':row.entity.activated == 0 }">
                            <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ 'ui-grid-row-header-cell': col.isRowHeader }" ui-grid-cell>
                            </div>
                        </div>`
        }, this.scope);

        this.scope.getUserImage = (entity) => {
            if(!entity.image) {
                return '/assets/images/vzportal/user_image.png';
            }
            return entity.image;
        };
        this.scope.deleteUser = (entity) => {
            this.model.delete(entity);
        };

        this.scope.deleteUserTitle = (entity) => {
            return 'Delete User ' + entity.username;
        };

        this.scope.deleteUserMessage = () => {
            return '<p>Are you sure you want to delete this user?</p><span>If you delete this user, you will have to recreate the user profile.</span>';
        };

        this.scope.toggleActivation = (entity) => {
            entity.activated = (entity.activated === '1')? '0' : '1';
            this.model.save(entity).then((resp) => {
                this.GridService.setData(this.model.getCollection());
            });
        };

        this.scope.toggleActivationHeader = (entity) => {
            let retVal = '';
            retVal = (entity.activated === '1')? 'Inactivate User ' : 'Activate User ';
            return retVal + entity.username;
        };

        this.scope.toggleActivationMessage = (entity) => {
            return (entity.activated === '1')? 'Are you sure you want to Inactivate this user?' : 'Are you sure you want to Activate this user?';
        };
    }

    toggleGridColumns(data) {
        if(this.GridService) {
            this.GridService.toggleGridColumns(data);
        }
    }

    getUserImage(entity){
        if(!entity.image) {
            return '/assets/images/vzportal/user_image.png';
        }
        return entity.image;
    }

    addEntry() {
        this.router.go(this.editorLink + '.add', {}, {notify: false});
    }

    editEntry(){
        this.router.go(this.editorLink + '.edit', {id: this.GridService.selectedRows[0].id});
    }

    deleteUserTitle(){
        var rows = _.map(this.GridService.selectedRows,function(item){
            return item.username;
        });
        var title = 'Delete %text%';
        if(rows.length === 1){
            title = title.replace('%text%', 'User ' + rows[0]);
        }
        else {
            title = title.replace('%text%', 'Selected Users');
        }
        return title;
    }

    deleteUserMessage(){
        var rows = _.map(this.GridService.selectedRows,function(item){
            return item.username;
        });
        var message = '<p>Are you sure you want to delete this user?</p><span>If you delete this user, you will have to recreate the user profile.</span>';

        if(rows.length >= 1){
            message = '<p>Are you sure you want to delete this users?</p>';
            _.each(rows, function(username){
                message+='<span>'+username+'</span><br/>';
            });
            message+='<span>If you delete these users, you will have to recreate the user profiles.</span>';
        }
        return message;
    }

    deleteEntries() {
        if(this.GridService.selectedRows.length > 1){
            this.model.batchDelete(this.GridService.selectedRows).then(()=>{
                this.GridService.updateEditDeleteButtons(0);
            });
        }
        else {
            this.model.delete(this.GridService.selectedRows[0]).then(()=>{
                this.GridService.updateEditDeleteButtons(0);
            });
        }
    }
}

export default UserList;