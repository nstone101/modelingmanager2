'use strict';

import template from './user.html!text';
import {RouteConfig, Inject} from '../../../../../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('portal.users.user.user-editor.edit', {
    url: '/:id/edit',
    onEnter: ['$stateParams', '$uibModal', 'ModalService', ($stateParams, $modal, ModalService) => {
        const id = $stateParams.id;
        $modal.open({
            template: template,
            resolve: {
                init: ['UserModel', 'GroupModel', 'RoleModel', (UserModel, GroupModel, RoleModel) => Promise.all([UserModel.initItem(id), GroupModel.initCollection(), RoleModel.initCollection()])]
            },
            controller: UserEditEditor,
            controllerAs: 'vm',
            size: 'lg'
        }).result.finally(ModalService.onFinal('portal.users.user.list'));
    }]
})
@Inject('$rootScope', '$state', 'UserModel', 'GroupModel', 'RoleModel', 'FormService', '$uibModalInstance')
//end-non-standard
class UserEditEditor {
    constructor($rootScope, $state, UserModel, GroupModel, RoleModel, FormService, $modalInstance) {
        console.log('user edit editor');
        this.result = null;
        this.isSubmitting = null;
        this.rootScope = $rootScope;
        this.modal = $modalInstance;
        this.Model = UserModel;
        this.groups = GroupModel.getCollection();
        this.roles = RoleModel.getCollection();
        this.states = ['AL','AK','AZ','CA','CO','CT','DE','DC','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY'];

        this.user = $rootScope.currentUser;
        this.entry = this.Model.getItem();
        console.log(this.entry);
        this.router = $state;

        this.FormService = FormService;
        this.saveButtonOptions = Object.assign({}, FormService.getModalSaveButtonOptions()); // clone the modal save button options so we don't overwrite default one
        this.saveButtonOptions.buttonDefaultText = 'Update';
        this.saveButtonOptions.buttonSuccessText = 'Updated';
        this.saveButtonOptions.buttonSubmittingText = 'Updating';
        this.saveButtonOptions.buttonInitialIcon = 'fa fa-plus';
    }

    cancel() {
        this.modal.dismiss('cancel');
    }

    save(form) {
        if(!form.$valid) {return;}
        this.isSubmitting = true;
        this.rootScope.$bus.publish('USER_DATA_UPDATED');
        this.FormService.save(this.Model, this.entry, this, form);
    }
}

export default UserEditEditor;
