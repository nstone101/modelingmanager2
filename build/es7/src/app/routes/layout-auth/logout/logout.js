'use strict';

import {RouteConfig, Component, View, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('auth.logout', {
    url: '/logout'
})
@Component({
    selector: 'logout'
})
@Inject('$rootScope', '$state', 'AuthenticationService')
//end-non-standard
class Logout {
    constructor($rootScope, $state, AuthenticationService) {
        this.$rootScope = $rootScope;
        this.router = $state;
        this.AuthenticationService = AuthenticationService;
        this.logout();
    }

    logout() {
        this.AuthenticationService.logout();
        this.router.go('auth.login');
    }
}

export default Logout;
