'use strict';

import template from './forgot-password.html!text';
import {RouteConfig, Component, View, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
/*@RouteConfig('auth.forgot-password', {
    url: '/forgot-password',
    template: '<forgot-password></forgot-password>'
})*/
@Component({
    selector: 'forgot-password'
})
@View({
    template: template
})
@Inject('$rootScope', 'AuthenticationResource', 'FormService')
//end-non-standard
class ForgotPassword {
    constructor($rootScope, AuthenticationResource, FormService) {
        this.result = null;
        this.bus = $rootScope.$bus;
        this.isSubmitting = null;
        this.copyrightDate = new Date();
        this.saveButtonOptions = Object.assign({}, FormService.getModalSaveButtonOptions());
        this.saveButtonOptions.buttonDefaultText = 'Reset password';
        this.saveButtonOptions.buttonSubmittingText = 'Resetting password';
        this.saveButtonOptions.buttonSuccessText = 'Reset password';
        this.AuthenticationResource = AuthenticationResource;
        this.FormService = FormService;
    }

    resetPassword(isFormValid, form) {
        if(!isFormValid) {return;}

        this.isSubmitting = true;
        return this.AuthenticationResource.resetPassword(this.credentials)
                .then(() => {
                this.credentials = {};
                form.$setPristine();
                this.result = 'success';
                this.hasSuccess = true;
                this.hasError = false;
                this.successMessage = 'We have emailed you instructions on how to reset your password. Please check your inbox.';
                this.bus.publish('BACK_TO_LOGIN', {hasSuccess: true, successMessage: this.successMessage});
            }, (response) => {
                this.hasSuccess = false;
                form.$setPristine();
                this.FormService.onFailure(this, response);
            });
    }
}

export default ForgotPassword;
// https://stormpath.com/blog/the-pain-of-password-reset/
// https://stormpath.com/blog/password-security-right-way/
