'use strict';

import './login/login';
import './logout/logout';
import './verify/verify';
import './forgot-password/forgot-password';

import template from './layout-auth.html!text';
import {RouteConfig} from '../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('auth', {
    url: '/auth',
    abstract: true,
    template: template
})
//end-non-standard
class LayoutAuth {}
