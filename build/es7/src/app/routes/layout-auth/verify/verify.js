'use strict';

import template from './verify.html!text';
import {RouteConfig, Component, View, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('auth.verify', {
    url: '/verify/:email/:hash',
    template: template,
    resolve: {
        verification: ($stateParams, AuthenticationService) => {
            return AuthenticationService.verifyEmail({email: $stateParams.email, hash: $stateParams.hash});
        }
    }
})
@Component({
    selector: 'verify'
})
@Inject('verification')
//end-non-standard
class Verify {
    constructor(verification) {
        this.verification = verification;
        console.log(this.verification);
    }
}

export default Verify;
