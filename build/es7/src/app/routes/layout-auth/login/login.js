'use strict';

import TweenMax from 'greensock/TweenMax'; window.TweenMax = TweenMax;

import template from './login.html!text';
import {RouteConfig, Component, View, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@RouteConfig('auth.login', {
    url: '/login',
    template: template
})
@Component({
    selector: 'login'
})
@Inject('$rootScope', '$state', 'AuthenticationService', 'FormService')
//end-non-standard
class Login {
    constructor($rootScope, $state, AuthenticationService, FormService) {
        this.$rootScope = $rootScope;
        this.bus = $rootScope.$bus;
        this.router = $state;
        this.result = null;
        this.isSubmitting = null;
        this.copyrightDate = new Date();
        this.saveButtonOptions = Object.assign({}, FormService.getModalSaveButtonOptions());
        this.saveButtonOptions.buttonDefaultText = 'Sign in';
        this.saveButtonOptions.buttonSubmittingText = 'Signing in';
        this.saveButtonOptions.buttonSuccessText = 'Signed in';
        this.AuthenticationService = AuthenticationService;
        this.FormService = FormService;

        $('.password-recovery').bind('click', function() {
            TweenMax.to($('.login-content'), 0.25, {x:-289});
            TweenMax.to($('.forgot-content'), 0.25, {x:-289});
            $('#messageUserFail').empty();
            $('#messagePWFail').empty();
        });

        $('.login-back-btn').bind('click', function() {
            TweenMax.to($('.login-content'), 0.25, {x:0});
            TweenMax.to($('.forgot-content'), 0.25, {x:0});
            $('#messageRecoverFail').empty();
        });

        var hash = window.location.hash.replace('#','');
        if(hash === 'PasswordRecovery'){
            TweenMax.to($('.login-content'), 0, {x:-292});
            TweenMax.to($('.forgot-content'), 0, {x:-292});
        }

        this.bus.subscribe('BACK_TO_LOGIN', (params) => {
            console.log(params);
            this.hasSuccess = params.hasSuccess;
            this.successMessage = params.successMessage;
            
            TweenMax.to($('.login-content'), 0.25, {x:0});
            TweenMax.to($('.forgot-content'), 0.25, {x:0});
            $('#messageRecoverFail').empty();
        });
    }

    login(isFormValid, form) {
        if(!isFormValid) {return;}

        this.isSubmitting = true;
        return this.AuthenticationService.login(this.credentials).then(() => {
            this.$rootScope.currentUser = this.AuthenticationService.getCurrentUser();
            this.FormService.onSuccess(this);
            this.router.go('portal.nodes.browser');
        }, (response) => {
            console.log(response);
            form.$setPristine();
            this.FormService.onFailure(this, response);
        });
    }
}

export default Login;
