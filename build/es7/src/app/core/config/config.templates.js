/**
 * Templates
 *
 * any files you want to put into $templateCache need to be imported here.
 */

'use strict';

import cgBusyTemplate from '../../../../jspm_packages/github/cgross/angular-busy@4.1.3/angular-busy.html!text';

import {Run, Inject} from '../../decorators'; // jshint unused: false
class OnRunTemplates {
    //start-non-standard
    @Run()
    @Inject('$rootScope', '$templateCache')
    //end-non-standard
    static runFactory($rootScope, $templateCache){
        $rootScope.ENV = 'dev';
        // put templates for template cache here
        $templateCache.put('angular-busy.html', cgBusyTemplate);
    }
}

export {OnRunTemplates};