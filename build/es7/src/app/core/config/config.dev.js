'use strict';

import {Config, Run, Inject} from '../../decorators'; // jshint unused: false

class OnConfigDev {
    //start-non-standard
    @Config()
    @Inject('localStorageServiceProvider')
    //end-non-standard
    static configFactory(localStorageServiceProvider){
        // use "e-scheduling" as a localStorage name prefix so app doesn’t accidently read data from another app using the same variable names
        localStorageServiceProvider.setPrefix('portal');
    }
}

class OnRunDev {
    //start-non-standard
    @Run()
    @Inject('$rootScope', 'localStorageService')
    //end-non-standard
    static runFactory($rootScope, localStorageService){
        $rootScope.ENV = 'dev';
        // clear all locale storage for portal
        //localStorageService.clearAll();
    }
}

export {OnConfigDev, OnRunDev};
