'use strict';

import './config.dev'; // TODO: (martin) use systemJs conditional imports
import './config.templates';
import {Constant, Config, Run, Inject} from '../../decorators'; // jshint unused: false

class OnConfig {
    //start-non-standard
    @Constant('BASE_URL', window.location.protocol + '//' + window.location.host)
    @Constant('API_URL', window.location.protocol + '//' + window.location.host + '/api')
    @Config()
    @Inject('$locationProvider', '$provide', '$urlRouterProvider', '$httpProvider')
    //end-non-standard
    static configFactory($locationProvider, $provide, $urlRouterProvider, $httpProvider){
        // overwrite the default behaviour for $uiViewScroll service (scroll to top of the page)
        $provide.decorator('$uiViewScroll', ['$delegate', '$window', function ($delegate, $window) {
            return function () {
                $window.scrollTo(0,0);
            };
        }]);

        // Create an event pub/sub
        $provide.decorator('$rootScope', $delegate => {
            Object.defineProperty($delegate.constructor.prototype, '$bus', {
                get: function() {
                    var self = this;
                    var rootScope = $delegate;
                    return {
                        publish: (msg, data) => {
                            data = data || {};
                            // emit goes to parents, broadcast goes down to children
                            // since rootScope has no parents, this is the least noisy approach
                            // however, with the caveat mentioned below
                            rootScope.$emit(msg, data);
                        },
                        subscribe: (msg, func) => {
                            var unbind = rootScope.$on(msg, (event, args) => func(args));
                            // being able to enforce unbinding here is why decorating rootscope
                            // is preferred over DI of an explicit bus service
                            self.$on('$destroy', unbind);
                        }
                    };
                }
            });

            Object.defineProperty($delegate.constructor.prototype, '$smartWatch', {
                get: function() {
                    var self = this;

                    return (expression, handler) => {
                        self.$watch(expression, (newValue, oldValue) => {
                            if (oldValue !== newValue) {
                                handler();
                            }
                        }, true);
                    };
                }
            });

            return $delegate;
        });

        /*********************************************************************
         * Route provider configuration based on these config constant values
         *********************************************************************/
        // set restful base API Route
        $httpProvider.interceptors.push('ApiUrlHttpInterceptor');

        // set restful base API Route
        $httpProvider.interceptors.push('HttpAuthenticationInterceptor');

        // use the HTML5 History API
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        // for any unmatched url, send to 404 page (Not page found)
        $urlRouterProvider.otherwise('/404');

        // the `when` method says if the url is `/` redirect to `/node/browser` what is basically our `home` for this application
        $urlRouterProvider.when('/', '/portal/nodes/browser/');
        //$urlRouterProvider.when('/', '/auth/login');
    }
}

class OnRun {
    //start-non-standard
    @Run()
    @Inject('$rootScope', '$state', '$log', '$window', 'BASE_URL', 'formlyConfig')
    //end-non-standard
    static runFactory($rootScope, $state, $log, $window, BASE_URL, formlyConfig){

        $window.onbeforeunload = () => {
            console.log('we are leaving. ');
        };
        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
            event.preventDefault();
            if(error.status && error.status === 401){
                $state.go('auth.login');
            }
            else {
                $log.error(error.stack);
                $state.go('500');
            }
        });
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            let base = (toState.url === 'BASE_URL') ? BASE_URL : toState.url;
            if (toState.name === 'external') {
                event.preventDefault();
                $window.open(base + toParams.destUrl, '_self');
            }
        });

       $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            event.preventDefault();
            console.log('$stateChangeSuccess to ' + toState.name + '- fired once the state transition is complete.');
       });

       $rootScope.$on('$viewContentLoaded', function (event) {
            event.preventDefault();
            console.log('$viewContentLoaded - fired after dom rendered', event);
       });

       $rootScope.$on('$stateNotFound', function (event, unfoundState, fromState, fromParams) {
            event.preventDefault();
            console.log('$stateNotFound ' + unfoundState.to + ' - fired when a state cannot be found by its name.');
            console.log(unfoundState, fromState, fromParams);
       });
        // NOTE: This next line is highly recommended. Otherwise Chrome's autocomplete will appear over your options!
        formlyConfig.extras.removeChromeAutoComplete = true;

        formlyConfig.setType({
            name: 'ui-select',
            extends: 'select',
            template:`<ui-select ng-model="model[options.key]" theme="bootstrap" ng-required="{{to.required}}" ng-disabled="{{to.disabled}}" reset-search-input="false">
                        <ui-select-match placeholder="{{to.placeholder}}">{{$select.selected[to.labelProp || \'name\']}}</ui-select-match>
                        <ui-select-choices group-by="to.groupBy" repeat="option[to.valueProp || \'value\'] as option in to.options | filter: $select.search">
                            <div ng-bind-html="option[to.labelProp || \'name\'] | highlight: $select.search"></div>
                        </ui-select-choices>
                    </ui-select>`
        });
    }
}

export {OnConfig, OnRun};
