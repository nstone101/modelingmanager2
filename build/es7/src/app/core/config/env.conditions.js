'use strict';

/* inject:env */
export var mock = false;
export var optimize = false;
export var environment = 'dev';
/* endinject */

console.log('mock: ' + mock);
console.log('optimize: ' + optimize);
console.log('environment: ' + environment);
