
'use strict';

import {Config, Inject} from '../../decorators'; // jshint unused: false

class OnConfigProd {
    //start-non-standard
    @Config()
    @Inject('$compileProvider', '$httpProvider', '$localStorage')
    //end-non-standard
    static configFactory($compileProvider, $httpProvider, $localStorage){
        // use "e-scheduling" as a localStorage name prefix so app doesn’t accidently read data from another app using the same variable names
        $localStorage.setPrefix('portal');

        // disabling debug data to get better performance gain in production
        $compileProvider.debugInfoEnabled(false);
        // configure $http service to combine processing of multiple http responses received at
        // around the same time via $rootScope.$applyAsync to get better performance gain in production
        $httpProvider.useApplyAsync(true);
    }
}

class OnRunProd {
    //start-non-standard
    @Run()
    @Inject('$rootScope', 'localStorageService')
    //end-non-standard
    static runFactory($rootScope, localStorageService){
        $rootScope.ENV = 'prod';
        // clear all locale storage for portal
        localStorageService.clearAll();
    }
}
export {OnConfigProd, OnRunProd};
