
'use strict';

export const USER_ROLES = Object.freeze({READONLY: 'Read-Only', READWRITE: 'Read-Write', ADMIN: 'admin', JMAAdmin: 'JMA-Admin', VZADMIN: 'VZ-Admin'});
// http://labs.qandidate.com/blog/2014/10/16/using-the-accept-header-to-version-your-api/
export const HEADER_API_VERSION = 'application/vnd.portal+json;version=1'; // application/vnd.portal.v1+json
