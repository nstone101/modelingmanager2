
'use strict';

import {USER_ROLES, HEADER_API_VERSION} from './constants';

describe('constants', () => {

    describe('USER_ROLES', () => {
        it('should contain admin role', () => {
            expect(USER_ROLES.ADMIN).toEqual('admin');
        });

        it('should contain manager role', () => {
            expect(USER_ROLES.MANAGER).toEqual('manager');
        });

        it('should contain buyer role', () => {
            expect(USER_ROLES.EMPLOYEE).toEqual('employee');
        });
    });

    it('should contain header api version', () => {
        expect(HEADER_API_VERSION).toEqual('application/vnd.portal+json;version=1');
    });
});
