'use strict';

// config
import './config/config';

// directives
import './directives/dropzone/dropzone';
import './directives/scroll-up/scroll-up';
import './directives/photo-widget/photo-widget';
import './directives/photo-layout/photo-layout';
import './directives/modal-confirm/modal-confirm';

// interceptors
import './interceptors/url-http';
import './interceptors/http-auth';

// models
import './models/modal';
import './models/token';
import './models/user';
import './models/role';
import './models/group';
import './models/node';
import './models/file-model';



import './models/node-pages/async';
import './models/node-pages/card-slotting';
import './models/node-pages/customer-addressing';
import './models/node-pages/customer-circuits';
import './models/node-pages/dns-trap-log';
import './models/node-pages/fw-golden-config';
import './models/node-pages/fw-setup';
import './models/node-pages/node-notes';
import './models/node-pages/ip-schema';
import './models/node-pages/ip-public';
import './models/node-pages/ip-private';
import './models/node-pages/ip-idn';
import './models/node-pages/mx960-port-assignment';
import './models/node-pages/network-connection';
import './models/node-pages/node-device-names';
import './models/node-pages/ns5400-port-assignment';
import './models/node-pages/ping-poller';
import './models/node-pages/subnet-advertising';
import './models/node-pages/subnet-information';
import './models/node-pages/tdr';
import './models/node-pages/vpn-routing';

import './models/reports/latest-node-state-changes';
import './models/reports/mx960-ports-requiring-scanning';
import './models/reports/utility-server-ips';


// resources
import './resources/authentication/authentication';
import './resources/user/user';
import './resources/role/role';
import './resources/group/group';
import './resources/node/node';
import './resources/admin-tools/file';

// Modeling -- Mics/MPC/Routers/Firewalls/Mic PAs
import './resources/modeling/modeling';






import './resources/node-pages/async';
import './resources/node-pages/card-slotting';
import './resources/node-pages/customer-addressing';
import './resources/node-pages/customer-circuits';
import './resources/node-pages/dns-trap-log';
import './resources/node-pages/fw-golden-config';
import './resources/node-pages/fw-setup';
import './resources/node-pages/node-notes';
import './resources/node-pages/ip-schema';
import './resources/node-pages/ip-public';
import './resources/node-pages/ip-private';
import './resources/node-pages/ip-idn';
import './resources/node-pages/mx960-port-assignment';
import './resources/node-pages/network-connection';
import './resources/node-pages/node-device-names';
import './resources/node-pages/ns5400-port-assignment';
import './resources/node-pages/ping-poller';
import './resources/node-pages/subnet-advertising';
import './resources/node-pages/subnet-information';
import './resources/node-pages/tdr';
import './resources/node-pages/vpn-routing';

import './resources/reports/latest-node-state-changes';
import './resources/reports/mx960-ports-requiring-scanning';
import './resources/reports/utility-server-ips';

// http://snippetrepo.com/snippets/angularjs-http-rest-service-abstraction
// https://medium.com/@tomastrajan/model-pattern-for-angular-js-67494389d6f
// http://blog.jonparrott.com/angular-writing-list-controllers-the-easy-way/ TODO: move away from restangular and instead of create resources with $http (loading lodash is overkill for this project)
// also add code to abstract resource where I initilize version of endpoint as I can have different version for each endpoint e.g. microservices styles. The default should be v1 and extends class should have option overwrite version number e.g. v2

// services
import './services/authentication';
import './services/form';
import './services/grid'; //node-data
import './services/modal';
import './services/node-page-model.js';
import './services/report-model';


//import './services/dmodels-page-model';
//import './resources/dmodels-pages/card';
//import './models/dmodels-pages/card';
//import './resources/dmodels/dmodels';