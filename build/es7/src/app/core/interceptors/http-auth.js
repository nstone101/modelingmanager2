'use strict';

import {Service, Inject} from '../../decorators'; // jshint unused: false

let self;
//start-non-standard
@Service({
    serviceName: 'HttpAuthenticationInterceptor'
})
@Inject('$location', '$injector')
//end-non-standard
class HttpAuthenticationInterceptor {
    constructor($location, $injector) {
        self = this; // http://stackoverflow.com/questions/28638600/angularjs-http-interceptor-class-es6-loses-binding-to-this
        this.$location = $location;
        this.$injector = $injector;
    }

    response(resp){
        if(resp.headers('X-TOKEN-REFRESH')){
            const TokenModel = self.$injector.get('TokenModel');
            TokenModel.set(resp.headers('X-TOKEN-REFRESH'));
            console.info('token refreshed');
        }
        return resp;
    }

    responseError(rejection) {
        switch (rejection.status) {
            case 400:
            case 401:
                // 400 is related to an invalid token, including an expried token
                // 401 is an missing token or unauthorized request.
                // injected manually to get around circular dependency problem
                const TokenModel = self.$injector.get('TokenModel');
                TokenModel.remove();
                self.$location.path('/auth/login');
                break;
            case 403:
                self.$location.path('/403');
                break;
        }

        return Promise.reject(rejection);
    }

    request(config) {
        // injected manually to get around circular dependency problem
        const TokenModel = self.$injector.get('TokenModel');
        const token = TokenModel.get();
        if (token) {
            config.headers = config.headers || {};
            config.headers.Authorization = 'Bearer ' + token;
        }

        return config;
    }
}

export default HttpAuthenticationInterceptor;
