'use strict';

import {HEADER_API_VERSION} from '../constants/constants';
import {Service, Inject} from '../../decorators'; // jshint unused: false

let self;
//start-non-standard
@Service({
    serviceName: 'ApiUrlHttpInterceptor'
})
@Inject('$q', '$injector', '$window')
//end-non-standard
class ApiUrlHttpInterceptor {
    constructor($q, $injector) {
        self = this; // http://stackoverflow.com/questions/28638600/angularjs-http-interceptor-class-es6-loses-binding-to-this
        self.apiUrl = '/api';
        self.$q = $q;
        self.$injector = $injector;
        self.whitelist = {
            'ui-grid/ui-grid':true,
            'ui-grid/uiGridMenu': true
        };
    }

    response(response) {
        // unwrap response.data for every successful request
        if(response.config.headers['Content-Type'] && response.config.headers['Content-Type'].includes(HEADER_API_VERSION)){
            return response.data;
        }

        return response;
    }

    responseError(rejection) {
        //http://jcrowther.io/2015/05/19/angular-js-http-interceptors/
        // retry request for 503 failure
        if(rejection.status === 401) {
            // auth error. redirect to the login page.
/*            if(window.location.hostname === 'localhost') {
                rejection.data = 'http://portal2:8081' + rejection.data;
            }
            window.location.href = rejection.data;*/
        }

        if (rejection.status !== 503) {return self.$q.reject(rejection);}
        
        if (rejection.config.retry) {
            rejection.config.retry++;
        } else {
            rejection.config.retry = 1;
        }

        if (rejection.config.retry < 5) {
            return self.$injector.get('$http')(rejection.config);
        } else {
            
            return self.$q.reject(rejection);
        }
    }

    shouldPrependApiUrl(reqConfig) {
        if (!self.apiUrl) {return false;}
        if(!self.whitelist[reqConfig.url]) {
            return !(/[\s\S]*.[html|htm]/.test(reqConfig.url) || (reqConfig.url && reqConfig.url.includes(self.apiUrl)));
        }
    }

    request(config) {
        // Filter out requests for .html templates, etc
        if (self.shouldPrependApiUrl(config)) {
            config.url = self.apiUrl + config.url;

            // add api version to header
            /*jshint -W069 */
            config.headers['Accept'] = HEADER_API_VERSION;
            if(config.method === 'POST' || config.method === 'PUT') {
                config.headers['Content-Type'] = HEADER_API_VERSION;
            }
        }

        return config;
    }
}

export default ApiUrlHttpInterceptor;
