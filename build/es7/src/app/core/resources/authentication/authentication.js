'use strict';

import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'AuthenticationResource'
})
@Inject('$rootScope', '$http', '$window')
//end-non-standard
class AuthenticationResource {
    constructor($rootScope, $http, $window) {
        this.http = $http;
        /* jshint ignore:start */
        this.base = window.PORTAL_URLS.API;
        /* jshint ignore:end */
        this.route = 'api/auth';
        this.$rootScope = $rootScope;
        this.$window = $window;
    }

    login(credentials) {
        if(this.$rootScope.ENV === 'dev'){
            const encoded = encodeURIComponent(this.$window.btoa(JSON.stringify(credentials)));
            return this.http.get(`${this.base}/${this.route}/login`, {params: {data: encoded}});
        }
        else {
            const encoded = this.$window.btoa(JSON.stringify(credentials));
            return this.http.post(`${this.base}/${this.route}/login`, encoded);
        }
    }

    logout() {
        return this.http.post(`${this.base}/${this.route}/logout`);
    }

    verifyEmail(credentials) {
        return this.http.get(`${this.base}/${this.route}/verify/${credentials.email}/${credentials.hash}`);
    }

    resetPassword(credentials) {
        const encoded = encodeURIComponent(this.$window.btoa(JSON.stringify(credentials)));
        return this.http.get(`${this.base}/${this.route}/recoverPassword`, {params: {data: encoded}});
    }

    updatePassword(credentials) {
        if(this.$rootScope.ENV === 'dev'){
            const encoded = encodeURIComponent(this.$window.btoa(JSON.stringify(credentials)));
            return this.http.get(`${this.base}/${this.route}/password`, {params: {data: encoded}});
        }
        else {
            const encoded = this.$window.btoa(JSON.stringify(credentials));
            return this.http.post(`${this.base}/${this.route}/password`, encoded);
        }
    }
}

export default AuthenticationResource;
