'use strict';

import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'UserResource'
})
@Inject('$http')
//end-non-standard
class UserResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/user');
    }

    getUserByEmail(email) {
        return this.http.get(`/${this.route}`, {params: {email: email}});
    }

    getUserByUsername(username) {
        return this.http.get(`/${this.route}`, {params: {username: username}});
    }

    updateUserProfile (user) {
        return this.http.post(`/${this.route}/updateUserProfile`, {params: {user: user}});
    }

    updateUserPassword (user) {
        return this.http.post(`/${this.route}/updateUserPassword`, {params: {user: user}});
    }
}

export default UserResource;
