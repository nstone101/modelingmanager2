'use strict';

class AbstractResource {
    constructor(http, route) {
        this.http = http;
        this.route = route;
        this.base = window.PORTAL_URLS.API;
    }

    setUri(uri){
        this.route = uri;
    }

    get(id, params, cache) {
        return this.http.get(`${this.base}/${this.route}/${id}`, {params: params, cache: cache ? true : false});
    }

    getList(params, cache) {
        return this.http.get(`${this.base}/${this.route}`, {params: params, cache: cache ? true : false});
    }

    create(newResource) {
        return this.http.post(`${this.base}/${this.route}`, newResource);
    }

    update(updatedResource) {
        return this.http.put(`${this.base}/${this.route}/${updatedResource.id}`, updatedResource);
    }

    delete(id) {
        return this.http.delete(`${this.base}/${this.route}/${id}`);
    }

    batchDelete(ids) {
        return this.http.post(`${this.base}/${this.route}/batch-delete`, ids);
    }
}

export default AbstractResource;
