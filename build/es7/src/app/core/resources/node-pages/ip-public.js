
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'IpPublicResource'
})
@Inject('$http')
//end-non-standard
class IpPublicResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/ip-public');
    }
}

export default IpPublicResource;
