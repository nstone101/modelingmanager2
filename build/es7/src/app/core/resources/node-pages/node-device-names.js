
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'NodeDeviceNamesResource'
})
@Inject('$http')
//end-non-standard
class NodeDeviceNamesResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/node-device-names');
    }
}

export default NodeDeviceNamesResource;
