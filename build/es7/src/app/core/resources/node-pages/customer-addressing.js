
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'CustomerAddressingResource'
})
@Inject('$http')
//end-non-standard
class CustomerAddressingResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/customer-addressing');
    }
}

export default CustomerAddressingResource;