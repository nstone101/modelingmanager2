
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'DnsTrapLogResource'
})
@Inject('$http')
//end-non-standard
class DnsTrapLogResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/dns-trap-log');
    }
}

export default DnsTrapLogResource;