
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'IpIDNResource'
})
@Inject('$http')
//end-non-standard
class IpIDNResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/ip-idn');
    }
}

export default IpIDNResource;
