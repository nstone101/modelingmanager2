
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'VpnRoutingResource'
})
@Inject('$http')
//end-non-standard
class VpnRoutingResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/vpn-routing');
    }
}

export default VpnRoutingResource;