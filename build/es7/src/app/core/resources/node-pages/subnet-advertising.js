
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'SubnetAdvResource'
})
@Inject('$http')
//end-non-standard
class SubnetAdvResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/subnet-adv');
    }
}

export default SubnetAdvResource;