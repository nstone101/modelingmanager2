
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'CardSlottingResource'
})
@Inject('$http')
//end-non-standard
class CardSlottingResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/card-slotting');
    }
}

export default CardSlottingResource;