
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'FwGoldenConfigResource'
})
@Inject('$http')
//end-non-standard
class FwGoldenConfigResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/fw-golden-config');
    }
}

export default FwGoldenConfigResource;