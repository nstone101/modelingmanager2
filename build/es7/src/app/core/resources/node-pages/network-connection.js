
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'NetworkConnectionResource'
})
@Inject('$http')
//end-non-standard
class NetworkConnectionResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/network-connections');
    }
}

export default NetworkConnectionResource;