
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'Ns5400PortAssignmentResource'
})
@Inject('$http')
//end-non-standard
class Ns5400PortAssignmentResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/ns5400-port-assignments');
    }
}

export default Ns5400PortAssignmentResource;