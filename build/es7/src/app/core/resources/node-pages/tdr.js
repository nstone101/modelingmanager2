
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'TdrResource'
})
@Inject('$http')
//end-non-standard
class TdrResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/tdr');
    }
}

export default TdrResource;