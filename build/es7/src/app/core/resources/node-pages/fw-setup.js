
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'FwSetupResource'
})
@Inject('$http')
//end-non-standard
class FwSetupResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/fw-setup');
    }
}

export default FwSetupResource;
