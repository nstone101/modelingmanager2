
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'IpPrivateResource'
})
@Inject('$http')
//end-non-standard
class IpPrivateResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/ip-private');
    }
}

export default IpPrivateResource;
