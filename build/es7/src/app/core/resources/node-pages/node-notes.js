
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'NodeNotesResource'
})
@Inject('$http')
//end-non-standard
class NodeNotesResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/node-notes');
    }
}

export default NodeNotesResource;
