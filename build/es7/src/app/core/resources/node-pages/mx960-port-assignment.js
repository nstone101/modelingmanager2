
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'Mx960PortAssignmentResource'
})
@Inject('$http')
//end-non-standard
class Mx960PortAssignmentResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/mx960-port-assignments');
    }
}

export default Mx960PortAssignmentResource;