
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'AsyncResource'
})
@Inject('$http')
//end-non-standard
class AsyncResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/async');
    }
}

export default AsyncResource;