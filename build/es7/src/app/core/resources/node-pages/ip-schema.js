
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'IpSchemaResource'
})
@Inject('$http')
//end-non-standard
class IpSchemaResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/ip-schema');
    }
}

export default IpSchemaResource;