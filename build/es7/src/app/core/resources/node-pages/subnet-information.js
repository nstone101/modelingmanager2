
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'SubnetInformationResource'
})
@Inject('$http')
//end-non-standard
class SubnetInformationResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/node-data/subnet-information');
    }
}

export default SubnetInformationResource;