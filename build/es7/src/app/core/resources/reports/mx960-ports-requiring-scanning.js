'use strict';

import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'Mx960PortsRequiringScanningResource'
})
@Inject('$http')
//end-non-standard
class Mx960PortsRequiringScanningResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/reports/mx960PortsRequiringScanning');
    }
}

export default Mx960PortsRequiringScanningResource;