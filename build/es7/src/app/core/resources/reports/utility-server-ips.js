'use strict';

import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'UtilityServerIpsResource'
})
@Inject('$http')
//end-non-standard
class UtilityServerIpsResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/reports/utilityServerIps');
    }
}

export default UtilityServerIpsResource;