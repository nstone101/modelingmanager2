'use strict';

import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'LatestNodeStateChangeResource'
})
@Inject('$http')
//end-non-standard
class LatestNodeStateChangeResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/reports/latestNodeStateChanges');
    }
}

export default LatestNodeStateChangeResource;