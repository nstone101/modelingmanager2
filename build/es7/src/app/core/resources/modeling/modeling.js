
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'ModelingResource'
})
@Inject('$http')
//end-non-standard
class ModelingResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/modeling');
        this.base = window.PORTAL_URLS.API;
    }

    getMICCards(params) {
        return this.http.get(`${this.base}/api/modeling/mic`);//orig mic
    }
    getMICPAs(params) {
        return this.http.get(`${this.base}/api/modeling/mic-portassignments`);
    }
    getMPCCards(params) {
        return this.http.get(`${this.base}/api/modeling/mpc`); //orig mpc
    }
}

export default ModelingResource;


