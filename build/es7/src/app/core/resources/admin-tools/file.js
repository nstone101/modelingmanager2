/**
 * Created by bpowell on 12/6/2015.
 */
'use strict';

import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'FileResource'
})
@Inject('$http')
//end-non-standard
class FileResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/files');
    }
}

export default FileResource;
