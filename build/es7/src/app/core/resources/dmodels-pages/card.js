
import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'CardResource'
})
@Inject('$http')
//end-non-standard
class CardResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/dmodels-data/card');
    }
}

export default CardResource;