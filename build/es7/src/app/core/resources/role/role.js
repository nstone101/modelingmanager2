'use strict';

import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'RoleResource'
})
@Inject('$http')
//end-non-standard
class RoleResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/role');
    }
}

export default RoleResource;
