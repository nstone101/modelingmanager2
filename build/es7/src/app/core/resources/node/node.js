'use strict';

import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'NodeResource'
})
@Inject('$http')
//end-non-standard
class NodeResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/nodes/node');
        /* jshint ignore:start */
        this.base = window.PORTAL_URLS.API;
        /* jshint ignore:end */
    }

    getNodeByName(nodeName) {
        return this.http.get(`${this.base}/${this.route}/${nodeName}`);
    }

    getNodeBrowser(filter) {
        let _url = (filter !== '' && filter !== undefined)? `${this.base}/api/nodes/browser/${filter}` : `${this.base}/api/nodes/browser`;
        return this.http.get(_url);
    }

    getNewNodesBrowser(filter) {
        let _url = (filter !== '' && filter !== undefined)? `${this.base}/api/nodes/newnodesbrowser/${filter}` : `${this.base}/api/nodes/newnodesbrowser`;
        return this.http.get(_url);
    }

    createNewNode(params) {
        let str = params.name + '/' + params.type;
        return this.http.post(`${this.base}/api/nodes/createnewnode/${str}`);
    }

    dumpCompleteNode(siteName) {

        let str2 = siteName.siteName;

        return this.http.post(`${this.base}/api/nodes/dumpcompletenode/${str2}`);
    }



    getNodeDocs(siteName) {
        if(siteName === undefined) {
            throw 'site name is required.';
        }
        return this.http.get(`${this.base}/api/nodes/docs/${siteName}`);
    }

    deleteNodeDoc(params) {
        return this.http.post(`${this.base}/api/nodes/docs/delete`, params);
    }

    downloadNodeDoc(params) {
        console.log('downloadNodeDoc with params : ' + angular.toJson(params));
        return this.http.get(`${this.base}/api/nodes/download`, params);
    }


    getNodePhotos(siteName) {
        if(siteName === undefined) {
            throw 'site name is required.';
        }
        return this.http.get(`${this.base}/api/nodes/photos/${siteName}`);
    }

    deleteNodePhoto(params) {
        return this.http.post(`${this.base}/api/nodes/photos/delete`, params);
    }

    getPhotoArchive() {
        return this.http.get(`${this.base}/api/nodes/photos/archive`);
    }

    getNodeDiagram(siteName) {
        if(siteName === undefined) {
            throw 'site name is required.';
        }
        return this.http.get(`${this.base}/api/nodes/diagrams/${siteName}`);
    }

    getDesignDocs(gen){
        return this.http.get(`${this.base}/api/nodes/designDocs/${gen}`);
    }
    //getCardManager(siteName) {
    //    if(siteName === undefined) {
    //        throw 'card/site name is required.';
    //    }
    //    return this.http.get(`${this.base}/api/nodes/cardManager/${siteName}`);
    //}//Do I add siteName to the end of this or will I put params or should I create something like cardNumber/cardName



}

export default NodeResource;
