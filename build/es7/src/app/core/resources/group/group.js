'use strict';

import AbstractResource from '../abstract-resource';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'GroupResource'
})
@Inject('$http')
//end-non-standard
class GroupResource extends AbstractResource {
    constructor($http) {
        super($http, 'api/group');
    }
}

export default GroupResource;
