'use strict';

import AbstractModel from './abstract-model';
import {Service} from '../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'ModalModel'
})
//end-non-standard
class ModalModel extends AbstractModel {
    constructor() {
        super(); // we only use modal with get/set item to store $modalInstance
    }
}

export default ModalModel;
