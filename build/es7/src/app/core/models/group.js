'use strict';

import AbstractModel from './abstract-model';
import {Service, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'GroupModel'
})
@Inject('GroupResource')
//end-non-standard
class GroupModel extends AbstractModel {
    constructor(GroupResource) {
        super(GroupResource);
        this.idProp = 'group_id';
    }
}

export default GroupModel;
