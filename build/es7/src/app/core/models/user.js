'use strict';

import AbstractModel from './abstract-model';
import {Service, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'UserModel'
})
@Inject('UserResource')
//end-non-standard
class UserModel extends AbstractModel {
    constructor(UserResource) {
        super(UserResource);

        /* jshint ignore:start */
        this.columns = [
            {"field": "image", "width": 64, "cellTemplate": '<div class="text-center"><img class="user-image" ng-src="{{grid.appScope.getUserImage(row.entity)}}" alt="{{row.entity.fullName}}"/></div>'},
            {"field": "FullName", "name": "Name", "ticked": true, "cellClass": "middle"},
            {"field": "username", "name": "User", "ticked": true, "cellClass": "middle"},
            {"field": "email", "name": "Email", "ticked": true, "cellClass": "middle"},
            {"field": "cellphone", "name": "Cell", "ticked": true, "cellClass": "middle"},
            {"field": "workphone", "name": "Work", "ticked": true, "cellClass": "middle"},
            {"field": "roles[0]", "displayName": "Role", "ticked": true, "cellClass": "middle"},
            {"field": "usergroup", "name": "Group", "ticked": true, "cellClass": "middle"},
            {"field": "activated", "name": "Active", "ticked": true, "width": 72, "cellClass": "middle",
                "cellTemplate": '<div class="text-center"><a <i ng-class="{\'fa fa-toggle-on active\':row.entity.activated == 1, \'fa fa-toggle-off inactive\':row.entity.activated == 0}" tooltip="Toggle Activation" tooltip-placement="left"  modal-confirm="grid.appScope.toggleActivation(row.entity)" confirm-header="grid.appScope.toggleActivationHeader(row.entity)" confirm-message="grid.appScope.toggleActivationMessage(row.entity)"></i></div>'},
            {"name": "Delete", "width": 92, "cellClass": "middle",
                "cellTemplate": '<div class="text-center"><span class="text-center delete-btn"><i class="fa fa-trash-o fa-2x" tooltip="Delete" tooltip-placement="left" modal-confirm="grid.appScope.deleteUser(row.entity)"  confirm-header-fn="grid.appScope.deleteUserTitle(row.entity)" confirm-message-fn="grid.appScope.deleteUserMessage(row.entity)" cancel-label="No, Do not delete" ok-label="Yes, Delete user profile(s)"></i></span>'}
        ];
        /* jshint ignore:end */
    }

    getColumns() {
        return this.columns;
    }

    save(item) {
        // update existing item if model contains id
        if (item.id) {
            /* jshint ignore:start*/
            item = {"id": item.id, "user": item};
            /* jshint ignore:end*/
            return this.resource.update(item).then(itemRespond => {
                for (let i = 0; i < this.collection.length; i++) {
                    if(this.collection[i].id === itemRespond.id) {
                        this.collection[i] = itemRespond;
                    }
                }
            });
        } else {
            /* jshint ignore:start*/
            item = {"user": item};
            /* jshint ignore:end*/
            return this.resource.create(item)
                .then((itemRespond) => {return itemRespond.data;})
                .then((itemRespond) => {
                    item.id = itemRespond.id;
                    this.collection.push(item);
                });
        }
    }
}

export default UserModel;
