'use strict';

import AbstractModel from '../abstract-model';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'Mx960PortsRequiringScanningModel'
})
@Inject('Mx960PortsRequiringScanningResource')
//end-non-standard
class Mx960PortsRequiringScanningModel extends AbstractModel {
    constructor(Mx960PortsRequiringScanningResource) {
        super(Mx960PortsRequiringScanningResource);
        /* jshint ignore:start */
        this.columns = [
            { 'name': 'Site Name', 'field': 'SiteName', width: 125, headerTooltip: 'Site Name', ticked: true },
            { 'name': 'Interface', 'field': 'Interface', width: 125, headerTooltip: 'Interface', ticked: true },
            { 'name': 'Connected Device Type', 'field': 'ConnectedDeviceType', width: 115, headerTooltip: 'Connected Device Type', ticked: true },
            { 'name': 'Connected Device Name', 'field': 'ConnectedDeviceName', width: 115, headerTooltip: 'Connected Device Name', ticked: true },
            { 'name': 'Connected Device Port', 'field': 'ConnectedDevicePort', width: 115, headerTooltip: 'Connected Device Port', ticked: true }
        ];
        /* jshint ignore:end */
    }

    getColumns() {
        return this.columns;
    }
}

export default Mx960PortsRequiringScanningModel;
