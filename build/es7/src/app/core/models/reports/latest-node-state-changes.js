'use strict';

import AbstractModel from '../abstract-model';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'LatestNodeStateChangesModel'
})
@Inject('LatestNodeStateChangeResource')
//end-non-standard
class LatestNodeStateChangesModel extends AbstractModel {
    constructor(LatestNodeStateChangeResource) {
        super(LatestNodeStateChangeResource);
        /* jshint ignore:start */
        this.columns = [
            { 'name': 'Site Name', 'field': 'SiteName', width: 125, headerTooltip: 'Site Name', ticked: true },
            { 'name': 'Site State', 'field': 'SiteState', width: 125, headerTooltip: 'Site State', ticked: true },
            { 'name': 'Edited By', 'field': 'EditedBy', width: 115, headerTooltip: 'Edited By', ticked: true },
            { 'name': 'Updated Time', 'field': 'UpdatedTime', width: 115, headerTooltip: 'Updated Time', ticked: true }
        ];
        /* jshint ignore:end */
    }

    getColumns() {
        return this.columns;
    }
}

export default LatestNodeStateChangesModel;