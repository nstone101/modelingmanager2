'use strict';

import AbstractModel from './abstract-model';
import {Service, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'FileModel'
})
@Inject('FileResource')
//end-non-standard
class FileModel extends AbstractModel {
    constructor(FileResource) {
        super(FileResource);
    }
}

export default FileModel;
