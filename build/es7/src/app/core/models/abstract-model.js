'use strict';

class AbstractModel {
    constructor(resource) {
        this.idProp = 'id';
        this.item = {};
        this.collection = [];
        this.resource = resource;
    }

    setUri(uri){
       this.resource.setUri(uri);
    }

    initItem(id) {
        if(id) {
            return this.resource.get(id)
                .then((resp) => {return resp.data;})
                .then((item) => {this.item = item;});
        } else {
            this.item = {};
            /*
             return and resolve helper promise to assure
             consistent API of method so that we can always
             use .then() method when calling initItem
             */
            return Promise.resolve();
        }
    }

    initCollection(params, cache) {
        return this.resource.getList(params, cache)
            .then(resp => {return resp.data;})
            .then((collection) => {
                this.collection = collection;
                return collection;
            });
    }

    getItemById(id) {
        return this.collection.find(item => item[this.idProp] === id);
    }

    getItem() {
        return this.item;
    }

    setItem(item) {
        this.item = item;
    }

    getCollection() {
        return this.collection;
    }

    setCollection(collection) {
        this.collection = collection;
    }

    save(item) {
        // update existing item if model contains id
        if (item[this.idProp]) {
            return this.resource.update(item).then(itemRespond => {
                for (let i = 0; i < this.collection.length; i++) {
                    if(this.collection[i][this.idProp] === itemRespond[this.idProp]) {
                        this.collection[i] = itemRespond;
                    }
                }
            });
        } else {
            return this.resource.create(item)
                .then((itemRespond) => {return itemRespond.data;})
                .then((itemRespond) => {
                    item[this.idProp] = itemRespond[this.idProp];
                    this.collection.push(item);
                });
        }
    }

    delete(item) {
        return this.resource.delete(item[this.idProp]).then(() => {
            this.collection.splice(this.collection.indexOf(item), 1);
        });
    }

    batchDelete(items) {
        let ids = _.map(items, (item) => {return item[this.idProp];});
        return this.resource.batchDelete(ids).then(() => {
            var self = this;
            _.remove(self.collection, (item) => {return _.where(items, {id: item[this.idProp]}).length === 1; });
        });
    }
}

export default AbstractModel;
