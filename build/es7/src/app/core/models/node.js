'use strict';

import AbstractModel from './abstract-model';
import {Service, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'NodeModel'
})
@Inject('NodeResource')
//end-non-standard
class NodeModel extends AbstractModel {
    constructor(NodeResource) {
        super(NodeResource);
        this.designDocs = undefined;
    }

    getNodeDocs(siteName) {
        return this.resource.getNodeDocs(siteName).then((resp)=>{return resp.data;});
    }
    
    getNodePhotos(siteName) {
        return this.resource.getNodePhotos(siteName).then((resp)=>{return resp.data;});
    }

    getNodeDiagram(siteName) {
        return this.resource.getNodeDiagram(siteName).then((resp)=>{return resp.data;});
    }

    getDesignDocs(gen){
        return this.resource.getDesignDocs(gen).then((resp)=>{this.designDocs = resp.data; return resp.data;});
    }

    getMops(type){
        if(this.designDocs === undefined) { return {doc: '', path: ''}; }
        var items = this.designDocs.docs[type].toString().split(',');
        var docs = items.map((item) => {
            return { doc: item.split('/').reverse()[0], path: item };
        });
        return docs;
    }

    getDocConfig(){
        /* jshint ignore:start */
        return [
            {
                "title": "ISPECs",
                "name": "ISPEC",
                "docType": "ispecs",
                "directory": "ispec",
                "logFile": "_ispec_audit.log"
            },
            {
                "title": "Layouts",
                "name": "Layout",
                "docType": "layouts",
                "directory": "layouts",
                "logFile": "_layouts_audit.log"
            },
            {
                "title": "Failovers",
                "name": "Failover",
                "docType": "failovers",
                "directory": "failovers",
                "logFile": "_failovers_audit.log"
            },
            {
                "title": "Site Visits",
                "name": "Site Visit",
                "docType": "sitevisits",
                "directory": "sitevisits",
                "logFile": "_sitevisit_audit.log"
            },
            {
                "title": "Miscellaneous items",
                "name": "Miscellaneous",
                "docType": "miscellaneous",
                "directory": "miscellaneous",
                "logFile": "_miscellaneous_audit.log"
            },
            {
                "title": "Config Files",
                "name": "Config",
                "docType": "configfiles",
                "directory": "configfiles",
                "logFile": "_configfile_audit.log"
            }
        ];
        /* jshint ignore:end */
    }
}

export default NodeModel;
