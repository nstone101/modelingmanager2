'use strict';

import AbstractModel from '../abstract-model';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'FwGoldenConfigModel'
})
@Inject('FwGoldenConfigResource')
//end-non-standard
class FwGoldenConfigModel extends AbstractModel {
    constructor(FwGoldenConfigResource) {
        super(FwGoldenConfigResource);
        /* jshint ignore:start */
        this.columns = {
            'audit': [
                { 'name': '', 'field': 'id',
                    cellTemplate: '<div class="cellTemplatePopUps" title="{{row.entity.ToolTip}}" data-html=true data-placement="right" ><i class="fa fa-info-circle"></i></div>', width: 30, ticked: true, enableColumnMenu: false },
                { 'name': 'Modify Time', 'field': 'UpdatedTime', width: 90, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'Action', 'field': 'Action', width: 90, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'SiteName', 'field': 'SiteName', width: 90, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'FNo', 'field': 'FNo', width: 250, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'FId', 'field': 'FId', width: 250, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'FromZone', 'field': 'FromZone', width: 125, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'Source', 'field': 'Source', width: 125, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'ToZone', 'field': 'ToZone', width: 125, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'Destination', 'field': 'Destination', width: 20, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'Service', 'field': 'Service', width: 20, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'FWAction', 'field': 'FWAction', width: 20, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'InstallOn', 'field': 'InstallOn', width: 20, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'RuleOptions', 'field': 'RuleOptions', width: 20, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" }
            ],
            'default': [
                { 'displayName': 'FNo', 'field': 'FNo', width: 200, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'displayName': 'FId', 'field': 'FId', width: 150, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'displayName': 'FomZone', 'field': 'FromZone', width: 150, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'displayName': 'Source', 'field': 'Source', width: 300, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'displayName': 'ToZone', 'field': 'ToZone', width: 200, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'displayName': 'Destination', 'field': 'Destination', width: 300, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'displayName': 'Service', 'field': 'Service', width: 200, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'displayName': 'FWAction', 'field': 'FWAction', width: 200, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'displayName': 'InstallOn', 'field': 'InstallOn', width: 200, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'displayName': 'RuleOptions', 'field': 'RuleOptions', width: 300, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'Data1', 'field': 'Data1', ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'Data2', 'field': 'Data2', ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'Data3', 'field': 'Data3', ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" }

            ]
        };
        /* jshint ignore:end */
    }

    getColumns(type) {
        type = type || 'default';
        return this.columns[type];
    }
}

export default FwGoldenConfigModel;
