'use strict';

import AbstractModel from '../abstract-model';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'NodeNotesModel'
})
@Inject('NodeNotesResource')
//end-non-standard
class NodeNotesModel extends AbstractModel {
    constructor(NodeNotesResource) {
        super(NodeNotesResource);
        /* jshint ignore:start */
        this.columns = {
            'audit': [
                { 'name': '', 'field': 'id',
                    cellTemplate: '<div class="cellTemplatePopUps" title="{{row.entity.ToolTip}}" data-html=true data-placement="right" ><i class="fa fa-info-circle"></i></div>', width: 30, ticked: true, enableColumnMenu: false },
                { 'name': 'Action', 'field': 'Action', width: 90, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'SiteName', 'field': 'SiteName', width: 100, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'displayName': 'NoteId', 'field': 'NoteId', width: 200, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'displayName': 'Note', 'field': 'Note', width: 200, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },

            ],
            'default': [
                { 'displayName': 'NoteId', 'field': 'NoteId', width: 200, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'displayName': 'Note', 'field': 'Note', width: 200, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'displayName': 'EditedBy', 'field': 'EditedBy', width: 300, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'Data1', 'field': 'Data1', ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'Data2', 'field': 'Data2', ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'Data3', 'field': 'Data3', ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" }

            ]
        };
        /* jshint ignore:end */
    }

    getColumns(type) {
        type = type || 'default';
        return this.columns[type];
    }
}

export default NodeNotesModel;
