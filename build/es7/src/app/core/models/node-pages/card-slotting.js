'use strict';

import AbstractModel from '../abstract-model';
import {Service, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'CardSlottingModel'
})
@Inject('CardSlottingResource')
//end-non-standard
class CardSlottingModel extends AbstractModel {
    constructor(CardSlottingResource) {
        super(CardSlottingResource);
        /* jshint ignore:start */
        this.columns = {
            'audit': [
                { 'name': '', 'field': 'id',
                    cellTemplate: '<div class="cellTemplatePopUps" title="{{row.entity.ToolTip}}" data-html=true data-placement="right" ><i class="fa fa-info-circle"></i></div>', width: 30, ticked: true, enableColumnMenu: false },
                { 'name': 'Modify Time', 'field': 'UpdatedTime', width: 90, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'Action', 'field': 'Action', width: 90, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'SiteName', 'field': 'SiteName', width: 90, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'Slot', 'field': 'Slot', width: 250, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'CardType', 'field': 'CardType', width: 250, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'InterfaceNum', 'field': 'InterfaceNum', width: 125, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'Device', 'field': 'Device', width: 125, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                { 'name': 'MIC', 'field': 'MIC', width: 125, ticked: true,
                    cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
            ],
            'default': [
                { 'name': 'Slot', 'field': 'Slot', width: 250, ticked: true },
                { 'name': 'CardType', 'field': 'CardType', width: 250, ticked: true },
                { 'name': 'InterfaceNum', 'field': 'InterfaceNum', width: 125, ticked: true },
                { 'name': 'Device', 'field': 'Device', width: 125, ticked: true },
                { 'name': 'MIC', 'field': 'MIC', width: 125, ticked: true },
                { 'name': 'Data1', 'field': 'Data1', ticked: true},
                { 'name': 'Data2', 'field': 'Data2', ticked: true},
                { 'name': 'Data3', 'field': 'Data3', ticked: true}

            ]
        };
        /* jshint ignore:end */
    }

    getColumns(type) {
        type = type || 'default';
        return this.columns[type];
    }
}

export default CardSlottingModel;
