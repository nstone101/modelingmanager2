'use strict';

import AbstractModel from './abstract-model';
import {Service, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'RoleModel'
})
@Inject('RoleResource')
//end-non-standard
class RoleModel extends AbstractModel {
    constructor(RoleResource) {
        super(RoleResource);

    }
}

export default RoleModel;
