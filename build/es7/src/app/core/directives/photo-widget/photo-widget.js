'use strict';

import template from './photo-widget.html!text';
import {Directive, Inject} from '../../../decorators'; // jshint unused: false
const WINDOW = new WeakMap();
const RESOURCE = new WeakMap();

//start-non-standard
@Directive({
    selector: 'photo-widget'
})
//end-non-standard
class PhotoWidget {
    constructor($window, NodeResource) {
        this.restrict = 'E';
        this.replace = true;
        this.template = template;
        this.scope = {
            nodeName: '=',
            photo: '=',
            isAdmin: '=',
        };
        WINDOW.set(this, $window);
        RESOURCE.set(this, NodeResource);
    }

    link(scope, element, attrs) {
        scope.basePhoto = scope.photo.replace(/^.*[\\\/]/, '');

        scope.config = {
            host: ''
        };
        if(WINDOW.get(PhotoWidget.instance).location.host.indexOf('localhost:8000') > -1) {
            scope.config.host = window.PORTAL_URLS.BASE;
        }
        var self = scope;
        scope.deletePhoto = (nodeName, photo) => {
            PhotoWidget.instance.deletePhoto(self, nodeName, photo);
        };
    }
    // TODO: (bpowell) inject this function through an api
    deletePhoto(scope, nodeName, photo){
        RESOURCE.get(PhotoWidget.instance).deleteNodePhoto({
            siteName: nodeName,
            logFile: '_photo_audit.log',
            filename: photo
        }).then((resp) => {
            scope.$root.$bus.publish('DZ_DELETED_FILE');
        });
    }

    //start-non-standard
    @Inject('$window', 'NodeResource')
    //end-non-standard
    static directiveFactory($window, NodeResource){
        PhotoWidget.instance = new PhotoWidget($window, NodeResource);
        return PhotoWidget.instance;
    }
}

export default PhotoWidget;