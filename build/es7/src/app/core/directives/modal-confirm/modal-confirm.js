/**
 * Modal Confirm Dialog
 *
 * API:
 * modal-confirm (fn -> void) the function to call when ok is pressed.
 * confirm-header (string) a string to display as the title
 * confirm-header-fn (fn -> string) a function to allow you to create the string to display as the title
 * confirm-message (string) dialog message
 * confirm-message-fn (fn -> string) function returning the message string
 * ok-label (string) if set, display the label and show the ok button, otherwise no Ok button
 * cancel-label (string) if set, display the label and show the cancel button, otherwise no Cancel button
 *
 * The confirm-* string and functions are mutually exclusive. No -fn then it looks for the confirm-message or title.
 * If the *-label are not set then it will not show the buttons.
 * There will always be an x in the top to cancel the dialog.
 */

'use strict';

import {Directive, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Inject('$uibModalInstance')
//end-non-standard
class ModalConfirmController {
    constructor($modalInstance) {
        this.modal = $modalInstance;
    }

    ok() {
        this.modal.close();
    }

    cancel() {
        this.modal.dismiss('cancel');
    }
}

const MODAL = new WeakMap();
//start-non-standard
@Directive({
    selector: 'modal-confirm'
})
//end-non-standard
class ModalConfirm {
    constructor($modal) {
        this.restrict = 'A';
        this.scope = {
            modalConfirm: '&'
        };
        MODAL.set(this, $modal);
    }

    link(scope, element, attrs) {

        var api = {
            okLabel: '',
            cancelLabel: '',
            header: '',
            headerFn: '',
            message: '',
            messageFn: ''
        };

        element.bind('click', (event) => {
            event.stopPropagation();

            let title = '';
            let message = '';
            let okLabel = attrs.okLabel;
            let cancelLabel = attrs.cancelLabel;
            let showCancel = (attrs.cancelLabel !== undefined);
            let showOk = (attrs.okLabel !== undefined);

            if(attrs.confirmHeaderFn !== undefined) {
                title = scope.$parent.$eval(attrs.confirmHeaderFn);
            }
            else {
                title = attrs.confirmHeader;
            }

            if(attrs.confirmMessageFn !== undefined){
                message = scope.$parent.$eval(attrs.confirmMessageFn);
            }
            else {
                message = attrs.confirmMessage;
            }

            if(!attrs.okLabel){
                showOk = false;
            }

            if(attrs.cancelLabel === undefined) {
                showCancel = false;
            }

            const modalInstance = MODAL.get(ModalConfirm.instance).open({
                template: `
                    <div class="modal-header">
                        <button type="button" class="close" ng-click="vm.cancel()"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title">${title}</h4>
                    </div>
                    <div class="modal-body">
                        <p>${message}</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-default" ng-click="vm.cancel()" ng-show="${showCancel}">${cancelLabel}</button>
                        <button class="btn btn-sm btn-success" ng-click="vm.ok()" ng-show="${showOk}">${okLabel}</button>
                    </div>
                `,
                controller: ModalConfirmController,
                controllerAs: 'vm'
            });

            modalInstance.result.then( () => scope.modalConfirm() );
        });
    }

    //start-non-standard
    @Inject('$uibModal')
    //end-non-standard
    static directiveFactory($modal){
        ModalConfirm.instance = new ModalConfirm($modal);
        return ModalConfirm.instance;
    }
}

export {ModalConfirm, ModalConfirmController};
