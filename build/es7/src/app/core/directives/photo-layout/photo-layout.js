'use strict';

import template from './photo-layout.html!text';
import {Directive, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Directive({
    selector: 'photo-layout'
})
//end-non-standard
class PhotoLayout {
    constructor() {
        this.restrict = 'E';
        this.replace = true;
        this.template = template;
        this.scope = {
            photos: '=',
            nodeName: '='
        };
    }

    link(scope, element, attrs) {
        scope.photoId = 0;
    }

    //start-non-standard
    @Inject()
    //end-non-standard
    static directiveFactory(){
        PhotoLayout.instance = new PhotoLayout();
        return PhotoLayout.instance;
    }
}

export default PhotoLayout;