'use strict';
import Dropzone from 'dropzone'; window.Dropzone = Dropzone;

import template from './dropzone.tpl.html!text';
import {Directive, Inject} from '../../../decorators'; // jshint unused: false

//start-non-standard
@Directive({
    selector: 'portal-dropzone'
})
//end-non-standard
class PortalDropzone {
    constructor(TokenModel) {
        this.restrict = 'C';
        this.scope = {
            config: '='
        };
        this.tokenModel = TokenModel;
    }

    link(scope, element, attrs) {
        scope.totalSize = 0;

        const token = PortalDropzone.instance.tokenModel.get();
        var authHeader = {};
        if (token) {
            authHeader.Authorization = 'Bearer ' + token;
        }

        var config = {
            url: '/api/nodes/docs/upload',
            maxFilesize: 20,
            maxFiles:20,
            maxThumbnailFilesize: 10,
            parallelUploads: 20,
            autoProcessQueue: false,
            uploadMultiple:true,
            createImageThumbnails: false,
            previewTemplate: template,
            clickable: '.add-file',
            dictDefaultMessage: 'Drop files here to upload',
            dictFileTooBig: 'File is over max filesize.',
            headers: {
                'Authorization': authHeader.Authorization
            }
        };

        let _config = Object.assign(config, scope.config);
        var dropzone = new Dropzone(element[0], _config);
        dropzone.autoDiscover = false;

        var eventHandlers = {
            'addedfile': (file) => {
                scope.file = file;
                scope.$evalAsync(() => {
                    scope.fileAdded = true;
                    scope.itemsInQueue = dropzone.getActiveFiles().length;
                    scope.totalSize += file.size;
                    scope.sizeMB = (scope.totalSize/1000/1000).toFixed(2);
                    scope.bucketStatus = 'Files in queue: '+ scope.itemsInQueue + ', ' + scope.sizeMB + 'MB';
                    scope.$root.$bus.publish('DZ_ADDED_FILE', {itemsInQueue: scope.itemsInQueue});
                });
            },
            'removedfile': (file) => {
                scope.$evalAsync(() => {
                    scope.itemsInQueue = dropzone.getActiveFiles().length;
                    scope.totalSize -= file.size;
                    scope.sizeMB = (scope.totalSize/1000/1000).toFixed(2);
                    scope.bucketStatus = 'Files in queue: '+ scope.itemsInQueue + ', ' + scope.sizeMB + 'MB';
                    scope.$root.$bus.publish('DZ_REMOVED_FILE', {itemsInQueue: scope.itemsInQueue});
                });
            },
            'uploadprogress': (file, progress, bytesSent) => {
                console.log(file +'-'+ progress +'-'+ bytesSent);
            },
            'sending': () => {
                scope.$evalAsync(() => {
                    scope.bucketStatus = 'Uploading ' + scope.itemsInQueue + ' files, ' + scope.sizeMB + 'MB';
                });
            },
            'totaluploadprogress': (totalUploadProgress, totalBytes, totalBytesSent) => {
                scope.bucketStatus = 'Uploaded '+ scope.itemsInQueue + ' files, ' + scope.sizeMB + 'MB';
                console.log(totalUploadProgress +'-'+ totalBytes +'-'+ totalBytesSent);
            },
            'success': (file, response) => {

            },
            'queuecomplete': () => {
                scope.$apply(() => {
                    scope.itemsInQueue = 0;
                    scope.$root.$bus.publish('DZ_QUEUE_COMPLETE', {itemsInQueue: scope.itemsInQueue});
                });
                scope.$root.$bus.publish('DZ_FILE_UPLOADED');
            },
            'error': function(){

            }
        };


        angular.forEach(eventHandlers, (handler, event) => {
            dropzone.on(event, handler);
        });

        scope.processDropzone = () => {
            dropzone.processQueue();
        };

        scope.resetDropzone = () => {
            dropzone.removeAllFiles();
            scope.totalSize = 0;
        };

        _config.onRegisterApi({processDropzone: scope.processDropzone, resetDropzone: scope.resetDropzone, itemsInQueue: scope.itemsInQueue});

    }

    //start-non-standard
    @Inject('TokenModel')
    //end-non-standard
    static directiveFactory(TokenModel){
        PortalDropzone.instance = new PortalDropzone(TokenModel);
        return PortalDropzone.instance;
    }

}

export default PortalDropzone;
