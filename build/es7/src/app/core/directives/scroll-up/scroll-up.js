'use strict';

import {Directive, Inject} from '../../../decorators'; // jshint unused: false

const LOCATION = new WeakMap();
const ANCHOR_SCROLL = new WeakMap();

//start-non-standard
@Directive({
    selector: 'scroll-up'
})
//end-non-standard
class ScrollUp {
    constructor($location, $anchorScroll) {
        this.restrict = 'A';
        LOCATION.set(this, $location);
        ANCHOR_SCROLL.set(this, $anchorScroll);
    }

    link(scope, element, attrs) {
        element.on('click', function() {
            LOCATION.get(ScrollUp.instance).hash(attrs.ScrollUp);
            ANCHOR_SCROLL.get(ScrollUp.instance)();
        });
    }

    //start-non-standard
    @Inject('$uibModal', '$anchorScroll')
    //end-non-standard
    static directiveFactory($location, $anchorScroll){
        ScrollUp.instance = new ScrollUp($location, $anchorScroll);
        return ScrollUp.instance;
    }
}

export default ScrollUp;
