'use strict';

import {Service, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'ModalService'
})
@Inject('$state')
//end-non-standard
class ModalService {
    constructor($state) {
        this.route = $state;
    }

    onSuccess() {
        return () => {};
    }

    onError($modal, state) {
        return (error) => {
            if(error.status) {
                $modal.open({
                    template: '<error-modal cancel="vm.cancel()" error="vm.error"></error-modal>',
                    controller: ['$uibModalInstance', function ($modalInstance) {
                        var vm = this;
                        vm.error = error;
                        vm.cancel = () => $modalInstance.dismiss('cancel');
                    }],
                    controllerAs: 'vm',
                    size: 'md'
                });//.result.finally(() => this.route.go(state));
            }
        };
    }

    onFinal(state, params) {
        if(params === undefined) {
        return () => this.route.go(state);
        }
        else {
            return () => this.route.go(state, params);
        }
    }
}

export default ModalService;
