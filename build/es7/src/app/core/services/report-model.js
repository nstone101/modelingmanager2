'use strict';

import {Service, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'ReportModelService'
})
@Inject('UtilityServerIpsModel', 'Mx960PortsRequiringScanningModel', 'LatestNodeStateChangesModel')
//end-non-standard
class ReportModelService {
    constructor(UtilityServerIpsModel, Mx960PortsRequiringScanningModel, LatestNodeStateChangesModel){
        this.models = {
            'UtilityServerIps': UtilityServerIpsModel,
            'Mx960PortsRequiringScanning': Mx960PortsRequiringScanningModel,
            'LatestNodeStateChanges': LatestNodeStateChangesModel
        };
    }

    getReportModel(title) {
        return this.models[title];
    }
}

export default ReportModelService;
