'use strict';

import {Service, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'NodePageModelService'
})
@Inject('AsyncModel', 'CardSlottingModel', 'CustomerAddressingModel', 'CustomerCircuitsModel', 'DnsTrapLogModel', 'FwGoldenConfigModel', 'FwSetupModel', 
        'NodeNotesModel', 'IpSchemaModel', 'Mx960PortAssignmentModel', 'IpPublicModel', 'IpIDNModel', 'IpPrivateModel', 
        'NetworkConnectionModel', 'NodeDeviceNamesModel', 'Ns5400PortAssignmentModel', 'PingPollerModel', 'SubnetAdvModel', 
        'SubnetInformationModel', 'TdrModel', 'VpnRoutingModel')
//end-non-standard
class NodePageModelService {
    constructor(AsyncModel, CardSlottingModel, CustomerAddressingModel, CustomerCircuitsModel, DnsTrapLogModel, FwGoldenConfigModel, FwSetupModel, 
                NodeNotesModel, IpSchemaModel, Mx960PortAssignmentModel, IpPublicModel, IpIDNModel, IpPrivateModel,
                NetworkConnectionModel, NodeDeviceNamesModel, Ns5400PortAssignmentModel, PingPollerModel, 
                SubnetInformationModel, SubnetAdvModel, TdrModel, VpnRoutingModel){
        this.models = {
            'Async': AsyncModel,
            'CardSlotting': CardSlottingModel,
            'CustomerAddressing': CustomerAddressingModel,
            'CustomerCircuits': CustomerCircuitsModel,
            'DnsTrapLog': DnsTrapLogModel,
            'FwGoldenConfig': FwGoldenConfigModel,
            'FwSetup': FwSetupModel,
            'NodeNotes': NodeNotesModel,
            'IpSchema': IpSchemaModel,
            'IpPublic': IpPublicModel,
            'IpPrivate': IpPrivateModel,
            'IpIDN': IpIDNModel,
            'Mx960PortAssignments': Mx960PortAssignmentModel,
            'NetConn': NetworkConnectionModel,
            'NodeDeviceNames': NodeDeviceNamesModel,
            'Ns5400PortAssignments': Ns5400PortAssignmentModel,
            'PingPoller': PingPollerModel,
            'SubnetAdvModel': SubnetAdvModel,
            'SubnetInfo': SubnetInformationModel,
            'Tdr': TdrModel,
            'VpnRouting': VpnRoutingModel
        };

        this.editorLinks = {
            'Async': 'portal.nodes.node.page-editor.async',
            'CardSlotting': 'portal.nodes.node.page-editor.card-slotting',
            'CustomerAddressing': 'portal.nodes.node.page-editor.customer-addressing',
            'CustomerCircuits': 'portal.nodes.node.page-editor.customer-circuits',
            'DnsTrapLog': 'portal.nodes.node.page-editor.dns-trap-log',
            'FwGoldenConfig': 'portal.nodes.node.page-editor.fw-golden-config',
            'FwSetup': 'portal.nodes.node.page-editor.fw-setup',
            'NodeNotes': 'portal.nodes.node.page-editor.node-notes',
            'IpSchema': 'portal.nodes.node.page-editor.ip-schema',
            'IpPublic': 'portal.nodes.node.page-editor.ip-public',
            'IpIDN': 'portal.nodes.node.page-editor.ip-idn',
            'IpPrivate': 'portal.nodes.node.page-editor.ip-private',
            'Mx960PortAssignments': 'portal.nodes.node.page-editor.mx960-port-assignments',
            'NetConn': 'portal.nodes.node.page-editor.network-connections',
            'NodeDeviceNames': 'portal.nodes.node.page-editor.node-device-names',
            'Ns5400PortAssignments': 'portal.nodes.node.page-editor.ns5400-port-assignments',
            'PingPoller': 'portal.nodes.node.page-editor.ping-poller',
            'SubnetAdvModel': 'portal.nodes.node.page-editor.subnet-advertising',
            'SubnetInfo': 'portal.nodes.node.page-editor.subnet-information',
            'Tdr': 'portal.nodes.node.page-editor.tdr',
            'VpnRouting': 'portal.nodes.node.page-editor.vpn-routing'
        };

        this.auditUris = {
            'Async': 'api/reports/node-data/async',
            'CardSlotting': 'api/reports/node-data/card-slotting',
            'CustomerAddressing': 'api/reports/node-data/customer-addressing',
            'CustomerCircuits': 'api/reports/node-data/customer-circuits',
            'DnsTrapLog': 'api/reports/node-data/dns-trap-log',
            'FwGoldenConfig': 'api/reports/node-data/fw-golden-config',
            'FwSetup': 'api/reports/node-data/fw-setup',
            'NodeNotes': 'api/reports/node-data/.node-notes',
            'IpSchema': 'api/reports/node-data/ip-schema',
            'IpPublic': 'api/reports/node-data/ip-public',
            'IpIDN': 'api/reports/node-data/ip-idn',
            'IpPrivate': 'api/reports/node-data/ip-private',
            'Mx960PortAssignments': 'api/reports/node-data/mx960-port-assignments',
            'NetConn': 'api/reports/node-data/network-connections',
            'NodeDeviceNames': 'api/reports/node-data/node-device-names',
            'Ns5400PortAssignments': 'api/reports/node-data/ns5400-port-assignments',
            'PingPoller': 'api/reports/node-data/ping-poller',
            'SubnetAdvModel': 'api/reports/node-data/subnet-advertising',
            'SubnetInfo': 'api/reports/node-data/subnet-information',
            'Tdr': 'api/reports/node-data/tdr',
            'VpnRouting': 'api/reports/node-data/vpn-routing'
        };
    }

    getNodePageModel(title, isAudit) {
        var model =  this.models[title];
        if(isAudit === true){
            model.setUri(this.auditUris[title]);
        }
        return model;
    }

    getEditorLink(title){
        return this.editorLinks[title];
    }
}

export default NodePageModelService;
