'use strict';

import 'pdfmake';
import vfs from 'pdfmake/vfs_fonts';
import {Service, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'GridService'
})
@Inject('$rootScope', 'uiGridConstants', 'uiGridExporterConstants')
//end-non-standard
class GridService {
    constructor($rootScope, uiGridConstants, uiGridExporterConstants) {
        console.log(vfs);
        this.gridApi = {};
        this.gridButtons = {
            'selected': '1'
        };

        this.$rootScope = $rootScope;
        if ($rootScope.currentUser.role === '1') {
               this.writeuser = false ;
        } else {
               this.writeuser = true;
        }

        this.selectedRows = {};
        this.multiSelectColumns = {};
        this.shownColumns = {};
        this.searchFilter = '';
        this.gridSearchFilter = '';
        this.uiGridConstants = uiGridConstants;
        this.uiGridExporterConstants = uiGridExporterConstants;
        this.defaults = {
            searchFilter: '',
            enableSorting: true,
            enableRowSelection: ($rootScope.currentUser.role === '1' ? false : true),
            enableSelectionBatchEvent: true,
            enableFullRowSelection: true,
            treeRowHeaderAlwaysVisible: true,
            multiSelect: true,
            enableSelectAll: ($rootScope.currentUser.role === '1' ? false : true),
            selectionRowHeaderWidth: 35,
            paginationPageSizes: [10, 20, 50, 75],
            paginationPageSize: 10,
            columnDefs: {},
            exporterLinkLabel: '',
            exporterPdfDefaultStyle: {fontSize: 9},
            exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
            exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
            exporterPdfOrientation: 'landscape',
            exporterPdfPageSize: 'LETTER',
            exporterPdfMaxGridWidth: 1400
        };

        this.localLang = {
            selectAll: 'Tick all',
            selectNone: 'Tick none',
            reset: 'Undo all',
            search: 'Type here to search...',
            nothingSelected: 'Nothing is selected'         //default-label is deprecated and replaced with this.
        };
    }

    init(options, $scope) {
        this.options = _.defaults(options, this.defaults);
        this.setOptions(options, $scope);
    }

    setOptions(options, $scope) {
         this.options.onRegisterApi = (gridApi) => {
            this.gridApi = gridApi;
            this.gridApi.grid.registerRowsProcessor(this.filterColumns, 200);

            this.gridApi.core.on.columnVisibilityChanged($scope, (column) => {
                this.toggleSelectColumns(column);
            });

            if(this.gridApi.selection) {
                this.gridApi.selection.on.rowSelectionChanged($scope, (row) => {
                    var selections = this.gridApi.selection.getSelectedRows();
                    this.selectedRows = selections;
                    this.updateEditDeleteButtons(selections.length);
                });

                this.gridApi.selection.on.rowSelectionChangedBatch($scope, (rows) => {
                    var selections = this.gridApi.selection.getSelectedRows();
                    this.selectedRows = selections;
                    this.updateEditDeleteButtons(selections.length);
                });
            }
        };
    }

    setData(data){
        this.options.data = data;
        this.gridApi.core.notifyDataChange(this.uiGridConstants.dataChange.ALL);
        this.updateEditDeleteButtons(0);
    }

    setColumns(columns) {

    }

    filter(){
        this.gridApi.grid.refresh();
    }

    debounceFilter(){
        return _.debounce(()=>this.filter, 250);
    }

    filterColumns(renderableRows){
        var matcher = new RegExp(this.options.searchFilter);
        renderableRows.forEach( (row) => {
            var match = false;
            this.options.columnDefs.forEach( (column) => {
                var field = column.field;
                if (row.entity[field] && row.entity[field].match(matcher)) {
                    match = true;
                }
            });

            if (!match) {
                row.visible = false;
            }
            else {
                row.visible = true;
            }
        });

        return renderableRows;
    }

    toggleGridColumns(data){
        let index = _.findIndex(this.options.columnDefs, {field: data.field});
        this.options.columnDefs[index].visible = data.ticked;
        this.gridApi.core.notifyDataChange(this.uiGridConstants.dataChange.COLUMN);
    }
    
    toggleSelectColumns(data){
        let index = _.findIndex(this.options.columnDefs, {field: data.field});
        this.options.columnDefs[index].ticked = data.visible;
    }

    csvExporter() {
        let csvLink = angular.element(document.querySelectorAll('.custom-csv-link-location'));
        this.gridApi.exporter.csvExport(this.uiGridExporterConstants.ALL, this.uiGridExporterConstants.ALL, csvLink);
    }

    pdfExporter() {
        this.gridApi.exporter.pdfExport(this.uiGridExporterConstants.ALL, this.uiGridExporterConstants.ALL);
    }

    updateEditDeleteButtons(length) {
        if (length === 1) {
            this.displayDeleteBtn = true;
            this.displayEditBtn = true;
        }
        else if (length > 1) {
            this.displayDeleteBtn = true;
            this.displayEditBtn = false;
        }
        else if (length < 1) {
            this.displayDeleteBtn = false;
            this.displayEditBtn = false;
        }
    }
}

export default GridService;
