'use strict';

import {Service, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'FormService'
})
//end-non-standard
class FormService {
    constructor() {
        this.saveButtonOptions = {
            iconsPosition: 'right',
            buttonDefaultText: 'Save',
            buttonSubmittingText: 'Saving',
            buttonSuccessText: 'Saved'
        };
    }

    getModalSaveButtonOptions() {
        this.saveButtonOptions.animationCompleteTime = '0';
        this.saveButtonOptions.buttonErrorClass = 'btn-success';

        return this.saveButtonOptions;
    }

    getSaveButtonOptions() {
        this.saveButtonOptions.animationCompleteTime = '1200';
        this.saveButtonOptions.buttonErrorClass = 'btn-danger';

        return this.saveButtonOptions;
    }

    onSuccess(self) {
        self.result = 'success';
        self.hasError = false;
        self.passwordError = '';
        self.usernameError = '';
        self.errorMessage = '';
        if(typeof self.cancel === 'function') {
            self.cancel();
        }
    }

    onFailure(self, response) {
        self.result = 'error';
        self.hasError = true;
        self.hasPwError = false;
        self.hasUsernameError = true;
        self.passwordError = '';
        self.usernameError = '';
        self.errorMessage = '';

        if(response.data.message.indexOf('password') > -1) {
            self.hasPwError = true;
            self.hasUsernameError = false;
            self.passwordError = response.data.message;
        }
        else {
            self.hasPwError = false;
            self.hasUsernameError = true;
            self.usernameError = response.data.message;
        }

        if(response.status === 401) {
            self.errorMessage = 'Not Authorized.';
        }
        else if(response.status === 404) {
            self.errorMessage = 'The requested record could not be found.';
        }
        else if(response.status === 409 && response.config.method === 'PUT') {
            self.errorMessage = 'Another user has updated this record while you were editing. Please reload the page and try again.';
        }
        else if(response.status === 409 && response.config.method === 'POST') {
            self.errorMessage = 'This record already exist.';
        }
        else {
            let action;
            switch(response.config.method) {
                case 'POST':
                    action = 'created';
                    break;
                case 'PUT':
                    action = 'updated';
                    break;
                case 'DELETE':
                    action = 'deleted';
                    break;
            }
            self.errorMessage = `This record could not be ${action}. Please try again.`;
        }
        self.errorMessage = response.data.message;
    }

    save(model, item, self, form) {
        return model.save(item).then(() => {
            this.onSuccess(self);
        }, (response) => {
            this.onFailure(self, response);
        }).finally(() => {
            form.$setPristine();
        });
    }

    delete(model, item, self) {
        return model.delete(item).then(() => {
            this.onSuccess(self);
        },(response) => {
            this.onFailure(self, response);
        });
    }
    /**
     * we are using ui-router for add wizard form so there might be possibility that user can jump to 'complete' state through URL
     * so we need to make sure that child form exist
     *
     */
    hasInvalidChildForms(router, formSteps) {
        const invalidForm = formSteps.find((form) => !form.valid);
        if(invalidForm) {
            router.go(invalidForm.route);
        }

        return invalidForm;
    }
}

export default FormService;
