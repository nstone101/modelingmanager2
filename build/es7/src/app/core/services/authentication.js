'use strict';

import {Service, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'AuthenticationService'
})
@Inject('AuthenticationResource', 'TokenModel')
//end-non-standard
class AuthenticationService {
    constructor(AuthenticationResource, TokenModel) {
        this.TokenModel = TokenModel;
        this.AuthenticationResource = AuthenticationResource;
    }

    login(credentials) {
        return this.AuthenticationResource.login(credentials).then((response) => {
            this.TokenModel.set(response.data.token);
        });
    }

    logout() {
        return this.AuthenticationResource.logout().then(() => {
            this.TokenModel.remove();
        });
    }
    verifyEmail(credentials) {
        return this.AuthenticationResource.verifyEmail(credentials).then((response) => {return response;});
    }

    resetPassword(credentials) {
        return this.AuthenticationResource.resetPassword(credentials).then((response) => {return response;});
    }

    updatePassword(credentials) {
        return this.AuthenticationResource.updatePassword(credentials).then((response) => {return response;});
    }
    isAuthorized(accessLevels) {
        if(accessLevels.indexOf('*') !== -1) {
            return true;
        }
        return (this.isAuthenticated() && accessLevels.indexOf(this.getCurrentUser().role) !== -1);
    }

    isAuthenticated() {
        return !!this.TokenModel.get();
    }

    getCurrentUser() {
        return this.TokenModel.getCurrentUser();
    }
}

export default AuthenticationService;
