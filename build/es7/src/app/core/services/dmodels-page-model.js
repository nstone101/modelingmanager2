'use strict';

import {Service, Inject} from '../../decorators'; // jshint unused: false

//start-non-standard
@Service({
    serviceName: 'DModelPageModelService'
})
@Inject('CardModel')
//end-non-standard
class DModelsPageModelService {
    constructor(CardModel){
        this.models = {
            'Model_ICard': CardModel
        };

        this.editorLinks = {
            'Model_ICard': 'portal.modeling.dmodels.page-editor.card'
        };
    console.log('DModelPageModelService in services');
    }

    getDModelsPageModel(title) {
        var model =  this.models[title];
        return model;
    }

    getEditorLink(title){
        return this.editorLinks[title];
    }
}

export default DModelsPageModelService;
