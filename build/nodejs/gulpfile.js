var gulp = require('gulp');
var gulps = require('gulp-load-plugins')();
var protractor = require('gulp-protractor').protractor;
gulps.mainBowerFiles = require('main-bower-files');
gulps.rt = require('gulp-react-templates');

var chalk = require('chalk');
var path = require('path');
var del = require('del');
var merge = require('merge-stream');
var runSequence = require('run-sequence');
var pipe = require('multipipe');
var fs = require('fs');
var karma = require('karma').server;

var config = require('./config.json');

function remove(location) {
    del.sync(location, { force: true });
}

function deleteJsBundle(location, key) {
    deleteBundle(location, "js", key);
}

function deleteCssBundle(location, key) {
    deleteBundle(location, "css", key);
}

function deleteBundle(location, extension, key) {
    var fileToDelete = path.join(location, key + config.bundles.suffix + "*." + extension);
    remove(fileToDelete);
}

function getJsBundleFileName(key) {
    return getBundleFileName(key, "js");
}

function getCssBundleFileName(key) {
    return getBundleFileName(key, "css");
}

function getBundleFileName(key, extension) {
    return key + config.bundles.suffix + '.' + extension;
}

// ********************* Stream build functions *********************
function getAssets(options) {
    options = options || {};

    var dest = options.publish ? config.public : config.build;

    var streams = [];

    var images = pipe(
        gulp.src(config.src.cssimages),
        gulps.flatten(),
        gulp.dest(dest.cssimages)
    );

    var fonts = pipe(
        gulp.src(config.src.cssfonts),
        gulps.flatten(),
        gulp.dest(dest.cssfonts)
    );

    if (config.src.kendoBootstrapImages) {
        var kendo = pipe(
            gulp.src(config.src.kendoBootstrapImages),
            gulps.flatten(),
            gulp.dest(dest.kendoBootstrapImages)
        );

        streams.push(kendo);
    }

    streams.push(images);
    streams.push(fonts);

    return streams;
}

function buildCssBundles(options) {
    options = options || {};

    var filterGlob = options.publish
        ? '**/*.min.css'
        : ['**/*.css', '!**/*.min.css'];

    var minFilter;

    return Object.keys(config.bundles.css).map(function (key) {

        if (!options.publish)
            deleteCssBundle(config.build.css, key);

        minFilter = gulps.filter(filterGlob);

        var bundleFileName = getCssBundleFileName(key);

        return pipe(
            gulp.src(config.bundles.css[key]),
            gulps.plumber(),
            minFilter,
            gulps.sourcemaps.init(),
            gulps.concat(bundleFileName),
            //gulps.rev(),
            gulps.sourcemaps.write('maps'),
			gulps.size({ title: bundleFileName }),
            gulp.dest(options.publish ? config.public.css : config.build.css)
        );
    });
}

function buildCssApp(options) {
    options = options || {};
    var bundleFileName = getCssBundleFileName('app');

    return pipe(
        gulp.src(config.src.css),
        gulps.plumber(),
        gulps.sourcemaps.init(),
        gulps.concat(bundleFileName),
        gulps.less(),
        gulps.if(options.publish, gulps.minifyCss()),
        //gulps.rev(),
        gulps.sourcemaps.write('maps'),
        gulps.size({ title: bundleFileName }),
        gulp.dest(options.publish ? config.public.css : config.build.css)
    );
}

function buildJsBundles(options) {
    options = options || {};

    var filterGlob = options.publish ? '**/*.min.js' : ['**/*.js', '!**/*.min.js'];

    var minFilter;

    return Object.keys(config.bundles.js).map(function (key) {

        if (!options.publish)
            deleteJsBundle(config.build.js, key);

        var bundleFileName = getJsBundleFileName(key);

        minFilter = gulps.filter(filterGlob);

        return pipe(
            gulp.src(config.bundles.js[key]),
            minFilter,
            gulps.plumber(),
            gulps.sourcemaps.init(),
            gulps.concat(bundleFileName, { newLine: ';' }),
            //gulps.rev(),
            gulps.sourcemaps.write('maps'),
			gulps.size({ title: bundleFileName }),
	        gulp.dest(options.publish ? config.public.js : config.build.js)
        );
    });
}

function buildJsApp(options) {
    options = options || {};

    var appBundleFileName = getJsBundleFileName('app');

    var htmlFilter = gulps.filter("**/*.html");
    var rtFilter = gulps.filter("**/*.rt");

    return pipe(
        gulp.src(config.src.js),
        gulps.plumber(),
        rtFilter,
        gulps.rt({modules: 'none'}),
        rtFilter.restore(),
        gulps.sourcemaps.init(),
        gulps.concat(appBundleFileName),
        //gulps.jsx(),
        gulps.babel(),
        gulps.if(!options.publish, gulp.dest(config.build.app)),
        gulps.if(options.publish, gulps.ngAnnotate()),
        gulps.if(options.useCachedTemplates, gulps.addSrc.append(config.build.html + "/*.*")),
        gulps.if(!options.useCachedTemplates, gulps.addSrc.append(config.src.html)),
            htmlFilter,
            gulps.angularTemplatecache({ module: 'app' }),
            gulps.if(!options.publish, gulp.dest(config.build.html)),
            htmlFilter.restore(),                
        gulps.if(options.publish, gulps.uglify({
            mangle: true,
            warnings: false,
            compress: {
                drop_console: true,
                drop_debugger: true
            }
        })),
        gulps.concat(appBundleFileName),        
        //gulps.rev(),
        gulps.sourcemaps.write('maps'),
        gulps.size({ title: appBundleFileName }),
        gulp.dest(options.publish ? config.public.js : config.build.js)
    );
}

function buildBower(src, dest){
    var jsFilter = gulps.filter(['**/*.js', '!**/*.{min,min.copy}.js']);
    var cssFilter = gulps.filter(['**/*.css', '!**/*.{min,min.copy}.css']);
    var minFilter = gulps.filter(['**/*min.{css,js}']);

    return pipe(
            gulp.src(gulps.mainBowerFiles(), { base: src }),
            gulps.plumber(),
            gulp.dest(dest),
            minFilter,
            gulps.rename({ suffix: '.copy' }),
            minFilter.restore(),
            jsFilter,
            gulps.uglify({ mangle: true, warnings: false }),
            gulps.rename({ suffix: '.min' }),
            jsFilter.restore(),
            cssFilter,
            gulps.minifyCss(),
            gulps.rename({ suffix: '.min' }),
            cssFilter.restore(),
            gulp.dest(dest)
        );
}

gulp.task('minJsFiles', function (done) {
    var jsFilter = gulps.filter(['**/*.js', '!**/*.{min,min.copy}.js']);

    return pipe(
        gulp.src('../../src/client/lib/*.js'),
        gulps.plumber(),
        gulps.uglify({ mangle: true, warnings: false }),
        gulps.rename({ suffix: '.min' }),
        gulp.dest('../../src/client/lib')
    );
    done();
});

gulp.task('clean-build', function (done) {
    remove(path.join(config.build.root, '**'));
    done();
});

gulp.task('clean-public', function (done) {
    remove(config.public.js);
    remove(config.public.css);
    remove(config.public.images);
    remove(config.public.fonts);
    done();
});


gulp.task('build-fonts', function () {
    return pipe(
        gulp.src(config.src.fonts),
        gulps.flatten(),
        gulp.dest(config.build.fonts)
    );
});

gulp.task('deploy-fonts', function () {
    return gulp.src(config.build.fonts + "/**/*.*")
        .pipe(gulp.dest(config.public.fonts));
});

gulp.task('build-images', function () {
    return pipe(
        gulp.src(config.src.images),
        gulps.flatten(),
        gulp.dest(config.build.images)
    );
});

gulp.task('deploy-images', function () {
    return gulp.src(config.build.images + "/**/*.*")
        .pipe(gulp.dest(config.public.images));
});

gulp.task('build-assets', ['build-fonts', 'build-images']);
gulp.task('deploy-assets', ['deploy-fonts', 'deploy-images']);


gulp.task('build-css-app', function () {

    deleteCssBundle(config.build.css, 'app');

    return buildCssApp();
});

gulp.task('build-css-bundles', function () {
    return merge(
        getAssets(),
        buildCssBundles()
    );
});

gulp.task('deploy-css', function () {

    var images = gulp.src(config.build.cssimages + "/**/*.*")
        .pipe(gulp.dest(config.public.cssimages));

    var fonts = gulp.src(config.build.cssfonts + "/**/*.*")
        .pipe(gulp.dest(config.public.cssfonts));

    var css = gulp.src(config.build.css + "/**/*.*")
        .pipe(gulp.dest(config.public.css));

    var streams = [];

    if (config.src.kendoBootstrapImages) {
        var kendo = gulp.src(config.build.kendoBootstrapImages + "/**/*.*")
            .pipe(gulp.dest(config.public.kendoBootstrapImages));
        streams.push(kendo);
    }

    streams.push(images);
    streams.push(fonts);
    streams.push(css);

    return merge(streams);
});

gulp.task('publish-css', function () {
    return merge(
        getAssets({ publish: true }),
        buildCssApp({ publish: true }),
        buildCssBundles({ publish: true })
    );
});

gulp.task('build-js-app', function () {

    deleteJsBundle(config.build.js, "app");

    return buildJsApp();
});

gulp.task('rebuild-js-app', function () {

    deleteJsBundle(config.build.js, "app");

    return buildJsApp({useCachedTemplates: true});
});

gulp.task('build-js-bundles', function () {
    return merge(buildJsBundles());
});

gulp.task('deploy-js', function () {
    return gulp.src(config.build.js + "/**/*.*")
        .pipe(gulp.dest(config.public.js));
});

gulp.task('publish-js', function () {
    return merge(
        buildJsApp({ publish: true }),
        buildJsBundles({publish: true})
    );
});

gulp.task('rebuild-html', function(){

    deleteJsBundle(config.build.js, "app");

    var appBundleFileName = getJsBundleFileName('app');

    var htmlFilter = gulps.filter("**/*.html");

    return pipe(
        gulp.src(config.build.app + "/*.*"),
        gulps.addSrc.append(config.src.html),
        htmlFilter,
            gulps.angularTemplatecache({ module: 'app' }),
            gulp.dest(config.build.html),
            htmlFilter.restore(),
        gulps.concat(appBundleFileName),
        gulps.rev(),
        gulp.dest(config.build.js)
    );
});

// ****************** PUBLIC API ******************

gulp.task('lint', function () {
    return pipe(
        gulp.src(config.lint),
		gulps.plumber(),
        //gulps.cached('scripts'),
        gulps.jshint('.jshintrc'),
        gulps.jshint.reporter('jshint-stylish')
    );
});

gulp.task('ui-tests', function (cb) {
    
    remove(config.protractor.dest);

    var specFilter = gulps.filter('**/*TestSpec.js');

    return pipe(
        gulp.src(config.protractor.files),
        gulps.flatten(),
        gulps.babel(),
        gulp.dest(config.protractor.dest),
        specFilter,
        protractor({ configFile: config.protractor.config })
    );
});

gulp.task('e2e-test', function(cb) {
    karma.start({
        configFile: '',
        singleRun: true
    }, cb);
});

gulp.task('default', ['watch']);

gulp.task('watch', ['build'], function () {

    var lintWatch = gulp.watch(config.lint, ['lint']);

    //lintWatch.on('change', function (event) {
    //    if (event.type === 'deleted') { // if a file is deleted, forget about it
    //        delete gulps.cached.caches.scripts[event.path]; // gulp-cached remove api
    //        //gulps.remember.forget('scripts', event.path); // gulp-remember remove api
    //    }
    //});

    gulps.livereload.listen();

    var jsWatch = gulp.watch(config.src.js, ['reload:js']);
    var htmlWatch = gulp.watch(config.src.html, ['reload:html']);
    var cssWatch = gulp.watch(config.src.css, ['reload:css']);

    jsWatch.on('change', changed);
    htmlWatch.on('change', changed);
    cssWatch.on('change', changed);

    function changed(event) {
        console.log(chalk.yellow(event.path + " " + event.type));
    }
});

gulp.task('reload:js', ['rebuild:js'], function (done) {
    gulps.livereload.changed("js");
    done();
});

gulp.task('reload:css', ['rebuild:css'], function (done) {
    gulps.livereload.changed("css");
    done();
});

gulp.task('reload:html', ['rebuild:html'], function (done) {
    gulps.livereload.changed("html");
    done();
});


gulp.task('bower-install', function () {
    return gulps.bower()
        .pipe(gulp.dest('./bower_components'));
});

gulp.task('bower', ['bower-install'], function () {
    
    remove(config.bower.dest);

    return buildBower(config.bower.src, config.bower.dest);
});

gulp.task('build', function(done) {

    runSequence(
        ['clean-build', 'clean-public'],
        ['build-js-app', 'build-css-app', 'build-js-bundles', 'build-css-bundles', 'build-assets'],
        ['deploy-js', 'deploy-css', 'deploy-assets'],
        done);
});

gulp.task('rebuild:html', function(done){
    deleteJsBundle(config.public.js, "app");

    runSequence(
        'rebuild-html',
        'deploy-js',
        done);
});

gulp.task('rebuild:js', function(done) {

    deleteJsBundle(config.public.js, "app");

    runSequence(
        'rebuild-js-app',
        'deploy-js',
        done);
});

gulp.task('rebuild:css', function (done) {

    deleteCssBundle(config.public.css, "app");

    runSequence(
        'build-css-app',
        'deploy-css',
        done);
});

gulp.task('publish', function (done) {

    runSequence(
        'clean-public',
        'build-assets',
        'deploy-assets',
        ['publish-js', 'publish-css'],
        done);
});