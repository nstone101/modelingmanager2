<?php
// cli-config.php
require_once "bootstrap.php";

$config = new \Doctrine\ORM\Configuration();
$driver = new \Doctrine\ORM\Mapping\Driver\XmlDriver(array(__DIR__ . '/Entities2'));
$driver->setFileExtension('.xml');
$config->setMetadataDriverImpl($driver);