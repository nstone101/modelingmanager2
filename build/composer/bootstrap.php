<?php
// bootstrap.php
/*use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;*/

require_once "vendor/autoload.php";

// Create a simple "default" Doctrine ORM configuration for Annotations
/*$paths = array(__DIR__ . '/../../lib/Portal/classes/Models/Doctrine');
$isDevMode = true;

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);*/

// or if you prefer yaml or XML
//$config = Setup::createXMLMetadataConfiguration(array(__DIR__."/config/xml"), $isDevMode);
//$config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/config/yaml"), $isDevMode);


/*define('DB_USER',"root");
define('DB_PASS',"Juniper1234");
define('DB_HOST',"BRADPOWELLAC19");
define('DB_NAME',"VZDB");
define('DB_NAME_AUDIT',"portal_audit");*/

// database configuration parameters
/*$conn = array(
    'driver' => 'pdo_mysql',
    'dbname' => 'Test',
    'host' => 'BRADPOWELLAC19',
    'user' => 'root',
    'password' => 'Juniper1234'
);

// obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);*/

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Annotations\AnnotationReader;

$paths = array(realpath(__DIR__ . 'Entities2'));
//$paths = array(realpath(__DIR__ . '/Models'));

$isDevMode = true;

// the connection configuration
$conn = array(
    'driver' => 'pdo_mysql',
    'dbname' => 'test2',
    'host' => 'BRADPOWELLAC19',
    'user' => 'root',
    'password' => 'Juniper1234'
);

$cache = new \Doctrine\Common\Cache\ArrayCache();

$reader = new AnnotationReader();
$driver = new \Doctrine\ORM\Mapping\Driver\AnnotationDriver($reader, $paths);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null,false);
$config->setMetadataCacheImpl( $cache );
$config->setQueryCacheImpl( $cache );
$config->setMetadataDriverImpl( $driver );

$entityManager = EntityManager::create($conn, $config);

//-- This I had to add to support the Mysql enum type.
$platform = $entityManager->getConnection()->getDatabasePlatform();
$platform->registerDoctrineTypeMapping('enum', 'string');
