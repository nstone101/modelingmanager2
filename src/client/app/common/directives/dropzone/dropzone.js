(function() {
	'use strict';
	angular.module('commonModule')
		.directive('dropzone', function($rootScope, $templateCache) {
			return {
				restrict: 'C',
				link: function(scope, element, attrs) {

					scope.totalSize = 0;

					var dropZoneTemplate = $templateCache.get('common/directives/dropzone/dropzone.tpl.html');

					var config = {
						url: '/node/docs/upload',
						maxFilesize: 20,
						maxFiles:20,
						maxThumbnailFilesize: 10,
						parallelUploads: 20,
						autoProcessQueue: false,
						uploadMultiple:true,
						createImageThumbnails: false,
						previewTemplate: dropZoneTemplate,
						clickable: '.add-file',
						dictDefaultMessage: 'Drop files here to upload',
						dictFileTooBig: "File is over max filesize."
					};

					var eventHandlers = {
						'addedfile': function(file) {
							scope.file = file;
							scope.$evalAsync(function() {
								scope.fileAdded = true;
								scope.itemsInQueue = dropzone.getActiveFiles().length;
								scope.totalSize += file.size;
								scope.sizeMB = (scope.totalSize/1000/1000).toFixed(2);
								scope.bucketStatus = 'Files in queue: '+ scope.itemsInQueue + ', ' + scope.sizeMB + 'MB';
							});
						},
						'removedfile': function(file){
							scope.$evalAsync(function() {
								scope.itemsInQueue = dropzone.getActiveFiles().length;
								scope.totalSize -= file.size;
								scope.sizeMB = (scope.totalSize/1000/1000).toFixed(2);
								scope.bucketStatus = 'Files in queue: '+ scope.itemsInQueue + ', ' + scope.sizeMB + 'MB';
							});
						},
						'uploadprogress': function(file, progress, bytesSent){
							console.log(file +"-"+ progress +"-"+ bytesSent);
						},
						'sending': function(){
							scope.$evalAsync(function() {
								scope.bucketStatus = 'Uploading ' + scope.itemsInQueue + ' files, ' + scope.sizeMB + 'MB';
							});
						},
						'totaluploadprogress': function(totalUploadProgress, totalBytes, totalBytesSent){
							scope.bucketStatus = 'Uploaded '+ scope.itemsInQueue + ' files, ' + scope.sizeMB + 'MB';
							console.log(totalUploadProgress +"-"+ totalBytes +"-"+ totalBytesSent);
						},
						'success': function (file, response) {

						},
						'queuecomplete': function() {
							scope.$apply(function() {
								scope.itemsInQueue = 0;
							});
							$rootScope.$emit('fileAdded');
						},
						'error': function(){

						}
					};

					var dropzone = new Dropzone(element[0], config);
					dropzone.autoDiscover = false;
					angular.forEach(eventHandlers, function(handler, event) {
						dropzone.on(event, handler);
					});

					scope.processDropzone = function() {
						dropzone.processQueue();
					};

					scope.resetDropzone = function() {
						dropzone.removeAllFiles();
						scope.totalSize = 0;
					};
				}
			}
		})
}(angular || {}));