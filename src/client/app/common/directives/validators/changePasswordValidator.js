//Validate Change password screen matching user password and hash 
angular.module('adminUsersModule').directive('changePasswordValidator', function($http, $q) {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            ngModel.$asyncValidators.password = function(modelValue, viewValue) {
                return $http.post('/api/changePasswordValidator', {username: ngModel.username, confirmPassword: viewValue}).then(
                    function(response) {
                        if (response.data.status != 'ok') {
                            return $q.reject(response.data.message);
                        }
                        return true;
                    }
                );
            };
        }
    };
});
  