angular.module('adminUsersModule').directive('emailValidator', function($http, $q) {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            if(scope.vm.state == 'create') {
            ngModel.$asyncValidators.email = function(modelValue, viewValue) {
                
                    return $http.post('/api/validators/email', {"email": viewValue}).then(
                        function(response) {
                            if (!response || response.data.status != 'ok') {
                                return $q.reject(response.data.message);
                            }
                            return true;
                        });
                
                };
            }
        }
    };
});