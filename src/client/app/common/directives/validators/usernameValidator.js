angular.module('adminUsersModule')
    .directive('usernameValidator', function($http, $q) {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel) {
                if(scope.vm.state == 'create') {
                    ngModel.$asyncValidators.username = function(modelValue, viewValue) {
                        
                            return $http.post('/api/validators/username', {"username": viewValue}).then(
                                function(response) {
                                    if (response.data.status != 'ok') {
                                        //ngModel.$setValidity("usernameValidator", false);
                                        return $q.reject(response.data.message);
                                    }
                                    //ngModel.$setValidity("usernameValidator", true);
                                    return true;
                                });
                        
                    };
                }
            }
        };
});