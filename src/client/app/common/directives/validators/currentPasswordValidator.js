angular.module('adminUsersModule').directive('currentPasswordValidator', function($http, $q) {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            ngModel.$asyncValidators.password = function(modelValue, viewValue) {
                return $http.post('/api/currentPasswordValidator', {username: ngModel.username, password: viewValue}).then(
                    function(response) {
                        if (response.data.status != 'ok') {
                            return $q.reject(response.data.message);
                        }
                        return true;
                    }
                );
            };
        }
    };
});