// IIFE
// http://benalman.com/news/2010/11/immediately-invoked-function-expression/
(function (angular) {
    'use strict';

    // setup the common module so we can continue to append config and run blocks
    var module = angular.module('commonModule', ['ngBootbox']);

    // angular decorators
    module.config(['$provide', function ($provide) {

        // add message bus as $bus to all scopes
        $provide.decorator('$rootScope', function ($delegate) {
            // TODO: Not sure if this verbose syntax is required for decorators

            Object.defineProperty($delegate.constructor.prototype, '$bus', {
                get: function () {
                    var self = this;
                    var rootScope = $delegate;

                    return {
                        publish: function (msg, data) {
                            data = data || {};
                            // emit goes to parents, broadcast goes down to children
                            // since rootScope has no parents, this is the least noisy approach
                            // however, with the caveat mentioned below
                            rootScope.$emit(msg, data);
                        },
                        subscribe: function (msg, func) {
                            var unbind = rootScope.$on(msg, function (event, args) {
                                func(args);
                            });
                            // being able to enforce unbinding here is why decorating rootscope 
                            // is preferred over DI of an explicit bus service
                            self.$on('$destroy', unbind);
                        }
                    };
                },
                enumerable: false
            });
            return $delegate;
        });
    }]);

    module.run($rootScope => {

   $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
       console.log('$stateChangeStart to ' + toState.to + '- fired when the transition begins. toState,toParams : \n', toState, toParams);
   });

        $rootScope.$on('$stateChangeError', (event, toState, toParams, fromState, fromParams, error) => {
            console.log('$stateChangeError - fired when an error occurs during transition.');
            console.log(error);
        });

   $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
       console.log('$stateChangeSuccess to ' + toState.name + '- fired once the state transition is complete.');
   });

   $rootScope.$on('$viewContentLoaded', function (event) {
       console.log('$viewContentLoaded - fired after dom rendered', event);
   });

   $rootScope.$on('$stateNotFound', function (event, unfoundState, fromState, fromParams) {
       console.log('$stateNotFound ' + unfoundState.to + ' - fired when a state cannot be found by its name.');
       console.log(unfoundState, fromState, fromParams);
   });

    });

    // factory to add the auth header to every request sent to the server
    module.factory('httpResponseInterceptor', function($window){
        return {
            responseError: function (resp) {
                //console.log(resp);
                $window.location.href = resp.data;
            }
        };
    });

    // add the interceptor
    module.config(['$httpProvider', function($httpProvider){
        $httpProvider.interceptors.push('httpResponseInterceptor');
    }]);
}(angular || {}));