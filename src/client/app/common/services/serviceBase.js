/**
 * base service for other service to inherit from
 * @param $http
 * @param $q
 * @param API_URL
 * @param GlobalSiteVars
 * @param uri
 * @constructor
 */
function ServiceBase($http, $q, API_URL, GlobalSiteVars, uri) {
    this.$http = $http;
    this.$q = $q;
    this.API_URL = API_URL;
    this.GlobalSiteVars = GlobalSiteVars;
    this.url = API_URL + '/' + uri;
}

ServiceBase.prototype = {
    read: function (params) {
        var dfd = this.$q.defer();

        if (params.vsn == 'All' || params.vsn == 'Revision') {
            params.vsn = undefined;
        }
        params.vsn = params.vsn || 'all';
        params.type = params.type || 'all';

        this.$http.get(this.url + '/read/' + this.GlobalSiteVars.SiteName + '/' + params.vsn + '/' + params.type)
            .then(function (resp) {
                dfd.resolve(resp);
            });

        return dfd.promise;
    },
    delete: function (params) {
        var url = this.url + '/delete';

        if (params.length == 1) {
            url = url + '/' + params[0];
            return this.$http.post(url);
        }

        return this.$http.post(url, params);
    },
    create: function (params) {
        return this.$http.post(this.url + '/create', params);
    },
    update: function (params) {
        return this.$http.post(this.url + '/update/' + params.id, params);
    },
    getMasterTags: function (val) {
        return this.$http.get(this.API_URL + '/getMasterTags/' + val);
    }
};
