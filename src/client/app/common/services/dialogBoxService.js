angular.module('commonModule')
    .factory('dialogBoxService', ['$ngBootbox',function($ngBootbox) {
        console.log($ngBootbox);
        var dialogType = {alert:1, confirm:2, prompt:3, userDelete: 4, roleOrGroupDelete:5, custom:999};
        var show = function (type, title, message, footer, callback) {
            var options = {
                title: title,
                message: message
            };
            switch (type) {
                case dialogType.userDelete:
                    options.buttons = {
                        cancel: {
                            label: "No, Do not delete",
                            className: "btn-success",
                            callback: function(result) {
                                callback(false);
                            }
                        },
                        main: {
                            label: "Yes, Delete user profile(s)",
                            className: "btn-default",
                            callback: function (result) {
                                callback(true);
                            }
                        }
                    };
                    break;
                case dialogType.roleOrGroupDelete:
                    options.buttons = {
                        cancel: {
                            label: "No, Do not delete",
                            className: "btn-success",
                            callback: function(result) {
                                callback(false);
                            }
                        },
                        main: {
                            label: "Yes, delete",
                            className: "btn-default",
                            callback: function (result) {
                                callback(true);
                            }
                        }
                    };
                    break;
                case dialogType.confirm:
                    options.buttons = {
                        cancel: {
                            label: "Cancel",
                            className: "btn-success",
                            callback: function(result) {
                                callback(false);
                            }
                        },
                        main: {
                            label: "OK",
                            className: "btn-default",
                            callback: function (result) {

                                callback(true);
                            }
                        }
                    };
                    break;
                case dialogType.alert:
                default:
                    options.buttons = {
                        main: {
                            label: "OK",
                            className: "btn-primary"
                        }
                    };
                    break;
            }
            
            $ngBootbox.customDialog(options);

            if(type == dialogType.userDelete || type == dialogType.roleOrGroupDelete || type == dialogType.confirm) {
                // add hover functionality on the modal dialog buttons
                angular.element(".modal-dialog .modal-content .modal-footer").find("button:eq(0)").focus();
                angular.element(".modal-dialog .modal-content .modal-footer button.btn-default").hover(function(){
                    angular.element(".modal-dialog .modal-content .modal-footer button.btn-success").removeClass("btn-success").addClass("btn-default");
                    angular.element(this).removeClass("btn-default").addClass("btn-success");
                    // attach the hover handler to the new default button
                    angular.element(".modal-dialog .modal-content .modal-footer button.btn-default").hover(function(){
                        angular.element(".modal-dialog .modal-content .modal-footer button.btn-success").removeClass("btn-success").addClass("btn-default");
                        angular.element(this).removeClass("btn-default").addClass("btn-success");
                    
                    });
                });
            }
        };
        return {
            dialogType: dialogType,
            show: show
        };
    }]);