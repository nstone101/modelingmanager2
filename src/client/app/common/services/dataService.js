﻿angular.module('commonModule')
    .factory('dataService', function ($q, $http, $window) {

        var api = {
            post: post,
            get: get,
            "delete": del
        };

        return api;


        function post(endPoint, body) {
            return $http.post(manager.dataService.serviceName + endPoint, body);
        }

        function get(endPoint, params) {
            return $http.get(manager.dataService.serviceName + endPoint, {params: params});
        }

        function del(endPoint, id) {
            return $http.delete(manager.dataService.serviceName + endPoint + '/' + id);
        }

    });
