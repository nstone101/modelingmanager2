(function(angular){
    angular.module('app')
        .controller('portalController', function(){

            // util functions
            // js equiv to php's nl2br function
            function nl2br(str) {
                return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br>' + '$2');
            }

//This function will toggle the CSS styling from the full side bar to a compact side bar


            $("#collapseBtn").click( function(e) {
                var sPath = window.location.pathname;
                var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);
                var menuLink = $('#sidebar a[href="'+ sPath +'"]');

                // This checks if the bar is extended, and if so, collapses it.
                if (localStorage.menuCollapsed == 0) {
                    $('#wrap').addClass('collapsed');
                    $('.submenu ul li').hide();

                    // removes the active class to the link in the menu for the open url
                    menuLink.parent().removeClass('active');
                    menuLink.parent().parent().parent().removeClass('open');

                    // activate the tooltips on the menu icons
                    // TODO: Someone bropke this :(
                    //$('.menu-icon').tooltip();

                    if(typeof(Storage)!=="undefined") {
                        localStorage.menuCollapsed = 1;
                    }
                }
                // if the bar is already collapsed, it will be expanded:
                else {
                    $('#wrap').removeClass('collapsed');
                    $('.submenu ul li').show();

                    // adds the active class to the link in the menu for the open url
                    menuLink.parent().addClass('active');
                    menuLink.parent().parent().parent().addClass('open');

                    // remove the tooltips on the menu icons
                    // TODO: Someone bropke this :(
                    //$('.menu-icon').tooltip('destroy');

                    if(typeof(Storage)!=="undefined") {
                        localStorage.menuCollapsed = 0;
                    }
                }
            });



// This function should be used to check if the menu is collapsed upon page load.
            function check_collapse(){
                // if the menu was collapsed previously, we collapse it again.
                if (localStorage.menuCollapsed == 1){
                    collapse();
                }
            }


            function validateIP(ip) {
                //Check Format
                var ip = ip.split(".");

                if (ip.length != 4){
                    return false;
                }

                //Check Numbers
                for(var c = 0; c < 4; c++){
                    //Perform Test
                    if ( ip[c] <= -1 || ip[c] > 255 || isNaN(parseFloat(ip[c])) || !isFinite(ip[c]) || ip[c].indexOf(" ") !== -1 ) {
                        return false;
                    }
                }

                //Invalidate addresses that start with 192.168
                if ( ip[0] == 192 && ip[1] == 168 ){
                    return false;
                }

                return true;
            }


            // portal.main.js

            var sPath = window.location.pathname;
            //var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);

            // adds the active class to the link in the menu for the open url
            $('#sidebar a[href="'+ sPath +'"]').parent().addClass('active');
            $('#sidebar a[href="'+ sPath +'"]').parent().parent().parent().addClass('open active');

            // Javascript called to remove the active class from drop down items staying active
            $(".data_drop").click( function(){
                $('.data_drop').removeClass('active');
            });

            // lazyload images
            $("img.lazy").lazyload({
                effect : "fadeIn"
            });

            // trigger the scroll event when a tab is shown so that lazy loaded images will appear

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $("img.lazy").show().lazyload();
            });

            // open the correct tab if there is a hash in the url

/*
console.log(location.hash);
console.log('a[href="' + location.hash + '"]');
console.log('a[href="' + location.hash + '"]');
*/

            /*if (location.hash && location.hash != '#/') {
                $('a[href="' + location.hash + '"]').tab('show');

                //get the hashed element and find all of it's parent tabs that aren't active
                $(location.hash).parents('.tab-pane:not(.active)').each(function() {
                    //$('a[href="#' + this.id + '"]').tab('show');
                });
            }*/

            /* Update hash based on tab, basically restores browser default behavior to fix bootstrap tabs */
            $(document.body).on("click", "a[data-toggle]", function(event) {
                //location.hash = this.getAttribute("href");
            });

            /* on history back activate the tab of the location hash if exists or the default tab if no hash exists */
            /*$(window).on('popstate', function() {
                if (location.hash) {
                    //$('a[href="' + location.hash + '"]').tab('show');
                }
            });*/

            // form common style
            //$('input[type=checkbox],input[type=radio],input[type=file]').uniform();
            //$('.colorpicker').colorpicker();
            //$('.datepicker').datepicker();
            //$('#sidebar').affix({ offset: { top: 56 } });

            // If the window scrolls below 100 px below the top, fade in the scroll to top button.
            $(window).scroll(function () {
                if ($(this).scrollTop() > 100) { $('.scrollup').fadeIn(); }
                else { $('.scrollup').fadeOut(); }
            });

            // If the scroll to top button is clicked, scroll to the top of the window.
            $('.scrollup').click(function () {
                $("html, body").animate({ scrollTop: 0 }, 300);
                return false;
            });

            $(".dropdown-menu").on('click', 'li', function(){
                var dropBtn = $(this).parent().parent().find('.dropdown-toggle')
                dropBtn.html($(this).text() + "<span class='caret'></span>");
                dropBtn.val($(this).text());
            });

            $(".dropdown-list").on('click', 'li', function(e){
                e.preventDefault();
            });


/********************************************************
per MarkR - no longer deleting site this way
            // This is the delete site form validation function.
            $("#SiteDeleteForm").validate({
                rules:{
                    SiteName:{
                        required: true
                    },
                    SiteNameRepeat:{
                        required:true,
                        equalTo:"#SiteName"
                    }
                },
                errorClass: "help-inline",
                errorElement: "span",
                highlight:function(element, errorClass, validClass) {
                    if ($(element).parents('.control-group').hasClass('success')){
                        $(element).parents('.control-group').removeClass('success');
                    }
                    $(element).parents('.control-group').addClass('error');
                    $('fa-exclamation-triangle').show();

                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).parents('.control-group').removeClass('error');
                    $(element).parents('.control-group').addClass('success');
                    $('fa-exclamation-triangle').hide();
                }
            });

*********************************************************/

            $('.password-recovery').bind('click', function() {
                TweenMax.to($('.login-content'), 0.25, {x:-289});
                TweenMax.to($('.forgot-content'), 0.25, {x:-289});
                $('#messageUserFail').empty();
                $('#messagePWFail').empty();
            });

            $('.login-back-btn').bind('click', function() {
                TweenMax.to($('.login-content'), 0.25, {x:0});
                TweenMax.to($('.forgot-content'), 0.25, {x:0});
                $('#messageRecoverFail').empty();
            });

            var hash = window.location.hash.replace('#','');
            if(hash == 'PasswordRecovery'){
                TweenMax.to($('.login-content'), 0, {x:-289});
                TweenMax.to($('.forgot-content'), 0, {x:-289});
            }


            // This is the form validation function for logging in.
            $("#Login").validate({
                rules:{
                    username:{
                        required: true
                    },
                    password:{
                        required: true
                    }
                },
                errorClass: "help-inline",
                errorElement: "span",
                highlight:function(element, errorClass, validClass) {
                    if ($(element).parents('.control-group').hasClass('success')){
                        $(element).parents('.control-group').removeClass('success');
                    }
                    $(element).parents('.control-group').addClass('error');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).parents('.control-group').removeClass('error');
                    $(element).parents('.control-group').addClass('success');
                    console.log($(element).parents('.control-group'));
                },
                onfocusin: function(){
                    $("#messagePWFail").empty();
                }
            });

            $('#PasswordRecovery').bind('click', function() {
                $('.login-content').removeClass('active');
                $('.forgot-content').addClass('active');
            });


            // This is the form validation function for editing an account.
            $("#EditAccount").validate({
                rules:{
                    pwd:{
                        minlength:6,
                        maxlength:20
                    },
                    pwd2:{
                        minlength:6,
                        maxlength:20,
                        equalTo:"#pwd"
                    },
                    email:{
                        required: true,
                        email: true
                    }
                },
                errorClass: "help-inline",
                errorElement: "span",
                highlight:function(element, errorClass, validClass) {

                    if ($(element).parents('.control-group').hasClass('success')){
                        $(element).parents('.control-group').removeClass('success');
                    }
                    $(element).parents('.control-group').addClass('error');
                    /*$('fa-exclamation-triangle').show();*/
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).parents('.control-group').removeClass('error');
                    $(element).parents('.control-group').addClass('success');
                    $("#messagePWFail").empty();
                }
            });


            // This is the form validation function for running the MX config builder script on the template_mgr.php page.
            $("#ConfigScriptForm").validate({
                rules:{
                    SiteName:{
                        required:true
                    },
                    TypeSelect:{
                        required:true
                    }
                },
                errorClass: "help-inline",
                errorElement: "span",
                highlight:function(element, errorClass, validClass) {
                    if ($(element).parents('.control-group').hasClass('success')){
                        $(element).parents('.control-group').removeClass('success');
                    }
                    $(element).parents('.control-group').addClass('error');
                    $('#messageUserFail').empty();
                    $('#messagePWFail').empty();
                    /* $('fafa-exclamation-triangle').show();*/
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).parents('.control-group').removeClass('error');
                    $(element).parents('.control-group').addClass('success');
                    /*$('fa-exclamation-triangle').hide();*/
                }
            });

            // This is the form validation function for creating a new user.
            $("#NewUser").validate({
                rules:{
                    username:{
                        required: true,
                        minlength:4,
                        maxlength:20
                    },
                    email:{
                        required:true,
                        email: true
                    }
                },
                errorClass: "help-inline",
                errorElement: "span",
                highlight:function(element, errorClass, validClass) {

                    if ($(element).parents('.control-group').hasClass('success')){
                        $(element).parents('.control-group').removeClass('success');
                    }
                    $(element).parents('.control-group').addClass('error');
                    /*$('fa-exclamation-triangle').show();*/
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).parents('.control-group').removeClass('error');
                    $(element).parents('.control-group').addClass('success');
                }
            });

            // This is the form validation function for uploading a new template on the template_mgr.php page.
            $("#UploadTemplateForm").validate({
                rules:{
                    UploadTypeSelect:{
                        required:true
                    },
                    TemplateUpload:{
                        required:true
                    }
                },
                errorClass: "help-inline",
                errorElement: "span",
                highlight:function(element, errorClass, validClass) {
                    if ($(element).parents('.control-group').hasClass('success')){
                        $(element).parents('.control-group').removeClass('success');
                    }
                    $(element).parents('.control-group').addClass('error');
                    /*$('fa-exclamation-triangle').show();*/
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).parents('.control-group').removeClass('error');
                    $(element).parents('.control-group').addClass('success');
                    /*$('fa-exclamation-triangle').hide();*/
                }
            });

            // This is the form validation function for uploading a new template on the template_mgr.php page.
            $("#TagifyScriptForm").validate({
                rules:{
                    TagifyTypeSelect:{
                        required:true
                    },
                    mopUpload:{
                        required:true
                    }
                },
                errorClass: "help-inline",
                errorElement: "span",
                highlight:function(element, errorClass, validClass) {
                    if ($(element).parents('.control-group').hasClass('success')){
                        $(element).parents('.control-group').removeClass('success');
                    }
                    $(element).parents('.control-group').addClass('error');
                    /*$('fa-exclamation-triangle').show();*/
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).parents('.control-group').removeClass('error');
                    $(element).parents('.control-group').addClass('success');
                    /* $('fa-exclamation-triangle').hide();*/
                }
            });


/****************************************************
per MarkR - dupe
            $("#EditSiteInfo").click(function () {
                $("#t_sitecontact .form-control").attr('disabled', false);
                $(".readonly").attr('disabled', true);

                $(this).hide();

                $("#AddVSN").show();
                $("#UpdateSiteInfo").show();
                $("#CancelEdit").show();

                // show delete node if node is decom, otherwise do not show delete node
                if ($("#SiteGroup").val() == 'Deactivated') {
                    $("#DeleteNode").show();
                }
            });


            $("#UpdateSiteInfo").click(function () {
                var selectedGroup = $("#SiteGroup").val();
                var selectedState = $("#SiteState").val();
                var selectedSiteCode = $("#SiteCode").val();
                var selectedSiteMux = $("#SiteMux").val();
                var selectedClliCode = $("#ClliCode").val();
                var selectedSiteAddress = $("#SiteAddress").val();
                var selectedContactName = $("#ContactName").val();
                var selectedContactPhone = $("#ContactPhone").val();
                var selectedContactEmail = $("#ContactEmail").val();
                var selectedComment = $("#Comment").val();

                $.ajax({
                    url: "/ajax/site_info",
                    type: "POST",
                    data: {
                        'SiteGroup': selectedGroup,
                        'SiteState': selectedState,
                        'SiteCode': selectedSiteCode,
                        'SiteMux': selectedSiteMux,
                        'ClliCode': selectedClliCode,
                        'SiteAddress': selectedSiteAddress,
                        'ContactName': selectedContactName,
                        'ContactPhone': selectedContactPhone,
                        'ContactEmail': selectedContactEmail,
                        'Comment': selectedComment
                    },
                    success: function () {
                        //alert("ok");
                    }
                });

                $("#AddVSN").hide();
                $("#CancelEdit").hide();
                $("#UpdateSiteInfo").hide();
                $("#EditSiteInfo").show();

                $("#t_sitecontact .form-control").attr('disabled', true);
            });


            $("#DeleteNode").click(function () {
                console.log('delete');
                $.ajax({
                    url: "/admin-tools/delete-node2",
                    type: "POST",
                    data: {
                        'NodeDeleteSelect': $("#SiteName").val(),
                        'TypeSelect': 'delete'
                    },
                    success: function () {
                        console.log('deleted node', $("#SiteName").val());
                    },
                    error: function (request, error) {
                        console.log(error);
                        alert("Delete node failed with error : " + error);
                    }
                });

                window.location = '/';
            });

            $('#modal_ispec_upload').on('show.bs.modal', function (event) {
                $("input:file").change(function () {
                    var fileName = $(this)[0].files[0].name;
                    $("input[name='ispec-label']").val(fileName);
                });
            });

            $('#modal_delete_ispec').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var ispecid = button.data('ispecid');
                var ispecpath = button.data('ispecpath');

                var modal = $(this)
                modal.find('.ispec').text(ispecid);
                modal.find("input[name='delete']").val(ispecpath);
            });


            $('#modal_mop_upload').on('show.bs.modal', function (event) {
                $("input:file").change(function () {
                    var fileName = $(this)[0].files[0].name;
                    $("input[name='mop-label']").val(fileName);
                });
            });

            $('#modal_delete_mop').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var mopid = button.data('mopid');
                var moppath = button.data('moppath');

                var modal = $(this)
                modal.find('.mopid').text(mopid);
                modal.find("input[name='delete']").val(moppath);
            });


            $('#modal_photo_upload').on('show.bs.modal', function (event) {
                $("input:file").change(function () {
                    var fileName = $(this)[0].files[0].name;
                    $("input[name='photo-label']").val(fileName);
                });
            });

            $('#modal_diagram_upload').on('show.bs.modal', function (event) {
                $("input:file").change(function () {
                    var fileName = $(this)[0].files[0].name;
                    $("input[name='diagram-label']").val(fileName);
                });
            });

*******************************************/


            // drag and drop for nodes
            // TODO: Move to a NodeBrowser specific js page
            try {
                var byId = function (id) {
                    return document.getElementById(id)
                };

                // Multi groups
                Sortable.create(byId('multi'), {
                    animation: 150,
                    draggable: '.tile',
                    handle: '.node-hopper.widget-box.widget-title'
                });

                [].forEach.call(byId('multi').getElementsByClassName('tile__list'), function (el) {
                    Sortable.create(el, {
                        group: 'photo',
                        animation: 150
                    });
                });
            } catch (err) {

            }
        });
}(angular||{}));
