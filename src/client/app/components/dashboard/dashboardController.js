(function (angular) {
    // user controller
    angular.module('portal.dashboard')
        .controller('dashboardController', ['$scope', '$timeout', '$templateCache',
    function($scope, $timeout, $templateCache) {
        var flipped = false;
        $scope.gridsterOptions = {
            margins: [10, 10],
            columns: 4,
            colWidth: 250,
            rowHeight: 250,
            draggable: {
                handle: 'h3'
            },
            resizable: {
                enabled: false
            }
        };

        $scope.dashboards = {
            '1': {
                id: '1',
                name: 'Home',
                widgets: [{
                    col: 0,
                    row: 0,
                    sizeY: 1,
                    sizeX: 1,
                    name: "Top 5 Nodes",
                    id: "Top5Nodes",
                    content: $templateCache.get("components/dashboard/contents/top5Nodes.html")
                },
                {
                    col: 1,
                    row: 0,
                    sizeY: 1,
                    sizeX: 2,
                    name: "Top Node Sites",
                    id: "TopNodeSites",
                    content: $templateCache.get("components/dashboard/contents/topNodeSites.html")
                },
                {
                    col: 0,
                    row: 1,
                    sizeY: 1,
                    sizeX: 2,
                    name: "Timeline for Top 5 Nodes",
                    id: "Timeline",
                    content: $templateCache.get("components/dashboard/contents/timeline.html")
                },
                {
                    col: 0,
                    row: 3,
                    sizeY: 1,
                    sizeX: 1,
                    name: "Nodes Group",
                    id: "NodesGroup",
                    content: $templateCache.get("components/dashboard/contents/nodeGroup.html")
                },
                {
                    col: 1,
                    row: 3,
                    sizeY: 1,
                    sizeX: 1,
                    name: "Node State",
                    id: "NodeState",
                    content: $templateCache.get("components/dashboard/contents/nodeState.html")
                },
                {
                    col: 2,
                    row: 3,
                    sizeY: 1,
                    sizeX: 1,
                    name: "Node Generation",
                    id: "NodeGeneration",
                    content: $templateCache.get("components/dashboard/contents/nodeGeneration.html")
                },
                {
                    col: 0,
                    row: 4,
                    sizeY: 1,
                    sizeX: 2,
                    name: "Last 10 Changes",
                    id: "Last10Changes",
                    content: $templateCache.get("components/dashboard/contents/last10Changes.html")
                },
                {
                    col: 0,
                    row: 6,
                    sizeY: 1,
                    sizeX: 1,
                    name: "To Do List/Notes",
                    id: "ToDoList",
                    content: $templateCache.get("components/dashboard/contents/todoList.html")
                },
                {
                    col: 1,
                    row: 6,
                    sizeY: 1,
                    sizeX: 1,
                    name: "Avg Build Days",
                    id: "AvgBuildDays",
                    content: $templateCache.get("components/dashboard/contents/avgBuildDays.html")
                },
                {
                    col: 2,
                    row: 6,
                    sizeY: 2,
                    sizeX: 1,
                    name: "Passed Due Date",
                    id: "PassedDueDate",
                    content: $templateCache.get("components/dashboard/contents/passedDueDate.html")
                },
                {
                    col: 0,
                    row: 8,
                    sizeY: 1,
                    sizeX: 2,
                    name: "Incomplete Site Wizard",
                    id: "IncompleteSiteWizard",
                    content: $templateCache.get("components/dashboard/contents/incompleteSiteWizard.html")
                }]
            },
            '2': {
                id: '2',
                name: 'Other',
                widgets: [{
                    col: 1,
                    row: 1,
                    sizeY: 1,
                    sizeX: 2,
                    name: "Other Widget 1"
                }, {
                    col: 1,
                    row: 3,
                    sizeY: 1,
                    sizeX: 1,
                    name: "Other Widget 2"
                }]
            }
        };

        $scope.clear = function() {
            $scope.dashboard.widgets = [];
        };

        $scope.addWidget = function() {
            $scope.dashboard.widgets.push({
                name: "New Widget",
                sizeX: 1,
                sizeY: 1
            });
        };

        $scope.$watch('selectedDashboardId', function(newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.dashboard = $scope.dashboards[newVal];
            } else {
                $scope.dashboard = $scope.dashboards[1];
            }
        });

        // init dashboard
        $scope.selectedDashboardId = '1';

    }
])

.controller('CustomWidgetCtrl', ['$scope', '$uibModal', '$templateCache',
    function($scope, $modal, $templateCache) {

        $scope.remove = function(widget) {
            $scope.dashboard.widgets.splice($scope.dashboard.widgets.indexOf(widget), 1);
        };

        $scope.openSettings = function(widget) {
            $modal.open({
                scope: $scope,
                template: $templateCache.get("components/dashboard/widget_settings.html"),
                controller: 'WidgetSettingsCtrl',
                resolve: {
                    widget: function() {
                        return widget;
                    }
                }
            });
        };

    }
])

.controller('WidgetSettingsCtrl', ['$scope', '$timeout', '$rootScope', '$uibModalInstance', 'widget',
    function($scope, $timeout, $rootScope, $modalInstance, widget) {
        $scope.widget = widget;

        $scope.form = {
            name: widget.name,
            sizeX: widget.sizeX,
            sizeY: widget.sizeY,
            col: widget.col,
            row: widget.row
        };

        $scope.sizeOptions = [{
            id: '1',
            name: '1'
        }, {
            id: '2',
            name: '2'
        }, {
            id: '3',
            name: '3'
        }, {
            id: '4',
            name: '4'
        }];

        $scope.dismiss = function() {
            $modalInstance.dismiss();
        };

        $scope.remove = function() {
            $scope.dashboard.widgets.splice($scope.dashboard.widgets.indexOf(widget), 1);
            $modalInstance.close();
        };

        $scope.submit = function() {
            angular.extend(widget, $scope.form);

            $modalInstance.close(widget);
        };

    }
])

// helper code
.filter('object2Array', function() {
    return function(input) {
        var out = [];
        console.log(input);
        _.map(input, function(i){
            console.log(i);
            out.push(i);
        });
        console.log(out);
        return out;
    }
});
}(angular || {}));
