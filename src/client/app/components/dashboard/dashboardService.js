(function (angular) {
    // user controller
    angular.module('portal.dashboard')
    .factory('dashboardService', function ($q, $http, $timeout) {
            var api = {
                getUserWidgets: getUserWidgets,
        		getAvailableWidgets: getAvailableWidgets,
        		addWidget: addWidget,
        		deleteWidget: deleteWidget
            },
            users = [{id: 1, name: "John Smith"}, {id: 2, name: "Paula Rogers"}, {id: 3, name: "Ward Dean"}],
            widgets = [
              { id: 1, sizeX: 2, sizeY: 1, row: 0, col: 0 },
              { id: 2, sizeX: 2, sizeY: 2, row: 0, col: 2 },
              { id: 3, sizeX: 1, sizeY: 1, row: 0, col: 4 },
              { id: 4, sizeX: 2, sizeY: 1, row: 1, col: 0 },
              { id: 5, sizeX: 1, sizeY: 1, row: 2, col: 0 },
              { id: 6, sizeX: 2, sizeY: 1, row: 2, col: 1 },
              { id: 7, sizeX: 1, sizeY: 1, row: 2, col: 3 },
            ],
            userWidgets = [
                {userId: 1, widgets: [
                    { id: 2, sizeX: 2, sizeY: 2, row: 0, col: 2 },
                    { id: 3, sizeX: 1, sizeY: 1, row: 0, col: 4 },
                    { id: 4, sizeX: 2, sizeY: 1, row: 1, col: 0 },
                    { id: 5, sizeX: 1, sizeY: 1, row: 2, col: 0 },
                    { id: 6, sizeX: 2, sizeY: 1, row: 2, col: 1 }]},
                {userId: 2, widgets: [
                    { id: 1, sizeX: 2, sizeY: 1, row: 0, col: 0 },
                    { id: 2, sizeX: 2, sizeY: 2, row: 0, col: 2 },
                    { id: 3, sizeX: 1, sizeY: 1, row: 0, col: 4 },
                    { id: 4, sizeX: 2, sizeY: 1, row: 1, col: 0 }]},
                {userId: 3, widgets: [
                    { id: 4, sizeX: 2, sizeY: 1, row: 1, col: 0 },
                    { id: 5, sizeX: 1, sizeY: 1, row: 2, col: 0 },
                    { id: 6, sizeX: 2, sizeY: 1, row: 2, col: 1 },
                    { id: 7, sizeX: 1, sizeY: 1, row: 2, col: 3 }]}
              ];

            return api;

            function getAvailableWidgets(){
                return this.widgets;
            }

            function getAvailableUsers(){
                return this.users;
            }

            function getUserWidgets(user){
                return this.userWidgets[user.id];
            }

            function addUserWidget(params){
                return this.userWidgets[params.userId].push(params);
            }

            function removeUserWidget(user, id){
                //_.where(_.flatten(_.pluck(_.where(userWidgets, {"userId":2}), "widgets")), {id: 2})
                return _.where(this.widgets, {"id": id});
            }

            function addUser(){

            }

            function deleteUser(){

            }

        });
}(angular || {}));