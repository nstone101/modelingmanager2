(function(angular){
	angular
		.module('portal.dashboard', [
			'gridster',
			'ui.router',
	        'ui.bootstrap',
	        'cgBusy',
	        'commonModule'
		])
		.config(function($stateProvider, $urlRouterProvider) {
	        //$urlRouterProvider.when('/', '/dashboard');

	        $stateProvider
	            .state('dashboardLayout', {
	                abstract: true,
	                parent: 'app',
	                controller: 'dashboardLayoutController',
	                templateUrl: 'components/dashboard/dashboardLayout.html'
	            })
	            .state('dashboard', {
	                url: '/dashboard',
	                parent: 'dashboardLayout',
	                templateUrl: 'components/dashboard/dashboard.html',
	                controller: 'dashboardController'
	            });
	    })
	    .filter("sanitize", ['$sce', function($sce) {
  			return function(htmlCode){
    			return $sce.trustAsHtml(htmlCode);
  			}
		}]);
}(angular||{}));
