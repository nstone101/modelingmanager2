(function (angular) {
    // user controller
    angular.module('portal.dashboard')
        .controller('dashboardLayoutController', function ($rootScope, $scope, $timeout) {
            var vm = {
        		widgets: []
            };

            function init() {
                $scope.$on('$locationChangeStart', function(e, next, current) {
                    $scope.page = next.split('/').splice(-1);
                    $scope.styleUrl = 'assets/demo/' + $scope.page + '/style.css'
                });
            	$scope.vm = vm;
            }

            init();
        });
}(angular || {}));