// wrap in an IIFE
(function(angular) {

    // setup a base module
    angular.module('adminUsersModule')
        .service('userEditorService', function($http){
            var  api = {};

            api.getMasterTags = function(val) {
                return $http.get(this.API_URL + '/getMasterTags/' + val);
            };

            return api;
        });
}(angular || {}));
