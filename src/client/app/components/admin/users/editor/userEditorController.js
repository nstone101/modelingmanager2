(function(angular){
    angular.module('adminUsersModule')
        .controller('userEditorController', function ($rootScope, $scope, $modal, $templateCache, $q, userEditorService, usaStateService) {
            var vm = {
                state: 'create', // create or edit
                labels: {
                    'create': {
                        'title': 'New User',
                        'subTitle': 'Create User',
                        'submit': 'Create'
                    },
                    'update': {
                        'title': 'Update Entry',
                        'subTitle': 'Update Existing Entry',
                        'submit': 'Update'
                    }
                },
                vsnOptions: [],
                form: {},
                animationsEnabled: true,
                events: {
                    modalOpen: '',
                    gridUpdate: ''
                }
            };



            //Password must be at least 8 characters long and use 1 uppercase letter, 1 lowercase letter, 1 number and special character(@,#,$,%,^)
            vm.passwordValidator = function(password) {
                if(!password){return;}
                
                if (password.length < 8) {
                    return "Password must be at least " + 8 + " characters long";
                }

                if (!password.match(/[A-Z]/)) {
                     return "Password must have at least one capital letter";
                }

                if (!password.match(/[a-z]/)) {
                     return "Password must have at least one lowercase letter";
                }

                if (!password.match(/[0-9]/)) {
                     return "Password must have at least one number";
                }

                if (!password.match(/[!@#$%^]/)) {
                     return "Password must have at least one special character";
                }

                return true;
            }

            $scope.$viewValue= '';

            /**
             * @param params
             *          state - create
             *                  update
             *          row - set of data to display
             *          SiteName - current site name
             *          templateUrl - url for the editor form
             *          service - the service associated with the grid controller
             *          gridUpdateEventName
             */
            $scope.open = function (params) {
                // cleanup anu old data
                var modalInstance;
                if(params.state)
                    vm.state = params.state;

                vm.form = params.row;
                console.log(params.row);

                vm.service = params.service;
                vm.events.modalOpen = params.modalOpenEventName;
                vm.events.gridUpdate = params.gridUpdateEventName;
                vm.active = true;
                vm.states = usaStateService;
                if(vm.form.roles && vm.form.roles.length > 0)
                    vm.form.role = vm.form.roles[0].role_id;
                else 
                    vm.form.role = null;
                //delete vm.form.role.$$hashKey;
                var template = $templateCache.get(params.templateUrl);

                $q.all([params.service.getGroups(), params.service.getRoles()])
                    .then(function(options){
                            console.log(options);
                            vm.groups = options[0].data;
                            vm.roles = options[1].data;//.map(function(role){return role.role_name;});
                            
                            modalInstance = $modal.open({
                                animation: vm.animationsEnabled,
                                template: template,//$templateCache.get(params.templateUrl),
                                controller: 'ModalInstanceCtrl',
                                size: 'lg',
                                resolve: {
                                    vm: function () {
                                        return $scope.vm;
                                    }
                                }
                            });

                            modalInstance.result.then(function (formData) {
                                if(vm.state == 'create'){
                                    // a new item doesn't have a row id.
                                    delete formData.id;
                                    var payload = {};
                                    payload.role = _.find(vm.roles, {role_id: formData.role});
                                    delete formData.role;
                                    delete formData.roles;
                                    payload.user = formData;
                                    //payload.user.usergroup = formData.usergroup.group_name;
                                    payload.user.FullName = formData.firstname + " " + formData.lastname;

                                    vm.service.addUser(payload)
                                        .then(function(resp){
                                            console.log(resp);
                                            updateGrid();
                                        });
                                }
                                if(vm.state == 'update'){
                                    var payload = {};
                                    payload.role = _.find(vm.roles, {role_id: formData.role});
                                    delete formData.role;
                                    delete formData.roles;
                                    payload.user = formData;
                                    //payload.user.usergroup = formData.usergroup.group_name;
                                    payload.user.FullName = formData.firstname + " " + formData.lastname;
                                    vm.service.editUser(payload).then(function(){
                                        updateGrid();
                                    });
                                }
                            }, function (args) {
                                console.info('Modal dismissed at: ' + new Date());
                            });
                        });
            }

            function updateGrid(){
                delete $scope.vm.form;
                $scope.vm.form = {};
                $rootScope.$bus.publish(vm.events.gridUpdate);
            }

            function getOptions(service){
                return ;
            }


            function init(){
                // get the groups
                $rootScope.$bus.subscribe('USER_EDITOR_OPEN', function(params){
                    $scope.open(params);
                });
                $scope.vm = vm;
            }

            init();
        })
        .controller('ModalInstanceCtrl', function ($scope, $modalInstance, vm) {
            $scope.vm = vm;
            $scope.selected = {
                form: $scope.vm.form
            };

            $scope.ok = function () {
                $scope.$broadcast('show-errors-check-validity');
                if($scope.userForm.$valid) {
                    $modalInstance.close($scope.selected.form);
                }
                
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        });
}(angular || {}));