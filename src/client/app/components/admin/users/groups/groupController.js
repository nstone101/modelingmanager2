(function (angular) {
    // user controller
    angular.module('adminUsersModule')
        .controller('groupController', function ($rootScope, $scope, $timeout, adminUserService, User, header) {
            var vm = {
                groups: [],
                filter: null,
                applyFilter: applyFilter,
                addGroup: addGroup,
                deleteGroup: deleteGroup,
                save: save,
                newGroup: { "group_id": -1, "group_name": "", "group_description": "" },
                errorMessage: null
            };

            init();

            function init() {
                $scope.vm = vm;
                $scope.$watch('vm.filter', applyFilter);
                $rootScope.$bus.publish('HEADER_UPDATE', {header: header});
            }

            function applyFilter(filter) {
                adminUserService.getGroups(filter)
                    .then(function(resp) {
                        vm.groups = resp.data;
                    });
            }

            function addGroup() {
                clearError();
                adminUserService.addGroup(vm.newGroup)
                    .then(function(res) {
                        vm.groups.push(vm.newGroup);
                        vm.newGroup = "";
                });
            }

            function deleteGroup(group) {
                clearError();
                adminUserService.deleteGroup(group)
                    .then((res) => {
                        vm.groups.push(group);
                    });
            }

            function save(group) {
                clearError();

                adminUserService.editGroup(group)
                    .then((res) => {
                        
                    });
            }
            function clearError() {
                vm.errorMessage = null;
            }
        });
}(angular || {}));