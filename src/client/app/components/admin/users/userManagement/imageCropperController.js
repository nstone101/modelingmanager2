(function (angular) {
	angular.module('userModule')
		.controller('imageCropperController', function ($rootScope, $scope, $timeout) {

			$scope.origImage='';
			$scope.croppedImage='';
			$scope.thumb='';

			function init(){
				$scope.$apply(function($scope) {
					// TODO: This is a short term hack until we get an angular model
					var thumb = angular.element(document.querySelector('#avatarUploadBtn img'));
					$scope.origImage = thumb[0].currentSrc;
				});

				//$('#fileInput').value = $scope.origImage;
				//console.log($('#fileInput').value);
			}

			var handleFileSelect = function(evt) {
				var file=evt.currentTarget.files[0];
				var reader = new FileReader();
				reader.onload = function (evt) {
					$scope.$apply(function($scope){
						$scope.origImage = evt.target.result;
						console.log($scope.origImage);
					});
				};
				reader.readAsDataURL(file);
			};

			$timeout(function() {
				angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);
				init();
			},500, false);



		});
}(angular || {}));