/**
 * User Managmentent Pages App44
 */
// avoid the object not defined error
(function (angular) {
    'use strict';
    // setup the app module
    angular.module('userModule', [
	    'ngImgCrop',
        'commonModule',
	    'ngBootbox'
    ]);
}(angular || {}));