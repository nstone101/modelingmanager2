(function (angular) {
    // user controller
    angular.module('adminUsersModule')
        .controller('userListController', function ($rootScope, $scope, $timeout, $templateCache, adminUserService, uiGridConstants, dialogBoxService, header) {
            var columns = [
                {"field": "image", "width": 64, "cellTemplate": '<div class="text-center"><img class="user-image" ng-src="{{grid.appScope.getUserImage(row.entity)}}" alt="{{row.entity.fullName}}"/></div>'},
                {"field": "fullName", "name": "Name", "ticked": true, "cellClass": "middle"},
                {"field": "username", "name": "User", "ticked": true, "cellClass": "middle"},
                {"field": "email", "name": "Email", "ticked": true, "cellClass": "middle"},
                {"field": "cellphone", "name": "Cell", "ticked": true, "cellClass": "middle"},
                {"field": "workphone", "name": "Work", "ticked": true, "cellClass": "middle"},
                {"field": "roles[0].role_name", "displayName": "Role", "ticked": true, "cellClass": "middle"},
                {"field": "usergroup", "name": "Group", "ticked": true, "cellClass": "middle"},
                {"field": "activated", "name": "Active", "ticked": true, "width": 72, "cellClass": "middle",
                    "cellTemplate": '<div class="text-center"><i ng-class="{\'fa fa-toggle-on active\':row.entity.activated == 1, \'fa fa-toggle-off inactive\':row.entity.activated == 0}" ng-click="grid.appScope.toggleActivation(row.entity)"></i></div>'},
                {"name": "Delete", "width": 92, "cellClass": "middle",
                    "cellTemplate": '<div class="text-center"><span class="text-center delete-btn"><i class="fa fa-trash-o fa-2x" tooltip="Delete" tooltip-placement="left" ng-click="grid.appScope.deleteUser(row.entity)"></i></span>'}
            ];

            var vm = {
                userFilterValue: '',
                filterValue: '',
                multiSelectColumns: columns,
                shownColumns: {},
                "options": {
                    rowHeight: 60,
                    enableSorting: true,
                    paginationPageSizes: [25, 50, 75],
                    paginationPageSize: 25,
                    displayDeleteBtn: false,
                    displayEditBtn: false,
                    columnDefs: columns,
                    rowTemplate: $templateCache.get('components/admin/users/userList/userRowTemplate.html'),
                    onRegisterApi: function (gridApi) {
                        vm.gridApi = gridApi;
                        vm.gridApi.grid.registerRowsProcessor(vm.filterColumns, 200);
                        vm.gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                            var selections = vm.gridApi.selection.getSelectedRows();
                            updateEditDeleteButtons(selections.length);
                        });

                        vm.gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                            var selections = vm.gridApi.selection.getSelectedRows();
                            updateEditDeleteButtons(selections.length);
                        });

                        vm.gridApi.core.on.columnVisibilityChanged($scope, function (column) {
                            vm.toggleSelectColumns(column);
                        });
                    }
                }
            };

            vm.filter = function () {
                vm.gridApi.grid.refresh();
            };

            vm.filterColumns = function (renderableRows) {
                var matcher = new RegExp(vm.filterValue);
                renderableRows.forEach(function (row) {
                    var match = false;
                    vm.options.columnDefs.forEach(function (column) {

                        var field = column.field;
                        if(!field) return;

                        if(field && field.indexOf('role') > -1) {
                            if(row.entity.roles && row.entity.roles.length > 0){
                                if((row.entity.roles[0].role_name && row.entity.roles[0].role_name.match) && row.entity.roles[0].role_name.match(matcher)) {
                                    match = true;
                                }
                            }
                        }

                        // make sure both the row entity and the match function exist
                        if ((row.entity[field] && row.entity[field].match) && row.entity[field].match(matcher)) {
                            match = true;
                        }
                    });

                    if (!match) {
                        row.visible = false;
                    }
                    else {
                        row.visible = true;
                    }
                });
                return renderableRows;
            };

            vm.toggleGridColumns = function (data) {
                var index = _.findIndex(vm.options.columnDefs, {field: data.field});
                vm.options.columnDefs[index].visible = data.ticked;
                vm.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
            };

            vm.toggleSelectColumns = function (data) {
                var index = _.findIndex(vm.options.columnDefs, {field: data.field});
                vm.options.columnDefs[index].ticked = data.visible;
            };

            vm.deleteRows = function () {
                var rows = _.map(vm.gridApi.selection.getSelectedRows(),function(item){
                    return item.username;
                });
                var title = "Delete %text%";
                var message = "<p>Are you sure you want to delete this user?</p><span>If you delete this user, you will have to recreate the user profile.</span>";


                console.log(rows);
                if(rows.length === 1){
                    console.log('tes');
                    title = title.replace('%text%', "User " + rows[0]);
                }
                else {
                    title = title.replace('%text%', "Selected Users");
                    message = "<p>Are you sure you want to delete this users?</p>";
                    _.each(rows, function(username){
                        message+="<span>"+username+"</span><br/>";
                    });
                    message+="<span>If you delete these users, you will have to recreate the user profiles.</span>";
                }


                dialogBoxService.show(
                    dialogBoxService.dialogType.userDelete,
                    title,
                    message, "",
                    function(result){
                        if(!result) return;
                        /*var entities = _.map(vm.gridApi.selection.getSelectedRows(),
                            function (item) {
                                // we are using the _backingStore bc the other properties are
                                return _.where(vm.dataStore.results, {_backingStore: {username: item.username}})[0];
                            });*/

                        adminUserService.deleteUsers(vm.gridApi.selection.getSelectedRows().map(function(row){return row.id;}))
                            .then(function(resp){
                                getGridData();
                            });
                        updateEditDeleteButtons(0);
                    });
            };

            vm.openModal = function (state) {
                if (state == 'update') {
                    var row = vm.gridApi.selection.getSelectedRows();
                    vm.updateRow = row[0];
                }
                else {
                    vm.updateRow = adminUserService.createUser();
                }

                $rootScope.$bus.publish('USER_EDITOR_OPEN', {
                    "row": vm.updateRow,
                    "state": state,
                    "service": adminUserService,
                    "gridUpdateEventName": 'USER_LIST_UPDATE',
                    "templateUrl": 'components/admin/users/userEdit/userEdit.html'
                });
            };


            $scope.getUserImage = function(entity){
                if(!entity.image)
                    return "/assets/images/vzportal/user_image.png";
            }

            $scope.deleteUser = function(entity){
                console.log(entity);
                
                var title = "Delete User " + entity.username;
                var message = "<p>Are you sure you want to delete this user?</p><span>If you delete this user, you will have to recreate the user profile.</span>";

                dialogBoxService.show(
                    dialogBoxService.dialogType.userDelete,
                    title,
                    message, "",
                    function(result){
                        if(!result) return;
                        
                        adminUserService.deleteUsers([entity.id])
                            .then(function(resp){
                                getGridData();
                            });
                    });
            };

            $scope.toggleActivation = function(entity){
                console.log(entity);
                var title = "%activatedTag% User " + entity.username;
                var message = "<p>Are you sure you want to %activatedTag% this user?</p>";
                
                if(entity.isActivated == 0){
                    title = title.replace('%activatedTag%', "Activate");
                    message = message.replace('%activatedTag%', "Activate");
                }
                else {
                    title = title.replace('%activatedTag%', "Inactivate");
                    message = message.replace('%activatedTag%', "Activate");
                }

                dialogBoxService.show(
                    dialogBoxService.dialogType.confirm,
                    title,
                    message, "",
                    function(result){
                        if(!result) return;
                        var item = {id: entity.id};
                        item.activated = (entity.activated == 0) ? 1 : 0;
                        
                        adminUserService.toggleActivated(item)
                            .then(function(resp){
                                getGridData();
                            });
                    });
            };

            $scope.getRoles = function(entity) {
                return entity.roles.map(role => roleAsDto(role));
            };

            function getGridData() {
                vm.loadingPromise = adminUserService.getUsers()
                    .then(function (resp) {
                        angular.element(document.getElementsByClassName('grid')[0]).css('height', 600 + 'px');
                        angular.element(document.getElementsByClassName('grid')[0]).css('width', 1330 + 'px');

                        console.log(resp);

                        vm.dataStore = resp.data;

                        $scope._Roles = {};

                        vm.options.data = resp.data;

                        console.log(vm.options.data);
                        vm.gridApi.core.notifyDataChange(uiGridConstants.dataChange.ALL);
                    });
            }

            function updateEditDeleteButtons(length) {
                if (length == 1) {
                    vm.displayDeleteBtn = true;
                    vm.displayEditBtn = true;
                }
                else if (length > 1) {
                    vm.displayDeleteBtn = true;
                    vm.displayEditBtn = false;
                }
                else if (length < 1) {
                    vm.displayDeleteBtn = false;
                    vm.displayEditBtn = false;
                }
            }

            function roleAsDto(role){
                return role._backingStore;
            }

            function init() {
                getGridData();

                $rootScope.$bus.subscribe('USER_LIST_UPDATE', function(){
                    console.log('grid update');
                    getGridData();
                });

                var filterThrottled = _.debounce(vm.filter, 500);
                $scope.$watch('vm.userFilterValue', function (newVal, oldVal) {
                    vm.filterValue = vm.userFilterValue;
                    filterThrottled($scope);
                });
                
                $rootScope.$bus.publish('HEADER_UPDATE', {header: header});
                
                $scope.vm = vm;
            }

            init();
        });
}(angular || {}));