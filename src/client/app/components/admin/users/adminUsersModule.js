/**
 * User Page App
 */
// avoid the object not defined error
(function (angular) {
    'use strict';
    // setup the app module
    angular.module('adminUsersModule', [
        'ui.router',
        'ui.bootstrap',
        'cgBusy',
        'ui.grid',
        'ncy-angular-breadcrumb',
        'xeditable',
        'ui.grid.pagination',
        'ui.grid.cellNav',
        'ui.grid.edit',
        'ui.grid.resizeColumns',
        'ui.grid.pinning',
        'ui.grid.selection',
        'ui.grid.moveColumns',
        'ui.grid.exporter',
        'ui.grid.grouping',
        'ui.grid.autoResize',
        'isteven-multi-select',
        'commonModule',
        'ngImgCrop',
        'angularValidator'
    ])
    .config(function($stateProvider, $urlRouterProvider) {
        
        // this is added bc there were some issues with the previous link
        $urlRouterProvider.otherwise(function($injector, $location){
            //what this function returns will be set as the $location.url
            if($location.absUrl().indexOf('admin/users/list') > -1 && $location.absUrl().indexOf('#/admin/users') === -1){
                return '/admin/users';
            }
        });

        $stateProvider
            .state('userslayout', {
                abstract: true,
                parent: 'app',
                controller: 'adminUserLayoutController',
                templateUrl: 'components/admin/users/layout/layout.html'
            })
            .state('admin', {
                url: '/admin',
                parent: 'userslayout',
                templateUrl: 'components/admin/users/adminUsers.html',
                ncyBreadcrumb: { label: 'User Management' },
                controller: 'userController'
            })
            .state('admin.users', {
                url: '/users',
                parent: 'admin',
                templateUrl: 'components/admin/users/userList/userList.html',
                controller: 'userListController',
                ncyBreadcrumb: { label: '' },
                resolve: {
                    header: function(){return "User Management";}
                }
            })
            .state('admin.roles', {
                url: '/roles',
                parent: 'admin',
                templateUrl: 'components/admin/users/roles/role.html',
                controller: 'roleController',
                ncyBreadcrumb: { label: '' },
                resolve: {
                    header: function(){return "Roles";}
                }
            })
            .state('admin.groups', {
                url: '/groups',
                parent: 'admin',
                templateUrl: 'components/admin/users/groups/group.html',
                controller: 'groupController',
                ncyBreadcrumb: { label: '' },
                resolve: {
                    header: function(){ return "Groups";}
                }
            });
    });
}(angular || {}));
