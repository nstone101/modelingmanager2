(function (angular) {
    // user controller
    angular.module('adminUsersModule')
        .controller('roleController', function ($rootScope, $scope, $timeout, adminUserService, User, header) {
            var vm = {
                roles: [],
                filter: null,
                applyFilter: applyFilter,
                addRole: addRole,
                deleteRole: deleteRole,
                save: save,
                newRole: { "role": "", roleDescription: "" },
                errorMessage: null
            };

            init();

            function init() {
                $scope.vm = vm;
                $scope.$watch('vm.filter', applyFilter);
                $rootScope.$bus.publish('HEADER_UPDATE', {header: header});
            }

            function applyFilter(filter) {
                adminUserService.getRoles(filter)
                    .then(function(data) {
                        vm.roles = data.results;
                    });
            }

            function addRole() {
                clearError();

                var role = adminUserService.createRole(vm.newRole);
                console.log(role);
                adminUserService.saveChanges()
                    .then(function(res) {
                        vm.roles.push(role);
                        vm.newRole = "";
                });
            }

            function deleteRole(role) {
                clearError();
                    console.log(vm.roles);
                adminUserService.deleteEntity(role)
                    .then(res => _.remove(vm.roles, { _backingStore: { Id: role.Id }}))
                    .catch(err => {
                        if (err.message && /(UPDATE|DELETE) statement conflicted with the (FOREIGN KEY|REFERENCE) constraint/.test(err.message)) {
                            vm.errorMessage = "Couldn't delete " + role.role + " since it's still in use.";
                        }
                        console.log(err);
                    });
            }

            function save() {
                clearError();

                adminUserService.saveChanges();
            }

            function clearError() {
                vm.errorMessage = null;
            }
        });
}(angular || {}));