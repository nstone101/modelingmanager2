angular.module('adminUsersModule')
    .factory('adminUserService', function($q, $http, dataService, User){
        var api = {
            getUsers: getUsers,
            createUser: createUser,
            addUser: addUser,
            editUser: editUser,
            deleteUsers: deleteUsers,
            generatePassword: generatePassword,
            getRoles: getRoles,
            createRole: createRole,
            getGroups: getGroups,
            createGroup: createGroup,
            addGroup: addGroup,
            editGroup: editGroup,
            deleteGroup: deleteGroup,
            saveUserChanges: saveUserChanges,
            
            toggleActivated: toggleActivated
        };

        var service = angular.extend(api, dataService);

        return service;

        /**
         * creates and returns a new role breeze entity 
         * @param  {mixed} initialValues any init properties to set for the new role
         * @return {Role}  breeze role object
         */
        function createRole(initialValues) {
            initialValues = initialValues || {};

            return  angular.extend(initialValues, {
                        editedBy: User.username,
                        editedDate: moment().format('YYYY-MM-DD HH:mm:ss')
                    });
        }

        /**
         * creates and returns a new breeze group entity
         * @param  {mixed} initialValues any init properties to set for the new group
         * @return {Group} breeze group entity
         */
        function createGroup(initialValues) {
            initialValues = initialValues || {};

            return angular.extend(initialValues, {});
        }

        /**
         * creates and returns a new breeze user entity
         * @param  {mixed} initialValues any init properties to set for the new user
         * @return {User} breeze user entity
         */
        function createUser(initialValues){
            initialValues = initialValues || {};

            return angular.extend(initialValues, {});
        }

        function addUser(user){
            delete user.id;
            return $http.post('/api/users', user);
        }

        function editUser(values){
            return $http.post('/api/users', values);
        }

        function generatePassword(password) {
            return $http.post('/api/generatePassword', {"password": password});
        }

        function getUsers(id) {
            var params = (id == undefined) ? {"id":id} : {};

            return $http.get('/api/users', params);
        }

        function getRoles(id){
            var params = (id == undefined) ? {"id":id} : {};

            return $http.get('/api/roles', params);
        }

        function getGroups(id){
            var params = (id == undefined) ? {"id":id} : {};

            return $http.get('/api/groups', params);
        }

         function addGroup(group){
            return $http.post('/api/groups', group);
        }

        function editGroup(group){
            return $http.post('/api/groups', group);
        }

        function deleteGroup(id){
            return $http.post('/api/groups/delete', {"ids": id});
        }

        function deleteUsers(id){
            return $http.post('/api/users/delete', {"ids": id});
        }

        function toggleActivated(data){
            return $http.post('/api/user/activated', data);
        }

        function saveUserChanges(data){
            return $http.post('/api/users', data);
        }
    });
