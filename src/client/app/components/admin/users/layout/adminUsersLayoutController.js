(function (angular) {
    // user controller
    angular.module('adminUsersModule')
        .controller('adminUserLayoutController', function ($rootScope, $scope, $timeout) {
            var vm = {
            	header: ''
            };

            function init() {
            	$rootScope.$bus.subscribe('HEADER_UPDATE', function(data){
            		
                        $timeout(function(){
                            vm.header = data.header;
                        });
            		
            	});
                $scope.vm = vm;
            }

            init();
        });
}(angular || {}));