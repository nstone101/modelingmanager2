// wrap in an IIFE
(function(angular, ServiceBase) {
    function SubnetAdvService($http, $q, API_URL, GlobalSiteVars){
        ServiceBase.call(this, $http, $q, API_URL, GlobalSiteVars, 'subnetAdv');
    }

    SubnetAdvService.prototype = new ServiceBase();
    SubnetAdvService.inject = ['$http', '$q', 'API_URL', 'GlobalSiteVars'];

    // setup a base module
    angular.module('nodeModule')
        .service('subnetAdvService', SubnetAdvService);

}(angular || {}, ServiceBase || {}));
