(function (angular) {
    // node controller
    angular.module('nodeModule')
        .controller('nodeController', function ($rootScope, $scope, $timeout) {
            var vm = {

                currentDropdownItem: 'All',
                vsnFilter: function(vsn){
                    vm.currentDropdownItem = vsn;
                    $rootScope.$bus.publish('NODE_VNS_FILTER', {vsnFilter: vsn});
                },
                pageDataRefresh: function(page, title){
                    // refresh the title andd breadcrumb
                    angular.element('.tab-title').html(title);
                    angular.element('#tab-name').html(title);
                    $rootScope.$bus.publish(page.toUpperCase() + '_DATA_UPDATE');

                }
            };

            function init(){
                angular.element('#page-tabs a').on('click',function(){

                    $scope.breadcrumb = 'Contact Information';
                    var self = this;
                    if($(self).html() != 'Node Data') {
                        angular.element('.tab-title').html($(self).html());

                        angular.element('#tab-name').html($(self).html());
                    }
                   // $scope.breadcrumb = $(self).html();

                });
                // add listender for the top level item
                angular.element('#lnk_nodeData').on('click', function() {
                    var selectedVal = angular.element('#sitedata-dropdown button').html(),
                        currentPage = $.trim(selectedVal.substring(0, selectedVal.indexOf('<span')));
                    console.log(selectedVal);
                    console.log(currentPage);
                    angular.element('.tab-title').html(currentPage);
                    angular.element('#tab-name').html(currentPage);
                });

                // TODO: spike to determine if there is a nicer way to handle this update?
                angular.element('#type-dropdown').on('hide.bs.dropdown', function(evt){
                    var templateSuffix = '<span class="caret"></span>';
                    angular.element('#type-dropdown button')
                        .html(angular.element('#type-dropdown ul li a[data-value="'+vm.currentDropdownItem+'"]')
                            .html() + templateSuffix);
                });


                $scope.vm = vm;
            }


            function setActiveTab() {

                var active = $('.nav-tabs .active');
                var activeTab = undefined;
                if(active != undefined && active.length >= 1) {
                    active = active[0];
                    activeTab = active.firstChild.id;
                    if (activeTab === "lnk_nodeData") {
                        $('#siteDataControls').show();
                    } else {
                        $('#siteDataControls').hide();
                    }

                }

                $(document).on('shown.bs.tab', '#page-tabs a', function (e) {
                    if (e.target.id == "lnk_nodeData") {
                        $('#siteDataControls').show();
                    } else {
                        $('#siteDataControls').hide();
                    }
                });

            }

            try {
                setActiveTab();
            } catch (err) {
                console.log(err);
            }



            $("#EditSiteInfo").click(function () {
                $("#t_sitecontact .form-control").attr('disabled', false);
                $(".readonly").attr('disabled', true);

                $(this).hide();

                $("#AddVSN").show();
                $("#UpdateSiteInfo").show();
                $("#CancelEdit").show();

                // show delete node if node is decom, otherwise do not show delete node
                if ($("#SiteGroup").val() == 'Deactivated') {
                    $("#DeleteNode").show();
                }
            });


            $("#UpdateSiteInfo").click(function () {
                var selectedGroup = $("#SiteGroup").val();
                var selectedState = $("#SiteState").val();
                var selectedSiteCode = $("#SiteCode").val();
                var selectedSiteMux = $("#SiteMux").val();
                var selectedClliCode = $("#ClliCode").val();
                var selectedSiteAddress = $("#SiteAddress").val();
                var selectedContactName = $("#ContactName").val();
                var selectedContactPhone = $("#ContactPhone").val();
                var selectedContactEmail = $("#ContactEmail").val();
                var selectedComment = $("#Comment").val();

                $.ajax({
                    url: "/ajax/site_info",
                    type: "POST",
                    data: {
                        'SiteGroup': selectedGroup,
                        'SiteState': selectedState,
                        'SiteCode': selectedSiteCode,
                        'SiteMux': selectedSiteMux,
                        'ClliCode': selectedClliCode,
                        'SiteAddress': selectedSiteAddress,
                        'ContactName': selectedContactName,
                        'ContactPhone': selectedContactPhone,
                        'ContactEmail': selectedContactEmail,
                        'Comment': selectedComment
                    },
                    success: function () {
                        //alert("ok");
                    }
                });

                $("#AddVSN").hide();
                $("#CancelEdit").hide();
                $("#UpdateSiteInfo").hide();
                $("#EditSiteInfo").show();

                $("#t_sitecontact .form-control").attr('disabled', true);
            });

// MQ : not used anymore
//            $("#GenNodeConf").click(function (event) {
//                //event.preventDefault();
//                console.log('Generating node config file.');
//                alert('Generating node config files.');
//                $.ajax({
//                    url: "/node/configfile",
//                    type: "POST",
//                    data: {
//                        'NodeSelect': $("#SiteName").val(),
//                        'action': 'generate'
//                    },
//                    success: function () {
//                        console.log('Node config file generated for node ', $("#SiteName").val());
//                    },
//                    error: function (request, error) {
//                        console.log(error);
//                       alert("Generate config file failed with error : " + error);
//                    }
//                });
//
//                // window.location = '/';
//            });

            $("#DeleteNode").click(function () {
                console.log('delete');
                $.ajax({
                    url: "/admin-tools/delete-node2",
                    type: "POST",
                    data: {
                        'NodeDeleteSelect': $("#SiteName").val(),
                        'TypeSelect': 'delete'
                    },
                    success: function () {
                        console.log('deleted node', $("#SiteName").val());
                    },
                    error: function (request, error) {
                        console.log(error);
                        alert("Delete node failed with error : " + error);
                    }
                });

                window.location = '/';
            });

            /*$('#modal_ispec_upload').on('show.bs.modal', function (event) {
                $("input:file").change(function () {
                    var fileName = $(this)[0].files[0].name;
                    $("input[name='ispec-label']").val(fileName);
                });
            });

            $('#modal_delete_ispec').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var ispecid = button.data('ispecid');
                var ispecpath = button.data('ispecpath');

                var modal = $(this)
                modal.find('.ispec').text(ispecid);
                modal.find("input[name='delete']").val(ispecpath);
            });


            $('#modal_mop_upload').on('show.bs.modal', function (event) {
                $("input:file").change(function () {
                    var fileName = $(this)[0].files[0].name;
                    $("input[name='mop-label']").val(fileName);
                });
            });

            $('#modal_delete_mop').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var mopid = button.data('mopid');
                var moppath = button.data('moppath');

                var modal = $(this)
                modal.find('.mopid').text(mopid);
                modal.find("input[name='delete']").val(moppath);
            });


            $('#modal_photo_upload').on('show.bs.modal', function (event) {
                $("input:file").change(function () {
                    var fileName = $(this)[0].files[0].name;
                    $("input[name='photo-label']").val(fileName);
                });
            });

            $('#modal_diagram_upload').on('show.bs.modal', function (event) {
                $("input:file").change(function () {
                    var fileName = $(this)[0].files[0].name;
                    $("input[name='diagram-label']").val(fileName);
                });
            });


            $('#modal_failover_upload').on('show.bs.modal', function (event) {
                $("input:file").change(function () {
                    var fileName = $(this)[0].files[0].name;
                    $("input[name='failover-label']").val(fileName);
                });
            });
            $('#modal_layout_upload').on('show.bs.modal', function (event) {
                $("input:file").change(function () {
                    var fileName = $(this)[0].files[0].name;
                    $("input[name='layout-label']").val(fileName);
                });
            });
            
            $('#modal_delete_failover').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var failoverid = button.data('failoverid');
                var failoverpath = button.data('failoverpath');

                var modal = $(this)
                modal.find('.failoverid').text(failoverid);
                modal.find("input[name='delete']").val(failoverpath);
            });
            $('#modal_delete_layout').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var layoutid = button.data('layoutid');
                var layoutpath = button.data('layoutpath');

                var modal = $(this)
                modal.find('.layoutid').text(layoutid);
                modal.find("input[name='delete']").val(layoutpath);
            });

            $('#modal_sitevisit_upload').on('show.bs.modal', function (event) {
                $("input:file").change(function () {
                    var fileName = $(this)[0].files[0].name;
                    $("input[name='sitevisit-label']").val(fileName);
                });
            });

            $('#modal_delete_sitevisit').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var sitevisitid = button.data('sitevisitid');
                var sitevisitpath = button.data('sitevisitpath');

                var modal = $(this)
                modal.find('.sitevisitid').text(sitevisitid);
                modal.find("input[name='delete']").val(sitevisitpath);
            });



            $('#modal_miscellaneous_upload').on('show.bs.modal', function (event) {
                $("input:file").change(function () {
                    var fileName = $(this)[0].files[0].name;
                    $("input[name='miscellaneous-label']").val(fileName);
                });
            });

            $('#modal_delete_miscellaneous').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var miscellaneousid = button.data('miscellaneousid');
                var miscellaneouspath = button.data('miscellaneouspath');

                var modal = $(this)
                modal.find('.miscellaneousid').text(miscellaneousid);
                modal.find("input[name='delete']").val(miscellaneouspath);
            });



            $('#modal_configfile_upload').on('show.bs.modal', function (event) {
                $("input:file").change(function () {
                    var fileName = $(this)[0].files[0].name;
                    $("input[name='configfile-label']").val(fileName);
                });
            });
            $('#modal_delete_configfile').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var configfileid = button.data('configfileid');
                var configfilepath = button.data('configfilepath');

                var modal = $(this)
                modal.find('.configfileid').text(configfileid);
                modal.find("input[name='delete']").val(configfilepath);
            });*/



            // drag and drop for nodes
            // TODO: Move to a NodeBrowser specific js page
            try {
                var byId = function (id) {
                    return document.getElementById(id)
                };

                // Multi groups
                Sortable.create(byId('multi'), {
                    animation: 150,
                    draggable: '.tile',
                    handle: '.node-hopper.widget-box.widget-title'
                });

                [].forEach.call(byId('multi').getElementsByClassName('tile__list'), function (el) {
                    Sortable.create(el, {
                        group: 'photo',
                        animation: 150
                    });
                });
            } catch (err) {

            }

            init();
        })
}(angular || {}));



