// wrap in an IIFE
(function(angular, ServiceBase) {
    function CustomerAddressingService($http, $q, API_URL, GlobalSiteVars){
        ServiceBase.call(this, $http, $q, API_URL, GlobalSiteVars, 'customerAddressing');
    }

    CustomerAddressingService.prototype = new ServiceBase();
    CustomerAddressingService.inject = ['$http', '$q', 'API_URL', 'GlobalSiteVars'];

    // setup a base module
    angular.module('nodeModule')
        .service('customerAddressingService', CustomerAddressingService);

}(angular || {}, ServiceBase || {}));
