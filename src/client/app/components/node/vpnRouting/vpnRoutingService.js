// wrap in an IIFE
(function(angular, ServiceBase) {
    function VpnRoutingService($http, $q, API_URL, GlobalSiteVars){
        ServiceBase.call(this, $http, $q, API_URL, GlobalSiteVars, 'vpnrouting');
    }

    VpnRoutingService.prototype = new ServiceBase();
    VpnRoutingService.inject = ['$http', '$q', 'API_URL', 'GlobalSiteVars'];

    // setup a base module
    angular.module('nodeModule')
        .service('vpnRoutingService', VpnRoutingService);

}(angular || {}, ServiceBase || {}));


