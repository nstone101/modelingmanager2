(function(angular) {
    angular.module('nodeModule')
        .service('vpnRoutingColumns', function () {
            var vm = {
                columns: {
                    'audit': [
                        { 'name': '', 'field': 'id',
                            cellTemplate: '<div class="cellTemplatePopUps" title="{{row.entity.ToolTip}}" data-html=true data-placement="right" ><i class="fa fa-info-circle"></i></div>', width: 30, ticked: true, enableColumnMenu: false },
                        { 'name': 'Modify Time', 'field': 'UpdatedTime', width: 90, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'VPNConfigName', 'field': 'VPNConfigName', width: 250, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'MainBranch', 'field': 'MainBranch', width: 250, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'RemoteNode', 'field': 'RemoteNode', width: 125, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'RemoteGatewayName', 'field': 'RemoteGatewayName', width: 125, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'RemoteSubnets', 'field': 'RemoteSubnets', width: 125, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'TunnelInt', 'field': 'TunnelInt', width: 20, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'LocalGwyIp', 'field': 'LocalGwyIp', width: 20, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'TunnelZone', 'field': 'TunnelZone', width: 20, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'TerminationPhyInt', 'field': 'TerminationPhyInt', width: 20, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'TerminationPhyIntZone', 'field': 'TerminationPhyIntZone', width: 20, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'PhyIntVRouter', 'field': 'PhyIntVRouter', width: 20, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'GwyParms', 'field': 'GwyParms', width: 20, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'AutoKeyIKEParms', 'field': 'AutoKeyIKEParms', width: 20, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'PreSharedKey', 'field': 'PreSharedKey', width: 20, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Description', 'field': 'Description', width: 20, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" }
                    ],
                    'all': [
                        { 'displayName': 'VPNConfigName', 'field': 'VPNConfigName', width: 200, ticked: true,                             
                           cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'MainBranch', 'field': 'MainBranch', width: 150, ticked: true,                             
                           cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'RemoteNode', 'field': 'RemoteNode', width: 150, ticked: true,                             
                           cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'RemoteGatewayName', 'field': 'RemoteGatewayName', width: 300, ticked: true,                             
                           cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'RemoteSubnets', 'field': 'RemoteSubnets', width: 200, ticked: true,                             
                           cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'TunnelInt', 'field': 'TunnelInt', width: 300, ticked: true,                             
                           cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'LocalGwyIp', 'field': 'LocalGwyIp', width: 200, ticked: true,                             
                           cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'TunnelZone', 'field': 'TunnelZone', width: 200, ticked: true,                             
                           cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'TerminationPhyInt', 'field': 'TerminationPhyInt', width: 200, ticked: true,                             
                           cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'TerminationPhyIntZone', 'field': 'TerminationPhyIntZone', width: 200, ticked: true,                             
                           cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'PhyIntVRouter', 'field': 'PhyIntVRouter', width: 200, ticked: true,                             
                           cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'GwyParms', 'field': 'GwyParms', width: 200, ticked: true,                             
                           cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'AutoKeyIKEParms', 'field': 'AutoKeyIKEParms', width: 200, ticked: true,                             
                           cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'PreSharedKey', 'field': 'PreSharedKey', width: 200, ticked: true,                             
                           cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'Description', 'field': 'Description', width: 300, ticked: true,                             
                           cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" }
                    ]
                }
            };

            return vm;
        });
}(angular||{}));
