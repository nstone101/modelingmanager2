// wrap in an IIFE
(function(angular, ServiceBase) {
    function FwGoldenConfigService($http, $q, API_URL, GlobalSiteVars){
        ServiceBase.call(this, $http, $q, API_URL, GlobalSiteVars, 'fwgoldenconfig');
    }

    FwGoldenConfigService.prototype = new ServiceBase();
    FwGoldenConfigService.inject = ['$http', '$q', 'API_URL', 'GlobalSiteVars'];

    // setup a base module
    angular.module('nodeModule')
        .service('fwGoldenConfigService', FwGoldenConfigService);

}(angular || {}, ServiceBase || {}));


