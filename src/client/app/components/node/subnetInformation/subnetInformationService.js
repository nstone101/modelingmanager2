// wrap in an IIFE
(function(angular){

    function IpPublicService($http, $q, API_URL, GlobalSiteVars){
        ServiceBase.call(this, $http, $q, API_URL, GlobalSiteVars, 'ipPublic');
    }

    IpPublicService.prototype = new ServiceBase();
    IpPublicService.inject = ['$http', '$q', 'API_URL', 'GlobalSiteVars'];

    function IpPrivateService($http, $q, API_URL, GlobalSiteVars){
        ServiceBase.call(this, $http, $q, API_URL, GlobalSiteVars, 'ipPrivate');
    }

    IpPrivateService.prototype = new ServiceBase();
    IpPrivateService.inject = ['$http', '$q', 'API_URL', 'GlobalSiteVars'];

    function IpIdnService($http, $q, API_URL, GlobalSiteVars){
        ServiceBase.call(this, $http, $q, API_URL, GlobalSiteVars, 'ipIdn');
    }

    IpIdnService.prototype = new ServiceBase();
    IpIdnService.inject = ['$http', '$q', 'API_URL', 'GlobalSiteVars'];

    // setup a base module
    angular.module('nodeModule')
        .service('subnetInformationService', function($http, $q, API_URL, GlobalSiteVars){
            // setup a public api
            var api = {
                ipPrivate: new IpPrivateService($http, $q, API_URL, GlobalSiteVars),
                ipPublic: new IpPublicService($http, $q, API_URL, GlobalSiteVars),
                ipIdn: new IpIdnService($http, $q, API_URL, GlobalSiteVars)
            };

            return api;
        });
}(angular || {}));
