// wrap in an IIFE
(function(angular){
    // setup a base module
    angular.module('nodeModule')
        .service('mx960Service', function($http, $q, API_URL, GlobalSiteVars){
            // setup a public api
            var api = {};

            /**
             *
             * @param payload
             * @returns {*}
             */
            api.read = function(payload){
                var dfd = $q.defer();

                if(payload.mx == undefined){
                    return dfd.reject('MX is required.')
                }
                console.log(payload.vsn);
                if(payload.vsn == 'All' || payload.vsn == 'Revision'){
                    payload.vsn = undefined;
                }
                payload.vsn = payload.vsn || 'all';
                payload.type = payload.type || 'all';

                $http.get(API_URL + '/mx960PortAssignments/read/' + GlobalSiteVars.SiteName + '/' + payload.mx + '/' + payload.vsn + '/' + payload.type )
                    .then(function(resp){
                        dfd.resolve(resp);
                    });

                return dfd.promise;
            };

            /**
             *
             * @param ports
             * @returns {HttpPromise}
             */
            api.delete = function(ports){
                var url = API_URL + '/mx960PortAssignments/delete';

                if(ports.length == 1){
                    url = url + '/' + ports[0];
                    return $http.post(url);
                }

                return $http.post(url, ports);
            };

            /**
             *
             * @param port
             * @returns {HttpPromise}
             */
            api.create = function(port){
                return $http.post(API_URL + '/mx960PortAssignments/create', port);
            };

            /**
             *
             * @param port
             * @returns {HttpPromise}
             */
            api.updatePortAssignment = function(port){
                return $http.post(API_URL + '/mx960PortAssignments/update/'+ port.id, port);
            };

            /**
             *
             * @param val
             * @returns {HttpPromise}
             */
            api.getMasterTags = function(val){
                return $http.get(API_URL + '/getMasterTags/'+val);
            };

            return api;
        });
}(angular || {}));
