(function(angular) {
    angular.module('nodeModule')
        .service('tdrColumns', function () {
            var vm = {
                columns: {
                    'audit': [
                        { 'name': '', 'field': 'id',
                            cellTemplate: '<div class="cellTemplatePopUps" title="{{row.entity.ToolTip}}" data-html=true data-placement="right" ><i class="fa fa-info-circle"></i></div>', width: 30, ticked: true, enableColumnMenu: false },
                        { 'name': 'Modify Time', 'field': 'UpdatedTime', width: 90, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'SBC', 'field': 'SBC', width: 200, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Status', 'field': 'Status', width: 150, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'SBC PairName', 'field': 'SBCPairName', width: 150, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'TDR Server', 'field': 'TDRServerName', width: 300, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'TDR Server IP', 'field': 'TDRServerIP', width: 200, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Port', 'field': 'Port', width: 300, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'IPs', 'field': 'IPs', width: 200, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Comment', 'field': 'Comment', width: 300, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                    ],
                    'all': [
                        { 'displayName': 'SBC', 'field': 'SBC', width: 200, ticked: true,                             cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Status', 'field': 'Status', width: 150, ticked: true,                             cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'SBC PairName', 'field': 'SBCPairName', width: 150, ticked: true,                             cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'TDR Server', 'field': 'TDRServerName', width: 300, ticked: true,                             cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'TDR Server IP', 'field': 'TDRServerIP', width: 200, ticked: true,                             cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Port', 'field': 'Port', width: 300, ticked: true,                             cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'displayName': 'IPs', 'field': 'IPs', width: 200, ticked: true,                             cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Comment', 'field': 'Comment', width: 300, ticked: true,                             cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" }
                    ]
                }
            };

            return vm;
        });
}(angular||{}));
