// wrap in an IIFE
(function(angular, ServiceBase) {
    function TdrService($http, $q, API_URL, GlobalSiteVars){
        ServiceBase.call(this, $http, $q, API_URL, GlobalSiteVars, 'tdr');
    }

    TdrService.prototype = new ServiceBase();
    TdrService.inject = ['$http', '$q', 'API_URL', 'GlobalSiteVars'];

    // setup a base module
    angular.module('nodeModule')
        .service('tdrService', TdrService);

}(angular || {}, ServiceBase || {}));
