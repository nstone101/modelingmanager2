// wrap in an IIFE
(function(angular){
    // setup a base module
    angular.module('nodeModule')
        .service('ipSchemaService', function($http, $q, API_URL, GlobalSiteVars){
            // setup a public api
            var api = {};
            /**
             *
             * @param params
             * @returns {promise|*|d.promise|Q.promise|qFactory.Deferred.promise|m.ready.promise}
             */
            api.read = function(params){
                var dfd = $q.defer();

                console.log(params.vsn);
                if(params.vsn == 'All' || params.vsn == 'Revision'){
                    params.vsn = undefined;
                }
                params.vsn = params.vsn || 'all';
                params.type = params.type || 'all';

                $http.get(API_URL + '/ipschema/read/' + GlobalSiteVars.SiteName + '/' + params.vsn + '/' + params.type )
                    .then(function(resp){
                        dfd.resolve(resp);
                    });

                return dfd.promise;
            };

            /**
             *
             * @param params
             * @returns {HttpPromise}
             */
            api.delete = function(params){
                var url = API_URL + '/ipschema/delete';

                if(params.length == 1){
                    url = url + '/' + params[0];
                    return $http.post(url);
                }

                return $http.post(url, params);
            };

            /**
             *
             * @param params
             * @returns {HttpPromise}
             */
            api.create = function(params){
                return $http.post(API_URL + '/ipschema/create', params);
            };

            /**
             *
             * @param params
             * @returns {HttpPromise}
             */
            api.update = function(params){
                return $http.post(API_URL + '/ipschema/update/'+ params.id, params);
            };

            /**
             *
             * @param val
             * @returns {HttpPromise}
             */
            api.getMasterTags = function(val){
                return $http.get(API_URL + '/getMasterTags/'+val);
            };

            return api;
        });
}(angular || {}));
