// wrap in an IIFE
(function(angular){
    // setup a base module
    angular.module('nodeModule')
        .service('ns5400Service', function($http, $q, API_URL, GlobalSiteVars){
            // setup a public api
            var api = {};
            /**
             * get ns5400PortAssignments data
             * @param mx
             * @param vsn
             * @param type
             * @returns {*}
             */
            api.read = function(payload){
                var dfd = $q.defer();

                if(payload.ns == undefined){
                    return dfd.reject('NS is required.')
                }
                console.log(payload.vsn);
                if(payload.vsn == 'All' || payload.vsn == 'Revision'){
                    payload.vsn = undefined;
                }
                payload.vsn = payload.vsn || 'all';
                payload.type = payload.type || 'all';

                $http.get(API_URL + '/ns5400PortAssignments/read/' + GlobalSiteVars.SiteName + '/' + payload.ns + '/' + payload.vsn + '/' + payload.type )
                    .then(function(resp){
                        dfd.resolve(resp);
                    });

                return dfd.promise;
            };

            /**
             * delete  ns5400 ports
             * @param ports
             * @returns {HttpPromise}
             */
            api.delete = function(ports){
                var url = API_URL + '/ns5400PortAssignments/delete';

                if(ports.length == 1){
                    url = url + '/' + ports[0];
                    return $http.post(url);
                }

                return $http.post(url, ports);
            };

            /**
             * create new port assignment
             * @param port
             * @returns {HttpPromise}
             */
            api.create = function(port){
                return $http.post(API_URL + '/ns5400PortAssignments/create', port);
            };

            /**
             * update current port assignment
             * @param port
             * @returns {HttpPromise}
             */
            api.update = function(port){
                return $http.post(API_URL + '/ns5400PortAssignments/update/'+ port.id, port);
            };

            /**
             * get master tags
             * @param val
             * @returns {HttpPromise}
             */
            api.getMasterTags = function(val){
                return $http.get(API_URL + '/getMasterTags/'+val);
            };

            return api;
        });
}(angular || {}));
