/**
 * Node Page App
 */
// avoid the object not defined error
(function (angular) {
    'use strict';
    // setup the app module
    angular.module('nodeModule', [
        'ui.bootstrap',
        'cgBusy',
        'ui.grid',
        'ui.grid.pagination',
        'ui.grid.cellNav',
        'ui.grid.edit',
        'ui.grid.resizeColumns',
        'ui.grid.pinning',
        'ui.grid.selection',
        'ui.grid.moveColumns',
        'ui.grid.exporter',
        'ui.grid.grouping',
        'ui.grid.autoResize',
        'isteven-multi-select',
        'commonModule'
    ]);
}(angular || {}));
