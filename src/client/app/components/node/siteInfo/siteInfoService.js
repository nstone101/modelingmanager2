// wrap in an IIFE
(function(angular, ServiceBase) {
    function SiteInfoService($http, $q, API_URL, GlobalSiteVars){
        ServiceBase.call(this, $http, $q, API_URL, GlobalSiteVars, 'siteInfo');
    }

    SiteInfoService.prototype = new ServiceBase();
    SiteInfoService.inject = ['$http', '$q', 'API_URL', 'GlobalSiteVars'];

    // setup a base module
    angular.module('nodeModule')
        .service('siteInfoService', SiteInfoService);

}(angular || {}, ServiceBase || {}));
