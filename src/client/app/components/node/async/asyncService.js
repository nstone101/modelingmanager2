// wrap in an IIFE
(function(angular, ServiceBase) {
    function AsyncService($http, $q, API_URL, GlobalSiteVars){
        ServiceBase.call(this, $http, $q, API_URL, GlobalSiteVars, 'async');
    }

    AsyncService.prototype = new ServiceBase();
    AsyncService.inject = ['$http', '$q', 'API_URL', 'GlobalSiteVars'];

    // setup a base module
    angular.module('nodeModule')
        .service('asyncService', AsyncService);

}(angular || {}, ServiceBase || {}));
