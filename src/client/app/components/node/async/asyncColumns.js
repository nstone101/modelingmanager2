(function(angular) {
    angular.module('nodeModule')
        .service('asyncColumns', function () {
            var vm = {
                columns: {
                    'audit': [
                        { 'name': '', 'field': 'id',
                            cellTemplate: '<div class="cellTemplatePopUps" title="{{row.entity.ToolTip}}" data-html=true data-placement="right" ><i class="fa fa-info-circle"></i></div>', width: 30, ticked: true, enableColumnMenu: false },
                        { 'name': 'Modify Time', 'field': 'UpdatedTime', width: 90, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Row ID', 'field': 'RowID', width: 50, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Serial', 'field': 'Serial', width: 125, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Bay', 'field': 'Bay', width: 80, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'HostName', 'field': 'HostName', width: 80, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Description', 'field': 'Description', width: 300, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Async', 'field': 'Async', width: 200, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'insert':row.entity.Action=='insert', 'delete':row.entity.Action=='delete', 'update':row.entity.Action=='update' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" }
                    ],
                    'all': [
                        { 'name': 'Serial', 'field': 'Serial', width: 200, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Bay', 'field': 'Bay', width: 80, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Hostname', 'field': 'Hostname', width: 200, ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Description', 'field': 'Description', ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                        { 'name': 'Async', 'field': 'ASYNC', ticked: true,
                            cellTemplate: "<div ng-class=\"{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }\" title=\"TOOLTIP\">{{COL_FIELD CUSTOM_FILTERS}}</div>" },
                    ]
                }
            };

            return vm;
        });
}(angular||{}));
