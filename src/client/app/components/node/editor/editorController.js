(function(angular){
    angular.module('nodeModule')
        .controller('editorController', function ($rootScope, $scope, $modal, editorService) {
            var vm = {
                state: 'create', // create or edit
                labels: {
                    'create': {
                        'title': 'New Entry',
                        'subTitle': 'Create New Entry',
                        'submit': 'Create'
                    },
                    'update': {
                        'title': 'Update Entry',
                        'subTitle': 'Update Existing Entry',
                        'submit': 'Update'
                    }
                },
                vsnOptions: [],
                form: {},
                animationsEnabled: true,
                events: {
                    modalOpen: '',
                    gridUpdate: ''
                },
                getMasterTags: function(val){
                    return editorService.getMasterTags(val)
                        .then(function(resp){
                            // do something here
                            return resp.data.map(function(item){
                                return item.Tag;
                            })
                        });
                }
            };

            $scope.$viewValue= '';

            /**
             * @param params
             *          state - create
             *                  update
             *          row - set of data to display
             *          SiteName - current site name
             *          templateUrl - url for the editor form
             *          service - the service associated with the grid controller
             *          gridUpdateEventName
             */
            $scope.open = function (params) {
                // cleanup anu old data
                console.log(params);
                if(params.state)
                    vm.state = params.state;
                if(params.row != undefined)
                    vm.form = params.row;
                else {
                    vm.form.SiteName = params.SiteName.toUpperCase();
                }
                vm.events.modalOpen = params.modalOpenEventName;
                vm.events.gridUpdate = params.gridUpdateEventName;
                vm.vsnOptions = params.nodeVsns;
                vm.masterTags = params.masterTags;
                vm.form.UpdatedTime = moment(new Date).format('YYYY-MM-DD HH:mm:ss');
                vm.active = true;
                var modalInstance = $modal.open({
                    animation: vm.animationsEnabled,
                    template: params.template,
                    controller: 'ModalInstanceCtrl',
                    size: 'lg',
                    resolve: {
                        vm: function () {
                            return $scope.vm;
                        }
                    }
                });

                modalInstance.result.then(function (formData) {
                    if(vm.state == 'create'){
                        // a new item doesn't have a row id.
                        delete formData.id;
                        params.service.create(formData).then(function(){
                            updateGrid();
                        });
                    }
                    if(vm.state == 'update'){
                        params.service.update(formData).then(function(){
                            updateGrid();
                        });
                    }
                }, function (args) {
                    console.info('Modal dismissed at: ' + new Date());
                });
            };

            function updateGrid(){
                delete $scope.vm.form;
                $scope.vm.form = {};
                $rootScope.$bus.publish(vm.events.gridUpdate);
            }

            function init(){
                $rootScope.$bus.subscribe('GRID_EDITOR_OPEN', function(params){

                    $scope.open(params);

                });
                $scope.vm = vm;
            }

            init();
        })
        .controller('ModalInstanceCtrl', function ($scope, $modalInstance, vm) {
            $scope.vm = vm;
            $scope.selected = {
                form: $scope.vm.form
            };

            $scope.ok = function () {
                $modalInstance.close($scope.selected.form);
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        });
}(angular || {}));