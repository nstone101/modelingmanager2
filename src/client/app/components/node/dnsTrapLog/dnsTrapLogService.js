// wrap in an IIFE
(function(angular, ServiceBase) {
    function DnsTrapLogService($http, $q, API_URL, GlobalSiteVars){
        ServiceBase.call(this, $http, $q, API_URL, GlobalSiteVars, 'dnsTrapLog');
    }

    DnsTrapLogService.prototype = new ServiceBase();
    DnsTrapLogService.inject = ['$http', '$q', 'API_URL', 'GlobalSiteVars'];

    // setup a base module
    angular.module('nodeModule')
        .service('dnsTrapLogService', DnsTrapLogService);

}(angular || {}, ServiceBase || {}));
