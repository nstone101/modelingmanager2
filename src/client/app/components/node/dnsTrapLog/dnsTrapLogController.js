/**
 * UI-Grid
 * http://ui-grid.info/docs/#/tutorial/314_external_pagination
 * Here are the grid docs. There is a lot of functionality with this grid so far.
 */
(function (angular) {
    // setup a base module
    angular.module('nodeModule')
        .controller('dnsTrapLogController',
        function ($rootScope, $scope, $timeout, $templateCache,
                  dialogBoxService, dnsTrapLogService, dnsTrapLogColumns,
                  uiGridConstants, uiGridExporterConstants, GlobalSiteVars, BASE_URL) {
            var vm = {
                siteName: GlobalSiteVars.SiteName,
                pageType: GlobalSiteVars.PageType,
                loadingPromise: {},
                gridApi: {},
                gridButtons: {
                    "selected": "1"
                },
                multiSelectColumns: (GlobalSiteVars.PageType == 'audit') ? dnsTrapLogColumns.columns[GlobalSiteVars.PageType].slice(2) : dnsTrapLogColumns.columns[GlobalSiteVars.PageType].slice(1),
                shownColumns: {},
                filterValue: "",
                dnsTrapLogFilterValue: "",
                options: {
                    enableSorting: true,
                    enableRowSelection: true,
                    enableSelectionBatchEvent: true,
                    enableFullRowSelection: true,
                    treeRowHeaderAlwaysVisible: true,
                    multiSelect: true,
                    enableSelectAll: true,
                    selectionRowHeaderWidth: 35,
                    paginationPageSizes: [25, 50, 75],
                    paginationPageSize: 25,
                    columnDefs: dnsTrapLogColumns.columns[GlobalSiteVars.PageType],
                    exporterLinkLabel: "",
                    exporterPdfDefaultStyle: {fontSize: 9},
                    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                    exporterPdfOrientation: "landscape",
                    exporterPdfPageSize: "LETTER",
                    exporterPdfMaxGridWidth: 1400,
                    rowTemplate: $templateCache.get('vsnGridRow.html')
                }
            };

            vm.filter = function () {
                vm.gridApi.grid.refresh();
            };

            vm.filterColumns = function (renderableRows) {
                var matcher = new RegExp(vm.filterValue);
                renderableRows.forEach(function (row) {
                    var match = false;
                    vm.options.columnDefs.forEach(function (column) {
                        var field = column.field;
                        if (row.entity[field] && row.entity[field].match(matcher)) {
                            match = true;
                        }
                    });

                    if (!match) {
                        row.visible = false;
                    }
                    else {
                        row.visible = true;
                    }
                });

                return renderableRows;
            };

            vm.options.onRegisterApi = function (gridApi) {
                vm.gridApi = gridApi;
                vm.gridApi.grid.registerRowsProcessor(vm.filterColumns, 200);
                vm.gridApi.core.on.columnVisibilityChanged($scope, function (column) {
                    vm.toggleSelectColumns(column);
                });

                vm.gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                    var selections = vm.gridApi.selection.getSelectedRows();
                    updateEditDeleteButtons(selections.length);
                });

                vm.gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                    var selections = vm.gridApi.selection.getSelectedRows();
                    updateEditDeleteButtons(selections.length);
                });
            };

            vm.toggleGridColumns = function (data) {
                var index = _.findIndex(vm.options.columnDefs, {field: data.field});
                vm.options.columnDefs[index].visible = data.ticked;
                vm.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
            };

            vm.toggleSelectColumns = function (data) {
                var index = _.findIndex(vm.options.columnDefs, {field: data.field});
                vm.options.columnDefs[index].ticked = data.visible;
            };

            vm.openModal = function (state) {
                console.log(state);
                if (state == 'update') {
                    var row = vm.gridApi.selection.getSelectedRows();
                    vm.updateRow = row[0];
                }
                else {
                        vm.updateRow = {};
                    }

                    $rootScope.$bus.publish('GRID_EDITOR_OPEN', {
                        "SiteName": vm.siteName,
                        "state": state,
                        "nodeVsns": GlobalSiteVars.NodeVsns,
                        "masterTags": GlobalSiteVars.MasterTags,
                        "row": vm.updateRow,
                        "service": dnsTrapLogService,
                        "gridUpdateEventName": 'DNSTRAPLOGGING_DATA_UPDATE',
                        "template": $templateCache.get('components/node/dnsTrapLog/dnsTrapLogEditorForm.html')

                    });
                };

            vm.csvExporter = function () {
                var csvLink = angular.element(document.querySelectorAll(".custom-csv-link-location"));
                vm.gridApi.exporter.csvExport(uiGridExporterConstants.ALL, uiGridExporterConstants.ALL, csvLink);
            };

            vm.pdfExporter = function () {
                vm.gridApi.exporter.pdfExport(uiGridExporterConstants.ALL, uiGridExporterConstants.ALL);
            };

            vm.deleteRows = function () {
                console.log('delete ports');

                dialogBoxService.show(
                    dialogBoxService.dialogType.confirm,
                    'Delete Selected Rows',
                    "Do you wish to delete the selected row(s)?", "",
                    function(result){
                        if(!result) return;
                        var rows = _.map(vm.gridApi.selection.getSelectedRows(),
                            function (item) {
                                return item.id
                            });

                        dnsTrapLogService.delete(rows)
                            .then(function (resp) {
                                getGridData();
                                vm.gridApi.core.notifyDataChange(uiGridConstants.dataChange.ALL);
                            });

                        updateEditDeleteButtons(0);
                    });
            };

            function getGridData() {

                vm.loadingPromise = dnsTrapLogService.read({
                    vsn: vm.vsnFilter, type: vm.pageType
                }).then(function (resp) {
                    if(GlobalSiteVars.PageType != 'audit') {
                        vm.options.data = resp.data;
                        vm.gridApi.core.notifyDataChange(uiGridConstants.dataChange.ALL);
                        updateEditDeleteButtons(0);
                        return;
                    }

                    // process the updates. the first row of the update is what has changed...
                    // the first row will go into the tooltip...
                    // grab all the
                    var toolTipTemplate = '<div style="z-index:999">' +
                        '<span>Edited By: %editBy%</span>'  +
                        '<br/>'  +
                        '<p>%updateValues%</p>' +
                        '<br/>' +
                        '<span>Comment:</span>' +
                        '<p>%comment%</p>' +
                        '</div>';
                    _.map(resp.data, function(row, index){
                        var curId = 0, prevId = 0;

                        // these are the only ones we care about.
                        if(row.Action == 'update') {
                            curId = row.RowID;
                            prevId = (resp.data[index-1]) ? resp.data[index-1]['RowID'] : 0;
                            // TODO: get some time to actually think about doing this....
                            // ugh, need to do a field by field comparision...
                            if(prevId == curId) {
                                // already processed, move along.
                                // we only care about the first update of the group.
                                delete(resp.data[index-1]);
                                return;
                            }
                            var updateVals = '';
                            if(resp.data[index+1]) {
                                _.map(row, function (field, key) {
                                    if (key == "id" || key == 'ToolTip') return;
                                    if (field != resp.data[index + 1][key]) {
                                        if(field != null) {
                                            updateVals += "Field: " + key + "<br/>" + "Previous Value: " + field + "<br/>";
                                        }
                                    }
                                });
                                resp.data[index + 1].ToolTip = toolTipTemplate.replace('%editBy%', row['EditedBy']).replace('%updateValues%', updateVals).replace('%comment%', row['Comment']);
                                updateVals = '';
                            }
                            else {
                                resp.data.ToolTip = toolTipTemplate.replace('%editBy%', row['EditedBy']).replace('%updateValues%', updateVals).replace('%comment%', row['Comment']);
                            }
                        }
                        else {
                            resp.data.ToolTip = toolTipTemplate.replace('%editBy%', row['EditedBy']).replace('%updateValues%', updateVals).replace('%comment%', row['Comment']);
                        }
                    });
                    vm.options.data = resp.data;
                    vm.gridApi.core.notifyDataChange(uiGridConstants.dataChange.ALL);
                    $timeout(function(){
                        angular.element('body').tooltip({html: true, placement: 'right', selector: '.cellTemplatePopUps', container: 'body'});
                    }, 100);
                });
            }

            function updateEditDeleteButtons(length) {
                // no edit or delete on the audit tables
                if(GlobalSiteVars.PageType == 'audit') return;

                if (length == 1) {
                    vm.displayDeleteBtn = true;
                    vm.displayEditBtn = true;
                }
                else if (length > 1) {
                    vm.displayDeleteBtn = true;
                    vm.displayEditBtn = false;
                }
                else if (length < 1) {
                    vm.displayDeleteBtn = false;
                    vm.displayEditBtn = false;
                }
            }

            function init() {
                $scope.localLang = {
                    selectAll: "Tick all",
                    selectNone: "Tick none",
                    reset: "Undo all",
                    search: "Type here to search...",
                    nothingSelected: "Nothing is selected"         //default-label is deprecated and replaced with this.
                };

                $rootScope.$bus.subscribe('NODE_VNS_FILTER', function (params) {

                    vm.vsnFilter = params.vsnFilter;
                    if(vm.vsnFilter == 'Revision') {
                        vm.pageType = 'audit';
                    }
                    else{
                        vm.pageType = 'all';
                    }
                    if(angular.element(document.getElementById('t_dnstrap')).hasClass('active') == false) return;
                    getGridData();
                });

                $rootScope.$bus.subscribe('DNSTRAPLOGGING_DATA_UPDATE', function(){
                    getGridData();
                });

                var filterThrottled = _.debounce(vm.filter, 500);
                $scope.$watch('vm.dnsTrapLogFilterValue', function (newVal, oldVal) {
                    vm.filterValue = vm.dnsTrapLogFilterValue;
                    filterThrottled($scope);
                });


                $scope.vm = vm;
            }
            init();
        });
}(angular || {}));
