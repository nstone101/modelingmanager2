// wrap in an IIFE
(function(angular, ServiceBase) {
    function NodeDeviceNameService($http, $q, API_URL, GlobalSiteVars){
        ServiceBase.call(this, $http, $q, API_URL, GlobalSiteVars, 'nodeDeviceName');
    }

    NodeDeviceNameService.prototype = new ServiceBase();
    NodeDeviceNameService.inject = ['$http', '$q', 'API_URL', 'GlobalSiteVars'];

    // setup a base module
    angular.module('nodeModule')
        .service('nodeDeviceNameService', NodeDeviceNameService);

}(angular || {}, ServiceBase || {}));
