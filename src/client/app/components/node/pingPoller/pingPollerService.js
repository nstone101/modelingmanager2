// wrap in an IIFE
(function(angular, ServiceBase) {
    function PingPollerService($http, $q, API_URL, GlobalSiteVars){
        ServiceBase.call(this, $http, $q, API_URL, GlobalSiteVars, 'pingpoller');
    }

    PingPollerService.prototype = new ServiceBase();
    PingPollerService.inject = ['$http', '$q', 'API_URL', 'GlobalSiteVars'];

    // setup a base module
    angular.module('nodeModule')
        .service('pingPollerService', PingPollerService);

}(angular || {}, ServiceBase || {}));


