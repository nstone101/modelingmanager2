(function (angular) {
	angular.module('nodeModule')
		.controller('nodeDocsController', function ($scope, $rootScope, $modal, nodeDocsService) {

			$scope.init = function(admin){
				$scope.siteName = nodeDocsService.getSiteName();
				//console.log($scope.siteName);
				$scope.isAdmin = admin;

				//get config data for each type of document
				var docConfig = function(resp) {
					$scope.docConfig = resp;
					//console.log($scope.docConfig);
				};
				nodeDocsService.getConfig(docConfig);

				//get documents list of documents on the server
				$scope.docDataLoaded = function(resp){
					$scope.docData = resp;
					//console.log($scope.docData);
				};
nodeDocsService.getDocs($scope.siteName, $scope.docDataLoaded);

				window.addEventListener("dragover",function(e){
					e = e || event;
					console.log(e);
					console.log(e.target);
					console.log(e.target.parentNode.id);
					e.preventDefault();

				},false);
				window.addEventListener("drop",function(e){
					e = e || event;
					e.preventDefault();
				},false);
			};

			$rootScope.$on('fileAdded', function (file) {
nodeDocsService.getDocs($scope.siteName, $scope.docDataLoaded);
			});
		})
		.controller('DeleteModalCtrl', function ($scope, $modal) {
			$scope.open = function (docConfig, doc, index) {
				$scope.delDocData = {
					docConfig: docConfig,
					doc:doc,
					index:index
				};
				var modalInstance = $modal.open({
					templateUrl: 'components/node/nodeDocs/deleteDocModal.html',
					controller: 'DeleteModalInstanceCtrl',
					resolve: {
						delDocData: function () {
							return $scope.delDocData;
						}
					}
				});
			}
		})
		.controller('DeleteModalInstanceCtrl', function ($scope, $modalInstance, delDocData, nodeDocsService) {
			$scope.siteName = nodeDocsService.getSiteName();
			$scope.docConfig = delDocData.docConfig;
			$scope.filename = delDocData.doc.replace(/^.*[\\\/]/, '');
			$scope.doc = delDocData.doc;
			$scope.index = delDocData.index;


			$scope.removeFile = function(docConfig, doc){
				var removed = function(resp){
					//TODO: need some sort of notification to the user.
					if(resp = 'success'){
						$modalInstance.dismiss('cancel');
					}
				};
				nodeDocsService.removeFile(docConfig, doc, removed);
			};


			$scope.cancel = function (event) {
				event.preventDefault();
				$modalInstance.dismiss('cancel');
			};
		})
		.controller('UploadModalCtrl', function ($scope, $modal) {
			$scope.open = function (docConfig) {
				$scope.uploadDocData = {
					docConfig: docConfig
				};

				var modalInstance = $modal.open({
					templateUrl: 'components/node/nodeDocs/uploadDocModal.html',
					controller: 'UploadModalInstanceCtrl',
					resolve: {
						uploadDocData: function () {
							return $scope.uploadDocData;
						}
					}
				});
			}
		})
		.controller('UploadModalInstanceCtrl', function ($scope, $modalInstance, uploadDocData, nodeDocsService) {
			$scope.siteName = nodeDocsService.getSiteName();
			$scope.docConfig = uploadDocData.docConfig;

			$scope.cancel = function (event) {
				event.preventDefault();
				$modalInstance.dismiss('cancel');
			};
		});
})(angular||{});