(function (angular) {
	// setup the app module
	angular.module('nodeModule')
		.factory('nodeDocsService', function($http, $rootScope, GlobalSiteVars) {

			var siteName = GlobalSiteVars.SiteName;
			var files = {};
			var urlBase = '/api/nodedocs/all/';
			var docConfig = [
				{
					"title": "ISPECs for this node:",
					"name": "ISPEC",
					"docType": "ispecs",
					"directory": "ispec",
					"logFile": "_ispec_audit.log"
				},
				{
					"title": "MOPs for this node:",
					"name": "MOP",
					"docType": "mops",
					"directory": "mops",
					"logFile": "_mop_audit.log"
				},
				{
					"title": "Layouts for this node:",
					"name": "Layout",
					"docType": "layouts",
					"directory": "layouts",
					"logFile": "_layouts_audit.log"
				},
				{
					"title": "Failovers for this node:",
					"name": "Failover",
					"docType": "failovers",
					"directory": "failovers",
					"logFile": "_failovers_audit.log"
				},
				{
					"title": "Site Visits for this node:",
					"name": "Site Visit",
					"docType": "sitevisits",
					"directory": "sitevisits",
					"logFile": "_sitevisit_audit.log"
				},
				{
					"title": "Miscellaneous items for this node:",
					"name": "Miscellaneous",
					"docType": "miscellaneous",
					"directory": "miscellaneous",
					"logFile": "_miscellaneous_audit.log"
				},
				{
					"title": "Config Files for this node:",
					"name": "Config",
					"docType": "configfiles",
					"directory": "configfiles",
					"logFile": "_configfile_audit.log"
				}
			];

			return {
				getSiteName: function(){
					return siteName;
				},

				// This could be external or pulled from db
				getConfig: function (callback) {
					/*$http.get('data/jobs.json')
						.then(function(resp){
							callback(resp);
						});*/
					callback(docConfig);
				},
				getDocs: function(siteName, callback){
					$http.get(urlBase + siteName)
						.then(function(resp){
							files = resp.data.docs;
							//console.log(files);
							if(callback){
								callback(files);
							} else{
								return files;
							}
						});
				},
				removeFile: function(config, doc, callback) {
					var formData = {
						"name": config.name,
						"docType": config.docType,
						"directory": config.directory,
						"logFile": config.logFile,
						"doc": doc,
						"siteName": siteName
					};

					$http({
						method  : 'POST',
						url     : '/node/docs/delete',
						data    : formData,
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					})
						.then(function(resp){
							callback(resp);
							var index = files[config.docType].indexOf(doc);
							//console.log(index);
							files[config.docType].splice(index, 1);
						});
				}
			}
		});
}(angular || {}));