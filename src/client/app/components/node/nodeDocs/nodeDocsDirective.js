(function() {
	'use strict';
	angular.module('nodeModule')
		.directive('nodeDocsDirective', function($rootScope, BASE_URL) {
			return {
				restrict: 'EA',
				replace: true,
				transclude: true,
				scope: {
					siteName: '=',
					isAdmin: "=",
					docConfig: "=docConfig",
					docData: "=docData"
				},
				templateUrl: 'components/node/nodeDocs/docsContainer.tpl.html',
				link: function(scope, elm, attr) {
					scope.isCollapsed = true;
				}
			};
		});
}());
