/**
 * Help Popups
 */
// avoid the object not defined error
(function (angular) {
	'use strict';

	// setup the app module
	angular.module('helpModule', [
		'ngBootbox'
	]);
}(angular || {}));