// TODO: Will refactor this into a directive later.

(function (angular) {
	angular.module('helpModule')
		.controller('helpController',
		function ($rootScope, $scope, helpService, $timeout, $templateCache) {

			$scope.currentTip = 0;

			$scope.init = function(page){
				//console.log(page);

				var setData = function(res) {
					$scope.helpData = res;
					$scope.dataReady();
				};
				helpService.get(setData);
			};

			$scope.dataReady = function(){
				//console.log($scope.helpData);
				angular.element('.help-btn').on('click',function(event){
					$scope.currentTip = 0;
					angular.element('#help-overlay').show();
					angular.element('#pageLoading').show();
					angular.element('#pageLoading span').hide();
					$timeout(function() {
						angular.element('.node-tip0').focus();
					},100);

					angular.element('body').on('click', function (e) {
						if(e.target == angular.element('#help-overlay')[0]){
							$scope.exitHelp();
						}
					});
				});
			};

			$scope.spotClicked = function (event, id){
				$scope.currentTip = id;
				if($scope.currentTip != id){
					$timeout(function() {
						angular.element('.node-tip' + $scope.currentTip).focus();
					},100);
				}
			};

			$scope.previousTip = function(event){
				if($scope.currentTip == 0){
					$scope.exitHelp();
				}else{
					$scope.currentTip--;
					$timeout(function() {
						angular.element('.node-tip' + $scope.currentTip).focus();
					},100);
				}
			};

			$scope.nextTip = function(event){
				if($scope.currentTip == $scope.helpData.length-1){
					$scope.exitHelp();
				}else{
					$scope.currentTip++;
					$timeout(function() {
						angular.element('.node-tip' + $scope.currentTip).focus();
					},100);
				}
			};

			$scope.exitHelp = function(event){
				angular.element('#help-overlay').hide();
				angular.element('#pageLoading').hide();
			};
		});

})(angular||{});