(function (angular) {
	// setup the app module
	angular.module('helpModule')
		.factory('helpService', function($http) {

			var helpData = [
				{"label": "Group", "placement": "right", "content": "This module is a grouping of Verizon Nodes. The group a node belongs to is controlled by the users on the Node Info Page. Nodes listed under the Decommission group are targeted for deletion and may be deleted by a Verizon Admin."},
				{"label": "Legend", "placement": "left", "content": "This drop down list is a legend of node generation types and node states."},
				{"label": "Control Components", "placement": "left", "content": "The single arrow button allows one to hide or show the nodes within a group. The four way arrow button allows one to change the order of groups on the Node Browser Page."},
				{"label": "Node", "placement": "right", "content": "This icon represents a Verizon VoIP Node. The icon shows the node’s generation type and current state. Clicking on the icon takes the user to pages about the node such as configuration data, photos, documents and diagrams."},
				{"label": "Control Component", "placement": "right", "content": "This icon allows the user to collapse and expand the side menu."},
				{"label": "Side Menu", "placement": "right", "content": "Menu options available to the user are displayed on the side menu. The user may use the side menu to navigate to the different areas in the Portal."}
			];

			return {
				get: function (callback) {
					/*$http.get('data/jobs.json')
					 .then(function(res){
					 helpData = res.data;
					 callback(helpData);
					 });*/

					callback(helpData);
				}
			}
		});
}(angular || {}));