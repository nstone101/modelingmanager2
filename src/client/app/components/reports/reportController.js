(function(angular){

    angular.module('app')
        .controller('reportController', function(){
            $('.accordion-toggle').click(function() {
                var image = $('img', this)[0];
                var imageSrc = image.src;
                var imageBase = imageSrc.substring(imageSrc.lastIndexOf('/') + 1);


                if (imageBase == "details_open.png") {
                    $('img', this).attr( 'src', "/assets/images/details_close.png" );
                    $(this).children().css('background-color', '#F8E0E0');
                } else {
                    $('img', this).attr( 'src', "/assets/images/details_open.png" );
                    $(this).children().css('background-color', '#F2F2F2');
                }

            });


            function globalsearch(){

                var searchtext=$("#globalsearch").val();
                if(searchtext!=""){

                    $.ajax({
                        type:"post",
                        url:"/ajax/reports/fglobalsearch",
                        data:"searchtext="+searchtext,
                        success:function(data){
                            $("#globalsearchresult").html(data);
                            $("#globalsearch").val("");
                        },
                        error: function(req, err){ console.log('my message' + err); }
                    });
                }
            }

            $("#globalsearchbutton").click(function(){
                globalsearch()
            });

            $('#globalsearch').keyup(function(e) {
                if(e.keyCode == 13) {
                    globalsearch();
                }
            });
        });
}(angular||{}));