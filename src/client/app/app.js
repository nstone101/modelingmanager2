/**
 * User Page App
 */
var _USER_ = _USER_ || {};

(function (angular, globals, user) {
    window.app = window.app || {};

    'use strict';
    // setup the app module
    angular.module('app', [
        'ui.router',
        'ui.select',
        'angularValidator',
        'ngBootbox',
        'ngImgCrop',
        'commonModule',
        'nodeModule',
	    'userModule',
		'adminUsersModule',
        'helpModule',
        'portal.dashboard'
	])
    .constant('API_URL', window.location.protocol + '//' + window.location.host + '/api')
    .constant('BASE_URL', window.location.protocol + '//' + window.location.host)
    .constant('User', user)
    .constant('GlobalSiteVars', globals)

    .config(($stateProvider, $urlRouterProvider) => {
        //$urlRouterProvider.when('/', '/dashboard')
        $stateProvider
            .state('app', {
                abstract: true,
                controller: 'layoutController',
                templateUrl: 'components/_layout/layout.html'
            });

    })
    // TODO: add angular in a way that will allow us to listen a hash change event
    .run(function ($rootScope, $window, $http, User) {
        $window.app = $window.app || {};
        $window.bootbox = bootbox;

        if(!User)
            User = $window.app.user;
    });
}(angular || {}, __globals__ || {}, _USER_ || {}));
