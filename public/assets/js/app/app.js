/**
 * Node Page App
 */

(function(angular, globals){
    'use strict';
    // setup the app module
    angular.module('app', [
        'commonModule',
        'nodeModule',
	    'userModule'
    ])
    .constant('API_URL', window.location.protocol + '//' + window.location.host + '/api')
    .constant('BASE_URL', window.location.protocol + '//' + window.location.host)
    .constant('GlobalSiteVars', globals)
    .config(function ($locationProvider) {
        $locationProvider.html5Mode(true).hashPrefix();
    })
    // TODO: add angular in a way that will allow us to listen a hash change event
    .run(function ($rootScope) {
        $rootScope.$watch(function(){
            return location.hash;
        },
        function(hash){
            console.log('hash has changed...', hash);
        });
    });
}(angular || {}, __globals__ || {}));
