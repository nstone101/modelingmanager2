var assets = {};

// Load up some CSS
assets.css = [
    //'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800',
    "/assets/css/bootstrap.css",
    "/assets/css/bootstrap-theme.css",
    "/assets/css/jquery-ui.css",
    "/assets/css/unicorn.css",
    "/assets/css/uniform.css",
    "/assets/css/font-awesome.css",
    "/assets/css/lightbox.css",
    "/assets/css/datatables-colvis.css",
    "/assets/css/datatables-editor.css",
    "/assets/css/bootstrap-multiselect.css",
    "/assets/css/portal.css",
    "/assets/css/styles.css",
    "/assets/css/nodeSiteData.css",
    "/assets/css/bootstrap-multiselect.css",
    "/assets/css/build/angular-bundle.css",
    "/assets/css/build/app-bundle.css",
    "/assets/css/uiGridPages.css",

    //TODO: Temporary css - will be moved to appropriate less files.
    "/assets/css/_temp.css"
];
head.load(assets.css);

// Load up base JS

head.load([
    //{ jquery: "/assets/js/lib/jquery.js" },
    { jquery: '/assets/js/lib/build/angular-bundle.js' },
    //{ sortable: "/assets/js/lib/sortable/Sortable.min.js"},
    { jquery_flip: "/assets/js/lib/jquery/jquery.flip.js" },
    { jquery_ui: "/assets/js/lib/jquery-ui.js" },
    { sortable: "/assets/js/lib/sortable/Sortable.min.js"},
    { jquery_lazyload: "/assets/js/lib/jquery/jquery.lazyload.js" },
    { jquery_validate: "/assets/js/lib/jquery/jquery.validate.js" },
    { jquery_hashchange: "/assets/js/lib/jquery/jquery.hashchange.js" },
    { jquery_form: "/assets/js/lib/jquery/jquery.form.js" },
    { datatables: "/assets/js/lib/datatables.js" },
    { datatables_fillwidth: "/assets/js/lib/datatables/datatables.fillwidth.js" },
    { datatables_colreorder: "/assets/js/lib/datatables/datatables.colreorderwithresize.js" },
    { datatables_rowgrouping: "/assets/js/lib/datatables/datatables.rowgrouping.js" },
    { datatables_colvis: "/assets/js/lib/datatables/datatables.colvis.js" },
    { datatables_tabletools: "/assets/js/lib/datatables/datatables.tabletools.js" },
    { datatables_fixedcolumns: "/assets/js/lib/datatables/datatables.fixedcolumns.js" },
    { datatables_editor: "/assets/js/lib/datatables/datatables.editor.js" },
    { bootstrap3_typeahead: "/assets/js/lib/bootstrap3-typeahead.min.js"},
    { bootstrap_multiselect: "/assets/js/lib/bootstrap-multiselect.js"},


    /*{ bootbox: "/assets/js/lib/bootbox.js" },*/
    { encoder: "/assets/js/lib/encoder.js" },
    { lightbox: "/assets/js/lib/lightbox.js" },
    { unicorn: "/assets/js/lib/unicorn.js" },
    { greensock: "/assets/js/lib/greensock/minified/TweenMax.min.js"}

], function(){

    assets.js = [

        { app_bundle: "/assets/js/lib/build/app-bundle.js" },
       /* { report: "/assets/js/app/components/reports/reportController.js" },
        { portal: "/assets/js/app/components/portal/portalController.js" },*/

        /*
	    { app: "/assets/js/app/app.js" },
        { nodeModule: "/assets/js/app/components/node/nodeModule.js" },
        { nodeController: "/assets/js/app/components/node/nodeController.js" },
        { commonModule: "/assets/js/app/common/commonModule.js" },
        { dialogService: "/assets/js/app/common/services/dialogBoxService.js" },
        { serviceBase: "/assets/js/app/common/services/serviceBase.js" },

        { editorController: "/assets/js/app/components/node/editor/editorController.js"},
        { editorService: "/assets/js/app/components/node/editor/editorService.js"},

        { mx960Service: "/assets/js/app/components/node/mx960PortAssignments/mx960Service.js" },
        { mx960VColumns: "/assets/js/app/components/node/mx960PortAssignments/mx960Columns.js" },
        { mx960Controller: "/assets/js/app/components/node/mx960PortAssignments/mx960Controller.js" },

        { ns5400Service: "/assets/js/app/components/node/ns5400PortAssignments/ns5400Service.js" },
        { ns5400Columns: "/assets/js/app/components/node/ns5400PortAssignments/ns5400Columns.js" },
        { ns5400Controller: "/assets/js/app/components/node/ns5400PortAssignments/ns5400Controller.js" },

        { ipSchemaService: "/assets/js/app/components/node/ipSchema/ipSchemaService.js" },
        { ipSchemaColumns: "/assets/js/app/components/node/ipSchema/ipSchemaColumns.js" },
        { ipSchemaController: "/assets/js/app/components/node/ipSchema/ipSchemaController.js" },

        { customerAddressingService: "/assets/js/app/components/node/customerAddressing/customerAddressingService.js" },
        { customerAddressingColumns: "/assets/js/app/components/node/customerAddressing/customerAddressingColumns.js" },
        { customerAddressingController: "/assets/js/app/components/node/customerAddressing/customerAddressingController.js" },

        { networkConnectionsService: "/assets/js/app/components/node/networkConnections/networkConnectionsService.js" },
        { networkConnectionsColumns: "/assets/js/app/components/node/networkConnections/networkConnectionsColumns.js" },
        { networkConnectionsController: "/assets/js/app/components/node/networkConnections/networkConnectionsController.js" },

        { cardSlottingService: "/assets/js/app/components/node/cardSlotting/cardSlottingService.js" },
        { cardSlottingColumns: "/assets/js/app/components/node/cardSlotting/cardSlottingColumns.js" },
        { cardSlottingController: "/assets/js/app/components/node/cardSlotting/cardSlottingController.js" },

        { subnetInformationService: "/assets/js/app/components/node/subnetInformation/subnetInformationService.js" },
        { subnetInformationColumns: "/assets/js/app/components/node/subnetInformation/subnetInformationColumns.js" },
        { subnetInformationController: "/assets/js/app/components/node/subnetInformation/subnetInformationController.js" },

        { dnsTrapLogService: "/assets/js/app/components/node/dnsTrapLog/dnsTrapLogService.js" },
        { dnsTrapLogColumns: "/assets/js/app/components/node/dnsTrapLog/dnsTrapLogColumns.js" },
        { dnsTrapLogController: "/assets/js/app/components/node/dnsTrapLog/dnsTrapLogController.js" },

        { subnetAdvService: "/assets/js/app/components/node/subnetAdv/subnetAdvService.js" },
        { subnetAdvColumns: "/assets/js/app/components/node/subnetAdv/subnetAdvColumns.js" },
        { subnetAdvController: "/assets/js/app/components/node/subnetAdv/subnetAdvController.js" },

        { asyncService: "/assets/js/app/components/node/async/asyncService.js" },
        { asyncColumns: "/assets/js/app/components/node/async/asyncColumns.js" },
        { asyncController: "/assets/js/app/components/node/async/asyncController.js" },

        { tdrService: "/assets/js/app/components/node/tdr/tdrService.js" },
        { tdrColumns: "/assets/js/app/components/node/tdr/tdrColumns.js" },
        { tdrController: "/assets/js/app/components/node/tdr/tdrController.js" },

        { nodeDeviceNameService: "/assets/js/app/components/node/nodeDeviceName/nodeDeviceNameService.js" },
        { nodeDeviceNameColumns: "/assets/js/app/components/node/nodeDeviceName/nodeDeviceNameColumns.js" },
        { nodeDeviceNameController: "/assets/js/app/components/node/nodeDeviceName/nodeDeviceNameController.js" },

        { siteInfoService: "/assets/js/app/components/node/siteInfo/siteInfoService.js" },
        { siteInfoColumns: "/assets/js/app/components/node/siteInfo/siteInfoColumns.js" },
        { siteInfoController: "/assets/js/app/components/node/siteInfo/siteInfoController.js" }*/
    ];

    head.load(assets.js, function(){
        //console.log('angular loaded');
        //angular.bootstrap(document, ['app']);
        var interval = setInterval(function(){
            if(angular != undefined) {
                angular.bootstrap(document, ['app']);
                spinner.stop();
                $("#pageLoading").hide();
                $(function($){
                    $("#card").flip({
                      trigger: 'click'
                    });
                    //console.log($("#card"));

                    $("#card2").flip({
                      trigger: 'click',
                      speed: 5
                    });

                     $("#card3").flip({
                      trigger: 'click',
                          speed: 5
                    });

                    $("#card4").flip({
                      trigger: 'click'
                    });
                });
                clearInterval(interval);
            }
        },10);

    });


});



