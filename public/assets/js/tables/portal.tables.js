$(function(){

// defines the variables for the window height, the tablename for window resizing,
// and defaults the tableHeight to 0
    var tableName = undefined;
    var tableHeight = 0;
    var removeData = ""; // array of the row data when removing a row.

// defaults the table variables for all available tables
    var asyncTable;
    var nodeDeviceNamesTable;
    var siteInfoTable;
    var tdrTable;
    var tagsTable;
    var customerTable;
    var netConnectionsTable;
    var customerCircuitsTable;
    var mx01PortsTable;
    var mx02PortsTable;
    var ns01PortsTable;
    var ns02PortsTable;
    var ipSchemaTable;
    var publicIPTable;
    var privateIPTable;
    var idnIPTable;
    var dnsTrapTable;
    var subnetAdvTable;
    var cardSlottingTable;
    var wz_asyncTable;

// These are the editor variables to make tables editable.
    var asyncEditor;
    var nodeDeviceNamesEditor;
    var siteInfoEditor;
    var tagsEditor;
    var tdrEditor;
    var customerInfoEditor;
    var netConnectionsEditor;
    var customerCircuitsEditor;
    var mx01Editor;
    var mx02Editor;
    var ns01Editor;
    var ns02Editor;
    var ipSchemaEditor;
    var publicIPEditor;
    var privateIPEditor;
    var idnIPEditor;
    var dnsTrapEditor;
    var subnetAdvEditor;
    var cardSlottingEditor;
    var rolesEditor;
    var wz_asyncEditor;


    var vsnOpts = [
        "BASE",
        "BROADSOFT",
        "DSCIP",
        "GENBAND",
        "IP-IVR",
        "IPAC",
        "ISCIP",
        "MX VC",
        "NSRS",
        "OCCAS",
        "PIP NNI",
        "SIDA"
    ];

    /**
     * Function definitions
     */

// This function is used to make a row grouping that includes the subnet and network mask.
    function RowGroupSubnet(group, networkTab){
        var json = new Array(group, networkTab);
        json = JSON.stringify( json );
        var networkString = '';
        // We then take our JSON encoded table row, and pass it to a php file which builds an SQL insert statement
        $.ajax({
            type: 'GET',
            async: false,
            url: "/ajax/datatables/subnet_details?json=" + json,
            success: function (data) {
                networkString = " - " + data;
            }
        });
        return networkString;
    }

// This function highlights rows that are for a VSN other than 'BASE' to color code them for easier viewing.
    function vsnHighlight(table, vsnCol) {
        // Make sure the table is ready, otherwise, keep recursion going
        if (typeof(table) != "undefined") {
            // Grab all rows of the table, and loop through them.
            var oRow = table.dataTable().$('tr');
            for (var i = 0; i < oRow.length; i++) {
                // Single out the current row.
                var row = oRow[i].getElementsByTagName('td');
                // Color the row based on the information inside the VSN Column
                $(oRow[i]).addClass(row[vsnCol].innerHTML.toLowerCase().replace(" ", "-"));
            }
            // Hide the VSN column once all highlighting has happened, passing the column to hide,
            // false sets it to hidden, and true forces a redraw of the table afterwards.
            //
            // These first tables have a hidden column in slot 0 (the group by column), so we need to hide the next one over.
            if (["ipSchema", "customerTable", "privateAddress", "idnAddress", "publicAddress", "dnsTrap"].indexOf(tableName) >= 0) {
                table.fnSetColumnVis(vsnCol+1, false, true);
            } else {
                table.fnSetColumnVis(vsnCol, false, true);
            }
        }
        // If the table wasn't ready yet, call this function again in 100 milliseconds.
        // FIXME: not sure that this is working. When loading the tables on the VPN, sometimes a table
        // will load, but never be highlighted.
        else {
            setTimeout(function() {vsnHighlight(table, vsnCol);}, 100);
        }
    }

    /**
     * End of Function Definitions
     */

    /**
     * edit node contact info
     */
    $("#frm_siteinfo").submit( function(e) {

        e.preventDefault();

        $("#frmfrm_siteinfo_siteinfo .form-control").attr('disabled', true);
        $(this).hide();
        $("#EditSiteInfo").show();
        $("#AddVSN").hide();
        $("#Group").show();

        var formData = $(this).serialize();
        var posting = $.post( '/ajax/site_info', formData );

        posting.done( function (result) {

            // send off the audit entry
            formData = formData + '&Created=0&Removed=0';
            $.post( '/ajax/datatables/site-info-audit/audit', formData );

            $('#modal_siteinfo').modal('hide');
            window.location.reload(true);
        });
    });

    $('#frm_diagram_upload').submit( function(e) {
        var options = {
            target:        '.upload-message',
            beforeSubmit:  function() {
                // validation code
            },
            uploadProgress: OnProgress,
            success:  function(responseText, statusText, xhr, $form) {
                if ( responseText == 'success' ) {
                    $('#modal_diagram_upload').modal('hide');
                    window.location.reload(true);
                } else {
                    $('.upload-message').html('Error: ' + responseText);
                }
            }
        };

        $(this).ajaxSubmit(options);
        return false; //prevent page navigation
    });

    $('#frm_photo_upload').submit( function(e) {
        var options = {
            target: '.upload-message',
            beforeSubmit:  function() {
                // validation code
                $('.progress-bar').css('width', 0+'%').attr('aria-valuenow', 0);
            },
            uploadProgress: OnProgress,
            success:  function(responseText, statusText, xhr, $form) {
                if ( responseText == 'success' ) {
                    $('#modal_photo_upload').modal('hide');
                    window.location.reload(true);
                } else {
                    $('.upload-message').html('Error: ' + responseText);
                }
            }
        };

        $(this).ajaxSubmit(options);
        return false; //prevent page navigation
    });


    function OnProgress(event, position, total, percentComplete)
    {
        //Progress bar
        jQuery('.progressbox').show();
        jQuery('#statustxt').show();
        //jQuery('#progressbar').width(percentComplete + '%') //update progressbar percent complete
        $('.progress-bar').css('width', percentComplete+'%').attr('aria-valuenow', aria-valuenow);
        jQuery('#statustxt').html(percentComplete + '%'); //update status text
        if(percentComplete == 100) {
            jQuery('.progressbox').hide();
        }
    }





// this gets called when the delete image button is clicked on the "Site Photos" tab
    $("a.image-del").click( function(e) {
        e.preventDefault(); // prevent page navigation
        var img = $(this).attr('name');
        //var img_id = img.replace(".", "_");

        bootbox.dialog(
            {
                message: "Are you sure you would like to delete <code>" + img + "</code> from " + sitename + " site photos?<br><br><em><strong>WARNING:</strong></em> this action is not reversable!",
                title: "Are you Sure?",
                className: "my-modal",
                buttons: {
                    cancel: {
                        label: "Cancel",
                        className: "btn-inverse",
                        "callback": function() {
                            // do nothing
                        }
                    },
                    Delete: {
                        label:"Delete",
                        className: "btn-danger",
                        callback: function() {
                            var post_data = 'site=' + sitename + '&image=' + img;
                            var posting = $.post( '/ajax/site-photos/delete', post_data );

                            posting.done(function (result) {
                                //$('#li_' + img_id).remove();
                                window.location.reload(true);
                            });
                        }
                    }
                }
            }
        );
    });

// This runs one ajax to create a zip file of all the site photos for this site,
// and on success, redirects them to download that zip file.
    $("#lnk_sitephotos_download").click(function(e) {

        e.preventDefault();
        bootbox.dialog(
            "Please wait, processing ZIP archive ...",
            []
        );
        $.get( '/ajax/site-photos/mkzip', function (fname) {
            bootbox.hideAll();
            window.location.href = '/download/e/'+fname+'/'+sitename+'_site_photos.zip';
        });
    });

    /**
     * Start of the Table Generation Functions.
     */
// table generation for the Master Tags editor
    $("#tbl_mastertag").ready(function() {
        if (typeof(tagsTable) == "undefined"){
            setTimeout(function () {
                tagsEditor = new $.fn.dataTable.Editor( {
                    "domTable": "#tbl_mastertag",
                    "ajaxUrl": "/ajax/datatables/mastertags",
                    "fields" : [
                        {
                            // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                            label: "Tag:",
                            name: "Tag",
                            type: "text",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.Tag ); }
                        },{
                            label: "Last Updated",
                            name: "UpdatedTime",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                        },{
                            label: "Last Edited By",
                            name: "EditedBy",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                        },{
                            label: "Comment",
                            name: "Comment",
                            type: "textarea",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                        }
                    ]
                });

                tagsEditor.on('onInitEdit', function () {
                    this.show('UpdatedTime');
                    this.show('EditedBy');
                    this.show('Comment');
                    this.disable('UpdatedTime');
                    this.disable('EditedBy');
                });

                tagsEditor.on('onInitCreate', function () {
                    this.hide('UpdatedTime');
                    this.hide('EditedBy');
                    this.hide('Comment');
                    this.set('Comment', "Create");
                });

                // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                tagsEditor.on('initRemove', function (e, node, data) {
                    removeData = data[0];
                    removeData['Created'] = 0;
                    removeData['Removed'] = 1;
                });

                // Set the last edited by field to the current username before submitting data
                tagsEditor.on('preSubmit', function (e, data, action) {
                    data['data']['EditedBy'] = username;
                });

                // Fires when a successful edit/creation has happened to copy the row to the audit table.
                tagsEditor.on('submitSuccess', function (e, json, data) {
                    data['Removed'] = 0;
                    data['Created'] = (data['Comment'] === "" ? 1 : 0);
                    $.ajax({ url: '/ajax/datatables/mastertags-audit/audit',
                        data: data,
                        type: 'post'
                    });
                });

                // Fires right after a remove happens to copy the row to the audit table.
                tagsEditor.on('remove', function (e, json) {
                    $.ajax({ url: '/ajax/datatables/mastertags-audit/audit',
                        data: removeData,
                        type: 'post'
                    });
                });

                tagsTable = $('#tbl_mastertag').dataTable({
                    //"sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/mastertags",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "sDom": '<"table-header"ClTfiR>t',
                    "oTableTools": {
                        "sRowSelect": "single",
                        "aButtons": [
                            {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":tagsEditor},
                            {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":tagsEditor},
                            {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":tagsEditor},
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]}
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": "id", "sWidth": "30px" },
                        { "mData": "Tag", "sWidth": "350px" }
                    ]
                });
            }, 10);
        }
    });

    function load_async() {
        $("#tab-name").empty().append("Async");

        if (typeof(asyncTable) == "undefined"){
            setTimeout(function () {
                asyncEditor = new $.fn.dataTable.Editor( {
                    "domTable": "#async-table",
                    "ajaxUrl": "/ajax/datatables/async",
                    "fields": [
                        {
                            // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                            "label": "Site Name",
                            "name": "SiteName",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                        },{  //FIXME: VSN needs to be a dropdown populated by an AJAX request retrieving the available VSNs.
                            "label": "VSN",
                            "name": "VSN",
                            "type": "select",
                            "ipOpts": vsnOpts,
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                        },{
                            "label": "Serial",
                            "name": "Serial",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.Serial ); }
                        },{
                            "label": "Bay",
                            "name": "Bay",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.Bay ); }
                        },{
                            "label": "Hostname",
                            "name": "Hostname",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.Hostname ); }
                        },{
                            "label": "Description",
                            "name": "Description",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.Description ); }
                        },{
                            "label": "ASYNC",
                            "name": "ASYNC",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.ASYNC ); }
                        },{
                            "label": "OS Version",
                            "name": "OSVersion",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.OSVersion ); }
                        },{
                            "label": "Last Updated",
                            "name": "UpdatedTime",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                        },{
                            "label": "Last Edited By",
                            "name": "EditedBy",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                        },{
                            "label": "Comment",
                            "name": "Comment",
                            "type": "textarea",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                        }
                    ]
                });

                // These functions execute on click of edit and create, and can hide these fields from the form
                asyncEditor.on('onInitEdit', function () {
                    this.hide('SiteName');
                    this.show('UpdatedTime');
                    this.show('EditedBy');
                    this.show('Comment');
                    this.disable('SiteName');
                    this.disable('Hostname');
                    this.disable('UpdatedTime');
                    this.disable('EditedBy');
                });

                asyncEditor.on('onInitCreate', function () {
                    //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                    //for (var j = 0; j < siteDataSplit.length; j++){
                    //    siteDataSplit[j] = siteDataSplit[j].trim();
                    //}
                    this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                    this.hide('SiteName');
                    this.hide('UpdatedTime');
                    this.hide('EditedBy');
                    this.hide('Comment');
                    this.set('Comment', "Create");
                });

                // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                asyncEditor.on('initRemove', function (e, node, data) {
                    removeData = data[0];
                    removeData['Created'] = 0;
                    removeData['Removed'] = 1;
                });

                // Set the last edited by field to the current username before submitting data
                asyncEditor.on('preSubmit', function (e, data, action) {
                    data['data']['EditedBy'] = username;
                });

                // Fires when a successful edit/creation has happened to copy the row to the audit table.
                asyncEditor.on('submitSuccess', function (e, json, data) {
                    data['Removed'] = 0;
                    data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                    $.ajax({ url: '/ajax/datatables/async-audit/audit',
                        data: data,
                        type: 'post'
                    });
                });

                // Fires right after a remove happens to copy the row to the audit table.
                asyncEditor.on('remove', function (e, json) {
                    $.ajax({ url: '/ajax/datatables/async-audit/audit',
                        data: removeData,
                        type: 'post'
                    });
                });

                asyncTable = $('#async-table').dataTable({
                    "sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/async",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "bStateSave": false,

                    /**   the sDom parameters that we are using:
                     * <"table-header"> puts all the contained items within the table-header div for sizing and display on firefox.
                     * 'C' is the column visibilty button.  'l' is "length changing" according to DT site, not sure what it is
                     * 'T' are the Table tools buttons, copy, csv, print, etc.  'f' is the filter search bar
                     * 'i' is the information showing the number of viewable entries. 'R' is the ability to resize columns
                     * 't' is the table data itself.  'F' will autofill the widths of the columns to the screen width
                     */
                    "sDom": '<"table-header"ClTFfiR>t',
                    "aaSorting": [[ 4, "asc" ]],
                    "oTableTools": {
                        "sRowSelect": "single",
                        "aButtons": [
                            {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":asyncEditor},
                            {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":asyncEditor},
                            {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":asyncEditor},
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]}
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": "VSN", "sWidth": "80px" },
                        { "mData": "Serial", "sWidth": "16%" },
                        { "mData": "Bay", "sWidth": "11%" },
                        { "mData": "Hostname", "sWidth": "16%" },
                        { "mData": "Description", "sWidth": "37%" },
                        { "mData": "ASYNC", "sWidth": "20%" },
                        { "mData": "OSVersion", "sWidth": "20%" }
                    ]
                });


            }, 10);
        }
        setTimeout(function() { vsnHighlight(asyncTable, 0); }, 200);
    };

    function load_nodedevicenames() {


// \ChromeLogger::log("in load_nodedevicenames() portal.tables.js");

        $("#tab-name").empty().append("NodeDeviceNames");

        if (typeof(nodeDeviceNamesTable) == "undefined"){
            setTimeout(function () {
                nodeDeviceNamesEditor = new $.fn.dataTable.Editor( {
                    "domTable": "#nodedevicenames-table",
                    "ajaxUrl": "/ajax/datatables/nodedevicenames",
                    "fields": [
                        {
                            // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                            "label": "Site Name",
                            "name": "SiteName",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                        },{
                            "label": "VSN",
                            "name": "VSN",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                        },{
                            "label": "Primary Device Tag",
                            "name": "PrimaryDeviceTag",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.PrimaryDeviceTag ); }
                        },{
                            "label": "Node Device Name",
                            "name": "NodeDeviceName",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.NodeDeviceName ); }
                        },{
                            "label": "Device Abbreviation",
                            "name": "DeviceAbbreviation",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.DeviceAbbreviation ); }
                        },{
                            "label": "Device Type",
                            "name": "DeviceType",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.DeviceType ); }
                        },{
                            "label": "Device Description",
                            "name": "DeviceDescription",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.DeviceDescription ); }
                        },{
                            "label": "Last Updated",
                            "name": "UpdatedTime",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                        },{
                            "label": "Last Edited By",
                            "name": "EditedBy",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                        },{
                            "label": "Comment",
                            "name": "Comment",
                            "type": "textarea",
                            data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                        }
                    ]
                });

                // These functions execute on click of edit and create, and can hide these fields from the form
                nodeDeviceNamesEditor.on('onInitEdit', function () {
                    this.hide('SiteName');
                    this.show('UpdatedTime');
                    this.show('EditedBy');
                    this.show('Comment');
                    this.disable('UpdatedTime');
                    this.disable('EditedBy');
                });

                nodeDeviceNamesEditor.on('onInitCreate', function () {
                    this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                    this.hide('SiteName');
                    this.hide('UpdatedTime');
                    this.hide('EditedBy');
                    this.hide('Comment');
                    this.set('Comment', "Create");
                });

                // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                nodeDeviceNamesEditor.on('initRemove', function (e, node, data) {
                    removeData = data[0];
                    removeData['Created'] = 0;
                    removeData['Removed'] = 1;
                });

                // Set the last edited by field to the current username before submitting data
                nodeDeviceNamesEditor.on('preSubmit', function (e, data, action) {
                    data['data']['EditedBy'] = username;
                });

                // Fires when a successful edit/creation has happened to copy the row to the audit table.
                nodeDeviceNamesEditor.on('submitSuccess', function (e, json, data) {
                    data['Removed'] = 0;
                    data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                    $.ajax({ url: '/ajax/datatables/nodedevicenames-audit/audit',
                        data: data,
                        type: 'post'
                    });
                });

                // Fires right after a remove happens to copy the row to the audit table.
                nodeDeviceNamesEditor.on('remove', function (e, json) {
                    $.ajax({ url: '/ajax/datatables/nodedevicenames-audit/audit',
                        data: removeData,
                        type: 'post'
                    });
                });

                nodeDeviceNamesTable = $('#nodedevicenames-table').dataTable({
                    "sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/nodedevicenames",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "bStateSave": false,

                    /**the sDom parameters that we are using:
                     * <"table-header"> puts all the contained items within the table-header div for sizing and display on firefox.
                     * 'C' is the column visibilty button.  'l' is "length changing" according to DT site, not sure what it is
                     * 'T' are the Table tools buttons, copy, csv, print, etc.  'f' is the filter search bar
                     * 'i' is the information showing the number of viewable entries. 'R' is the ability to resize columns
                     * 't' is the table data itself.  'F' will autofill the widths of the columns to the screen width
                     */
                    "sDom": '<"table-header"ClTFfiR>t',
                    "aaSorting": [[ 4, "asc" ]],
                    "oTableTools": {
                        "sRowSelect": "single",
                        "aButtons": [
                            {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":nodeDeviceNamesEditor},
                            {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":nodeDeviceNamesEditor},
                            {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":nodeDeviceNamesEditor},
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]}
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": "VSN", "sWidth": "80px" },
                        { "mData": "PrimaryDeviceTag", "sWidth": "16%" },
                        { "mData": "NodeDeviceName", "sWidth": "11%" },
                        { "mData": "DeviceAbbreviation", "sWidth": "16%" },
                        { "mData": "DeviceType", "sWidth": "37%" },
                        { "mData": "DeviceDescription", "sWidth": "20%" },
                    ]
                });


            }, 10);
        }
        setTimeout(function() { vsnHighlight(nodeDeviceNamesTable, 0); }, 200);
    };


// table generation for the TDRInfo table
    function load_tdrinfo() {
        $("#tab-name").empty().append("TDR");
        if (typeof(tdrTable) == "undefined") {
            setTimeout(function () {
                $(document).ready(function(){
                    tdrEditor = new $.fn.dataTable.Editor( {
                        "domTable": "#tdr-table",
                        "ajaxUrl": "/ajax/datatables/tdrinfo",
                        "fields" : [
                            {
                                // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                                "label": "Site Name",
                                "name": "SiteName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                            },{
                                "label": "VSN",
                                "name": "VSN",
                                "type": "select",
                                "ipOpts": vsnOpts,
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                            },{
                                "label": "SBC:",
                                "name": "SBC",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SBC ); }
                            },{
                                "label": "Status:",
                                "name": "Status",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Status ); }
                            },{
                                "label": "SBC Pair Name",
                                "name": "SBCPairName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SBCPairName ); }
                            },{
                                "label": "TDR Server Name",
                                "name": "TDRServerName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.TDRServerName ); }
                            },{
                                "label": "TDR Server IP",
                                "name": "TDRServerIP",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.TDRServerIP ); }
                            },{
                                "label": "Server Port",
                                "name": "Port",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Port ); }
                            },{
                                "label": "Source IP",
                                "name": "IPs",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.IPs ); }
                            },{
                                "label": "Last Updated",
                                "name": "UpdatedTime",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                            },{
                                "label": "Last Edited By",
                                "name": "EditedBy",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                            },{
                                "label": "Comment",
                                "name": "Comment",
                                "type": "textarea",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                            }
                        ]
                    });

                    // These functions execute on click of edit and create, and can hide these fields from the form
                    tdrEditor.on('onInitEdit', function () {
                        this.hide('SiteName');
                        this.show('UpdatedTime');
                        this.show('EditedBy');
                        this.show('Comment');
                        this.disable('SiteName');
                        this.disable('VSN');
                        this.disable('SBC');
                        this.disable('UpdatedTime');
                        this.disable('EditedBy');
                    });

                    tdrEditor.on('onInitCreate', function () {
                        //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                        //var siteDataSplit = document.getElementById("site-header").innerHTML.split("-");
                        //for (var j = 0; j < siteDataSplit.length; j++){
                        //    siteDataSplit[j] = siteDataSplit[j].trim();
                        //}
                        this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                        this.hide('SiteName');
                        this.hide('UpdatedTime');
                        this.hide('EditedBy');
                        this.hide('Comment');
                        this.set('Comment', "Create");
                    });

                    // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                    tdrEditor.on('initRemove', function (e, node, data) {
                        removeData = data[0];
                        removeData['Created'] = 0;
                        removeData['Removed'] = 1;
                    });

                    // Set the last edited by field to the current username before submitting data
                    tdrEditor.on('preSubmit', function (e, data, action) {
                        data['data']['EditedBy'] = username;
                    });

                    // Fires when a successful edit/creation has happened to copy the row to the audit table.
                    tdrEditor.on('submitSuccess', function (e, json, data) {
                        data['Removed'] = 0;
                        data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                        $.ajax({ url: '/ajax/datatables/tdr-audit/audit',
                            data: data,
                            type: 'post'
                        });
                    });

                    // Fires right after a remove happens to copy the row to the audit table.
                    tdrEditor.on('remove', function (e, json) {
                        $.ajax({ url: '/ajax/datatables/tdr-audit/audit',
                            data: removeData,
                            type: 'post'
                        });
                    });

                    tdrTable = $('#tdr-table').dataTable({
                        "sScrollX": "100%",
                        "sScrollY": "100%",
                        "sAjaxSource": "/ajax/datatables/tdrinfo",
                        "bScrollCollapse": true,
                        "bJQueryUI": true,
                        "bPaginate": false,
                        "sDom": '<"table-header"ClTFfiR>t',
                        "aaSorting": [[ 0, "asc" ]],
                        "oTableTools": {
                            "sRowSelect": "single",
                            "aButtons": [
                                {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":tdrEditor},
                                {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":tdrEditor},
                                {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":tdrEditor},
                                {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                    {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                                ]}
                            ],
                            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                        },
                        "aoColumns": [
                            { "mData": "VSN", "sWidth": "80px" },
                            { "mData": "SBC", "sWidth": "20%" },
                            { "mData": "Status", "sWidth": "10%" },
                            { "mData": "SBCPairName", "sWidth": "20%" },
                            { "mData": "TDRServerName","sWidth": "20%" },
                            { "mData": "TDRServerIP","sWidth": "20%" },
                            { "mData": "Port", "sWidth": "10%" },
                            { "mData": "IPs", "sWidth": "20%" }
                        ]
                    });
                });
            }, 10);
        }
        setTimeout(function() { vsnHighlight(tdrTable, 0); }, 200);
    };

// table generation for the Customer Addressing table
    function load_custaddr() {
        $("#tab-name").empty().append("Customer Information");

        // generate the table
        tableHeight = 275;
        tableName = "customerTable";
        if (typeof(customerTable) == "undefined"){
            setTimeout(function () {
                $(document).ready(function(){
                    customerInfoEditor = new $.fn.dataTable.Editor( {
                        "domTable": "#customer-table",
                        "ajaxUrl": "/ajax/datatables/customeraddressing",
                        "fields" : [
                            {
                                // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                                "label": "Site Name",
                                "name": "SiteName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                            },{
                                "label": "VSN",
                                "name": "VSN",
                                "type": "select",
                                "ipOpts": vsnOpts,
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                            },{
                                "label": "SPIP:",
                                "name": "SPIP",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SPIP ); }
                            },{
                                "label": "Host Offset:",
                                "name": "HostOffset",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.HostOffset ); }
                            },{
                                "label": "IP Address:",
                                "name": "IPAddress",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.IPAddress ); }
                            },{
                                "label": "Device:",
                                "name": "Device",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Device ); }
                            },{
                                "label": "Description:",
                                "name": "Description",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Description ); }
                            },{
                                "label": "Last Updated",
                                "name": "UpdatedTime",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                            },{
                                "label": "Last Edited By",
                                "name": "EditedBy",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                            },{
                                "label": "Comment",
                                "name": "Comment",
                                "type": "textarea",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                            }
                        ]
                    });

                    // These functions execute on click of edit and create, and can hide these fields from the form
                    customerInfoEditor.on('onInitEdit', function () {
                        this.hide('SiteName');
                        this.show('UpdatedTime');
                        this.show('EditedBy');
                        this.show('Comment');
                        this.disable('SiteName');
                        this.disable('VSN');
                        this.disable('Device');
                        this.disable('UpdatedTime');
                        this.disable('EditedBy');
                    });

                    customerInfoEditor.on('onInitCreate', function () {
                        //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                        //var siteDataSplit = document.getElementById("site-header").innerHTML.split("-");
                        //for (var j = 0; j < siteDataSplit.length; j++){
                        //    siteDataSplit[j] = siteDataSplit[j].trim();
                        //}
                        this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                        this.hide('SiteName');
                        this.hide('UpdatedTime');
                        this.hide('EditedBy');
                        this.hide('Comment');
                        this.set('Comment', "Create");
                    });

                    // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                    customerInfoEditor.on('initRemove', function (e, node, data) {
                        removeData = data[0];
                        removeData['Created'] = 0;
                        removeData['Removed'] = 1;
                    });

                    // Set the last edited by field to the current username before submitting data
                    customerInfoEditor.on('preSubmit', function (e, data, action) {
                        data['data']['EditedBy'] = username;
                    });

                    // Fires when a successful edit/creation has happened to copy the row to the audit table.
                    customerInfoEditor.on('submitSuccess', function (e, json, data) {
                        data['Removed'] = 0;
                        data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                        $.ajax({ url: '/ajax/datatables/cust-addr-audit/audit',
                            data: data,
                            type: 'post'
                        });
                    });

                    // Fires right after a remove happens to copy the row to the audit table.
                    customerInfoEditor.on('remove', function (e, json) {
                        $.ajax({ url: '/ajax/datatables/cust-addr-audit/audit',
                            data: removeData,
                            type: 'post'
                        });
                    });

                    customerTable = $('#customer-table').dataTable({
                        //"sScrollX": "100%",
                        "sScrollY": "100%",
                        "sAjaxSource": "/ajax/datatables/customeraddressing",
                        "bScrollCollapse": true,
                        "bJQueryUI": true,
                        "bPaginate": false,
                        // "bSort": false,
                        "aaSorting": [[ 1, "desc" ]],
                        "sDom": '<"table-header"ClTfiR>t',
                        "oTableTools": {
                            "sRowSelect": "single",
                            "aButtons": [
                                {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":customerInfoEditor},
                                {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":customerInfoEditor},
                                {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":customerInfoEditor},
                                {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                    {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                                ]}
                            ],
                            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                        },
                        "aoColumns": [
                            { "mData": "SPIP", "sWidth": "10%" },
                            { "mData": "VSN", "sWidth": "80px" },
                            { "mData": "HostOffset", "sWidth": "10%" },
                            { "mData": "IPAddress", "sWidth": "15%" },
                            { "mData": "Device", "sWidth": "35%" },
                            { "mData": "Description", "sWidth": "30%" }
                        ]
                    }).rowGrouping({"bExpandableGrouping": true, "sGroupingClass": 'group'});
                });
            }, 10);
        }
        setTimeout(function() { vsnHighlight(customerTable, 0); }, 200);
    };

// table generation for the Network Connections table
    function load_netconn() {
        $("#tab-name").empty().append("Network Connections");

        // generate the table
        tableHeight = 470;
        tableName = "netConnectionsTable";
        if (typeof(netConnectionsTable) == "undefined"){
            setTimeout(function () {
                $(document).ready(function(){
                    netConnectionsEditor = new $.fn.dataTable.Editor( {
                        "domTable": "#networkConnections-table",
                        "ajaxUrl": "/ajax/datatables/networkconn",
                        "fields" : [
                            {
                                // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                                "label": "Site Name",
                                "name": "SiteName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                            },{
                                "label": "VSN",
                                "name": "VSN",
                                "type": "select",
                                "ipOpts": vsnOpts,
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                            },{
                                "label": "Node Device:",
                                "name": "NodeDevice",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.NodeDevice ); }
                            },{
                                "label": "Node Interface:",
                                "name": "NodeInterface",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.NodeInterface ); }
                            },{
                                "label": "Interface Tag:",
                                "name": "InterfaceTag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.InterfaceTag ); }
                            },{
                                "label": "Remote Device Name:",
                                "name": "RemoteDeviceName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.RemoteDeviceName ); }
                            },{
                                "label": "Network:",
                                "name": "Network",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Network ); }
                            },{
                                "label": "Speed:",
                                "name": "Speed",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Speed ); }
                            },{
                                "label": "Remote Device Type:",
                                "name": "RemoteDeviceType",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.RemoteDeviceType ); }
                            },{
                                "label": "Remote Device Interface:",
                                "name": "RemoteDeviceInt",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.RemoteDeviceInt ); }
                            },{
                                "label": "Remote Device Circuit:",
                                "name": "RemoteDeviceCircuit",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.RemoteDeviceCircuit ); }
                            },{
                                "label": "Description:",
                                "name": "Description",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Description ); }
                            },{
                                "label": "Last Updated",
                                "name": "UpdatedTime",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                            },{
                                "label": "Last Edited By",
                                "name": "EditedBy",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                            },{
                                "label": "Comment",
                                "name": "Comment",
                                "type": "textarea",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                            }
                        ]
                    });

                    // These functions execute on click of edit and create, and can hide these fields from the form
                    netConnectionsEditor.on('onInitEdit', function () {
                        this.hide('SiteName');
                        this.show('UpdatedTime');
                        this.show('EditedBy');
                        this.show('Comment');
                        this.disable('SiteName');
                        this.disable('VSN');
                        this.disable('NodeDevice');
                        this.disable('RemoteDeviceName');
                        this.disable('UpdatedTime');
                        this.disable('EditedBy');
                    });

                    netConnectionsEditor.on('onInitCreate', function () {
                        //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                        //var siteDataSplit = document.getElementById("site-header").innerHTML.split("-");
                        //for (var j = 0; j < siteDataSplit.length; j++){
                        //    siteDataSplit[j] = siteDataSplit[j].trim();
                        //}
                        this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                        this.hide('SiteName');
                        this.hide('UpdatedTime');
                        this.hide('EditedBy');
                        this.hide('Comment');
                        this.set('Comment', "Create");
                    });

                    // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                    netConnectionsEditor.on('initRemove', function (e, node, data) {
                        removeData = data[0];
                        removeData['Created'] = 0;
                        removeData['Removed'] = 1;
                    });

                    // Set the last edited by field to the current username before submitting data
                    netConnectionsEditor.on('preSubmit', function (e, data, action) {
                        data['data']['EditedBy'] = username;
                    });

                    // Fires when a successful edit/creation has happened to copy the row to the audit table.
                    netConnectionsEditor.on('submitSuccess', function (e, json, data) {
                        data['Removed'] = 0;
                        data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                        $.ajax({ url: '/ajax/datatables/net-conn-audit/audit',
                            data: data,
                            type: 'post'
                        });
                    });

                    // Fires right after a remove happens to copy the row to the audit table.
                    netConnectionsEditor.on('remove', function (e, json) {
                        $.ajax({ url: '/ajax/datatables/net-conn-audit/audit',
                            data: removeData,
                            type: 'post'
                        });
                    });

                    netConnectionsTable = $('#networkConnections-table').dataTable({
                        //"sScrollX": "100%",
                        "sScrollY": "100%",
                        "sAjaxSource": "/ajax/datatables/networkconn",
                        "bScrollCollapse": true,
                        "bJQueryUI": true,
                        "bPaginate": false,
                        "sDom": '<"table-header"ClTfiR>t',
                        "aaSorting": [[ 4, "desc" ]],
                        "oTableTools": {
                            "sRowSelect": "single",
                            "aButtons": [
                                {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":netConnectionsEditor},
                                {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":netConnectionsEditor},
                                {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":netConnectionsEditor},
                                {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                    {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                                ]}
                            ],
                            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                        },
                        "aoColumns": [
                            { "mData": "VSN", "sWidth": "80px" },
                            { "mData": "NodeDevice", "sWidth": "8%" },
                            { "mData": "NodeInterface", "sWidth": "10%" },
                            { "mData": "InterfaceTag","sWidth": "25%" },
                            { "mData": "RemoteDeviceName", "sWidth": "10%" },
                            { "mData": "Network","sWidth": "7%" },
                            { "mData": "Speed", "sWidth": "5%" },
                            { "mData": "RemoteDeviceType", "sWidth": "10%" },
                            { "mData": "RemoteDeviceInt", "sWidth": "10%" },
                            { "mData": "RemoteDeviceCircuit", "sWidth": "10%" },
                            { "mData": "Description","sWidth": "15%" }
                        ]
                    });
                });
            }, 10);
        }
        setTimeout(function() { vsnHighlight(netConnectionsTable, 0); }, 200);
    };

// table generation for the Customer Circuits table
    function load_cust_ckts() {
        $("#tab-name").empty().append("Customer Circuits");

        // generate the table
        tableHeight = 470;
        tableName = "custCircuitsTable";
        if (typeof(customerCircuitsTable) == "undefined"){
            setTimeout(function () {
                $(document).ready(function(){
                    customerCircuitsEditor = new $.fn.dataTable.Editor( {
                        "domTable": "#customerCircuits-table",
                        "ajaxUrl": "/ajax/datatables/cust_ckt",
                        "fields" : [
                            {
                                // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                                "label": "Site Name",
                                "name": "SiteName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                            },{
                                "label": "VSN",
                                "name": "VSN",
                                "type": "select",
                                "ipOpts": vsnOpts,
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                            },{
                                "label": "Customer Circuit Type:",
                                "name": "CustomerCktType",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.CustomerCktType ); }
                            },{
                                "label": "Speed:",
                                "name": "Speed",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Speed ); }
                            },{
                                "label": "CDR:",
                                "name": "CDR",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.CDR ); }
                            },{
                                "label": "Circuit ID:",
                                "name": "CircuitID",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.CircuitID ); }
                            },{
                                "label": "MX960 Router:",
                                "name": "MX960Router",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.MX960Router ); }
                            },{
                                "label": "PIP or UUNET Router:",
                                "name": "RemoteRouter",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.RemoteRouter ); }
                            },{
                                "label": "Last Updated",
                                "name": "UpdatedTime",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                            },{
                                "label": "Last Edited By",
                                "name": "EditedBy",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                            },{
                                "label": "Comment",
                                "name": "Comment",
                                "type": "textarea",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                            }
                        ]
                    });

                    // These functions execute on click of edit and create, and can hide these fields from the form
                    customerCircuitsEditor.on('onInitEdit', function () {
                        this.hide('SiteName');
                        this.show('UpdatedTime');
                        this.show('EditedBy');
                        this.show('Comment');
                        this.disable('SiteName');
                        this.disable('VSN');
                        this.disable('MX960Router');
                        this.disable('RemoteRouter');
                        this.disable('UpdatedTime');
                        this.disable('EditedBy');
                    });

                    customerCircuitsEditor.on('onInitCreate', function () {
                        //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                        //var siteDataSplit = document.getElementById("site-header").innerHTML.split("-");
                        //for (var j = 0; j < siteDataSplit.length; j++){
                        //    siteDataSplit[j] = siteDataSplit[j].trim();
                        //}
                        this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                        this.hide('SiteName');
                        this.hide('UpdatedTime');
                        this.hide('EditedBy');
                        this.hide('Comment');
                        this.set('Comment', "Create");
                    });

                    // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                    customerCircuitsEditor.on('initRemove', function (e, node, data) {
                        removeData = data[0];
                        removeData['Created'] = 0;
                        removeData['Removed'] = 1;
                    });

                    // Set the last edited by field to the current username before submitting data
                    customerCircuitsEditor.on('preSubmit', function (e, data, action) {
                        data['data']['EditedBy'] = username;
                    });

                    // Fires when a successful edit/creation has happened to copy the row to the audit table.
                    customerCircuitsEditor.on('submitSuccess', function (e, json, data) {
                        data['Removed'] = 0;
                        data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                        $.ajax({ url: '/ajax/datatables/cust-ckts-audit/audit',
                            data: data,
                            type: 'post'
                        });
                    });

                    // Fires right after a remove happens to copy the row to the audit table.
                    customerCircuitsEditor.on('remove', function (e, json) {
                        $.ajax({ url: '/ajax/datatables/cust-ckts-audit/audit',
                            data: removeData,
                            type: 'post'
                        });
                    });

                    customerCircuitsTable = $('#customerCircuits-table').dataTable({
                        //"sScrollX": "100%",
                        "sScrollY": "100%",
                        "sAjaxSource": "/ajax/datatables/cust_ckt",
                        "bScrollCollapse": true,
                        "bJQueryUI": true,
                        "bPaginate": false,
                        "sDom": '<"table-header"ClTfiR>t',
                        "aaSorting": [[ 4, "asc" ]],
                        "oTableTools": {
                            "sRowSelect": "single",
                            "aButtons": [
                                {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":customerCircuitsEditor},
                                {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":customerCircuitsEditor},
                                {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":customerCircuitsEditor},
                                {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                    {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                                ]}
                            ],
                            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                        },
                        "aoColumns": [
                            { "mData": "VSN", "sWidth": "80px" },
                            { "mData": "CustomerCktType", "sWidth": "15%" },
                            { "mData": "Speed", "sWidth": "15%" },
                            { "mData": "CDR","sWidth": "10%" },
                            { "mData": "CircuitID", "sWidth": "15%" },
                            { "mData": "MX960Router", "sWidth": "20%" },
                            { "mData": "RemoteRouter", "sWidth": "25%" }
                        ]
                    });
                });
            }, 10);
        }
        setTimeout(function() { vsnHighlight(customerCircuitsTable, 0); }, 200);
    };

// Activated when a user clicks on the MX Port Assignments BUTTON
    function load_mx01() {
        $("#tab-name").empty().append("MX960 Port Assignments");

        // Checks which tab is active to set the resizing parameters
        if ($('#pill_mx01').hasClass('active')){
            tableName = "mx01Table";
            tableHeight = 260;
        }
        else if ($('#pill_mx02').hasClass('active')){
            tableName = "mx02Table";
            tableHeight = 260;
        }

        // generate the table
        if (typeof(mx01PortsTable) == "undefined"){
            setTimeout(function () {
                $(document).ready(function(){
                    mx01Editor = new $.fn.dataTable.Editor( {
                        "domTable": "#mx01-table",
                        "ajaxUrl": "/ajax/datatables/mx960/1",
                        "fields" : [
                            {// Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                                "label": "Site Name",
                                "name": "SiteName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                            },{
                                "label": "VSN",
                                "name": "VSN",
                                "type": "select",
                                "ipOpts": vsnOpts,
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                            },{
                                "label": "MX",
                                "name": "MX",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.MX ); }
                            },{
                                "label": "Interface:",
                                "name": "Interface",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Interface ); }
                            },{
                                "label": "Sub Interface:",
                                "name": "SubInterface",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SubInterface ); }
                            },{
                                "label": "Interface Tag:",
                                "name": "InterfaceTag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.InterfaceTag ); }
                            },{
                                "label": "Port Type:",
                                "name": "PortType",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.PortType ); }
                            },{
                                "label": "Layer:",
                                "name": "Layer",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Layer ); }
                            },{
                                "label": "VSN Instance:",
                                "name": "VSNInstance",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSNInstance ); }
                            },{
                                "label": "Connected Device Type:",
                                "name": "ConnectedDeviceType",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.ConnectedDeviceType ); }
                            },{
                                "label": "Connected Device Name:",
                                "name": "ConnectedDeviceName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.ConnectedDeviceName ); }
                            },{
                                "label": "Connected Device Port:",
                                "name": "ConnectedDevicePort",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.ConnectedDevicePort ); }
                            },{
                                "label": "Address Tag:",
                                "name": "AddressTag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.AddressTag ); }
                            },{
                                "label": "IP Address:",
                                "name": "IPAddress",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.IPAddress ); }
                            },{
                                "label": "VLAN ID:",
                                "name": "VLANTag",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VLANTag ); }
                            },{
                                "label": "Speed & Duplex:",
                                "name": "SpeedDuplex",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SpeedDuplex ); }
                            },{
                                "label": "Description Tag:",
                                "name": "DescriptionTag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.DescriptionTag ); }
                            },{
                                "label": "Description:",
                                "name": "Description",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Description ); }
                            },{
                                "label": "Last Updated",
                                "name": "UpdatedTime",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                            },{
                                "label": "Last Edited By",
                                "name": "EditedBy",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                            },{
                                "label": "Comment",
                                "name": "Comment",
                                "type": "textarea",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                            }
                        ]
                    });

                    // These functions execute on click of edit and create, and can hide these fields from the form
                    mx01Editor.on('onInitEdit', function () {
                        this.hide('SiteName');
                        this.hide('MX');
                        this.show('UpdatedTime');
                        this.show('EditedBy');
                        this.show('Comment');
                        this.disable('SiteName');
                        this.disable('VSN');
                        this.disable('ConnectedDeviceName');
                        this.disable('UpdatedTime');
                        this.disable('EditedBy');


                    });

                    mx01Editor.on('onInitCreate', function () {
                        //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                        //var siteDataSplit = document.getElementById("site-header").innerHTML.split("-");
                        //for (var j = 0; j < siteDataSplit.length; j++){
                        //    siteDataSplit[j] = siteDataSplit[j].trim();
                        //}
                        this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                        this.set('MX', '1');
                        this.hide('SiteName');
                        this.hide('MX');
                        this.hide('UpdatedTime');
                        this.hide('EditedBy');
                        this.hide('Comment');
                        this.set('Comment', "Create");
                    });

                    // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                    mx01Editor.on('initRemove', function (e, node, data) {
                        removeData = data[0];
                        removeData['Created'] = 0;
                        removeData['Removed'] = 1;
                    });

                    // Set the last edited by field to the current username before submitting data
                    mx01Editor.on('preSubmit', function (e, data, action) {
                        data['data']['EditedBy'] = username;
                    });

                    // Fires when a successful edit/creation has happened to copy the row to the audit table.
                    mx01Editor.on('submitSuccess', function (e, json, data) {
                        data['Removed'] = 0;
                        data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                        $.ajax({ url: '/ajax/datatables/mx960-audit/1/audit',
                            data: data,
                            type: 'post'
                        });
                    });

                    // Fires right after a remove happens to copy the row to the audit table.
                    mx01Editor.on('remove', function (e, json) {
                        $.ajax({ url: '/ajax/datatables/mx960-audit/1/audit',
                            data: removeData,
                            type: 'post'
                        });
                    });

                    mx01PortsTable = $('#mx01-table').dataTable({
                        "sAjaxSource": "/ajax/datatables/mx960/1",
                        "sScrollX": "100%",
                        "sScrollY": "100%",
                        "bScrollCollapse": true,
                        "bPaginate": false,
                        "sDom": '<"table-header"ClTfiR>t',
                        "oTableTools": {
                            "sRowSelect": "single",
                            "aButtons": [
                                {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":mx01Editor},
                                {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":mx01Editor},
                                {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":mx01Editor},
                                {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                    {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                                ]}
                            ],
                            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                        },
                        "aoColumns": [
                            { "mData": "VSN", "sWidth": "80px" },
                            { "mData": "Interface", "sWidth": "270px" },
                            { "mData": "SubInterface", "sWidth": "90px" },
                            { "mData": "InterfaceTag","sWidth": "350px" },
                            { "mData": "PortType", "sWidth": "100px" },
                            { "mData": "Layer", "sWidth": "30px" },
                            { "mData": "VSNInstance", "sWidth": "90px" },
                            { "mData": "ConnectedDeviceType", "sWidth": "100px" },
                            { "mData": "ConnectedDeviceName", "sWidth": "100px" },
                            { "mData": "ConnectedDevicePort", "sWidth": "100px" },
                            { "mData": "AddressTag","sWidth": "350px" },
                            { "mData": "IPAddress", "sWidth": "120px" },
                            { "mData": "VLANTag", "sWidth": "45px" },
                            { "mData": "SpeedDuplex", "sWidth": "250px" },
                            { "mData": "DescriptionTag","sWidth": "475px" },
                            { "mData": "Description", "sWidth": "1200px" }
                        ]
                    });
                });
            }, 10);
        }
        setTimeout(function() { vsnHighlight(mx01PortsTable, 0); }, 200);
    };

// table generation for MX02 Port Assignments
    function load_mx02() {
        $("#tab-name").empty().append("MX960 Port Assignments");

        // Checks which tab is active to set the resizing parameters
        if ($('#pill_mx01').hasClass('active')){
            tableName = "mx01Table";
            tableHeight = 260;
        }
        else if ($('#pill_mx02').hasClass('active')){
            tableName = "mx02Table";
            tableHeight = 260;
        }

        if (typeof(mx02PortsTable) == "undefined"){
            setTimeout(function () {
                $(document).ready(function(){
                    mx02Editor = new $.fn.dataTable.Editor( {
                        "domTable": "#mx02-table",
                        "ajaxUrl": "/ajax/datatables/mx960/2",
                        "fields" : [
                            {// Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                                "label": "Site Name",
                                "name": "SiteName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                            },{
                                "label": "VSN",
                                "name": "VSN",
                                "type": "select",
                                "ipOpts": vsnOpts,
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                            },{
                                "label": "MX",
                                "name": "MX",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.MX ); }
                            },{
                                "label": "Interface:",
                                "name": "Interface",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Interface ); }
                            },{
                                "label": "Sub Interface:",
                                "name": "SubInterface",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SubInterface ); }
                            },{
                                "label": "Interface Tag:",
                                "name": "InterfaceTag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.InterfaceTag ); }
                            },{
                                "label": "Port Type:",
                                "name": "PortType",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.PortType ); }
                            },{
                                "label": "Layer:",
                                "name": "Layer",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Layer ); }
                            },{
                                "label": "VSN Instance:",
                                "name": "VSNInstance",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSNInstance ); }
                            },{
                                "label": "Connected Device Type:",
                                "name": "ConnectedDeviceType",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.ConnectedDeviceType ); }
                            },{
                                "label": "Connected Device Name:",
                                "name": "ConnectedDeviceName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.ConnectedDeviceName ); }
                            },{
                                "label": "Connected Device Port:",
                                "name": "ConnectedDevicePort",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.ConnectedDevicePort ); }
                            },{
                                "label": "Address Tag:",
                                "name": "AddressTag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.AddressTag ); }
                            },{
                                "label": "IP Address:",
                                "name": "IPAddress",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.IPAddress ); }
                            },{
                                "label": "VLAN ID:",
                                "name": "VLANTag",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VLANTag ); }
                            },{
                                "label": "Speed & Duplex:",
                                "name": "SpeedDuplex",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SpeedDuplex ); }
                            },{
                                "label": "Description Tag:",
                                "name": "DescriptionTag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.DescriptionTag ); }
                            },{
                                "label": "Description:",
                                "name": "Description",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Description ); }
                            },{
                                "label": "Last Updated",
                                "name": "UpdatedTime",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                            },{
                                "label": "Last Edited By",
                                "name": "EditedBy",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                            },{
                                "label": "Comment",
                                "name": "Comment",
                                "type": "textarea",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                            }
                        ]
                    });


                    // These functions execute on click of edit and create, and can hide these fields from the form
                    mx02Editor.on('onInitEdit', function () {
                        this.hide('SiteName');
                        this.hide('MX');
                        this.show('UpdatedTime');
                        this.show('EditedBy');
                        this.show('Comment');
                        this.disable('SiteName');
                        this.disable('VSN');
                        this.disable('ConnectedDeviceName');
                        this.disable('UpdatedTime');
                        this.disable('EditedBy');

                    });

                    mx02Editor.on('onInitCreate', function () {
                        //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                        //var siteDataSplit = document.getElementById("site-header").innerHTML.split("-");
                        //for (var j = 0; j < siteDataSplit.length; j++){
                        //    siteDataSplit[j] = siteDataSplit[j].trim();
                        //}
                        this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                        this.set('MX', '2');
                        this.hide('SiteName');
                        this.hide('MX');
                        this.hide('UpdatedTime');
                        this.hide('EditedBy');
                        this.hide('Comment');
                        this.set('Comment', "Create");
                    });

                    // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                    mx02Editor.on('initRemove', function (e, node, data) {
                        removeData = data[0];
                        removeData['Created'] = 0;
                        removeData['Removed'] = 1;
                    });

                    // Set the last edited by field to the current username before submitting data
                    mx02Editor.on('preSubmit', function (e, data, action) {
                        data['data']['EditedBy'] = username;
                    });

                    // Fires when a successful edit/creation has happened to copy the row to the audit table.
                    mx02Editor.on('submitSuccess', function (e, json, data) {
                        data['Removed'] = 0;
                        data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                        $.ajax({ url: '/ajax/datatables/mx960-audit/2/audit',
                            data: data,
                            type: 'post'
                        });
                    });

                    // Fires right after a remove happens to copy the row to the audit table.
                    mx02Editor.on('remove', function (e, json) {
                        $.ajax({ url: '/ajax/datatables/mx960-audit/2/audit',
                            data: removeData,
                            type: 'post'
                        });
                    });

                    mx02PortsTable = $('#mx02-table').dataTable({
                        "sScrollX": "100%",
                        "sScrollY": "100%",
                        "bScrollCollapse": true,
                        "bPaginate": false,
                        "sAjaxSource": "/ajax/datatables/mx960/2",
                        "sDom": '<"table-header"ClTfiR>t',
                        "oTableTools": {
                            "sRowSelect": "single",
                            "aButtons": [
                                {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":mx02Editor},
                                {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":mx02Editor},
                                {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":mx02Editor},
                                {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                    {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                                ]}
                            ],
                            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                        },
                        "aoColumns": [
                            { "mData": "VSN", "sWidth": "80px" },
                            { "mData": "Interface", "sWidth": "270px" },
                            { "mData": "SubInterface", "sWidth": "90px" },
                            { "mData": "InterfaceTag","sWidth": "350px" },
                            { "mData": "PortType", "sWidth": "100px" },
                            { "mData": "Layer", "sWidth": "30px" },
                            { "mData": "VSNInstance", "sWidth": "90px" },
                            { "mData": "ConnectedDeviceType", "sWidth": "100px" },
                            { "mData": "ConnectedDeviceName", "sWidth": "100px" },
                            { "mData": "ConnectedDevicePort", "sWidth": "100px" },
                            { "mData": "AddressTag","sWidth": "350px" },
                            { "mData": "IPAddress", "sWidth": "120px" },
                            { "mData": "VLANTag", "sWidth": "45px" },
                            { "mData": "SpeedDuplex", "sWidth": "250px" },
                            { "mData": "DescriptionTag","sWidth": "475px" },
                            { "mData": "Description", "sWidth": "1200px" }
                        ]
                    });
                });
            }, 10);
        }
        setTimeout(function() { vsnHighlight(mx02PortsTable, 0); }, 200);
    };

// Activates when the NS Port Assignment menu button is clicked.
    function load_ns01() {
        $("#tab-name").empty().append("NS Port Assignments");

        // Checks which tab is active to set the resizing parameters
        if ($('#5_1').hasClass('active')){
            tableName = "ns01Table";
            tableHeight = 260;
        }
        else if ($('#5_2').hasClass('active')){
            tableName = "ns02Table";
            tableHeight = 260;
        }

        // generate the table
        if (typeof(ns01PortsTable) == "undefined"){
            setTimeout(function () {
                $(document).ready(function(){
                    ns01Editor = new $.fn.dataTable.Editor( {
                        "domTable": "#ns01-table",
                        "ajaxUrl": "/ajax/datatables/ns/1",
                        "fields" : [
                            {// Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                                "label": "Site Name",
                                "name": "SiteName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                            },{
                                "label": "VSN",
                                "name": "VSN",
                                "type": "select",
                                "ipOpts": vsnOpts,
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                            },{
                                "label": "NS",
                                "name": "NS",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.NS ); }
                            },{
                                "label": "Interface:",
                                "name": "Interface",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Interface ); }
                            },{
                                "label": "Sub Interface:",
                                "name": "SubInterface",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SubInterface ); }
                            },{
                                "label": "Port Type:",
                                "name": "PortType",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.PortType ); }
                            },{
                                "label": "Layer:",
                                "name": "Layer",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Layer ); }
                            },{
                                "label": "Slot:",
                                "name": "Slot",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Slot ); }
                            },{
                                "label": "Connected Device Type:",
                                "name": "ConnectedDeviceType",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.ConnectedDeviceType ); }
                            },{
                                "label": "Connected Device Name:",
                                "name": "ConnectedDeviceName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.ConnectedDeviceName ); }
                            },{
                                "label": "Connected Device Port:",
                                "name": "ConnectedDevicePort",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.ConnectedDevicePort ); }
                            },{
                                "label": "Address Tag:",
                                "name": "AddressTag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.AddressTag ); }
                            },{
                                "label": "IP Address:",
                                "name": "IPAddress",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.IPAddress ); }
                            },{
                                "label": "VLAN Tag:",
                                "name": "VLANTag",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VLANTag ); }
                            },{
                                "label": "Speed / Duplex:",
                                "name": "SpeedDuplex",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SpeedDuplex ); }
                            },{
                                "label": "Description:",
                                "name": "Description",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Description ); }
                            },{
                                "label": "Last Updated",
                                "name": "UpdatedTime",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                            },{
                                "label": "Last Edited By",
                                "name": "EditedBy",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                            },{
                                "label": "Comment",
                                "name": "Comment",
                                "type": "textarea",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                            }
                        ]
                    });

                    // These functions execute on click of edit and create, and can hide these fields from the form
                    ns01Editor.on('onInitEdit', function () {
                        this.hide('SiteName');
                        this.hide('NS');
                        this.show('UpdatedTime');
                        this.show('EditedBy');
                        this.show('Comment');
                        this.disable('SiteName');
                        this.disable('VSN');
                        this.disable('ConnectedDeviceName');
                        this.disable('UpdatedTime');
                        this.disable('EditedBy');
                    });

                    ns01Editor.on('onInitCreate', function () {
                        //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                        //var siteDataSplit = document.getElementById("site-header").innerHTML.split("-");
                        //for (var j = 0; j < siteDataSplit.length; j++){
                        //    siteDataSplit[j] = siteDataSplit[j].trim();
                        //}
                        this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                        this.set('NS', '1');
                        this.hide('SiteName');
                        this.hide('NS');
                        this.hide('UpdatedTime');
                        this.hide('EditedBy');
                        this.hide('Comment');
                        this.set('Comment', "Create");
                    });

                    // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                    ns01Editor.on('initRemove', function (e, node, data) {
                        removeData = data[0];
                        removeData['Created'] = 0;
                        removeData['Removed'] = 1;
                    });

                    // Set the last edited by field to the current username before submitting data
                    ns01Editor.on('preSubmit', function (e, data, action) {
                        data['data']['EditedBy'] = username;
                    });

                    // Fires when a successful edit/creation has happened to copy the row to the audit table.
                    ns01Editor.on('submitSuccess', function (e, json, data) {
                        data['Removed'] = 0;
                        data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                        $.ajax({ url: '/ajax/datatables/ns-audit/1/audit',
                            data: data,
                            type: 'post'
                        });
                    });

                    // Fires right after a remove happens to copy the row to the audit table.
                    ns01Editor.on('remove', function (e, json) {
                        $.ajax({ url: '/ajax/datatables/ns-audit/1/audit',
                            data: removeData,
                            type: 'post'
                        });
                    });

                    ns01PortsTable = $('#ns01-table').dataTable({
                        "sScrollX": "100%",
                        "sScrollY": "100%",
                        "sAjaxSource": "/ajax/datatables/ns/1",
                        "bScrollCollapse": true,
                        //"bJQueryUI": true,
                        "bPaginate": false,
                        "sDom": '<"table-header"ClTFfiR>t',
                        "oTableTools": {
                            "sRowSelect": "single",
                            "aButtons": [
                                {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":ns01Editor},
                                {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":ns01Editor},
                                {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":ns01Editor},
                                {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                    {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                                ]}
                            ],
                            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                        },
                        "aoColumns": [
                            { "mData": "VSN", "sWidth": "80px" },
                            { "mData": "Interface", "sWidth": "75px" },
                            { "mData": "SubInterface", "sWidth": "90px" },
                            { "mData": "PortType", "sWidth": "70px" },
                            { "mData": "Layer", "sWidth": "50px" },
                            { "mData": "Slot", "sWidth": "50px" },
                            { "mData": "ConnectedDeviceType","sWidth": "100px" },
                            { "mData": "ConnectedDeviceName", "sWidth": "105px" },
                            { "mData": "ConnectedDevicePort", "sWidth": "100px" },
                            { "mData": "AddressTag","sWidth": "250px" },
                            { "mData": "IPAddress", "sWidth": "150px" },
                            { "mData": "VLANTag","sWidth": "75px" },
                            { "mData": "SpeedDuplex", "sWidth": "85px" },
                            { "mData": "Description", "sWidth": "300px" }
                        ]
                    });
                });
            }, 10);
        }
        setTimeout(function() { vsnHighlight(ns01PortsTable, 0); }, 200);
    };

// table generation for NS5400 02 Port Assignments
    function load_ns02() {
        $("#tab-name").empty().append("NS Port Assignments");

        tableHeight = 320;
        tableName = "ns02Table";
        if (typeof(ns02PortsTable) == "undefined"){
            setTimeout(function () {
                $(document).ready(function(){
                    ns02Editor = new $.fn.dataTable.Editor( {
                        "domTable": "#ns02-table",
                        "ajaxUrl": "/ajax/datatables/ns/2",
                        "fields" : [
                            {// Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                                "label": "Site Name",
                                "name": "SiteName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                            },{
                                "label": "VSN",
                                "name": "VSN",
                                "type": "select",
                                "ipOpts": vsnOpts,
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                            },{
                                "label": "NS",
                                "name": "NS",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.NS ); }
                            },{
                                "label": "Interface:",
                                "name": "Interface",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Interface ); }
                            },{
                                "label": "Sub Interface:",
                                "name": "SubInterface",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SubInterface ); }
                            },{
                                "label": "Port Type:",
                                "name": "PortType",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.PortType ); }
                            },{
                                "label": "Layer:",
                                "name": "Layer",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Layer ); }
                            },{
                                "label": "Slot:",
                                "name": "Slot",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Slot ); }
                            },{
                                "label": "Connected Device Type:",
                                "name": "ConnectedDeviceType",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.ConnectedDeviceType ); }
                            },{
                                "label": "Connected Device Name:",
                                "name": "ConnectedDeviceName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.ConnectedDeviceName ); }
                            },{
                                "label": "Connected Device Port:",
                                "name": "ConnectedDevicePort",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.ConnectedDevicePort ); }
                            },{
                                "label": "Address Tag:",
                                "name": "AddressTag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.AddressTag ); }
                            },{
                                "label": "IP Address:",
                                "name": "IPAddress",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.IPAddress ); }
                            },{
                                "label": "VLAN Tag:",
                                "name": "VLANTag",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VLANTag ); }
                            },{
                                "label": "Speed / Duplex:",
                                "name": "SpeedDuplex",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SpeedDuplex ); }
                            },{
                                "label": "Description:",
                                "name": "Description",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Description ); }
                            },{
                                "label": "Last Updated",
                                "name": "UpdatedTime",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                            },{
                                "label": "Last Edited By",
                                "name": "EditedBy",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                            },{
                                "label": "Comment",
                                "name": "Comment",
                                "type": "textarea",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                            }
                        ]
                    });
                    // These functions execute on click of edit and create, and can hide these fields from the form
                    ns02Editor.on('onInitEdit', function () {
                        this.hide('SiteName');
                        this.hide('NS');
                        this.show('UpdatedTime');
                        this.show('EditedBy');
                        this.show('Comment');
                        this.disable('SiteName');
                        this.disable('VSN');
                        this.disable('ConnectedDeviceName');
                        this.disable('UpdatedTime');
                        this.disable('EditedBy');
                    });

                    ns02Editor.on('onInitCreate', function () {
                        //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                        //var siteDataSplit = document.getElementById("site-header").innerHTML.split("-");
                        //for (var j = 0; j < siteDataSplit.length; j++){
                        //    siteDataSplit[j] = siteDataSplit[j].trim();
                        //}
                        this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                        this.set('NS', '2');
                        this.hide('SiteName');
                        this.hide('NS');
                        this.hide('UpdatedTime');
                        this.hide('EditedBy');
                        this.hide('Comment');
                        this.set('Comment', "Create");
                    });

                    // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                    ns02Editor.on('initRemove', function (e, node, data) {
                        removeData = data[0];
                        removeData['Created'] = 0;
                        removeData['Removed'] = 1;
                    });

                    // Set the last edited by field to the current username before submitting data
                    ns02Editor.on('preSubmit', function (e, data, action) {
                        data['data']['EditedBy'] = username;
                    });

                    // Fires when a successful edit/creation has happened to copy the row to the audit table.
                    ns02Editor.on('submitSuccess', function (e, json, data) {
                        data['Removed'] = 0;
                        data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                        $.ajax({ url: '/ajax/datatables/ns-audit/2/audit',
                            data: data,
                            type: 'post'
                        });
                    });

                    // Fires right after a remove happens to copy the row to the audit table.
                    ns02Editor.on('remove', function (e, json) {
                        $.ajax({ url: '/ajax/datatables/ns-audit/2/audit',
                            data: removeData,
                            type: 'post'
                        });
                    });

                    ns02PortsTable = $('#ns02-table').dataTable({
                        "sScrollX": "100%",
                        "sScrollY": "100%",
                        "sAjaxSource": "/ajax/datatables/ns/2",
                        "bScrollCollapse": true,
                        "bJQueryUI": true,
                        "bPaginate": false,
                        "sDom": '<"table-header"ClTfiR>t',
                        "oTableTools": {
                            "sRowSelect": "single",
                            "aButtons": [
                                {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":ns02Editor},
                                {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":ns02Editor},
                                {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":ns02Editor},
                                {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                    {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                                ]}
                            ],
                            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                        },
                        "aoColumns": [
                            { "mData": "VSN", "sWidth": "80px" },
                            { "mData": "Interface", "sWidth": "75px" },
                            { "mData": "SubInterface", "sWidth": "90px" },
                            { "mData": "PortType", "sWidth": "70px" },
                            { "mData": "Layer", "sWidth": "50px" },
                            { "mData": "Slot", "sWidth": "50px" },
                            { "mData": "ConnectedDeviceType","sWidth": "100px" },
                            { "mData": "ConnectedDeviceName", "sWidth": "105px" },
                            { "mData": "ConnectedDevicePort", "sWidth": "100px" },
                            { "mData": "AddressTag","sWidth": "250px" },
                            { "mData": "IPAddress", "sWidth": "150px" },
                            { "mData": "VLANTag","sWidth": "75px" },
                            { "mData": "SpeedDuplex", "sWidth": "85px" },
                            { "mData": "Description", "sWidth": "300px" }
                        ]
                    });
                });
            }, 10);
        }
        setTimeout(function() { vsnHighlight(ns02PortsTable, 0); }, 200);
    };

// table generation for the IPSchema table
    function load_ipschema() {

        $("#tab-name").empty().append("IP Schema");

        if (typeof(ipSchemaTable) == "undefined"){
            tableHeight = 320;
            tableName = "ipSchema";
            setTimeout(function () {
                $(document).ready(function(){
                    ipSchemaEditor = new $.fn.dataTable.Editor( {
                        "domTable": "#ipSchema-table",
                        "ajaxUrl": "/ajax/datatables/ipschema",
                        "fields" : [
                            {
                                // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                                "label": "Site Name",
                                "name": "SiteName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                            },{
                                "label": "VSN",
                                "name": "VSN",
                                "type": "select",
                                "ipOpts": vsnOpts,
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                            },{
                                "label": "Network Type:",
                                "name": "NetworkType",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.NetworkType ); }
                            },{
                                "label": "Name:",
                                "name": "Name",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Name ); }
                            },{
                                "label": "Subnet:",
                                "name": "Subnet",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Subnet ); }
                            },{
                                "label": "Netmask:",
                                "name": "Netmask",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Netmask ); }
                            },{
                                "label": "Purpose",
                                "name": "Purpose",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Purpose ); }
                            },{
                                "label": "VLAN:",
                                "name": "VLAN",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VLAN ); }
                            },{
                                "label": "Notes:",
                                "name": "Notes",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Notes ); }
                            },{
                                "label": "Last Updated",
                                "name": "UpdatedTime",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                            },{
                                "label": "Last Edited By",
                                "name": "EditedBy",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                            },{
                                "label": "Comment",
                                "name": "Comment",
                                "type": "textarea",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                            }
                        ]
                    });

                    // These functions execute on click of edit and create, and can hide these fields from the form
                    ipSchemaEditor.on('onInitEdit', function () {
                        this.hide('SiteName');
                        this.show('UpdatedTime');
                        this.show('EditedBy');
                        this.show('Comment');
                        this.disable('SiteName');
                        this.disable('VSN');
                        this.disable('UpdatedTime');
                        this.disable('EditedBy');
                    });

                    ipSchemaEditor.on('onInitCreate', function () {
                        //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                        //var siteDataSplit = document.getElementById("site-header").innerHTML.split("-");
                        //for (var j = 0; j < siteDataSplit.length; j++){
                        //    siteDataSplit[j] = siteDataSplit[j].trim();
                        //}
                        this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                        this.hide('SiteName');
                        this.hide('UpdatedTime');
                        this.hide('EditedBy');
                        this.hide('Comment');
                        this.set('Comment', "Create");
                    });

                    // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                    ipSchemaEditor.on('initRemove', function (e, node, data) {
                        removeData = data[0];
                        removeData['Created'] = 0;
                        removeData['Removed'] = 1;
                    });

                    // Set the last edited by field to the current username before submitting data
                    ipSchemaEditor.on('preSubmit', function (e, data, action) {
                        data['data']['EditedBy'] = username;
                    });

                    // Fires when a successful edit/creation has happened to copy the row to the audit table.
                    ipSchemaEditor.on('submitSuccess', function (e, json, data) {
                        data['Removed'] = 0;
                        data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                        $.ajax({ url: '/ajax/datatables/ipschema-audit/audit',
                            data: data,
                            type: 'post'
                        });
                    });

                    // Fires right after a remove happens to copy the row to the audit table.
                    ipSchemaEditor.on('remove', function (e, json) {
                        $.ajax({ url: '/ajax/datatables/ipschema-audit/audit',
                            data: removeData,
                            type: 'post'
                        });
                    });

                    ipSchemaTable = $('#ipSchema-table').dataTable({
                        "sScrollX": "100%",
                        "sScrollY": "100%",
                        "sAjaxSource": "/ajax/datatables/ipschema",
                        "fnDrawCallback": function() {
                            $("#ipSchema-table tbody tr").dblclick(function () {
                                var nTds = this.getElementsByTagName('td');
                                if (nTds[0].innerHTML == 'Public'){ // value of the first column in this row
                                    $('.publicAddress').trigger('click');
                                    $('.PublicAddress-tab').trigger('click');
                                    setTimeout(function () {
                                        $('#public-table tr').each(function() {
                                            if ($(this).find("td").eq(0).html() !== null){
                                                var match = $(this).find("td").eq(0).html().match(nTds[1].innerHTML);
                                                if (match){
                                                    $(this).trigger('click');
                                                    var scroller = publicIPTable.fnSettings().nTable.parentNode;
                                                    $(scroller).scrollTo( this, 300, {offset: {top:-250}} );
                                                }
                                            }
                                        });
                                    },1000);
                                }
                                else if (nTds[0].innerHTML == 'IDN'){ // value of the first column in this row
                                    $('.publicAddress').trigger('click');
                                    setTimeout(function () {
                                        $('.idnIP').trigger('click');
                                    },10);
                                    setTimeout(function () {
                                        $('#idn-table tr').each(function() {
                                            if ($(this).find("td").eq(0).html() !== null){
                                                var match = $(this).find("td").eq(0).html().match(nTds[1].innerHTML, 'g');
                                                if (match){
                                                    var scroller = idnIPTable.fnSettings().nTable.parentNode;
                                                    $(scroller).scrollTo( this, 300, {offset: {top:-250}} );
                                                }
                                            }
                                        });
                                    },1000);
                                }
                                else if (nTds[0].innerHTML == 'Private'){ // value of the first column in this row
                                    $('.publicAddress').trigger('click');
                                    setTimeout(function () {
                                        $('.privateIP').trigger('click');
                                    },10);
                                    setTimeout(function () {
                                        $('#private-table tr').each(function() {
                                            if ($(this).find("td").eq(0).html() !== null){
                                                var match = $(this).find("td").eq(0).html().match(nTds[1].innerHTML, 'g');
                                                if (match){
                                                    $(this).trigger('click');
                                                    var scroller = privateIPTable.fnSettings().nTable.parentNode;
                                                    $(scroller).scrollTo( this, 300, {offset: {top:-250}} );
                                                }
                                            }
                                        });
                                    },1000);
                                }
                            });
                        },
                        "bScrollCollapse": true,
                        "bJQueryUI": true,
                        "bPaginate": false,
                        "sDom": '<"table-header"ClTfiR>t',
                        "oTableTools": {
                            "sRowSelect": "single",
                            "aButtons": [
                                {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":ipSchemaEditor},
                                {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":ipSchemaEditor},
                                {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":ipSchemaEditor},
                                {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                    {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                                ]}
                            ],
                            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                        },
                        "aoColumns": [
                            { "mData": "NetworkType", "sWidth": "65px" },
                            { "mData": "VSN", "sWidth": "80px" },
                            { "mData": "Name", "sWidth": "300px" },
                            { "mData": "Subnet","sWidth": "125px" },
                            { "mData": "Netmask", "sWidth": "65px" },
                            { "mData": "Purpose","sWidth": "400px" },
                            { "mData": "VLAN", "sWidth": "65px" },
                            { "mData": "Notes","sWidth": "500px"}
                        ]
                    }).rowGrouping({"bExpandableGrouping": true, "sGroupingClass": 'group'});
                });
            }, 10);
        }
        setTimeout(function() { vsnHighlight(ipSchemaTable, 0); }, 200);
    };

// table generation for the Private Address Subnet table
    function load_privaddr() {
        // Checks which tab is active to set the resizing parameters
        if ($('#7_1').hasClass('active')){
            tableName = "publicAddress";
        }
        else if ($('#7_2').hasClass('active')){
            tableName = "privateAddress";
        }
        else if ($('#7_3').hasClass('active')){
            tableName = "idnAddress";
        }
        tableHeight = 260;

        if (typeof(privateIPTable) == "undefined"){
            setTimeout(function () {
                tableName = "privateAddress";
                $(document).ready(function(){
                    privateIPEditor = new $.fn.dataTable.Editor( {
                        "domTable": "#private-table",
                        "ajaxUrl": "/ajax/datatables/privateip",
                        "fields" : [
                            {
                                // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                                "label": "Site Name",
                                "name": "SiteName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                            },{
                                "label": "VSN",
                                "name": "VSN",
                                "type": "select",
                                "ipOpts": vsnOpts,
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                            },{
                                "label": "VLAN Name:",
                                "name": "VLANName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VLANName ); }
                            },{
                                "label": "VLAN ID:",
                                "name": "VLANID",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VLANID ); }
                            },{
                                "label": "Subnet:",
                                "name": "Subnet",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Subnet ); }
                            },{
                                "label": "Address Tag:",
                                "name": "AddressTag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.AddressTag ); }
                            },{
                                "label": "IP Address:",
                                "name": "IPAddress",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.IPAddress ); }
                            },{
                                "label": "Netmask Tag:",
                                "name": "NetmaskTag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.NetmaskTag ); }
                            },{
                                "label": "Netmask:",
                                "name": "Netmask",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Netmask ); }
                            },{
                                "label": "Device:",
                                "name": "Device",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Device ); }
                            },{
                                "label": "DNS Name:",
                                "name": "DNSName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.DNSName ); }
                            },{
                                "label": "Purpose:",
                                "name": "Purpose",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Purpose ); }
                            },{
                                "label": "Last Updated",
                                "name": "UpdatedTime",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                            },{
                                "label": "Last Edited By",
                                "name": "EditedBy",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                            },{
                                "label": "Comment",
                                "name": "Comment",
                                "type": "textarea",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                            }
                        ]
                    });

                    // These functions execute on click of edit and create, and can hide these fields from the form
                    privateIPEditor.on('onInitEdit', function () {
                        this.hide('SiteName');
                        this.show('UpdatedTime');
                        this.show('EditedBy');
                        this.show('Comment');
                        this.disable('SiteName');
                        this.disable('VSN');
                        this.disable('UpdatedTime');
                        this.disable('EditedBy');
                    });

                    privateIPEditor.on('onInitCreate', function () {
                        //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                        //var siteDataSplit = document.getElementById("site-header").innerHTML.split("-");
                        //for (var j = 0; j < siteDataSplit.length; j++){
                        //    siteDataSplit[j] = siteDataSplit[j].trim();
                        //}
                        this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                        this.hide('SiteName');
                        this.hide('UpdatedTime');
                        this.hide('EditedBy');
                        this.hide('Comment');
                        this.set('Comment', "Create");
                    });

                    // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                    privateIPEditor.on('initRemove', function (e, node, data) {
                        removeData = data[0];
                        removeData['Created'] = 0;
                        removeData['Removed'] = 1;
                    });

                    // Set the last edited by field to the current username before submitting data
                    privateIPEditor.on('preSubmit', function (e, data, action) {
                        data['data']['EditedBy'] = username;
                    });

                    // Fires when a successful edit/creation has happened to copy the row to the audit table.
                    privateIPEditor.on('submitSuccess', function (e, json, data) {
                        data['Removed'] = 0;
                        data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                        $.ajax({ url: '/ajax/datatables/privateip-audit/audit',
                            data: data,
                            type: 'post'
                        });
                    });

                    // Fires right after a remove happens to copy the row to the audit table.
                    privateIPEditor.on('remove', function (e, json) {
                        $.ajax({ url: '/ajax/datatables/privateip-audit/audit',
                            data: removeData,
                            type: 'post'
                        });
                    });

                    privateIPTable = $('#private-table').dataTable({
                        "sScrollX": "100%",
                        "sScrollY": "100%",
                        "sAjaxSource": "/ajax/datatables/privateip",
                        "bScrollCollapse": true,
                        "bJQueryUI": true,
                        "bPaginate": false,
                        "sDom": '<"table-header"ClTFfiR>t',
                        "oTableTools": {
                            "sRowSelect": "single",
                            "aButtons": [
                                {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":privateIPEditor},
                                {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":privateIPEditor},
                                {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":privateIPEditor},
                                {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                    {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                                ]}
                            ],
                            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                        },
                        "aoColumns": [
                            { "mData": "VLANName" },
                            { "mData": "VSN", "sWidth": "80px" },
                            { "mData": "VLANID", "sWidth": "50px" },
                            { "mData": "AddressTag","sWidth": "350px" },
                            { "mData": "IPAddress", "sWidth": "250px" },
                            { "mData": "Device", "sWidth": "125px" },
                            { "mData": "DNSName", "sWidth": "125px" },
                            { "mData": "Purpose", "sWidth": "265px" }
                        ]
                    }).rowGrouping({"bExpandableGrouping": true, "sGroupingClass": 'group', fnGroupLabelFormat: function(label) { return ""+ label + " " + RowGroupSubnet(label, "private");}});
                });
            }, 10);
        }
        setTimeout(function() { vsnHighlight(privateIPTable, 0); }, 200);
    };

// table generation for the IDN Address Subnet table
    function load_idnip() {
        // Checks which tab is active to set the resizing parameters
        if ($('#7_1').hasClass('active')){
            tableName = "publicAddress";
        }
        else if ($('#7_2').hasClass('active')){
            tableName = "privateAddress";
        }
        else if ($('#7_3').hasClass('active')){
            tableName = "idnAddress";
        }
        tableHeight = 260;

        if (typeof(idnIPTable) == "undefined"){
            setTimeout(function () {
                tableName = "idnAddress";
                $(document).ready(function(){
                    idnIPEditor = new $.fn.dataTable.Editor( {
                        "domTable": "#idn-table",
                        "ajaxUrl": "/ajax/datatables/idnip",
                        "fields" : [
                            {
                                // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                                "label": "Site Name",
                                "name": "SiteName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                            },{
                                "label": "VSN",
                                "name": "VSN",
                                "type": "select",
                                "ipOpts": vsnOpts,
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                            },{
                                "label": "VLAN Name:",
                                "name": "VLANName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VLANName ); }
                            },{
                                "label": "VLAN ID:",
                                "name": "VLANID",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VLANID ); }
                            },{
                                "label": "Subnet:",
                                "name": "Subnet",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Subnet ); }
                            },{
                                "label": "Address Tag:",
                                "name": "AddressTag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.AddressTag ); }
                            },{
                                "label": "IP Address:",
                                "name": "IPAddress",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.IPAddress ); }
                            },{
                                "label": "Netmask Tag:",
                                "name": "NetmaskTag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.NetmaskTag ); }
                            },{
                                "label": "Netmask:",
                                "name": "Netmask",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Netmask ); }
                            },{
                                "label": "Device:",
                                "name": "Device",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Device ); }
                            },{
                                "label": "DNS Name:",
                                "name": "DNSName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.DNSName ); }
                            },{
                                "label": "Purpose:",
                                "name": "Purpose",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Purpose ); }
                            },{
                                "label": "Last Updated",
                                "name": "UpdatedTime",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                            },{
                                "label": "Last Edited By",
                                "name": "EditedBy",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                            },{
                                "label": "Comment",
                                "name": "Comment",
                                "type": "textarea",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                            }
                        ]
                    });

                    // These functions execute on click of edit and create, and can hide these fields from the form
                    idnIPEditor.on('onInitEdit', function () {
                        this.hide('SiteName');
                        this.show('UpdatedTime');
                        this.show('EditedBy');
                        this.show('Comment');
                        this.disable('SiteName');
                        this.disable('VSN');
                        this.disable('UpdatedTime');
                        this.disable('EditedBy');
                    });

                    idnIPEditor.on('onInitCreate', function () {
                        //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                        //var siteDataSplit = document.getElementById("site-header").innerHTML.split("-");
                        //for (var j = 0; j < siteDataSplit.length; j++){
                        //    siteDataSplit[j] = siteDataSplit[j].trim();
                        //}
                        this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                        this.hide('SiteName');
                        this.hide('UpdatedTime');
                        this.hide('EditedBy');
                        this.hide('Comment');
                        this.set('Comment', "Create");
                    });

                    // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                    idnIPEditor.on('initRemove', function (e, node, data) {
                        removeData = data[0];
                        removeData['Created'] = 0;
                        removeData['Removed'] = 1;
                    });

                    // Set the last edited by field to the current username before submitting data
                    idnIPEditor.on('preSubmit', function (e, data, action) {
                        data['data']['EditedBy'] = username;
                    });

                    // Fires when a successful edit/creation has happened to copy the row to the audit table.
                    idnIPEditor.on('submitSuccess', function (e, json, data) {
                        data['Removed'] = 0;
                        data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                        $.ajax({ url: '/ajax/datatables/idnip-audit/audit',
                            data: data,
                            type: 'post'
                        });
                    });

                    // Fires right after a remove happens to copy the row to the audit table.
                    idnIPEditor.on('remove', function (e, json) {
                        $.ajax({ url: '/ajax/datatables/idnip-audit/audit',
                            data: removeData,
                            type: 'post'
                        });
                    });

                    idnIPTable = $('#idn-table').dataTable({
                        "sScrollX": "100%",
                        "sScrollY": "100%",
                        "sAjaxSource": "/ajax/datatables/idnip",
                        "bScrollCollapse": true,
                        "bJQueryUI": true,
                        "bPaginate": false,
                        "sDom": '<"table-header"ClTFfiR>t',
                        "oTableTools": {
                            "sRowSelect": "single",
                            "aButtons": [
                                {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":idnIPEditor},
                                {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":idnIPEditor},
                                {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":idnIPEditor},
                                {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                    {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                                ]}
                            ],
                            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                        },
                        "aoColumns": [
                            { "mData": "VLANName" },
                            { "mData": "VSN", "sWidth": "80px" },
                            { "mData": "VLANID", "sWidth": "50px" },
                            { "mData": "AddressTag","sWidth": "350px" },
                            { "mData": "IPAddress", "sWidth": "150px" },
                            { "mData": "Device", "sWidth": "175px" },
                            { "mData": "DNSName", "sWidth": "125px" },
                            { "mData": "Purpose", "sWidth": "265px" }
                        ]
                    }).rowGrouping({"bExpandableGrouping": true, "sGroupingClass": 'group', fnGroupLabelFormat: function(label) { return ""+ label + " " + RowGroupSubnet(label, "idn");}});
                });
            }, 10);
        }
        setTimeout(function() { vsnHighlight(idnIPTable, 0); }, 200);
    };

// table generation for the Public Address Subnet table
    function load_subnetinfo() {

        $("#tab-name").empty().append("Address Subnets");

        // Checks which tab is active to set the resizing parameters
        if ($('#7_1').hasClass('active')){
            tableName = "publicAddress";
        }
        else if ($('#7_2').hasClass('active')){
            tableName = "privateAddress";
        }
        else if ($('#7_3').hasClass('active')){
            tableName = "idnAddress";
        }
        tableHeight = 260;

        if (typeof(publicIPTable) == "undefined"){
            setTimeout(function () {
                tableName = "publicAddress";
                $(document).ready(function(){
                    publicIPEditor = new $.fn.dataTable.Editor( {
                        "domTable": "#public-table",
                        "ajaxUrl": "/ajax/datatables/pubip",
                        "fields" : [
                            {
                                // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                                "label": "Site Name",
                                "name": "SiteName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                            },{
                                "label": "VSN",
                                "name": "VSN",
                                "type": "select",
                                "ipOpts": vsnOpts,
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                            },{
                                "label": "VLAN Name:",
                                "name": "VLANName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VLANName ); }
                            },{
                                "label": "VLAN ID:",
                                "name": "VLANID",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VLANID ); }
                            },{
                                "label": "Subnet:",
                                "name": "Subnet",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Subnet ); }
                            },{
                                "label": "Address Tag:",
                                "name": "AddressTag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.AddressTag ); }
                            },{
                                "label": "IP Address:",
                                "name": "IPAddress",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.IPAddress ); }
                            },{
                                "label": "Netmask Tag:",
                                "name": "NetmaskTag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.NetmaskTag ); }
                            },{
                                "label": "Netmask:",
                                "name": "Netmask",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Netmask ); }
                            },{
                                "label": "Device:",
                                "name": "Device",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Device ); }
                            },{
                                "label": "DNS Name:",
                                "name": "DNSName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.DNSName ); }
                            },{
                                "label": "Purpose:",
                                "name": "Purpose",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Purpose ); }
                            },{
                                "label": "Last Updated",
                                "name": "UpdatedTime",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                            },{
                                "label": "Last Edited By",
                                "name": "EditedBy",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                            },{
                                "label": "Comment",
                                "name": "Comment",
                                "type": "textarea",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                            }
                        ]
                    });

                    // These functions execute on click of edit and create, and can hide these fields from the form
                    publicIPEditor.on('onInitEdit', function () {
                        this.hide('SiteName');
                        this.show('UpdatedTime');
                        this.show('EditedBy');
                        this.show('Comment');
                        this.disable('SiteName');
                        this.disable('VSN');
                        this.disable('UpdatedTime');
                        this.disable('EditedBy');
                    });

                    publicIPEditor.on('onInitCreate', function () {
                        //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                        //var siteDataSplit = document.getElementById("site-header").innerHTML.split("-");
                        //for (var j = 0; j < siteDataSplit.length; j++){
                        //    siteDataSplit[j] = siteDataSplit[j].trim();
                        //}
                        this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                        this.hide('SiteName');
                        this.hide('UpdatedTime');
                        this.hide('EditedBy');
                        this.hide('Comment');
                        this.set('Comment', "Create");
                    });

                    // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                    publicIPEditor.on('initRemove', function (e, node, data) {
                        removeData = data[0];
                        removeData['Created'] = 0;
                        removeData['Removed'] = 1;
                    });

                    // Set the last edited by field to the current username before submitting data
                    publicIPEditor.on('preSubmit', function (e, data, action) {
                        data['data']['EditedBy'] = username;
                    });

                    // Fires when a successful edit/creation has happened to copy the row to the audit table.
                    publicIPEditor.on('submitSuccess', function (e, json, data) {
                        data['Removed'] = 0;
                        data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                        $.ajax({ url: '/ajax/datatables/pubip-audit/audit',
                            data: data,
                            type: 'post'
                        });
                    });

                    // Fires right after a remove happens to copy the row to the audit table.
                    publicIPEditor.on('remove', function (e, json) {
                        $.ajax({ url: '/ajax/datatables/pubip-audit/audit',
                            data: removeData,
                            type: 'post'
                        });
                    });

                    publicIPTable = $('#public-table').dataTable({
                        "sScrollX": "100%",
                        "sScrollY": "100%",
                        "sAjaxSource": "/ajax/datatables/pubip",
                        "bScrollCollapse": true,
                        "bJQueryUI": true,
                        "bPaginate": false,
                        "sDom": '<"table-header"ClTFfiR>t',
                        "oTableTools": {
                            "sRowSelect": "single",
                            "aButtons": [
                                {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":publicIPEditor},
                                {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":publicIPEditor},
                                {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":publicIPEditor},
                                {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                    {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                                ]}
                            ],
                            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                        },
                        "aoColumns": [
                            { "mData": "VLANName" },
                            { "mData": "VSN", "sWidth": "80px" },
                            { "mData": "VLANID", "sWidth": "50px" },
                            { "mData": "AddressTag","sWidth": "350px" },
                            { "mData": "IPAddress", "sWidth": "150px" },
                            { "mData": "Device", "sWidth": "175px" },
                            { "mData": "DNSName", "sWidth": "125px" },
                            { "mData": "Purpose", "sWidth": "265px" }
                        ]
                    }).rowGrouping({"bExpandableGrouping": true, "sGroupingClass": 'group', fnGroupLabelFormat: function(label) { return ""+ label + " " + RowGroupSubnet(label, "public");}});
                });
            }, 10);
        }
        setTimeout(function() { vsnHighlight(publicIPTable, 0); }, 200);
    };

// table generation for the DNS Trap Log table
    function load_dnstrap() {
        $("#tab-name").empty().append("DNS / NTP / Logging");

        if (typeof(dnsTrapTable) == "undefined"){
            tableHeight = 320;
            tableName = "dnsTrap";
            setTimeout(function () {
                $(document).ready(function(){
                    dnsTrapEditor = new $.fn.dataTable.Editor( {
                        "domTable": "#dnsTrap-table",
                        "ajaxUrl": "/ajax/datatables/dns_trap",
                        "fields" : [
                            {
                                // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                                "label": "Site Name",
                                "name": "SiteName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                            },{
                                "label": "VSN",
                                "name": "VSN",
                                "type": "select",
                                "ipOpts": vsnOpts,
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                            },{
                                "label": "Function:",
                                "name": "Function",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Function ); }
                            },{
                                "label": "Tag:",
                                "name": "Tag",
                                "type": "tagField",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Tag ); }
                            },{
                                "label": "Host IP:",
                                "name": "HostIP",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.HostIP ); }
                            },{
                                "label": "Reference:",
                                "name": "Reference",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Reference ); }
                            },{
                                "label": "Name:",
                                "name": "Name",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Name ); }
                            },{
                                "label": "Last Updated",
                                "name": "UpdatedTime",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                            },{
                                "label": "Last Edited By",
                                "name": "EditedBy",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                            },{
                                "label": "Comment",
                                "name": "Comment",
                                "type": "textarea",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                            }
                        ]
                    });

                    // These functions execute on click of edit and create, and can hide these fields from the form
                    dnsTrapEditor.on('onInitEdit', function () {
                        this.hide('SiteName');
                        this.show('UpdatedTime');
                        this.show('EditedBy');
                        this.show('Comment');
                        this.disable('SiteName');
                        this.disable('VSN');
                        this.disable('Name');
                        this.disable('UpdatedTime');
                        this.disable('EditedBy');
                    });

                    dnsTrapEditor.on('onInitCreate', function () {
                        //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                        //var siteDataSplit = document.getElementById("site-header").innerHTML.split("-");
                        //for (var j = 0; j < siteDataSplit.length; j++){
                        //    siteDataSplit[j] = siteDataSplit[j].trim();
                        //}
                        this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                        this.hide('SiteName');
                        this.hide('UpdatedTime');
                        this.hide('EditedBy');
                        this.hide('Comment');
                        this.set('Comment', "Create");
                    });

                    // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                    dnsTrapEditor.on('initRemove', function (e, node, data) {
                        removeData = data[0];
                        removeData['Created'] = 0;
                        removeData['Removed'] = 1;
                    });

                    // Set the last edited by field to the current username before submitting data
                    dnsTrapEditor.on('preSubmit', function (e, data, action) {
                        data['data']['EditedBy'] = username;
                    });

                    // Fires when a successful edit/creation has happened to copy the row to the audit table.
                    dnsTrapEditor.on('submitSuccess', function (e, json, data) {
                        data['Removed'] = 0;
                        data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                        $.ajax({ url: '/ajax/datatables/dns-trap-audit/audit',
                            data: data,
                            type: 'post'
                        });
                    });

                    // Fires right after a remove happens to copy the row to the ƒknj table.
                    dnsTrapEditor.on('remove', function (e, json) {
                        $.ajax({ url: '/ajax/datatables/dns-trap-audit/audit',
                            data: removeData,
                            type: 'post'
                        });
                    });

                    dnsTrapTable = $('#dnsTrap-table').dataTable({
                        "sScrollX": "100%",
                        "sScrollY": "100%",
                        "sAjaxSource": "/ajax/datatables/dns_trap",
                        "bScrollCollapse": true,
                        "bJQueryUI": true,
                        "bPaginate": false,
                        "sDom": '<"table-header"ClTFfiR>t',
                        "oTableTools": {
                            "sRowSelect": "single",
                            "aButtons": [
                                {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":dnsTrapEditor},
                                {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":dnsTrapEditor},
                                {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":dnsTrapEditor},
                                {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                    {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                    {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                                ]}
                            ],
                            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                        },
                        "aoColumns": [
                            { "mData": "Function" },
                            { "mData": "VSN", "sWidth": "80px" },
                            { "mData": "Tag","sWidth": "300px" },
                            { "mData": "HostIP", "sWidth": "200px" },
                            { "mData": "Reference","sWidth": "200px" },
                            { "mData": "Name", "sWidth": "350px" }
                        ]
                    }).rowGrouping({"bExpandableGrouping": true, "sGroupingClass": 'group'});


                });
            }, 10);
        }
        setTimeout(function() { vsnHighlight(dnsTrapTable, 0); }, 200);
    };

// table generation for the SubnetAdv table
    function load_subnet_adv() {


        $("#tab-name").empty().append("Subnet Advertising");

        // generate the table
        tableHeight = 470;
        tableName = "subnetAdvTable";
        if (typeof(subnetAdvTable) == "undefined"){
            setTimeout(function () {
                $(document).ready(function(){
                    subnetAdvEditor = new $.fn.dataTable.Editor( {
                        "domTable": "#subnetadv-table",
                        "ajaxUrl": "/ajax/datatables/subnetadv",
                        "fields" : [
                            {
                                // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                                "label": "Site Name",
                                "name": "SiteName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                            },{
                                "label": "VSN",
                                "name": "VSN",
                                "type": "select",
                                "ipOpts": vsnOpts,
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.VSN ); }
                            },{
                                "label": "Subnet Name:",
                                "name": "SubnetName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SubnetName ); }
                            },{
                                "label": "Network:",
                                "name": "Network",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Network ); }
                            },{
                                "label": "Public IP Adv:",
                                "name": "PublicIPAdv",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.PublicIPAdv ); }
                            },{
                                "label": "Last Updated",
                                "name": "UpdatedTime",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                            },{
                                "label": "Last Edited By",
                                "name": "EditedBy",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                            },{
                                "label": "Comment",
                                "name": "Comment",
                                "type": "textarea",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                            }
                        ]
                    });




                    // These functions execute on click of edit and create, and can hide these fields from the form
                    subnetAdvEditor.on('onInitEdit', function () {
                        this.hide('SiteName');
                        this.show('UpdatedTime');
                        this.show('EditedBy');
                        this.show('Comment');
                        this.disable('SiteName');
                        this.disable('VSN');
                        this.disable('UpdatedTime');
                        this.disable('EditedBy');
                    });

                    subnetAdvEditor.on('onInitCreate', function () {
                        //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                        //var siteDataSplit = document.getElementById("site-header").innerHTML.split("-");
                        //for (var j = 0; j < siteDataSplit.length; j++){
                        //    siteDataSplit[j] = siteDataSplit[j].trim();
                        //}
                        this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                        this.hide('SiteName');
                        this.hide('UpdatedTime');
                        this.hide('EditedBy');
                        this.hide('Comment');
                        this.set('Comment', "Create");

                    });

                    // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                    subnetAdvEditor.on('initRemove', function (e, node, data) {
                        removeData = data[0];
                        removeData['Created'] = 0;
                        removeData['Removed'] = 1;
                    });

                    // Set the last edited by field to the current username before submitting data
                    subnetAdvEditor.on('preSubmit', function (e, data, action) {
                        data['data']['EditedBy'] = username;
                    });

                    // Fires when a successful edit/creation has happened to copy the row to the audit table.
                    subnetAdvEditor.on('submitSuccess', function (e, json, data) {
                        data['Removed'] = 0;
                        data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                        $.ajax({ url: '/ajax/datatables/subnet-adv-audit/audit',
                            data: data,
                            type: 'post'
                        });
                    });

                    // Fires right after a remove happens to copy the row to the audit table.
                    subnetAdvEditor.on('remove', function (e, json) {
                        $.ajax({ url: '/ajax/datatables/subnet-adv-audit/audit',
                            data: removeData,
                            type: 'post'
                        });
                    });

                    subnetAdvTable = $('#subnetadv-table').dataTable({
                            //"sScrollX": "100%",
                            "sScrollY": "100%",
                            "sAjaxSource": "/ajax/datatables/subnetadv",
                            "bScrollCollapse": true,
                            "bJQueryUI": true,
                            "bPaginate": false,
                            "sDom": '<"table-header"ClTfiR>t',
                            "aaSorting": [[ 1, "asc" ]],
                            "oTableTools": {
                                "sRowSelect": "single",
                                "aButtons": [
                                    {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":subnetAdvEditor},
                                    {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":subnetAdvEditor},
                                    {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":subnetAdvEditor},
                                    {"sExtends": "copy"},
                                    {"sExtends": "csv"},
                                    {"sExtends": "pdf"},
                                    {"sExtends": "print", "sInfo": "<h2>Print view</h2><p>Please use your browser's print function to print this table. Press escape when finished."}
                                ],
                                "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                            },
                            "aoColumns": [
                                { "mData": "VSN", "sWidth": "80px" },
                                { "mData": "SubnetName", "sWidth": "15%" },
                                { "mData": "Network", "sWidth": "15%" },
                                { "mData": "PublicIPAdv","sWidth": "10%" }
                            ]
                        }
                    );


                });
            }, 10);

        }
        setTimeout(function() { vsnHighlight(subnetAdvTable, 0); }, 200);
    };

// table generation for the SubnetAdv table
    function load_cardslotting() {


        $("#tab-name").empty().append("Card Slotting");

        // generate the table
        tableHeight = 470;
        tableName = "cardSlottingTable";
        if (typeof(cardSlottingTable) == "undefined"){
            setTimeout(function () {
                $(document).ready(function(){
                    cardSlottingEditor = new $.fn.dataTable.Editor( {
                        "domTable": "#cardslotting-table",
                        "ajaxUrl": "/ajax/datatables/cardslotting",
                        "fields" : [
                            {
                                // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                                "label": "Site Name",
                                "name": "SiteName",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.SiteName ); }
                            },{
                                "label": "Card Type:",
                                "name": "CardType",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.CardType ); }
                            },{
                                "label": "Slot:",
                                "name": "Slot",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Slot ); }
                            },{
                                "label": "Interface Num:",
                                "name": "InterfaceNum",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.InterfaceNum ); }
                            },{
                                "label": "Device:",
                                "name": "Device",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Device ); }
                            },{
                                "label": "MIC:",
                                "name": "MIC",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.MIC ); }
                            },{
                                "label": "Last Updated",
                                "name": "UpdatedTime",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.UpdatedTime ); }
                            },{
                                "label": "Last Edited By",
                                "name": "EditedBy",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                            },{
                                "label": "Comment",
                                "name": "Comment",
                                "type": "textarea",
                                data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                            }
                        ]
                    });




                    // These functions execute on click of edit and create, and can hide these fields from the form
                    cardSlottingEditor.on('onInitEdit', function () {
                        this.hide('SiteName');
                        this.show('UpdatedTime');
                        this.show('EditedBy');
                        this.show('Comment');
                        this.disable('SiteName');
                        this.disable('VSN');
                        this.disable('Device');
                        this.disable('UpdatedTime');
                        this.disable('EditedBy');
                    });

                    cardSlottingEditor.on('onInitCreate', function () {
                        //var siteDataSplit = document.getElementById("lnk_nodename").innerHTML;
                        //var siteDataSplit = document.getElementById("site-header").innerHTML.split("-");
                        //for (var j = 0; j < siteDataSplit.length; j++){
                        //    siteDataSplit[j] = siteDataSplit[j].trim();
                        //}
                        this.set('SiteName', document.getElementById("lnk_nodename").innerHTML);
                        this.hide('SiteName');
                        this.hide('UpdatedTime');
                        this.hide('EditedBy');
                        this.hide('Comment');
                        this.set('Comment', "Create");
                    });

                    // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
                    cardSlottingEditor.on('initRemove', function (e, node, data) {
                        removeData = data[0];
                        removeData['Created'] = 0;
                        removeData['Removed'] = 1;
                    });

                    // Set the last edited by field to the current username before submitting data
                    cardSlottingEditor.on('preSubmit', function (e, data, action) {
                        data['data']['EditedBy'] = username;
                    });

                    // Fires when a successful edit/creation has happened to copy the row to the audit table.
                    cardSlottingEditor.on('submitSuccess', function (e, json, data) {
                        data['Removed'] = 0;
                        data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
                        $.ajax({ url: '/ajax/datatables/cardslotting-audit/audit',
                            data: data,
                            type: 'post'
                        });
                    });

                    // Fires right after a remove happens to copy the row to the audit table.
                    cardSlottingEditor.on('remove', function (e, json) {
                        $.ajax({ url: '/ajax/datatables/cardslotting-audit/audit',
                            data: removeData,
                            type: 'post'
                        });
                    });

                    cardSlottingTable = $('#cardslotting-table').dataTable({
                            //"sScrollX": "100%",
                            "sScrollY": "100%",
                            "sAjaxSource": "/ajax/datatables/cardslotting",
                            "bScrollCollapse": true,
                            "bJQueryUI": true,
                            "bPaginate": false,
                            "sDom": '<"table-header"ClTfiR>t',
                            "aaSorting": [[ 1, "asc" ]],
                            "oTableTools": {
                                "sRowSelect": "single",
                                "aButtons": [
                                    {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":cardSlottingEditor},
                                    {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":cardSlottingEditor},
                                    {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":cardSlottingEditor},
                                    {"sExtends": "copy"},
                                    {"sExtends": "csv"},
                                    {"sExtends": "pdf"},
                                    {"sExtends": "print", "sInfo": "<h2>Print view</h2><p>Please use your browser's print function to print this table. Press escape when finished."}
                                ],
                                "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                            },
                            "aoColumns": [
                                { "mData": "Slot", "sWidth": "15%" },
                                { "mData": "CardType", "sWidth": "15%" },
                                { "mData": "InterfaceNum","sWidth": "10%" },
                                { "mData": "Device","sWidth": "10%" },
                                { "mData": "MIC","sWidth": "10%" }
                            ]
                        }
                    );


                });
            }, 10);

        }
        setTimeout(function() { vsnHighlight(cardSlottingTable, 0); }, 200);
    };


    $(document).ready(function() {
        rolesEditor = new $.fn.dataTable.Editor( {
            "domTable": "#roles_tbl",
            "ajaxUrl": "/ajax/datatables/usermanagement_roles",
            "fields" :
                [ {
                    // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                    "label": "Role Name",
                    "name": "role_name",
                    data: function ( data, type, set ) { return Encoder.htmlDecode( data.role_name ); }
                },{
                    "label": "Role Description",
                    "name": "role_description",
                    data: function ( data, type, set ) { return Encoder.htmlDecode( data.role_description ); }
                },{
                    "label": "Edited By",
                    "name": "EditedBy",
                    data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                }
                ]
        } );

        // These functions execute on click of edit and create, and can hide these fields from the form
        rolesEditor.on('onInitEdit', function () {
            this.show('role_name');
            this.show('role_description');
            this.hide('EditedBy');
        });

        rolesEditor.on('onInitCreate', function () {
            this.show('role_name');
            this.show('role_description');
            this.hide('EditedBy');
        });

        // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
        //rolesEditor.on('initRemove', function (e, node, data) {
        //    removeData = data[0];
        //    removeData['Created'] = 0;
        //    removeData['Removed'] = 1;
        //});

        // Set the last edited by field to the current username before submitting data
        rolesEditor.on('preSubmit', function (e, data, action) {
            data['data']['EditedBy'] = username;
        });

        // Fires when a successful edit/creation has happened to copy the row to the audit table.
        //rolesEditor.on('submitSuccess', function (e, json, data) {
        //    data['Removed'] = 0;
        //    data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
        //    $.ajax({ url: '/ajax/datatables/roles-audit/audit',
        //             data: data,
        //             type: 'post'
        //           });
        //});

        // Fires right after a remove happens to copy the row to the audit table.
        //rolesEditor.on('remove', function (e, json) {
        //    $.ajax({ url: '/ajax/datatables/roles-audit/audit',
        //        data: removeData,
        //        type: 'post'
        //    });
        //});

        $('#roles_tbl').DataTable( {
            //"sScrollX": "100%",
            "sScrollY": "100%",
            "sAjaxSource": "/ajax/datatables/usermanagement_roles",
            "bScrollCollapse": true,
            "bJQueryUI": true,
            "bPaginate": false,
            "bSort": true,
            "aaSorting": [[ 1, "desc" ]],
            "sDom": '<"table-header"ClTfiR>t',
            "oTableTools": {
                "sRowSelect": "single",
                "aButtons": [
                    {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":rolesEditor},
                    {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":rolesEditor},
                    {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":rolesEditor},
                    {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                        {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                        {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                        {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                        {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                    ]}
                ],
                "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
            },
            "aoColumns": [
                { "mData": "role_name", "sWidth": "10%" },
                { "mData": "role_description", "sWidth": "30%" },
                { "mData": "EditedBy", "sWidth": "10%" }
            ]
        });
    });

    $(document).ready(function() {
        permissionsEditor = new $.fn.dataTable.Editor( {
            "domTable": "#permissions_tbl",
            "ajaxUrl": "/ajax/datatables/usermanagement_perms",
            "fields" :
                [ {
                    "label": "Permission Description",
                    "name": "permission_description",
                    data: function ( data, type, set ) { return Encoder.htmlDecode( data.role_description ); }
                },{
                    "label": "Edited By",
                    "name": "EditedBy",
                    data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                }
                ]
        } );

        // These functions execute on click of edit and create, and can hide these fields from the form
        permissionsEditor.on('onInitEdit', function () {
            this.show('permission_description');
            this.hide('EditedBy');
        });

        permissionsEditor.on('onInitCreate', function () {
            this.show('permission_description');
            this.hide('EditedBy');
        });

        // store the data to be removed in an array, so that we can write it to the audit table after it's been removed.
        //permissionsEditor.on('initRemove', function (e, node, data) {
        //    removeData = data[0];
        //    removeData['Created'] = 0;
        //    removeData['Removed'] = 1;
        //});

        // Set the last edited by field to the current username before submitting data
        permissionsEditor.on('preSubmit', function (e, data, action) {
            data['data']['EditedBy'] = username;
        });

        // Fires when a successful edit/creation has happened to copy the row to the audit table.
        //permissionsEditor.on('submitSuccess', function (e, json, data) {
        //    data['Removed'] = 0;
        //    data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
        //    $.ajax({ url: '/ajax/datatables/perms-audit/audit',
        //             data: data,
        //             type: 'post'
        //           });
        //});

        // Fires right after a remove happens to copy the row to the audit table.
        //permissionsEditor.on('remove', function (e, json) {
        //    $.ajax({ url: '/ajax/datatables/perms-audit/audit',
        //        data: removeData,
        //        type: 'post'
        //    });
        //});

        $('#permissions_tbl').DataTable( {
            //"sScrollX": "100%",
            "sScrollY": "100%",
            "sAjaxSource": "/ajax/datatables/usermanagement_perms",
            "bScrollCollapse": true,
            "bJQueryUI": true,
            "bPaginate": false,
            "bSort": true,
            "aaSorting": [[ 1, "desc" ]],
            "sDom": '<"table-header"ClTfiR>t',
            "oTableTools": {
                "sRowSelect": "single",
                "aButtons": [
                    {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":permissionsEditor},
                    {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":permissionsEditor},
                    {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":permissionsEditor},
                    {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                        {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                        {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                        {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                        {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                    ]}
                ],
                "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
            },
            "aoColumns": [
                { "mData": "permission_description", "sWidth": "30%" },
                { "mData": "EditedBy", "sWidth": "10%" }
            ]
        });
    });


    $(document).ready(function() {
        wz_asyncEditor = new $.fn.dataTable.Editor( {
            "domTable": "#async-tbl",
            "ajaxUrl": "/ajax/datatables/async",
            "fields" :
                [ {
                    // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                    "label": "Serial Number",
                    "name": "Serial",
                    data: function ( data, type, set ) { return Encoder.htmlDecode( data.Serial ); }
                },{
                    "label": "Bay Location",
                    "name": "Bay",
                    data: function ( data, type, set ) { return Encoder.htmlDecode( data.Bay ); }
                },{
                    "label": "Hostname",
                    "name": "Hostname",
                    data: function ( data, type, set ) { return Encoder.htmlDecode( data.Hostname ); }
                },{
                    "label": "Description",
                    "name": "Description",
                    data: function ( data, type, set ) { return Encoder.htmlDecode( data.Description ); }
                },{
                    "label": "OOB Command",
                    "name": "OSVersion",
                    data: function ( data, type, set ) { return Encoder.htmlDecode( data.OSVersion ); }
                },{
                    "label": "Comments",
                    "name": "Comment",
                    data: function ( data, type, set ) { return Encoder.htmlDecode( data.Comment ); }
                },{
                    "label": "Edited By",
                    "name": "EditedBy",
                    data: function ( data, type, set ) { return Encoder.htmlDecode( data.EditedBy ); }
                }
                ]
        } );

        // These functions execute on click of edit and create, and can hide these fields from the form
        wz_asyncEditor.on('onInitEdit', function () {
            this.show('Serial');
            this.show('Bay');
            this.show('Hostname');
            this.show('Description');
            this.show('OSVersion');
            this.show('Comment');
            this.hide('EditedBy');
        });

        wz_asyncEditor.on('onInitCreate', function () {
            this.show('Serial');
            this.show('Bay');
            this.show('Hostname');
            this.show('Description');
            this.show('OSVersion');
            this.show('Comment');
            this.hide('EditedBy');
        });

        // Set the last edited by field to the current username before submitting data
        wz_asyncEditor.on('preSubmit', function (e, data, action) {
            data['data']['EditedBy'] = username;
        });

        // Fires when a successful edit/creation has happened to copy the row to the audit table.
        //wz_asyncEditor.on('submitSuccess', function (e, json, data) {
        //    data['Removed'] = 0;
        //    data['Created'] = (data['Comment'] === "Create" ? 1 : 0);
        //    $.ajax({ url: '/ajax/datatables/async-audit/audit',
        //             data: data,
        //             type: 'post'
        //           });
        //});

        // Fires right after a remove happens to copy the row to the audit table.
        //wz_asyncEditor.on('remove', function (e, json) {
        //    $.ajax({ url: '/ajax/datatables/async-audit/audit',
        //        data: removeData,
        //        type: 'post'
        //    });
        //});

        $('#async-tbl').DataTable( {
            //"sScrollX": "100%",
            "sScrollY": "100%",
            "sAjaxSource": "/ajax/datatables/async",
            "bScrollCollapse": true,
            "bJQueryUI": true,
            "bPaginate": false,
            "bSort": true,
            "aaSorting": [[ 1, "desc" ]],
            "sDom": '<"table-header"ClTfiR>t',
            "oTableTools": {
                "sRowSelect": "single",
                "aButtons": [
                    {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":wz_asyncEditor},
                    {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":wz_asyncEditor},
                    {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":wz_asyncEditor},
                    {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                        {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                        {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                        {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                        {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                    ]}
                ],
                "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
            },
            "aoColumns": [
                { "mData": "Serial", "sWidth": "15%" },
                { "mData": "Bay", "sWidth": "10%" },
                { "mData": "Hostname", "sWidth": "15%" },
                { "mData": "Description", "sWidth": "20%" },
                { "mData": "OSVersion", "sWidth": "20%" },
                { "mData": "Comment", "sWidth": "10%" },
                { "mData": "EditedBy", "sWidth": "10%" }
            ]
        });
    });



// load the datatables when the links to show its tab is clicked
    $("#lnk_contact").click(function() {
        $("#tab-name").empty().append("Contact Information");
        tableName = "siteInfoTable";
        tableHeight = 275;
    });

    $("#lnk_async").click(function() {
        load_async();
    });

    $("#lnk_nodedevicenames").click(function() {
        load_nodedevicenames();
    });

    $("#lnk_tdrinfo").click(function() {
        load_tdrinfo();
    });


    $("#lnk_sitedocs").click(function() {
        $("#tab-name").empty().append("Documents");
    });

    $("#lnk_sitephotos").click(function() {
        $("#tab-name").empty().append("Photos");
    });

    $("#lnk_sitediagram").click(function() {
        $("#tab-name").empty().append("Diagram");
    });


    $("#lnk_sitedata").click(function() {
        load_custaddr();
    });

    $("#lnk_netconn").click(function() {
        tableName = "netConnectionsTable";
        tableHeight = 320;
        load_netconn();
    });

    $("#lnk_cust_ckts").click(function() {
        load_cust_ckts();
    });

    $('#lnk_mx01').click(function(){
        tableName = "mx01Table";
        tableHeight = 320;
        load_mx01();
    });

    $('#lnk_mx02').click(function(){
        load_mx02();
    });

    $('#lnk_ns01').click(function(){
        tableName = "ns01Table";
        tableHeight = 290;
        load_ns01();
    });

    $('#lnk_ns02').click(function(){
        load_ns02();
    });

    $('#lnk_ipschema').click(function(){
        load_ipschema();
    });

    $('#lnk_subnetinfo').click(function(){
        load_subnetinfo();
    });

    $('#lnk_pubaddr').click(function(){
        tableName = "publicAddress";
        tableHeight = 320;
        load_subnetinfo();
    });

    $('#lnk_idnip').click(function(){
        load_idnip();
    });

    $('#lnk_privaddr').click(function(){
        load_privaddr();
    });

    $('#lnk_dnstrap').click(function(){
        load_dnstrap();
    });

    $('#lnk_subnetadv').click(function(){
        load_subnet_adv();
    });

    $('#lnk_cardslotting').click(function(){
        load_cardslotting();
    });


// load the datatables when page loads if tab already open
    switch (location.hash) {
        case '#t_async':
            load_async();
            break;
        case '#t_nodedevicenames':
            load_nodedevicenames();
            break;
        case '#t_tdr':
            load_tdrinfo();
            break;
        case '#t_custaddr':
            load_custaddr();
            break;
        case '#t_netconn':
        case '#pill_netconn':
            load_netconn();
            break;
        case '#pill_cust_ckts':
            load_cust_ckts();
            break;
        case '#t_mx960':
        case '#pill_mx01':
            load_mx01();
            break;
        case '#pill_mx02':
            load_mx02();
            break;
        case '#t_nsport':
        case '#pill_ns01':
            load_ns01();
            break;
        case '#pill_ns02':
            load_ns02();
            break;
        case '#t_ipschema':
            load_ipschema();
            break;
        case '#t_subnetinfo':
        case '#7_1':
            load_subnetinfo();
            break;
        case '#7_2':
            load_idnip();
            break;
        case '#7_3':
            load_privaddr();
            break;
        case '#t_dnstrap':
            load_dnstrap();
            break;
        case '#t_subnetadv':
            load_subnet_adv();
            break;
        case '#t_cardslotting':
            load_cardslotting();
            break;
    }

}); // document.ready
