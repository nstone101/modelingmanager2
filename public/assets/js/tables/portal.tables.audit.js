/**
 * This file contains all the click functions when a user clicks on different tabs on the database pages.
 * These will generate the table as a DataTable, using the specified parameters.
**/
$(function(){

// defines the variables for the window height, the tablename for window resizing,
// and defaults the tableHeight to 0
var $window = $(window);
var tableName = undefined;
var tableHeight = 0;
var anOpen = [];  // Array for tracking the table rows for opening and closing.

// defaults the table variables for all available tables
var asyncTable;
var siteInfoTable;
var tdrTable;
var tagsTable;
var customerTable;
var netConnectionsTable;
var customerCircuitsTable;
var mx01PortsTable;
var mx02PortsTable;
var ns01PortsTable;
var ns02PortsTable;
var ipSchemaTable;
var publicIPTable;
var privateIPTable;
var idnIPTable;
var dnsTrapTable;
var subnetAdvTable;
var cardSlottingTable;

// These are the editor variables to make tables editable.
var asyncEditor;
var siteInfoEditor;
var tagsEditor;
var tdrEditor;
var customerInfoEditor;
var netConnectionsEditor;
var customerCircuitsEditor;
var mx01Editor;
var mx02Editor;
var ns01Editor;
var ns02Editor;
var ipSchemaEditor;
var publicIPEditor;
var privateIPEditor;
var idnIPEditor;
var dnsTrapEditor;
var subnetAdvEditor;
var cardSlottingEditor;

// set the encoder type to html entity
// this makes a dependancy on encoder.js
Encoder.EncodeType = "entity";

/**
 * Function definitions
 */
// This function is used to make a row grouping that includes the subnet and network mask.
function RowGroupSubnet(group, networkTab){
    var json = new Array(group, networkTab);
    json = JSON.stringify( json );
    var networkString = '';
    // We then take our JSON encoded table row, and pass it to a php file which builds an SQL insert statement
    $.ajax({
        type: 'GET',
        async: false,
        url: "/ajax/datatables/subnet_details?json=" + json,
        success: function (data) {
            networkString = " - " + data;
        }});
    return networkString;
}

// defines the function for calculating the height of the table without the header space
var calcDataTableHeight = function() {
    return Math.round($window.height() - tableHeight);
};

// This function opens the details sub-row on an audit table.
// Called when the first column (open or close image) is clicked on.
// oTable is the Table  object, nTr is the row node, item is the <img> element in the first column. 
function openDetails(oTable, nTr, item) {
    var i = $.inArray( nTr, anOpen );
    if ( i === -1 ) {
        item.attr( 'src', "/assets/images/details_close.png" );
        var oData = oTable.fnGetData( nTr );
        var sOut =
            '<div class="innerDetails">'+
                '<table class="table table-bordered">'+
                    '<tr><td style="width: 125px;">Edited By:</td><td>'+oData.EditedBy+'</td></tr>'+
                    '<tr><td style="width: 125px;">Comment:</td><td>'+nl2br(oData.Comment)+'</td></tr>'+
                '</table>'+
            '</div>';
        var nDetailsRow = oTable.fnOpen( nTr, sOut, 'details' );
        $('div.innerDetails', nDetailsRow).slideDown();
        anOpen.push( nTr );
    } else {
        item.attr( 'src', "/assets/images/details_open.png" );
        $('div.innerDetails', $(nTr).next()[0]).slideUp(function () {
            oTable.fnClose( nTr );
        });
        anOpen.splice( i, 1 );
    }
}

// This function is used to change the background color of td elements that changed values in relation to the previous row
// or for deleted rows.
function modHighlight(table, deleteCol, createCol) {
    // Make sure the table is ready, otherwise, keep recursion going
    if (typeof(table) != "undefined") {
        // This is a variable containing all the rows in the datatable.
        var oRow = table.dataTable().$('tr');
        // We loop through each row finding changes and deletions.
        for (var i = 0; i < oRow.length; i++) {
            // Grab this row and the previous row.
            var row = oRow[i].getElementsByTagName('td');
            if (i>0) { // Can't grab the previous row on the first iteration.
                var preRow = oRow[i-1].getElementsByTagName('td');
                // The current row is the same row ID as the previous row.
                if (row[2].innerHTML === preRow[2].innerHTML) {
                    // loop through and find columns that changed
                    for (var j = 0; j < row.length; j++) {
                        // j>1 test prevents highlighting modify time column.
                        // we also should ignore the delete and created columns.
                        if ((j>1) && (j != deleteCol) && (j != createCol)) {
                            if (row[j].innerHTML != preRow[j].innerHTML) {
                                $(row[j]).css({'backgroundColor': '#FFFF99'});
                            }
                        }
                    }
                }
            }
            // Removed Rows
            if (row[deleteCol].innerHTML === "1") {
                $(row).css({'backgroundColor': '#fa6a5f'});
            }
            // Created rows
            if (row[createCol].innerHTML === "1") {
                $(row).css({'backgroundColor': '#99FF99'});
            }
        }
    }
    // If the table wasn't ready yet, call this function again in 100 milliseconds.
    else {
        setTimeout(function() {modHighlight(table, deleteCol, createCol);}, 100);
    }
}

/**
 * End of Function Definitions
 */

$("#lnk_contact").click(function() {
    //$("#tab-name").empty().append("Contact Information");
    tableName = "siteInfoTable";
    tableHeight = 275;
});

/*$("#lnk_async").click(function() {
    $("#tab-name").empty().append("Async");
});

$("#lnk_tdrinfo").click(function() {
    $("#tab-name").empty().append("TDR");
});

$("#lnk_sitedocs").click(function() {
    // Change the Header text
    $("#tab-name").empty().append("Documents");
});

$("#lnk_sitephotos").click(function() {
    // Change the Header text
    $("#tab-name").empty().append("Photos");
});

$("#lnk_sitediagram").click(function() {
    // Change the Header text
    $("#tab-name").empty().append("Diagram");
});*/

/**
 * The following three are needed because their respective table generation functions are
 * called upon clicking of the items in the 'Site Data' Dropdown button, and therefore
 * we have to add the change of resize parameters when their tabs are clicked.
 */

// Activated when the user clicks on the Network Connections tab to set the resize parameters
$(document).on('click','#lnk_netconn',function(){
    tableName = "netConnectionsTable";
    tableHeight = 275;
});

// Activated when the user clicks on the MX01 tab to set the resize parameters
$(document).on('click','#lnk_mx01',function(){
    tableName = "mx01Table";
    tableHeight = 275;
});

// Activated when the user clicks on the NS01 tab to set the resize parameters
$(document).on('click','#lnk_ns01',function(){
    tableName = "ns01Table";
    tableHeight = 290;
});

// Activated when the user clicks on the Public Address Tab to set the resize parameters
$(document).on('click','#lnk_publicaddr',function(){
   tableName = "publicAddress";
   tableHeight = 320;
});

/**
 * Start of the Table Generation Functions.
 */
// table generation for the Master Tags editor
$("#tbl_mastertag").ready(function() {
    tableHeight = 280;
    tableName = "tagsTable";
    if (typeof(tagsTable) == "undefined"){
        setTimeout(function () {
            $(document).ready(function(){
                tagsTable = $('#tbl_mastertag').dataTable({
                    //"sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/mastertags-audit/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "aaSorting": [[ 2, "asc" ], [ 1, "asc"]],
                    "sDom": '<"table-header"ClTRfi>t',
                    "oTableTools": {
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#tbl_mastertag td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#tbl_mastertag td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "90px"},
                        { "mData": "RowID", "sWidth": "50px" },
                        { "mData": "Tag", "sWidth": "350px" },
                        { "mData": "Removed", "sWidth": "20px" },
                        { "mData": "Created", "sWidth": "20px" }
                    ]
                });
                $(document).on( 'click', '#masterTag-table td.control', function () {
                    openDetails(tagsTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    setTimeout(function() { modHighlight(tagsTable, 4, 5); }, 200);
});

// table generation for the Async table
$("#lnk_async").click(function() {
    tableHeight = 275;
    tableName = "asyncTable";
    if (typeof(asyncTable) == "undefined"){
        setTimeout(function () {
            $(document).ready(function(){
                asyncTable = $('#async-table').dataTable({
                    "sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/async-audit/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    // "bStateSave": false,
                    /* the sDom parameters that we are using:
                     * <"table-header"> puts all the contained items within the table-header div for sizing and display on firefox.
                     * 'C' is the column visibilty button.  'l' is "length changing" according to DT site, not sure what it is
                     * 'T' are the Table tools buttons, copy, csv, print, etc.  'f' is the filter search bar
                     * 'i' is the information showing the number of viewable entries. 'R' is the ability to resize columns
                     * 't' is the table data itself.  'F' will autofill the widths of the columns to the screen width */
                    "sDom": '<"table-header"ClTFRfi>t',
                    // Sort first by row ID, then secondly by Modify Time.
                    "aaSorting": [[ 2, "asc"], [ 1, "asc" ]],
                    "oTableTools": {
                        // "sRowSelect": "single",
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#async-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#async-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "16%"},
                        { "mData": "RowID", "sWidth": "5%" },
                        { "mData": "Serial", "sWidth": "14%" },
                        { "mData": "Bay", "sWidth": "8%" },
                        { "mData": "Hostname", "sWidth": "16%" },
                        { "mData": "Description", "sWidth": "20%" },
                        { "mData": "ASYNC", "sWidth": "20%" },
                        { "mData": "OSVersion", "sWidth": "20%" },
                        { "mData": "Removed", "sWidth": "5%" },
                        { "mData": "Created", "sWidth": "5%" }
                    ]
                });

                // This function is used to open or close a details row when the image is clicked on.
                $(document).on( 'click', '#async-table td.control', function () {
                    openDetails(asyncTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    // This function waits 200 milliseconds after loading to try and change the color of the table data
    // based on if the item changed or was deleted.
    setTimeout(function() {
        // the parameters are ,--- The current table
        //                    |  ,-- Deleted column index (starting from 0)
        //                    |  |  ,--- Created Column index (starting from 0)
        modHighlight(asyncTable, 9, 10);
    }, 200);
});

// table generation for the Site Info table
$("#info-table").ready(function() {
    tableHeight = 275;
    tableName = "infoTable";
    if (typeof(infoTable) == "undefined"){
        setTimeout(function () {
            $(document).ready(function(){
                infoTable = $('#info-table').dataTable({
                    "sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/tabs-audit/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "sDom": '<"table-header"ClTFRfi>t',
                    "aaSorting": [[ 1, "asc" ]],
                    "oTableTools": {
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#info-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#info-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<i class="fa fa-exclamation-circle"></i>', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "116px"},
                        { "mData": "SiteName", "sWidth": "54px" },
                        { "mData": "SiteGroup", "sWidth": "76px" },
                        { "mData": "SiteCode", "sWidth": "50px" },
                        { "mData": "ClliCode", "sWidth": "62px" },
                        { "mData": "SiteMux", "sWidth": "36px" },
                        { "mData": "SiteAddress", "sWidth": "300px" },
                        { "mData": "ContactName", "sWidth": "90px" },
                        { "mData": "ContactPhone", "sWidth": "80px" },
                        { "mData": "ContactEmail", "sWidth": "100px" },
                        { "mData": "Lab", "sWidth": "36px" },
                        { "mData": "Removed", "sWidth": "40px" },
                        { "mData": "Created", "sWidth": "36px" }
                    ]
                });

                // This function is used to open or close a details row when the image is clicked on.
                $(document).on( 'click', '#info-table td.control', function () {
                    openDetails(infoTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    // This function waits 200 milliseconds after loading to try and change the color of the table data
    // based on if the item changed or was deleted.
    setTimeout(function() { modHighlight(infoTable, 13, 14); }, 200);
});

// table generation for the TDRInfo table
// This is activated when the user clicks on the TDRInfo tab.
$("#lnk_tdrinfo").click(function() {
    if (typeof(tdrTable) == "undefined"){
        setTimeout(function () {
            $(document).ready(function(){
                tdrTable = $('#tdr-table').dataTable({
                    "sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/tdr-audit/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "sDom": '<"table-header"ClTFRfi>t',
                    "aaSorting": [[ 2, "asc" ], [ 1, "asc" ]],
                    "oTableTools": {
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#tdr-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#tdr-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "12%"},
                        { "mData": "RowID", "sWidth": "5%" },
                        { "mData": "SBC", "sWidth": "14%" },
                        { "mData": "Status", "sWidth": "10%" },
                        { "mData": "SBCPairName", "sWidth": "15%" },
                        { "mData": "TDRServerName","sWidth": "15%" },
                        { "mData": "TDRServerIP","sWidth": "15%" },
                        { "mData": "Port", "sWidth": "5%" },
                        { "mData": "IPs", "sWidth": "15%" },
                        { "mData": "Removed", "sWidth": "5%" },
                        { "mData": "Created", "sWidth": "5%" }
                    ]
                });

                $(document).on( 'click', '#tdr-table td.control', function () {
                    openDetails(tdrTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }

    setTimeout(function() { modHighlight(tdrTable, 10, 11); }, 200);
});

// table generation for the Customer Addressing table
$("#lnk_sitedata").click(function() {
    // Change the Header text
    document.getElementById('tab-name').innerHTML = "Customer Information";

    // generate the table
    tableHeight = 230;
    tableName = "customerTable";
    if (typeof(customerTable) == "undefined"){
        setTimeout(function () {
            $(document).ready(function(){
                customerTable = $('#customer-table').dataTable({
                    "sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/cust-addr-audit/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    // "bSort": false,
                    "aaSorting": [[ 2, "asc" ], [ 1, "asc"]],
                    "sDom": '<"table-header"ClTFfiR>t',
                    "oTableTools": {
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#customer-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#customer-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "12%"},
                        { "mData": "RowID", "sWidth": "5%" },
                        { "mData": "SPIP", "sWidth": "10%" },
                        { "mData": "HostOffset", "sWidth": "10%" },
                        { "mData": "IPAddress", "sWidth": "10%" },
                        { "mData": "Device", "sWidth": "25%" },
                        { "mData": "Description", "sWidth": "25%" },
                        { "mData": "Removed", "sWidth": "5%" },
                        { "mData": "Created", "sWidth": "5%" }
                    ]
                });
                // }).rowGrouping({"bExpandableGrouping": true, "sGroupingClass": 'group'});

                $(document).on( 'click', '#customer-table td.control', function () {
                    openDetails(customerTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    setTimeout(function() { modHighlight(customerTable, 8, 9); }, 200);
});

// Network Connections Audit Table
$("#lnk_netconn").click(function() {
    // Checks which tab is active to set the resizing parameters
    if ($('#t_netconn_1').hasClass('active')){
        // Change the Header text
        document.getElementById('tab-name').innerHTML = "Network Connections";
        tableHeight = 470;
        tableName = "netConnectionsTable";
    }
    else if ($('#t_cust_ckts').hasClass('active')){
        // Change the Header text
        document.getElementById('tab-name').innerHTML = "Customer Circuits";
        tableName = "custCircuitsTable";
        tableHeight = 470;
    }

    if (typeof(netConnectionsTable) == "undefined"){
        setTimeout(function () {
            $(document).ready(function(){
                netConnectionsTable = $('#networkConnections-table').dataTable({
                    //"sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/net-conn-audit/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "sDom": '<"table-header"ClTRfi>t',
                    "aaSorting": [[ 2, "asc" ], [1, "asc"]],
                    "oTableTools": {
                        "sRowSelect": "single",
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#networkConnections-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#networkConnections-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "12%"},
                        { "mData": "RowID", "sWidth": "5%" },
                        { "mData": "NodeDevice", "sWidth": "8%" },
                        { "mData": "NodeInterface", "sWidth": "10%" },
                        { "mData": "InterfaceTag","sWidth": "20%" },
                        { "mData": "RemoteDeviceName", "sWidth": "10%" },
                        { "mData": "Network","sWidth": "7%" },
                        { "mData": "Speed", "sWidth": "5%" },
                        { "mData": "RemoteDeviceType", "sWidth": "10%" },
                        { "mData": "RemoteDeviceInt", "sWidth": "10%" },
                        { "mData": "RemoteDeviceCircuit", "sWidth": "10%" },
                        { "mData": "Description","sWidth": "15%" },
                        { "mData": "Removed", "sWidth": "5%" },
                        { "mData": "Created", "sWidth": "5%" }
                    ]
                });

                $(document).on( 'click', '#networkConnections-table td.control', function () {
                    openDetails(netConnectionsTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    setTimeout(function() { modHighlight(netConnectionsTable, 13, 14); }, 200);
});


// This is activated when the user clicks on the Customer Circuits tab.
$(document).on('click','#lnk_cust_ckts',function(){
    // Change the Header text
    document.getElementById('tab-name').innerHTML = "Customer Circuits";
    tableHeight = 470;
    tableName = "custCircuitsTable";
    if (typeof(customerCircuitsTable) == "undefined"){
        setTimeout(function () {
            $(document).ready(function(){
                customerCircuitsTable = $('#customerCircuits-table').dataTable({
                    //"sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/cust-ckts-audit/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "sDom": '<"table-header"ClTRfi>t',
                    "aaSorting": [[ 2, "asc" ], [ 1, "asc"]],
                    "oTableTools": {
                        "sRowSelect": "single",
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#customerCircuits-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#customerCircuits-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "16%"},
                        { "mData": "RowID", "sWidth": "5%" },
                        { "mData": "CustomerCktType", "sWidth": "15%" },
                        { "mData": "Speed", "sWidth": "15%" },
                        { "mData": "CDR","sWidth": "10%" },
                        { "mData": "CircuitID", "sWidth": "15%" },
                        { "mData": "MX960Router", "sWidth": "20%" },
                        { "mData": "RemoteRouter", "sWidth": "25%" },
                        { "mData": "Removed", "sWidth": "5%" },
                        { "mData": "Created", "sWidth": "5%" }
                    ]
                });

                $(document).on( 'click', '#customerCircuits-table td.control', function () {
                    openDetails(customerCircuitsTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    setTimeout(function() { modHighlight(customerCircuitsTable, 9, 10); }, 200);
});

// Activated when a user clicks on the MX Port Assignments BUTTON
// table generation for MX01 Port Assignments
$("#lnk_mx01").click(function() {
    // Change the Header text
    document.getElementById('tab-name').innerHTML = "MX960 Port Assignments";

    // Checks which tab is active to set the resizing parameters
    if ($('#4_1').hasClass('active')){
        tableName = "mx01Table";
        tableHeight = 260;
    }
    else if ($('#4_2').hasClass('active')){
        tableName = "mx02Table";
        tableHeight = 260;
    }

    // generate the table
    if (typeof(mx01PortsTable) == "undefined"){
        setTimeout(function () {
            $(document).ready(function(){
                mx01PortsTable = $('#mx01-table').dataTable({
                    "bAutoWidth": false,
                    "sScrollX": "100%",
                    "sAjaxSource": "/ajax/datatables/mx960-audit/1/view",
                    "sScrollY": "100%",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "sDom": '<"table-header"ClTRfi>t',
                    "aaSorting": [[ 2, "asc" ], [ 1, "asc"]],
                    "oTableTools": {
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#mx01-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#mx01-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "90px"},
                        { "mData": "RowID", "sWidth": "50px" },
                        { "mData": "Interface", "sWidth": "270px" },
                        { "mData": "SubInterface", "sWidth": "90px" },
                        { "mData": "InterfaceTag","sWidth": "350px" },
                        { "mData": "PortType", "sWidth": "100px" },
                        { "mData": "Layer", "sWidth": "30px" },
                        { "mData": "VSNInstance", "sWidth": "90px" },
                        { "mData": "ConnectedDeviceType", "sWidth": "100px" },
                        { "mData": "ConnectedDeviceName", "sWidth": "100px" },
                        { "mData": "ConnectedDevicePort", "sWidth": "100px" },
                        { "mData": "AddressTag","sWidth": "350px" },
                        { "mData": "IPAddress", "sWidth": "120px" },
                        { "mData": "VLANTag", "sWidth": "45px" },
                        { "mData": "SpeedDuplex", "sWidth": "250px" },
                        { "mData": "DescriptionTag","sWidth": "475px" },
                        { "mData": "Description", "sWidth": "1200px" },
                        { "mData": "Removed", "sWidth": "20px" },
                        { "mData": "Created", "sWidth": "20px" }
                    ]
                });
                $(document).on( 'click', '#mx01-table td.control', function () {
                    openDetails(mx01PortsTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    setTimeout(function() { modHighlight(mx01PortsTable, 18, 19); }, 200);
});

// table generation for MX02 Port Assignments
$(document).on('click','.MX02',function(){

    // Checks which tab is active to set the resizing parameters
    if ($('#4_1').hasClass('active')){
        tableName = "mx01Table";
        tableHeight = 260;
    }
    else if ($('#4_2').hasClass('active')){
        tableName = "mx02Table";
        tableHeight = 260;
    }

    if (typeof(mx02PortsTable) == "undefined"){
        setTimeout(function () {
            $(document).ready(function(){
                mx02PortsTable = $('#mx02-table').dataTable({
                    "bAutoWidth": false,
                    "sScrollX": "100%",
                    "sAjaxSource": "/ajax/datatables/mx960-audit/2/view",
                    "sScrollY": "100%",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "sDom": '<"table-header"ClTRfi>t',
                    "aaSorting": [[ 2, "asc" ], [ 1, "asc"]],
                    "oTableTools": {
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#mx02-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#mx02-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "90px"},
                        { "mData": "RowID", "sWidth": "50px" },
                        { "mData": "Interface", "sWidth": "270px" },
                        { "mData": "SubInterface", "sWidth": "90px" },
                        { "mData": "InterfaceTag","sWidth": "350px" },
                        { "mData": "PortType", "sWidth": "100px" },
                        { "mData": "Layer", "sWidth": "30px" },
                        { "mData": "VSNInstance", "sWidth": "90px" },
                        { "mData": "ConnectedDeviceType", "sWidth": "100px" },
                        { "mData": "ConnectedDeviceName", "sWidth": "100px" },
                        { "mData": "ConnectedDevicePort", "sWidth": "100px" },
                        { "mData": "AddressTag","sWidth": "350px" },
                        { "mData": "IPAddress", "sWidth": "120px" },
                        { "mData": "VLANTag", "sWidth": "45px" },
                        { "mData": "SpeedDuplex", "sWidth": "250px" },
                        { "mData": "DescriptionTag","sWidth": "475px" },
                        { "mData": "Description", "sWidth": "1200px" },
                        { "mData": "Removed", "sWidth": "20px" },
                        { "mData": "Created", "sWidth": "20px" }
                    ]
                });
                $(document).on( 'click', '#mx02-table td.control', function () {
                    openDetails(mx02PortsTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    setTimeout(function() { modHighlight(mx02PortsTable, 18, 19); }, 200);
});

// Activates when the NS Port Assignment menu button is clicked.
$("#lnk_ns01").click(function() {
    // Change the Header text
    document.getElementById('tab-name').innerHTML = "NS Port Assignments";

    // Checks which tab is active to set the resizing parameters
    if ($('#5_1').hasClass('active')){
        tableName = "ns01Table";
        tableHeight = 260;
    }
    else if ($('#5_2').hasClass('active')){
        tableName = "ns02Table";
        tableHeight = 320;
    }

    // generate the table
    if (typeof(ns01PortsTable) == "undefined"){
        setTimeout(function () {
            $(document).ready(function(){
                ns01PortsTable = $('#ns01-table').dataTable({
                    "sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/ns-audit/1/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "aaSorting": [[ 2, "asc" ], [ 1, "asc"]],
                    "sDom": '<"table-header"ClTFfiR>t',
                    "oTableTools": {
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#ns01-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#ns01-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "90px"},
                        { "mData": "RowID", "sWidth": "50px" },
                        { "mData": "Interface", "sWidth": "75px" },
                        { "mData": "SubInterface", "sWidth": "90px" },
                        { "mData": "PortType", "sWidth": "70px" },
                        { "mData": "Layer", "sWidth": "50px" },
                        { "mData": "Slot", "sWidth": "50px" },
                        { "mData": "ConnectedDeviceType","sWidth": "100px" },
                        { "mData": "ConnectedDeviceName", "sWidth": "105px" },
                        { "mData": "ConnectedDevicePort", "sWidth": "100px" },
                        { "mData": "AddressTag","sWidth": "250px" },
                        { "mData": "IPAddress", "sWidth": "150px" },
                        { "mData": "VLANTag","sWidth": "75px" },
                        { "mData": "SpeedDuplex", "sWidth": "85px" },
                        { "mData": "Description", "sWidth": "300px" },
                        { "mData": "Removed", "sWidth": "20px" },
                        { "mData": "Created", "sWidth": "20px" }
                    ]
                });
                $(document).on( 'click', '#ns01-table td.control', function () {
                    openDetails(ns01PortsTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    setTimeout(function() { modHighlight(ns01PortsTable, 16, 17); }, 200);
});

// table generation for NS5400 02 Port Assignments
$(document).on('click','.NS02',function(){
    tableHeight = 320;
    tableName = "ns02Table";
    if (typeof(ns02PortsTable) == "undefined"){
        setTimeout(function () {
            $(document).ready(function(){
                ns02PortsTable = $('#ns02-table').dataTable({
                    "sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/ns-audit/2/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "aaSorting": [[ 2, "asc" ], [ 1, "asc"]],
                    "sDom": '<"table-header"ClTRfi>t',
                    "oTableTools": {
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#ns02-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#ns02-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "90px"},
                        { "mData": "RowID", "sWidth": "50px" },
                        { "mData": "Interface", "sWidth": "75px" },
                        { "mData": "SubInterface", "sWidth": "90px" },
                        { "mData": "PortType", "sWidth": "70px" },
                        { "mData": "Layer", "sWidth": "50px" },
                        { "mData": "Slot", "sWidth": "50px" },
                        { "mData": "ConnectedDeviceType","sWidth": "100px" },
                        { "mData": "ConnectedDeviceName", "sWidth": "105px" },
                        { "mData": "ConnectedDevicePort", "sWidth": "100px" },
                        { "mData": "AddressTag","sWidth": "250px" },
                        { "mData": "IPAddress", "sWidth": "150px" },
                        { "mData": "VLANTag","sWidth": "75px" },
                        { "mData": "SpeedDuplex", "sWidth": "85px" },
                        { "mData": "Description", "sWidth": "300px" },
                        { "mData": "Removed", "sWidth": "20px" },
                        { "mData": "Created", "sWidth": "20px" }
                    ]
                });
                $(document).on( 'click', '#ns02-table td.control', function () {
                    openDetails(ns02PortsTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    setTimeout(function() { modHighlight(ns02PortsTable, 16, 17); }, 200);
});

// table generation for the IPSchema table
// This is activated when the user clicks on the IPSchema tab.
$("#lnk_ipschema").click(function() {
    // Change the Header text
    document.getElementById('tab-name').innerHTML = "IP Schema";

    if (typeof(ipSchemaTable) == "undefined"){
        tableHeight = 320;
        tableName = "ipSchema";
        setTimeout(function () {
            $(document).ready(function(){
                ipSchemaTable = $('#ipSchema-table').dataTable({
                    "sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/ipschema-audit/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "aaSorting": [[ 2, "asc" ], [ 1, "asc"]],
                    "sDom": '<"table-header"ClTRfi>t',
                    "oTableTools": {
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#ipSchema-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#ipSchema-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "90px"},
                        { "mData": "RowID", "sWidth": "50px" },
                        { "mData": "NetworkType", "sWidth": "65px" },
                        { "mData": "Name", "sWidth": "300px" },
                        { "mData": "Subnet","sWidth": "125px" },
                        { "mData": "Netmask", "sWidth": "65px" },
                        { "mData": "Purpose","sWidth": "400px" },
                        { "mData": "VLAN", "sWidth": "65px" },
                        { "mData": "Notes","sWidth": "500px"},
                        { "mData": "Removed", "sWidth": "20px" },
                        { "mData": "Created", "sWidth": "20px" }
                    ]
                // }).rowGrouping({"bExpandableGrouping": true, "sGroupingClass": 'group'});
                });
                $(document).on( 'click', '#ipschema-table td.control', function () {
                    openDetails(ipSchemaTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    setTimeout(function() { modHighlight(ipSchemaTable, 10, 11); }, 200);
});

// table generation for the Private Address Subnet table
// This is activated when the user clicks on the Private Address Subnet tab.
$(document).on('click','.privateIP',function(){
    tableName = "privateAddress";
    tableHeight = 260;

    if (typeof(privateIPTable) == "undefined"){
        setTimeout(function () {
            tableName = "privateAddress";
            $(document).ready(function(){
                privateIPTable = $('#private-table').dataTable({
                    "sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/privateip-audit/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "aaSorting": [[ 2, "asc" ], [ 1, "asc"]],
                    "sDom": '<"table-header"ClTFfiR>t',
                    "oTableTools": {
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#private-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#private-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "90px"},
                        { "mData": "RowID", "sWidth": "50px" },
                        { "mData": "VLANName" },
                        { "mData": "VLANID", "sWidth": "50px" },
                        { "mData": "Subnet", "sWidth": "100px" },
                        { "mData": "Netmask", "sWidth": "30px" },
                        { "mData": "NetmaskTag", "sWidth": "100px" },
                        { "mData": "AddressTag","sWidth": "350px" },
                        { "mData": "IPAddress", "sWidth": "100px" },
                        { "mData": "Device", "sWidth": "125px" },
                        { "mData": "DNSName", "sWidth": "125px" },
                        { "mData": "Purpose", "sWidth": "250px" },
                        { "mData": "Removed", "sWidth": "20px" },
                        { "mData": "Created", "sWidth": "20px" }
                    ]
                // }).rowGrouping({"bExpandableGrouping": true, "sGroupingClass": 'group', fnGroupLabelFormat: function(label) { return ""+ label + " " + RowGroupSubnet(label, "private");}});
                });
                $(document).on( 'click', '#private-table td.control', function () {
                    openDetails(privateIPTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    setTimeout(function() { modHighlight(privateIPTable, 13, 14); }, 200);
});


// table generation for the IDN Address Subnet table
// This is activated when the user clicks on the IDN Address Subnet tab.
$(document).on('click','.idnIP',function(){
    tableName = "idnAddress";
    tableHeight = 260;

    if (typeof(idnIPTable) == "undefined"){
        setTimeout(function () {
            tableName = "idnAddress";
            $(document).ready(function(){
                idnIPTable = $('#idn-table').dataTable({
                    "sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/idnip-audit/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "aaSorting": [[ 2, "asc" ], [ 1, "asc"]],
                    "sDom": '<"table-header"ClTFfiR>t',
                    "oTableTools": {
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#idn-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#idn-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "90px"},
                        { "mData": "RowID", "sWidth": "50px" },
                        { "mData": "VLANName" },
                        { "mData": "VLANID", "sWidth": "50px" },
                        { "mData": "Subnet", "sWidth": "100px" },
                        { "mData": "Netmask", "sWidth": "30px" },
                        { "mData": "NetmaskTag", "sWidth": "100px" },
                        { "mData": "AddressTag","sWidth": "350px" },
                        { "mData": "IPAddress", "sWidth": "100px" },
                        { "mData": "Device", "sWidth": "125px" },
                        { "mData": "DNSName", "sWidth": "125px" },
                        { "mData": "Purpose", "sWidth": "250px" },
                        { "mData": "Removed", "sWidth": "20px" },
                        { "mData": "Created", "sWidth": "20px" }
                    ]
                // }).rowGrouping({"bExpandableGrouping": true, "sGroupingClass": 'group', fnGroupLabelFormat: function(label) { return ""+ label + " " + RowGroupSubnet(label, "idn");}});
                });
                $(document).on( 'click', '#idn-table td.control', function () {
                    openDetails(idnIPTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    setTimeout(function() { modHighlight(idnIPTable, 13, 14); }, 200);
});

// table generation for the Public Address Subnet table
// This is activated when the user clicks on the Public Address Subnet BUTTON in the 'Site Data' button dropdown
$("#lnk_subnetinfo").click(function() {
    // Change the Header text
    document.getElementById('tab-name').innerHTML = "Address Subnets";

    // Checks which tab is active to set the resizing parameters
    if ($('#7_1').hasClass('active')){
        tableName = "publicAddress";
    }
    else if ($('#7_2').hasClass('active')){
        tableName = "privateAddress";
    }
    else if ($('#7_3').hasClass('active')){
        tableName = "idnAddress";
    }
    tableHeight = 260;

    if (typeof(publicIPTable) == "undefined"){
        setTimeout(function () {
            tableName = "publicAddress";
            $(document).ready(function(){
                publicIPTable = $('#public-table').dataTable({
                    "sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/pubip-audit/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "aaSorting": [[ 2, "asc" ], [ 1, "asc"]],
                    "sDom": '<"table-header"ClTFfiR>t',
                    "oTableTools": {
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#public-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#public-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "90px"},
                        { "mData": "RowID", "sWidth": "50px" },
                        { "mData": "VLANName" },
                        { "mData": "VLANID", "sWidth": "50px" },
                        { "mData": "Subnet", "sWidth": "100px" },
                        { "mData": "Netmask", "sWidth": "30px" },
                        { "mData": "NetmaskTag", "sWidth": "100px" },
                        { "mData": "AddressTag","sWidth": "350px" },
                        { "mData": "IPAddress", "sWidth": "100px" },
                        { "mData": "Device", "sWidth": "125px" },
                        { "mData": "DNSName", "sWidth": "125px" },
                        { "mData": "Purpose", "sWidth": "250px" },
                        { "mData": "Removed", "sWidth": "20px" },
                        { "mData": "Created", "sWidth": "20px" }
                    ]
                // }).rowGrouping({"bExpandableGrouping": true, "sGroupingClass": 'group', fnGroupLabelFormat: function(label) { return ""+ label + " " + RowGroupSubnet(label, "public");}});
                });
                $(document).on( 'click', '#public-table td.control', function () {
                    openDetails(publicIPTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    setTimeout(function() { modHighlight(publicIPTable, 13, 14); }, 200);
});


// table generation for the DNS Trap Log table
// This is activated when the user clicks on the DNSTrap dropdown button.
$("#lnk_dnstrap").click(function() {
    // Change the Header text
    document.getElementById('tab-name').innerHTML = "DNS / NTP / Logging";

    if (typeof(dnsTrapTable) == "undefined"){
        tableHeight = 320;
        tableName = "dnsTrap";
        setTimeout(function () {
            $(document).ready(function(){
                dnsTrapTable = $('#dnsTrap-table').dataTable({
                    "sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/dns-trap-audit/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "aaSorting": [[ 2, "asc" ], [ 1, "asc"]],
                    "sDom": '<"table-header"ClTFfiR>t',
                    "oTableTools": {
                        "aButtons": [
                            {"sExtends":"collection", "sButtonText": "Export", "aButtons": [
                                {"sExtends": "copy", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "csv", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "pdf", "sButtonClass": "ui-button dd-button"},
                                {"sExtends": "print", "sButtonClass": "ui-button dd-button"}
                            ]},
                            {"sExtends": "text", "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#dnsTrap-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) { $( this ).click(); }
                                    });
                                }
                            },
                            {"sExtends": "text", "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#dnsTrap-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) { $( this ).click(); }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "16%"},
                        { "mData": "RowID", "sWidth": "5%" },
                        { "mData": "Function", "sWidth": "100px" },
                        { "mData": "Tag","sWidth": "300px" },
                        { "mData": "HostIP", "sWidth": "200px" },
                        { "mData": "Reference","sWidth": "200px" },
                        { "mData": "Name", "sWidth": "350px" },
                        { "mData": "Removed", "sWidth": "5%" },
                        { "mData": "Created", "sWidth": "5%" }
                    ]
                });

                $(document).on( 'click', '#dnsTrap-table td.control', function () {
                    openDetails(dnsTrapTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    setTimeout(function() { modHighlight(dnsTrapTable, 8, 9); }, 200);
});


// table generation for the SubnetADV table
$("#lnk_subnetadv").click(function() {
    // Change the Header text
    document.getElementById('tab-name').innerHTML = "Subnet Advertising";

    // generate the table
    tableHeight = 230;
    tableName = "subnetAdvTable";
    if (typeof(subnetAdvTable) == "undefined"){
        setTimeout(function () {
            $(document).ready(function(){
                subnetAdvTable = $('#subnetadv-table').dataTable({
                    "sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/subnet-adv-audit/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    // "bSort": false,
                    "aaSorting": [[ 2, "asc" ], [ 1, "asc"]],
                    "sDom": '<"table-header"ClTFfiR>t',
                    "oTableTools": {
                        "aButtons": [
                            {"sExtends": "copy"},
                            {"sExtends": "csv"},
                            {"sExtends": "pdf"},
                            {"sExtends": "print", "sInfo": "<h2>Print view</h2><p>Please use your browser's print function to print this table. Press escape when finished."},
                            {
                                "sExtends": "text",
                                "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#subnetadv-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) {
                                            $( this ).click();
                                        }
                                    });
                                }
                            }, {
                                "sExtends": "text",
                                "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#subnetadv-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) {
                                            $( this ).click();
                                        }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "90px"},
                        { "mData": "RowID", "sWidth": "50px" },
                        { "mData": "SubnetName", "sWidth": "300px" },
                        { "mData": "Network","sWidth": "300px" },
                        { "mData": "PublicIPAdv", "sWidth": "350px" },
                        { "mData": "Removed", "sWidth": "50px" },
                        { "mData": "Created", "sWidth": "50px" }
                    ]
                });

                $(document).on( 'click', '#subnetadv-table td.control', function () {
                    openDetails(subnetAdvTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    setTimeout(function() { modHighlight(subnetAdvTable, 6, 7); }, 200);
});



// table generation for the SubnetADV table
$("#lnk_cardslotting").click(function() {
    // Change the Header text
    document.getElementById('tab-name').innerHTML = "Card Slotting";


    // generate the table
    tableHeight = 230;
    tableName = "cardSlottingTable";
    if (typeof(cardSlottingTable) == "undefined"){

        setTimeout(function () {
            $(document).ready(function(){

                cardSlottingTable = $('#cardslotting-table').dataTable({
                    "sScrollX": "100%",
                    "sScrollY": "100%",
                    "sAjaxSource": "/ajax/datatables/cardslotting-audit/view",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    // "bSort": false,
                    "aaSorting": [[ 2, "asc" ], [ 1, "asc"]],
                    "sDom": '<"table-header"ClTFfiR>t',
                    "oTableTools": {
                        "aButtons": [
                            {"sExtends": "copy"},
                            {"sExtends": "csv"},
                            {"sExtends": "pdf"},
                            {"sExtends": "print", "sInfo": "<h2>Print view</h2><p>Please use your browser's print function to print this table. Press escape when finished."},
                            {
                                "sExtends": "text",
                                "sButtonText": "Expand All",
                                "fnClick": function ( button, config ) {
                                    $('#cardslotting-table td.control').each( function() {
                                        if (($.inArray( this.parentNode, anOpen )) === -1) {
                                            $( this ).click();
                                        }
                                    });
                                }
                            }, {
                                "sExtends": "text",
                                "sButtonText": "Collapse All",
                                "fnClick": function ( button, config ) {
                                    $('#cardslotting-table td.control').each( function() {
                                        if (!(($.inArray( this.parentNode, anOpen )) === -1)) {
                                            $( this ).click();
                                        }
                                    });
                                }
                            }
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "20px" },
                        { "mData": "UpdatedTime", "sWidth": "90px"},
                        { "mData": "RowID", "sWidth": "50px" },
                        { "mData": "Slot", "sWidth": "300px" },
                        { "mData": "CardType","sWidth": "300px" },
                        { "mData": "InterfaceNum", "sWidth": "350px" },
                        { "mData": "Device", "sWidth": "300px" },
                        { "mData": "MIC", "sWidth": "300px" },
                        { "mData": "Removed", "sWidth": "50px" },
                        { "mData": "Created", "sWidth": "50px" }
                    ]
                });


                $(document).on( 'click', '#cardslotting-table td.control', function () {
                    openDetails(cardSlottingTable, this.parentNode, $('img', this));
                });
            });
        }, 10);
    }
    setTimeout(function() { modHighlight(cardSlottingTable, 8, 9); }, 200);
});


// This function is called upon resizing the window, to resize the table height.
$window.resize(function() {
    // first we find the currently viewed table so we can call the function to resize that table.
    if (tableName == "asyncTable") {
        // we first pull the settings from currently displayed table.
        var oSettings = asyncTable.fnSettings();

        // then we calculate the height of the resized table.
        oSettings.oScroll.sY = calcDataTableHeight();

        // lastly we redraw the table with the newly calculated height.
        asyncTable.fnDraw(false);
    }
    else if (tableName == "tagsTable") {

        if (tagsTable.fnSettings() != 'undefined' ){
            var oSettings = tagsTable.fnSettings();
        }

        oSettings.oScroll.sY = calcDataTableHeight();
        tagsTable.fnDraw(false);
    }
    else if (tableName == "infoTable") {
        var oSettings = infoTable.fnSettings();
        oSettings.oScroll.sY = calcDataTableHeight();
        infoTable.fnDraw(false);
    }
    else if (tableName == "customerTable") {
        var oSettings = customerTable.fnSettings();
        oSettings.oScroll.sY = calcDataTableHeight();
        customerTable.fnDraw(false);
    }
    else if (tableName == "netConnectionsTable") {
        var oSettings = netConnectionsTable.fnSettings();
        oSettings.oScroll.sY = calcDataTableHeight();
        netConnectionsTable.fnDraw(false);
    }
    else if (tableName == "custCircuitsTable") {
        var oSettings = customerCircuitsTable.fnSettings();
        oSettings.oScroll.sY = calcDataTableHeight();
        customerCircuitsTable.fnDraw(false);
    }
    else if (tableName == "mx01Table") {
        var oSettings = mx01PortsTable.fnSettings();
        oSettings.oScroll.sY = calcDataTableHeight();
        mx01PortsTable.fnDraw(false);
    }
    else if (tableName == "mx02Table") {
        var oSettings = mx02PortsTable.fnSettings();
        oSettings.oScroll.sY = calcDataTableHeight();
        mx02PortsTable.fnDraw(false);
    }
    else if (tableName == "ns01Table") {
        var oSettings = ns01PortsTable.fnSettings();
        oSettings.oScroll.sY = calcDataTableHeight();
        ns01PortsTable.fnDraw(false);
    }
    else if (tableName == "ipSchema") {
        var oSettings = ipSchemaTable.fnSettings();
        oSettings.oScroll.sY = calcDataTableHeight();
        ipSchemaTable.fnDraw(false);
    }
    else if (tableName == "publicAddress") {
        var oSettings = publicIPTable.fnSettings();
        oSettings.oScroll.sY = calcDataTableHeight();
        publicIPTable.fnDraw(false);
    }
    else if (tableName == "privateAddress") {
        var oSettings = publicIPTable.fnSettings();
        oSettings.oScroll.sY = calcDataTableHeight();
        publicIPTable.fnDraw(false);
    }
    else if (tableName == "idnAddress") {
        var oSettings = publicIPTable.fnSettings();
        oSettings.oScroll.sY = calcDataTableHeight();
        publicIPTable.fnDraw(false);
    }
    else if (tableName == "dnsTrap") {
        var oSettings = dnsTrapTable.fnSettings();
        oSettings.oScroll.sY = calcDataTableHeight();
        dnsTrapTable.fnDraw(false);
    }
    else if (tableName == "siteInfo") {
        var oSettings = siteInfoTable.fnSettings();
        oSettings.oScroll.sY = calcDataTableHeight();
        siteInfoTable.fnDraw(false);
    }
    else if (tableName == "subnetAdvTable") {
        var oSettings = subnetAdvTable.fnSettings();
        oSettings.oScroll.sY = calcDataTableHeight();
        subnetAdvTable.fnDraw(false);
    } else if (tableName == "cardSlottingTable") {
        var oSettings = cardSlottingTable.fnSettings();
        oSettings.oScroll.sY = calcDataTableHeight();
        cardSlottingTable.fnDraw(false);
    } 




});


}); // document.ready
