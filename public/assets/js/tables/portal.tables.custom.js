
// Our custom field type for pulling AJAX server responses for suggestions from the master tags table
// This should be used for any locations where a tag is used, to ensure integrity of the tags
// being entered into the database. 
$.fn.dataTable.Editor.fieldTypes.tagField = $.extend( true, {}, $.fn.dataTable.Editor.models.baseFieldType, {
    "create": function ( conf ) {
        // Build the actual input field
        conf._input = $('<input/>').attr( $.extend( {
            // The id of the element
            id: conf.id,
            // the type attribute
            type: 'text',
            // the class attribute
            class: 'tag-field'
            // This ensures that conf.attr exists, or else sets it to {}
        }, conf.attr || {} ) );

        // setup jquery autocomplete on the tag-field,
        // this is what actually makes the call to get the suggestions
        conf._input.autocomplete({
            source: function(request, response) {
                $.getJSON("/ajax/master-tags/" + request.term, function (data) {
                    var suggestions = [];
                    // loop for up to 10 suggestions 
                    for (i=0;i<20;i++) {
                        // must check if it's undefined to ensure we are error free for <20 results
                        if (data[i] !== undefined) {
                            suggestions.push({"value":data[i].Tag});
                        }
                    }
                    response(suggestions);
                });
            },
            // don't start autocompleting until three characters are in the field.
            minLength: 3
        });
        return conf._input[0];
    },
    // Build the other standard functions, pulled from the DT Editor core file. 
    "get": function ( conf ) {
        return conf._input.val();
    },
    "set": function ( conf, val ) {
        conf._input.val( val ).trigger( 'change' );
    },
    "enable": function ( conf ) {
        conf._input.prop( 'disabled', false );
    },
    "disable": function ( conf ) {
        conf._input.prop( 'disabled', true );
    }
});

