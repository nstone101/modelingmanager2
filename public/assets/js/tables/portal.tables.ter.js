/**
 * User: nprintz
 * Date: 8/16/13
 * Time: 10:06 AM
 *
 * This file is used for the TER page. this file's format is very similar to jma.tables.js. The reason for
 * this file to be separate is to eliminate excess code in the file and jma.tables.js since they are needed for
 * separate pages.
 */

// defines the variables for the window height, the tablename for window resizing,
// and defaults the tableHeight to 0
var $window = $(window);
var tableName = undefined;
var tableHeight = 0;

//define the table and editor variables
var terTable;
var terEditor;

/**
 * Function definitions
 */
// This function is called upon resizing the window, to resize the table height.
$window.resize(function() {
    // first we find the currently viewed table so we can call the function to resize that table.
    if (tableName == "terTable") {
        // we first pull the settings from currently displayed table.
        var oSettings = terTable.fnSettings();

        // then we calculate the height of the resized table.
        oSettings.oScroll.sY = calcDataTableHeight();

        // lastly we redraw the table with the newly calculated height.
        terTable.fnDraw(false);
    }
});


/* Formating function for row details */
//function fnFormatDetails ( oTable, nTr )
//{
//    var aData = oTable.fnGetData( nTr );
//    var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
//    sOut += '<tr><td>Rendering engine:</td><td>'+aData[1]+' '+aData[4]+'</td></tr>';
//    sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
//    sOut += '<tr><td>Extra info:</td><td>And any further details here (images etc)</td></tr>';
//    sOut += '</table>';
//
//    return sOut;
//}


// defines the function for calculating the height of the table without the header space
var calcDataTableHeight = function() {
    return Math.round($window.height() - tableHeight);
};
/**
 * End of Function Definitions
 */

/**
 * Start of TER Table generation
 */
// table generation for the TER Table editor
$(document.getElementById('ter-table')).ready(function() {
    tableHeight = 250;
    tableName = "terTable";
    if (typeof(terTable) == "undefined"){
        setTimeout(function () {
            $(document).ready(function(){
                terEditor = new $.fn.dataTable.Editor( {
                    "domTable": "#ter-table",
                    "ajaxUrl": "/ajax/datatables/ter",
                    "fields" : [
                        { // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                            "label": "SIT:",
                            "name": "SIT",
                            "type": "select",
                            "ipOpts": [
                                { "label": "Select a SIT" },
                                { "label": "SIT 3", "value": "SIT 3" },
                                { "label": "SIT 4", "value": "SIT 4" },
                                { "label": "SIT 5", "value": "SIT 5" },
                                { "label": "SIT 6", "value": "SIT 6" },
                                { "label": "SIT 7", "value": "SIT 7" },
                                { "label": "SIT 8", "value": "SIT 8" },
                                { "label": "SIT 9", "value": "SIT 9" }
                            ]
                        },{
                            "label": "VSN:",
                            "name": "VSN",
                            "type": "select",
                            "ipOpts": [
                                { "label": "Select a VSN"},
                                { "label": "Base", "value": "Base" },
                                { "label": "BROADSOFT", "value": "BROADSOFT" },
                                { "label": "DSCIP", "value": "DSCIP" },
                                { "label": "C20G9 Genband", "value": "DSCIP" },
                                { "label": "IPAC", "value": "IPAC" },
                                { "label": "IP-IVR", "value": "IP-IVR" },
                                { "label": "ISCIP", "value": "ISCIP" },
                                { "label": "NSRS", "value": "NSRS" },
                                { "label": "OCCAS", "value": "OCCAS" },
                                { "label": "PIP NNI", "value": "PIP NNI" },
                                { "label": "SIDA", "value": "SIDA" }
                            ]
                        },
                        { "label": "Document ID:", "name": "DocID" },
                        { "label": "Requirements ID:", "name": "RqmtsID" },
                        { "label": "TPL #:", "name": "TPLNumber" },
                        { "label": "Requirements Description:", "name": "RqmtsDesc" },
                        {
                            "label": "Result:",
                            "name": "Result",
                            "type": "select",
                            "ipOpts": [
                                { "label": "Select a Result" },
                                { "label": "Pass", "value": "Pass" },
                                { "label": "Fail", "value": "Fail" },
                                { "label": "In Progress", "value": "In Progress" },
                                { "label": "On Hold", "value": "On Hold" },
                                { "label": "Duplicate", "value": "Duplicate" }
                            ]
                        },
                        { "label": "Testers:", "name": "Testers" },
                        { "label": "Test Date:", "name": "TestDate", "type": "date", "dateFormat": "yy-mm-dd" },
                        { "label": "Test Description:", "name": "Description", "type": "textarea" },
                        { "label": "Results Description:", "name": "ResultDesc", "type": "textarea" },
                        { "label": "Last Updated", "name": "UpdatedTime" },
                        { "label": "Last Edited By", "name": "EditedBy" },
                        { "label": "Comment", "name": "Comment", "type": "textarea" }
                    ],
                    "events": {
                        "onPreSubmit": function ( o ) {
                            if ( o.data.SIT === "Select a SIT" ) {
                                this.error('SIT', 'A SIT must be chosen');
                                return false;
                            }
                            else if ( o.data.VSN === "Select a VSN" ) {
                                this.error('VSN', 'A VSN must be chosen');
                                return false;
                            }
                            else if ( o.data.DocID === "" ) {
                                this.error('DocID', 'A Document ID must be specified');
                                return false;
                            }
                            else if ( o.data.ReqID === "" ) {
                                this.error('RqmtsID', 'A Requirements ID must be specified');
                                return false;
                            }
                            else if ( o.data.Result === "Select a Result" ) {
                                this.error('Result', 'A Result must be chosen');
                                return false;
                            }
                        }
                    }
                });

                terEditor.on('onInitEdit', function () {
                    this.show('UpdatedTime');
                    this.show('EditedBy');
                    this.show('Comment');
                    this.disable('UpdatedTime');
                    this.disable('EditedBy');
                });

                terEditor.on('onInitCreate', function () {
                    this.hide('UpdatedTime');
                    this.hide('EditedBy');
                    this.hide('Comment');
                });

                terTable = $('#ter-table').dataTable({
//                    "sScrollX": "100%",
                    "sScrollY": calcDataTableHeight(),
                    "sAjaxSource": "/ajax/datatables/ter",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "sDom": '<"table-header"ClTfiR>t',
                    "oTableTools": {
                        "sRowSelect": "single",
                        "aButtons": [
                            {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":terEditor},
                            {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":terEditor},
                            {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":terEditor},
                            {"sExtends": "copy"},
                            {"sExtends": "csv"},
                            {"sExtends": "pdf"},
                            {"sExtends": "print", "sInfo": "<h2>Print view</h2><p>Please use your browser's print function to print this table. Press escape when finished."}
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
//                    "aaSorting": [[1, 'asc']],
                    "aoColumns": [
//                        { "bSortable": false, "aTargets": [ 0 ]},
                        { "mData": "SIT" , "bVisible": false},
                        { "mData": "VSN", "bVisible": false},
                        { "mData": "DocID", "sWidth": "125px" },
                        { "mData": "RqmtsID", "sWidth": "125px" },
                        { "mData": "TPLNumber", "sWidth": "125px" },
                        {

                            "mData": "RqmtsDesc",
                            "sWidth": "200px"
                        },
                        { "mData": "Result", "sWidth": "75px" },
                        { "mData": "Testers", "sWidth": "100px" },
                        { "mData": "TestDate", "sWidth": "65px" },
                        {

                            "mData": "Description",
                            "sWidth": "300px"
                        },{

                            "mData": "ResultDesc",
                            "sWidth": "300px"
                        }
                    ]
                });

//                /**
//                 * Insert a 'details' column to the table, for the row details
//                 */
//                var nCloneTh = document.createElement( 'th' );
//                var nCloneTd = document.createElement( 'td' );
//                nCloneTd.innerHTML = '<img src="/assets/images/details_open.png">';
//                nCloneTd.className = "center";
//
//                $('#ter-table thead tr').each( function () {
//                    this.insertBefore( nCloneTh, this.childNodes[0] );
//                } );
//
//                $('#ter-table tbody tr').each( function () {
//                    this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
//                } );
//
//                /** Add event listener for opening and closing details
//                 * Note that the indicator for showing which row is open is not controlled by DataTables,
//                 * rather it is done here
//                 */
//                $('#ter-table tbody td img').live('click', function () {
//                    var nTr = this.parentNode.parentNode;
//                    if ( this.src.match('details_close') ) {
//                        /* This row is already open - close it */
//                        this.src = "/assets/images/details_open.png";
//                        terTable.fnClose( nTr );
//                    } else {
//                        /* Open this row */
//                        this.src = "/assets/images/details_close.png";
//                        terTable.fnOpen( nTr, fnFormatDetails(terTable, nTr), 'details' );
//                    }
//                } );
            });
        }, 10);
    }
});