/**
 * User: nprintz
 * Date: 3/10/14
 *
 * This file is used for the Site Manager page. this file's format is very similar to jma.tables.js. The reason for
 * this file to be separate is to eliminate excess code in the file and jma.tables.js since they are needed for
 * separate pages.
 */

// defines the variables for the window height, the tablename for window resizing,
// and defaults the tableHeight to 0
var $window = $(window);
var tableName = undefined;
var tableHeight = 0;

//define the table and editor variables
var statusTable;
var statusEditor;

var today = new Date();


/**
 * Function definitions
 */
// This function is called upon resizing the window, to resize the table height.
$window.resize(function() {
    // first we find the currently viewed table so we can call the function to resize that table.
    if (tableName == "statusTable") {
        // we first pull the settings from currently displayed table.
        var oSettings = statusTable.fnSettings();

        // then we calculate the height of the resized table.
        oSettings.oScroll.sY = calcDataTableHeight();

        // lastly we redraw the table with the newly calculated height.
        statusTable.fnDraw(false);
    }
});

function statusHighlight() {
	// This is a variable containing all the rows in the datatable.
	var oRow = $('#status-table').dataTable().$('tr');
	// We loop through each row comparing the expiration date to today
	for (var i = 0; i < oRow.length; i++) {
		var row = oRow[i].getElementsByTagName('td');

		// ExpDate[5] is the 6th column, the expiration date, which we split on the hyphen and turn into a date object.
		var dueDate = row[6].innerHTML.split('-');
		dueDate = new Date(dueDate[0], dueDate[1]-1, dueDate[2]);
		// if the date is prior to today, we change the background color.
		if ((dueDate.getTime() < today.getTime()) && ((row[4].innerHTML != "Complete") && (row[4].innerHTML != 'N/A'))) {
			$(row[6]).css({'backgroundColor': '#F8B2B2'});
		} else {
			$(row[6]).css({'backgroundColor': ''});
		}
		if (row[4].innerHTML === "Delayed") {
			$(row[4]).css({'backgroundColor': '#F8B2B2'});
		}
		else if (row[4].innerHTML === "Complete") {
			$(row[4]).css({'backgroundColor': '#99FF99'});
		}
		else if (row[4].innerHTML === "In Progress") {
			$(row[4]).css({'backgroundColor': '#FFFF99'});
		} else {
			$(row[4]).css({'backgroundColor': ''});
		}
	}
}

// defines the function for calculating the height of the table without the header space
var calcDataTableHeight = function() {
    return Math.round($window.height() - tableHeight);
};
/**
 * End of Function Definitions
 */

/**
 * Start of Site Status Table generation
 */
// table generation for the Site Status Table editor
$(document.getElementById('status-table')).ready(function() {
    tableHeight = 250;
    tableName = "statusTable";
    if (typeof(statusTable) == "undefined"){
        setTimeout(function () {
            $(document).ready(function(){
				var anOpen = [];  // Array for tracking the table rows for opening and closing.
                statusEditor = new $.fn.dataTable.Editor( {
                    "domTable": "#status-table",
                    "ajaxUrl": "/ajax/datatables/sitestatus",
                    "fields" : [
                        { // Label is label in form, name is how to handle the field in code, and type is type of input / display of field
                            "label": "Site Name:", "name": "SiteName"
                        },{
                            "label": "VSN:",
                            "name": "VSN",
                            "type": "select",
                            "ipOpts": [
                                { "label": "Select a VSN" },
                                { "label": "Initial Build", "value": "Initial Build" },
                                { "label": "BASE", "value": "BASE" },
                                { "label": "BROADSOFT", "value": "BROADSOFT" },
                                { "label": "DSCIP", "value": "DSCIP" },
                                { "label": "C20G9 Genband", "value": "DSCIP" },
                                { "label": "IPAC", "value": "IPAC" },
                                { "label": "IP-IVR", "value": "IP-IVR" },
                                { "label": "ISCIP", "value": "ISCIP" },
                                { "label": "NSRS", "value": "NSRS" },
                                { "label": "OCCAS", "value": "OCCAS" },
                                { "label": "PIP NNI", "value": "PIP NNI" },
                                { "label": "SIDA", "value": "SIDA" }
                            ]
                        },
                        { "label": "Phase:", "name": "Phase", "type": "select",
                            "ipOpts": [
                                { "label": "Select a Phase" },
                                { "label": "Phase 1 - Staging", "value": "Phase 1 - Staging" },
                                { "label": "Phase 2 - Spreadsheet", "value": "Phase 2 - Spreadsheet" },
                                { "label": "Phase 3 - Site Build", "value": "Phase 3 - Site Build" },
                                { "label": "Phase 4 - Pre-Verifications", "value": "Phase 4 - Pre-Verifications" },
                                { "label": "Phase 5 - Call Through Testing", "value": "Phase 5 - Call Through Testing" }
                            ]
                        },
                        { "label": "Task Summary:", "name": "Task" },
                        { "label": "Task Detail:", "name": "Description", "type": "textarea" },
                        {
							"label": "Assigned to:",
							"name": "AssignedTo",
							"type": "select",
							"ipOpts": [
								{ "label": 'Select an Engineer' },
								{ "label": 'N/A', 'value': 'N/A' },
								{ "label": 'Amanda Glenn', 'value': 'aglenn' },
								{ "label": 'Chris Wallace', 'value': 'cwallace' },
								{ "label": 'Derek Rhoades', 'value': 'drhoades' },
								{ "label": 'Eugene Williams', 'value': 'ewilliams' },
								{ "label": 'Gopi Balakrishnan', 'value': 'bgopi' },
                                { "label": 'Kelly Henneous', 'value': 'khenneous' },
								{ "label": 'Krissy Williams', 'value': 'kwilliams' },
								{ "label": 'Nathan Printz', 'value': 'nprintz' },
								{ "label": 'Pat Waters', 'value': 'pwaters' },
								{ "label": 'Pete Symmonds', 'value': 'psymmonds' },
								{ "label": 'Quan Nguyen', 'value': 'qnguyen' },
								{ "label": 'Tarek Haque', 'value': 'mhaque' }
							]
						},{
                            "label": "Status:",
                            "name": "Status",
                            "type": "select",
                            "ipOpts": [
                                { "label": "Select a Status" },
                                { "label": "Not Started", "value": "Not Started" },
                                { "label": "N/A", "value": "N/A" },
                                { "label": "Delayed", "value": "Delayed" },
                                { "label": "In Progress", "value": "In Progress" },
                                { "label": "Complete", "value": "Complete" }
                            ]
                        },{
                            "label": "Progress Amount:",
                            "name": "Progress",
                            "type": "select",
                            "ipOpts": [
                                { "label": "" },
                                { "label": "10%", "value": "10%" },
                                { "label": "20%", "value": "20%" },
                                { "label": "30%", "value": "30%" },
                                { "label": "40%", "value": "40%" },
                                { "label": "50%", "value": "50%" },
                                { "label": "60%", "value": "60%" },
                                { "label": "70%", "value": "70%" },
                                { "label": "80%", "value": "80%" },
                                { "label": "90%", "value": "90%" },
                                { "label": "100%", "value": "100%" }
                            ]
                        },
                        { "label": "Reason for Delay:", "name": "DelayReason", "type": "textarea" },
						{ "label": "Comment", "name": "Comment", "type": "textarea" },
                        { "label": "Date Due:", "name": "DateDue", "type": "date", "dateFormat": "yy-mm-dd" },
                        { "label": "Date Completed:", "name": "DateCompleted", "type": "date", "dateFormat": "yy-mm-dd" },
						{ "label": "Link URL:", "name": "LinkURL" },
                        { "label": "Last Updated", "name": "UpdatedTime" },
                        { "label": "Last Edited By", "name": "EditedBy" }
                    ],
                    "events": {
                        "onPreSubmit": function ( o ) {
                            if ( o.data.SiteName === "" ) {
                                this.error('SiteName', 'A Site Name must be specified');
                                return false;
                            }
                            else if ( o.data.VSN === "Select a VSN" ) {
                                this.error('VSN', 'A VSN must be chosen');
                                return false;
                            }
                            else if ( o.data.Phase === "Select a Phase" ) {
                                this.error('Phase', 'A Phase must be specified');
                                return false;
                            }
                            else if ( o.data.Task === "" ) {
                                this.error('Task', 'A Task Summary must be specified');
                                return false;
                            }
                            else if ( o.data.Status === "Select a Status" ) {
                                this.error('Result', 'A Status must be chosen');
                                return false;
                            }
							else if ((o.data.Status === "In Progress") && (o.data.Progress === "")) {
								this.error('Progress', 'A Progress Amount must be chosen if task is In Progress.');
								return false;
							}
							else if ((o.data.Status === "Delayed") && (o.data.Progress === "")) {
								this.error('Progress', 'A Progress Amount must be chosen if task is Delayed.');
								return false;
							}
                            else if ( o.data.Status === "Delayed" && o.data.DelayReason === "" ) {
                                this.error('DelayReason', 'A Reason must be stated if delayed.');
                                return false;
                            }
                            else if (( o.data.Status === "Complete" ) && (o.data.DateCompleted === "2014-01-01" )) {
                                this.error('DateCompleted', 'Date Completed cannot be the default date.');
                                return false;
                            }
							else if (( o.data.Status === "Complete" ) && (o.data.Progress != "100%" )) {
                                this.error('Progress', 'Progress must be 100% if completed.');
                                return false;
                            }
                            else if ((o.data.AssignedTo === "") && ((o.data.Status != "Not Started") && (o.data.Status != "N/A"))) {
                                this.error('AssignedTo', 'Task needs to be assigned to someone.');
                                return false;
                            }
							else {
								return true
							}
                        },
						"onPostSubmit": function ( o ) {
							// Send an email to the user if: They aren't assigning themselves a task, the task isn't completed, the task isn't N/A, and the user isn't blank or N/A.
							if ((o.row.Status != "Complete") && (o.row.Status != "N/A") && (o.row.AssignedTo != username) && (o.row.AssignedTo != 'N/A') && (o.row.AssignedTo != 'Select an Engineer')) {
								var xmlHTTP = new XMLHttpRequest();
								xmlHTTP.open("GET", '/php/site_tools/site_track_email.php?a='+ o.row.AssignedTo +'&s='+ o.row.SiteName +'&d='+ o.row.DateDue +'&t='+ o.row.Task +'&u='+ username,true);
								xmlHTTP.send();
							}
						},
						"onSubmitComplete": function ( o ) {
							statusHighlight();
						}
                    }
                });


                statusEditor.on('onInitEdit', function () {
                    this.show('UpdatedTime');
                    this.show('EditedBy');
                    this.show('Comment');
                    this.disable('VSN');
                    this.disable('SiteName');
                    this.disable('UpdatedTime');
                    this.disable('EditedBy');
				});

                statusEditor.on('onInitCreate', function () {
					this.set ( 'SiteName', siteName);
					this.enable( 'VSN');
                    this.hide('UpdatedTime');
                    this.hide('EditedBy');
                });

                statusTable = $('#status-table').dataTable({
                    "sScrollY": calcDataTableHeight(),
                    "sAjaxSource": "/ajax/datatables/sitestatus",
                    "bScrollCollapse": true,
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "sDom": '<"table-header"ClTfiR>t',
                    "oTableTools": {
                        "sRowSelect": "single",
                        "aButtons": [
                            {"sExtends":"editor_create", "sButtonClass":"DTTT_button_create", "editor":statusEditor},
                            {"sExtends":"editor_edit", "sButtonClass":"DTTT_button_edit", "editor":statusEditor},
                            {"sExtends":"editor_remove", "sButtonClass":"DTTT_button_delete", "editor":statusEditor},
                            {"sExtends": "copy"},
                            {"sExtends": "csv"},
                            {"sExtends": "pdf"},
                            {"sExtends": "print", "sInfo": "<h2>Print view</h2><p>Please use your browser's print function to print this table. Press escape when finished."}
                        ],
                        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
						{ "mData": null, "sClass": "control center", "sDefaultContent": '<img src="/assets/images/details_open.png">', "sWidth": "25px" },
                        { "mData": "VSN", "sWidth": '75px' },
                        { "mData": "Phase" },
                        { "mData": "Task", "sWidth": "200px" },
                        {

                            "mData": "AssignedTo",
                            "sWidth": "75px"
                        },
                        { "mData": "Status", "sWidth": "75px" },
                        { "mData": "Progress", "sWidth": "40px" },
                        {

                            "mData": "DateDue",
                            "sWidth": "75px"
                        },{
                            "mRender": function ( data, type, row ) {
								if (data != null) {
									return data;
								} else {
									return data;
								}
							},
                            "mData": "DateCompleted",
                            "sWidth": "75px"
                        }
                    ]
                }).rowGrouping({"bExpandableGrouping": true, "sGroupingClass": 'group', "iGroupingColumnIndex": 2 });

				// This function is used to open or close a details row when the image is clicked on.
				$('#status-table td.control').click( function () {
					var nTr = this.parentNode;
					var i = $.inArray( nTr, anOpen );

					if ( i === -1 ) {
						$('img', this).attr( 'src', "/assets/images/details_close.png" );
						var nDetailsRow = statusTable.fnOpen( nTr, fnFormatDetails(statusTable, nTr), 'details' );
						$('div.innerDetails', nDetailsRow).slideDown();
						anOpen.push( nTr );
					}
					else {
						$('img', this).attr( 'src', "/assets/images/details_open.png" );
						$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
							statusTable.fnClose( nTr );
							anOpen.splice( i, 1 );
						} );
					}
				} );

				// This builds the format for the details row.
				function fnFormatDetails( oTable, nTr )
				{
					var oData = oTable.fnGetData( nTr );
					var sOut =
						'<div class="innerDetails">'+
							'<table class="table table-bordered">'+
							'<tr><td style="width: 125px;">Task Description:</td><td>'+oData.Description+'</td></tr>'+
							'<tr><td style="width: 125px;">Delayed Reason:</td><td>'+oData.DelayReason+'</td></tr>'+
							'<tr><td style="width: 125px;">Task Comment:</td><td>'+oData.Comment+'</td></tr>'+
							'<tr><td style="width: 125px;">Link URL:</td><td><a href="'+oData.LinkURL+'">'+oData.LinkURL+'</a></td></tr>'+
							'</table>'+
							'</div>';
					return sOut;
				}
            });
        }, 10);
    }
    // This function waits 1.5 seconds after clicking on auth devices tab, then finds all the expired devices and
	// changes their backgrounds to red.
	setTimeout(function() {
		statusHighlight();
	}, 400);
});