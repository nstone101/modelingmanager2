<?php
/*
 *      Lets define things we will need for the app
 */
define('APP_SHORTNAME','VNDP');
define('APP_NAME','VoIP Data Management Portal');
define('APP_VERSION', 'v0.1');

define('SMTP_HOST', 'smtp.outlook.com');            // SMTP server
define('SMTP_AUTH', true);                          // enable SMTP authentication
define('SMTP_USERNAME', 'noreply@jmait.com');       // SMTP username
define('SMTP_PASSWORD', 'Jm@!t123##');              // SMTP password
define('SMTP_ENCRYPTION', 'tls');                   // 'tls' or 'ssl' accepted
define('SMTP_FROM', 'noreply@jmait.com');           // SMTP from address

define('VZP_LOCAL_MACHINE', gethostname());
define('VZP_DIR', $_SERVER['DOCUMENT_ROOT']);
define('VZP_LIB', VZP_DIR . '/api/lib');
define('VZP_FILES', VZP_DIR . '/files');
define('VZP_LOG', VZP_DIR . '/api/log');
define('VZP_TMP', sys_get_temp_dir());
define('VZP_ASSETS', VZP_DIR.'/assets');
define('VZP_TEMPLATES', VZP_ASSETS.'/templates');

switch (VZP_LOCAL_MACHINE) {
    case "portal-web-01":
        define('DB_USER',"root");
        define('DB_PASS',"Juniper1234");
        define('DB_HOST',"10.0.0.141");  // db node 01
        define('DB_NAME',"VZDB");
        define('DB_NAME_AUDIT',"portal_audit");
        break;
    case "portal-web-02":
        define('DB_USER',"root");
        define('DB_PASS',"Juniper1234");
        define('DB_HOST',"10.0.0.142");  // db node 02
        define('DB_NAME',"VZDB");
        define('DB_NAME_AUDIT',"portal_audit");
        break;
    case 'BRADPOWELLAC19':
        date_default_timezone_set('America/Chicago');
        define('DB_USER',"root");
        define('DB_PASS',"Juniper1234");
        define('DB_HOST',"localhost");
        define('DB_NAME',"VZDB");
        define('DB_NAME_AUDIT',"portal_audit");
        break;
	case 'Cords-MBP':
	case 'Cords-MacBook-Pro.local':
		date_default_timezone_set('America/Chicago');
		define('DB_USER',"root");
		define('DB_PASS',"root");
		define('DB_HOST',"localhost");
		define('DB_NAME',"VZDB");
		define('DB_NAME_AUDIT',"portal_audit");
		break;
    case 'vndpidc':
        define('DB_HOST',"vndpdbidc.verizon.com");
        define('DB_USER',"root");
        define('DB_PASS',"Juniper1234");
        define('DB_NAME',"VZDB");
        define('DB_NAME_AUDIT',"portal_audit");
        break;
    case 'vndprcn':
        define('DB_HOST',"vndpdbrcn.verizon.com");
        define('DB_USER',"root");
        define('DB_PASS',"Juniper1234");
        define('DB_NAME',"VZDB");
        define('DB_NAME_AUDIT',"portal_audit");
        break;
    default:
        date_default_timezone_set('America/Chicago');
        define('DB_USER',"root");
        define('DB_PASS',"P@ssw0rd1");
        define('DB_HOST',"127.0.0.1");  // db node 01
        define('DB_NAME',"VZDB");
        define('DB_NAME_AUDIT',"portal_audit");
}

/*
 *  Common VZ Portal library
 */
// TODO: move this line to the /index.php file, so this can be re-used.
require VZP_LIB . '/Portal/__init__.php';

