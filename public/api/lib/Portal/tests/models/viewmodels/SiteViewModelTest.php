<?php
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 6/20/2015
 * Time: 11:22 AM
 */

/**
 * This is not a true unit test. This is more of a sandbox for now to test
 * the models to ensure we get the data we expect.
 */

try{
    // exception are ok here.
    $model = new \Portal\Models\ViewModels\SiteViewModel('', '');
    $model->getOrmModel()->find_one();
}
catch(PDOException $ex){
    echo '<br/>';
    echo 'no table found';
    echo '<br/>';
}
catch(ErrorException $ex){
    echo '<br/>';
    echo 'no table found';
    echo '<br/>';
}

try {
    // exception are ok here.

    $model = new \Portal\Models\ViewModels\SiteViewModel('SiteInfo', '');
    $test = $model->getOrmModel()->find_one();
    assert($test->SiteName == 'RCNSN2', 'test the value of the model');
}
catch(PDOException $ex){
    echo '<br/>';
    echo 'wrong site provided';
    echo '<br/>';
}
catch(ErrorException $ex){
    echo '<br/>';
    echo 'wrong site provided';
    echo '<br/>';
}

try {
    // exception are bad here.
    $model = new \Portal\Models\ViewModels\SiteViewModel('SiteInfo', 'rcnsn2');
    assert(strpos($model->VSN, ',') !== false, 'test if VSN has a list of csv items');
    assert($model->SiteName == 'RCNSN2', 'test if the sitename is correct');

    $model->type = 'all';


}
catch(PDOException $ex){
    echo '<br/>';
    echo 'wrong site provided again';
    echo '<br/>';
}
catch(ErrorException $ex){
    echo '<br/>';
    echo 'wrong site provided again';
    echo '<br/>';
}
