<?php
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 6/20/2015
 * Time: 11:23 AM
 */

/**
 * Sandbox to test the models
 */
try{
    $model = new \Portal\Models\Model('');
    $model->getOrmModel()->find_one();
}
catch(PDOException $ex){
    echo '<br/>';
    echo 'no table found';
    echo '<br/>';
    echo '<br/>';
}

$model = new \Portal\Models\Model('SiteInfo');
assert(strpos($model->getOrmModel()->find_one('rcnsn2')->VSN, ',') !== false, 'test if VSN has a list of csv items');

