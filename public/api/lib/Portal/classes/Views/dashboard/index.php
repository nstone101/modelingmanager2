<?php
page_header();
?>
<!--     <link href="assets/dashboard/demo/common/style.css"></link>
    <script src="assets/dashboard/src/angular-gridster.js"></script> -->
    <style>
    
.controls {
    margin-bottom: 20px;
}
.page-header {
    margin-top: 20px;
}
ul {
    list-style: none;
}
.box {
    height: 100%;
    border: 1px solid #ccc;
    background-color: #fff;
}
.box-header {
    background-color: #eee;
    padding: 0 30px 0 10px;
    border-bottom: 1px solid #ccc;
    cursor: move;
    position: relative;
}
.box-header h3 {
    margin-top: 10px;
    display: inline-block;
}
.box-content {
    padding: 10px;
}
.box-header-btns {
    top: 15px;
    right: 10px;
    cursor: pointer;
    position: absolute;
}
a {
    color: #ccc;
}
form {
    margin-bottom: 0;
}
.gridster {
    border: 1px solid #ccc;
}
    </style>
        
    <!-- Testing -->
    <script>
        var _USER_  = <?= $user; ?>;
/*        window.app = window.app || {};
        window.app.user = _USER_;
*/    </script>

        <div ui-view></div>

    </div>
<?

page_footer();
?>
