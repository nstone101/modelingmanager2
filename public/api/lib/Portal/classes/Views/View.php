<?php

namespace Portal\Views;

/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 6/19/2015
 * Time: 11:27 PM
 */
class View
{
    protected $variables = array();
    protected $_viewPath;

    function __construct($viewPath = 'lib/Portal/classes/Views/')
    {
        // init stuff here
        $this->_viewPath = $viewPath;
    }

    function set($name, $value)
    {
        $this->variables[$name] = $value;
    }

    function render($viewName)
    {
        extract($this->variables);

        if (file_exists($this->_viewPath . $viewName . '.html')) {
            include($this->_viewPath . $viewName . '.html');
        }
        else if (file_exists($this->_viewPath . $viewName . '.php')) {
            include($this->_viewPath . $viewName . '.php');
        }
        else { /* throw exception */
            echo 'not found';
        }
    }

}