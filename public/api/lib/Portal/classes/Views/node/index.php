<?php
page_header();
?>
    <script>
        head.ready(["jquery", "datatables_editor"], function () {
            //head.load("/assets/js/nodeBrowser.js");
/*            head.load("/assets/js/tables/portal.tables.custom.js");
            head.load("/assets/js/tables/portal.tables.<?= $vm->type == 'audit' ? 'audit.' : ''; ?>js");*/
        });

        // TODO: get rid of global variables
        var siteName = '<?= lower($vm->SiteName); ?>';
        var globalNodePageType = '<?= lower($vm->type); ?>';
        globalNodePageType = globalNodePageType || 'all';
        var nodeVsns = <?= json_encode($vm->vsns) ?>;
        var masterTags = <?= json_encode($vm->tags) ?>;

        var __globals__ = {
            SiteName: siteName,
            PageType: globalNodePageType,
            NodeVsns: nodeVsns,
            MasterTags: masterTags
        };
    </script>
    <style>
        #siteDataControls .dropdown {
            min-width: 190px;
        }

        #content {}

        #content .btn-group .dropdown-toggle {
            border: 1px solid #d1d1d1;
            font-weight: bold;
        }

        #content ul.dropdown-menu {
            border: 1px solid #d1d1d1;
        }

        #content ul.dropdown-menu li a {
            font-family: OpenSansRegular;
            font-weight: normal;
        }

        #content ul.dropdown-menu li a:hover, .dropdown-menu .active a, .dropdown-menu .active a:hover {
            font-family: OpenSansRegular;
            background-color: #d3d3d3;
            color: black;
        }
    </style>
    <!-- Node Main Content -->
    <div id="content" class="node node-scope" ng-controller="nodeController">
        <div id="content-header">
            <div class="row-fluid">
                <div class="crumb">
                    <a href="/node/browser">Node Browser</a>
                    <a href="/node/browser/<?= $vm->SiteGroup; ?>"><?= $vm->SiteGroup; ?></a>
                    <a id="lnk_nodename"
                       href="/node/<?= lower($vm->SiteName); ?>"><?= $vm->SiteName; ?></a>
                    <a href="" class="active"><span id="tab-name">Contact Information</span></a>
                </div>
            </div>
            <?php
            if ($vm->type === 'audit') {
                echo '<h1 class="tab-title">Revision</h1>';
            } else if ($vm->type === 'all') {
                echo '<h1 class="tab-title">Node Info</h1>';
            }
            ?>
        </div>
        <!-- // #content-header -->

        <div class="container-fluid">
            <div class="widget-box">
                <div class="widget-title">
                    <ul id="page-tabs" class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#t_sitecontact" id="lnk_contact">Node Info</a></li>
                        <!--<li class=""><a data-toggle="tab" href="#t_async" id="lnk_async">Async</a></li>-->
                        <!--<li class=""><a data-toggle="tab" href="#t_nodedevicenames" id="lnk_nodedevicenames">Node Device Names</a></li>-->
                        <?php if ($vm->type == "all" || upper($vm->type) == "PIP NNI" || $vm->type == 'audit') { ?>
                            <!--<li class=""><a data-toggle="tab" href="#t_tdr" id="lnk_tdrinfo">TDR Info</a></li>-->
                        <?php } ?>

                        <li class=""><a data-toggle="tab" href="#t_sitedocs" id="lnk_sitedocs">Node Docs</a></li>
                        <li class=""><a data-toggle="tab" href="#t_sitephotos" id="lnk_sitephotos">Node Photos</a></li>
                        <li class=""><a data-toggle="tab" href="#t_sitediagram" id="lnk_sitediagram">Node Diagram</a></li>
                        <li class=""><a data-toggle="tab" href="#t_custaddr" id="lnk_nodeData">Node Data</a></li>
                        <!--<li class="pull-right"><a href="<?/*= $_SESSION['rules']['HelpFileDirectory'].'4'; */?>" target="vzHelp"><i class="fa fa-question-circle"></i></a></li>-->
                    </ul>

                    <!-- Color Legend -->
                    <div id="color_legend" class="modal fade" style="display: none;" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3>Color Legend</h3>
                                </div>
                                <div class="modal-body">
                                    <div class="span6">

                                        <h4>VSN colors on Data Tables:</h4>
                                        <div class="row-fluid span8 offset2">
                                            <p>BASE</p>
                                            <p class="broadsoft">BROADSOFT</p>
                                            <p class="dscip">DSCIP</p>
                                            <p class="genband">GENBAND</p>
                                            <p class="ip-ivr">IP-IVR</p>
                                            <p class="ipac">IPAC</p>
                                            <p class="iscip">ISCIP</p>
                                            <p class="nsrs">NSRS</p>
                                            <p class="occas">OCCAS</p>
                                            <p class="pip-nni">PIP NNI</p>
                                            <p class="sida">SIDA</p>
                                        </div>
                                    </div>
                                    <div class="span4 offset1">
                                        <h4>Audit Table colors:</h4>

                                        <p>No Change</p>

                                        <p style="background-color: #8ef376;">New Row</p>

                                        <p style="background-color: #ffff99;">Modified Field</p>

                                        <p style="background-color: #fa6a5f;">Deleted Row</p>
                                    </div>
                                </div>
                                <!-- modal-body -->
                                <div class="modal-close" data-dismiss="modal"></div>
                            </div>
                            <!-- modal-content -->
                        </div>
                        <!-- modal-dialog -->
                    </div>
                    <!-- #SiteInfoModal -->
                </div>
                <!-- // .widget-title -->


                <div class="widget-content tab-content nopadding">
                    <div id="siteDataControls" class="tab-content">
                        <div class="row">
                            <div class="col-md-2">
                                <!-- SiteData Options -->
                                <div class="btn-group">
                                    <div id="sitedata-dropdown" class="form-horizontal">
                                        <div class="btn-toolbar">
                                            <div class="dropdown btn-group">
                                                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                                                    Customer Addressing <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li class="data_drop" data-toggle="tab" data-target="#t_custaddr" id="lnk_custaddr">
                                                        <a ng-click="vm.pageDataRefresh('CustomerAddressing', 'Customer Addressing')">Customer Addressing</a>
                                                    </li>
                                                    <li class="data_drop" data-toggle="tab" data-target="#t_netconn" id="lnk_netconn">
                                                        <a ng-click="vm.pageDataRefresh('NetConn', 'Network Connections')">Network Connections</a></li>
                                                    <li class="data_drop" data-toggle="tab" data-target="#t_nodedevicename" id="lnk_nodedevicename">
                                                        <a ng-click="vm.pageDataRefresh('NodeDeviceName', 'Node Device Name')">Node Device Name</a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li class="data_drop" data-toggle="tab" data-target="#t_async" id="lnk_async">
                                                        <a ng-click="vm.pageDataRefresh('async', 'Async')">Async</a>
                                                    <li>
                                                    <li class="data_drop" data-toggle="tab" data-target="#t_tdr" id="lnk_tdr">
                                                        <a ng-click="vm.pageDataRefresh('tdr', 'TDR')">TDR</a></li>
                                                    <li class="divider"></li>
                                                    <li class="data_drop" data-toggle="tab" data-target="#t_mx960" id="lnk_mx01">
                                                        <a ng-click="vm.pageDataRefresh('mx960', 'MX960 Port Assignments')">MX960 Port Assignments</a>
                                                    <li>
                                                    <li class="data_drop" data-toggle="tab" data-target="#t_nsport" id="lnk_ns01">
                                                        <a ng-click="vm.pageDataRefresh('NsPort', 'NS Port Assignments')">NS Port Assignments</a></li>
                                                    <li class="divider"></li>
                                                    <li class="data_drop" data-toggle="tab" data-target="#t_ipschema" id="lnk_ipschema">
                                                        <a ng-click="vm.pageDataRefresh('IpSchema', 'IP Schema')">IP
                                                            Schema</a></li>
                                                    <li class="data_drop" data-toggle="tab" data-target="#t_subnetinfo" id="lnk_subnetinfo">
                                                        <a ng-click="vm.pageDataRefresh('SubnetInfo', 'Subnet Information')">Subnet Information</a></li>
                                                    <li class="data_drop" data-toggle="tab" data-target="#t_dnstrap" id="lnk_dnstrap">
                                                        <a ng-click="vm.pageDataRefresh('DnsTrapLogging', 'DNS Trap Logging')">DNS
                                                            / NTP / Logging</a></li>
                                                    <li class="data_drop" data-toggle="tab" data-target="#t_subnetadv" id="lnk_subnetadv">
                                                        <a ng-click="vm.pageDataRefresh('SubnetAdv', 'Subnet Advertising')">Subnet Advertising</a></li>
                                                    <li class="data_drop" data-toggle="tab" data-target="#t_cardslotting"
                                                        id="lnk_cardslotting">
                                                        <a ng-click="vm.pageDataRefresh('CardSlotting', 'Card Slotting')">Card Slotting</a></li>
                                                    <li class="divider"></li>
                                                    <li class="data_drop" data-toggle="tab" data-target="#t_fwgoldenconfig" id="lnk_fwgoldenconfig">
                                                        <a ng-click="vm.pageDataRefresh('FwGoldenConfig', 'FW Golden Config')">FW Golden Config</a></li>
                                                    <li class="data_drop" data-toggle="tab" data-target="#t_vpnrouting" id="lnk_vpnrouting">
                                                        <a ng-click="vm.pageDataRefresh('VpnRouting', 'VPN Routing')">VPN Routing</a></li>

                                                    <li class="divider"></li>
                                                    <li class="data_drop" data-toggle="tab" data-target="#t_pingpoller" id="lnk_pingpoller">
                                                        <a ng-click="vm.pageDataRefresh('PingPoller', 'Ping Poller')">Ping Poller</a></li>


                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <!-- not sure what this is exactly -->
                                <div class="btn-group" dropdown>
                                    <div id="type-dropdown" class="form-horizontal">
                                        <div class="btn-toolbar">
                                            <div class="btn-group">
                                                <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                                                    <i class="fa fa-square all"></i>&nbsp;All <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a ng-click="vm.vsnFilter('All')" class="small" data-value="All">
                                                            <i class="fa fa-square all"></i>&nbsp;All
                                                        </a>
                                                    </li>
                                                    <?php foreach ($vm->vsns as $vsn): ?>
                                                        <li>
                                                            <a ng-click="vm.vsnFilter('<?= $vsn ?>')" class="small" data-value="<?= $vsn ?>">
                                                                <i class="fa fa-square <?= str_replace(' ', '-', strtolower($vsn)) ?>"></i>&nbsp;<?= $vsn ?>
                                                            </a>
                                                        </li>
                                                    <?php endforeach ?>
                                                    <li>
                                                        <a ng-click="vm.vsnFilter('Revision')" class="small" data-value="Revision">
                                                            <i class="fa fa-square all"></i>&nbsp;Revision
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


              <!-- "Site Info" tab-->
              <?php
                  include('partials/tabContent/siteInfoTab.php');
              ?>

              <!-- "Site Docs" tab-->
              <?php
                  include('partials/tabContent/siteDocsTab.php');
              ?>

              <!-- "Site Photos" tab-->
              <?php
                  include('partials/tabContent/sitePhotosTab.php');
              ?>

              <!-- "Site Diagram" tab-->
              <?php
                  include('partials/tabContent/siteDiagramTab.php');
              ?>

              <div ng-controller="editorController"></div>

              <!-- "Customer Addressing" tab -->
              <?php
                  include('partials/tabContent/custAddrTab.php');
              ?>
              <!-- "Network Connections" tab -->
              <?php
                  include('partials/tabContent/netConnTab.php');
              ?>

                  <!-- Node Deivcce Name tab -->
                  <?php
                  include('partials/tabContent/nodeDeviceNameTab.php');
                  ?>
              <!-- "MX960 Port Assignments" tab -->
              <?php
                  include('partials/tabContent/mx960PortAssignmentsTab.php');
              ?>

              <!-- "NS Port Assignments" tab -->
              <?php
                  include('partials/tabContent/nsPortTab.php');
              ?>

              <!-- "IP Schema" tab -->
              <?php
                  include('partials/tabContent/ipSchemaTab.php');
              ?>

              <!-- "Subnet Information" tab -->
              <?php
                  include('partials/tabContent/subnetInfoTab.php');
              ?>

              <!-- "DNS / NTP / Logging" tab -->
              <?php
                  include('partials/tabContent/dnsTrapTab.php');
              ?>

              <!-- "SubnetAdv" tab -->
              <?php
                  include('partials/tabContent/subnetAdvTab.php');
              ?>

              <!-- "CardSlotting" tab -->
              <?php
                  include('partials/tabContent/cardSlottingTab.php');
              ?>

              <!-- "Async" tab-->
              <?php
                  include('partials/tabContent/asyncTab.php');
              ?>

              <!-- "NodeDeviceNames" tab-->
              <?php
                  include('partials/tabContent/nodeDeviceNamesTab.php');
              ?>

              <!-- "TDR Info" tab-->
              <?php
                  include('partials/tabContent/tdrInfoTab.php');
              ?>

              <!-- "FwGoldenConfig" tab-->
              <?php
                  include('partials/tabContent/fwGoldenConfigTab.php');
              ?>

              <!-- "VpnRouting" tab-->
              <?php
                  include('partials/tabContent/vpnRoutingTab.php');
              ?>

              <!-- "PingPoller" tab-->
              <?php
                  include('partials/tabContent/pingPollerTab.php');
              ?>


                        </div>
                        <!-- // .widget-content -->
                    </div>
                    <!-- // .widget-box -->
                </div>
                <!-- // .span12 -->
            </div>
            <!-- // .row-fluid -->
            <?php page_footer(); ?>
        </div>
        <!-- // .container-fluid -->
    </div> <!-- // #content -->
    <script type="text/ng-template" id="vsnGridRow.html">
        <div ng-class="{'base':true, 'gen2-0':row.entity.VSN=='2.0 Gen', 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }">
            <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ 'ui-grid-row-header-cell': col.isRowHeader }" ui-grid-cell>
            </div>
        </div>
    </script>
    <script type="text/ng-template" id="vsnGridCell.html">
        <div ng-class="{'ui-grid-cell-contents':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div>
    </script>


    <script type="text/ng-template" id="auditActionRow.html">
        <div ng-class="{'base':true, 'broadsoft':row.entity.VSN=='BROADSOFT', 'dscip':row.entity.VSN=='DSCIP', 'genband':row.entity.VSN=='GENBAND', 'ip-ivr':row.entity.VSN=='IP IVR', 'iscip':row.entity.VSN=='SCIP', 'nsrs':row.entity.VSN=='NSRS', 'occas':row.entity.VSN=='OCCAS', 'pip-nni':row.entity.VSN=='PIP NNI', 'sida':row.entity.VSN=='SIDA' }">
            <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ 'ui-grid-row-header-cell': col.isRowHeader }" ui-grid-cell>
            </div>
        </div>
    </script>
<?php
