<div id="t_sitedocs" class="p10 tab-pane">
    <div class="tab-content">
        <?php
        // SITE DOCS AUDIT PAGE
        if ($vm->type == 'audit'): ?>
            <p>ISPEC Upload Trail (<a href='/download/l/<?= $vm->site; ?>_ispec_audit.log'>download</a>):</p>
            <pre><?= @file_get_contents(VZP_LOG . '/' . $vm->site . '_ispec_audit.log'); ?></pre>
            <p>MOP Upload Trail (<a href='/download/l/<?= $vm->site; ?>_mop_audit.log'>download</a>):</p>
            <pre><?= @file_get_contents(VZP_LOG . '/' . $vm->site . '_mop_audit.log'); ?></pre>
            <p>Layouts Upload Trail (<a href='/download/l/<?= $vm->site; ?>_layout_audit.log'>download</a>):</p>
            <pre><?= @file_get_contents(VZP_LOG . '/' . $vm->site . '_layout_audit.log'); ?></pre>
            <p>Failovers Upload Trail (<a href='/download/l/<?= $vm->site; ?>_failover_audit.log'>download</a>):</p>
            <pre><?= @file_get_contents(VZP_LOG . '/' . $vm->site . '_failover_audit.log'); ?></pre>
            <p>SiteVisit Upload Trail (<a href='/download/l/<?= $vm->site; ?>_sitevisit_audit.log'>download</a>):</p>
            <pre><?= @file_get_contents(VZP_LOG . '/' . $vm->site . '_sitevisit_audit.log'); ?></pre>
            <p>Miscellaneous Upload Trail (<a href='/download/l/<?= $vm->site; ?>_miscellaneous_audit.log'>download</a>):</p>
            <pre><?= @file_get_contents(VZP_LOG . '/' . $vm->site . '_miscellaneous_audit.log'); ?></pre>
            <p>Config Files Upload Trail (<a href='/download/l/<?= $vm->site; ?>_configfile_audit.log'>download</a>):</p>
            <pre><?= @file_get_contents(VZP_LOG . '/' . $vm->site . '_configfile_audit.log'); ?></pre>

        <?php
        // SITE DOCS DATA PAGE (NOT AUDIT TRAILS)
        else: ?>
            <div ng-controller="nodeDocsController" ng-init="init('<? echo isRWOrAdmin() ?>')">
                <div ng-repeat="docs in docConfig">
                    <node-docs-directive doc-config="docs" doc-data="docData[docs.docType]" is-admin="('<? echo isRWOrAdmin() ?>')"></node-docs-directive>
                </div>
            </div>
        <?php
        endif; ?>
    </div>
</div>
