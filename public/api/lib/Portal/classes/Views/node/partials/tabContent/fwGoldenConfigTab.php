<div id="t_fwgoldenconfig" class="tab-pane" ng-controller="fwGoldenConfigController">
    <style>
        #t_fwgoldenconfig {
        }

        #t_fwgoldenconfig .btn-group.grid-buttons {
            margin-left: 20px;
            margin-bottom: 15px;
            border-radius: 7px;
            width: 75%;
        }

        #t_fwgoldenconfig .btn-group.grid-buttons .btn {
            height: 50px;
            padding-top: 15px;
            width: 100px;
            color: black;
            background-color: white;
            border: 1px solid #d1d1d1;
            width: 40%;
        }

        #t_fwgoldenconfig .btn-group.grid-buttons .btn.active {
            background-color: #d3d3d3;
            color: black;
        }

        #t_fwgoldenconfig .row-offset {
            margin-top: -70px;
        }

        #t_fwgoldenconfig .grid-align {
            margin-right: 20px;
        }

        #t_fwgoldenconfig .searchicon input {
            padding-left: 25px;
        }

        #t_fwgoldenconfig .searchicon:before {
            opacity: .8;
            height: 100%;
            width: 25px;
            display: -webkit-box;
            -webkit-box-pack: center;
            -webkit-box-align: center;
            position: absolute;
            content: "\e003";
            font-family: 'Glyphicons Halflings';
            font-style: normal;
            font-weight: 400;
            pointer-events: none;
            -webkit-font-smoothing: antialiased;
        }

        #t_fwgoldenconfig .multiselect-selected-text {
            color: black;
        }

        #t_fwgoldenconfig .grid-actions {
            margin-bottom: 10px;
            height: 45px;
            width: 100%;
        }
    </style>
    <div class="widget-content tab-content nopadding">
        <div class="row">
            <div class="col-md-offset-4 col-md-2 row-offset grid-align">
                <!-- Show/Hide Column Functionality -->
                <div
                    isteven-multi-select
                    input-model="vm.multiSelectColumns"
                    output-model="vm.shownColumns"
                    button-label="name"
                    item-label="name"
                    tick-property="ticked" ,
                    output-properties="field" ,
                    max-labels="1" ,
                    search-property="name" ,
                    helper-elements="" ,
                    on-item-click="vm.toggleGridColumns(data)">
                </div>
            </div>
            <div class="col-md-3 pull-right row-offset grid-align">
                <!-- Search -->
                <span class="searchicon">
                    <input ng-model="vm.fwGoldenConfigFilterValue" class="form-control" name="search"
                           placeholder="Search within this selection"/>
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="grid-actions">
                    <div class="grid-align">
                        <button type="button" class="btn btn-success pull-right" ng-click="vm.openModal('create')"
                                ng-if="vm.pageType != 'audit'">
                            <i class="fa fa-plus"></i>New
                        </button>
                        <div id="exportDropdown" class="btn-group pull-right" dropdown>
                            <button type="button" class="btn btn-primary dropdown-toggle" dropdown-toggle
                                    style="color:black;">
                                <i class="fa fa-file-text-o" style="color:black;"></i></span>Export <span
                                    class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a ng-click="vm.csvExporter()">Export as CSV</a></li>
                                <li><a ng-click="vm.pdfExporter()">Export as PDF</a></li>
                            </ul>
                        </div>
                        <span class="pull-right delete-btn" ng-if="vm.displayDeleteBtn">
                            <i class="fa fa-trash-o fa-2x" ng-click="vm.deleteRows()"></i>
                        </span>
                        <span class="pull-right edit-btn" ng-if="vm.displayEditBtn">
                            <i class="fa fa-pencil fa-2x" ng-click="vm.openModal('update')"></i>
                        </span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div cg-busy="vm.loadingPromise">
                            <div ui-grid-selection ui-grid-exporter ui-grid-resize-columns ui-grid-move-columns ui-grid="vm.options"
                                 class="grid"></div>
                        </div>
                        <!-- TODO: this will need to be removed -->
                        <div style="display:none;" class="custom-csv-link-location">
                            <label>Your CSV will show below:</label>
                            <span class="ui-grid-exporter-csv-link">&nbsp</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
