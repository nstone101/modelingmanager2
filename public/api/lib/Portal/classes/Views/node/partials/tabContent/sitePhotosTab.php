<div id="t_sitephotos" class="p10 tab-pane">
    <div class="tab-content">
        <?php if ($vm->type == 'audit'): ?>
            <p>Photo Upload Audit Log (<a href='/download/l/<?= $vm->site; ?>_photo_audit.log'>download</a>):</p>
            <pre><?= @file_get_contents(VZP_LOG . '/' . $vm->site . '_photo_audit.log'); ?></pre>
        <?php else: ?>

            <div class="row-fluid btn-toolbar">
                <div class="pull-right">
					<?php if (isRWUser() || isAdmin()) { ?>
						<a data-toggle="modal" href="#modal_photo_upload" class="btn btn-success"><icon class="fa fa-cloud-upload"></icon> Upload New Photo</a>
					<?php } ?>
                    <a href="#" id="lnk_sitephotos_download" class="btn btn-download"><icon class="fa fa-cloud-download"></icon> Download Photo Archive</a>
                </div>
            </div>

            <div class="thumbContainer">
                <ul class="thumbnails">
                    <?php $images = array_chunk($vm->images, 6);
                    foreach ($images as $chunk) {
                        foreach ($chunk as $image) {
                            $image_id = str_replace(".", "_", basename($image));
                            echo "<li class='span2 vzp-thumb thumbnail' id='li_" . $image_id . "'>";
                            echo "<div class='thumb-img-container'>";
                            echo "<a href='/files/node/" . $vm->site . "/photos/full/" . basename($image) . "' data-lightbox='" . $vm->site . "-images' data-title='" . basename($image) . "'>";
                            echo "<img class='lazy' data-original='/files/node/" . $vm->site . "/photos/thumb/" . basename($image) . "'>";
                            echo "</div>";
                            echo "<h4>" . basename($image) . "</h4>";
                            echo "</a>";
	                        echo "<div class='photo-btns'>";
	                        if (isRWUser() || isAdmin()) {
		                        echo "<a href='#' name='".basename($image)."' class='image-del' id='lnk_del_photo_".$image_id."'><i class='fa fa-trash'></i></a>";
	                        }
	                        echo "<a href=/files/node/chtsn3/photos/thumb/" .basename($image) ." download=" .basename($image) ."><i class='fa fa-cloud-download'></i></a>";
	                        echo "</div>";
                            echo "</li>";
                        }
                    }
                    ?>
                </ul>
            </div>

            <!-- "Site Photos" Upload Modal -->
            <div id="modal_photo_upload" class="modal fade" style="display: none;" aria-hidden="true">
                <form class="form-horizontal" id="frm_photo_upload" enctype="multipart/form-data" action="/upload" method="POST">
                    <input type="hidden" name="upload-type" id="upload-type" value="site-photo">
                    <input type="hidden" name="sitename" id="sitename" value="<?= lower($vm->site) ?>">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3>Upload New <?= upper($vm->site); ?> Photo</h3>
                            </div>
                            <div class="modal-body">
                                <div class="control-group">
                                    <div class="input-group">
										<div class="input-group-btn">
											<button class="btn btn-success btn-file">
												<i class="fa fa-paperclip"></i>
												Attach New File
												<input type="file" name="PhotoUpload" accept="image/gif, image/jpeg, image/png" >
											</button>
										</div>
                                        <input type="hidden" name="MAX_FILE_SIZE" value="20000000"/>
                                        <label class="control-label pull-right" for="PhotoUpload">Maximum Upload File Size (20MB max)</label>
                                    </div>
                                </div>
	                            <input type="text" name="photo-label" class="form-control upload-label" readonly="" disabled>
                                <!-- control-group -->
	                            <hr />
	                            <br />
	                            <div class="progressbox" style="display:none;">
		                            <div id="progressbar"></div >
		                            <div class="progress">
			                            <div id="progressbar" class="progress-bar active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
		                            </div>
		                            <div id="statustxt">0%</div>
	                            </div>
	                            <div class="upload-message"></div>
                            </div><!-- modal-body -->

                            <div class="modal-footer">
                                <div class="controls">
	                                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">
		                                <i class="fa fa-close"></i> Cancel
	                                </button>
                                    <button class="btn btn-success" type="submit" value="Upload">
                                        <i class="fa fa-cloud-upload"></i> Upload
                                    </button>
                                </div>
                            </div>
                            <!-- modal-footer -->
                            <div class="modal-close" data-dismiss="modal">
                                <icon class="glyphicon glyphicon-remove"></icon>
                            </div>
                        </div>
                        <!-- modal-content -->
                    </div>
                    <!-- modal-dialog -->
                </form>
            </div>
            <!-- #modal_photo_upload -->

            <!-- "Site Photos" Download Modal -->
	        <div id="modal_photo_download" class="modal fade" style="display: none;" aria-hidden="true">
		        <div class="modal-dialog">
			        <div class="modal-content">
				        <div class="modal-header">
					        <h3>Downloading photos. ?></h3>

				        </div>
				        <div class="modal-body">
					        Please wait, processing ZIP archive ..."
					        <div class="progressbox" style="display:none;">
						        <div id="progressbar"></div>

						        <div class="progress">
							        <div id="progressbar" class="progress-bar active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
						        </div>
						        <div id="statustxt">0%</div>
					        </div>

					        <div class="upload-message"></div>
				        </div><!-- modal-body -->
				        <div class="modal-footer">
					        <div class="controls">
						        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Cancel</button>
					        </div>
				        </div>
				        <!-- modal-footer -->
				        <div class="modal-close" data-dismiss="modal">
					        <icon class="glyphicon glyphicon-remove"></icon>
				        </div>
			        </div>
			        <!-- modal-content -->
		        </div>
		        <!-- modal-dialog -->
	        </div>
	        <!-- #modal_photo_download -->
        <?php endif ?>
    </div>
</div>
