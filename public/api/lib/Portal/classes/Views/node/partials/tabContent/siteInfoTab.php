<div id="t_sitecontact" class="<?php if ($vm->type != 'audit') { echo 'p10'; } ?> tab-pane active">
	<div class="tab-content">
		<?php if ($vm->type == 'audit'): ?>
		<div id="site-info-dropdown" class="form-horizontal" >
			<div class="btn-toolbar">
				<div id="frm_audit" class="dropdown sub-group pull-left">
					<button class="btn btn-default dropdown-toggle" type="button"
					        data-toggle="dropdown"
					        aria-expanded="true">
						<?= $vm->typeLabel ?>
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu" aria-labelledby="site-info-dropdown">
						<li class="data_drop"><a href="/node/<?php echo lower($vm->SiteName); ?>/all">Node Info</a></li>
						<li class="data_drop"><a href="/node/<?php echo lower($vm->SiteName); ?>/audit">Revision</a></li>
					</ul>
				</div>
			</div>
		</div>

        <!-- Put the grid here -->
        <div class="row" ng-controller="siteInfoController">
            <div class="col-md-12">
                <div class="grid-actions">
                    <div class="grid-align">
                        <div class="dropdown pull-left">
                            <button class="btn btn-default dropdown-toggle" type="button" id="frm_siteinfo" data-toggle="dropdown" aria-expanded="true">
                                <i class="fa fa-file-text-o" style="color:black;"></i></span>Export <span class="caret"></span>
                            </button>
	                        <ul class="dropdown-menu" role="menu">
                                <li><a ng-click="vm.csvExporter()">Export as CSV</a></li>
                                <li><a ng-click="vm.pdfExporter()">Export as PDF</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div cg-busy="vm.loadingPromise">
                            <div ui-grid-selection ui-grid-resize-columns ui-grid-exporter ui-grid-move-columns ui-grid="vm.options" class="grid"></div>
                        </div>
                        <!-- TODO: this will need to be removed -->
                        <div style="display:none;" class="custom-csv-link-location">
                            <label>Your CSV will show below:</label>
                            <span class="ui-grid-exporter-csv-link">&nbsp</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<?php else: ?>
		<div class="controls">
			<form class="form-horizontal" id="site-audit-dropdown">
				<div class="btn-toolbar">
					<div class="btn-group">
						<div class="pull-left">
							<button class="btn dropdown-toggle" type="button" id="frm_siteinfo" data-toggle="dropdown" aria-expanded="true">
								<?= $vm->typeLabel ?>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu" aria-labelledby="site-info-dropdown">
								<li class="data_drop"><a href="/node/<?php echo lower($vm->SiteName); ?>/all">Node Info</a></li>
								<li class="data_drop"><a href="/node/<?php echo lower($vm->SiteName); ?>/audit">Revision</a></li>
							</ul>
						</div>

						<?php // TODO: Wire the buttons up. ?>
						<div class="sub-group pull-right">
							<?php if (isRWUser() || isAdmin()) { ?>
                                <button id="GenSiteConfig" class="btn btn-success" href="#modal_genConf" data-toggle="modal" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil"></i> Generate Config file</button>
								<button id="AddVSN" class="btn btn-default" href="#modal_addVSN" data-toggle="modal" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> Add VSN</button>
								<button id="EditSiteInfo" class="btn btn-success" href="#"><i class="fa fa-pencil"></i> Edit</button>
								<!--Hack Alert: reload page to undo changes-->
								<button id="CancelEdit" class="btn btn-default" onclick="window.location.reload(true); return false;"><i class="fa fa-close"></i> Cancel</button>
								<button id="UpdateSiteInfo" class="btn btn-success" type="submit"><i class="fa fa-save"></i> Save</button>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="control-group">
					<input type="hidden" id="SiteName" name="SiteName" value="<?= upper($vm->SiteName) ?>">
					<input type="hidden" id="VSN" name="VSN" value="<?= $vm->VSN ?>">
					<input type="hidden" id="Lab" name="Lab" value="<?= $vm->Lab ?>">

					<div class="controls col-md-6">
						<label class="control-label" for="GenType">Site Type</label>
						<input readonly class="form-control readonly" id="GenType" name="GenType" type="text" autocomplete="off" value="<?= $vm->GenType ?>" disabled>

						<label class="control-label" for="GroupField">Group</label>
						<select id="SiteGroup" name="SiteGroup" class="form-control" disabled>
							<?php if ($vm->Lab == 1): ?>
								<option value="Lab" selected="selected" disabled>Lab</option>
							<?php else: ?>
								<option value="Production" <?php if ($vm->SiteGroup == 'Production') echo ' selected="selected"'; ?>>Production (processing live traffic)</option>
								<option value="Production-Add-VSN" <?php if ($vm->SiteGroup == 'Production-Add-VSN') echo ' selected="selected"'; ?>>Production (adding a VSN)</option>
								<option value="Pre-Production" <?php if ($vm->SiteGroup == 'Pre-Production') echo ' selected="selected"'; ?>>Pre-Production (no live traffic)</option>
								<option value="Lab" <?php if ($vm->SiteGroup == 'Lab') echo ' selected="selected"'; ?>>Lab</option>
								<option value="Deactivated" <?php if ($vm->SiteGroup == 'Deactivated') echo ' selected="selected"'; ?>>Decommissioned</option>
							<?php endif; ?>
						</select>


						<!--<div class="dropdown form-dropdown btn-group" class="form-control" id="SiteGroup" name="SiteGroup" >
						<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true" disabled>
							<?php /*echo $vm->SiteGroup */?><span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li class="data_drop" data-toggle="tab" ng-click="setGroup('Production')"><a>Production (processing live traffic)</a></li>
							<li class="data_drop" data-toggle="tab" ng-click="setGroup('Production-Add-VSN')"><a>Production (adding a VSN)</a></li>
							<li class="data_drop" data-toggle="tab" ng-click="setGroup('Pre-Production')"><a>Production (no live traffic)</a></li>
							<li class="data_drop" data-toggle="tab" ng-click="setGroup('Lab')"><a>Lab</a></li>
							<li class="data_drop" data-toggle="tab" ng-click="setGroup('Deactivated')"><a>Decommissioned</a></li>
						</ul>
					</div>-->


					</div>

					<div class="controls col-md-6">
						<label class="control-label" for="VSNDisplay">VSNs</label>
						<input class="form-control hidden" id="VSNDisplay" name="VSNDisplay" type="text" autocomplete="off" value="<?= $vm->VSN ?>">
						<div class="vsn-list">
							<?php foreach ($vm->vsns as $node_vsn) {
								echo "<span style='color: #333333;' class='badge " . str_replace(" ", "-", lower($node_vsn)) . "'>" . upper($node_vsn) . "</span>\n";
							} ?>
						</div>

						<label class="control-label" for="StateField">Site State</label>
						<select id="SiteState" name="SiteState" class="form-control selectpicker" disabled>
							<?php if ($vm->Lab == 1): ?>
								<option value="Lab" selected="selected" disabled>Lab</option>
							<?php else: ?>
								<option value="Staging-Equipment" <?php if ($vm->SiteState == 'Staging-Equipment') echo ' selected="selected"'; ?>>Staging Equipment</option>
								<option value="Install-ASync-Verification" <?php if ($vm->SiteState == 'Install-ASync-Verification') echo ' selected="selected"'; ?>>Install/ASync Verification</option>
								<option value="Site-Visit-Base-Config" <?php if ($vm->SiteState == 'Site-Visit-Base-Config') echo ' selected="selected"'; ?>>Site Visit/Base Config</option>
								<option value="Node-Device-Config" <?php if ($vm->SiteState == 'Node-Device-Config') echo ' selected="selected"'; ?>>Node Device Config</option>
								<option value="Network-Verification" <?php if ($vm->SiteState == 'Network-Verification') echo ' selected="selected"'; ?>>Network Verification</option>
								<option value="Failover-Complete-25" <?php if ($vm->SiteState == 'Failover-Complete-25') echo ' selected="selected"'; ?>>Failover Pre-Verification: 25% Complete</option>
								<option value="Failover-Complete-50" <?php if ($vm->SiteState == 'Failover-Complete-50') echo ' selected="selected"'; ?>>Failover Pre-Verification: 50% Complete</option>
								<option value="Failover-Complete-75" <?php if ($vm->SiteState == 'Failover-Complete-75') echo ' selected="selected"'; ?>>Failover Pre-Verification: 75% Complete</option>
								<option value="Failover-Complete-100" <?php if ($vm->SiteState == 'Failover-Complete-100') echo ' selected="selected"'; ?>>Failover Pre-Verification: 100% Complete</option>
								<option value="Call-Through-Testing" <?php if ($vm->SiteState == 'Call-Through-Testing') echo ' selected="selected"'; ?>>Call Through Testing</option>
                                                                <option value="Released-ISNTS" <?php if ($vm->SiteState == 'Released-ISNTS') echo ' selected="selected"'; ?>>Released to ISNTS</option>
								<option value="Accepted-ISNTS" <?php if ($vm->SiteState == 'Accepted-ISNTS') echo ' selected="selected"'; ?>>Accepted by ISNTS</option>
							<?php endif; ?>
						</select>
					</div>

					<div class="controls col-md-6 control-clear">
						<label class="control-label" for="SiteCode">Site Code</label>
						<input class="form-control" id="SiteCode" name="SiteCode"
						       type="text" autocomplete="off" value="<?= $vm->SiteCode ?>" disabled>

						<label class="control-label" for="SiteMux">Site Mux</label>
						<input class="form-control" id="SiteMux" name="SiteMux"
						       type="text" autocomplete="off" value="<?= $vm->SiteMux ?>" disabled>

						<label class="control-label" for="ClliCode">CLLI Code</label>
						<input class="form-control" id="ClliCode" name="ClliCode"
						       type="text" autocomplete="off" value="<?= $vm->ClliCode ?>" disabled>

                                                <label class="control-label" for="nodeEMTS">EMTS</label>
                                                <input class="form-control" id="nodeEMTS" name="nodeEMTS"
                                                       type="text" autocomplete="off" value="<?= $vm->EMTS ?>" disabled>

						<label class="control-label" for="SiteAddress">Site Address</label>
						<input class="form-control" id="SiteAddress" name="SiteAddress"
						       type="text" autocomplete="off" value="<?= $vm->SiteAddress ?>" disabled>

						<label class="control-label" for="ContactName">Contact Name</label>
						<input class="form-control" id="ContactName" name="ContactName"
						       type="text" autocomplete="off" value="<?= $vm->ContactName ?>" disabled>

						<label class="control-label" for="ContactPhone">Contact
							Phone</label>
						<input class="form-control" id="ContactPhone" name="ContactPhone"
						       type="text" autocomplete="off" value="<?= $vm->ContactPhone ?>" disabled>

						<label class="control-label" for="ContactEmail">Contact
							Email</label>
						<input class="form-control" id="ContactEmail" name="ContactEmail"
						       type="text" autocomplete="off" value="<?= $vm->ContactEmail ?>" disabled>

						<label class="control-label" for="UpdatedTimeDisplay">Last Updated</label>
						<?php
							$updated_timestamp = strtotime($vm->UpdatedTime);
							$updated_display_time = date("h:i A T, F jS Y", $updated_timestamp);
						?>
						<input readonly class="form-control readonly" id="UpdatedTimeDisplay" name="UpdatedTimeDisplay"
						       type="text" autocomplete="off" value="<?php echo $updated_display_time; ?>" disabled>

						<label class="control-label" for="EditedByDisplay">Updated By</label>
						<input readonly class="form-control readonly" id="EditedByDisplay" name="EditedByDisplay"
						       type="text" autocomplete="off" value="<?= $vm->EditedBy ?>" disabled>


						<!-- TODO: validate Comment to ensure that the form cannot be submitted without a comment being entered/modified. -->
						<label class="control-label comment-label" for="Comment">Comment</label>
						<textarea class="input-xxlarge form-control" id="Comment" name="Comment" rows="5" disabled><?= $vm->Comment ?></textarea>
					</div>
				</div>
				<!-- control-group -->

                <div id="modal_genConf" class="modal fade" style="display: none;" aria-hidden="true">
                    <form id="gen-node-config" action="/node/configfile">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3>Generate Config file for this node</h3>
                            </div>
                            <div class="modal-body">
                                <!-- this needs to be a form -->
                                <input type="hidden" id="SiteName" name="SiteName" value="<?= upper($vm->SiteName) ?>">
                                <input type="hidden" id="VSN" name="VSN" value="<?= $vm->VSN ?>">
                                <input id="res_email" name="res_email" type="checkbox" checked>Email results &nbsp;&nbsp;&nbsp;
                                <input id="res_download" name="res_download" type="checkbox">Download results
                            </div>
                            <div class="modal-footer">
                                <div class="controls">
                                    <button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Cancel</button>
                                    <button id="GenNodeConfig" data-dismiss="modal" type="submit" class="btn btn-success"><icon class="fa fa-plus"></icon> Generate</button>
                                </div>
                            </div>
                            <!-- modal-footer -->
                            <div class="modal-close" data-dismiss="modal">
                                <icon class="glyphicon glyphicon-remove"></icon>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

				<!--AddVSN code here: /node-tools/add-vsn -->
				<div id="modal_addVSN" class="modal fade" style="display: none;" aria-hidden="true">
					<!--<form class="form-horizontal" id="frm_addVSN">-->
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h3>Add VSN</h3>
							</div>
							<div class="modal-body">

								<h3>Please add a VSN to this node.</h3>
								<div class="row">
									<h4>VSN: <span>*</span></h4>
									<div class="btn-group">
										<button class="btn dropdown-toggle" type="button" id="vsn-dropdown" data-toggle="dropdown" aria-expanded="true">
											<div class="d-label">Select a VSN </div><span class="caret"></span>
										</button>
										<ul class="dropdown-menu" role="menu" aria-labelledby="vsn-dropdown">
											<li class="data_drop" value="BROADSOFT"><a>BROADSOFT</a></li>
											<li class="data_drop" value="DSCIP"><a>DSCIP</a></li>
											<li class="data_drop" value="GENBAND"><a>GENBAND</a></li>
											<li class="data_drop" value="IP-IVR"><a>IP-IVR</a></li>
											<li class="data_drop" value="IPAC"><a>IPAC</a></li>
											<li class="data_drop" value="ISCIP"><a>ISCIP</a></li>
											<li class="data_drop" value="NSRS"><a>NSRS</a></li>
											<li class="data_drop" value="OCCAS"><a>OCCAS</a></li>
											<li class="data_drop" value="PIP NNI"><a>PIP NNI</a></li>
											<li class="data_drop" value="SIDA"><a>SIDA</a></li>
										</ul>
									</div>
								</div>
							</div>
							<!-- modal-body -->
							<div class="modal-footer">
								<div class="controls">
									<!-- TODO: check for privlage to save changed info here and show/hide the submit button -->
									<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Cancel</button>

									<!-- TODO: button does not submit anything yet, only closes modal. -->
									<button class="btn btn-success" value="submitVSN" data-dismiss="modal"><i class="fa fa-plus"></i> Add</button>
									<!--<button class="btn btn-success" type="submit" value="submitVSN"><i class="fa fa-plus"></i> Add</button>-->
								</div>
							</div>
							<!-- modal-footer -->
							<div class="modal-close" data-dismiss="modal">
								<icon class="glyphicon glyphicon-remove"></icon>
							</div>
						</div>
						<!-- modal-content -->
					</div>
					<!-- modal-dialog -->
				</div>
			</form>
		</div>
	<?php endif; ?>
	</div>
</div>

<div id="help-overlay" ng-init="init('browser')" ng-controller="helpController" >
	<button ng-repeat="tip in helpData track by $index"
	        type="button"
	        data-container="body"
	        class="spot"
	        ng-class="['spot node-tip' + $index]"
	        ng-click="spotClicked($event, $index)"
	        data-toggle="popover"
	        data-popover-trigger="focus"
	        data-popover-placement="{% raw %} {{ tip.placement }} {% endraw %}"
	        data-popover-template="'/assets/js/app/components/help/helpPopover.html'"
		>
		{% raw %}{{ $index + 1 }}{% endraw %}
	</button>
</div>
