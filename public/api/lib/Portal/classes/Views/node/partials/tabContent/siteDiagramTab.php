<div id="t_sitediagram" class="p10 tab-pane">
    <div class="tab-content">
        <?php if ($vm->type == 'audit'): ?>
            <p>Diagram Upload Audit Log (<a
                    href='/download/l/<?= $vm->site; ?>_diagram_audit.log'>download</a>):
            </p>
            <pre><?= @file_get_contents(VZP_LOG . '/' . $vm->site . '_diagram_audit.log'); ?></pre>
        <?php else: ?>

            <div class="row-fluid btn-toolbar">
                <div class='pull-left'>
                </div>
                <div class="pull-right">
					<?php if (isRWUser() || isAdmin()) { ?>
                    <a data-toggle="modal" href="#modal_diagram_upload"
                       class="btn btn-success">
                        <icon class="fa fa-cloud-upload"></icon>
                        Upload Diagram</a>
					<?php } ?>
                    <?php
                    $diagram_image = '/node/' . lower($vm->site) . '/diagram.png';
                    if (is_file(VZP_FILES . $diagram_image)) {
                        echo '<a href="/files/' . $diagram_image . '" download="node-' . lower($vm->site) . '-diagram.png" id="lnk_sitephotos_download" class="btn btn-download"><icon class="fa fa-cloud-download"></icon> Download Diagram</a>';
                    }
                    ?>
                </div>
            </div>

            <div class='row-fluid ' id='diagram-container'>
                <?php
                $diagram_image = '/node/' . lower($vm->site) . '/diagram.png';
                if (is_file(VZP_FILES . $diagram_image)) {
                    echo "<img id='site-diagram' class='lazy' data-original='/files/" . $diagram_image . "'>";
                } else {
                    echo "<icon id='no-diagram' class='fa fa-photo fa-7'></icon>";
                }
                ?>
            </div>
        <?php endif ?>
    </div>

    <!-- "Site Diagram" Upload Modal -->
    <div id="modal_diagram_upload" class="modal fade" style="display: none;"
         aria-hidden="true">
        <form class="form-horizontal" id="frm_diagram_upload"
              enctype="multipart/form-data" action="/upload" method="POST">
            <input type="hidden" name="upload-type" id="upload-type"
                   value="site-diagram">
            <input type="hidden" name="sitename" id="sitename"
                   value="<?= lower($vm->site) ?>">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3>Upload <?= strtoupper($vm->site); ?> Diagram</h3>
                    </div>
                    <div class="modal-body">
                        <div class="control-group">
                            <div class="input-group">
								<div class="input-group-btn">
									<button class="btn btn btn-success btn-file">
										<i class="fa fa-paperclip"></i>
										Attach New File
										<input type="file" name="DiagramUpload" accept="image/gif, image/jpeg, image/png" >
									</button>
								</div>
                                <input type="hidden" name="MAX_FILE_SIZE" value="20000000"/>
                                <label class="control-label pull-right" for="DiagramUpload">Maximum Upload File Size (20MB max)</label>
                            </div>
	                        <input type="text" name="diagram-label" class="form-control upload-label" readonly disabled>
                        </div>
	                    <hr />
	                    <br />


	                    <div class="progressbox" style="display:none;">
		                    <!-- <div id="progressbar"></div >-->
		                    <div class="progress">
			                    <div id="progressbar" class="progress-bar active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
		                    </div>
		                    <div id="statustxt">0%</div>
	                    </div>

                        <!--<p>This will replace the current Site Diagram.</p>-->
                        <div class="upload-message"></div>

                        <!-- control-group -->
                    </div><!-- modal-body -->

                    <div class="modal-footer">
                        <div class="controls">
	                        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">
		                        <i class="fa fa-close"></i> Cancel
	                        </button>
	                        <button class="btn btn-success" type="submit" value="Upload">
		                        <i class="fa fa-cloud-upload"></i> Upload
	                        </button>
                        </div>
                    </div>
                    <!-- modal-footer -->
                    <div class="modal-close" data-dismiss="modal">
                        <icon class="glyphicon glyphicon-remove"></icon>
                    </div>
                </div>
                <!-- modal-content -->
            </div>
            <!-- modal-dialog -->
        </form>
    </div>
    <!-- #modal_photo_upload -->
</div>
