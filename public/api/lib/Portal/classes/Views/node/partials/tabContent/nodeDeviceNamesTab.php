<div id="t_nodedevicenames" class="tab-pane">
    <table class="table table-bordered table-hover fixed-table"
           id="nodedevicenames-table">
        <thead>
        <tr>
            <?php if ($vm->type == 'audit'): ?>
                <th></th>
                <th>Modify Time</th>
                <th>Row ID</th>
                <th>Primary Device Tag</th>
                <th>Node Device Name</th>
                <th>Device Abbreviation</th>
                <th>Device Type</th>
                <th>Device Description</th>
                <th>Removed</th>
                <th>Created</th>
            <?php else: ?>
                <th>VSN</th>
                <th>Primary Device Tag</th>
                <th>Node Device Name</th>
                <th>Device Abbreviation</th>
                <th>Device Type</th>
                <th>Device Description</th>
            <?php endif ?>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
