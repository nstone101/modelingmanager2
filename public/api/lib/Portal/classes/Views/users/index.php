<?php
page_header();
?>
    <style>
        .grid {
            height: 560px;
            width: 1320px;
            margin-left: 25px;
            margin-top: 5px;
        }

        .ng-cloak {
            display: none;
        }

        .grid-align {
            margin-right: 20px;
        }

        .grid-align .delete-btn {
            margin-right: 15px;
        }

        .grid-align .edit-btn {
            margin-right: 15px;
        }
    </style>
    <!-- Testing -->
    <script>
        var _USER_  = <?= $user; ?>;
        window.app = window.app || {};
        window.app.user = _USER_;
    </script>

    <div ui-view=""></div>

<?php
page_footer();