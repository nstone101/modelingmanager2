<?php
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 7/7/2015
 * Time: 9:06 PM
 */

namespace Portal\Services;

use \Slim\Middleware;

/**
 * Class AuthMiddleware
 * @package Portal\Services
 */
class AuthMiddleware extends \Slim\Middleware {

    /**
     * an attempt at taking the user back to their original page and tab.
     * @var array
     */
    protected $pageToTabMappings;

    /**
     * used to provide white listed exceptions to the auth check
     * whitelistings are just pieces of url used to determine if the request
     * url show be allowed to bypass the auth check or not. if a page, like the
     * temp user validation page doesn't need security, then you add it to the
     * whitelisting. just be careful what pages, or api's you allow to bypass the auth check.
     *
     * @var mixed white listed page urls
     */
    protected $whiteListings;

    function __construct(){
        // unable to redirect
        $this->pageToTabMappings = array(
            'customerAddressing' => 'tab_custAddr',
            'ipschema' => 'tab_ipSchema',
            'mx960PortAssignments' => 'tab_mx960',
            'ns5400PortAssignments' => 'tab_ns5400',
            'networkConnections' => 'tab_netConn',
            'cardSlotting' => 'tab_cardSlotting'
        );

        $this->whiteListings = array('/admin/verify');

    }

    protected function mapApiToPage($uri){
        $parts = explode('/', $uri);
        // /api/customerAddressing/read//all/all
        $pageName = $parts[2];
        $siteName = $parts[4];
        // TODO: add in something that will let me nav to the second level tabs
        return sprintf('/node/%s%s%s', $siteName, '%23',  't_custaddr'/*$this->pageToTabMappings[$pageName]*/);
    }

    /**
     * Call
     */
    public function call()
    {
        $uri = $this->app->request()->getPathInfo();

        // look through whitelistings
        foreach($this->whiteListings as $k => $v) {
            // check if url is in the whitelist
            if(strpos($uri, $v) !== false){
                // allow the request through
                $this->next->call();
                return;
            }
        }
        $at_login = strpos($uri, "/admin/login");
        if(!isset($_SESSION['user']) && $at_login === false){
            if(strpos($uri, "/api/") === false){
                $this->app->redirect('/auth/login?req='.$uri);
            }
            else {
                $res = $this->app->response();
                $res->status(401);
                $res->body('/auth/login?req='.$this->mapApiToPage($uri));
            }
        }
        else {
            $this->next->call();
        }

    }
}