<?php
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 7/30/2015
 * Time: 7:35 PM
 */
namespace Portal\Services;

require_once VZP_DIR . '/build/composer/vendor/autoload.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Adrotec\BreezeJs\Doctrine\ORM;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Breeze
 * @package Portal\Services
 * abstraction around breeze to be called from slim php routes
 * this class is a singleton bc of the expense of setting up the object.
 * the goal is to be able to reuse the object a few times before gc gets it.
 */
class Breeze {

    /**
     * @var Breeze singleton
     */
    private static $instance;

    /**
     * @var Adrotec\BreezeJs\Framework\Application
     */
    protected $breeze;

    /**
     * Returns the *Singleton* instance of this class.
     *
     * @return Singleton The *Singleton* instance.
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    protected function __construct(){

        $paths  = array( realpath(VZP_LIB . '/Portal/classes/Models/Doctrine'));
        $isDevMode = true;
        $dbConnection = array(
            'driver' => 'pdo_mysql',
            'dbname' => DB_NAME,
            'host' => DB_HOST,
            'user' => DB_USER,
            'password' => DB_PASS
        );

        $cache = new \Doctrine\Common\Cache\ArrayCache();

        $reader = new AnnotationReader();
        $driver = new \Doctrine\ORM\Mapping\Driver\AnnotationDriver($reader, $paths);

        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
        $config->setMetadataCacheImpl( $cache );
        $config->setQueryCacheImpl( $cache );
        $config->setMetadataDriverImpl( $driver );

        $entityManager = EntityManager::create($dbConnection, $config);

        //-- This I had to add to support the Mysql enum type.
        $platform = $entityManager->getConnection()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');

        /* @var $entityManager instanceof \Doctrine\ORM\EntityManager */
        /* @var $serializer instanceof \JMS\Serializer\SerializerInterface */
        /* @var $validator instanceof \Symfony\Component\Validator\Validator\ValidatorInterface */

        $serializer = \Adrotec\BreezeJs\Serializer\SerializerBuilder::create($entityManager);

        $this->breeze = new \Adrotec\BreezeJs\Framework\Application(
            $entityManager,
            $serializer,
            $validator
        );

        $this->breeze->addResources(array(
            // Resource name => Class name
            'breeze/Users' => 'Portal\Models\Doctrine\Users',
            'breeze/UserRole' => 'Portal\Models\Doctrine\UserRole',
            'breeze/Roles' => 'Portal\Models\Doctrine\Roles',
            'breeze/Permissions' => 'Portal\Models\Doctrine\Permissions',
            'breeze/RolePermission' => 'Portal\Models\Doctrine\RolePermission',
        ));

    }

    public function handleRequest(/*$request*/){
        $request = Request::createFromGlobals();

        if(strpos($request, 'metadata') === false){
            $request->attributes->set('resource', trim($request->getPathInfo(), '/'));
        }
        else {
            $request->attributes->set('resource', 'Metadata');
        }

        return $this->breeze->handle($request);
    }

    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     *
     * @return void
     */
    private function __wakeup()
    {
    }

}