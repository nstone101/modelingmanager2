<?php
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 6/29/2015
 * Time: 9:25 PM
 */
namespace Portal\Services;

/**
 * ValidationSevice 
 * provide any validations needed to run async for the application
 */
class ValidationService {
    /**
     * validate the username to ensure no duplication
     * @param  string $username potential username
     * @return boolean can we use it or nah
     */
    public function validateUsername($username) {
        $user = \ORM::for_table('users')->whereEqual('username', $username)->findArray();

        if ($user == null) {
            $retVal = new \StdClass();
            $retVal->status = 'ok';
            $retVal->message = 'username not in use';
            echo json_encode($retVal);
        }
        else {
            $retVal = new \StdClass();
            $retVal->status = 'error';
            $retVal->message = 'username in use';
            echo json_encode($retVal);
        }
        return;
    }

    /**
     * validate the potential email address
     * @param  string $email potenetial email
     * @return boolean can we use it or nah
     */
    public function validateEmail($email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $retVal = new \StdClass();
                $retVal->status = 'error';
                $retVal->message = 'invalid email';
                echo json_encode($retVal);
                return;
            }

            $user = \ORM::for_table('users')->whereEqual('email', $email)->findArray();

            if ($user == null) {
                $retVal = new \StdClass();
                $retVal->status = 'ok';
                $retVal->message = 'email not in use';
                echo json_encode($retVal);
            }
            else {
                $retVal = new \StdClass();
                $retVal->status = 'error';
                $retVal->message = 'email in use';
                echo json_encode($retVal);
            }
            //}
    }
}