<?php
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 7/10/2015
 * Time: 8:23 PM
 */

namespace Portal\Models\ViewModels;
use \Portal\Models\NodeDataModel;

class DnsTrapLogViewModel extends NodeDataModel
{
    /**
     * selected site
     * @var $site
     */
    public $siteName;

    /**
     * track columns needed for the data tables
     * @var array $columns
     */
    public $columns = array();

    function __construct($type, $siteName = '')
    {
        $this->tableName = 'DNSTrapLog';
        $this->tableName .= ($type == 'audit')? '_Audit' : '';

        parent::__construct($this->tableName);

        $this->siteName = strtoupper($siteName);
    }
}