<?php
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 7/25/2015
 * Time: 1:15 PM
 */

namespace Portal\Models\ViewModels;

use \Portal\Models\ModelBase;

class UserViewModel extends ModelBase {
    /**
     * track columns needed for the data tables
     * @var array $columns
     */
    public $columns = array();

    function __construct()
    {
        $this->tableName = 'users';
        parent::__construct($this->tableName);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function read($id)
    {
        if($id == 0) {
            $this->_model = $this->_orm->findArray();
        }
        else {
            $this->_model = $this->_orm->whereIdIn($id)->findArray();
        }

        return $this->_model;
    }

    /**
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    public function create($params){
        $schema = $this->_orm->create();
        return $this->saveOrmObject($schema, $params);
    }

    /**
     * @param $params
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update($params, $id){
        $schema = $this->_orm->findOne($id);
        return $this->saveOrmObject($schema, $params);
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id){
        $schema = $this->_orm->findOne($id);
        if ($schema !== false) {
            $schema->delete();
        }
        return true;
    }
}