<?php

namespace Portal\Models\ViewModels;

use \Portal\Models\Model;

/**
 * Class IpSchemaViewModel
 * @package Portal\Models\ViewModels
 */
class NetworkTypesViewModel extends Model
{
    function __construct($type, $siteName = '')
    {
        $this->tableName = 'NetworkTypes';
        $this->tableName .= ($type == 'audit')? '_Audit' : '';
        parent::__construct($this->tableName);
    }

    public function read($conditions)
    {
        // $conditions is key,value pairs
        if ($conditions) {
            $this->_model = $this->_orm->where($conditions)->findArray();
        } else {
            $this->_model = $this->_orm->findArray();
        }

        return $this->_model;
    }
}
