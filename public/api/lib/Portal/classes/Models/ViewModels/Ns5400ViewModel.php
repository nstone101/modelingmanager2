<?php

namespace Portal\Models\ViewModels;

use \Portal\Models\NodeDataModel;

/**
 * Class Ns5400ViewModel
 * @package Portal\Models\ViewModels
 */
class Ns5400ViewModel extends NodeDataModel
{
    /**
     * selected site
     * @var $site
     */
    public $siteName;

    /**
     * track columns needed for the data tables
     * @var array $columns
     */
    public $columns = array();

    function __construct($type, $siteName = '')
    {
        $this->tableName = 'NS5400PortAssignments';
        $this->tableName .= ($type == 'audit')? '_Audit' : '';
        parent::__construct($this->tableName);

        $this->siteName = strtoupper($siteName);
    }

    public function read(array $params)
    {
        $params = (object)$params;

        $conditions = array(
            'SiteName' => strtoupper($params->siteName),
            'NS' => $params->ns
        );

        if ($params->vsn != 'all') {
            $conditions['VSN'] = $params->vsn;
        }
        $this->_model = $this->_orm->where($conditions)->findArray();

        return $this->_model;
    }
}