<?php

namespace Portal\Models\ViewModels;

use \Portal\Models\NodeDataModel;

/**
 * Class CardSlottingViewModel
 * @package Portal\Models\ViewModels
 */
class CardSlottingViewModel extends NodeDataModel
{
    /**
     * selected site
     * @var $site
     */
    public $siteName;

    /**
     * track columns needed for the data tables
     * @var array $columns
     */
    public $columns = array();

    function __construct($type, $siteName = '')
    {
        $this->tableName = 'CardSlotting';
        $this->tableName .= ($type == 'audit')? '_Audit' : '';

        parent::__construct($this->tableName);

        $this->siteName = strtoupper($siteName);
    }
}