<?php

namespace Portal\Models\ViewModels;

use \Portal\Models\NodeDataModel;

/**
 * Class NetworkConnectionsViewModel
 * @package Portal\Models\ViewModels
 */
class NetworkConnectionsViewModel extends NodeDataModel
{
    /**
     * selected site
     * @var $site
     */
    public $siteName;

    /**
     * track columns needed for the data tables
     * @var array $columns
     */
    public $columns = array();

    function __construct($type, $siteName = '')
    {
        $this->tableName = 'NetworkConnections';
        $this->tableName .= ($type == 'audit')? '_Audit' : '';
        parent::__construct($this->tableName);

        $this->siteName = strtoupper($siteName);
    }
}