<?php

namespace Portal\Models\ViewModels;

use \Portal\Models\NodeDataModel;

/**
 * @class SiteViewModel
 * User: bpowell
 * Date: 6/20/2015
 * Time: 10:05 AM
 */
class SiteViewModel extends NodeDataModel
{
    /**
     * selected site
     * @var $site
     */
    public $site;

    /**
     * page type
     * @var $type
     * @values 'all', 'audit', 'SiteInfo'
     */
    public $type;

    /**
     * display for page type
     * @var $typeLabel
     */
    public $typeLabel;

    /**
     * virtual service names
     * @var $vsns
     */
    public $vsns;

    /**
     * something about a mop document?
     * @var $mops
     */
    public $mops;

    /**
     * something about a ispec document?
     * @var $ispecs
     */
    public $ispecs;

    /* add layouts and failovers */
    public $layouts;
    public $failovers;
    public $sitevisits;
    public $miscellaneous;
    public $configfiles;






    /**
     * get images for an image carousel
     * @var $images
     */
    public $images;

    /**
     * MX960 Port Assignments
     * @var Mx960ViewModel $mx960ViewModels
     */
    public $mx960ViewModel;


    function __construct($tableName, $site, $type = 'all')
    {
        parent::__construct($tableName);

        $this->site = $site;
        $this->type = $type;
        if ($type === 'audit') {
            $this->typeLabel = 'Revision';
        } else {
            $this->typeLabel = 'Node Info';
        }

        // TODO: need an abstraction
        // this will throw an exception and we don't want to
        // do that in a constructor
        if (!empty($site) || strlen($site) > 0) {
            $this->_model = $this->_orm->find_one($site);
        }

        // setup property values
        $this->vsns = explode(",", $this->VSN);

        // Get the ISPEC and MOP documents for the site docs tab.
        $this->ispecs = glob(VZP_DIR . "/files/node/" . $site . "/ispec/*");
        $this->mops = glob(VZP_DIR . "/files/node/" . $site . "/mops/*");

        $this->layouts = glob(VZP_DIR . "/files/node/" . $site . "/layouts/*");
        $this->failovers = glob(VZP_DIR . "/files/node/" . $site . "/failovers/*");
        $this->sitevisits = glob(VZP_DIR . "/files/node/" . $site . "/sitevisits/*");
        $this->miscellaneous = glob(VZP_DIR . "/files/node/" . $site . "/miscellaneous/*");
        $this->configfiles = glob(VZP_DIR . "/files/node/" . $site . "/configfiles/*");





        // Get the site photos for the image carousel
        $this->images = glob(VZP_DIR . "/files/node/" . $site . "/photos/full/*");

       $this->mx960ViewModel = new \Portal\Models\ViewModels\Mx960ViewModel($site, $type);
    }

}
