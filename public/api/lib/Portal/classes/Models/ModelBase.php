<?php
namespace Portal\Models;
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 6/19/2015
 * Time: 11:24 PM
 */

/**
 * Class Model
 * base class to provide helper methods
 * @package Portal\Models
 */
class ModelBase
{
    /**
     * @var string $tableName
     */
    protected $tableName;

    /**
     * contains the orm model
     * @var \ORM $_orm
     */
    protected $_orm;

    /**
     * contains the results fo an orm query.
     * @var object $_model
     */
    protected $_model;

    function __construct($tableName)
    {
        $this->_orm = \ORM::for_table($tableName);
    }

    /**
     * escape hatch in case you need to ORM object
     * itself to run queries.
     */
    public function getOrmModel(){
        return $this->_orm;
    }

    /**
     * @param $object
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    protected function saveOrmObject($object, $params){
        try {
            $object->set($params);
            return $object->save();
        }
        catch(\Exception $e){
            throw $e;
        }
    }

    /**
     * despite the public accessor
     * this will only be called by the model class when
     * it doesn't find the property already defined as in the orm model
     *
     * @access private
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->_model->{$name} = $value;
    }

    /**
     * despite the public accessor
     * this will only be called by the model class when
     * it doesn't find the property already defined as in the orm model
     *
     * @access private
     * @param $name
     * @return object
     */
    public function __get($name)
    {
        return $this->_model->{$name};
    }
}