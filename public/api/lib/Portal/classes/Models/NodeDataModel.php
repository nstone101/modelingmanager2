<?php
namespace Portal\Models;
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 6/19/2015
 * Time: 11:24 PM
 */
class NodeDataModel extends ModelBase
{
    function __construct($tableName)
    {
        parent::__construct($tableName);
    }

    public function read(array $params)
    {
        $params = (object)$params;
        $conditions = array(
            'SiteName' => strtoupper($params->siteName)
        );

        if ($params->vsn != 'all') {
            $conditions['VSN'] = $params->vsn;
        }
        $this->_model = $this->_orm->where($conditions)->findArray();
        return $this->_model;
    }

    /**
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    public function create($params){
        $schema = $this->_orm->create();
        return $this->saveOrmObject($schema, $params);
    }

    /**
     * @param $params
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update($params, $id){
        $schema = $this->_orm->findOne($id);
        return $this->saveOrmObject($schema, $params);
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id){
        $schema = $this->_orm->findOne($id);
        if ($schema !== false) {
            $schema->delete();
        }
        return true;
    }
}