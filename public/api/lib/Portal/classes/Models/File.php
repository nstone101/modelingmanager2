<?php

namespace Portal\Models;

use Model;

/**
 * File model
 */
class File extends Model {
    public static $_table_use_short_name = true;
    public static $_table = 'Files';
}