<?php

namespace Portal\Models\Users;
use Model;
class Permission extends \Model {
     public static $_table_use_short_name = true;
     public static $_table = 'permissions';
     public function roles(){
     	return $this->hasManyThrough('\Portal\Models\Users\Role', 'RolePermission', 'permission_id', 'role_id', 'permission_id', 'role_id');
     }
}