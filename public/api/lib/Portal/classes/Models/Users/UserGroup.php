<?php
namespace Portal\Models\Users;
use Model;

/**
 * UserGroup
 * stores the user's groups
 * one to many relationship with users
 */
class UserGroup extends Model {
     public static $_id_column = 'group_id';
     public static $_instance_id_column = 'group_id';
     public static $_table_use_short_name = true;
     public static $_table = 'user_groups';

     
/*     public function users(){
     	return $this->hasMany('\Portal\Models\Users\User', 'Usergroup', 'group_name');
     }*/
}