<?php

namespace Portal\Models\Users;

use Model;

class Role extends Model {
     public static $_table_use_short_name = true;
     public static $_table = 'roles';

	public function users(){
		return $this->hasManyThrough('\Portal\Models\Users\User', 'UserRole', 'role_id', 'user_id', 'role_id', 'id');
	}

	public function permissions(){
		return $this->hasManyThrough('\Portal\Models\Users\Permission', 'RolePermission', 'role_id', 'permission_id', 'role_id',  'permission_id');
	}
}