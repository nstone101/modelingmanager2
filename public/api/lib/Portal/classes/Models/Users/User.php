<?php

namespace Portal\Models\Users;

use Model;

/**
 * User model
 */
class User extends Model {
     public static $_table_use_short_name = true;
     public static $_table = 'users';

     public function roles(){
     	return $this->hasManyThrough('\Portal\Models\Users\Role', 'UserRole', 'user_id', 'role_id', 'id', 'role_id');
     }
}