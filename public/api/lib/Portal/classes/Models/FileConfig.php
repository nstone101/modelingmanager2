<?php
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 12/20/2015
 * Time: 11:49 PM
 */
namespace Portal\Models;

use Model;

/**
 * FileConfig model
 */
class FileConfig extends Model {
    public static $_table_use_short_name = true;
    public static $_table = 'FileConfigs';
}