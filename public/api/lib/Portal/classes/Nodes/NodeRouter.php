<?php

namespace Portal\Nodes;

use \Portal\Services;

class NodeRouter {
    public static function routes($app){

        //$app->group('/api', function() use($app){
            $app->group('/node-data', function() use($app){
                // cstomer addressing
                $app->group('/customer-addressing', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'CustomerAddressing', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'CustomerAddressing', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'CustomerAddressing';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'CustomerAddressing');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'CustomerAddressing';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = array($id); //["1906", "1907"]

                        $params = array('ids'=>$json, 'tableName'=>'CustomerAddressing');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });
                // cstomer circuits
                $app->group('/customer-circuits', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'CustomerCircuits', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'CustomerCircuits', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'CustomerCircuits';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'CustomerCircuits');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'CustomerCircuits';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = array($id); //["1906", "1907"]

                        $params = array('ids'=>$json, 'tableName'=>'CustomerCircuits');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // node device name
                $app->group('/node-device-names', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'NodeDeviceNames', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'NodeDeviceNames', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'NodeDeviceNames';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'NodeDeviceNames';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'NodeDeviceNames');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // ip schema
                $app->group('/ip-schema', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'IPSchema', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'IPSchema', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'IPSchema';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'IPSchema');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'IPSchema';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'IPSchema');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // MX960 Port Assignments
                $app->group('/mx960-port-assignments', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'MX960PortAssignments', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'MX960PortAssignments', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'MX960PortAssignments';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'MX960PortAssignments');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'MX960PortAssignments';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'MX960PortAssignments');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // ns5400 port assignments
                $app->group('/ns5400-port-assignments', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'NS5400PortAssignments', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'NS5400PortAssignments', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'NS5400PortAssignments';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'NS5400PortAssignments');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'NS5400PortAssignments';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'NS5400PortAssignments');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // Network Connections
                $app->group('/network-connections', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'NetworkConnections', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'NetworkConnections', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'NS5400PortAssignments';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'NetworkConnections');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'NetworkConnections';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'NetworkConnections');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // CardSlotting
                $app->group('/card-slotting', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'CardSlotting', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'CardSlotting', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'CardSlotting';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'CardSlotting');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'CardSlotting';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'CardSlotting');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // IP_Private
                $app->group('/ip-private', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'IP_Private', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'IP_Private', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'IP_Private';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'IP_Private');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'IP_Private';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'IP_Private');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // IP IDN
                $app->group('/ip-idn', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'IP_IDN', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'IP_IDN', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'IP_IDN';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'IP_IDN');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'IP_IDN';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'IP_IDN');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // IP Public
                $app->group('/ip-public', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'IP_Public', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'IP_Public', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'IP_Public';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'IP_Public');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'IP_Public';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'IP_Public');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // DNS Trap Log
                $app->group('/dns-trap-log', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'DNSTrapLog', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'DNSTrapLog', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'DNSTrapLog';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'DNSTrapLog');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'DNSTrapLog';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'DNSTrapLog');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // Subnet Adv
                $app->group('/subnet-information', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'SubnetAdv', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'SubnetAdv', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'SubnetAdv';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'SubnetAdv');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'SubnetAdv';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'SubnetAdv');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // Subnet Adv
                $app->group('/subnet-adv', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'SubnetAdv', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'SubnetAdv', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'SubnetAdv';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'SubnetAdv');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'SubnetAdv';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'SubnetAdv');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // Async
                $app->group('/async', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'Async', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'Async', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'Async';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'Async');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'Async';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'Async');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // PingPoller
                $app->group('/ping-poller', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'PingPoller', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'PingPoller', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'PingPoller';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'PingPoller');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'PingPoller';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'PingPoller');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // TDR
                $app->group('/tdr', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'TDR', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'TDR', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'TDR';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'TDR');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'TDR';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'TDR');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // FWGoldenConfig
                $app->group('/fw-golden-config', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'FWGoldenConfig', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'FWGoldenConfig', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'FWGoldenConfig';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'FWGoldenConfig');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'FWGoldenConfig';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'FWGoldenConfig');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // FWSetup
                $app->group('/fw-setup', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'FWSetup', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'FWSetup', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'FWSetup';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'FWSetup');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'FWSetup';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'FWSetup');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // NodeNotes
                $app->group('/node-notes', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'NodeNotes', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'NodeNotes', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'NodeNotes';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'NodeNotes');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'NodeNotes';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'NodeNotes');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

                // vpn-routing
                $app->group('/vpn-routing', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'VPNRouting', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                    $app->get('/:id', function($id) {
                        $node = new NodeService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'VPNRouting', 'id' => $id)));
                    });

                    $app->post('/', function() use ($app) {
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'VPNRouting';
                        $node = new NodeService();
                        echo json_encode($node->createNodeTable($json));
                    });

                    $app->post('/batch-delete', function() use($app){
                        $json = json_decode($app->request->getBody());

                        $params = array('ids'=>$json, 'tableName'=>'VPNRouting');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });

                    $app->put('/:id', function($id) use($app){
                        $json = json_decode($app->request->getBody());
                        $json->tableName = 'VPNRouting';
                        $node = new NodeService();
                        echo json_encode($node->updateNodeTable($id, $json));
                    });

                    $app->delete('(/:id)', function($id=null) use($app){
                        $json = json_decode($app->request->getBody());
                        if($json == null){
                            $json = array($id); //["1906", "1907"]
                        }
                        $params = array('ids'=>$json, 'tableName'=>'VPNRouting');
                        $node = new NodeService();
                        echo json_encode($node->deleteNodeTableByIds($params));
                    });
                });

            });

            // node grouping
            $app->group('/node', function() use($app) {
                $app->get('/', function () use ($app) {
                    try {
                        $filter = $app->request()->params('filter');

                        $service = new NodeService($filter);
                        echo json_encode($service->getNodeBrowser($filter));
                    } catch (\Exception $e) {
                        echo json_encode("{'code': 500, 'message': 'unable to get the nodes. '" . $e->getMessage() . "}");
                    }
                });

                $app->get('/:site', function ($site) use ($app) {
                    try {
                        $filter = $app->request()->params('filter');

                        $service = new NodeService($site, $filter);
                        echo json_encode($service->getNodeInformation($site, $filter));
                    } catch (\Exception $e) {
                        echo json_encode("{'code': 500, 'message': 'unable to retrieve node information. '" . $e->getMessage() . "}");
                    }
                });

                $app->post('/', function () use ($app) {
                    // do something to add a node here.
                });

                $app->put('/', function () use ($app) {
                    $json = $app->request->getBody();
                    // get portal controller
                    if ($json)
                        $json = json_decode($json);
                    try {
                        $service = new NodeService($site, 'all');

                        $retVal = $service->updateNode($site, $json);
                        echo json_encode($retVal);
                    } catch (\Exception $e) {
                        echo json_encode("{'code': 500, 'message': 'unable to update the node info. '" . $e->getMessage() . "}");
                    }
                });
            });
            //$siteName, $vsn = 'all', $type = 'all'


            $app->group('/nodes', function () use ($app) {

                $app->get('/downloadlog', function () use($app) {
                    $log = VZP_FILES . '/node/rvgsn2/diagram.png';
                    $res = $app->response();
                    $res['Content-Description'] = 'File Transfer';
                    $res['Content-Type'] = 'application/octet-stream';
                    $res['Content-Disposition'] ='attachment; filename=' . basename($log);
                    $res['Content-Transfer-Encoding'] = 'binary';
                    $res['Expires'] = '0';
                    $res['Cache-Control'] = 'must-revalidate';
                    $res['Pragma'] = 'public';
                    $res['Content-Length'] = filesize($log);
                    readfile($log);
                });

                $app->get('/browser(/:filter)', function ($filter = '') {
                    try {
                        $service = new NodeService($filter);
                        echo json_encode($service->getNodeBrowser($filter));
                    }
                    catch(\Exception $e) {
                        echo json_encode("{'code': 500, 'message': 'unable to get the node browser. '" . $e->getMessage() . "}");
                    }
                });

                $app->get('/newnodesbrowser(/:filter)', function ($filter = '') {

                    try {
                        $service = new NodeService($filter);
                        echo json_encode($service->getNewNodesBrowser($filter));
                    }
                    catch(\Exception $e) {
                        echo json_encode("{'code': 500, 'message': 'unable to get the new nodes browser. '" . $e->getMessage() . "}");
                    }
                });


                $app->post('/createnewnode/:sitename/:sitetype', function($sitename, $sitetype) use($app){
                    try {
                        $service = new NodeService('');
                        echo json_encode($service->createNewNode($sitename, $sitetype));
                    }
                    catch(\Exception $e) {
                        echo json_encode("{'code': 500, 'message': 'unable to get the new nodes browser. '" . $e->getMessage() . "}");
                    }
                });

                $app->post('/dumpcompletenode/:sitename', function($sitename) use($app){
                    try {

                        $service = new NodeService('');
                        echo json_encode($service->dumpCompleteNode($sitename));

                    }
                    catch(\Exception $e) {
                        echo json_encode("{'code': 500, 'message': 'unable to dump node data. '" . $e->getMessage() . "}");
                    }
                });


                $app->group('/designDocs', function() use($app){
                    $app->get('/:gen', function($gen){
                        $docs = array(
                            "docs" => array(
                                "mops" => glob(VZP_DIR . "/files/mops/" .$gen ."/*"),
                                "vsnmops" => glob(VZP_DIR . "/files/vsnmops/" .$gen ."/*"),
                                "lld" => glob(VZP_DIR . "/files/lld/" .$gen ."/*")
                            ),
                            "paths" => array(
                                "mops" => "/files/mops/" .$gen ."/",
                                "vsnmops" => "/files/vsnmops/" .$gen ."/",
                                "lld" => "/files/lld/" .$gen ."/"
                            ),
                        );
                        echo json_encode($docs);
                    });
                });

                $app->group('/siteInfo', function () use ($app) {
                    $app->map('/read/:siteName(/:vsn)(/:type)', function ($siteName, $vsn = 'all', $type = 'all') {
                        $controller = new \Portal\Controllers\SiteInfoApiController($type, $siteName);
                        $controller->read(array('siteName' => $siteName, 'vsn' => $vsn));
                    })->via('GET', 'POST');

                    $app->post('/update(/:siteName(/:type))', function ($siteName, $type='all') use ($app) {
                        $json = $app->request->getBody();
                        $controller = new \Portal\Controllers\NodeInfoFormApiController($type, $siteName);
                        $controller->update($siteName, $json);
                    });
                });

                $app->get('/docs/:site', function($site) use($app){
                    $docs = array(
                        "docs" => array(
                            "ispecs" => glob(VZP_DIR . "/files/node/" . $site . "/ispec/*.*"),
                            "mops" => glob(VZP_DIR . "/files/node/" . $site . "/mops/*.*"),
                            "layouts" => glob(VZP_DIR . "/files/node/" . $site . "/layouts/*.*"),
                            "failovers" => glob(VZP_DIR . "/files/node/" . $site . "/failovers/*.*"),
                            "sitevisits" => glob(VZP_DIR . "/files/node/" . $site . "/sitevisits/*.*"),
                            "miscellaneous" => glob(VZP_DIR . "/files/node/" . $site . "/miscellaneous/*.*")
                        ),
                        "paths" => array(
                            "ispecs" => "/files/node/" . $site . "/ispec/",
                            "mops" => "/files/node/" . $site . "/mops/",
                            "layouts" => "/files/node/" . $site . "/layouts/",
                            "failovers" => "/files/node/" . $site . "/failovers/",
                            "sitevisits" => "/files/node/" . $site . "/sitevisits/",
                            "miscellaneous" => "/files/node/" . $site . "/miscellaneous/"
                        )
                    );
                    echo json_encode($docs);
                });

                $app->get('/download', function() use($app){
                    $json = json_decode($app->request->getBody());
                    $service = new NodeService();
                    echo json_encode($service->downloadNodeDoc($json));

                });


                $app->post('/docs/:action', function ($action) {
                    $controller = new \Portal\Controllers\NodeController();
                    $controller->docManipulation($action);
                });

                $app->get('/photos/:site', function($site) use($app){
                    $images = array(
                        "photos" => glob(VZP_DIR . "/files/node/" . $site . "/photos/thumb/*.*")
                    );
                    echo json_encode($images);
                });

                $app->post('/photos/upload', function(){
                    try {
                        $service = new NodeService();
                        echo json_encode($service->uploadPhoto());
                    }
                    catch(\Exception $e) {
                        echo json_encode("{'code': 500, 'message': 'unable to upload a photo. '" . $e->getMessage() . "}");
                    }
                });


                $app->post('/photos/delete', function(){
                    try {
                        $service = new NodeService();
                        echo json_encode($service->deletePhoto());
                    }
                    catch(\Exception $e) {
                        echo json_encode("{'code': 500, 'message': 'unable to delete a photo. '" . $e->getMessage() . "}");
                    }
                });

                $app->get('/photos/archive', function(){
                    try {
                        $service = new NodeService();
                        echo json_encode($service->archivePhotos());
                    }
                    catch(\Exception $e) {
                        echo json_encode("{'code': 500, 'message': 'unable to archive photos. '" . $e->getMessage() . "}");
                    }
                });

                $app->get('/diagrams/:site', function($site) use($app){
                    $diagram =  glob(VZP_DIR . "/files/node/" . $site . "/diagram/*.*");
                    echo json_encode($diagram);
                });

                $app->post('/diagrams/upload', function(){
                    try {
                        $service = new NodeService();
                        echo json_encode($service->uploadDiagram());
                    }
                    catch(\Exception $e) {
                        echo json_encode("{'code': 500, 'message': 'unable to upload a diagram. '" . $e->getMessage() . "}");
                    }
                });

                $app->get('/node/:site(/:filter)', function ($site, $filter = 'all') {
                    try {
                        $service = new NodeService($site, $filter);
                        echo json_encode($service->getNodeInformation($site, $filter));
                    }
                    catch(\Exception $e) {
                        echo json_encode("{'code': 500, 'message': 'unable to retrieve node information. '" . $e->getMessage() . "}");
                    }
                });

                $app->put('/node/:site', function ($site) use ($app) {
                    $json = $app->request->getBody();
                    // get portal controller
                    if($json)
                        $json = json_decode($json);
                    try {
                            $service = new NodeService($site, 'all');

                            $retVal = $service->updateNode($site, $json);
                            echo json_encode($retVal);
                        }
                        catch(\Exception $e) {
                            echo json_encode("{'code': 500, 'message': 'unable to update the site info. '" . $e->getMessage() . "}");
                        }
                });
            });
    }
}
