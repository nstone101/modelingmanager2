<?php

namespace Portal\Nodes;

class NodeService
{
    public function getNodeInformation($site, $type = 'all')
    {
        $type = ($type == null) ? 'all' : $type;
        $model = new \Portal\Models\ViewModels\SiteViewModel('SiteInfo', $site, $type);

        get_business_rules();
        //$_SESSION['rules']
        //$_SESSION['user']['image'] = get_user_image();
        date_default_timezone_set($_SESSION['rules']['DefaultTimeZone']);

        $_SESSION['help']['context'] = 1;
        //var_dump($model->read(array('siteName' => $site, 'vsn' => $type))[0]);
        $retVal = array(
            'node' => $model->read(array('siteName' => $site, 'vsn' => $type))[0],
            'nodeInfo' => $model,
            'help' => $_SESSION['help'],
            'rules' => $_SESSION['rules'],
            'siteName' => $site,
            'vsn' => $type
        );

        return $retVal;
    }

    public function getNodeBrowser($filter)
    {
        $retVal = new \stdClass();
        $retVal->prod = array('name' => 'prod', 'title' => 'Production (processing live traffic)', 'data' => get_node_names("Production"));
        $retVal->prodAddVSN = array('name' => 'prodAddVSN', 'title' => 'Production (adding a VSN)', 'data' => get_node_names("Production-Add-VSN"));
        $retVal->prodNoTraffic = array('name' => 'prodNoTraffic', 'title' => 'Pre-Production (no live traffic)', 'data' => get_node_names("Pre-Production"));
        $retVal->lab = array('name' => 'lab', 'title' => 'Lab', 'data' => get_node_names("Lab"));
        $retVal->deactivated = array('name' => 'deactivated', 'title' => 'Deactivated', 'data' => null);
        $retVal->filter = $filter;
        return $retVal;
    }

    public function getNewNodesBrowser($filter)
    {
        $retVal = new \stdClass();
        $retVal->newNode = array('name' => 'newNode', 'title' => 'New/Incomplete Nodes', 'data' => get_node_names("NewNode"));
        $retVal->filter = $filter;

        return $retVal;
    }


    public function dumpCompleteNode($sitename)
    {


         $siteName = strtoupper($sitename); // for the DB and new client filesystem
         $thedir = VZP_FILES . '/node/' . $siteName . '/miscellaneous';

         $siteInfoResults = array();
         try {

              $siteInfoQueryString  = "select * from SiteInfo  s where s.SiteName = '".$siteName."'; ";

                $siteInfoResults =  \ORM::for_table( 'SiteInfo' )
                ->raw_query( $siteInfoQueryString )
                ->find_array();

         } catch (\Exception $e) {
         }


         $mx9601Results = array();
         $mx9602Results = array();
         $ns54001Results = array();
         $ns54002Results = array();
         try {

              $mx9601QueryString  = "select * from MX960PortAssignments  m where m.SiteName = '".$siteName."' and m.MX = '1'; ";
              $mx9602QueryString  = "select * from MX960PortAssignments  m where m.SiteName = '".$siteName."' and m.MX = '2'; ";
              $ns54001QueryString = "select * from NS5400PortAssignments n where n.SiteName = '".$siteName."' and n.NS = '1'; ";
              $ns54002QueryString = "select * from NS5400PortAssignments n where n.SiteName = '".$siteName."' and n.NS = '2'; ";
          
                $mx9601Results =  \ORM::for_table( 'MX960PortAssignments' )
                ->raw_query( $mx9601QueryString )
                ->find_array();
                $mx9602Results =  \ORM::for_table( 'MX960PortAssignments' )
                ->raw_query( $mx9602QueryString )
                ->find_array();
          
                $ns54001Results =  \ORM::for_table( 'NS5400PortAssignments' )
                ->raw_query( $ns54001QueryString )
                ->find_array();
                $ns54002Results =  \ORM::for_table( 'NS5400PortAssignments' )
                ->raw_query( $ns54002QueryString )
                ->find_array();
         } catch (\Exception $e) {
         }


         try {

              $sitesIPSubnets = array();
              $sitesIPPublic = array();
              $sitesIPIDN = array();
              $sitesIPPrivate = array();
              $sitesNetworkConnections = array();
              $sitesCardSlotting = array();
              $sitesNotes = array();
              $sitesAsync = array();
              $sitesTDR = array();
              $sitesNodeDeviceNames = array();
              $sitesDNS = array();
              $sitesFWGoldenConfig = array();
              $sitesFWSetup = array();
              $sitesVPNRouting = array();
              $sitesCustomerCircuits = array();

              $ipSubnetsQueryString  = "select * from IPSchema  m where m.SiteName = '".$siteName."' ; ";
              $ipPublicQueryString  = "select * from IP_Public  m where m.SiteName = '".$siteName."' ; ";
              $ipIDNQueryString  = "select * from IP_IDN  m where m.SiteName = '".$siteName."' ; ";
              $ipPrivateQueryString  = "select * from IP_Private  m where m.SiteName = '".$siteName."' ; ";
              $networkConnQueryString  = "select * from NetworkConnections  m where m.SiteName = '".$siteName."' ; ";
              $cardSlotQueryString  = "select * from CardSlotting  m where m.SiteName = '".$siteName."' ; ";
              $notesQueryString  = "select * from NodeNotes  m where m.SiteName = '".$siteName."' ; ";
              $asyncQueryString  = "select * from Async  m where m.SiteName = '".$siteName."' ; ";
              $tdrQueryString  = "select * from TDR  m where m.SiteName = '".$siteName."' ; ";
              $nodeDeviceNamesQueryString  = "select * from NodeDeviceNames  m where m.SiteName = '".$siteName."' ; ";
              $dnsQueryString  = "select * from DNSTrapLog  m where m.SiteName = '".$siteName."' ; ";
              $fwgQueryString  = "select * from FWGoldenConfig  m where m.SiteName = '".$siteName."' ; ";
              $fwsQueryString  = "select * from FWSetup  m where m.SiteName = '".$siteName."' ; ";
              $vpnrQueryString  = "select * from VPNRouting  m where m.SiteName = '".$siteName."' ; ";
              $cuscirQueryString  = "select * from CustomerCircuits  m where m.SiteName = '".$siteName."' ; ";
          
              try {
                $sitesIPSubnets =  \ORM::for_table( 'IPSchema' )
                ->raw_query( $ipSubnetsQueryString )
                ->find_array();
              } catch ( Exception $em ) {
              }
          
              try {
                $sitesIPPublic =  \ORM::for_table( 'IP_Public' )
                ->raw_query( $ipPublicQueryString )
                ->find_array();
              } catch ( Exception $en ) {
              }
              try {
                $sitesIPIDN =  \ORM::for_table( 'IP_IDN' )
                ->raw_query( $ipIDNQueryString )
                ->find_array();
              } catch ( Exception $en ) {
              }
              try {
                $sitesIPPrivate =  \ORM::for_table( 'IP_Private' )
                ->raw_query( $ipPrivateQueryString )
                ->find_array();
              } catch ( Exception $en ) {
              }
              try {
                $sitesNetworkConnections =  \ORM::for_table( 'NetworkConnections' )
                ->raw_query( $networkConnQueryString )
                ->find_array();
              } catch ( Exception $en ) {
              }
              try {
                $sitesCardSlotting =  \ORM::for_table( 'CardSlotting' )
                ->raw_query( $cardSlotQueryString )
                ->find_array();
              } catch ( Exception $en ) {
              }
              try {
                $sitesNotes =  \ORM::for_table( 'NodeNotes' )
                ->raw_query( $notesQueryString )
                ->find_array();
              } catch ( Exception $en ) {
              }

              try {
                $sitesAsync =  \ORM::for_table( 'Async' )
                ->raw_query( $asyncQueryString )
                ->find_array();
              } catch ( Exception $en ) {
              }
              try {
                $sitesTDR =  \ORM::for_table( 'TDR' )
                ->raw_query( $tdrQueryString )
                ->find_array();
              } catch ( Exception $en ) {
              }
              try {
                $sitesNodeDeviceNames =  \ORM::for_table( 'NodeDeviceNames' )
                ->raw_query( $nodeDeviceNamesQueryString )
                ->find_array();
              } catch ( Exception $en ) {
              }
              try {
                $sitesDNS =  \ORM::for_table( 'DNSTrapLog' )
                ->raw_query( $dnsQueryString )
                ->find_array();
              } catch ( Exception $en ) {
              }
              try {
                $sitesFWGoldenConfig =  \ORM::for_table( 'FWGoldenConfig' )
                ->raw_query( $fwgQueryString )
                ->find_array();
              } catch ( Exception $en ) {
              }
              try {
                $sitesFWSetup =  \ORM::for_table( 'FWSetup' )
                ->raw_query( $fwsQueryString )
                ->find_array();
              } catch ( Exception $en ) {
              }
              try {
                $sitesVPNRouting =  \ORM::for_table( 'VPNRouting' )
                ->raw_query( $vpnrQueryString )
                ->find_array();
              } catch ( Exception $en ) {
              }
              try {
                $sitesCustomerCircuits =  \ORM::for_table( 'CustomerCircuits' )
                ->raw_query( $cuscirQueryString )
                ->find_array();
              } catch ( Exception $en ) {
              }
         } catch (\Exception $e) {
         }


















         try {
           exec ("rm " . $thedir . "/*.csv" );
           exec ("rm " . $thedir . "/*.xlsx" );
         } catch(\Exception $e){
         }









         try {

         file_put_contents("/Users/mrichardson/tmp/logs/excel.log", "getting new PHPExcel()", FILE_APPEND);
         file_put_contents("/Users/mrichardson/tmp/logs/excel.log", "\n", FILE_APPEND);

//           $objPHPExcel = new \Portal\PHPExcel\PHPExcel();
$objPHPExcel = new \PHPExcel();
$objPHPExcel->getProperties()->setCreator("VZ Portal")
							 ->setLastModifiedBy("VZ Portal Admin")
							 ->setTitle("PHPExcel Test Document")
							 ->setSubject("PHPExcel Test Document")
							 ->setDescription("Test document for PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Test result file");
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'SiteName')
            ->setCellValue('A2', 'SiteCode')
            ->setCellValue('A3', 'SiteMux')
            ->setCellValue('A4', 'ClliCode')
            ->setCellValue('A5', 'SiteAddress')
            ->setCellValue('A6', 'ContactName')
            ->setCellValue('A7', 'ContactPhone')
            ->setCellValue('A8', 'ContactEmail')
            ->setCellValue('A9', 'EMTS')
            ->setCellValue('A10', 'LongName')
            ->setCellValue('A11', 'SiteState')
            ->setCellValue('A12', 'Address1')
            ->setCellValue('A13', 'Address2')
            ->setCellValue('A14', 'MOP')
            ->setCellValue('A15', 'VSNMOP1')
            ->setCellValue('A16', 'VSNMOP2')
            ->setCellValue('A17', 'LLD')
            ->setCellValue('A18', 'SiteGroup');

               foreach ( $siteInfoResults as $siteinfo ) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B1', $siteName) 
            ->setCellValue('B2', $siteinfo['SiteCode'])
            ->setCellValue('B3', $siteinfo['SiteMux'])
            ->setCellValue('B4', $siteinfo['ClliCode'])
            ->setCellValue('B5', $siteinfo['SiteAddress'])
            ->setCellValue('B6', $siteinfo['ContactName'])
            ->setCellValue('B7', $siteinfo['ContactPhone'])
            ->setCellValue('B8', $siteinfo['ContactEmail'])
            ->setCellValue('B9', $siteinfo['EMTS'])
            ->setCellValue('B10', $siteinfo['LongName'])
            ->setCellValue('B11', $siteinfo['SiteState'])
            ->setCellValue('B12', $siteinfo['Address1'])
            ->setCellValue('B13', $siteinfo['Address2'])
            ->setCellValue('B14', basename($siteinfo['MOP']))
            ->setCellValue('B15', basename($siteinfo['VSNMOP1']))
            ->setCellValue('B16', basename($siteinfo['VSNMOP2']))
            ->setCellValue('B17', basename($siteinfo['LLD']))
            ->setCellValue('B18', $siteinfo['SiteGroup']);
}

//$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
//$objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->setTitle('Site Info');

$objPHPExcel->createSheet();

$objPHPExcel->setActiveSheetIndex(1)
            ->setCellValue('A1', 'MX')
            ->setCellValue('B1', 'DPC')
            ->setCellValue('C1', 'VSNInstance')
            ->setCellValue('D1', 'Interface')
            ->setCellValue('E1', 'SubInterface')
            ->setCellValue('F1', 'InterfaceTag')
            ->setCellValue('G1', 'PortType')
            ->setCellValue('H1', 'Layer')
            ->setCellValue('I1', 'Slot')
            ->setCellValue('J1', 'ConnectedDeviceType')
            ->setCellValue('K1', 'ConnectedDeviceName')
            ->setCellValue('L1', 'ConnectedDevicePort')
            ->setCellValue('M1', 'AddressTag')
            ->setCellValue('N1', 'IPAddress')
            ->setCellValue('O1', 'VLANTag')
            ->setCellValue('P1', 'SpeedDuplex')
            ->setCellValue('Q1', 'DescriptionTag')
            ->setCellValue('R1', 'Description')
            ->setCellValue('S1', 'Engine')
            ->setCellValue('T1', 'CardType')
            ->setCellValue('U1', 'VSNSAbbrev')
            ->setCellValue('V1', 'Hostname');


$objPHPExcel->getActiveSheet()->setTitle('Router 1');
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);

$rindex = 2;
               foreach ( $mx9601Results as $mx960 ) {


$objPHPExcel->setActiveSheetIndex(1)
            ->setCellValue('A'.$rindex, $mx960['MX'])
            ->setCellValue('B'.$rindex, $mx960['DPC'])
            ->setCellValue('C'.$rindex, $mx960['VSNInstance'])
            ->setCellValue('D'.$rindex, $mx960['Interface'])
            ->setCellValue('E'.$rindex, $mx960['SubInterface'])
            ->setCellValue('F'.$rindex, $mx960['InterfaceTag'])
            ->setCellValue('G'.$rindex, $mx960['PortType'])
            ->setCellValue('H'.$rindex, $mx960['Layer'])
            ->setCellValue('I'.$rindex, $mx960['Slot'])
            ->setCellValue('J'.$rindex, $mx960['ConnectedDeviceType'])
            ->setCellValue('K'.$rindex, $mx960['ConnectedDeviceName'])
            ->setCellValue('L'.$rindex, $mx960['ConnectedDevicePort'])
            ->setCellValue('M'.$rindex, $mx960['AddressTag'])
            ->setCellValue('N'.$rindex, $mx960['IPAddress'])
            ->setCellValue('O'.$rindex, $mx960['VLANTag'])
            ->setCellValue('P'.$rindex, $mx960['SpeedDuplex'])
            ->setCellValue('Q'.$rindex, $mx960['DescriptionTag'])
            ->setCellValue('R'.$rindex, $mx960['Description'])
            ->setCellValue('S'.$rindex, $mx960['Engine'])
            ->setCellValue('T'.$rindex, $mx960['CardType'])
            ->setCellValue('U'.$rindex, $mx960['VSNAbbrev'])
            ->setCellValue('V'.$rindex, $mx960['Hostname']);

$rindex = $rindex + 1;

               }



$objPHPExcel->createSheet();

$objPHPExcel->setActiveSheetIndex(2)
            ->setCellValue('A1', 'MX')
            ->setCellValue('B1', 'DPC')
            ->setCellValue('C1', 'VSNInstance')
            ->setCellValue('D1', 'Interface')
            ->setCellValue('E1', 'SubInterface')
            ->setCellValue('F1', 'InterfaceTag')
            ->setCellValue('G1', 'PortType')
            ->setCellValue('H1', 'Layer')
            ->setCellValue('I1', 'Slot')
            ->setCellValue('J1', 'ConnectedDeviceType')
            ->setCellValue('K1', 'ConnectedDeviceName')
            ->setCellValue('L1', 'ConnectedDevicePort')
            ->setCellValue('M1', 'AddressTag')
            ->setCellValue('N1', 'IPAddress')
            ->setCellValue('O1', 'VLANTag')
            ->setCellValue('P1', 'SpeedDuplex')
            ->setCellValue('Q1', 'DescriptionTag')
            ->setCellValue('R1', 'Description')
            ->setCellValue('S1', 'Engine')
            ->setCellValue('T1', 'CardType')
            ->setCellValue('U1', 'VSNSAbbrev')
            ->setCellValue('V1', 'Hostname');


$objPHPExcel->getActiveSheet()->setTitle('Router 2');
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);

$rindex = 2;
               foreach ( $mx9602Results as $mx960 ) {


$objPHPExcel->setActiveSheetIndex(2)
            ->setCellValue('A'.$rindex, $mx960['MX'])
            ->setCellValue('B'.$rindex, $mx960['DPC'])
            ->setCellValue('C'.$rindex, $mx960['VSNInstance'])
            ->setCellValue('D'.$rindex, $mx960['Interface'])
            ->setCellValue('E'.$rindex, $mx960['SubInterface'])
            ->setCellValue('F'.$rindex, $mx960['InterfaceTag'])
            ->setCellValue('G'.$rindex, $mx960['PortType'])
            ->setCellValue('H'.$rindex, $mx960['Layer'])
            ->setCellValue('I'.$rindex, $mx960['Slot'])
            ->setCellValue('J'.$rindex, $mx960['ConnectedDeviceType'])
            ->setCellValue('K'.$rindex, $mx960['ConnectedDeviceName'])
            ->setCellValue('L'.$rindex, $mx960['ConnectedDevicePort'])
            ->setCellValue('M'.$rindex, $mx960['AddressTag'])
            ->setCellValue('N'.$rindex, $mx960['IPAddress'])
            ->setCellValue('O'.$rindex, $mx960['VLANTag'])
            ->setCellValue('P'.$rindex, $mx960['SpeedDuplex'])
            ->setCellValue('Q'.$rindex, $mx960['DescriptionTag'])
            ->setCellValue('R'.$rindex, $mx960['Description'])
            ->setCellValue('S'.$rindex, $mx960['Engine'])
            ->setCellValue('T'.$rindex, $mx960['CardType'])
            ->setCellValue('U'.$rindex, $mx960['VSNAbbrev'])
            ->setCellValue('V'.$rindex, $mx960['Hostname']);

$rindex = $rindex + 1;

               }




$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(3)
            ->setCellValue('A1', 'NS')
            ->setCellValue('B1', 'Interface')
            ->setCellValue('C1', 'SubInterface')
            ->setCellValue('D1', 'PortType')
            ->setCellValue('E1', 'Layer')
            ->setCellValue('F1', 'Slot')
            ->setCellValue('G1', 'ConnectedDeviceType')
            ->setCellValue('H1', 'ConnedtedDeviceName')
            ->setCellValue('I1', 'ConnectedDevciePort')
            ->setCellValue('J1', 'AddressTag')
            ->setCellValue('K1', 'IPAddress')
            ->setCellValue('L1', 'VLANTag')
            ->setCellValue('M1', 'SpeedDuplex')
            ->setCellValue('N1', 'CardType')
            ->setCellValue('O1', 'Hostname')
            ->setCellValue('P1', 'Description');


$objPHPExcel->getActiveSheet()->setTitle('Firewall 1');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);

$rindex = 2;
               foreach ( $ns54001Results as $firew1 ) {


$objPHPExcel->setActiveSheetIndex(3)
            ->setCellValue('A'.$rindex, $firew1['NS'])
            ->setCellValue('B'.$rindex, $firew1['Interface'])
            ->setCellValue('C'.$rindex, $firew1['SubInterface'])
            ->setCellValue('D'.$rindex, $firew1['PortType'])
            ->setCellValue('E'.$rindex, $firew1['Layer'])
            ->setCellValue('F'.$rindex, $firew1['Slot'])
            ->setCellValue('G'.$rindex, $firew1['ConnectedDeviceType'])
            ->setCellValue('H'.$rindex, $firew1['ConnectedDeviceName'])
            ->setCellValue('I'.$rindex, $firew1['ConnectedDevicePort'])
            ->setCellValue('J'.$rindex, $firew1['AddressTag'])
            ->setCellValue('K'.$rindex, $firew1['IPAddress'])
            ->setCellValue('L'.$rindex, $firew1['VLANTag'])
            ->setCellValue('M'.$rindex, $firew1['SpeedDuplex'])
            ->setCellValue('N'.$rindex, $firew1['CardType'])
            ->setCellValue('O'.$rindex, $firew1['Hostname'])
            ->setCellValue('P'.$rindex, $firew1['Description']);

$rindex = $rindex + 1;

               }



$objPHPExcel->createSheet();

$objPHPExcel->setActiveSheetIndex(4)
            ->setCellValue('A1', 'NS')
            ->setCellValue('B1', 'Interface')
            ->setCellValue('C1', 'SubInterface')
            ->setCellValue('D1', 'PortType')
            ->setCellValue('E1', 'Layer')
            ->setCellValue('F1', 'Slot')
            ->setCellValue('G1', 'ConnectedDeviceType')
            ->setCellValue('H1', 'ConnedtedDeviceName')
            ->setCellValue('I1', 'ConnectedDevciePort')
            ->setCellValue('J1', 'AddressTag')
            ->setCellValue('K1', 'IPAddress')
            ->setCellValue('L1', 'VLANTag')
            ->setCellValue('M1', 'SpeedDuplex')
            ->setCellValue('N1', 'CardType')
            ->setCellValue('O1', 'Hostname')
            ->setCellValue('P1', 'Description');


$objPHPExcel->getActiveSheet()->setTitle('Firewall 2');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);

$rindex = 2;
               foreach ( $ns54002Results as $firew2 ) {


$objPHPExcel->setActiveSheetIndex(4)
            ->setCellValue('A'.$rindex, $firew2['NS'])
            ->setCellValue('B'.$rindex, $firew2['Interface'])
            ->setCellValue('C'.$rindex, $firew2['SubInterface'])
            ->setCellValue('D'.$rindex, $firew2['PortType'])
            ->setCellValue('E'.$rindex, $firew2['Layer'])
            ->setCellValue('F'.$rindex, $firew2['Slot'])
            ->setCellValue('G'.$rindex, $firew2['ConnectedDeviceType'])
            ->setCellValue('H'.$rindex, $firew2['ConnectedDeviceName'])
            ->setCellValue('I'.$rindex, $firew2['ConnectedDevicePort'])
            ->setCellValue('J'.$rindex, $firew2['AddressTag'])
            ->setCellValue('K'.$rindex, $firew2['IPAddress'])
            ->setCellValue('L'.$rindex, $firew2['VLANTag'])
            ->setCellValue('M'.$rindex, $firew2['SpeedDuplex'])
            ->setCellValue('N'.$rindex, $firew2['CardType'])
            ->setCellValue('O'.$rindex, $firew2['Hostname'])
            ->setCellValue('P'.$rindex, $firew2['Description']);

$rindex = $rindex + 1;

               }





$objPHPExcel->createSheet();

$objPHPExcel->setActiveSheetIndex(5)
            ->setCellValue('A1', 'Slot')
            ->setCellValue('B1', 'CardType')
            ->setCellValue('C1', 'InterfaceNum')
            ->setCellValue('D1', 'Device')
            ->setCellValue('E1', 'MIC')
            ->setCellValue('F1', 'Hostname');

$objPHPExcel->getActiveSheet()->setTitle('Card Slotting');

$rindex = 2;
               foreach ( $sitesCardSlotting as $cards ) {

$objPHPExcel->setActiveSheetIndex(5)
            ->setCellValue('A'.$rindex, $cards['Slot'])
            ->setCellValue('B'.$rindex, $cards['CardType'])
            ->setCellValue('C'.$rindex, $cards['InterfaceNum'])
            ->setCellValue('D'.$rindex, $cards['Device'])
            ->setCellValue('E'.$rindex, $cards['MIC'])
            ->setCellValue('F'.$rindex, $cards['Hostname']);

$rindex = $rindex + 1;

               }



$objPHPExcel->createSheet();

$objPHPExcel->setActiveSheetIndex(6)
            ->setCellValue('A1', 'NodeDevice')
            ->setCellValue('B1', 'NodeInterface')
            ->setCellValue('C1', 'InterfaceTag')
            ->setCellValue('D1', 'RemoteDeviceName')
            ->setCellValue('E1', 'Network')
            ->setCellValue('F1', 'Speed')
            ->setCellValue('G1', 'RemoteDeviceType')
            ->setCellValue('H1', 'RemoteDeviceInt')
            ->setCellValue('I1', 'RemoteDeviceCircuit')
            ->setCellValue('J1', 'Description');
$objPHPExcel->getActiveSheet()->setTitle('Network Connections');
$rindex = 2;
               foreach ( $sitesNetworkConnections as $netc ) {

$objPHPExcel->setActiveSheetIndex(6)
            ->setCellValue('A'.$rindex, $netc['NodeDevice'])
            ->setCellValue('B'.$rindex, $netc['NodeInterface'])
            ->setCellValue('C'.$rindex, $netc['InterfaceTag'])
            ->setCellValue('D'.$rindex, $netc['RemoteDeviceName'])
            ->setCellValue('E'.$rindex, $netc['Network'])
            ->setCellValue('F'.$rindex, $netc['Speed'])
            ->setCellValue('G'.$rindex, $netc['RemoteDeviceType'])
            ->setCellValue('H'.$rindex, $netc['RemoteDeviceInt'])
            ->setCellValue('I'.$rindex, $netc['RemoteDeviceCircuit'])
            ->setCellValue('J'.$rindex, $netc['Description']);

$rindex = $rindex + 1;

               }



$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(7)
            ->setCellValue('A1', 'NetworkType')
            ->setCellValue('B1', 'Name')
            ->setCellValue('C1', 'Subnet')
            ->setCellValue('D1', 'Netmask')
            ->setCellValue('E1', 'NumIPs')
            ->setCellValue('F1', 'Purpose')
            ->setCellValue('G1', 'VLAN')
            ->setCellValue('H1', 'MX960LogicalSystem')
            ->setCellValue('I1', 'MX960VirtualRouter')
            ->setCellValue('J1', 'MX960Interface')
            ->setCellValue('K1', 'FirewallVirtualRouter')
            ->setCellValue('L1', 'FirewallZone')
            ->setCellValue('M1', 'FirewallInterface')
            ->setCellValue('N1', 'VSNInstance')
            ->setCellValue('O1', 'Notes')
            ->setCellValue('P1', 'SubnetIntVal');
$objPHPExcel->getActiveSheet()->setTitle('IP Schema');
$rindex = 2;
               foreach ( $sitesIPSubnets as $ipschema ) {

$objPHPExcel->setActiveSheetIndex(7)
            ->setCellValue('A'.$rindex, $ipschema['NetworkType'])
            ->setCellValue('B'.$rindex, $ipschema['Name'])
            ->setCellValue('C'.$rindex, $ipschema['Subnet'])
            ->setCellValue('D'.$rindex, $ipschema['Netmask'])
            ->setCellValue('E'.$rindex, $ipschema['NumIPs'])
            ->setCellValue('F'.$rindex, $ipschema['Purpose'])
            ->setCellValue('G'.$rindex, $ipschema['VLAN'])
            ->setCellValue('H'.$rindex, $ipschema['MX960LogicalSystem'])
            ->setCellValue('I'.$rindex, $ipschema['MX960VirtualRouter'])
            ->setCellValue('J'.$rindex, $ipschema['MX960Interface'])
            ->setCellValue('K'.$rindex, $ipschema['FirewallVirtualRouter'])
            ->setCellValue('L'.$rindex, $ipschema['FirewallZone'])
            ->setCellValue('M'.$rindex, $ipschema['FirewallInterface'])
            ->setCellValue('N'.$rindex, $ipschema['VSNInstance'])
            ->setCellValue('O'.$rindex, $ipschema['Notes'])
            ->setCellValue('P'.$rindex, $ipschema['SubnetIntVal']);

$rindex = $rindex + 1;

               }


$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(8)
            ->setCellValue('A1', 'VLANName')
            ->setCellValue('B1', 'VLANID')
            ->setCellValue('C1', 'Subnet')
            ->setCellValue('D1', 'NetmaskTag')
            ->setCellValue('E1', 'Netmask')
            ->setCellValue('F1', 'SubnetMask')
            ->setCellValue('G1', 'IPOffset')
            ->setCellValue('H1', 'AddressTag')
            ->setCellValue('I1', 'IPAddress')
            ->setCellValue('J1', 'Device')
            ->setCellValue('K1', 'DNSName')
            ->setCellValue('L1', 'Purpose');
$objPHPExcel->getActiveSheet()->setTitle('IP Public');
$rindex = 2;
               foreach ( $sitesIPPublic as $ippub ) {

$objPHPExcel->setActiveSheetIndex(8)
            ->setCellValue('A'.$rindex, $ippub['VLANName'])
            ->setCellValue('B'.$rindex, $ippub['VLANID'])
            ->setCellValue('C'.$rindex, $ippub['Subnet'])
            ->setCellValue('D'.$rindex, $ippub['NetmaskTag'])
            ->setCellValue('E'.$rindex, $ippub['Netmask'])
            ->setCellValue('F'.$rindex, $ippub['SubnetMask'])
            ->setCellValue('G'.$rindex, $ippub['IPOffset'])
            ->setCellValue('H'.$rindex, $ippub['AddressTag'])
            ->setCellValue('I'.$rindex, $ippub['IPAddress'])
            ->setCellValue('J'.$rindex, $ippub['Device'])
            ->setCellValue('K'.$rindex, $ippub['DNSName'])
            ->setCellValue('L'.$rindex, $ippub['Purpose']);

$rindex = $rindex + 1;

               }

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(9)
            ->setCellValue('A1', 'VLANName')
            ->setCellValue('B1', 'VLANID')
            ->setCellValue('C1', 'Subnet')
            ->setCellValue('D1', 'NetmaskTag')
            ->setCellValue('E1', 'Netmask')
            ->setCellValue('F1', 'SubnetMask')
            ->setCellValue('G1', 'IPOffset')
            ->setCellValue('H1', 'AddressTag')
            ->setCellValue('I1', 'IPAddress')
            ->setCellValue('J1', 'Device')
            ->setCellValue('K1', 'DNSName')
            ->setCellValue('L1', 'Purpose');
$objPHPExcel->getActiveSheet()->setTitle('IP IDN');
$rindex = 2;
               foreach ( $sitesIPIDN as $ipidn ) {

$objPHPExcel->setActiveSheetIndex(9)
            ->setCellValue('A'.$rindex, $ipidn['VLANName'])
            ->setCellValue('B'.$rindex, $ipidn['VLANID'])
            ->setCellValue('C'.$rindex, $ipidn['Subnet'])
            ->setCellValue('D'.$rindex, $ipidn['NetmaskTag'])
            ->setCellValue('E'.$rindex, $ipidn['Netmask'])
            ->setCellValue('F'.$rindex, $ipidn['SubnetMask'])
            ->setCellValue('G'.$rindex, $ipidn['IPOffset'])
            ->setCellValue('H'.$rindex, $ipidn['AddressTag'])
            ->setCellValue('I'.$rindex, $ipidn['IPAddress'])
            ->setCellValue('J'.$rindex, $ipidn['Device'])
            ->setCellValue('K'.$rindex, $ipidn['DNSName'])
            ->setCellValue('L'.$rindex, $ipidn['Purpose']);

$rindex = $rindex + 1;

               }


$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(10)
            ->setCellValue('A1', 'VLANName')
            ->setCellValue('B1', 'VLANID')
            ->setCellValue('C1', 'Subnet')
            ->setCellValue('D1', 'NetmaskTag')
            ->setCellValue('E1', 'Netmask')
            ->setCellValue('F1', 'SubnetMask')
            ->setCellValue('G1', 'IPOffset')
            ->setCellValue('H1', 'AddressTag')
            ->setCellValue('I1', 'IPAddress')
            ->setCellValue('J1', 'Device')
            ->setCellValue('K1', 'DNSName')
            ->setCellValue('L1', 'Purpose');
$objPHPExcel->getActiveSheet()->setTitle('IP Private');
$rindex = 2;
               foreach ( $sitesIPPrivate as $ippriv ) {

$objPHPExcel->setActiveSheetIndex(10)
            ->setCellValue('A'.$rindex, $ippriv['VLANName'])
            ->setCellValue('B'.$rindex, $ippriv['VLANID'])
            ->setCellValue('C'.$rindex, $ippriv['Subnet'])
            ->setCellValue('D'.$rindex, $ippriv['NetmaskTag'])
            ->setCellValue('E'.$rindex, $ippriv['Netmask'])
            ->setCellValue('F'.$rindex, $ippriv['SubnetMask'])
            ->setCellValue('G'.$rindex, $ippriv['IPOffset'])
            ->setCellValue('H'.$rindex, $ippriv['AddressTag'])
            ->setCellValue('I'.$rindex, $ippriv['IPAddress'])
            ->setCellValue('J'.$rindex, $ippriv['Device'])
            ->setCellValue('K'.$rindex, $ippriv['DNSName'])
            ->setCellValue('L'.$rindex, $ippriv['Purpose']);

$rindex = $rindex + 1;

               }







$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(11)
            ->setCellValue('A1', 'NoteId')
            ->setCellValue('B1', 'Note')
            ->setCellValue('C1', 'EditedBy');
$objPHPExcel->getActiveSheet()->setTitle('Notes');
$rindex = 2;
               foreach ( $sitesNotes as $tnote ) {

$objPHPExcel->setActiveSheetIndex(11)
            ->setCellValue('A'.$rindex, $tnote['NoteId'])
            ->setCellValue('B'.$rindex, $tnote['Note'])
            ->setCellValue('C'.$rindex, $tnote['EditedBy']);

$rindex = $rindex + 1;

               }




$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(12)
            ->setCellValue('A1', 'Serial')
            ->setCellValue('B1', 'Bay')
            ->setCellValue('C1', 'Hostname')
            ->setCellValue('D1', 'ASYNC')
            ->setCellValue('E1', 'OSVersion')
            ->setCellValue('F1', 'Description');
$objPHPExcel->getActiveSheet()->setTitle('Async');
$rindex = 2;
               foreach ( $sitesAsync as $tasync ) {

$objPHPExcel->setActiveSheetIndex(12)
            ->setCellValue('A'.$rindex, $tasync['Serial'])
            ->setCellValue('B'.$rindex, $tasync['Bay'])
            ->setCellValue('C'.$rindex, $tasync['Hostname'])
            ->setCellValue('D'.$rindex, $tasync['ASYNC'])
            ->setCellValue('E'.$rindex, $tasync['OSVersion'])
            ->setCellValue('F'.$rindex, $tasync['Description']);

$rindex = $rindex + 1;

               }




$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(13)
            ->setCellValue('A1', 'Status')
            ->setCellValue('B1', 'SBC')
            ->setCellValue('C1', 'SBCPairName')
            ->setCellValue('D1', 'TDRServerName')
            ->setCellValue('E1', 'TDRServerIP')
            ->setCellValue('F1', 'Port')
            ->setCellValue('G1', 'IPs');
$objPHPExcel->getActiveSheet()->setTitle('TDR');
$rindex = 2;
               foreach ( $sitesTDR as $ttdr ) {

$objPHPExcel->setActiveSheetIndex(13)
            ->setCellValue('A'.$rindex, $ttdr['Status'])
            ->setCellValue('B'.$rindex, $ttdr['SBC'])
            ->setCellValue('C'.$rindex, $ttdr['SBCPairName'])
            ->setCellValue('D'.$rindex, $ttdr['TDRServerName'])
            ->setCellValue('E'.$rindex, $ttdr['TDRServerIP'])
            ->setCellValue('F'.$rindex, $ttdr['Port'])
            ->setCellValue('G'.$rindex, $ttdr['IPs']);

$rindex = $rindex + 1;

               }






$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(14)
            ->setCellValue('A1', 'PrimaryDeviceTag')
            ->setCellValue('B1', 'NodeDeviceName')
            ->setCellValue('C1', 'DeviceAbbreviation')
            ->setCellValue('D1', 'DeviceType')
            ->setCellValue('E1', 'DeviceDescription');
$objPHPExcel->getActiveSheet()->setTitle('Node Device Names');
$rindex = 2;
               foreach ( $sitesNodeDeviceNames as $tnodedn ) {

$objPHPExcel->setActiveSheetIndex(14)
            ->setCellValue('A'.$rindex, $tnodedn['PrimaryDeviceTag'])
            ->setCellValue('B'.$rindex, $tnodedn['NodeDeviceName'])
            ->setCellValue('C'.$rindex, $tnodedn['DeviceAbbreviation'])
            ->setCellValue('D'.$rindex, $tnodedn['DeviceType'])
            ->setCellValue('E'.$rindex, $tnodedn['DeviceDescription']);

$rindex = $rindex + 1;

               }




$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(15)
            ->setCellValue('A1', 'Function')
            ->setCellValue('B1', 'Tag')
            ->setCellValue('C1', 'HostIP')
            ->setCellValue('D1', 'Reference')
            ->setCellValue('E1', 'Name');
$objPHPExcel->getActiveSheet()->setTitle('DNS Trap Log');
$rindex = 2;
               foreach ( $sitesDNS as $tdns ) {

$objPHPExcel->setActiveSheetIndex(15)
            ->setCellValue('A'.$rindex, $tdns['Function'])
            ->setCellValue('B'.$rindex, $tdns['Tag'])
            ->setCellValue('C'.$rindex, $tdns['HostIP'])
            ->setCellValue('D'.$rindex, $tdns['Reference'])
            ->setCellValue('E'.$rindex, $tdns['Name']);

$rindex = $rindex + 1;

               }




$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(16)
            ->setCellValue('A1', 'FNo')
            ->setCellValue('B1', 'FId')
            ->setCellValue('C1', 'FromZone')
            ->setCellValue('D1', 'Source')
            ->setCellValue('E1', 'ToZone')
            ->setCellValue('F1', 'Destination')
            ->setCellValue('G1', 'Service')
            ->setCellValue('H1', 'FWAction')
            ->setCellValue('I1', 'InstallOn')
            ->setCellValue('J1', 'RuleOptions');
$objPHPExcel->getActiveSheet()->setTitle('FW Golden Config');
$rindex = 2;
               foreach ( $sitesFWGoldenConfig as $tfwg ) {

$objPHPExcel->setActiveSheetIndex(16)
            ->setCellValue('A'.$rindex, $tfwg['FNo'])
            ->setCellValue('B'.$rindex, $tfwg['FId'])
            ->setCellValue('C'.$rindex, $tfwg['FromZone'])
            ->setCellValue('D'.$rindex, $tfwg['Source'])
            ->setCellValue('E'.$rindex, $tfwg['ToZone'])
            ->setCellValue('F'.$rindex, $tfwg['Destination'])
            ->setCellValue('G'.$rindex, $tfwg['Service'])
            ->setCellValue('H'.$rindex, $tfwg['FWAction'])
            ->setCellValue('I'.$rindex, $tfwg['InstallOn'])
            ->setCellValue('J'.$rindex, $tfwg['RuleOptions']);

$rindex = $rindex + 1;

               }


$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(17)
            ->setCellValue('A1', 'A')
            ->setCellValue('B1', 'B')
            ->setCellValue('C1', 'C')
            ->setCellValue('D1', 'D')
            ->setCellValue('E1', 'E')
            ->setCellValue('F1', 'F')
            ->setCellValue('G1', 'G')
            ->setCellValue('H1', 'H')
            ->setCellValue('I1', 'I')
            ->setCellValue('J1', 'J')
            ->setCellValue('K1', 'K')
            ->setCellValue('L1', 'L')
            ->setCellValue('M1', 'M')
            ->setCellValue('N1', 'N')
            ->setCellValue('O1', 'O')
            ->setCellValue('P1', 'P')
            ->setCellValue('Q1', 'Q')
            ->setCellValue('R1', 'R')
            ->setCellValue('S1', 'S')
            ->setCellValue('T1', 'T')
            ->setCellValue('U1', 'U')
            ->setCellValue('V1', 'V')
            ->setCellValue('W1', 'W')
            ->setCellValue('X1', 'X')
            ->setCellValue('Y1', 'Y')
            ->setCellValue('Z1', 'Z');
$objPHPExcel->getActiveSheet()->setTitle('FW Setup');
$rindex = 2;
               foreach ( $sitesFWSetup as $tfws ) {

$objPHPExcel->setActiveSheetIndex(17)
            ->setCellValue('A'.$rindex, $tfws['A'])
            ->setCellValue('B'.$rindex, $tfws['B'])
            ->setCellValue('C'.$rindex, $tfws['C'])
            ->setCellValue('D'.$rindex, $tfws['D'])
            ->setCellValue('E'.$rindex, $tfws['E'])
            ->setCellValue('F'.$rindex, $tfws['F'])
            ->setCellValue('G'.$rindex, $tfws['G'])
            ->setCellValue('H'.$rindex, $tfws['H'])
            ->setCellValue('I'.$rindex, $tfws['I'])
            ->setCellValue('J'.$rindex, $tfws['J'])
            ->setCellValue('K'.$rindex, $tfws['K'])
            ->setCellValue('L'.$rindex, $tfws['L'])
            ->setCellValue('M'.$rindex, $tfws['M'])
            ->setCellValue('N'.$rindex, $tfws['N'])
            ->setCellValue('O'.$rindex, $tfws['O'])
            ->setCellValue('P'.$rindex, $tfws['P'])
            ->setCellValue('Q'.$rindex, $tfws['Q'])
            ->setCellValue('R'.$rindex, $tfws['R'])
            ->setCellValue('S'.$rindex, $tfws['S'])
            ->setCellValue('T'.$rindex, $tfws['T'])
            ->setCellValue('U'.$rindex, $tfws['U'])
            ->setCellValue('V'.$rindex, $tfws['V'])
            ->setCellValue('W'.$rindex, $tfws['W'])
            ->setCellValue('X'.$rindex, $tfws['X'])
            ->setCellValue('Y'.$rindex, $tfws['Y'])
            ->setCellValue('Z'.$rindex, $tfws['Z']);

$rindex = $rindex + 1;

               }

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(18)
            ->setCellValue('A1', 'VPNConfigName')
            ->setCellValue('B1', 'MainBranch')
            ->setCellValue('C1', 'RemoteNode')
            ->setCellValue('D1', 'RemoteGatewayName')
            ->setCellValue('E1', 'RemoteSubnets')
            ->setCellValue('F1', 'TunnelInt')
            ->setCellValue('G1', 'LocalGwyIp')
            ->setCellValue('H1', 'TunnelZone')
            ->setCellValue('I1', 'TerminationPhyInt')
            ->setCellValue('J1', 'TerminationPhyIntZone')
            ->setCellValue('K1', 'PhyIntVRouter')
            ->setCellValue('L1', 'GwyParms')
            ->setCellValue('M1', 'AutoKeyIKEParms')
            ->setCellValue('N1', 'PreSharedKey');
$objPHPExcel->getActiveSheet()->setTitle('VPN Routing');
$rindex = 2;
               foreach ( $sitesVPNRouting as $tvpnr ) {

$objPHPExcel->setActiveSheetIndex(18)
            ->setCellValue('A'.$rindex, $tvpnr['VPNConfigName'])
            ->setCellValue('B'.$rindex, $tvpnr['MainBranch'])
            ->setCellValue('C'.$rindex, $tvpnr['RemoteNode'])
            ->setCellValue('D'.$rindex, $tvpnr['RemoteGatewayName'])
            ->setCellValue('E'.$rindex, $tvpnr['RemoteSubnets'])
            ->setCellValue('F'.$rindex, $tvpnr['TunnelInt'])
            ->setCellValue('G'.$rindex, $tvpnr['LocalGwyIp'])
            ->setCellValue('H'.$rindex, $tvpnr['TunnelZone'])
            ->setCellValue('I'.$rindex, $tvpnr['TerminationPhyInt'])
            ->setCellValue('J'.$rindex, $tvpnr['TerminationPhyIntZone'])
            ->setCellValue('K'.$rindex, $tvpnr['PhyIntVRouter'])
            ->setCellValue('L'.$rindex, $tvpnr['GwyParms'])
            ->setCellValue('M'.$rindex, $tvpnr['AutoKeyIKEParms'])
            ->setCellValue('N'.$rindex, $tvpnr['PreSharedKey']);

$rindex = $rindex + 1;

               }

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(18)
            ->setCellValue('A1', 'CustomerCktType')
            ->setCellValue('B1', 'Speed')
            ->setCellValue('C1', 'CDR')
            ->setCellValue('D1', 'CircuitID')
            ->setCellValue('E1', 'MX960Router')
            ->setCellValue('F1', 'RemoteRouter');
$objPHPExcel->getActiveSheet()->setTitle('Customer Circuits');
$rindex = 2;
               foreach ( $sitesCustomerCircuits as $tcusc ) {

$objPHPExcel->setActiveSheetIndex(18)
            ->setCellValue('A'.$rindex, $tcusc['CustomerCktType'])
            ->setCellValue('B'.$rindex, $tcusc['Speed'])
            ->setCellValue('C'.$rindex, $tcusc['CDR'])
            ->setCellValue('D'.$rindex, $tcusc['CircuitID'])
            ->setCellValue('E'.$rindex, $tcusc['MX960Router'])
            ->setCellValue('F'.$rindex, $tcusc['RemoteRouter']);

$rindex = $rindex + 1;

               }






$objPHPExcel->setActiveSheetIndex(0);

         file_put_contents("/Users/mrichardson/tmp/logs/excel.log", "got new PHPExcel()", FILE_APPEND);
         file_put_contents("/Users/mrichardson/tmp/logs/excel.log", "\n", FILE_APPEND);

         file_put_contents("/Users/mrichardson/tmp/logs/excel.log", "now get a writer...", FILE_APPEND);
         file_put_contents("/Users/mrichardson/tmp/logs/excel.log", "\n", FILE_APPEND);

//$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2013');
$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

         file_put_contents("/Users/mrichardson/tmp/logs/excel.log", "got a PHPExcel_IOFactory()", FILE_APPEND);
         file_put_contents("/Users/mrichardson/tmp/logs/excel.log", "\n", FILE_APPEND);
$objWriter->save($thedir . "/" . $siteName .".xlsx");
         file_put_contents("/Users/mrichardson/tmp/logs/excel.log", "saving......". $thedir . "/" . $siteName .".xlsx", FILE_APPEND);
         file_put_contents("/Users/mrichardson/tmp/logs/excel.log", "\n", FILE_APPEND);




         } catch(\Exception $e){

         file_put_contents("/Users/mrichardson/tmp/logs/excel.log", "exception new PHPExcel()", FILE_APPEND);
         file_put_contents("/Users/mrichardson/tmp/logs/excel.log", "\n", FILE_APPEND);
         file_put_contents("/Users/mrichardson/tmp/logs/excel.log", $e->getMessage(), FILE_APPEND);
         file_put_contents("/Users/mrichardson/tmp/logs/excel.log", "\n", FILE_APPEND);

         }


        try {

        $sqlStrmx1 = "SELECT concat(SiteName , ',' , VSN, ',', MX, ',', DPC, ',', VSNInstance, ',', Interface, ',', SubInterface, ',', InterfaceTag, ',', PortType, ',', Layer, ',', Slot, ',',         ConnectedDeviceType, ',', ConnectedDeviceName, ',', ConnectedDevicePort, ',', AddressTag, ',',         IPAddress, ',', VLANTag, ',', SpeedDuplex, ',', DescriptionTag, ',', Description, ',',           Engine, ',', CardType, ',', VSNAbbrev, ',', Hostname ) FROM MX960PortAssignments mx WHERE   mx.SiteName = '" . $siteName . "';'";
        $testitmx1 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $sqlStrmx1 )
        ->find_array();

    foreach ( $testitmx1 as $mx ) {
//         file_put_contents($thedir . "/MX960portassignments.csv", implode(PHP_EOL, $mx), FILE_APPEND);
         file_put_contents($thedir . "/MX960portassignments.csv", implode($mx), FILE_APPEND);
         file_put_contents($thedir . "/MX960portassignments.csv", PHP_EOL, FILE_APPEND);
    }

        $sqlStrns1 = "SELECT concat(SiteName , ',' , VSN, ',', NS, ',', Interface, ',', SubInterface, ',',  PortType, ',', Layer, ',', Slot, ',',  ConnectedDeviceType, ',', ConnectedDeviceName, ',', ConnectedDevicePort, ',', AddressTag, ',', IPAddress, ',', VLANTag, ',', SpeedDuplex, ',', Description, ',', CardType, ',', Hostname ) FROM NS5400PortAssignments ns WHERE   ns.SiteName = '" . $siteName . "';'";
        $testitns1 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $sqlStrns1 )
        ->find_array();

    foreach ( $testitns1 as $ns ) {
         file_put_contents($thedir . "/NS5400portassignments.csv", implode($ns), FILE_APPEND);
         file_put_contents($thedir . "/NS5400portassignments.csv", PHP_EOL, FILE_APPEND);
    }

        $sqlStr1 = "SELECT concat(SiteName , ',' , Slot, ',', CardType, ',', InterfaceNum, ',', Device, ',',  MIC, ',', Hostname ) FROM CardSlotting cs WHERE   cs.SiteName = '" . $siteName . "';'";
        $testit1 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $sqlStr1 )
        ->find_array();

    foreach ( $testit1 as $cs ) {
         file_put_contents($thedir . "/CardSlotting.csv", implode($cs), FILE_APPEND);
         file_put_contents($thedir . "/CardSlotting.csv", PHP_EOL, FILE_APPEND);
    }


        $sqlStr1 = "SELECT concat(SiteName , ',',  VSN, ',', NodeDevice, ',', NodeInterface, ',', InterfaceTag, ',', RemoteDeviceName, ',',  Network, ',', Speed, ',', RemoteDeviceType, ',', RemoteDeviceInt, ',', RemoteDeviceCircuit, ',', Description  ) FROM NetworkConnections nc WHERE   nc.SiteName = '" . $siteName . "';'";
        $testit1 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $sqlStr1 )
        ->find_array();

    foreach ( $testit1 as $nc ) {
         file_put_contents($thedir . "/NetworkConnections.csv", implode($nc), FILE_APPEND);
         file_put_contents($thedir . "/NetworkConnections.csv", PHP_EOL, FILE_APPEND);
    }

        $sqlStripsc1 = "SELECT concat(SiteName , ',' , VSN, ',',  NetworkType, ',', Name, ',', Subnet, ',', Netmask, ',',  NumIPs, ',', Purpose, ',', VLAN, ',', IFNULL(MX960LogicalSystem,\"\") , ',', IFNULL(MX960VirtualRouter,\"\"), ',', IFNULL(MX960Interface,\"\"), ',', IFNULL(FirewallVirtualRouter,\"\"), ',', IFNULL(FirewallZone,\"\"), ',', IFNULL(FirewallInterface,\"\"), ',', VSNInstance, ',', Notes ) FROM IPSchema ips WHERE   ips.SiteName = '" . $siteName . "';'";
        $testitipsc1 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $sqlStripsc1 )
        ->find_array();

    foreach ( $testitipsc1 as $ipsc ) {
         file_put_contents($thedir . "/IPSchema.csv", implode($ipsc), FILE_APPEND);
         file_put_contents($thedir . "/IPSchema.csv", PHP_EOL, FILE_APPEND);
    }


        $sqlStripp1 = "SELECT concat(SiteName , ',' , VSN, ',',  VLANName, ',', VLANID, ',', Subnet, ',', NetmaskTag, ',',  Netmask, ',', SubnetMask, ',', IPOffset, ',', IFNULL(AddressTag,\"\") , ',', IFNULL(IPAddress,\"\"), ',', IFNULL(Device,\"\"), ',', IFNULL(DNSName,\"\"), ',', IFNULL(Purpose,\"\") ) FROM IP_Public ipp WHERE   ipp.SiteName = '" . $siteName . "';'";
        $testitipp1 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $sqlStripp1 )
        ->find_array();

    foreach ( $testitipp1 as $ipp ) {
         file_put_contents($thedir . "/IP_Public.csv", implode($ipp), FILE_APPEND);
         file_put_contents($thedir . "/IP_Public.csv", PHP_EOL, FILE_APPEND);
    }



        $sqlStrippr1 = "SELECT concat(SiteName , ',' , VSN, ',',  VLANName, ',', VLANID, ',', Subnet, ',', NetmaskTag, ',',  Netmask, ',', SubnetMask, ',', IPOffset, ',', IFNULL(AddressTag,\"\") , ',', IFNULL(IPAddress,\"\"), ',', IFNULL(Device,\"\"), ',', IFNULL(DNSName,\"\"), ',', IFNULL(Purpose,\"\") ) FROM IP_Private ippr WHERE   ippr.SiteName = '" . $siteName . "';'";
        $testitippr1 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $sqlStrippr1 )
        ->find_array();

    foreach ( $testitippr1 as $ippr ) {
         file_put_contents($thedir . "/IP_Private.csv", implode($ippr), FILE_APPEND);
         file_put_contents($thedir . "/IP_Private.csv", PHP_EOL, FILE_APPEND);
    }


        $sqlStripidn1 = "SELECT concat(SiteName , ',' , VSN, ',',  VLANName, ',', VLANID, ',', Subnet, ',', NetmaskTag, ',',  Netmask, ',', SubnetMask, ',', IPOffset, ',', IFNULL(AddressTag,\"\") , ',', IFNULL(IPAddress,\"\"), ',', IFNULL(Device,\"\"), ',', IFNULL(DNSName,\"\"), ',', IFNULL(Purpose,\"\") ) FROM IP_IDN ipidn WHERE   ipidn.SiteName = '" . $siteName . "';'";
        $testitipidn1 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $sqlStripidn1 )
        ->find_array();

    foreach ( $testitipidn1 as $ipidn ) {
         file_put_contents($thedir . "/IP_IDN.csv", implode($ipidn), FILE_APPEND);
         file_put_contents($thedir . "/IP_IDN.csv", PHP_EOL, FILE_APPEND);
    }



        $sqlStrndns1 = "SELECT concat(SiteName , ',' , VSN, ',',  PrimaryDeviceTag, ',', NodeDeviceName, ',', DeviceAbbreviation, ',', DeviceType, ',',  DeviceDescription ) FROM NodeDeviceNames ndns WHERE   ndns.SiteName = '" . $siteName . "';'";
        $testitndns1 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $sqlStrndns1 )
        ->find_array();

    foreach ( $testitndns1 as $ndns ) {
         file_put_contents($thedir . "/NodeDeviceNames.csv", implode($ndns), FILE_APPEND);
         file_put_contents($thedir . "/NodeDeviceNames.csv", PHP_EOL, FILE_APPEND);
    }


        $sqlStrca1 = "SELECT concat(SiteName , ',' , VSN, ',',  SPIP, ',', HostOffset, ',', IPAddress, ',', Device, ',',  Description ) FROM CustomerAddressing ca WHERE   ca.SiteName = '" . $siteName . "';'";
        $testitca1 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $sqlStrca1 )
        ->find_array();

    foreach ( $testitca1 as $ca ) {
         file_put_contents($thedir . "/CustomerAddressing.csv", implode($ca), FILE_APPEND);
         file_put_contents($thedir . "/CustomerAddressing.csv", PHP_EOL, FILE_APPEND);
    }

        $sqlStrcc1 = "SELECT concat(SiteName , ',' , VSN, ',',  CustomerCktType, ',', Speed, ',', CDR, ',', CircuitID, ',',  MX960Router, ',', RemoteRouter ) FROM CustomerCircuits cc WHERE   cc.SiteName = '" . $siteName . "';'";
        $testitcc1 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $sqlStrcc1 )
        ->find_array();

    foreach ( $testitcc1 as $cc ) {
         file_put_contents($thedir . "/CustomerCircuits.csv", implode($cc), FILE_APPEND);
         file_put_contents($thedir . "/CustomerCircuits.csv", PHP_EOL, FILE_APPEND);
    }

        $sqlStrdnst1 = "SELECT concat(SiteName , ',' , VSN, ',',  IFNULL(Function,\"\"), ',', IFNULL(Tag,\"\"), ',', IFNULL(HostIP,\"\"), ',', IFNULL(Reference,\"\"), ',',  IFNULL(Name,\"\") ) FROM DNSTrapLog dnst WHERE   dnst.SiteName = '" . $siteName . "';'";
        $testitdnst1 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $sqlStrdnst1 )
        ->find_array();

    foreach ( $testitdnst1 as $dnst ) {
         file_put_contents($thedir . "/DNSTrapLog.csv", implode($dnst), FILE_APPEND);
         file_put_contents($thedir . "/DNSTrapLog.csv", PHP_EOL, FILE_APPEND);
    }


        $sqlStrfwg1 = "SELECT concat(SiteName , ',' , VSN, ',',  FNo, ',', FId, ',', FromZone, ',', Source, ',',  ToZone, ',', Destination, ',', Service, ',', IFNULL(FWAction,\"\") , ',', IFNULL(InstallOn,\"\"), ',', IFNULL(RuleOptions,\"\") ) FROM FWGoldenConfig fwg WHERE   fwg.SiteName = '" . $siteName . "';'";
        $testitfwg1 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $sqlStrfwg1 )
        ->find_array();

    foreach ( $testitfwg1 as $fwg ) {
         file_put_contents($thedir . "/FWGoldenConfig.csv", implode($fwg), FILE_APPEND);
         file_put_contents($thedir . "/FWGoldenConfig.csv", PHP_EOL, FILE_APPEND);
    }


        $sqlStrfws1 = "SELECT concat(SiteName , ',' , VSN, ',',  IFNULL(A, \"\"), ',', IFNULL(B, \"\"), ',', IFNULL(C, \"\"), ',', IFNULL(D, \"\"), ',', IFNULL(E, \"\"), ',', IFNULL(F, \"\"), ',', IFNULL(G, \"\"), ',', IFNULL(H, \"\"), ',', IFNULL(I, \"\"), ',', IFNULL(J, \"\"), ',', IFNULL(K, \"\"), ',', IFNULL(L, \"\"), ',', IFNULL(M, \"\"), ',', IFNULL(N, \"\"), ',', IFNULL(O, \"\"), ',', IFNULL(P, \"\"), ',', IFNULL(Q, \"\"), ',', IFNULL(R, \"\"), ',', IFNULL(S, \"\"), ',', IFNULL(T, \"\"), ',', IFNULL(U, \"\"), ',', IFNULL(V, \"\"), ',', IFNULL(W, \"\"), ',', IFNULL(X, \"\"), ',', IFNULL(Y, \"\"), ',', IFNULL(Z,\"\")  ) FROM FWSetup fws WHERE   fws.SiteName = '" . $siteName . "';'";
        $testitfws1 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $sqlStrfws1 )
        ->find_array();

    foreach ( $testitfws1 as $fws ) {
         file_put_contents($thedir . "/FWSetup.csv", implode($fws), FILE_APPEND);
         file_put_contents($thedir . "/FWSetup.csv", PHP_EOL, FILE_APPEND);
    }

        $sqlStrvpnr1 = "SELECT concat(SiteName , ',' , VSN, ',',  IFNULL(VPNConfigName, \"\"), ',', IFNULL(MainBranch, \"\"), ',', IFNULL(RemoteNode, \"\"), ',', IFNULL(RemoteGatewayName, \"\"), ',', IFNULL(RemoteSubnets, \"\"), ',', IFNULL(TunnelInt, \"\"), ',', IFNULL(LocalGwyIp, \"\"), ',', IFNULL(TunnelZone, \"\"), ',', IFNULL(TerminationPhyInt, \"\"), ',', IFNULL(TerminationPhyIntZone, \"\"), ',', IFNULL(PhyIntVRouter, \"\"), ',', IFNULL(GwyParms, \"\"), ',', IFNULL(AutoKeyIKEParms, \"\"), ',', IFNULL(PreSharedKey, \"\") ) FROM VPNRouting vpnr WHERE   vpnr.SiteName = '" . $siteName . "';'";
        $testitvpnr1 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $sqlStrvpnr1 )
        ->find_array();

    foreach ( $testitvpnr1 as $vpnr ) {
         file_put_contents($thedir . "/VPNRouting.csv", implode($vpnr), FILE_APPEND);
         file_put_contents($thedir . "/VPNRouting.csv", PHP_EOL, FILE_APPEND);
    }


         } catch(\Exception $e){


         }
//         $sqlStr = "SELECT * FROM MX960PortAssignments mx WHERE   mx.SiteName = '" . $siteName . "' INTO OUTFILE '" . $thedir . "/MX960portassignments.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'; "
//          . "SELECT * FROM NS5400PortAssignments mx WHERE   mx.SiteName = '" . $siteName . "' INTO OUTFILE '" . $thedir . "/NS5400portassignments.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'; "
//          . "SELECT * FROM CardSlotting mx WHERE   mx.SiteName = '" . $siteName . "' INTO OUTFILE '" . $thedir . "/CardSlotting.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'; "
//          . "SELECT * FROM NetworkConnections mx WHERE   mx.SiteName = '" . $siteName . "' INTO OUTFILE '" . $thedir . "/NetworkConnections.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'; "
//          . "SELECT * FROM IPSchema mx WHERE   mx.SiteName = '" . $siteName . "' INTO OUTFILE '" . $thedir . "/IPSchema.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'; "
//          . "SELECT * FROM IP_Public mx WHERE   mx.SiteName = '" . $siteName . "' INTO OUTFILE '" . $thedir . "/IP_Public.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'; "
//          . "SELECT * FROM IP_IDN mx WHERE   mx.SiteName = '" . $siteName . "' INTO OUTFILE '" . $thedir . "/IP_IDN.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'; "
//          . "SELECT * FROM IP_Private mx WHERE   mx.SiteName = '" . $siteName . "' INTO OUTFILE '" . $thedir . "/IP_Private.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'; "
//          . "SELECT * FROM NodeDeviceNames mx WHERE   mx.SiteName = '" . $siteName . "' INTO OUTFILE '" . $thedir . "/NodeDeviceNames.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'; "
//          . "SELECT * FROM CustomerAddressing mx WHERE   mx.SiteName = '" . $siteName . "' INTO OUTFILE '" . $thedir . "/CustomerAddressing.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'; "
//          . "SELECT * FROM CustomerCircuits mx WHERE   mx.SiteName = '" . $siteName . "' INTO OUTFILE '" . $thedir . "/CustomerCircuits.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'; "
//          . "SELECT * FROM DNSTrapLog mx WHERE   mx.SiteName = '" . $siteName . "' INTO OUTFILE '" . $thedir . "/DNSTrapLog.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'; "
//          . "SELECT * FROM FWGoldenConfig mx WHERE   mx.SiteName = '" . $siteName . "' INTO OUTFILE '" . $thedir . "/FWGoldenConfig.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'; "
//          . "SELECT * FROM FWSetup mx WHERE   mx.SiteName = '" . $siteName . "' INTO OUTFILE '" . $thedir . "/FWSetup.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'; "
//          . "SELECT * FROM VPNRouting mx WHERE   mx.SiteName = '" . $siteName . "' INTO OUTFILE '" . $thedir . "/VPNRouting.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'; " ;
//
//         $testit =  \ORM::for_table( 'DUAL' )
//                     ->raw_execute( $sqlStr );

    }


    public function createNewNode($sitename, $sitetype)
    {

         $siteName = strtoupper($sitename); // for the DB and new client filesystem

         $thedir = VZP_FILES . '/node/' . $siteName . '/layouts';
         if (!file_exists($thedir)) {
           mkdir($thedir, 0777, true);
         } 

         $thedir = VZP_FILES . '/node/' . $siteName . '/miscellaneouss';
         if (!file_exists($thedir)) {
           mkdir($thedir, 0777, true);
         }
         $thedir = VZP_FILES . '/node/' . $siteName . '/miscellaneous';
         if (!file_exists($thedir)) {
           mkdir($thedir, 0777, true);
         }

         $thedir = VZP_FILES . '/node/' . $siteName . '/photos';
         if (!file_exists($thedir)) {
           mkdir($thedir, 0777, true);
         }

         $thedir = VZP_FILES . '/node/' . $siteName . '/photos/thumb';
         if (!file_exists($thedir)) {
           mkdir($thedir, 0777, true);
         }

         $thedir = VZP_FILES . '/node/' . $siteName . '/photos/full';
         if (!file_exists($thedir)) {
           mkdir($thedir, 0777, true);
         }

         $thedir = VZP_FILES . '/node/' . $siteName . '/photos/vsd';
         if (!file_exists($thedir)) {
           mkdir($thedir, 0777, true);
         }

         $thedir = VZP_FILES . '/node/' . $siteName . '/sitevisits';
         if (!file_exists($thedir)) {
           mkdir($thedir, 0777, true);
         }

         $thedir = VZP_FILES . '/node/' . $siteName . '/configfiles';
         if (!file_exists($thedir)) {
           mkdir($thedir, 0777, true);
         }

         $thedir = VZP_FILES . '/node/' . $siteName . '/failovers';
         if (!file_exists($thedir)) {
           mkdir($thedir, 0777, true);
         }

         $thedir = VZP_FILES . '/node/' . $siteName . '/ispec';
         if (!file_exists($thedir)) {
           mkdir($thedir, 0777, true);
         }

         $thedir = VZP_FILES . '/node/' . $siteName . '/mops';
         if (!file_exists($thedir)) {
           mkdir($thedir, 0777, true);
         }

         $thedir = VZP_FILES . '/node/' . $siteName . '/diagram';
         if (!file_exists($thedir)) {
           mkdir($thedir, 0777, true);
         }
        
          if (strcmp($sitetype, "2.0PIP") == 0) {
              $lVSN = "PIP NNI";
              $lGenType = "2.0";
              $lSiteName = "Model2_PIPNNI";
              
          } else if (strcmp($sitetype, "2.0GB") == 0) {
              $lVSN = "GENBAND";
              $lGenType = "2.0";
              $lSiteName = "Model2_GENBAND";
              
          } else if (strcmp($sitetype, "3.0VX") == 0) {
              $lVSN = "VX";
              $lGenType = "3.0";
              $lSiteName = "Model3_VX";
              
          } else if (strcmp($sitetype, "2.0MX") == 0) {
              $lVSN = "2.0";
              $lGenType = "2.0";
              $lSiteName = "Model2_MX";
          } else {
             return 0;
          }

/*
$msg = "call createNewNode('" . $sitename . "', 'PIP NNI')";
var_dump($msg);
var_dump("dumped msg...");
$testit =  \ORM::for_table( 'DUAL' )
  ->raw_execute( $msg );
*/


/*
 having issues with calling a stored procedure - using raw query for now
 */

if (strcmp($lSiteName, "Model2_MX") == 0) {


          $sqlStr = "insert into SiteInfo (`SiteName`, `VSN`, `SiteGroup`, `GenType`, `SiteState`) values ('". $siteName . "', '" . $lVSN . "', 'NewNode', '" . $lGenType . "', 'Node-Device-Config') ; " .
          "insert into VSN (`SiteName`, `VSN`) values ('". $siteName . "', '" . $lGenType . "') ; " .
          "insert into MX960PortAssignments ( `SiteName`, `VSN`, `MX`, `DPC`, `VSNInstance`, `Interface`, `SubInterface`, `InterfaceTag`, `PortType`, `Layer`, `Slot`, `ConnectedDeviceType`, `ConnectedDeviceName`, `ConnectedDevicePort`, `AddressTag`, `IPAddress`, `VLANTag`, `SpeedDuplex`, `DescriptionTag`, `Description`, `Engine` )
                  select '" . $siteName . "', `VSN`, `MX`, `DPC`, `VSNInstance`, `Interface`, `SubInterface`, `InterfaceTag`, `PortType`, `Layer`, `Slot`, `ConnectedDeviceType`, `ConnectedDeviceName`, `ConnectedDevicePort`, `AddressTag`, `IPAddress`, `VLANTag`, `SpeedDuplex`, `DescriptionTag`, `Description`, `Engine` 
                  from Model_MX960PortAssignments 
                  where SiteName = '" . $lSiteName . "' ; "  


          ;

file_put_contents("/Users/mrichardson/tmp/logs/mx.txt", $sqlStr, FILE_APPEND);

          $testit =  \ORM::for_table( 'DUAL' )
                     ->raw_execute( $sqlStr );


} else {
          $sqlStr = "insert into SiteInfo (`SiteName`, `VSN`, `SiteGroup`, `GenType`, `SiteState`) values ('". $siteName . "', '" . $lVSN . "', 'NewNode', '" . $lGenType . "', 'Node-Device-Config') ; " .
          "insert into VSN (`SiteName`, `VSN`) values ('". $siteName . "', '" . $lGenType . "') ; " .
          "insert into NodeDeviceNames (`SiteName`, `VSN`, `PrimaryDeviceTag`, `NodeDeviceName`, `DeviceAbbreviation`, `DeviceType`, `DeviceDescription`) 
                  select '" . $siteName . "', `VSN`, `PrimaryDeviceTag`, CONCAT('" . $siteName . "',`DeviceAbbreviation`), `DeviceAbbreviation`, `DeviceType`, `DeviceDescription` from Model_NodeDeviceNames
                  where SiteName = '" . $lSiteName . "' ; " .
          "insert into Async (`SiteName`, `VSN`, `Serial`, `Bay`, `Hostname`, `Description`, `ASYNC`, `OSVersion` )
                  select '" . $siteName . "', `VSN`, `Serial`, `Bay`, `Hostname`, `Description`, `ASYNC`, `OSVersion`
                  from Model_Async
                  where SiteName = '" . $lSiteName . "' ; "  .
          "insert into CardSlotting (`SiteName`, `Slot`, `CardType`, `InterfaceNum`, `Device`, `MIC`)
                  select '" . $siteName . "', `Slot`, `CardType`, `InterfaceNum`, `Device`, `MIC` 
                  from Model_CardSlotting
                  where  SiteName = '" . $lSiteName . "' ; " .
          "insert into DNSTrapLog (`SiteName`, `VSN`, `Function`, `Tag`, `HostIP`, `Reference`, `Name`)
                  select '" . $siteName . "', `VSN`, `Function`, `Tag`, `HostIP`, `Reference`, `Name` 
                  from Model_DNSTrapLog
                  where SiteName = '" . $lSiteName . "' ; " .
          "insert into IPSchema (`SiteName`, `VSN`, `NetworkType`, `Name`, `Subnet`, `Netmask`, `NumIPs`, `Purpose`, `VLAN`, `MX960LogicalSystem`, `MX960VirtualRouter`, `MX960Interface`, `FirewallVirtualRouter`, `FirewallZone`, `FirewallInterface`, `VSNInstance`, `Notes`)
                  select '" . $siteName . "', `VSN`, `NetworkType`, `Name`, `Subnet`, `Netmask`, `NumIPs`, `Purpose`, `VLAN`, `MX960LogicalSystem`, `MX960VirtualRouter`, `MX960Interface`, `FirewallVirtualRouter`, `FirewallZone`, `FirewallInterface`, `VSNInstance`, `Notes`
                  from Model_IPSchema 
                  where SiteName = '" . $lSiteName . "' ; "  .
          "insert into CustomerAddressing (`SiteName`, `VSN`, `SPIP`, `HostOffset`, `IPAddress`, `Device`, `Description`)
                  select '" . $siteName . "', `VSN`, `SPIP`, `HostOffset`, `IPAddress`, `Device`, `Description` 
                  from  Model_CustomerAddressing 
                  where SiteName = '" . $lSiteName . "' ; "  .
          "insert into CustomerCircuits (`SiteName`, `VSN`, `CustomerCktType`, `Speed`, `CDR`, `CircuitID`, `MX960Router`, `RemoteRouter`)
                  select '" . $siteName . "', `VSN`, `CustomerCktType`, `Speed`, `CDR`, `CircuitID`, `MX960Router`, `RemoteRouter`
                  from Model_CustomerCircuits
                  where SiteName = '" . $lSiteName . "' ; "  .
          "insert into IP_Public (`SiteName`, `VSN`, `VLANName`, `VLANID`, `Subnet`, `NetmaskTag`, `Netmask`, `SubnetMask`, `IPOffset`, `AddressTag`, `IPAddress`, `Device`, `DNSName`, `Purpose`)
                  select '" . $siteName . "', `VSN`, `VLANName`, `VLANID`, `Subnet`, `NetmaskTag`, `Netmask`, `SubnetMask`, `IPOffset`, `AddressTag`, `IPAddress`, `Device`, `DNSName`, `Purpose`
                  from Model_IP_Public
                  where SiteName = '" . $lSiteName . "' ; "  .
          "insert into IP_IDN (`SiteName`, `VSN`, `VLANName`, `VLANID`, `Subnet`, `NetmaskTag`, `Netmask`, `SubnetMask`, `IPOffset`, `AddressTag`, `IPAddress`, `Device`, `DNSName`, `Purpose`)
                  select '" . $siteName . "', `VSN`, `VLANName`, `VLANID`, `Subnet`, `NetmaskTag`, `Netmask`, `SubnetMask`, `IPOffset`, `AddressTag`, `IPAddress`, `Device`, `DNSName`, `Purpose`
                  from Model_IP_IDN
                  where SiteName = '" . $lSiteName . "' ; "  .
          "insert into IP_Private (`SiteName`, `VSN`, `VLANName`, `VLANID`, `Subnet`, `NetmaskTag`, `Netmask`, `SubnetMask`, `IPOffset`, `AddressTag`, `IPAddress`, `Device`, `DNSName`, `Purpose`)
                  select '" . $siteName . "', `VSN`, `VLANName`, `VLANID`, `Subnet`, `NetmaskTag`, `Netmask`, `SubnetMask`, `IPOffset`, `AddressTag`, `IPAddress`, `Device`, `DNSName`, `Purpose`
                  from Model_IP_Private
                  where SiteName = '" . $lSiteName . "' ; "  .
          "insert into MX960PortAssignments ( `SiteName`, `VSN`, `MX`, `DPC`, `VSNInstance`, `Interface`, `SubInterface`, `InterfaceTag`, `PortType`, `Layer`, `Slot`, `ConnectedDeviceType`, `ConnectedDeviceName`, `ConnectedDevicePort`, `AddressTag`, `IPAddress`, `VLANTag`, `SpeedDuplex`, `DescriptionTag`, `Description`, `Engine` )
                  select '" . $siteName . "', `VSN`, `MX`, `DPC`, `VSNInstance`, `Interface`, `SubInterface`, `InterfaceTag`, `PortType`, `Layer`, `Slot`, `ConnectedDeviceType`, `ConnectedDeviceName`, `ConnectedDevicePort`, `AddressTag`, `IPAddress`, `VLANTag`, `SpeedDuplex`, `DescriptionTag`, `Description`, `Engine` 
                  from Model_MX960PortAssignments 
                  where SiteName = '" . $lSiteName . "' ; "  .
          "insert into NetworkConnections  (`SiteName`,`VSN`, `NodeDevice`, `NodeInterface`, `InterfaceTag`, `RemoteDeviceName`, `Network`, `Speed`, `RemoteDeviceType`, `RemoteDeviceInt`, `RemoteDeviceCircuit`, `Description`)
                  select '" . $siteName . "', `VSN`, `NodeDevice`, `NodeInterface`, `InterfaceTag`, `RemoteDeviceName`, `Network`, `Speed`, `RemoteDeviceType`, `RemoteDeviceInt`, `RemoteDeviceCircuit`, `Description`
                  from Model_NetworkConnections
                  where SiteName = '" . $lSiteName . "' ; "  .
          "insert into NS5400PortAssignments (`SiteName`, `VSN`, `NS`, `Interface`, `SubInterface`, `PortType`, `Layer`, `Slot`, `ConnectedDeviceType`, `ConnectedDeviceName`, `ConnectedDevicePort`, `AddressTag`, `IPAddress`, `VLANTag`, `SpeedDuplex`, `Description`)
                  select '" . $siteName . "', `VSN`, `NS`, `Interface`, `SubInterface`, `PortType`, `Layer`, `Slot`, `ConnectedDeviceType`, `ConnectedDeviceName`, `ConnectedDevicePort`, `AddressTag`, `IPAddress`, `VLANTag`, `SpeedDuplex`, `Description`
                  from Model_NS5400PortAssignments 
                  where SiteName = '" . $lSiteName . "' ; "  .
          "insert into TDR ( `SiteName`, `VSN`, `SBC`, `Status`, `SBCPairName`, `TDRServerName`, `TDRServerIP`, `Port`, `IPs` )
                  select '" . $siteName . "', `VSN`, `SBC`, `Status`, `SBCPairName`, `TDRServerName`, `TDRServerIP`, `Port`, `IPs`
                  from Model_TDR
                  where SiteName = '" . $lSiteName . "' ; "  


          ;

          $testit =  \ORM::for_table( 'DUAL' )
                     ->raw_execute( $sqlStr );

}

        return 0;
    }


    public function updateNode($site, $data)
    {
        // update the node info
        unset($data->id);
        $model = new \Portal\Models\ViewModels\SiteViewModel('SiteInfo', $site, 'all');
        return $model->update((array)$data, $site);
    }

    public function uploadPhoto()
    {
        $copy_dir = VZP_FILES . '/node/' . $_POST['siteName'] . '/photos/full/';
        if (!file_exists($copy_dir)) {
            mkdir($copy_dir, 0777, true);
        }
        $thumb_dir = VZP_FILES . '/node/' . $_POST['siteName'] . '/photos/thumb/';
        if (!file_exists($thumb_dir)) {
            mkdir($thumb_dir, 0777, true);
        }

        if (!file_exists(VZP_LOG . '/' . $_POST['siteName'])) {
            mkdir(VZP_LOG . '/' . $_POST['siteName'], 0777, true);
        }

        for ($x = 0; $x <= count($_FILES['file']['tmp_name']) - 1; $x++) {
            if (!move_uploaded_file($_FILES['file']['tmp_name'][$x], $copy_dir . "/" . $_FILES['file']['name'][$x])) {
                $log_file = fopen(VZP_LOG . '/' . $_POST['siteName'] . '_photo_audit.log', 'a+');
                fputs($log_file, $_FILES['file']['tmp_name'][$x] . " upload failed\n");
                echo "error";
            } else {
                // Create thumbnail of newly uploaded photo.
                $image = new \Portal\ImageResize($copy_dir . $_FILES['file']['name'][$x]);
                $image->resizeToWidth(260);
                $image->save($thumb_dir . $_FILES['file']['name'][$x]);
                $log_file = fopen(VZP_LOG . '/' . $_POST['siteName'] . '_photo_audit.log', 'a+');
                $log_string = date("Y-m-d G:i e") . " | " . $_SERVER['REMOTE_ADDR'] . " | " . $_SESSION['user']['username'] . " uploaded " . $_POST['name'] . " '" . $_FILES['file']['name'][$x] . "'\n";
                fputs($log_file, $log_string);
                fputs($log_file, $_FILES['file']['tmp_name'][$x] . " upload complete\n");
                echo "success";
            }
        }
    }

    public function uploadDiagram(){

        $copy_dir = VZP_FILES . '/node/' . $_POST['siteName'] . '/diagram/';
        if (!file_exists($copy_dir)) {
            mkdir($copy_dir, 0777, true);
        }

        if (!file_exists(VZP_LOG . '/' . $_POST['siteName'])) {
            mkdir(VZP_LOG . '/' . $_POST['siteName'], 0777, true);
        }

        if (!move_uploaded_file($_FILES['file']['tmp_name'], $copy_dir . "/diagram.png")) {
            $log_file = fopen(VZP_LOG . '/' . $_POST['siteName'] . '_diagram_audit.log', 'a+');
            fputs($log_file, $_FILES['file']['tmp_name'] . " upload failed\n");
            echo "error";
        } else {
            // Create thumbnail of newly uploaded photo.
            $log_file = fopen(VZP_LOG . '/' . $_POST['siteName'] . '_diagram_audit.log', 'a+');
            $log_string = date("Y-m-d G:i e") . " | " . $_SERVER['REMOTE_ADDR'] . " | " . $_SESSION['user']['username'] . " uploaded " . $_POST['name'] . " '" . $_FILES['file']['name'] . "'\n";
            fputs($log_file, $log_string);
            fputs($log_file, $_FILES['file']['tmp_name'] . " upload complete\n");
            echo "success";
        }
    }

    public function deletePhoto()
    {

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        //var_dump($request);

        $log_file = fopen(VZP_LOG . '/' . $request->siteName . $request->logFile, 'a+');

        if (!unlink($request->filename)) {
            echo "error";
        } else {
            $log_string = date("Y-m-d G:i e") . " | " . $_SERVER['REMOTE_ADDR'] . " | " . $_SESSION['user']['username'] . " deleted " . $request->name . " '" . basename($request->doc) . "'\n";
            fputs($log_file, $log_string);
            echo "success";
        }
    }

    public function archivePhotos()
    {
        // create zip of all site photos that are not a thumbnail
        $photos = rglob(VZP_FILES . '/node/' . $_SESSION['siteName'] . '/photos/full/*.*', 0, true);
        $temp_file = tempnam(VZP_TMP, $_SESSION['siteName']);
        $zip = create_zip($photos, $temp_file, true);

        // return the location of the zip file.
        if ($zip) {
            echo basename($temp_file);
        }
    }

    public function getNodeTable($params) {
        $params = (object)$params;
        $retVal = new \stdClass();
        $conditions = array();

        if(isset($params->siteName) && $params->siteName != null) {
            $conditions['SiteName'] = strtoupper($params->siteName);
        }

        if (isset($params->vsn) && ($params->vsn != 'all' && $params->vsn != null)) {
            $conditions['VSN'] = $params->vsn;
        }

        $query = \ORM::for_table($params->tableName);

        if(isset($params->id) && $params->id != null) {

           return $query->findOne($params->id)->asArray();
        }

        if(count($conditions) > 0) {
            return $query->where($conditions)->findArray();
        }
        else {
            return $query->findArray();
        }
    }

    public function createNodeTable($params) {
        $table = $params->tableName;
        unset($params->tableName);
        $retVal = new \stdClass();
        $query = \ORM::for_table($table)->create();
        if($this->saveOrmObject($query, $params) === true) {
            $params->id = $query->id();
            $result = \ORM::for_table($table)->findOne($query->id());
            $retVal->result = $result->asArray();
        }
        else {
            $retVal->result = array();
        }
        return $retVal->result;
    }

    public function updateNodeTable($id, $params){
        $table = $params->tableName;
        unset($params->tableName);
        $retVal = new \stdClass();
        $query = \ORM::for_table($table)->findOne($id);
        if($this->saveOrmObject($query, $params) === true) {
            $params->id = $query->id();
            $result = \ORM::for_table($table)->findOne($query->id());
            $retVal->result = $result->asArray();
        }
        else {
            $retVal->result = array();
        }
        return $retVal->result;
    }

    public function deleteNodeTableByIds($params) {
        $query = \ORM::for_table($params['tableName'])->whereIn('id', $params['ids'])->deleteMany();
        return $query;
    }

    public function deleteNodeTableById($id, $tableName) {
        $query = \ORM::for_table($tableName)->findOne($id);
        if ($query !== false) {
            $query->delete();
        }
        return $query;
    }

    /**
     * @param $object
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    protected function saveOrmObject($object, $params){
        try {
            $params = (array)$params;
            $object->set($params);
            return $object->save();
        }
        catch(\Exception $e){
            var_dump('sdkljlkdsfj;kldsf');
            var_dump($e);
            throw $e;
        }
    }

    public function downloadNodeDoc($params)
    {
          // get here, but with bad data and/or returning bad data
    }

}
