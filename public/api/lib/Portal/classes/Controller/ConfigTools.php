<?php

Namespace Portal\Controller;

class ConfigTools {

   function config_mgr_controller() {
   	//TODO: update $cwd to use /tmp/ instead of /temp/user/, by using the tempnam() function. 
   	//Eliminates the need for creating and cleaning out the temp user folders.
   
   	/* This page is used to manage templates, whether it be uploading new ones, deleting existing ones, or runing the
   	 * tagify script on a MOP and inserting the new template appropriately.
   	 */
   
   	page_header();
   
   	//initialize variables
   	$siteTypes = array();
   
   	// This only builds the siteTypes array if the POST for uploading or removing a template hasn't been done,
   	// since it will be built in those stanzas.
   	if (empty($_POST['delete-submit']) && empty($_POST['upload-submit']) && empty($_POST['tagify-submit'])) {
   
   		// This builds an array $siteTypes, which will be an array of arrays.
   	    // The first item of each inner array is the VSN type,
   	    // Each item inside the inner array after the first is a template name in that directory.
   	    foreach (glob(VZP_DIR . '/files/templates/*', GLOB_ONLYDIR) as $key => $type) {
   	        $siteTypes[$key][] = basename($type);
   	        foreach(glob($type . '/*.txt') as $template) {
   	            $siteTypes[$key][] = $template;
   	        }
   	    }
   	} ?>
       <div id="content">
           <div id="content-header">
               <h1>Template Manager and Config Builder</h1>
           </div>
           <div class="container-fluid">
               <?php // This will output the widget to let the user know the status of the template upload or deletion.
               if (!empty($_POST['upload-submit'])){ // UPLOAD WIDGET OUTPUT
                   // Prints out the divs to start the upload output widget box.
                   echo '<div class="row-fluid">
                       <div class="span12">
                           <div class="widget-box">
                               <div class="widget-content">
                                   <div class="row-fluid">
                                       <div class="span12">';
   
                   // Builds the targetPath for the uploaded file for where the template will be deposited.
                   $targetPath = VZP_DIR . '/files/templates/' . $_POST['UploadTypeSelect'] . '/';
                   // Adds the file name to the target path, while replacing the extention with lowercase 'txt', 
                   // since that is what we expect in the config_builder_script.
                   $targetPath .= substr(basename($_FILES['TemplateUpload']['name']), 0, strlen(basename($_FILES['TemplateUpload']['name']))-3) . "txt";
   
                   echo $targetPath . '<br />';
   
                   // attempts to move the uploaded template.
                   if(move_uploaded_file($_FILES['TemplateUpload']['tmp_name'], $targetPath)) {
                       echo "The file <b>".  basename( $_FILES['TemplateUpload']['name']). "</b> has been uploaded into the <b>".$_POST['UploadTypeSelect']."</b> folder.";
                   } else {
                       echo "There was an error uploading the file, please try again!";
                   }
   
                   // Build the siteTypes array with the uploaded templates
                   foreach (glob(VZP_DIR . '/files/templates/*', GLOB_ONLYDIR) as $key => $type) {
                       $siteTypes[$key][] = basename($type);
                       foreach(glob($type . '/*.txt') as $template) {
                           $siteTypes[$key][] = $template;
                       }
                   }
   
                   echo '</div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>';
               } else if (!empty($_POST['delete-submit'])) { // DELETE WIDGET OUTPUT
                   // Prints out the divs to start the delete output widget box.
                   echo '<div class="row-fluid">
                       <div class="span12">
                           <div class="widget-box">
                               <div class="widget-content">
                                   <div class="row-fluid">
                                       <div class="span12">';
   
                   // Loop through and delete each of the files checked.
                   foreach ($_POST as $key => $template) {
                       // Avoid the last entry, as it won't be a template, it will be the 'delete-submit' value.
                       if ($template != 'Delete' ) {
                           // Try to unlink each template.
                           if(unlink($template)) {
                               echo "The file '".  basename($template). "' has been removed from the ". end(explode('/',dirname($template))) ." folder. <br />";
                           } else{
                               echo "There was an error removing the file: ". basename($template) .", please try again!<br />";
                           }
                       }
                   }
                   echo "All files have been deleted. <br />";
   
                   // Build the siteTypes array without the deleted templates
                   foreach (glob(VZP_DIR . '/files/templates/*', GLOB_ONLYDIR) as $key => $type) {
                       $siteTypes[$key][] = basename($type);
                       foreach(glob($type . '/*.txt') as $template) {
                           $siteTypes[$key][] = $template;
                       }
                   }
   
                   echo '</div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>';
               }
               // This is the tagify script that runs if they posted a mop to tagify.
               else if (!empty($_POST['tagify-submit'])){ // TAGIFY WIDGET OUTPUT
                   // Prints out the divs to start the script output widget box.
                   echo '<div class="row-fluid">
                   <div class="span12">
                       <div class="widget-box">
                           <div class="widget-title"><span class="icon"><icon class="fa fa-asterisk"></icon></span><h5>Tagify Script Output</h5></div>
                           <div class="widget-content">
                               <div class="row-fluid">
                                   <div class="span12">';
                   // initialize variables
                   $template = "";
                   $MX01 = "";
                   $MX02 = "";
   
                   $cwd = '/temp/' . $_SESSION['user']['username']; // set the cwd to their username.
                   // remove any temporary files in their username folder:
                   exec("rm -R $cwd/*");
                   echo 'All previous temporary files have been removed.<br /><br />';
   
                   mkdir($cwd,0777); // make the directory with their username
                   // Create Directory to build Templates under their username.
                   if (!is_dir($cwd . "/template")){
                       if (!mkdir($cwd . "/template")) {
                           echo ('Failed to create folder \'template\' in the user\'s temp folder.');
                       }
                   }
   
                   // This checks if the uploaded file was .doc or .docx and runs the python script to acquire a .txt file.
                   if (preg_match('/.docx$/',$_FILES['mopUpload']['name'])){
                       $output = array(); // initialize the output array for the python script output.
   
                       echo "The temporary docx being used is: ".$_FILES['mopUpload']['tmp_name'] . "<br />";
                       echo "The docx file name is: ".$_FILES['mopUpload']['name'] . "<br /><br />";
   
                       // set up the command for the python script
                       $command = 'python /var/www/python/py-docx/example-extracttext.py \''.$_FILES['mopUpload']['tmp_name'].'\' '.$cwd.'/MOP.txt';
                       echo "The command being run is: \"".$command . "\"<br /><br />";
   
                       // run the python script to turn the docx into a .txt file.
                       exec($command,$output);
   
                       if (!$output) {
                           echo 'The docx file was <b>successfully</b> converted into a txt for parsing.<br />Beginning to read the file for MX01 / MX02 sections.<br />';
   
                           //Create MOP files for both MXs, removing the .docx extention, adding .txt
                           $MX01 = fopen("$cwd/template/MX01_" . substr($_FILES['mopUpload']['name'],0,(strlen($_FILES['mopUpload']['name'])-5)) . ".txt", "w+") or exit("Unable to create MX01 file!");
                           $MX02 = fopen("$cwd/template/MX02_" . substr($_FILES['mopUpload']['name'],0,(strlen($_FILES['mopUpload']['name'])-5)) . ".txt", "w+") or exit("Unable to create MX02 file!");
   
                           // Open the new txt file for parsing.
                           $template = file("$cwd/MOP.txt");
                       }
                       else {
                           echo "The docx file could not be converted into a txt for parsing, the error from the python script was: ". var_dump($output) . "<br />";
                       }
                   } else if (preg_match('/.doc$/',$_FILES['mopUpload']['name'])) {
                       echo "This script cannot accept a .doc file. Please save it as a .docx or export it from Word into a .txt file.";
                   } else { // handles if the uploaded file was .txt
   
                       echo "The temporary text file being used is: ".$_FILES['mopUpload']['tmp_name'] . "<br />";
                       echo "The text file name is: ".$_FILES['mopUpload']['name'] . "<br /><br />";
                       echo "Reading the text MOP file for MX01 / MX02 sections:<br/>";
   
                       //Create MOP files for both MXs
                       $MX01 = fopen("$cwd/template/MX01_" . substr(basename($_FILES['mopUpload']['name']), 0, strlen($_FILES['mopUpload']['name'])-3) . 'txt', "w+") or exit("Unable to create MX01 file!");
                       $MX02 = fopen("$cwd/template/MX02_" . substr(basename($_FILES['mopUpload']['name']), 0, strlen($_FILES['mopUpload']['name'])-3) . 'txt', "w+") or exit("Unable to create MX02 file!");
   
                       // Open the txt file for parsing.
                       $template = file($_FILES['mopUpload']['tmp_name']);
                   }
   
                   // Define variables to determine if we are handling MX01 or MX02
                   $MX01Bool = false;
                   $MX02Bool = false;
                   $flagLine = false;
                   echo "If there are any rows in the table below that have more or less than four columns, there is a missing Start or Stop TAG.<br/><br/>";
                   // Initialize the table
                   echo '<table class="table table-bordered table-hover"><th>MX01 Starts</th><th>MX01 Stops</th><th>MX02 Starts</th><th>MX02 Stops</th><tbody><tr>';
                   foreach($template as $row => $data) { // Loops through the MOP, finding mx01/mx02 start and stops.
                       if (strpos($data, "MX01_START")) {
                           echo "<td>Line ". $row ." - Found MX01_START</td>";
                           $flagLine = true;
                           $MX01Bool = true;
                       } else if (strpos($data, "MX02_START")){
                           echo "<td>Line ". $row ." - Found MX02_START</td>";
                           $flagLine = true;
                           $MX02Bool = true;
                       } else if (strpos($data, "MX01_STOP")){
                           echo "<td>Line ". $row ." - Found MX01_STOP</td>";
                           $flagLine = true;
                           $MX01Bool = false;
                       } else if (strpos($data, "MX02_STOP")){
                           echo "<td>Line ". $row ." - Found MX02_STOP</td></tr>";
                           $flagLine = true;
                           $MX02Bool = false;
                       }
                       //
                       if (!$flagLine && $MX01Bool){
                           if (!fwrite( $MX01, $data)) {
                               exit ("Cannot write to file ".$MX01."<br />");
                           }
                       }
   
                       if (!$flagLine && $MX02Bool){
                           if (!fwrite( $MX02, $data)) {
                               exit ("Cannot write to file ".$MX02."<br />");
                           }
                       }
                       $flagLine = false;
                   }
                   echo "</tbody></table><br />";
                   echo "Finished reading the MOP. Moving into template directory: <b>". $_POST['TagifyTypeSelect'] ."</b><br />";
   
                   // build the destination folder based on the type they chose.
                   $dest = VZP_DIR . '/files/templates/'. $_POST['TagifyTypeSelect'] .'/';
   
                   // move the template files into the VSN they chose.
                   foreach (glob("$cwd/template/*.txt") as $filename) {
                       if (!rename($filename,$dest . basename($filename))){
                           echo "Failed to move file: <b>" . $filename ."</b><br />";
                       } else {
                           echo "Moved file: <b>" . basename($filename) ."</b><br />";
                       }
                   }
                   echo "<br />Finished moving all template files.";
   
                   // Build the siteTypes array with the new templates that were created.
                   foreach (glob(VZP_DIR.'/files/templates/*', GLOB_ONLYDIR) as $key => $type) {
                       $siteTypes[$key][] = basename($type);
                       foreach(glob($type . '/*.txt') as $template) {
                           $siteTypes[$key][] = $template;
                       }
                   }
   
                   // Close out the Div's for the script output widget.
                   echo '</div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>';
               } ?>
               <div class="row-fluid">
                   <div class="span12">
                       <div class="widget-box">
                           <div class="widget-title">
                               <ul class="nav nav-tabs">
   								<?php if (!empty($_POST['upload-submit'])) {
   									echo "<li class=''>";
   								} else {
   									echo "<li class='active'>";
   								} ?>
   								<a data-toggle="tab" href="#TagifyTab"><icon class="fa fa-wrench"></icon> Generate Template From a MOP</a></li>
                                   <li class=""><a data-toggle="tab" href="#MXConfigTab"><icon class="fa fa-clock-o"></icon> Generate MX Configs</a></li>
                                   <li class=""><a data-toggle="tab" href="#DeleteTab"><icon class="fa fa-trash"></icon> Delete Templates</a></li>
   								<?php if (!empty($_POST['upload-submit'])) {
   									echo "<li class='active'>";
   								} else {
   									echo "<li class=''>";
   								} ?>
   								<a data-toggle="tab" href="#UploadTab"><icon class="fa fa-arrow-up"></icon> Upload a Template</a></li>
                                   <li class=""><a data-toggle="tab" href="#DownloadTab"><icon class="fa fa-arrow-down"></icon></span> Download a Template</a></li>
                               </ul>
                           </div>
                           <div class="widget-content tab-content">
   							<?php if (!empty($_POST['upload-submit'])) {
   								echo '<div id="TagifyTab" class="tab-pane">';
   							} else {
   								echo '<div id="TagifyTab" class="tab-pane active">';
   							} ?>
                                   <div class="row-fluid">
                                       <div class="span12">
                                           <p>Here you can run the tagify script to generate a template from a MOP file and insert that template into the VSN folder you specify.</p>
                                           <form id="TagifyScriptForm" enctype="multipart/form-data" action="/config" method="POST">
                                               <div class="row-fluid">
                                                   <div class="span12">
                                                       Please Select the VSN type
                                                       <div class="control-group">
                                                           <div class="controls">
                                                               <select id="TagifyTypeSelect" name="TagifyTypeSelect" class="required">
                                                                   <option value="">Select a Type</option>
                                                                   <?php foreach($siteTypes as $key => $row): ?>
                                                                       <option value="<?php echo htmlentities($row[0], ENT_QUOTES, 'UTF-8'); ?>"><?php echo htmlentities(dir_to_vsn($row[0]), ENT_QUOTES, 'UTF-8'); ?></option>
                                                                   <?php endforeach; ?>
                                                               </select>
                                                           </div>
                                                       </div>
                                                       <input type="hidden" name="MAX_FILE_SIZE" value="10000000"/>
                                                       Upload the MOP in <b>.txt</b> or (NOT Word Doc) format.<br />If exporting to .txt in MS Word, choose 'CR / LF' for line breaks to avoid errors.<br />
                                                       <input name="mopUpload" type="file"><br /><br />
                                                   </div>
                                               </div>
                                               <div class="row-fluid">
                                                   <span class="pull-right"><input class="btn btn-success" type="submit" name="tagify-submit" value="Upload and Run" />&nbsp;</span>
                                               </div>
                                           </form>
                                       </div>
                                   </div>
                               </div>
                               <div id="MXConfigTab" class="tab-pane">
                                   <div class="row-fluid">
                                       <div class="span12">
                                           <p>By selecting templates and uploading a Spreadsheet for a node you can replace the tags in the template with values from the spreadsheet, generating a config.</p>
                                           <form id="ConfigScriptForm" enctype="multipart/form-data" action="/config/mx_builder" method="POST">
                                               <div class="row-fluid">
                                                   <div class="span12">
                                                       Please Enter the Site Name:<br />
                                                       <div class="control-group">
                                                           <div class="controls">
                                                               <input id="SiteName" name="SiteName" type="text" placeholder="Site Name"/><br />
                                                           </div>
                                                       </div>
                                                       Select the VSN:
                                                       <div class="control-group">
                                                           <div class="controls">
                                                               <select id="TypeSelect" name="TypeSelect" class="required" onchange="changeselect()">
                                                                   <option value="">Select a Type</option>
                                                                   <?php foreach($siteTypes as $row): ?>
                                                                       <option value="<?php echo htmlentities($row[0], ENT_QUOTES, 'UTF-8'); ?>"><?php echo htmlentities(dir_to_vsn($row[0]), ENT_QUOTES, 'UTF-8'); ?></option>
                                                                   <?php endforeach; ?>
                                                               </select>
                                                           </div>
                                                       </div>
                                                       Check the template files you want to generate configs for: <br />
                                                       <div id="Checkboxes">
                                                           <label><span><input class="no-margin" type="checkbox" name="radios" ></span> Select a Type</label>
                                                       </div>
                                                       <input type="hidden" name="MAX_FILE_SIZE" value="20000000"/>
                                                       <br />Upload your spreadsheet: <br />
                                                       <input name="spreadsheet" type="file"><br /><br />
                                                       <label><span><input class="no-margin" type="checkbox" name="email-self" id="email-self"></span> Email you the completed files?</label><br />
                                                       Enter other comma separated emails to send them a copy.<br />
                                                       <input id="email" name="email" type="text" placeholder="Email Address"/><br /><br />
                                                       <label><span><input class="no-margin" type="checkbox" name="verbose" id="verbose" onchange="checkVerbose()"></span> Display verbose debugging output?</label><br />
                                                       <div id="TagInput" style="display: none;">
                                                           Enter a comma separated list of tags to trace:<br />
                                                           <input style="width: 150%;" id="TagTrace" name="TagTrace" type="text" placeholder="Tracing Tags"/><br />
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="row-fluid">
                                                   <span class="pull-right"><input class="btn btn-success" type="submit" value="Upload and Run" />&nbsp;</span>
                                               </div>
                                           </form>
                                       </div>
                                       <!-- This div is used to insert the appropriate modals for the template select list -->
                                       <div id="modal" class="row-fluid"></div>
                                   </div>
                               </div>
                               <div id="DeleteTab" class="tab-pane">
                                   <div class="row-fluid">
                                       <div class="span12">
                                           <form id="DeleteTemplateForm" enctype="multipart/form-data" action="/config" method="POST">
                                               <div class="row-fluid">
                                                   Please check all the templates you'd like to delete, and then click delete.
                                                   <span class="pull-right"><input class="btn btn-warning" type="submit" name="delete-submit" value="Delete" />&nbsp;</span>
                                               </div>
                                               <div class="row-fluid">
                                                   <div class="span12">
                                                       <div class="accordion" id="deleteAccordion">
                                                           <?php // This loops through each site type, building the accordion to delete the files.
                                                           foreach($siteTypes as $key => $row){
                                                               $type = $row[0]; // sets a $type variable for the VSN Type name.
                                                               // This outputs the Divs necessary for the accordion of the given VSN type.
                                                               echo '<div class="accordion-group"><div class="accordion-heading">
                                                                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#deleteAccordion" href="#'.$type.'Delete"> <b>'.dir_to_vsn($type).'</b></a></div>
                                                                           <div class="accordion-body collapse" id="'.$type.'Delete"><div class="accordion-inner"><ul class="nomargins">';
                                                               foreach($row as $k => $template){ // This loops through each item of the inner array,
                                                                   if ($k>0) { // We skip index 0, since it is the VSN Type Name.
                                                                       echo '<label><span><input class="no-margin" type="checkbox" value="'.$template.'" name="'.$template.'"></span> '.basename($template).'</label>';
                                                                   }
                                                               }
                                                               echo "</ul>";
                                                               echo '</div></div></div>';
                                                           } ?>
                                                       </div>
                                                   </div>
                                               </div>
                                           </form>
                                       </div>
                                   </div>
                               </div>
   							<?php if (!empty($_POST['upload-submit'])) {
   								echo '<div id="UploadTab" class="tab-pane active">';
   							} else {
   								echo '<div id="UploadTab" class="tab-pane">';
   							} ?>
                                   <div class="row-fluid">
                                       <div class="span12">
                                           <p>Here you can upload a new, already tagified template.</p>
                                           <form id="UploadTemplateForm" enctype="multipart/form-data" action="/config" method="POST">
                                               <div class="row-fluid">
                                                   <div class="span12">
                                                       <div class="control-group">
                                                           <div class="controls">
                                                               Select the type of template:<br />
                                                               <select id="UploadTypeSelect" name="UploadTypeSelect" class="required">
                                                                   <option value="">Select a Type</option>
                                                                   <?php foreach($siteTypes as $row): ?>
                                                                       <option value="<?php echo htmlentities($row[0], ENT_QUOTES, 'UTF-8'); ?>"><?php echo htmlentities(dir_to_vsn($row[0]), ENT_QUOTES, 'UTF-8'); ?></option>
                                                                   <?php endforeach; ?>
                                                               </select>
                                                           </div>
                                                       </div>
                                                       <input type="hidden" name="MAX_FILE_SIZE" value="20000000"/>
                                                       Upload your new template (in txt format): <br />
                                                       <input name="TemplateUpload" type="file"><br /><br />
                                                   </div>
                                               </div>
                                               <div class="row-fluid">
                                                   <span class="pull-right"><input class="btn btn-success" type="submit" name="upload-submit" value="Upload Template" />&nbsp;</span>
                                               </div>
                                           </form>
                                       </div>
                                   </div>
                               </div>
                               <div id="DownloadTab" class="tab-pane">
                                   <div class="row-fluid">
                                       <div class="span12">
                                           <p>Click on any template you'd like to download.</p>
                                           <div class="accordion" id="downloadAccordion">
                                               <?php // This loops through each site type, building the accordion to download the files.
                                               foreach($siteTypes as $key => $row){
                                                   $type = $row[0]; // sets a $type variable for the VSN Type name.
                                                   // This outputs the Divs necessary for the accordion of the given VSN type.
                                                   echo '<div class="accordion-group"><div class="accordion-heading">
                                                                       <a class="accordion-toggle" data-toggle="collapse" data-parent="#downloadAccordion" href="#'.$type.'Collapse"> <b>'.dir_to_vsn($type).'</b></a></div>
                                                                       <div class="accordion-body collapse" id="'.$type.'Collapse"><div class="accordion-inner"><ul>';
                                                   foreach($row as $k => $template){ // This loops through each item of the inner array,
                                                       if ($k>0) { // We skip index 0, since it is the VSN Type Name.
                                                           echo '<li><a href="/download/s/'. basename($template).'" role="button">'.basename($template).'</a></li>';
                                                       }
                                                   }
                                                   echo "</ul>";
                                                   echo '</div></div></div>';
                                               } ?>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <?php page_footer(); ?>
   	<script type="text/javascript">
   	    function checkVerbose() { // This function is used to display or hide the input to add a tracing tag to the verbose output.
   	        var checkbox = document.getElementById("TagInput");
   	        if (checkbox.style.display == 'none'){
   	            checkbox.style.display = "inline-block";
   	        }
   	        else {
   	            checkbox.style.display = "none";
   	        }
   	    }
   	    var TypeSelect = document.getElementById("TypeSelect");
   	    var Checklist = document.getElementById("Checkboxes");
   	    // This function is used to change the the values of the second select input, based on the choice made in the first select input.
   	    function changeselect() {
   	        <?php
   	        foreach ($siteTypes as $st) { // Loops for each of the site types, so that it can create the list of the templates of each type. ?>
   	        if (TypeSelect.value == '<?php echo $st[0]; ?>') {
   	            var checkbox_html = "";
   	            <?php if (isset($st[1])) { // Make sure the site type has templates.
   	                foreach ($st as $key => $value) { // loops to create a list item for each type of a given site type.
   	                    if ($key >0 ) { ?>
   	            checkbox_html += '<label><span><input class="no-margin" type="checkbox" value="<?php echo basename($value); ?>" name="MOP<?php echo $key; ?>"></span> <?php echo basename($value); ?></label>';
   	            <?php }
   	            }
   	        }
   	         // Adds the checkbox_html that we've built with all the list items to the page. ?>
   	            Checklist.innerHTML = checkbox_html;
   	        }
   	        <?php } ?>
   	    }
   	</script>
   <?php 
   }
   
   //TODO: This controller not currently working, overwrites lst_files() function in __init__.php somehow?
   //TODO: will also not work until the $cwd problem is solved.
   /**
   * MX_BUILDER_CONTROLLER
   **/
   function mx_builder_controller() {
   	/**
   	* Version 4.0 - Jeremy Couser and Nathan Printz
   	* This Script will take a user uploaded spreadsheet, and replace tags found in user chosen template files
   	* with values from the spreadsheet.
   	* Changes have been made to the original script to accommodate acclimation into VZDB web portal environment.
   	**/
   
   	require (VZP_DIR."/bootstrap.php"); // Required for access to session variables
   
   	require_once VZP_LIB . '/php-excel-reader/excel_reader.php'; // required to be able to parse the spreadsheet
   	require_once VZP_LIB . '/PHPMailer/class.phpmailer.php'; // Required to use the mail() class for sending emails
   	require_once VZP_LIB . '/PHPMailer/class.smtp.php'; // Required to use JMA IT's smtp server
   
   	// Define global variables.
   	$cwd  = "/temp";  // The current working directory. this will get changed to include the user's username in the folder structure once that folder has been created.
   	$templateDir = ''; // The template directory for the type of site that we are working on.
   	$data = new Spreadsheet_Excel_Reader($_FILES['spreadsheet']['tmp_name']); // This is the data variable used for the actual spreadsheet parser
   	$found = 0; // Global found used to determine if a tag is found.
   	$notFoundPos = 0; // Position on the line when an existing tag is not found, so that it can be skipped when checking the next tags on the line.
   	$totalMops = 0; // Used to know the total number of mops in the template folder, and how many iterations to run.
   	$mopFilename = array(); // an array of the MOP filenames, used to reference specific mops.
   	$tracerT = explode(',',$_POST['TagTrace']); // this is an array of the tags entered for tracing
   
   	/***********************************************
   	//// Define functions used throughout the script.
   	************************************************/
   
   	// This function is used to remove all the files that are potentially created by this script. It should be called whenever
   	// the script is exited, aborted, or started to ensure all files are removed, preventing errors upon next execution.
   	function removeTempFiles($cwd) {
   	    exec("rm -R $cwd/*");
   	    echo 'The temporary files have been removed. <br />';
   	}
   
   	// $find - the tag we're looking to replace
   	// $replace - the string that we're replacing the tag with.
   	// $string - the entire line containing the tag
   	// $count - the number of times we want to search for that tag in the line.
   	function strReplaceOccur($find,$replace,$string,$count = 0) {
   	    if ($count == 0) {
   	        return str_replace($find,$replace,$string);
   	    }
   
   	    $pos = 0;
   	    $len = strlen($find);
   	    while($count) {
   	        $pos = strpos($string,$find,$pos);
   	        $string = substr_replace($string,$replace,$pos,$len);
   	        $count--;
   	    }
   	    return $string;
   	}
   
   	// Function to find a TAG in a MOP, i.e. <sometext>
   	// $start_limiter - the starting character of a tag, should be "<"
   	// $end_limiter - the ending character of a tag, should be ">"
   	// $line - the line of text that we are looking within for the tag.
   	function findText($startLimiter, $endLimiter, $line) {
   	    $startPos = strpos($line, $startLimiter, $GLOBALS['notFoundPos'] + 2);
   
   	    if ($startPos === FALSE) { // this returns false, ending the function, if there is no tag on the line
   	        return FALSE;
   	    }
   	    $endPos = strpos($line, $endLimiter, $startPos); // This is the case when there is no tag on the line, but it did have an open bracket, just not an end bracket.
   	    if ($endPos === FALSE) {
   	        return FALSE;
   	    }
   	    //echo "FindText function start: " . $start_limiter . "   end: " . $end_limiter . "  Line: " . htmlspecialchars($haystack, ENT_NOQUOTES) . "<br />";
   	    return substr($line, $startPos, ($endPos+1)-$startPos);
   	}
   
   	// This function looks for a given tag on a sheet of a spreadsheet.
   	// Passed Parameters: excelSheet - string, The sheet name to look on
   	// $column - string, the character value of the column to search for the tag. Example - 'A', 'BC'
   	// $dataOffset - string, the character value of the column in which the data for the tag will be found.
   	// $lookFor - the string that we are looking for as we go through the cells in the $column.
   	// $data - the spreadsheet parsing stream.
   	// $updatedLine - the data of the line that we are replacing the tag in.
   	// $tag - the tag that we are replacing.
   	function findTag($excelSheet, $column, $dataOffset, $lookFor, &$data, $updatedLine, $tag){
   
   	    $sheetIndex = $data->getSheetIndex($excelSheet);
   	    $currentRow = 0; // Initialize the current row number
   	    $numRow = $data->rowcount($sheetIndex); // Find the total number of rows.
   	    if ($numRow == 0){ // No rows found indicates an error with the sheet, or the sheet wasn't found.
   	        removeTempFiles($GLOBALS['cwd']); //remove the files using the global cwd.
   	        exit ("<br />The sheet: ".$excelSheet." seemed to contain no rows. Make sure the name of the sheet is correct.<br />You might want to try saving the .xls as .xml and back, or copying the only the values out of the cells to another sheet, and remaking the original sheet.<br />");
   	    }
   	    // VERBOSE DEBUG OUTPUT
   	    if (isset($_POST['verbose'])) {
   	        foreach ($GLOBALS['tracerT'] as $traceTag){
   	            if ($lookFor === $traceTag){
   	                echo "<div class=\"offset1\"><br/><b>Checking sheet: </b>" . $excelSheet . " <b>which has sheet index: </b>".$sheetIndex."<br/><b>The number of rows in the
   	                    sheet are: </b>".$numRow."<b>, The column to find the tag is:</b> ".$column."<br/><b>The column to find replacement data is:</b> ".$dataOffset."</div>";
   	            }
   	        }
   	    }
   	    // Iterates for every row in the sheet.
   	    while ($currentRow <= $numRow) {
   	        $currentCell = $data->val($currentRow, $column, $sheetIndex); // Pulls the current cell based on the sheet, column, and row.
   
   	        // The reason we use a substring of the current cell using the length of look for is so there can be any
   	        // trailing spaces after the tag in the cell, and we'll still find the tag in the spreadsheet.
   	        if (substr($currentCell,0,strlen($lookFor)) == $lookFor) {
   	            $GLOBALS['found'] = true; // we found a tag!
   	            // gets the replacement value based on the offset, and creates a new updated line, replacing the tag.
   	            if ($lookFor != 'Address') {
   	                $replacement = $data->val($currentRow, $dataOffset, $sheetIndex);
   	                $updatedLine = strReplaceOccur($tag, $replacement, $updatedLine, 1);
   	            }
   	            else if ($lookFor == 'Address') { // This is a special case if we're looking for address, since we have to return two cells to get the complete address.
   	                $replacement = $data->val($currentRow, $dataOffset, $sheetIndex) . " " . $data->val($currentRow + 1, $dataOffset, $sheetIndex)
   	                        . " " . $data->val($currentRow + 2, $dataOffset, $sheetIndex);
   	                $updatedLine = strReplaceOccur($tag, $replacement, $updatedLine, 1);
   	            }
   	            // VERBOSE DEBUG OUTPUT
   	            if (isset($_POST['verbose'])) {
   	                foreach ($GLOBALS['tracerT'] as $traceTag){
   	                    if ($lookFor === $traceTag){
   	                        echo "<div class=\"offset1\"><br/><b>The tag was found on row: </b>" . $currentRow. ", <b>The data in this cell is: </b>".
   	                            htmlspecialchars($currentCell,ENT_NOQUOTES,'UTF-8') ."<br/><b>The new line is:</b> ".
   	                            htmlspecialchars($updatedLine,ENT_NOQUOTES,'UTF-8') ."<br /></div>";
   	                    }
   	                }
   	            }
   	            $currentRow = $numRow+1; // This sets the current row to greater than the number of rows, so we exit the while loop.
   	        }
   	        $currentRow++; // moves to the next row in the spreadsheet.
   	    }
   	    // VERBOSE DEBUG OUTPUT
   	    if (isset($_POST['verbose'])) {
   	        foreach ($GLOBALS['tracerT'] as $traceTag){
   	            if ($lookFor === $traceTag && $GLOBALS['found'] == false){
   	            echo "<div class=\"offset1\"><br/><b>The tracer tag was not found, the last row checked was:</b> " . ($currentRow-1) . ".<br/></div>";
   	            }
   	        }
   	    }
   	    // return the line with the now replaced tag.
   	    return $updatedLine;
   	}
   	/***********************************************
   	//// End of function Definitions.
   	 ***********************************************/
   	page_header(); ?>
       <div id="content">
           <div id="content-header">
               <h1 class="pull-left">Config Script Results</h1>
               <div class="pull-right">
                   <a href="/php/config_tools/template_mgr.php"><button class="btn pull-right footer-text"><icon class="fa fa-arrow-left"></icon> Go Back</button></a>
               </div>
           </div>
           <div class="container-fluid">
               <div class="row-fluid">
                   <div class="span12">
                       <div class="span10 offset1 widget-box">
                           <div class="widget-title"><span class="icon"><icon class="fa fa-wrench"></icon></span><h5>Config Script Output</h5></div>
                           <div class="widget-content">
                               <div class="row-fluid">
                                   <div class="span12">
   	<?php
   	// Checks to make sure a spreadsheet was given
   	if (!$_FILES['spreadsheet']) {
   	    exit ("A spreadsheet was not uploaded, please try again.");
   	}
   	// Creates the temporary config directory with their username
   	if (!is_dir($cwd . "/" . $_SESSION['user']['username'])){
   	    if (!mkdir($cwd . "/" . $_SESSION['user']['username'])) {
   	        exit('Failed to create temporary username folder');
   	    }
   	    //This resets the cwd to include their username in the folder structure for easier access later.
   	    $cwd = "/temp/" .$_SESSION['user']['username'];
   	} else { // Removes their temporary files from last time
   	    $cwd = "/temp/" .$_SESSION['user']['username'];
   	    echo "Removing your temporary files from last time.<br/>";
   	    removeTempFiles($cwd);
   	    // Then has to recreate the directory after it was removed.
   	    if (!is_dir($cwd)) {
   	        if (!mkdir($cwd)) {
   	            exit('Failed to create temporary username folder');
   	        }
   	    }
   	}
   
   	// This loops through all POSTed items, adding the MOP filenames to an array
   	foreach ($_POST as $key => $value) {
   	    if (preg_match('/^MOP.*/',$key)){
   	        $mopFilename[]= $value;
   	        $totalMops++; // inrement the total number of mops
   	    }
   	}
   
   	// This sets the template directory accordingly, based on the VSN type they chose.
   	$templateDir = "/var/www/php/scripts/templates/" .$_POST['TypeSelect'];
   
   	// VERBOSE DEBUGGING OUTPUT
   	if (isset($_POST['verbose'])) {
   	    echo "<b>Verbose debugging turned on.</b><br />";
   	    if ($tracerT != "") {
   	        foreach ($tracerT as $key => $traceTag){
   	            $tracerT[$key] = trim($traceTag);
   	            echo "<b>Tracing tag: </b>". htmlspecialchars($tracerT[$key],ENT_NOQUOTES,'UTF-8')."<br/>";
   	        }
   	    }
   	    // This is the Verbose table displaying every tag type we search for and where it is looked for.
   	    echo "<b>These are the tag types, and the search criteria used to match them:</b><br />";
   	    echo "<table class=\"table table-bordered table-hover fixed-table\"><thead><tr>
   	            <th style=\"width: 175px;\">Regex Search String</th><th>Tag Type</th><th>Sheet tag found in</th><th style=\"width: 75px;\">Tag in Col:</th><th style=\"width: 100px;\">Replacement Col:</th>
   	        </tr></thead><tbody>";
   	    echo "<b><tr><td>/_intf>$/ </td><td>an interface tag</td><td>MX960#1 Port Assignments or<br />MX960#2 Port Assignments</td><td><b>E</b></td><td><b>C</b></td></b></tr>";
   	    echo "<b><tr><td>/_desc>$/ </td><td>a description tag</td><td>MX960#1 Port Assignments or<br />MX960#2 Port Assignments</td><td><b>R</b></td><td><b>S</b></td></b></tr>";
   	    echo "<b><tr><td>/_idn_gr_route_/ OR<br /><br />/_server/ && NOT<br />(/_intf>$/ OR /_desc>$/)</td><td>a server tag, or idn_gr route</td><td>DNS_Syslog_NTP_Trap</td><td><b>C</b></td><td><b>D</b></td></b></tr>";
   	    echo "<b><tr><td>/_ip>$/ OR /_netwk_mask>$/</td><td>an IP tag or Network Mask tag</td><td>Public_Address_Subnets_Details or<br />IDN_Address_Subnets_Details or<br />Private_Address_Subnets_Details</td><td><b>D</b></td><td><b>E</b></td></b></tr>";
   	    echo "<b><tr><td>/XXXX>$/</td><td>a node name tag</td><td>Site Info</td><td><b>A</b></td><td><b>B</b></td></b></tr>";
   	    echo "<b><tr><td>/_hostname>$/ OR /_device>$/</td><td>a hostname tag</td><td>Node Device Names</td><td><b>B</b></td><td><b>C</b></td></b></tr>";
   	    echo "<b><tr><td>/_priority>$/ OR<br />/_10g_portmirror_/ OR <br />/_vrrp_group_num>$/ OR <br />/_pm_filter>$/</td><td>a priority, port mirror, port mirror filter, or vrrp tag</td><td>Node Service VSNs</td><td><b>B</b></td><td><b>C</b></td></b></tr>";
   	    echo "<b><tr><td>/_information_tab>$/</td><td>Address from site info</td><td>Site Info</td><td><b>A</b></td><td><b>B</b></td></b></tr>";
   	    echo "</tbody></table><br />";
   	}
   
   	echo ("Checking the Site Info matches what you entered: ". $_POST['SiteName'] . "</br>");
   
   	// Find site name for filename output
   	$siteName = ""; // initialize siteName to nothing.
   	$sheet = $data->getSheetIndex('Site Info');
   	$currentRow = 0;
   
   	// This gets the number of rows in the sheet, and checks to make sure the sheet is good.
   	$numRow = $data->rowcount($sheet);
   	if ($numRow == 0) {
   	    removeTempFiles($cwd); //remove the files using the global cwd.
   	    exit ("<br />The sheet: 'Site Info' was found, but seemed to contain no rows.<br />You might want to try saving the .xls file as a .xml file, then re-saving as .xls again. <br />
   	    Or try copying only the values out of the cells to another sheet, and remaking the original sheet.<br />");
   	}
   
   	// This loops through the Site Info sheet and finds the site name.
   	while ($currentRow < $numRow) {
   	    $cell = $data->val($currentRow, 'A', $sheet);
   	    if ($cell == "VoIP 10Gb Node Name:") {
   	        $found = 1;
   	        $siteName = $data->val($currentRow, 'B', $sheet);
   	        $currentRow = $numRow;
   	    }
   	    $currentRow++;
   	}
   	// If the site name wasn't found, we error out to the user.
   	If ($found == 0) {
   	    exit ('<div class="text-center"><b>Couldn\'t find the keywords \'VoIP 10Gb Node Name:\' in Column \'A\' of Sheet \'Site Info\', so the site name couldn\'t be determined. <br />Please check that the columns are correct on the Site Info tab.</b><br /></div>');
   	}
   
   	// Perform a check to verify the spreadsheet reflects the sitename the user has enetered
   	if ($siteName != $_POST['SiteName']){
   	    exit ("<b>The site name you entered does not match the spreadsheet. </b></br>  Your entry: <b>". $_POST['SiteName'] .
   	        "</b>. The spreadsheet is for <b>" . $siteName. "</b>");
   	}
   	else {
   	    echo "The Site Name you entered matches the Site Name found within the spreadsheet. <br />Starting to build the MX configs...<br /><br />";
   	}
   
   	/*******************************************************************
   	 * Start of the for loop iterating through each .txt template file.
   	 *******************************************************************/
   	for ($i = 0; $i < $totalMops; $i++){
   	    $routerNum = ""; // initialize routerNum variable.
   
   	    // find the current mop file, and open that template as our haystack
   	    $input = $mopFilename[$i];
   	    $haystack = file("$templateDir/$input");
   
   	    // create the output file
   	    $file = fopen("$cwd/$siteName" . "_$input", "w+") or exit("Unable to open file!");
   
   	    // This creates a File with all the missing tags for the current input template.
   	    $missingFile = fopen("$cwd/$siteName" . "_" . $input . "_Missing_TAGs_1.txt", "w+") or exit("Unable to open file!");
   
   	    echo ("Currently processing: " . $input); // Let the user know what file we're starting.
   
   	    foreach ($haystack as $number => $line) {
   	        $updatedLine = $line; // set updatedLine to the original line, in case there is no tag, so that the original is output
   	        $notFoundPos = 0; // reinitialize notFoundPos to 0 each iteration, so it looks at the start of the string.
   	        $tag = findText("<",">", $line);
   
   	        if ($tag) { // only runs if there is a tag on the line.
   	            // count the number of tags on the line, so we know how many times to iterate on the same line.
   	            $numTags = substr_count($line, '<');
   	            while ( $numTags > 0 ) { // iterate for each tag on the line.
   	                $found = false; // initialize found to be false on every iteration of a tag.
   	                $tag = findText("<",">", $updatedLine); // find the next tag on the updated line.
   
   	                if ($tag != False) { //only runs if a tag is found.
   	                    // VERBOSE DEBUGGING OUTPUT
   	                    foreach ($tracerT as $traceTag){
   	                        if ($tag === $traceTag){
   	                            echo "<div class=\"offset1\"><br /><b>Found tracing tag: </b>" . htmlspecialchars($traceTag,ENT_NOQUOTES,'UTF-8') . "<b> on line:</b> "
   	                                . $number . "<b> of current template file.</b><br /></div>";
   	                        }
   	                    }
   
   	                    // Search and Replace server tags.
   	                    if ((preg_match('/_server/',$tag)) && !preg_match('/_intf>$/',$tag) && !preg_match('/_desc>$/',$tag)) {
   	                        $updatedLine = findTag('DNS_Syslog_NTP_Trap','C','D',$tag,$data,$updatedLine,$tag);
   	                    }
   	                    // Search and Replace idn gr route tags.
   	                    else if (preg_match('/_idn_gr_route_/', $tag)) {
   	                        $updatedLine = findTag('DNS_Syslog_NTP_Trap','C','D',$tag,$data,$updatedLine,$tag);
   	                    }
   	                    // Search and Replace interface port
   	                    else if (preg_match('/_intf>$/',$tag)) {
   	                        // Determine Router_Number
   	                        $routerNum = substr ($tag , 8, 1 );
   	                        if (is_numeric($routerNum)){
   	                            if ($routerNum == 1){
   	                                $updatedLine = findTag('MX960#1 Port Assignments','E','C',$tag,$data,$updatedLine,$tag);
   	                            }
   	                            else if ($routerNum == 2){
   	                                $updatedLine = findTag('MX960#2 Port Assignments','E','C',$tag,$data,$updatedLine,$tag);
   	                            }
   	                        }
   	                        // If the tag does not specify a router number check both tags
   	                        else{
   	                            $updatedLine = findTag('MX960#1 Port Assignments','E','C',$tag,$data,$updatedLine,$tag);
   	                            if (!$found){
   	                                $updatedLine = findTag('MX960#2 Port Assignments','E','C',$tag,$data,$updatedLine,$tag);
   	                            }
   	                        }
   	                    }
   	                    // Search and Replace interface description
   	                    else if (preg_match('/_desc>$/',$tag)) {
   	                        // Determine Router_Number
   	                        $routerNum = substr ($tag , 8, 1 );
   	                        if (is_numeric($routerNum)){
   	                            if ($routerNum == 1){
   	                                $updatedLine = findTag('MX960#1 Port Assignments','R','S',$tag,$data,$updatedLine,$tag);
   	                            }
   	                            else if ($routerNum == 2){
   	                                $updatedLine = findTag('MX960#2 Port Assignments','R','S',$tag,$data,$updatedLine,$tag);
   	                            }
   	                        }
   	                        // If the tag does not specify a router number check both tags
   	                        else{
   	                            $updatedLine = findTag('MX960#1 Port Assignments','R','S',$tag,$data,$updatedLine,$tag);
   	                            if (!$found){
   	                                $updatedLine = findTag('MX960#2 Port Assignments','R','S',$tag,$data,$updatedLine,$tag);
   	                            }
   	                        }
   	                    }
   	                    // Search and Replace ip address and network masks
   	                    else if (preg_match('/_ip>$/',$tag) || (preg_match('/_netwk_mask>$/',$tag))){
   	                        $updatedLine = findTag('Public_Address_Subnets_Details','D','E',$tag,$data,$updatedLine,$tag);
   	                        If (!$found){
   	                            $updatedLine = findTag('IDN_Address_Subnets_Details','D','E',$tag,$data,$updatedLine,$tag);
   	                            if (!$found) {
   	                                $updatedLine = findTag('Private_Address_Subnets_Details','D','E',$tag,$data,$updatedLine,$tag);
   	                            }
   	                        }
   	                        // This prevents the double slashes in lines where we have the netmask being replaced.
   	                        $updatedLine = str_replace('//','/',$updatedLine);
   	                    }
   	                    // Search and Replace MX Hostname with node name
   	                    else if ( preg_match('/_device>$|_hostname>$/',$tag)){
   	                        $updatedLine = findTag('Node Device Names','C','D',$tag,$data,$updatedLine,$tag);
   	                    }
   	                    // Search and Replace XXXX with node name
   	                    else if (preg_match('/XXXX>$/',$tag)) {
   	                        $updatedLine = findTag('Site Info','A','B','VoIP 10Gb Node Name:',$data,$updatedLine,$tag);
   	                    }
   	                    // Search and Replace priority
   	                    else if ( (preg_match('/_priority>$|_10g_portmirror_|_pm_filter>$|_vrrp_group_num>$/',$tag)) ){
   	                        $updatedLine = findTag('Node Service VSNs','B','C',$tag,$data,$updatedLine,$tag);
   	                    }
   	                    // Search and Replace Site Address with the full address from the Site Info Tab.
   	                    else if (preg_match('/information/',$tag)){
   	                        $updatedLine = findTag('Site Info','A','B','Address',$data,$updatedLine,$tag);
   	                    }
   	                    // Search and Replace customer VLAN START and STOP assignments.  As well as the Public AS
   	                    else if (preg_match('/_vlan_start>|_vlan_stop>|_vlan_end>|_autonomous_system>/',$tag)){
   	                        $updatedLine = findTag('Node Service VSNs','B','C',$tag,$data,$updatedLine,$tag);
   	                    }
   	                }
   	                // runs if the tag wasn't found (or a tag type couldn't be determined).
   	                if (!$found){
   	                    // Updates the global notFoundPos position in the current string to be two positions ahead of where it was,
   	                    // so it will start looking for the next tag on the same line.
   	                    $GLOBALS['notFoundPos'] = strpos($updatedLine, $tag, $GLOBALS['notFoundPos'] + 2);
   	                    if (fwrite($missingFile, ($number + 1) . " - " . $tag . "\n") === FALSE) {
   	                        exit ("Cannot write to file ($missingFile)");
   	                    }
   	                }
   	                $numTags--; // Decrements for the number of tags on the line left to look for.
   	            }
   	        }
   	        // Write the line to the output file.
   	        if (fwrite($file, $updatedLine) === FALSE) {
   	            exit ("Cannot write to file ($file)");
   	        }
   	        $updatedLine = "";
   	    } // end the for loop for the current mop file.
   	    echo ("<br />Finished Processing: ". $input. "<br /><br />");
   	}
   	/*******************************************************************
   	 * End of the for loop iterating through each .txt template file.
   	 *******************************************************************/
   
   	echo "All template files were processed.<br />";
   	// This removes any empty files before the files are archived (as all tags could have been found and replaced for some MOPs).
   	foreach (glob("$cwd/*.txt") as $filename) {
   	    if (!filesize($filename)){
   	        unlink($filename);
   	    }
   	}
   
   	// This creates an array of all of the txt files we created in this script for zipping them up.
   	$configFiles = array();
   	foreach (glob("$cwd/*.txt") as $filename) {
   	    $configFiles[] = $filename;
   	}
   
   	//The location of the archive file is kept for the zip function, and to attach it to the email.
   	$archive_loc = $cwd . '/' . $siteName .'_configs.zip';
   	$result = create_zip($configFiles, $archive_loc, true);
   
   	echo "<br />Here are the individual files:";
   	// Loops for all files, presenting an individual download link for each file.
   	foreach ($configFiles as $downloadFile) {
   	    echo '<br /><a href="/download.php/s/' . basename($downloadFile) . '&l=t">Download '. basename($downloadFile) .'</a>';
   	}
   
   	// If the configs were successfully zipped, we let the user know and send out the appropriate emails.
   	if ($result){
   	    echo "<br /><br />These files have been archived into one zip as well:";
   
   	    // Presents the zip file for the user to download.
   	    echo '<br /><a href="/download.php/s/' . $siteName . '_configs.zip&l=t">Download Zip containing all files</a><br />';
   
   	    // If the user selected to email anyone, we build the email and send it.
   	    if ((strlen($_POST['email'])>2) || isset($_POST['email-self'])){
   
   	        // Sets up the parameters to send the email
   	        $mail = new \PHPMailer;
   	        $mail->IsSMTP();                              // Set mailer to use SMTP
   	        $mail->Host = 'smtp.outlook.com';             // Specify the main server
   	        $mail->SMTPAuth = true;                       // Enable SMTP authentication
   	        $mail->Username = 'noreply@jmait.com';        // SMTP username
   	        $mail->Password = 'Jm@!t123##';               // SMTP password
   	        $mail->SMTPSecure = 'tls';                    // Enable encryption, 'ssl' also accepted
   	        $mail->From = 'noreply@jmait.com';
   	        $mail->FromName = 'JMA VZDB Portal';
   
   	        // These two IFs add the email recipients and change the message that is to be output to the user accordingly.
   	        if (isset($_POST['email-self'])) { // This adds the user if they checked the box to email themselves the configs
   	            $mail->AddAddress($_SESSION['user']['email']);  // Add a recipient
   	            $message = 'The config files have been sent to: ' . $_SESSION['user']['email'];
   	        }
   	        // This will add any other email addresses the user entered in the text field.
   	        $email_array = array();
   	        if (isset($_POST['email'])){
   	            if (!isset($_POST['email-self'])){ // sets the user message if only other addresses are listed.
   	                $message = 'The config files have been sent to: ' . $_POST['email'];
   	            }
   	            // Creates an array of the entered emails, separate by commas.
   	            $email_array = explode(',', $_POST['email']);
   	            foreach ($email_array as $email){
   	                $mail->AddAddress($email);
   	                if (isset($_POST['email-self']) && (strlen($_POST['email'])>2)) {
   	                    // Adds the email strings to the user message if the user is sending to themselves also.
   	                    $message .= ', ' . $email;
   	                }
   	            }
   	        }
   	        // Finish the rest of the email parameters
   	        $mail->AddAttachment($archive_loc);         // Add attachments
   	        $mail->IsHTML(true);                        // Set email format to HTML
   	        $mail->Subject = 'JMA VZDB '. $_POST['SiteName'] .' MX Configs';
   	        // This is the body for HTML capable email clients
   	        $mail->Body    = '<b>Your request has been processed!</b><br /><br />
   	You will find an attached zip file containing your config files and any missing tag files.</br>
   	Prior to applying, please review the configs to verify any tags that have not been replaced.
   	<br /><br />
   	Thank You,<br />
   	Verizon DB Team
   	';
   	        // This is the alternate body for those without HTML capable email clients.
   	        $mail->AltBody = 'Your request has been processed!
   
   	You will find an attached zip file containing your config files and any missing tag files.
   	Prior to applying, please review the configs to verify any tags that have not been replaced.
   
   	Thank You,
   	Verizon DB Team
   	';
   
   	        // This tries to send the message and generates an error if it couldn't be sent.
   	        if(!$mail->Send()) {
   	            echo 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo . '<br />';
   	        }
   	        // if the mail does send, update the user accordingly.
   	        else {
   	            echo $message . '<br />';
   	        }
   	    }
   	}
   	else { // This else executes if the zip file could not be created and lets the user know.
   	     echo "<br />Failed to create zip, so could not email the results or present them for download. Resultant files have been deleted.<br />";
   	} ?>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
   	<?php page_footer();
   }
 
   
}