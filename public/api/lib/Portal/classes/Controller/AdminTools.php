<?php

Namespace Portal\Controller;

class AdminTools {


  public static function delete_node2() {
    // copied from below, made a simple delete, no cares
    global $template;

    $_SESSION['help']['context'] = 0;
    $content = array();
    // Delete the node they chose if POSTing.
    if ($_POST) {
            extract($_POST);

            $node = \ORM::for_table('SiteInfo')->where( array('SiteName' => $NodeDeleteSelect,
                                                          'SiteGroup' => 'Deactivated')
                                                  )->find_one();

        if (empty($node)) {
            $content['message'] = 'Deactivated node not found for deletion.';
        } else {
                $node->delete();
                $content['message'] = "The node " . $NodeDeleteSelect . " has been deleted.";
        }
    }
  }



  public static function delete_node() {
    global $template;

    $_SESSION['help']['context'] = 0;
    $content = array();
    // Delete the node they chose if POSTing.
    if ($_POST) {
	    extract($_POST);

	    $node = \ORM::for_table('SiteInfo')->where( array('SiteName' => $NodeDeleteSelect,
                                                          'SiteGroup' => 'Deactivated')
                                                  )->find_one();

        if (empty($node)) {
            $content['message'] = 'No Deactivated node(s) found.';
        } else {
            if ($TypeSelect == "delete") {
                $node->delete();
                $content['message'] = "The node " . $NodeDeleteSelect . " has been irreversibly removed from the database, including audit information.";
                // TODO: No way to see who deleted the node currently...
            } elseif ($TypeSelect == "deactivate") {
                $node->set('SiteGroup', 'Deactivated');
                $node->set('EditedBy', $_SESSION['user']['username']);

                // Create the Audit table entry for deactivating the node.
                $audit = \ORM::for_table('SiteInfo_Audit')->create();
                $audit->SiteName = $node->SiteName;
                $audit->VSN = $node->VSN;
                $audit->SiteCode = $node->SiteCode;
                $audit->SiteMux = $node->SiteMux;
                $audit->ClliCode = $node->ClliCode;
                $audit->SiteAddress = $node->SiteAddress;
                $audit->ContactName = $node->ContactName;
                $audit->ContactPhone = $node->ContactPhone;
                $audit->ContactEmail = $node->ContactEmail;
                $audit->ContactEmail = $node->ContactEmail;
                $audit->Comment = $node->Comment;
                $audit->SiteGroup = "Deactivated";
                $audit->Removed = 0;
                $audit->Lab = $node->Lab;
                $audit->Created = 0;

                // Save the node and the audit entry.
                $node->save();
                $audit->save();
                // User notification
                $content['message'] = "The node " . $NodeDeleteSelect . " has been deactivated. Only those with the proper permission can view it now.";
            }
        }

	    $node = \ORM::for_table('SiteInfo')->where('SiteName',$NodeDeleteSelect)->find_one();
	    if ($TypeSelect == "delete") {
		    $node->delete();
		    $content['message'] = "The node " . $NodeDeleteSelect . " has been irreversibly removed from the database, including audit information.";

            // MQ 06/15 : Need to remove the node from WizProgress as well to avoid the following multiple identical completes:
            // +----------+-------------------------------------------------------------------------------+-----+---------+
            // | SiteName | TabsComplete                                                                  | Gen | VSN     |
            // +----------+-------------------------------------------------------------------------------+-----+---------+
            // | AAASN000 | 1,1,1,1,1,1,2,3,4,1,2,3,4,1,2,3,4,5,1,1,2,1,1,2,1,2,1,2,1,2,1,2,1,2,3,1,2,3,1 | 1.5 | 1.5 Gen |
            // | AAASN001 | 1,1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4,5,1,2,3,4,5                         | 1.5 | 1.5 Gen |

            $wiz_node = \ORM::for_table('WizProgress')->where('SiteName',$NodeDeleteSelect)->find_one();
            if (!empty($wiz_node))
               $wiz_node->delete();
            // end of MQ 06/15

		    // TODO: No way to see who deleted the node currently...
	    } elseif ($TypeSelect == "deactivate") {
	    	$node->set('SiteState', 'Deactivated');
	    	$node->set('EditedBy', $_SESSION['user']['username']);

	    	// Create the Audit table entry for deactivating the node.
		    $audit = \ORM::for_table('SiteInfo_Audit')->create();
		    $audit->SiteName = $node->SiteName;
		    $audit->VSN = $node->VSN;
		    $audit->SiteCode = $node->SiteCode;
		    $audit->SiteMux = $node->SiteMux;
		    $audit->ClliCode = $node->ClliCode;
		    $audit->SiteAddress = $node->SiteAddress;
		    $audit->ContactName = $node->ContactName;
		    $audit->ContactPhone = $node->ContactPhone;
		    $audit->ContactEmail = $node->ContactEmail;
		    $audit->ContactEmail = $node->ContactEmail;
		    $audit->Comment = $node->Comment;
		    $audit->SiteState = "Deactivated";
		    $audit->Removed = 0;
		    $audit->Lab = $node->Lab;
		    $audit->Created = 0;

	    	// Save the node and the audit entry.
	    	$node->save();
	    	$audit->save();
	    	// User notification
		    $content['message'] = "The node " . $NodeDeleteSelect . " has been deactivated. Only those with the proper permission can view it now.";
	    }
    }

    $content['nodes'] = array();
    foreach (\ORM::for_table('SiteInfo')->where_raw( "SiteGroup = 'Deactivated'" )->find_many() as $node) {
    	$content['nodes'][] = $node->SiteName;
    }
    print $template->render('html/admin-tools/delete.html.twig', $content);
  }

   public static function master_tags_controller($type = "") {
     global $template;

     $_SESSION['help']['context'] = 0;
     $content = "";
     if ($type == 'audit') {
        $content['javascript'] = "/assets/js/tables/portal.tables.audit.js";
        $content['is_audit'] = true;
     } else {
        $content['javascript'] = "/assets/js/tables/portal.tables.js";
        $content['is_audit'] = false;
     }
     print $template->render('html/admin-tools/master-tags.html.twig', $content);

   } // End of master_tags_controller()

   public static function vsn_editor_controller() {

       $_SESSION['help']['context'] = 0;

    	// Site types list needed for form selection.
    	foreach (glob(VZP_DIR . '/files/templates/*', GLOB_ONLYDIR) as $key => $type) {
    	    $siteTypes[] = basename($type);
    	}
    	page_header(); ?>
        <div id="content">
            <div id="content-header">
                <h1 class="pull-left">VSN Templates</h1>
            </div>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="widget-box">
                            <div class="widget-title"><span class="icon"><icon class="fa fa-upload"></icon></span><h5>VSN Editor</h5></div>
                            <div class="widget-content">
                                <span class="pull-left">
                                    <select id="TypeSelect" name="TypeSelect" onchange="SelectVSN(this.value)" class="required">
                                        <option value="">Select VSN</option>
                                        <?php foreach($siteTypes as $row): ?>
                                            <option value="<?php echo htmlentities(dir_to_vsn($row), ENT_QUOTES, 'UTF-8'); ?>"><?php echo htmlentities(dir_to_vsn($row), ENT_QUOTES, 'UTF-8'); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <button class="btn" onclick="">New VSN</button>
                                    <select id="TemplateSelect" name="TemplateSelect" onchange="loadFile(this.value)"class="required">
                                        <option value="">Select VSN First</option>
    <!--                                        <option value="networkConnections">Network Connections</option>-->
    <!--                                        <option value="ipSchema">IP Schema</option>-->
    <!--                                        <option value="tdr">TDR</option>-->
                                    </select>
                                    <button class="btn" onclick="">New Template</button>
                                </span>
                                <div class="row-fluid">
                                    <div class="span12">
    									<pre id="editor"></pre>
                                        <span class="pull-right"><button class="btn" onclick="save();">Save</button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <script src="/assets/js/lib/ace_editor/ace.js" type="text/javascript" charset="utf-8"></script>
    <script>
        var editor = ace.edit("editor");
        editor.session.setUseWorker(false);
        editor.setTheme("ace/theme/tomorrow_night_eighties");
        editor.getSession().setMode("ace/mode/json");
        editor.commands.addCommand({
            name: 'saveFile',
            bindKey: {
                win: 'Ctrl-S',
                mac: 'Command-S',
                sender: 'editor|cli'
            },
            exec: function(env, args, request) {
                save();
    //            alert("File Saved");
            }
        });
        function save(){
            var contents = editor.getValue();
    //        alert('successful save');
            $.post("new_site_templates/save.php",
                 {contents: contents,
                vsn: document.getElementById("TypeSelect").value,
                template: document.getElementById("TemplateSelect").value},
                function() {
                    // add error checking
                    alert(document.getElementById("TemplateSelect").value + " Saved");
                }
            )
        }

        //AJAX function call to retrieve available files on disk, and build template dropdown
        function SelectVSN(vsn){
            templateFiles = new Array();
            $.ajax({ url: 'new_site_templates/get_templates.php',
                data: {VSN: vsn},
                type: 'post',
                dataType : "json",
                success: function(output) {
                    templateFiles = output;
                    console.log(templateFiles);
                    $("#TemplateSelect").empty();
                    if (templateFiles.length > 0){
                        for (i = 0; i < templateFiles.length; i++){

                            var addOpt = document.createElement("option");
                            document.getElementById("TemplateSelect").options.add(addOpt);
                            addOpt.text = templateFiles[i];
                            addOpt.value = templateFiles[i];
                        }
                        loadFile(templateFiles[0]);
                    }
                    else{
                        var selectOption = document.getElementById("TemplateSelect");
                    selectOption.options.add(new Option("No Templates","No Templates"));
                    }
                }
            });
        }
        function loadFile(filetoLoad){
            var xmlhttp;
            if (window.XMLHttpRequest){ // Modern browsers
                xmlhttp=new XMLHttpRequest();
            }
            else{ // IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
                xmlhttp.onreadystatechange=function(){
                    if (xmlhttp.readyState==4 && xmlhttp.status==200){
                        //document.getElementById("editor").innerHTML=xmlhttp.responseText;
                        filetoEdit = xmlhttp.responseText;
                        editor.setValue(filetoEdit);
                        setTimeout(function () {
                            editor.gotoLine(1);
                        },20);
                    }

                }
                xmlhttp.open("GET","new_site_templates/" + document.getElementById("TypeSelect").value + "/" + document.getElementById("TemplateSelect").value + ".txt",true);
                xmlhttp.setRequestHeader("Cache-Control","no-cache,max-age=0");
                xmlhttp.setRequestHeader("Pragma", "no-cache");
                xmlhttp.send();
        }
    </script>
    <?php page_footer();
    }

    public static function remove_vsn() {
        global $template;

        $_SESSION['help']['context'] = 0;
        $content = array();
        $templates = array ('Node' => 'html/admin-tools/remove_vsn_node.html.twig',
                            'VSN'  => 'html/admin-tools/remove_vsn_vsn.html.twig',
        );
        // Delete the node they chose if POSTing.
        if ($_POST) {
            extract($_POST);

            if ($delNextField == 'VSN') {

                $vsns = array();
                $vsns = \ORM::for_table('VSN')->raw_query('SELECT VSN FROM VSN v WHERE v.VSN not like \'%Gen\' and v.SiteName = :node', array('node' => $VSNDeleteSelectNode))
                                              ->find_many();
                if (count($vsns)) {
                    foreach($vsns as $vsn)
                        $content['vsns'][] = $vsn->VSN;
                } else {
                    $content['vsns'][] = 'No VSN available';
                    $content['empty'] = 1;
                }
                $content['Node'] = $VSNDeleteSelectNode;
                $templateView = $templates['VSN'];

            } else {

                $vsn = \ORM::for_table('VSN')->where(array('SiteName' => $Node, 'VSN' => $VSNDeleteSelectVsn))->find_one();
                $vsn->delete();

                // User notification
                $content['message'] = "The VSN " . $VSNDeleteSelectVsn . " for Node ".$Node." has been irreversibly removed from the database, including audit information.";
                $templateView = $templates['Node'];
                // TODO: No way to see who deleted the node currently...

            }

        } else {

            $content[] = array();
            $templateView = $templates['Node'];
            foreach (\ORM::for_table('VSN')->raw_query('SELECT DISTINCT(SiteName) FROM VSN ORDER BY SiteName')->find_result_set() as $nodes) {
                $content['nodes'][] = $nodes->SiteName;
            }

        }
        // Render the Twig Template
        print $template->render($templateView, $content);

    }
}
