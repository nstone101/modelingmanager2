<?php

Namespace Portal\Controller;

class Reports {

  public static function port_usage_controller() {

    $_SESSION['help']['context'] = 0;
    $resultsPU = \ORM::for_table( 'SiteInfo' )
    ->raw_query( 'select s.SiteName, s.SiteCode, s.ClliCode, s.SiteGroup from SiteInfo s order by s.SiteName' )
    ->find_many();

    /*
  for each site, get MX960 and NS5400 data
   */


    $sitesMX960Ports = array();
    $sitesNS5400Ports = array();

    foreach ( $resultsPU as $puResult ) {

      $puResultDataKey = trim( $puResult['SiteName'] ) . trim( $puResult['SiteCode'] ) . trim( $puResult['ClliCode'] ) ;

      //$mx960QueryString = "select * from MX960PortAssignments m where (m.InterfaceTag like '%unused%' || (length(m.SubInterface) = 0)) and m.SiteName = '".$puResult['SiteName']."' ";
      //$ns5400QueryString = "select * from NS5400PortAssignments n where ((n.SubInterface is NULL) ||  (length(n.SubInterface) = 0)) and n.SiteName = '".$puResult['SiteName']."' ";

      $mx960QueryString = "select * from MX960PortAssignments m where m.InterfaceTag like '%unused%' and (length(m.SubInterface) = 0) and m.SiteName = '".$puResult['SiteName']."' and (UPPER(m.Interface) not like '%AE%') and (UPPER(m.Interface) not like '%IRB%')";
      $ns5400QueryString = "select * from NS5400PortAssignments n where ((n.SubInterface is NULL) ||  (length(n.SubInterface) = 0)) and n.SiteName = '".$puResult['SiteName']."' and (UPPER(n.Interface) NOT LIKE '%RETH%')";




      try {
        $mx960Results =  \ORM::for_table( 'DUAL' )
        ->raw_query( $mx960QueryString )
        ->find_array();

        $sitesMX960Ports[$puResultDataKey] = $mx960Results;

      } catch ( Exception $em ) {
        $sitesMX960Ports[$puResultDataKey] = null;
      }

      try {
        $ns5400Results =  \ORM::for_table( 'DUAL' )
        ->raw_query( $ns5400QueryString )
        ->find_array();

        $sitesNS5400Ports[$puResultDataKey] = $ns5400Results;

      } catch ( Exception $en ) {
        $sitesNS5400Ports[$puResultDataKey] = null;
      }
    }



    page_header(); ?>



<script>
    head.ready( function() {
        head.load("/assets/css/reports.css");
    });
</script>



       <div id="content" ng-controller="reportController">
        <div id="content-header">
            <h1 class="pull-left" id="site-header">Port Usage</h1>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">

                        <div class="widget-content tab-content nopadding">
                            <!--<table class="table table-bordered table-hover " id="portu-table">-->
                            <table class="table table-striped table-bordered table-hover table-condensed" >


                              <thead>
                                <tr>
                                  <th>&nbsp;</th>
                                  <th>Site Name</th>
                                  <th>SiteCode</th>
                                  <th>ClliCode</th>
                                  <th>SiteGroup</th>
                                </tr>
                              </thead>
                                <tbody>
                                    <?php foreach ( $resultsPU as $puResult2 ) {
      $rsSiteName = $puResult2['SiteName'];
      $rsSiteCode = $puResult2['SiteCode'];
      $rsClliCode = $puResult2['ClliCode'];
      $rsSiteGroup = $puResult2['SiteGroup'];
      $puSiteDataKey = trim( $puResult2['SiteName'] ) . trim( $puResult2['SiteCode'] ) . trim( $puResult2['ClliCode'] ) ;

      echo "<tr data-toggle='collapse' data-target='.$puSiteDataKey' class='accordion-toggle' > ";

      echo "<td width='20px'><img src='/assets/images/details_open.png' class='img-rounded'></td>";

      echo "<td width='120px'>$rsSiteName</td> ";
      echo "<td width='120px'>$rsSiteCode</td> ";
      echo "<td width='120px'>$rsClliCode</td> ";
      echo "<td width='320px'>$rsSiteGroup</td> ";
      echo "</tr> ";
?>

                                    <tr>
                                      <td colspan="10" class="hiddenRow">
                                        <div class="accordian-body <?php echo $puSiteDataKey ?>" data-target="#<?php echo $puSiteDataKey."MX960" ?> .<?php echo $puSiteDataKey."MX960" ?>">
                                          <!--<table class="table table-striped table-bordered table-hover table-condensed table-fixed">-->
                                            <table class="table table-striped table-bordered table-hover table-condensed" >

                                            <thead>
                                            </thead>
                                              <tbody>
                                                 <tr data-toggle='collapse' data-target='#<?php echo $puSiteDataKey."MX960" ?>' class='accordion-toggle ' >
                                                     <td width='20px'><img src='/assets/images/details_open.png' class='img-rounded'></td>
                                                     <td>MX960</td>
                                                 </tr>
                                                 <tr>
                                                    <td colspan="10" class="hiddenRow">
                                                      <div class="accordian-body <?php echo $puSiteDataKey."MX960" ?>" id="<?php echo $puSiteDataKey."MX960" ?>">
                                                        <table class="table table-striped table-bordered table-hover table-condensed table-fixed">
                                                          <thead>
                                                             <tr>
                                                                <th>Interface</th>
                                                                <th>Interface Tag</th>
                                                                <th>Port Type</th>
                                                                <th>Layer</th>
                                                                <th>Connected Device Type</th>
                                                                <th>Connected Device Name</th>
                                                                <th>Connected Device Port</th>
                                                                <th>Address Tag</th>
                                                                <th>Speed Duplex</th>
                                                                <th>Description</th>
                                                             </tr>
                                                          </thead>
                                                          <tbody>
                                                               <?php
      foreach ( $sitesMX960Ports[$puSiteDataKey] as $mPort ) {
        $tInterface = trim( $mPort['Interface'] );
        $tInterfaceTag = htmlentities( trim( $mPort['InterfaceTag'] ) );
        $tPortType = trim( $mPort['PortType'] );
        $tLayer = trim( $mPort['Layer'] );
        $tConnectedDeviceType = trim( $mPort['ConnectedDeviceType'] );
        $tConnectedDeviceName = trim( $mPort['ConnectedDeviceName'] );
        $tConnectedDevicePort = trim( $mPort['ConnectedDevicePort'] );
        $tAddressTag = htmlentities( trim( $mPort['AddressTag'] ) );
        $tSpeedDuplex = trim( $mPort['SpeedDuplex'] );
        $tDescription = trim( $mPort['Description'] );

        echo "<tr>";
        echo "<td>$tInterface</td>";
        echo "<td>$tInterfaceTag</td>";
        echo "<td>$tPortType</td>";
        echo "<td>$tLayer</td>";
        echo "<td>$tConnectedDeviceType</td>";
        echo "<td>$tConnectedDeviceName</td>";
        echo "<td>$tConnectedDevicePort</td>";
        echo "<td>$tAddressTag</td>";
        echo "<td>$tSpeedDuplex</td>";
        echo "<td>$tDescription</td>";
        echo "</tr>";
?>
                                                               <?php } ?>
                                                          </tbody>
                                                        </table>
                                                       </div>
                                                      </td>
                                                    </tr>
                                              </tbody>
                                            </table>
                          </div>
                       </td>
                     </tr>

                      <tr>
                       <td colspan="10" class="hiddenRow">
                          <div class="accordian-body <?php echo $puSiteDataKey ?>" data-target="#<?php echo $puSiteDataKey."NS5400" ?> .<?php echo $puSiteDataKey."NS5400" ?>">
                          <table class="table table-striped table-bordered table-hover table-condensed table-fixed">
                             <thead>
                             </thead>
                             <tbody>
                                     <tr data-toggle='collapse' data-target='#<?php echo $puSiteDataKey."NS5400" ?>' class='accordion-toggle ' >
                                       <td width='20px'><img src='/assets/images/details_open.png' class='img-rounded'></td>
                                       <td>NS5400</td>
                                   </tr>


                                     <tr>
                                      <td colspan="10" class="hiddenRow">
                                        <div class="accordian-body <?php echo $puSiteDataKey."NS5400" ?>" id="<?php echo $puSiteDataKey."NS5400" ?>">
                                           <!--<table class="table table-striped table-bordered table-hover table-condensed table-fixed">-->
                                           <table class="table table-striped table-bordered table-hover table-condensed" >

                                             <thead>
                                                <tr>
                                                   <th>Interface</th>
                                                   <th>Port Type</th>
                                                   <th>Layer</th>
                                                   <th>Connected Device Type</th>
                                                   <th>Connected Device Name</th>
                                                   <th>Connected Device Port</th>
                                                   <th>Address Tag</th>
                                                   <th>Speed Duplex</th>
                                                   <th>Description</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php
      foreach ( $sitesNS5400Ports[$puSiteDataKey] as $mPort ) {
        $tInterface = trim( $mPort['Interface'] );
        $tPortType = trim( $mPort['PortType'] );
        $tLayer = trim( $mPort['Layer'] );
        $tConnectedDeviceType = trim( $mPort['ConnectedDeviceType'] );
        $tConnectedDeviceName = trim( $mPort['ConnectedDeviceName'] );
        $tAddressTag = htmlentities( trim( $mPort['AddressTag'] ) );
        $tConnectedDevicePort = trim( $mPort['ConnectedDevicePort'] );
        $tSpeedDuplex = trim( $mPort['SpeedDuplex'] );
        $tDescription = trim( $mPort['Description'] );

        //   echo "<script>console.log( 'sitesNS5400Ports:   " . $tInterface . "  ' );</script>";

        echo "<tr>";
        echo "<td>$tInterface</td>";
        echo "<td>$tPortType</td>";
        echo "<td>$tLayer</td>";
        echo "<td>$tConnectedDeviceType</td>";
        echo "<td>$tConnectedDeviceName</td>";
        echo "<td>$tConnectedDevicePort</td>";
        echo "<td>$tAddressTag</td>";
        echo "<td>$tSpeedDuplex</td>";
        echo "<td>$tDescription</td>";
        echo "</tr>";
?>

                                               <?php } ?>
                                            </tbody>
                                           </table>
                                        </div>
                                      </td>
                                    </tr>
                              </tbody>
                          </table>
                          </div>
                       </td>
                     </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php page_footer();
  } // End of Port Usage controller.

  public static function ip_subnet_usage_controller() {

    $_SESSION['help']['context'] = 0;
    $resultsSNU = \ORM::for_table( 'SiteInfo' )
    ->raw_query( 'select s.SiteName, s.SiteCode, s.ClliCode, s.SiteGroup from SiteInfo s order by s.SiteName' )
    ->find_many();

    $sitesIPSchemas = array();
    $availableIPs3 = array();



    foreach ( $resultsSNU as $aSubNet ) {

      /* distinct select here ? */
      $resultsSitesIPSchemasQueryString = "select ips.SiteName, ips.NetworkType, ips.Name, ips.Subnet, ips.Netmask from IPSchema ips " .
        "where ips.SiteName = '" .$aSubNet['SiteName'] . "' " .
        "order by ips.SiteName, ips.NetworkType, concat(ips.Subnet, ips.Netmask) ";


      //echo "<script>console.log( 'resultsSNU - get IPSchema(s) for :   (" . $resultsSitesIPSchemasQueryString . ")' );</script>";




      $resultsSitesIPSchemas =  \ORM::for_table( 'IPSchema' )
      ->raw_query( $resultsSitesIPSchemasQueryString )
      ->find_array();

      $dataSNUKey = trim( $aSubNet['SiteName'] ) . trim( $aSubNet['SiteCode'] ) . trim( $aSubNet['ClliCode'] ) ; //. trim( $aSubNet['SiteGroup'] ) ;


      $sitesIPSchemas[$dataSNUKey] = $resultsSitesIPSchemas;










      foreach ( $resultsSitesIPSchemas as $networkTypeList ) {


        $queryString3 = "select * from " .
          "( " .
          "select VLANID, concat(Subnet,Netmask) as theSubnet, NetmaskTag, AddressTag, IPAddress, Device, DNSName, Purpose from IP_Public   " .
          "where VLANName = '" . $networkTypeList['Name'] . "' " .
          "and SiteName = '" . $networkTypeList['SiteName'] . "' " .
          "UNION  " .
          "select VLANID, concat(Subnet,Netmask) as theSubnet, NetmaskTag, AddressTag, IPAddress, Device, DNSName, Purpose  from IP_Private   " .
          "where VLANName = '" . $networkTypeList['Name'] . "' " .
          "and SiteName = '" . $networkTypeList['SiteName'] . "' " .
          "UNION  " .
          "select VLANID, concat(Subnet,Netmask) as theSubnet, NetmaskTag, AddressTag, IPAddress, Device, DNSName, Purpose  from IP_IDN   " .
          "where VLANName = '" . $networkTypeList['Name'] . "' " .
          "and SiteName = '" . $networkTypeList['SiteName'] . "' " .
          ") a " .
          "order by IPAddress;";



        $ipKey3 = $networkTypeList['SiteName'] . "_" . $networkTypeList['Name'];

        //  echo "<script>console.log( 'ipKey3 :   (" . $ipKey3 . ")' );</script>";


        $results3 =  \ORM::for_table( 'DUAL' )
        ->raw_query( $queryString3 )
        ->find_array();

        $availableIPs3[$ipKey3] = $results3;

        //echo "<script>console.log( 'ip queryString3 :   (" . $queryString3 . ")' );</script>";


      }
    }


    page_header(); ?>

       <script>
    head.ready( function() {
        head.load("/assets/js/reports.js");
        head.load("/assets/css/reports.css");
    });
</script>


   <div id="content">
 <div id="content-header">
   <h1 class="pull-left span10" id="site-header">IP Subnet Usage</h1></br>
 </div>


  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-content tab-content nopadding">
            <tbody>

              <table class="table table-striped table-bordered table-hover table-condensed" >


                 <thead>
                   <tr>
                      <th>&nbsp;</th>
                      <th>Site Name</th>
                      <th>SiteCode</th>
                      <th>ClliCode</th>
                      <th>SiteGroup</th>
                   </tr>
                 </thead>
                 <body>  <!-- Site table body -->
                     <?php foreach ( $resultsSNU as $aSubNet2 ) {
      $rsSiteName = $aSubNet2['SiteName'];
      $rsSiteCode = $aSubNet2['SiteCode'];
      $rsClliCode = $aSubNet2['ClliCode'];
      $rsSiteGroup = $aSubNet2['SiteGroup'];

      $dataSNUKey2 = trim( $aSubNet2['SiteName'] ) . trim( $aSubNet2['SiteCode'] ) . trim( $aSubNet2['ClliCode'] ) ; // . trim( $aSubNet2['SiteGroup'] ) ;




      echo "<tr data-toggle='collapse' data-target='#$dataSNUKey2' class='accordion-toggle ' > ";
      echo "<td width='20px'><img src='/assets/images/details_open.png' class='img-rounded'></td>";
      echo "<td>$rsSiteName</td> ";
      echo "<td>$rsSiteCode</td> ";
      echo "<td>$rsClliCode</td> ";
      echo "<td>$rsSiteGroup</td> ";
      echo "</tr> ";


?>

                     <!-- for each Site, get Subnets (IDN, Private, Public) -->
                     <tr>
                       <td colspan="10" class="hiddenRow">
                          <div class="accordian-body " id="<?php echo $dataSNUKey2 ?>">
                          <table class="table table-striped table-bordered table-hover table-condensed" >

                             <thead>
                               <tr>
                                  <!--<th>SiteName</th>-->
                                  <th>&nbsp;</th>
                                  <th>Network Type</th>
                                  <th>Subnet</th>
                                  <th>Name</th>
                               </tr>
                             </thead>
                             <tbody>


                                <!-- one of these <tr>s for each networktypesubnet -->
                                <?php
      foreach ( $sitesIPSchemas[$dataSNUKey2] as $network ) {
        //$tSiteName = trim( $network['SiteName'] );
        $tNetwork = trim( $network['NetworkType'] );
        $tName = trim( $network['Name'] );
        $tSubnet = trim( $network['Subnet'] ) . trim( $network['Netmask'] );

        $ipKey3 = $network['SiteName'] . "_" . $network['Name'];


        //echo "<tr>";
        echo "<tr data-toggle='collapse' data-target='#$ipKey3' class='accordion-toggle ' > ";

        echo "<td width='20px'><img src='/assets/images/details_open.png' class='img-rounded'></td>";

        //echo "<td>$tSiteName</td>";
        echo "<td>$tNetwork</td>";
        echo "<td>$tSubnet</td>";
        echo "<td>$tName</td>";
        echo "</tr>";


?>


                                <!-- now get all IP rows -->
                                <tr>
                         <td colspan="8" class="hiddenRow">
                           <div class="accordian-body " id="<?php echo $ipKey3 ?>">
                             <table class="table table-striped table-bordered table-hover table-condensed" >

                               <thead>
                                 <tr>
                                   <th>VLANID</th>
                                   <th>Subnet</th>
                                   <th>IPAddress</th>
                                   <th>AddressTag</th>
                                   <th>Device</th>
                                   <th>DNSName</th>
                                   <th>Purpose</th>
                                 </tr>
                               </thead>
                               <tbody>

                                  <!-- one of these <tr>s for each ipaddress -->
                                  <?php
        foreach ( $availableIPs3[$ipKey3] as $ip3 ) {
          $stVLANID = trim( $ip3['VLANID'] );
          $staSubNet = trim( $ip3['theSubnet'] );
          $stIPAddress = trim( $ip3['IPAddress'] );
          $stDevice = trim( $ip3['Device'] );
          $stDNSName = trim( $ip3['DNSName'] );
          $stPurpose = trim( $ip3['Purpose'] );
          $stAddressTag = htmlentities( trim( $ip3['AddressTag'] ), ENT_QUOTES, 'UTF-8' );

          echo "<tr>";
          echo "<td>$stVLANID</td>";
          echo "<td>$staSubNet</td>";
          echo "<td>$stIPAddress</td>";
          echo "<td>$stAddressTag</td>";
          echo "<td>$stDevice</td>";
          echo "<td>$stDNSName</td>";
          echo "<td>$stPurpose</td>";
          echo "</tr>";
        } ?>
                               </tbody>
                             </table>
                           </div>
                         </td>
                       </tr>

                                 <?php } ?>
                              </tbody>
                          </table>
                          </div>
                       </td>
                     </tr>


                     <?php } ?>
                 </body> <!-- Site table body -->

              </table> <!--Site table -->
            </tbody>   <!--outer body -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

       <?php page_footer();
  } // End of IP Subnet Usage controller.

  public static function node_vsns_controller() {


    $_SESSION['help']['context'] = 0;
    $vresultsVSNs = \ORM::for_table( 'SiteInfo' )
    ->raw_query( 'select distinct s.SiteName, s.VSN from SiteInfo s' )
    ->find_many();

    page_header(); ?>


<script>
    head.ready( function() {
        head.load("/assets/js/reports.js");
        head.load("/assets/css/reports.css");
    });
</script>





<div id="content">
  <div id="content-header">
    <h1 class="pull-left" id="site-header">Node VSN</h1>
  </div>

  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-content tab-content nopadding">
            <!--<table class="table table-bordered table-hover fixed-table" id="vsns-table">-->
            <table class="table table-striped table-bordered table-hover table-condensed" >

               <thead>
                 <tr>
                   <th>&nbsp;</th>
                   <th>Node VSN</th>
                 </tr>
               </thead>
               <tbody>
                                   <?php foreach ( $vresultsVSNs as $vresult ) {
      $siteName = $vresult['SiteName'];
      $vsnList = $vresult['VSN'];
      $vsns = explode( ",", $vsnList );

      echo "<tr data-toggle='collapse' data-target='.$siteName' class='accordion-toggle ' > ";
      //echo "<td><button class='btn btn-default btn-xs'><span class='glyphicon glyphicon-eye-open'></span></button></td>";

      echo "<td width='20px'><img src='/assets/images/details_open.png' class='img-rounded'></td>";


      echo "<td >$siteName</td> ";
      echo "</tr> ";
?>





                  <tr>
                   <td colspan="10" class="hiddenRow">
                     <div class="accordian-body <?php echo $siteName ?>" data-target="#<?php echo $siteName ?> .<?php echo $siteName ?>">

                        <!-- <table class="table table-striped table-bordered table-hover table-condensed table-fixed">-->
                         <table class="table table-striped table-bordered table-hover table-condensed" >

                           <thead>
                           </thead>
                           <tbody>
                             <tr data-toggle='collapse' data-target='#<?php echo $siteName ?>' class='accordion-toggle ' >
                               <td>VSNs</td></tr>
                             <tr>
                             <td colspan="10" class="hiddenRow">
                               <div class="accordian-body <?php echo $siteName ?>" id="<?php echo $siteName ?>">
                                 <!--<table class="table table-striped table-bordered table-hover table-condensed table-fixed">-->
                                 <table class="table table-striped table-bordered table-hover table-condensed" >

                                   <thead>

                                   </thead>
                                   <tbody>
                                      <?php foreach ( $vsns as $vsn ) {
        echo "<tr>";
        echo "<td>$vsn</td>";
        echo "</tr>";
?>
                                     <?php } ?>
                                   </tbody>
                                 </table>
                               </div>
                             </td>
                           </tr>
                         </tbody>
                       </table>

                     </div>
                   </td>
                 </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


       <?php page_footer();
  } // End of Node VSNs controller.

  public static function node_mop_controller() {

    $_SESSION['help']['context'] = 0;
    $mops = rglob( VZP_DIR . "/files/node/*/mops/*.doc*" );

    $sitesarray = array();


    foreach ( $mops as $mop ) {

      $aindex = basename( $mop );

      //echo "<script>console.log( 'aindex = basename $mop:   " . $aindex . "  ' );</script>";


      $parts = explode( "/", $mop );
      if ( array_key_exists( $aindex, $sitesarray ) ) {
        //      echo "<script>console.log( 'aindex:   " . $aindex . "  exists, adding array entry' );</script>";
        array_push( $sitesarray[$aindex], $parts[7] );
      } else {
        //      echo "<script>console.log( 'aindex:   " . $aindex . " doesnot exist, adding new array and entry' );</script>";
        $sitesarray[$aindex] = array();
        array_push( $sitesarray[$aindex], $parts[7] );
      }
    }





    $mopsarray = array();


    foreach ( $mops as $mop ) {

      $parts = explode( "/", $mop );
      $aindex = $parts[7];

      $themop = basename( $mop );

      if ( array_key_exists( $aindex, $mopsarray ) ) {
        array_push( $mopsarray[$aindex], $themop );
      } else {
        $mopsarray[$aindex] = array();
        array_push( $mopsarray[$aindex], $themop );
      }
    }



    page_header(); ?>


<script>
    head.ready( function() {
        head.load("/assets/js/reports.js");
        head.load("/assets/css/reports.css");
    });
</script>



<div id="content">
  <div id="content-header">
            <h1 class="pull-left" id="site-header">MOPs</h1>
  </div>

  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-content tab-content nopadding">

<div class="tabs-mop">
    <ul class="nav nav-tabs">
        <li class="active">
            <a data-toggle="tab" href="#mopNodes">MOP Nodes</a></li>
        <li>
            <a data-toggle="tab" href="#nodeMOPs">Node MOPs</a></li>
    </ul>
    <div class="tab-content">
        <div id="mopNodes" class="tab-pane fade in active">
            <table class="table table-striped table-bordered table-hover table-condensed" >

               <thead>
                 <tr>
                   <th>&nbsp;</th>
                   <th>Mop Name</th>
                 </tr>
               </thead>
               <tbody>
                  <?php foreach ( $sitesarray as $key => $value ) {
      $mop = $key;
      $mop_id = str_replace( ".", "_", basename( $mop ) );
      $mop_id = str_replace( " ", "_", $mop_id );
      $mop_id = str_replace( "-", "_", $mop_id );
      echo "<tr data-toggle='collapse' data-target='.$mop_id' class='accordion-toggle ' > ";

      echo "<td width='20px'><img src='/assets/images/details_open.png' class='img-rounded'></td>";

      echo "<td >$mop_id</td> ";
      echo "</tr> ";
?>

                  <tr>
                   <td colspan="10" class="hiddenRow">
                     <div class="accordian-body <?php echo $mop_id ?>" data-target="#<?php echo $mop_id ?> .<?php echo $mop_id ?>">

                                        <table class="table table-striped table-bordered table-hover table-condensed" >

                           <thead>
                           </thead>
                           <tbody>
                             <tr data-toggle='collapse' data-target='#<?php echo $mop_id ?>' class='accordion-toggle ' >
                               <td>Sites</td></tr>
                             <tr>
                             <td colspan="10" class="hiddenRow">
                               <div class="accordian-body <?php echo $mop_id ?>" id="<?php echo $mop_id ?>">
                                                <table class="table table-striped table-bordered table-hover table-condensed" >

                                   <thead>

                                   </thead>
                                   <tbody>
                                      <?php foreach ( $sitesarray[basename( $mop )] as $mopsite ) {
        echo "<tr>";
        echo "<td>$mopsite</td>";
        echo "</tr>";
?>
                                     <?php } ?>
                                    </tbody>
                                 </table>
                               </div>
                             </td>
                           </tr>
                         </tbody>
                       </table>
                     </div>
                   </td>
                 </tr>
                <?php } ?>
              </tbody>
            </table>
        </div>
        <div id="nodeMOPs" class="tab-pane fade">
            <table class="table table-striped table-bordered table-hover table-condensed" >

               <thead>
                 <tr>
                   <th>&nbsp;</th>
                   <th>Node</th>
                 </tr>
               </thead>
               <tbody>
                  <?php foreach ( $mopsarray as $key => $value ) {
      $node_id = $key;
      //    $mop_id = str_replace( ".", "_", basename( $mop ) );
      //    $mop_id = str_replace( " ", "_", $mop_id );
      //    $mop_id = str_replace( "-", "_", $mop_id );
      echo "<tr data-toggle='collapse' data-target='.$node_id' class='accordion-toggle ' > ";

      echo "<td width='20px'><img src='/assets/images/details_open.png' class='img-rounded'></td>";

      echo "<td >$node_id</td> ";
      echo "</tr> ";
?>

                  <tr>
                   <td colspan="10" class="hiddenRow">
                     <div class="accordian-body <?php echo $node_id ?>" data-target="#<?php echo $node_id ?> .<?php echo $node_id ?>">

                                        <table class="table table-striped table-bordered table-hover table-condensed" >

                           <thead>
                           </thead>
                           <tbody>
                             <tr data-toggle='collapse' data-target='#<?php echo $node_id ?>' class='accordion-toggle ' >
                               <td>MOPs</td></tr>
                             <tr>
                             <td colspan="10" class="hiddenRow">
                               <div class="accordian-body <?php echo $node_id ?>" id="<?php echo $node_id ?>">
                                                <table class="table table-striped table-bordered table-hover table-condensed" >

                                   <thead>

                                   </thead>
                                   <tbody>
                                      <?php foreach ( $mopsarray[$node_id] as $nodemop ) {
        echo "<tr>";
        echo "<td>$nodemop</td>";
        echo "</tr>";
?>
                                     <?php } ?>
                                    </tbody>
                                 </table>
                               </div>
                             </td>
                           </tr>
                         </tbody>
                       </table>
                     </div>
                   </td>
                 </tr>
                <?php } ?>
              </tbody>
            </table>
        </div>
    </div>
</div>














          </div>
        </div>
      </div>
    </div>
  </div>
</div>



</script>


       <?php page_footer();
  } // End of Node MOP controller.

  public static function node_validation_controller() {

    $_SESSION['help']['context'] = 0;

    function ipCheck( $IP, $CIDR ) {
      list ( $net, $mask ) = split( "/", $CIDR );

      $ip_net = ip2long( $net );
      $ip_mask = ~( ( 1 << ( 32 - $mask ) ) - 1 );
      $ip_ip = ip2long( $IP );
      $ip_ip_net = $ip_ip & $ip_mask;

      return $ip_ip_net == $ip_net;
    }

    function validate_data_entry($node) {
        echo '<h1>Validating ' . $node . '</h1>';
        $validation = new \Portal\SiteValidation\DataEntryController($node);
        $tabs = $validation->getValidationTables();
        foreach ($tabs as $key => $table) {
            //echo $key . '=' . $table . '<br>';
            echo '<h2>Bad data - '.$table.'</h2>';
            $validation->validateDataEntry($table);
        }
        unset($validation);
    }

    function validate_configuration($node) {
        echo '<h1>Validating configuration for node ' . $node . '</h1>';
        $validation = new \Portal\SiteValidation\DataEntryController($node);

        //subnets defined in IPSchema, but cannot be found in IP_Public, IP_Private nor IP_IDN
        echo '<h1>Unallocated/Mis-allocated subnets</h2>';
        $validation->findUnallocatedSubnets($node);

        //Subnets specified in either IP_Public, IP_Private or IP_IDN, should exist in IPSchema
        echo '<h1>Missing/Mis-allocated subnets</h2>';
        $validation->findMissingSubnets($node);

        echo '<h1>Duplicate subnets</h2>';
        $validation->findDuplicateSubnets($node);

        echo '<h1>Overlapping subnets</h2>';
        $validation->findOverlappingSubnets($node);

        unset($validation);
    }

    // Also accepts dotted decimal mask. The mask may also be passed to the second
    // argument. Any val
    // id combination of dotted decimal, CIDR and integers will be
    // accepted
    //$subnet = new IPv4Subnet( '192.168.0.0/24' );
    //$subnet = new \Portal\SiteValidation\IPv4Subnet( '192.168.0.0/24' );

    // These methods will accept a string or another instance
    //var_dump( $subnet->contains( '192.168.0.1' ) ); //TRUE
    //var_dump( $subnet->contains( '192.168.1.1' ) ); //FALSE
    //var_dump( $subnet->contains( '192.168.0.0/16' ) ); //FALSE
    //var_dump( $subnet->within( '192.168.0.0/16' ) ); //TRUE

    //public function addSubnet ($newSubnet) {
    //    $newSubnet = new IPv4Subnet($newSubnet);
    //    foreach ($this->subnets as &$existingSubnet) {
    //        if ($existingSubnet->contains($newSubnet)) {
    //            throw new Exception('Subnet already added');
    //        } else if ($existingSubnet->within($newSubnet)) {
    //            $existingSubnet = $newSubnet;
    //            return;
    //        }
    //    }
    //    $this->subnets[] = $newSubnet;
    //}

    //$subnets1 = \ORM::for_table( 'IPSchema' )->raw_query( 'select concat(ips.Subnet, ips.NetMask) as thesubnet, ips.SiteName, ips.VLAN from IPSchema ips' )
    //                                         ->find_many();

    $sites = \ORM::for_table('SiteInfo')->select('SiteName')->find_many();

    page_header();

    ?>
    <script>
    head.ready( function() {
       head.load("/assets/js/reports.js");
       head.load("/assets/css/reports.css");
    });
    </script>

    <div id="content">
    <div id="content-header">
        <h1 class="pull-left" id="site-header">Node Validation</h1>
    </div>
      <div class="container-fluid">
         <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                   <div class="widget-content tab-content nopadding">

                      <?php
                      if (!empty($_POST)) {

                         // basic tests for BaseValidation class
                         // $node = new \Portal\SiteValidation\DataEntryController('testnode');
                         // $node->testBaseValidation();

                         if ($_POST['node'] == 'All') {
                             foreach ($sites as $node) {
                                 validate_data_entry($node['SiteName']);
                                 validate_configuration($_POST['node']);
                             }
                         } else {
                             validate_data_entry($_POST['node']);
                             validate_configuration($_POST['node']);
                         }

                      } else { ?>
                          <div class="span12">
                              <div id="message" class="text-center"></div>
                                <form id="ValidateNode" action="/reports/node-validation" method="POST">
                                    <fieldset>
                                        <div class="control-group text-center">
                                            <div class="controls">
                                                <label><span>Node to be validated : </span>
                                                    <select class="no-margin" id="node" name="node" >
                                                        <option value="All">All nodes</option>
                                                        <?php
                                                        foreach ( $sites as $site ) { ?>
                                                            <option value="<?php echo $site['SiteName'];?>"><?php echo $site['SiteName'];?></option>
                                                        <?php } ?>
                                                    </select>
                                                </label>
                                            </div>
                                        </div>
                                        <div>
                                            <span class="pull-right"><input type="submit" class="btn btn-inverse" value="Validate" /></span>
                                        </div>
                                    </fieldset>
                                </form>
                          </div>
                          <?php
                      }
                      ?>

                   </div>
               </div>
           </div>
       </div>
    </div>
    <?php
    page_footer();
  }

  public static function test_exit_report() {
    $_SESSION['help']['context'] = 0;
    page_header();
    page_footer();
  }

  public static function global_search_controller() {




    $_SESSION['help']['context'] = 0;
    page_header();
     ?>

       <script>
    head.ready( function() {
        head.load("/assets/js/reports.js");
        head.load("/assets/css/reports.css");
    });
</script>


   <div id="content">
 <div id="content-header">
   <h1 class="pull-left span10" id="site-header">Global Search</h1></br>
 </div>


  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-content tab-content nopadding">
            <tbody>




            <input type="text" id="globalsearch" placeholder="Search DB"  />
            <input type="button" id="globalsearchbutton" value="Search" />

            <div id="globalsearchresult"></div>




            </tbody>   <!--outer body -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

       <?php 

    page_footer();
  }

  public static function f_global_search() {

//     echo "<script>console.log(\" searchtext : " . $_POST["searchtext"] . " \");</script>";

\ChromeLogger::log($_POST["searchtext"]);

    $_SESSION['help']['context'] = 0;
    $theSearchString = $_POST["searchtext"];

     $vzdbTablesSql = "SELECT distinct c.TABLE_NAME FROM information_schema.`COLUMNS` c WHERE TABLE_SCHEMA = 'VZDB' and c.TABLE_NAME not like '%Audit' and c.TABLE_NAME != 'users'";


    $vzdbTables = \ORM::for_table( 'DUAL' )
    ->raw_query( $vzdbTablesSql )
    ->find_many();

echo "  <div class=\"container-fluid\">";
echo "    <div class=\"row-fluid\">";
echo "";
echo "      <div class=\"span10\">";
echo "        <div class=\"widget-box\">";
echo "          <div class=\"widget-content tab-content nopadding\">";
echo "            <table class=\"table table-striped table-bordered table-hover table-condensed\" >";
echo "               <thead>";
echo "                 <tr>";
echo "                   <th>&nbsp;</th>";
echo "                   <th>Data Results</th>";
echo "                 </tr>";
echo "               </thead>";
echo "               <tbody>";

    foreach ( $vzdbTables as $vzdbTable ) {

      $tableKey = $vzdbTable['TABLE_NAME'];

     \ChromeLogger::log(" tableKey : " . $tableKey   );

      $tableDesc = "desc " . $vzdbTable['TABLE_NAME'] . " ; ";
      $tableColumns = \ORM::for_table( 'DUAL' )
      ->raw_query( $tableDesc )
      ->find_many();


      $tableColumns = \ORM::for_table( 'DUAL' )
      ->raw_query( $tableDesc )
      ->find_many();

      $searchSQLStr = "select * from " . $tableKey . " where ";
      $colCount = 0;

      foreach ( $tableColumns as $tableColumn ) {
         $tableColumnKey =  $vzdbTable['TABLE_NAME'] .  $tableColumn['Field'] ;
         \ChromeLogger::log(" table.column : " . $vzdbTable['TABLE_NAME'] . "." . $tableColumn['Field']);

         if (($tableColumn['Field'] == "id") 
            || ($tableColumn['Field'] == "EditedBy")
            || ($tableColumn['Field'] == "UpdatedTime")) {
            continue;
         }

         if ($colCount != 0)  {
            $searchSQLStr = $searchSQLStr . " or ";
         } 
         $searchSQLStr = $searchSQLStr . " " . $tableColumn['Field'] . " like '%" . $theSearchString . "' ";
         $colCount = $colCount + 1;
       }

       \ChromeLogger::log(" searchSQLStr: " . $searchSQLStr);


      // here, run the search
      // if results, add child table in the body below
      // if no results, skip

      $gSearchResults = \ORM::for_table( $tableKey )
      ->raw_query( $searchSQLStr )
      ->find_many();






echo "<tr data-toggle='collapse' data-target='.$tableKey' class='accordion-toggle ' > ";
echo "<td width='20px'><img src='/assets/images/details_open.png' class='img-rounded'></td>";
echo "<td >$tableKey</td> ";
echo "</tr> ";



echo "                  <tr>";
echo "                   <td colspan=\"10\" class=\"hiddenRow\">";
echo "                     <div class=\"accordian-body <?php echo $tableKey ?>\" data-target=\"#<?php echo $tableKey ?> .<?php echo $tableKey ?>\">";
echo "                         <table class=\"table table-striped table-bordered table-hover table-condensed\" >";
echo "                           <thead>";
echo "                           </thead>";
echo "                           <tbody>";
echo "                             <tr data-toggle='collapse' data-target='#<?php echo $tableKey ?>' class='accordion-toggle ' >";
echo "                               <td>Data Results</td>";
echo "                             </tr>";
echo "                             <tr>";
echo "                               <td colspan=\"10\" class=\"hiddenRow\">";
echo "                                 <div class=\"accordian-body <?php echo $tableKey ?>\" id=\"<?php echo $tableKey ?>\">";
echo "                                   <table class=\"table table-striped table-bordered table-hover table-condensed\" >";
echo "                                     <thead>";
echo "                                     </thead>";
echo "                                     <tbody>";



if ($tableKey == "Async") {
      foreach ( $gSearchResults as $gsearchresult ) {
             $asyncSiteName = $gsearchresult['SiteName'];
             $asyncVSN = $gsearchresult['VSN'];
             $asyncSerial = $gsearchresult['Serial'];
             $asyncBay = $gsearchresult['Bay'];
             $asyncHostname = $gsearchresult['Hostname'];
             $asyncDescription = $gsearchresult['Description'];
             $asyncASYNC = $gsearchresult['ASYNC'];
             $asyncOSVersion = $gsearchresult['OSVersion'];
             $asyncComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$asyncSiteName</td>";
             echo "  <td>$asyncVSN</td>";
             echo "  <td>$asyncSerial</td>";
             echo "  <td>$asyncBay</td>";
             echo "  <td>$asyncHostname</td>";
             echo "  <td>$asyncDescription</td>";
             echo "  <td>$asyncASYNC</td>";
             echo "  <td>$asyncOSVersion</td>";
             echo "  <td>$asyncComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "CardSlotting") { 
      foreach ( $gSearchResults as $gsearchresult ) {
             $cardSlottingSiteName = $gsearchresult['SiteName'];
             $cardSlottingSlot = $gsearchresult['Slot'];
             $cardSlottingCardType = $gsearchresult['CardType'];
             $cardSlottingInterfaceNum = $gsearchresult['InterfaceNum'];
             $cardSlottingDevice = $gsearchresult['Device'];
             $cardSlottingMIC = $gsearchresult['MIC'];
             $cardSlottingComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$cardSlottingSiteName</td>";
             echo "  <td>$cardSlottingSlot</td>";
             echo "  <td>$cardSlottingCardType</td>";
             echo "  <td>$cardSlottingInterfaceNum</td>";
             echo "  <td>$cardSlottingDevice</td>";
             echo "  <td>$cardSlottingMIC</td>";
             echo "  <td>$cardSlottingComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "CustomerAddressing") { 
      foreach ( $gSearchResults as $gsearchresult ) {
             $custaddrSiteName = $gsearchresult['SiteName'];
             $custaddrVSN = $gsearchresult['VSN'];
             $custaddrSPIP = $gsearchresult['SPIP'];
             $custaddrHostOffset = $gsearchresult['HostOffset'];
             $custaddrIPAddress = $gsearchresult['IPAddress'];
             $custaddrDevice = $gsearchresult['Device'];
             $custaddrDescription = $gsearchresult['Description'];
             $custaddrComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$custaddrSiteName</td>";
             echo "  <td>$custaddrVSN</td>";
             echo "  <td>$custaddrSPIP</td>";
             echo "  <td>$custaddrHostOffset</td>";
             echo "  <td>$custaddrIPAddress</td>";
             echo "  <td>$custaddrDevice</td>";
             echo "  <td>$custaddrDescription</td>";
             echo "  <td>$custaddrComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "CustomerCircuits") { 
      foreach ( $gSearchResults as $gsearchresult ) {
             $custcircSiteName = $gsearchresult['SiteName'];
             $custcircVSN = $gsearchresult['VSN'];
             $custcircCustomerCktType = $gsearchresult['CustomerCktType'];
             $custcircSpeed = $gsearchresult['Speed'];
             $custcircCDR = $gsearchresult['CDR'];
             $custcircCircuitID = $gsearchresult['CircuitID'];
             $custcircMX960Router = $gsearchresult['MX960Router'];
             $custcircRemoteRouter = $gsearchresult['RemoteRouter'];
             $custcircComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$custcircSiteName</td>";
             echo "  <td>$custcircVSN</td>";
             echo "  <td>$custcircCustomerCktType</td>";
             echo "  <td>$custcircSpeed</td>";
             echo "  <td>$custcircCDR</td>";
             echo "  <td>$custcircCircuitID</td>";
             echo "  <td>$custcircMX960Router</td>";
             echo "  <td>$custcircRemoteRouter</td>";
             echo "  <td>$custcircComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "DNSTrapLog") { 
      foreach ( $gSearchResults as $gsearchresult ) {
             $dnstraplogSiteName = $gsearchresult['SiteName'];
             $dnstraplogVSN = $gsearchresult['VSN'];
             $dnstraplogFunction = $gsearchresult['Function'];
             $dnstraplogTag = $gsearchresult['Tag'];
             $dnstraplogHostIP = $gsearchresult['HostIP'];
             $dnstraplogReference = $gsearchresult['Reference'];
             $dnstraplogName = $gsearchresult['Name'];
             $dnstraplogComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$dnstraplogSiteName</td>";
             echo "  <td>$dnstraplogVSN</td>";
             echo "  <td>$dnstraplogFunction</td>";
             echo "  <td>$dnstraplogTag</td>";
             echo "  <td>$dnstraplogHostIP</td>";
             echo "  <td>$dnstraplogReference</td>";
             echo "  <td>$dnstraplogName</td>";
             echo "  <td>$dnstraplogComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "IP_IDN") { 
      foreach ( $gSearchResults as $gsearchresult ) {
             $ipidnSiteName = $gsearchresult['SiteName'];
             $ipidnVSN = $gsearchresult['VSN'];
             $ipidnVLANName = $gsearchresult['VLANName'];
             $ipidnVLANID = $gsearchresult['VLANID'];
             $ipidnSubnet = $gsearchresult['Subnet'];
             $ipidnIPAddress = $gsearchresult['IPAddress'];
             $ipidnDevice = $gsearchresult['Device'];
             $ipidnDNSName = $gsearchresult['DNSName'];
             $ipidnPurpose = $gsearchresult['Purpose'];
             $ipidnComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$ipidnSiteName</td>";
             echo "  <td>$ipidnVSN</td>";
             echo "  <td>$ipidnVLANName</td>";
             echo "  <td>$ipidnVLANID</td>";
             echo "  <td>$ipidnSubnet</td>";
             echo "  <td>$ipidnIPAddress</td>";
             echo "  <td>$ipidnDevice</td>";
             echo "  <td>$ipidnDNSName</td>";
             echo "  <td>$ipidnPurpose</td>";
             echo "  <td>$ipidnComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "IP_Private") { 
      foreach ( $gSearchResults as $gsearchresult ) {
             $ipprivSiteName = $gsearchresult['SiteName'];
             $ipprivVSN = $gsearchresult['VSN'];
             $ipprivVLANName = $gsearchresult['VLANName'];
             $ipprivVLANID = $gsearchresult['VLANID'];
             $ipprivSubnet = $gsearchresult['Subnet'];
             $ipprivIPAddress = $gsearchresult['IPAddress'];
             $ipprivDevice = $gsearchresult['Device'];
             $ipprivDNSName = $gsearchresult['DNSName'];
             $ipprivPurpose = $gsearchresult['Purpose'];
             $ipprivComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$ipprivSiteName</td>";
             echo "  <td>$ipprivVSN</td>";
             echo "  <td>$ipprivVLANName</td>";
             echo "  <td>$ipprivVLANID</td>";
             echo "  <td>$ipprivSubnet</td>";
             echo "  <td>$ipprivIPAddress</td>";
             echo "  <td>$ipprivDevice</td>";
             echo "  <td>$ipprivDNSName</td>";
             echo "  <td>$ipprivPurpose</td>";
             echo "  <td>$ipprivComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "IP_Public") { 
      foreach ( $gSearchResults as $gsearchresult ) {
             $ippublrivSiteName = $gsearchresult['SiteName'];
             $ippublrivVSN = $gsearchresult['VSN'];
             $ippublrivVLANName = $gsearchresult['VLANName'];
             $ippublrivVLANID = $gsearchresult['VLANID'];
             $ippublrivSubnet = $gsearchresult['Subnet'];
             $ippublrivIPAddress = $gsearchresult['IPAddress'];
             $ippublrivDevice = $gsearchresult['Device'];
             $ippublrivDNSName = $gsearchresult['DNSName'];
             $ippublrivPurpose = $gsearchresult['Purpose'];
             $ippublrivComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$ippublrivSiteName</td>";
             echo "  <td>$ippublrivVSN</td>";
             echo "  <td>$ippublrivVLANName</td>";
             echo "  <td>$ippublrivVLANID</td>";
             echo "  <td>$ippublrivSubnet</td>";
             echo "  <td>$ippublrivIPAddress</td>";
             echo "  <td>$ippublrivDevice</td>";
             echo "  <td>$ippublrivDNSName</td>";
             echo "  <td>$ippublrivPurpose</td>";
             echo "  <td>$ippublrivComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "IPSchema") { 
      foreach ( $gSearchResults as $gsearchresult ) {
             $ipschemaSiteName = $gsearchresult['SiteName'];
             $ipschemaVSN = $gsearchresult['VSN'];
             $ipschemaNetworkType = $gsearchresult['NetworkType'];
             $ipschemaName = $gsearchresult['Name'];
             $ipschemaSubnet = $gsearchresult['Subnet'];
             $ipschemaVLAN = $gsearchresult['IPAddress'];
             $ipschemaPurpose = $gsearchresult['Purpose'];
             $ipschemaComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$ipschemaSiteName</td>";
             echo "  <td>$ipschemaVSN</td>";
             echo "  <td>$ipschemaNetworkType</td>";
             echo "  <td>$ipschemaName</td>";
             echo "  <td>$ipschemaSubnet</td>";
             echo "  <td>$ipschemaVLAN</td>";
             echo "  <td>$ipschemaPurpose</td>";
             echo "  <td>$ipschemaComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "MasterTags") {
      foreach ( $gSearchResults as $gsearchresult ) {
             $mstrtagTag = $gsearchresult['SiteName'];
             $mstrtagComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$mstrtagTag</td>";
             echo "  <td>$mstrtagComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "MX960PortAssignments") {
      foreach ( $gSearchResults as $gsearchresult ) {
             $mx960paSiteName = $gsearchresult['SiteName'];
             $mx960paVSN = $gsearchresult['VSN'];
             $mx960paMX = $gsearchresult['MX'];
             $mx960paDPC = $gsearchresult['DPC'];
             $mx960paVSNInstance = $gsearchresult['VSNInstance'];
             $mx960paInterface = $gsearchresult['Interface'];
             $mx960paSubInterface = $gsearchresult['SubInterface'];
             $mx960paPortType = $gsearchresult['PortType'];
             $mx960paLayer = $gsearchresult['Layer'];
             $mx960paSlot = $gsearchresult['Slot'];
             $mx960paConnectedDeviceType = $gsearchresult['ConnectedDeviceType'];
             $mx960paConnectedDeviceName = $gsearchresult['ConnectedDeviceName'];
             $mx960paConnectedDevicePort = $gsearchresult['ConnectedDevicePort'];
             $mx960paIPAddress = $gsearchresult['IPAddress'];
             $mx960paSpeedDuplex = $gsearchresult['SpeedDuplex'];
             $mx960paDescription = $gsearchresult['Description'];
             $mx960paComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$mx960paSiteName</td>";
             echo "  <td>$mx960paVSN</td>";
             echo "  <td>$mx960paMX</td>";
             echo "  <td>$mx960paDPC</td>";
             echo "  <td>$mx960paVSNInstance</td>";
             echo "  <td>$mx960paInterface</td>";
             echo "  <td>$mx960paSubInterface</td>";
             echo "  <td>$mx960paPortType</td>";
             echo "  <td>$mx960paLayer</td>";
             echo "  <td>$mx960paSlot</td>";
             echo "  <td>$mx960paConnectedDeviceType</td>";
             echo "  <td>$mx960paConnectedDeviceName</td>";
             echo "  <td>$mx960paConnectedDevicePort</td>";
             echo "  <td>$mx960paIPAddress</td>";
             echo "  <td>$mx960paSpeedDuplex</td>";
             echo "  <td>$mx960paDescription</td>";
             echo "  <td>$mx960paComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "NS5400PortAssignments") {
      foreach ( $gSearchResults as $gsearchresult ) {
             $ns5400paSiteName = $gsearchresult['SiteName'];
             $ns5400paVSN = $gsearchresult['VSN'];
             $ns5400paNS = $gsearchresult['NS'];
             $ns5400paInterface = $gsearchresult['Interface'];
             $ns5400paSubInterface = $gsearchresult['SubInterface'];
             $ns5400paPortType = $gsearchresult['PortType'];
             $ns5400paLayer = $gsearchresult['Layer'];
             $ns5400paSlot = $gsearchresult['Slot'];
             $ns5400paConnectedDeviceType = $gsearchresult['ConnectedDeviceType'];
             $ns5400paConnectedDeviceName = $gsearchresult['ConnectedDeviceName'];
             $ns5400paConnectedDevicePort = $gsearchresult['ConnectedDevicePort'];
             $ns5400paIPAddress = $gsearchresult['IPAddress'];
             $ns5400paSpeedDuplex = $gsearchresult['SpeedDuplex'];
             $ns5400paDescription = $gsearchresult['Description'];
             $ns5400paComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$ns5400paSiteName</td>";
             echo "  <td>$ns5400paVSN</td>";
             echo "  <td>$ns5400paNS</td>";
             echo "  <td>$ns5400paInterface</td>";
             echo "  <td>$ns5400paSubInterface</td>";
             echo "  <td>$ns5400paPortType</td>";
             echo "  <td>$ns5400paLayer</td>";
             echo "  <td>$ns5400paSlot</td>";
             echo "  <td>$ns5400paConnectedDeviceType</td>";
             echo "  <td>$ns5400paConnectedDeviceName</td>";
             echo "  <td>$ns5400paConnectedDevicePort</td>";
             echo "  <td>$ns5400paIPAddress</td>";
             echo "  <td>$ns5400paSpeedDuplex</td>";
             echo "  <td>$ns5400paDescription</td>";
             echo "  <td>$ns5400paComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "NetworkConnections") {
      foreach ( $gSearchResults as $gsearchresult ) {
             $netcSiteName = $gsearchresult['SiteName'];
             $netcVSN = $gsearchresult['VSN'];
             $netcNodeDevice = $gsearchresult['NodeDevice'];
             $netcNodeInterface = $gsearchresult['NodeInterface'];
             $netcRemoteDeviceName = $gsearchresult['RemoteDeviceName'];
             $netcRemoteDeviceType = $gsearchresult['RemoteDeviceType'];
             $netcRemoteDeviceInt = $gsearchresult['RemoteDeviceInt'];
             $netcRemoteDeviceCircuit = $gsearchresult['RemoteDeviceCircuit'];
             $netcNetwork = $gsearchresult['Network'];
             $netcSpeed = $gsearchresult['Speed'];
             $netcDescription = $gsearchresult['Description'];
             $netcComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$netcSiteName</td>";
             echo "  <td>$netcVSN</td>";
             echo "  <td>$netcNodeDevice</td>";
             echo "  <td>$netcNodeInterface</td>";
             echo "  <td>$netcRemoteDeviceName</td>";
             echo "  <td>$netcRemoteDeviceType</td>";
             echo "  <td>$netcRemoteDeviceInt</td>";
             echo "  <td>$netcRemoteDeviceCircuit</td>";
             echo "  <td>$netcNetwork</td>";
             echo "  <td>$netcSpeed</td>";
             echo "  <td>$netcDescription</td>";
             echo "  <td>$netcComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "SiteInfo") {
      foreach ( $gSearchResults as $gsearchresult ) {
             $siteSiteName = $gsearchresult['SiteName'];
             $siteVSN = $gsearchresult['VSN'];
             $siteSiteCode = $gsearchresult['SiteCode'];
             $siteSiteMux = $gsearchresult['SiteMux'];
             $siteClliCode = $gsearchresult['ClliCode'];
             $siteSiteAddress = $gsearchresult['SiteAddress'];
             $siteContactName = $gsearchresult['ContactName'];
             $siteContactPhone = $gsearchresult['ContactPhone'];
             $siteContactEmail = $gsearchresult['ContactEmail'];
             $siteSiteGroup = $gsearchresult['SiteGroup'];
             $siteEMTS = $gsearchresult['EMTS'];
             $siteLongName = $gsearchresult['LongName'];

             echo "<tr>";
             echo "  <td>$siteSiteName</td>";
             echo "  <td>$siteVSN</td>";
             echo "  <td>$siteSiteCode</td>";
             echo "  <td>$siteSiteMux</td>";
             echo "  <td>$siteClliCode</td>";
             echo "  <td>$siteSiteAddress</td>";
             echo "  <td>$siteContactName</td>";
             echo "  <td>$siteContactPhone</td>";
             echo "  <td>$siteContactEmail</td>";
             echo "  <td>$siteSiteGroup</td>";
             echo "  <td>$siteEMTS</td>";
             echo "  <td>$siteLongName</td>";
             echo "</tr>";
      }

} else if ($tableKey == "SiteStatus") {
      foreach ( $gSearchResults as $gsearchresult ) {
             $sitestSiteName = $gsearchresult['SiteName'];
             $sitestVSN = $gsearchresult['VSN'];
             $sitestPhase = $gsearchresult['Phase'];
             $sitestTask = $gsearchresult['Task'];
             $sitestDescription = $gsearchresult['Description'];
             $sitestStatus = $gsearchresult['Status'];
             $sitestDelayReason = $gsearchresult['DelayReason'];
             $sitestLinkURL = $gsearchresult['LinkURL'];
             $sitestComment = $gsearchresult['Comment'];
             $sitestProgress = $gsearchresult['Progress'];

             echo "<tr>";
             echo "  <td>$sitestSiteName</td>";
             echo "  <td>$sitestVSN</td>";
             echo "  <td>$sitestPhase</td>";
             echo "  <td>$sitestTask</td>";
             echo "  <td>$sitestDescription</td>";
             echo "  <td>$sitestStatus</td>";
             echo "  <td>$sitestDelayReason</td>";
             echo "  <td>$sitestLinkURL</td>";
             echo "  <td>$sitestComment</td>";
             echo "  <td>$sitestProgress</td>";
             echo "</tr>";
      }

} else if ($tableKey == "SubnetAdv") {
      foreach ( $gSearchResults as $gsearchresult ) {
             $subadvSiteName = $gsearchresult['SiteName'];
             $subadvVSN = $gsearchresult['VSN'];
             $subadvSubnetName = $gsearchresult['SubnetName'];
             $subadvNetwork = $gsearchresult['Network'];
             $subadvPublicIPAdv = $gsearchresult['PublicIPAdv'];
             $subadvComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$subadvSiteName</td>";
             echo "  <td>$subadvVSN</td>";
             echo "  <td>$subadvSubnetName</td>";
             echo "  <td>$subadvNetwork</td>";
             echo "  <td>$subadvPublicIPAdv</td>";
             echo "  <td>$subadvComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "TDR") {
      foreach ( $gSearchResults as $gsearchresult ) {
             $tdrSiteName = $gsearchresult['SiteName'];
             $tdrVSN = $gsearchresult['VSN'];
             $tdrSBC = $gsearchresult['SBC'];
             $tdrStatus = $gsearchresult['Status'];
             $tdrSBCPairName = $gsearchresult['SBCPairName'];
             $tdrTDRServerName = $gsearchresult['TDRServerName'];
             $tdrTDRServerIP = $gsearchresult['TDRServerIP'];
             $tdrPort = $gsearchresult['Port'];
             $tdrIPs = $gsearchresult['IPs'];
             $tdrComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$tdrSiteName</td>";
             echo "  <td>$tdrVSN</td>";
             echo "  <td>$tdrSBC</td>";
             echo "  <td>$tdrStatus</td>";
             echo "  <td>$tdrSBCPairName</td>";
             echo "  <td>$tdrTDRServerName</td>";
             echo "  <td>$tdrPort</td>";
             echo "  <td>$tdrIPs</td>";
             echo "  <td>$tdrComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "VSN") {
      foreach ( $gSearchResults as $gsearchresult ) {
             $vsnSiteName = $gsearchresult['SiteName'];
             $vsnVSN = $gsearchresult['VSN'];
             $vsnDescription = $gsearchresult['Description'];
             $vsnComment = $gsearchresult['Comment'];

             echo "<tr>";
             echo "  <td>$vsnSiteName</td>";
             echo "  <td>$vsnVSN</td>";
             echo "  <td>$vsnDescription</td>";
             echo "  <td>$vsnComment</td>";
             echo "</tr>";
      }

} else if ($tableKey == "WizProgress") {
      foreach ( $gSearchResults as $gsearchresult ) {
             $wizSiteName = $gsearchresult['SiteName'];
             $wizVSN = $gsearchresult['VSN'];
             $wizTabsComplete = $gsearchresult['TabsComplete'];
             $wizGen = $gsearchresult['Gen'];

             echo "<tr>";
             echo "  <td>$wizSiteName</td>";
             echo "  <td>$wizVSN</td>";
             echo "  <td>$wizTabsComplete</td>";
             echo "  <td>$wizGen</td>";
             echo "</tr>";
      }


  
}


echo "                                     </tbody>";
echo "                                   </table>";
echo "                                 </div>";
echo "                               </td>";
echo "                             </tr>";
echo "                           </tbody>";
echo "                         </table>";
echo "                     </div>";
echo "                   </td>";
echo "                  </tr>";

    }

echo "              </tbody>";
echo "            </table>";

echo "          </div>";
echo "        </div>";
echo "";
echo "      </div>";
echo "    </div>";
echo "";
echo "  </div>";
echo "</div> ";

    
  }

}
