<?php

Namespace Portal\Controller\Ajax;

require_once VZP_LIB . '/DataTables/DataTables.php';

use \DataTables\Editor,
    \DataTables\Editor\Field,
    \DataTables\Editor\Format,
    \DataTables\Editor\Join,
    \DataTables\Editor\Upload,
    \DataTables\Editor\Validate;

class UserManagement {

   public static function ajax_usermanagement_roles_controller() {
   
       $db = $GLOBALS['db'];
   
       $editor = \DataTables\Editor::inst( $db, 'roles', "role_id" ) // these are the db connection, the table name, and the primary key for that table.
           ->fields( \DataTables\Editor\Field::inst( 'role_name' )->validator( 'Validate::notEmpty' ),
                     \DataTables\Editor\Field::inst( 'role_description' )->validator( 'Validate::notEmpty' ),
                     \DataTables\Editor\Field::inst( 'EditedBy' )
                   )
           ->process( $_POST )
           ->json();
   }

   public static function ajax_usermanagement_perms_controller() {

       $db = $GLOBALS['db'];

       $editor = \DataTables\Editor::inst( $db, 'permissions', "permission_id" ) // these are the db connection, the table name, and the primary key for that table.
           ->fields( \DataTables\Editor\Field::inst( 'permission_description' )->validator( 'Validate::notEmpty' ),
                     \DataTables\Editor\Field::inst( 'EditedBy' )
                   )
           ->process( $_POST )
           ->json();
   }
}