<?php

Namespace Portal\Controller\Ajax;

require_once VZP_LIB . '/DataTables/DataTables.php';

use \DataTables\Editor,
    \DataTables\Editor\Field,
    \DataTables\Editor\Format,
    \DataTables\Editor\Join,
    \DataTables\Editor\Validate;


class Network {

   public static function ajax_subnet_details_controller() {
       $json = $_GET['json'];
       $group = json_decode($json);
   
       switch (strtolower($group[1])) {
           case 'public':
               $query_table = 'IP_Public';
               break;
           case 'private':
               $query_table = 'IP_Private';
               break;
           case 'idn':
               $query_table = 'IP_IDN';
               break;
       }
   
       $query = "SELECT Subnet, Netmask FROM {$query_table} WHERE SiteName = '{$_SESSION['siteName']}' and VLANName = '{$group[0]}' LIMIT 1";
       $stmt = \Portal\DB::fetch_array($query);
       $stmt = array_pop($stmt);
       echo($stmt["Subnet"] . $stmt["Netmask"]);
   
   }

   public static function ajax_subnetadv_controller() {
   
       $db = $GLOBALS['db'];
       
       $editor = \DataTables\Editor::inst( $db, 'SubnetAdv',"id" ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'SubnetName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'Network' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'PublicIPAdv' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       if ($_SESSION['VSN'] == 'all') {
           $out = $editor
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       } else {
           $out = $editor
               ->where("VSN",$_SESSION['VSN'])
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       }
   }

   public static function ajax_dns_trap_controller() {
   
       $db = $GLOBALS['db'];
   
       $editor = \DataTables\Editor::inst( $db, 'DNSTrapLog' ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Function' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Tag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( function ( $val, $data, $opts ) { return tag_validate($val); }),
               \DataTables\Editor\Field::inst( 'HostIP' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::ip'),
               \DataTables\Editor\Field::inst( 'Reference' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Name' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       if ($_SESSION['VSN'] == 'all') {
           $out = $editor
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       } else {
           $out = $editor
               ->where("VSN",$_SESSION['VSN'])
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       }
   }

   public static function ajax_pubip_controller() {
   
       $db = $GLOBALS['db'];
       
       $editor = \DataTables\Editor::inst( $db, 'IP_Public' ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VLANName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VLANID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::numeric'),
               \DataTables\Editor\Field::inst( 'Subnet' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::ip'),
               \DataTables\Editor\Field::inst( 'NetmaskTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( function ( $val, $data, $opts ) { return tag_validate($val); }),
               \DataTables\Editor\Field::inst( 'Netmask' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'AddressTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( function ( $val, $data, $opts ) { return tag_validate($val); }),
               \DataTables\Editor\Field::inst( 'IPAddress' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::ip' ),
               \DataTables\Editor\Field::inst( 'Device' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'DNSName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Purpose' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       if ($_SESSION['VSN'] == 'all') {
           $out = $editor
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       } else {
           $out = $editor
               ->where("VSN",$_SESSION['VSN'])
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       }
   }

   public static function ajax_idnip_controller() {
   
       $db = $GLOBALS['db'];
       
       $editor = \DataTables\Editor::inst( $db, 'IP_IDN' ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VLANName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VLANID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::numeric'),
               \DataTables\Editor\Field::inst( 'Subnet' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::ip'),
               \DataTables\Editor\Field::inst( 'NetmaskTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( function ( $val, $data, $opts ) { return tag_validate($val); }),
               \DataTables\Editor\Field::inst( 'Netmask' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'AddressTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( function ( $val, $data, $opts ) { return tag_validate($val); }),
               \DataTables\Editor\Field::inst( 'IPAddress' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::ip' ),
               \DataTables\Editor\Field::inst( 'Device' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'DNSName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Purpose' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       if ($_SESSION['VSN'] == 'all') {
           $out = $editor
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       } else {
           $out = $editor
               ->where("VSN",$_SESSION['VSN'])
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       }
   }

   public static function ajax_privateip_controller() {
   
       $db = $GLOBALS['db'];
       
       $editor = \DataTables\Editor::inst( $db, 'IP_Private' ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VLANName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VLANID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::numeric'),
               \DataTables\Editor\Field::inst( 'Subnet' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::ip'),
               \DataTables\Editor\Field::inst( 'NetmaskTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( function ( $val, $data, $opts ) { return tag_validate($val); }),
               \DataTables\Editor\Field::inst( 'Netmask' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'AddressTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( function ( $val, $data, $opts ) { return tag_validate($val); }),
               \DataTables\Editor\Field::inst( 'IPAddress' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::ip' ),
               \DataTables\Editor\Field::inst( 'Device' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'DNSName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Purpose' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       if ($_SESSION['VSN'] == 'all') {
           $out = $editor
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       } else {
           $out = $editor
               ->where("VSN",$_SESSION['VSN'])
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       }
   }

   public static function ajax_ipschema_controller() {
   
       $db = $GLOBALS['db'];
       
       $editor = \DataTables\Editor::inst( $db, 'IPSchema' ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'NetworkType' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Name' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'Subnet' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::ip'),
               \DataTables\Editor\Field::inst( 'Netmask' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Purpose' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VLAN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::numeric'),
               \DataTables\Editor\Field::inst( 'Notes' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       if ($_SESSION['VSN'] == 'all') {
           $out = $editor
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       } else {
           $out = $editor
               ->where("VSN",$_SESSION['VSN'])
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       }
   }

   public static function ajax_ns_controller($ns_num) {
   
       $db = $GLOBALS['db'];
       
       $editor = \DataTables\Editor::inst( $db, 'NS5400PortAssignments',"id" ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'NS' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Interface' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'SubInterface' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'PortType' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Layer' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Slot' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'ConnectedDeviceType' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'ConnectedDeviceName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'ConnectedDevicePort' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'AddressTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( function ( $val, $data, $opts ) { return tag_validate($val); }),
               \DataTables\Editor\Field::inst( 'IPAddress' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::ip' ),
               \DataTables\Editor\Field::inst( 'VLANTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'SpeedDuplex' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Description' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       if ($_SESSION['VSN'] == 'all') {
           $out = $editor
               ->where("SiteName",$_SESSION['siteName'])
               ->where("NS",$ns_num)
               ->process( $_POST )
               ->json();
       } else {
           $out = $editor
               ->where("VSN",$_SESSION['VSN'])
               ->where("SiteName",$_SESSION['siteName'])
               ->where("NS",$ns_num)
               ->process( $_POST )
               ->json();
       }
   }

   public static function ajax_networkconn_controller() {
   
       $db = $GLOBALS['db'];
       
       $editor = \DataTables\Editor::inst( $db, 'NetworkConnections', 'id' ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'NodeDevice' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'NodeInterface' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'InterfaceTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( function ( $val, $data, $opts ) { return tag_validate($val); }),
               \DataTables\Editor\Field::inst( 'RemoteDeviceName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Network' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Speed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'RemoteDeviceType' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'RemoteDeviceInt' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'RemoteDeviceCircuit' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Description' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       if ($_SESSION['VSN'] == 'all') {
           $out = $editor
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       } else {
           $out = $editor
               ->where("VSN",$_SESSION['VSN'])
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       }
   }

    public static function ajax_vsn_controller() {

        $db = $GLOBALS['db'];

        $editor = \DataTables\Editor::inst( $db, 'VSN', 'id' ) // these are the db connection, the table name, and the primary key for that table.
            ->fields(
                \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                \DataTables\Editor\Field::inst( 'Description' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
            );

        if ($_SESSION['VSN'] == 'all') {
            $out = $editor
                ->where("SiteName",$_SESSION['siteName'])
                ->process( $_POST )
                ->json();
        } else {
            $out = $editor
                ->where("VSN",$_SESSION['VSN'])
                ->where("SiteName",$_SESSION['siteName'])
                ->process( $_POST )
                ->json();
        }
    }
}