<?php

Namespace Portal\Controller\Ajax;

require_once VZP_LIB . '/DataTables/DataTables.php';

use \DataTables\Editor,
    \DataTables\Editor\Field,
    \DataTables\Editor\Format,
    \DataTables\Editor\Join,
    \DataTables\Editor\Validate;


class Audit {

   public static function ajax_privateip_audit_controller($audit) {
       if ($audit == "audit") {
           $query = "INSERT INTO IP_Private_Audit
               SET SiteName='".\Portal\DB::escape($_POST['SiteName'])."',
               VSN='".\Portal\DB::escape($_POST['VSN'])."',
               VLANName='".\Portal\DB::escape($_POST['VLANName'])."',
               VLANID='".\Portal\DB::escape($_POST['VLANID'])."',
               Subnet='".\Portal\DB::escape($_POST['Subnet'])."',
               NetmaskTag='".\Portal\DB::escape($_POST['NetmaskTag'])."',
               Netmask='".\Portal\DB::escape($_POST['Netmask'])."',
               AddressTag='".\Portal\DB::escape($_POST['AddressTag'])."',
               IPAddress='".\Portal\DB::escape($_POST['IPAddress'])."',
               Device='".\Portal\DB::escape($_POST['Device'])."',
               DNSName='".\Portal\DB::escape($_POST['DNSName'])."',
               Purpose='".\Portal\DB::escape($_POST['Purpose'])."',
               EditedBy='".\Portal\DB::escape($_POST['EditedBy'])."',
               Comment='".\Portal\DB::escape($_POST['Comment'])."',
               RowID='".\Portal\DB::escape(substr($_POST['DT_RowId'], 4))."',
               Created='".\Portal\DB::escape($_POST['Created'])."',
               Removed='".\Portal\DB::escape($_POST['Removed'])."'";
           \Portal\DB::run($query);
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'IP_Private_Audit' ) // these are the db connection, the table name, and the primary key for that table.
               ->fields(
                   \DataTables\Editor\Field::inst( 'RowID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VLANName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VLANID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Subnet' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'NetmaskTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Netmask' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'AddressTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'IPAddress' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::ip' ),
                   \DataTables\Editor\Field::inst( 'Device' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'DNSName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Purpose' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
               );
   
           if ($_SESSION['VSN'] == 'all' || $_SESSION['VSN'] == 'audit') {
               $out = $editor
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           } else {
               $out = $editor
                   ->where("VSN",$_SESSION['VSN'])
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           }
       }
   }

   public static function ajax_cardslotting_audit_controller($audit) {
       if ($audit == "audit") {
   
           extract(array_map("html_decode", $_POST), EXTR_PREFIX_ALL, "post");
   
           $audit_entry = \ORM::for_table('CardSlotting_Audit')->create();
   
           $audit_entry->SiteName = $post_SiteName;
           $audit_entry->Slot = $post_Slot;
           $audit_entry->CardType = $post_CardType;
           $audit_entry->InterfaceNum = $post_InterfaceNum;
           $audit_entry->Device = $post_Device;
           $audit_entry->MIC = $post_MIC;
           $audit_entry->EditedBy = $post_EditedBy;
           $audit_entry->Comment = $post_Comment;
           $audit_entry->RowID = substr($post_DT_RowId, 4);
           $audit_entry->Created = $post_Created;
           $audit_entry->Removed = $post_Removed;
   
           $audit_entry->save();
   
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'CardSlotting_Audit',"id" ) // these are the db connection, the table name, and the primary key for that table.
               ->fields(
                   \DataTables\Editor\Field::inst( 'RowID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Slot' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'CardType' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'InterfaceNum' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Device' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'MIC' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
               );
   
           if ($_SESSION['VSN'] == 'all' || $_SESSION['VSN'] == 'audit') {
               $out = $editor
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           } else {
               $out = $editor
                   ->where("VSN",$_SESSION['VSN'])
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           }
       }
   }

   public static function ajax_subnetadv_audit_controller($audit) {
       if ($audit == "audit") {
   
           extract(array_map("html_decode", $_POST), EXTR_PREFIX_ALL, "post");
   
           $audit_entry = \ORM::for_table('SubnetAdv_Audit')->create();
   
           $audit_entry->SiteName = $post_SiteName;
           $audit_entry->VSN = $post_VSN;
           $audit_entry->SubnetName = $post_SubnetName;
           $audit_entry->Network = $post_Network;
           $audit_entry->PublicIPAdv = $post_PublicIPAdv;
           $audit_entry->EditedBy = $post_EditedBy;
           $audit_entry->Comment = $post_Comment;
           $audit_entry->RowID = substr($post_DT_RowId, 4);
           $audit_entry->Created = $post_Created;
           $audit_entry->Removed = $post_Removed;
   
           $audit_entry->save();
   
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'SubnetAdv_Audit',"id" ) // these are the db connection, the table name, and the primary key for that table.
               ->fields(
                   \DataTables\Editor\Field::inst( 'RowID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SubnetName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Network' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'PublicIPAdv' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
               );
   
           if ($_SESSION['VSN'] == 'all' || $_SESSION['VSN'] == 'audit') {
               $out = $editor
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           } else {
               $out = $editor
                   ->where("VSN",$_SESSION['VSN'])
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           }
       }
   }

   public static function ajax_dns_trap_audit_controller($audit) {
       if ($audit == "audit") {
           $query = "INSERT INTO DNSTrapLog_Audit
               SET SiteName='".\Portal\DB::escape($_POST['SiteName'])."',
               VSN='".\Portal\DB::escape($_POST['VSN'])."',
               Function='".\Portal\DB::escape($_POST['Function'])."',
               Tag='".\Portal\DB::escape($_POST['Tag'])."',
               HostIP='".\Portal\DB::escape($_POST['HostIP'])."',
               Reference='".\Portal\DB::escape($_POST['Reference'])."',
               Name='".\Portal\DB::escape($_POST['Name'])."',
               EditedBy='".\Portal\DB::escape($_POST['EditedBy'])."',
               Comment='".\Portal\DB::escape($_POST['Comment'])."',
               RowID='".\Portal\DB::escape(substr($_POST['DT_RowId'], 4))."',
               Created='".\Portal\DB::escape($_POST['Created'])."',
               Removed='".\Portal\DB::escape($_POST['Removed'])."'";
           \Portal\DB::run($query);
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'DNSTrapLog_Audit' ) // these are the db connection, the table name, and the primary key for that table.
               ->fields(
                   \DataTables\Editor\Field::inst( 'RowID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Function' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Tag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'HostIP' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Reference' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Name' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
               );
   
           if ($_SESSION['VSN'] == 'all' || $_SESSION['VSN'] == 'audit') {
               $out = $editor
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           } else {
               $out = $editor
                   ->where("VSN",$_SESSION['VSN'])
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           }
       }
   }

   public static function ajax_pubip_audit_controller($audit) {
       if ($audit == "audit") {
           $query = "INSERT INTO IP_Public_Audit
               SET SiteName='".\Portal\DB::escape($_POST['SiteName'])."',
               VSN='".\Portal\DB::escape($_POST['VSN'])."',
               VLANName='".\Portal\DB::escape($_POST['VLANName'])."',
               VLANID='".\Portal\DB::escape($_POST['VLANID'])."',
               Subnet='".\Portal\DB::escape($_POST['Subnet'])."',
               NetmaskTag='".\Portal\DB::escape($_POST['NetmaskTag'])."',
               Netmask='".\Portal\DB::escape($_POST['Netmask'])."',
               AddressTag='".\Portal\DB::escape($_POST['AddressTag'])."',
               IPAddress='".\Portal\DB::escape($_POST['IPAddress'])."',
               Device='".\Portal\DB::escape($_POST['Device'])."',
               DNSName='".\Portal\DB::escape($_POST['DNSName'])."',
               Purpose='".\Portal\DB::escape($_POST['Purpose'])."',
               EditedBy='".\Portal\DB::escape($_POST['EditedBy'])."',
               Comment='".\Portal\DB::escape($_POST['Comment'])."',
               RowID='".\Portal\DB::escape(substr($_POST['DT_RowId'], 4))."',
               Created='".\Portal\DB::escape($_POST['Created'])."',
               Removed='".\Portal\DB::escape($_POST['Removed'])."'";
           \Portal\DB::run($query);
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'IP_Public_Audit' ) // these are the db connection, the table name, and the primary key for that table.
               ->fields(
                   \DataTables\Editor\Field::inst( 'RowID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VLANName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VLANID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Subnet' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'NetmaskTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Netmask' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'AddressTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'IPAddress' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::ip' ),
                   \DataTables\Editor\Field::inst( 'Device' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'DNSName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Purpose' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
               );
   
           if ($_SESSION['VSN'] == 'all' || $_SESSION['VSN'] == 'audit') {
               $out = $editor
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           } else {
               $out = $editor
                   ->where("VSN",$_SESSION['VSN'])
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           }
       }
   }
   
   public static function ajax_idnip_audit_controller($audit) {
       if ($audit == "audit") {
           $query = "INSERT INTO IP_IDN_Audit
               SET SiteName='".\Portal\DB::escape($_POST['SiteName'])."',
               VSN='".\Portal\DB::escape($_POST['VSN'])."',
               VLANName='".\Portal\DB::escape($_POST['VLANName'])."',
               VLANID='".\Portal\DB::escape($_POST['VLANID'])."',
               Subnet='".\Portal\DB::escape($_POST['Subnet'])."',
               NetmaskTag='".\Portal\DB::escape($_POST['NetmaskTag'])."',
               Netmask='".\Portal\DB::escape($_POST['Netmask'])."',
               AddressTag='".\Portal\DB::escape($_POST['AddressTag'])."',
               IPAddress='".\Portal\DB::escape($_POST['IPAddress'])."',
               Device='".\Portal\DB::escape($_POST['Device'])."',
               DNSName='".\Portal\DB::escape($_POST['DNSName'])."',
               Purpose='".\Portal\DB::escape($_POST['Purpose'])."',
               EditedBy='".\Portal\DB::escape($_POST['EditedBy'])."',
               Comment='".\Portal\DB::escape($_POST['Comment'])."',
               RowID='".\Portal\DB::escape(substr($_POST['DT_RowId'], 4))."',
               Created='".\Portal\DB::escape($_POST['Created'])."',
               Removed='".\Portal\DB::escape($_POST['Removed'])."'";
           \Portal\DB::run($query);
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'IP_IDN_Audit' ) // these are the db connection, the table name, and the primary key for that table.
               ->fields(
                   \DataTables\Editor\Field::inst( 'RowID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VLANName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VLANID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Subnet' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'NetmaskTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Netmask' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'AddressTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'IPAddress' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::ip' ),
                   \DataTables\Editor\Field::inst( 'Device' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'DNSName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Purpose' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
               );
   
           if ($_SESSION['VSN'] == 'all' || $_SESSION['VSN'] == 'audit') {
               $out = $editor
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           } else {
               $out = $editor
                   ->where("VSN",$_SESSION['VSN'])
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           }
       }
   }

   public static function ajax_ipschema_audit_controller($audit) {
       if ($audit == "audit") {
           $query = "INSERT INTO IPSchema_Audit
               SET SiteName='".\Portal\DB::escape($_POST['SiteName'])."',
               VSN='".\Portal\DB::escape($_POST['VSN'])."',
               NetworkType='".\Portal\DB::escape($_POST['NetworkType'])."',
               Name='".\Portal\DB::escape($_POST['Name'])."',
               Subnet='".\Portal\DB::escape($_POST['Subnet'])."',
               Netmask='".\Portal\DB::escape($_POST['Netmask'])."',
               Purpose='".\Portal\DB::escape($_POST['Purpose'])."',
               VLAN='".\Portal\DB::escape($_POST['VLAN'])."',
               Notes='".\Portal\DB::escape($_POST['Notes'])."',
               EditedBy='".\Portal\DB::escape($_POST['EditedBy'])."',
               Comment='".\Portal\DB::escape($_POST['Comment'])."',
               RowID='".\Portal\DB::escape(substr($_POST['DT_RowId'], 4))."',
               Created='".\Portal\DB::escape($_POST['Created'])."',
               Removed='".\Portal\DB::escape($_POST['Removed'])."'";
           \Portal\DB::run($query);
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'IPSchema_Audit' ) // these are the db connection, the table name, and the primary key for that table.
               ->fields(
                   \DataTables\Editor\Field::inst( 'RowID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'NetworkType' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Name' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Subnet' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::ip'),
                   \DataTables\Editor\Field::inst( 'Netmask' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Purpose' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VLAN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::numeric'),
                   \DataTables\Editor\Field::inst( 'Notes' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
               );
   
           if ($_SESSION['VSN'] == 'all' || $_SESSION['VSN'] == 'audit') {
               $out = $editor
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           } else {
               $out = $editor
                   ->where("VSN",$_SESSION['VSN'])
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           }
       }
   }

   public static function ajax_ns_audit_controller($ns_num, $audit) {
       if ($audit == "audit") {
           $query = "INSERT INTO NS5400PortAssignments_Audit
               SET SiteName='".\Portal\DB::escape($_POST['SiteName'])."',
               VSN='".\Portal\DB::escape($_POST['VSN'])."',
               NS='".\Portal\DB::escape($_POST['NS'])."',
               Interface='".\Portal\DB::escape($_POST['Interface'])."',
               SubInterface='".\Portal\DB::escape($_POST['SubInterface'])."',
               PortType='".\Portal\DB::escape($_POST['PortType'])."',
               Layer='".\Portal\DB::escape($_POST['Layer'])."',
               Slot='".\Portal\DB::escape($_POST['Slot'])."',
               ConnectedDeviceType='".\Portal\DB::escape($_POST['ConnectedDeviceType'])."',
               ConnectedDeviceName='".\Portal\DB::escape($_POST['ConnectedDeviceName'])."',
               ConnectedDevicePort='".\Portal\DB::escape($_POST['ConnectedDevicePort'])."',
               AddressTag='".\Portal\DB::escape($_POST['AddressTag'])."',
               IPAddress='".\Portal\DB::escape($_POST['IPAddress'])."',
               VLANTag='".\Portal\DB::escape($_POST['VLANTag'])."',
               SpeedDuplex='".\Portal\DB::escape($_POST['SpeedDuplex'])."',
               Description='".\Portal\DB::escape($_POST['Description'])."',
               EditedBy='".\Portal\DB::escape($_POST['EditedBy'])."',
               Comment='".\Portal\DB::escape($_POST['Comment'])."',
               RowID='".\Portal\DB::escape(substr($_POST['DT_RowId'], 4))."',
               Created='".\Portal\DB::escape($_POST['Created'])."',
               Removed='".\Portal\DB::escape($_POST['Removed'])."'";
           \Portal\DB::run($query);
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'NS5400PortAssignments_Audit',"id" ) // these are the db connection, the table name, and the primary key for that table.
               ->fields(
                   \DataTables\Editor\Field::inst( 'RowID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'NS' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Interface' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SubInterface' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'PortType' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Layer' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Slot' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'ConnectedDeviceType' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'ConnectedDeviceName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'ConnectedDevicePort' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'AddressTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( function ( $val, $data, $opts ) { return tag_validate($val); }),
                   \DataTables\Editor\Field::inst( 'IPAddress' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::ip' ),
                   \DataTables\Editor\Field::inst( 'VLANTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SpeedDuplex' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Description' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
               );
   
           if ($_SESSION['VSN'] == 'all' || $_SESSION['VSN'] == 'audit') {
               $out = $editor
                   ->where("SiteName",$_SESSION['siteName'])
                   ->where("NS",$ns_num)
                   ->process( $_POST )
                   ->json();
           } else {
               $out = $editor
                   ->where("VSN",$_SESSION['VSN'])
                   ->where("SiteName",$_SESSION['siteName'])
                   ->where("NS",$ns_num)
                   ->process( $_POST )
                   ->json();
           }
       }
   }

   public static function ajax_custckt_audit_controller($audit) {
       if ($audit == "audit") {
           $query = "INSERT INTO CustomerCircuits_Audit
               SET SiteName='".\Portal\DB::escape($_POST['SiteName'])."',
               VSN='".\Portal\DB::escape($_POST['VSN'])."',
               CustomerCktType='".\Portal\DB::escape($_POST['CustomerCktType'])."',
               Speed='".\Portal\DB::escape($_POST['Speed'])."',
               CDR='".\Portal\DB::escape($_POST['CDR'])."',
               CircuitID='".\Portal\DB::escape($_POST['CircuitID'])."',
               MX960Router='".\Portal\DB::escape($_POST['MX960Router'])."',
               RemoteRouter='".\Portal\DB::escape($_POST['RemoteRouter'])."',
               EditedBy='".\Portal\DB::escape($_POST['EditedBy'])."',
               Comment='".\Portal\DB::escape($_POST['Comment'])."',
               RowID='".\Portal\DB::escape(substr($_POST['DT_RowId'], 4))."',
               Created='".\Portal\DB::escape($_POST['Created'])."',
               Removed='".\Portal\DB::escape($_POST['Removed'])."'";
           \Portal\DB::run($query);
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'CustomerCircuits_Audit' ) // these are the db connection, the table name, and the primary key for that table.
               ->fields(
                   \DataTables\Editor\Field::inst( 'RowID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'CustomerCktType' )->validator( '\DataTables\Editor\Validate::required' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Speed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'CDR' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'CircuitID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'MX960Router' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'RemoteRouter' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
               );
   
           if ($_SESSION['VSN'] == 'all' || $_SESSION['VSN'] == 'audit') {
               $out = $editor
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           } else {
               $out = $editor
                   ->where("VSN",$_SESSION['VSN'])
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           }
       }
   }

   public static function ajax_networkconn_audit_controller($audit) {
       if ($audit == "audit") {
   /*
           extract(array_map("html_decode", $_POST), EXTR_PREFIX_ALL, "post");
   
           $audit_entry = ORM::for_table('NetworkConnections_Audit')->create();
   
           $audit_entry->SiteName = $post_SiteName;
           $audit_entry->VSN = $post_VSN;
   
           $audit_entry-> = $post_;
   
           $audit_entry->EditedBy = $post_EditedBy;
           $audit_entry->Comment = $post_Comment;
           $audit_entry->RowID = substr($post_DT_RowId, 4);
           $audit_entry->Created = $post_Created;
           $audit_entry->Removed = $post_Removed;
   
           $audit_entry->save();
   */
           extract(array_map("html_decode", $_POST), EXTR_PREFIX_ALL, "post");
   
           $audit_entry = \ORM::for_table('NetworkConnections_Audit')->create();
   
           $audit_entry->SiteName = $post_SiteName;
           $audit_entry->VSN = $post_VSN;
           $audit_entry->NodeDevice = $post_NodeDevice;
           $audit_entry->NodeInterface = $post_NodeInterface;
           $audit_entry->InterfaceTag = $post_InterfaceTag;
           $audit_entry->RemoteDeviceName = $post_RemoteDeviceName;
           $audit_entry->Network = $post_Network;
           $audit_entry->Speed = $post_Speed;
           $audit_entry->RemoteDeviceType = $post_RemoteDeviceType;
           $audit_entry->RemoteDeviceInt = $post_RemoteDeviceInt;
           $audit_entry->RemoteDeviceCircuit = $post_RemoteDeviceCircuit;
           $audit_entry->Description = $post_Description;
           $audit_entry->EditedBy = $post_EditedBy;
           $audit_entry->Comment = $post_Comment;
           $audit_entry->RowID = substr($post_DT_RowId, 4);
           $audit_entry->Created = $post_Created;
           $audit_entry->Removed = $post_Removed;
   
           $audit_entry->save();
   
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'NetworkConnections_Audit', 'id' ) // these are the db connection, the table name, and the primary key for that table.
               ->fields(
                   \DataTables\Editor\Field::inst( 'RowID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'NodeDevice' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'NodeInterface' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'InterfaceTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'RemoteDeviceName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Network' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Speed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'RemoteDeviceType' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'RemoteDeviceInt' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'RemoteDeviceCircuit' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Description' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
               );
   
           if ($_SESSION['VSN'] == 'all' || $_SESSION['VSN'] == 'audit') {
               $out = $editor
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           } else {
               $out = $editor
                   ->where("VSN",$_SESSION['VSN'])
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           }
       }
   }

   public static function ajax_customeraddressing_audit_controller($audit) {
       if ($audit == "audit") {
   
           extract(array_map("html_decode", $_POST), EXTR_PREFIX_ALL, "post");
   
           $audit_entry = \ORM::for_table('CustomerAddressing_Audit')->create();
   
           $audit_entry->SiteName = $post_SiteName;
           $audit_entry->VSN = $post_VSN;
           $audit_entry->SPIP = $post_SPIP;
           $audit_entry->HostOffset = $post_HostOffset;
           $audit_entry->IPAddress = $post_IPAddress;
           $audit_entry->Device = $post_Device;
           $audit_entry->Description = $post_Description;
           $audit_entry->EditedBy = $post_EditedBy;
           $audit_entry->Comment = $post_Comment;
           $audit_entry->RowID = substr($post_DT_RowId, 4);
           $audit_entry->Created = $post_Created;
           $audit_entry->Removed = $post_Removed;
   
           $audit_entry->save();
   
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'CustomerAddressing_Audit',"id" ) // these are the db connection, the table name, and the primary key for that table.
               ->fields(
                   \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'RowID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SPIP' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'HostOffset' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'IPAddress' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::ip' ),
                   \DataTables\Editor\Field::inst( 'Device' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Description' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
               );
   
           if ($_SESSION['VSN'] == 'all' || $_SESSION['VSN'] == 'audit') {
               $out = $editor
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           } else {
               $out = $editor
                   ->where("VSN",$_SESSION['VSN'])
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           }
       }
   }

   public static function ajax_mastertags_audit_controller($audit) {
       if ($audit == "audit") {
   
           extract(array_map("html_decode", $_POST), EXTR_PREFIX_ALL, "post");
   
           $audit_entry = \ORM::for_table('MasterTags_Audit')->create();
   
           $audit_entry->Tag = $post_Tag;
           $audit_entry->EditedBy = $post_EditedBy;
           $audit_entry->Comment = $post_Comment;
           $audit_entry->RowID = substr($post_DT_RowId, 4);
           $audit_entry->Created = $post_Created;
           $audit_entry->Removed = $post_Removed;
   
           $audit_entry->save();
   
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'MasterTags_Audit' ) // these are the db connection, the table name, and the primary key for that table.
               ->fields(
                   \DataTables\Editor\Field::inst( 'RowID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Tag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
               );
   
           $out = $editor
               ->process( $_POST )
               ->json();
       }
   }

   public static function ajax_tdrinfo_audit_controller($audit) {
       if ($audit == "audit") {
   
           extract(array_map("html_decode", $_POST), EXTR_PREFIX_ALL, "post");
   
           $audit_entry = \ORM::for_table('TDR_Audit')->create();
   
           $audit_entry->SiteName = $post_SiteName;
           $audit_entry->VSN = $post_VSN;
           $audit_entry->SBC = $post_SBC;
           $audit_entry->Status = $post_Status;
           $audit_entry->SBCPairName = $post_SBCPairName;
           $audit_entry->TDRServerName = $post_TDRServerName;
           $audit_entry->TDRServerIP = $post_TDRServerIP;
           $audit_entry->Port = $post_Port;
           $audit_entry->IPs = $post_IPs;
           $audit_entry->EditedBy = $post_EditedBy;
           $audit_entry->Comment = $post_Comment;
           $audit_entry->RowID = substr($post_DT_RowId, 4);
           $audit_entry->Created = $post_Created;
           $audit_entry->Removed = $post_Removed;
   
           $audit_entry->save();
   
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'TDR_Audit',"id" ) // these are the db connection, the table name, and the primary key for that table.
               ->fields(
                   \DataTables\Editor\Field::inst( 'RowID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SBC' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Status' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SBCPairName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'TDRServerName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'TDRServerIP' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Port' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'IPs' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
               );
   
           if ($_SESSION['VSN'] == 'all' || $_SESSION['VSN'] == 'audit') {
               $out = $editor
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           } else {
               $out = $editor
                   ->where("VSN",$_SESSION['VSN'])
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           }
       }
   }

   public static function ajax_async_audit_controller($audit) {
       if ($audit == "audit") {
   
           extract(array_map("html_decode", $_POST), EXTR_PREFIX_ALL, "post");
   
           $audit_entry = \ORM::for_table('Async_Audit')->create();
   
           $audit_entry->SiteName = $post_SiteName;
           $audit_entry->VSN = $post_VSN;
           $audit_entry->Serial = $post_Serial;
           $audit_entry->Bay = $post_Bay;
           $audit_entry->Hostname = $post_Hostname;
           $audit_entry->Description = $post_Description;
           $audit_entry->ASYNC = $post_ASYNC;
           $audit_entry->OSVersion = $post_OSVersion;
           $audit_entry->EditedBy = $post_EditedBy;
           $audit_entry->Comment = $post_Comment;
           $audit_entry->RowID = substr($post_DT_RowId, 4);
           $audit_entry->Created = $post_Created;
           $audit_entry->Removed = $post_Removed;
   
           $audit_entry->save();
   
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'Async_Audit',"id" ) // these are the db connection, the table name, and the primary key for that table.
               ->fields(
                   \DataTables\Editor\Field::inst( 'RowID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Serial' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Bay' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Hostname' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Description' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'ASYNC' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'OSVersion' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
               );
   
           if ($_SESSION['VSN'] == 'all' || $_SESSION['VSN'] == 'audit') {
               $out = $editor
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           } else {
               $out = $editor
                   ->where("VSN",$_SESSION['VSN'])
                   ->where("SiteName",$_SESSION['siteName'])
                   ->process( $_POST )
                   ->json();
           }
       }
   }

   public static function ajax_mx960_audit_controller($mx_num, $audit) {
       if ($audit == "audit") {
   
           extract(array_map("html_decode", $_POST), EXTR_PREFIX_ALL, "post");
   
           $audit_entry = \ORM::for_table('MX960PortAssignments_Audit')->create();
   
           $audit_entry->SiteName = $post_SiteName;
           $audit_entry->VSN = $post_VSN;
           $audit_entry->MX = html_decode($mx_num);
           $audit_entry->DPC = $post_DPC;
           $audit_entry->Interface = $post_Interface;
           $audit_entry->SubInterface = $post_SubInterface;
           $audit_entry->InterfaceTag = $post_InterfaceTag;
           $audit_entry->PortType = $post_PortType;
           $audit_entry->Layer = $post_Layer;
           $audit_entry->VSNInstance = $post_VSNInstance;
           $audit_entry->ConnectedDeviceType = $post_ConnectedDeviceType;
           $audit_entry->ConnectedDeviceName = $post_ConnectedDeviceName;
           $audit_entry->ConnectedDevicePort = $post_ConnectedDevicePort;
           $audit_entry->AddressTag = $post_AddressTag;
           $audit_entry->IPAddress = $post_IPAddress;
           $audit_entry->VLANTag = $post_VLANTag;
           $audit_entry->SpeedDuplex = $post_SpeedDuplex;
           $audit_entry->DescriptionTag = $post_DescriptionTag;
           $audit_entry->Description = $post_Description;
           $audit_entry->EditedBy = $post_EditedBy;
           $audit_entry->Comment = $post_Comment;
           $audit_entry->RowID = substr($post_DT_RowId, 4);
           $audit_entry->Created = $post_Created;
           $audit_entry->Removed = $post_Removed;
   
           $audit_entry->save();
   
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'MX960PortAssignments_Audit',"id" )
               ->fields(
                   \DataTables\Editor\Field::inst( 'RowID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'MX' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Interface' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'InterfaceTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SubInterface' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'PortType' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Layer' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'VSNInstance' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'ConnectedDeviceType' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'ConnectedDeviceName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'ConnectedDevicePort' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'AddressTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'IPAddress' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::ip' ),
                   \DataTables\Editor\Field::inst( 'VLANTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'SpeedDuplex' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'DescriptionTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Description' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
               );
   
           if ($_SESSION['VSN'] == 'all' || $_SESSION['VSN'] == 'audit') {
               $out = $editor
                   ->where("SiteName",$_SESSION['siteName'])
                   ->where("MX",$mx_num)
                   ->process( $_POST )
                   ->json();
           } else {
               $out = $editor
                   ->where("VSN",$_SESSION['VSN'])
                   ->where("SiteName",$_SESSION['siteName'])
                   ->where("MX",$mx_num)
                   ->process( $_POST )
                   ->json();
           }
       }
   }


   public static function ajax_site_info_audit_controller($audit) {
       if ($audit == "audit") {
   
           extract(array_map("html_decode", $_POST), EXTR_PREFIX_ALL, "post");
   
           $audit_entry = \ORM::for_table('SiteInfo_Audit')->create();
   
           $audit_entry->SiteName = $post_SiteName;
           $audit_entry->VSN = $post_VSN;
           $audit_entry->SiteCode = $post_SiteCode;
           $audit_entry->ClliCode = $post_ClliCode;
           $audit_entry->SiteMux = $post_SiteMux;
           $audit_entry->SiteAddress = $post_SiteAddress;
           $audit_entry->ContactName = $post_ContactName;
           $audit_entry->ContactPhone = $post_ContactPhone;
           $audit_entry->ContactEmail = $post_ContactEmail;
           $audit_entry->SiteGroup = $post_SiteGroup;
           $audit_entry->Lab = $post_Lab;
           $audit_entry->EditedBy = html_decode($_SESSION['user']['username']);
           $audit_entry->Comment = $post_Comment;
           $audit_entry->Created = $post_Created;
           $audit_entry->Removed = $post_Removed;
   
           $audit_entry->save();
   
       } else if ($audit == "view") {
       
           $db = $GLOBALS['db'];
           
           $editor = \DataTables\Editor::inst( $db, 'SiteInfo_Audit' )
               ->fields(
                   \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } ),
                   \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } ),
                   \DataTables\Editor\Field::inst( 'SiteCode' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } ),
                   \DataTables\Editor\Field::inst( 'ClliCode' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } ),
                   \DataTables\Editor\Field::inst( 'SiteMux' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } ),
                   \DataTables\Editor\Field::inst( 'SiteAddress' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } ),
                   \DataTables\Editor\Field::inst( 'ContactName' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } ),
                   \DataTables\Editor\Field::inst( 'ContactPhone' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } ),
                   \DataTables\Editor\Field::inst( 'ContactEmail' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } ),
                   \DataTables\Editor\Field::inst( 'SiteGroup' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } ),
                   \DataTables\Editor\Field::inst( 'Lab' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } ),
                   \DataTables\Editor\Field::inst( 'UpdatedTime' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } )->set( false ),
                   \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } ),
                   \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } ),
                   \DataTables\Editor\Field::inst( 'Removed' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } ),
                   \DataTables\Editor\Field::inst( 'Created' )->getFormatter( function ( $val, $data, $opts ) { return htmlentities($val, ENT_QUOTES); } )
               );
   
           $out = $editor
               ->where("SiteName", $_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       }
   }

    public static function ajax_roles_audit_controller($audit) {
        $db = $GLOBALS['db'];
        // todo
    }

    public static function ajax_perms_audit_controller($audit) {
        $db = $GLOBALS['db'];
        // todo
    }
}
