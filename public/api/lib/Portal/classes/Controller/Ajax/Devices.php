<?php

Namespace Portal\Controller\Ajax;

require_once VZP_LIB . '/DataTables/DataTables.php';

use \DataTables\Editor,
    \DataTables\Editor\Field,
    \DataTables\Editor\Format,
    \DataTables\Editor\Join,
    \DataTables\Editor\Validate;
    
    
class Devices {

   public static function ajax_cardslotting_controller() {
   
       $db = $GLOBALS['db'];
       
       $editor = \DataTables\Editor::inst( $db, 'CardSlotting',"id" ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Slot' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'CardType' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'InterfaceNum' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'Device' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'MIC' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       if ($_SESSION['VSN'] == 'all') {
           $out = $editor
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       } else {
           $out = $editor
               ->where("VSN",$_SESSION['VSN'])
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       }
   }

   public static function ajax_tdrinfo_controller() {
   
       $db = $GLOBALS['db'];
       
       $editor = \DataTables\Editor::inst( $db, 'TDR',"id" ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'SBC' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Status' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'SBCPairName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'TDRServerName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'TDRServerIP' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Port' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'IPs' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::ip'),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       if ($_SESSION['VSN'] == 'all') {
           $out = $editor
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       } else {
           $out = $editor
               ->where("VSN",$_SESSION['VSN'])
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       }
   }

   public static function ajax_async_controller() {

   
       $db = $GLOBALS['db'];
       
       $editor = \DataTables\Editor::inst( $db, 'Async',"id" ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Serial' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Bay' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::numeric'),
               \DataTables\Editor\Field::inst( 'Hostname' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'Description' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'ASYNC' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'OSVersion' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       if ($_SESSION['VSN'] == 'all') {
           $out = $editor
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       } else {
           $out = $editor
               ->where("VSN",$_SESSION['VSN'])
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       }
   }



   public static function ajax_nodedevicenames_controller() {


       $db = $GLOBALS['db'];
       
       $editor = \DataTables\Editor::inst( $db, 'NodeDeviceNames',"id" ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'PrimaryDeviceTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'NodeDeviceName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::numeric'),
               \DataTables\Editor\Field::inst( 'DeviceAbbreviation' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'DeviceType' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'DeviceDescription' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       if ($_SESSION['VSN'] == 'all') {
           $out = $editor
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       } else {
           $out = $editor
               ->where("VSN",$_SESSION['VSN'])
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       }
   }










   public static function ajax_mx960_controller($mx_num) {
   
       $db = $GLOBALS['db'];
       
       $editor = \DataTables\Editor::inst( $db, 'MX960PortAssignments',"id" )
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'MX' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Interface' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'InterfaceTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( function ( $val, $data, $opts ) { return tag_validate($val); }),
               \DataTables\Editor\Field::inst( 'SubInterface' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'PortType' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Layer' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VSNInstance' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'ConnectedDeviceType' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'ConnectedDeviceName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'ConnectedDevicePort' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'AddressTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'IPAddress' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VLANTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'SpeedDuplex' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'DescriptionTag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( function ( $val, $data, $opts ) { return tag_validate($val); }),
               \DataTables\Editor\Field::inst( 'Description' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::required')
           );
   
       if ($_SESSION['VSN'] == 'all') {
           $out = $editor
               ->where("SiteName",$_SESSION['siteName'])
               ->where("MX",$mx_num)
               ->process( $_POST )
               ->json();
       } else {
           $out = $editor
               ->where("VSN",$_SESSION['VSN'])
               ->where("SiteName",$_SESSION['siteName'])
               ->where("MX",$mx_num)
               ->process( $_POST )
               ->json();
       }
   }

}
