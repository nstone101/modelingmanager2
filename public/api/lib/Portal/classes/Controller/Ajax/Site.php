<?php

Namespace Portal\Controller\Ajax;

require_once VZP_LIB . '/DataTables/DataTables.php';

use \DataTables\Editor,
    \DataTables\Editor\Field,
    \DataTables\Editor\Format,
    \DataTables\Editor\Join,
    \DataTables\Editor\Validate;


class Site {

   public static function ajax_sitestatus_controller() {
   
       $db = $GLOBALS['db'];
       
       $editor = \DataTables\Editor::inst( $db, 'SiteStatus' ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::required'),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::required'),
               \DataTables\Editor\Field::inst( 'Phase' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::required'),
               \DataTables\Editor\Field::inst( 'Task' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::required'),
               \DataTables\Editor\Field::inst( 'Description' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'AssignedTo' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Status' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Progress' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'DelayReason' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'LinkURL' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'DateDue' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'DateCompleted' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )
           );
   
       $out = $editor
           ->where("SiteName",$_SESSION['siteName'])
           ->process( $_POST )
           ->json();
   }

   public static function ajax_ter_controller() {
   
       $db = $GLOBALS['db'];
       
       $editor = \DataTables\Editor::inst( $db, 'TER' ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SIT' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::required'),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::required'),
               \DataTables\Editor\Field::inst( 'DocID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::required'),
               \DataTables\Editor\Field::inst( 'RqmtsID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator('\DataTables\Editor\Validate::required'),
               \DataTables\Editor\Field::inst( 'TPLNumber' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'RqmtsDesc' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Result' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Testers' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'TestDate' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Description' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'ResultDesc' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       $out = $editor
           ->process( $_POST )
           ->json();
   }

   public static function ajax_site_info_controller() {
   
//       extract(array_map("html_decode", $_POST), EXTR_PREFIX_ALL, "post");
//       $node_info = \ORM::for_table('SiteInfo')->where('SiteName', $post_SiteName)->find_one();
       $node_info = \ORM::for_table('SiteInfo')->where('SiteName', $_SESSION['siteName'])->find_one();
   
    
       $node_info->SiteCode = $_POST['SiteCode'];
       $node_info->SiteMux = $_POST['SiteMux'];
       $node_info->ClliCode = $_POST['ClliCode'];
       $node_info->SiteAddress = $_POST['SiteAddress'];
       $node_info->ContactName = $_POST['ContactName'];
       $node_info->ContactPhone = $_POST['ContactPhone'];
       $node_info->ContactEmail = $_POST['ContactEmail'];
       $node_info->SiteGroup = $_POST['SiteGroup'];
       $node_info->SiteState = $_POST['SiteState'];
       $node_info->EditedBy = $_SESSION['user']['username'];
       $node_info->Comment = $_POST['Comment']; 

       $node_info->save();
       echo "success";
   }


   public static function ajax_track_overview_controller() {
   
   	// GRAB THE NODES THAT WE ARE ALREADY TRACKING FOR CHOICE SELECTION IN THE FORM.
   	$query = "SELECT SiteName, Status, Progress, DateDue FROM SiteStatus ORDER BY SiteName ASC";
    $status = \Portal\DB::fetch_array($query);
   
   	// Variables needed for calculation
   	$siteName = "";
   	$i = 0;
   	$sum = 0.00;
   	$count = 0;
   	$delayCount = 0;
   	$overdueCount = 0;
   	$today = strtotime('today');
   	foreach ($status as $row) {
     		// First run setup
     		if ($i == 0) {
       			$siteName = $row['SiteName'];
       			echo "<table class='table table-bordered'>
       			<thead>
       			<th>Site Name</th>
       			<th>Percent Complete</th>
       			<th>Delayed Tasks</th>
       			<th>Overdue Tasks</th>
       			</thead>
       			<tbody>
       			";
     		}
     
     		// Handle moving to a new site
     		if ($row['SiteName'] != $siteName) {
     
       			echo "<tr><td><a href='#' onclick='open_output_popup(\"".$siteName."\")'>".$siteName."</a></td><td>". round(($sum / $count) *100, 2) ."%</td><td>".$delayCount."</td><td>".$overdueCount ."</td></tr>";
       			// reset the count and sum since we are on a new site
       			$sum = 0;
       			$count = 0;
       			$delayCount = 0;
       			$overdueCount = 0;
       			$siteName = $row['SiteName'];
     		}
     
     		if (($row['Status'] == 'Complete') || ($row['Status']== 'N/A')){  // Add 1 to sum to indicate a completed task
     			  $sum++;
     		} else if (($row['Status'] == 'Delayed') || ($row['Status'] == 'In Progress')) { // Add the decimal value for the task based on the percentage complete stored in 'Progress'
     			 if ($row['Status'] == 'Delayed') {
     				   $delayCount++;
     			 }
     			 $sum += (str_replace('%', '', $row['Progress']) / 100);
     		}
     		if (($row['Status'] != 'Complete') && ($row['Status'] != 'N/A') && ($row['DateDue'] < date('Y-m-d'))) {
     			  $overdueCount++;
     		}
     		// Increase the count value.
     		$count++;
     
     		$i++;
     	}
     	echo "<tr><td><a href='#' onclick='open_output_popup(\"".$siteName."\")'>".$siteName."</a></td><td>". round(($sum / $count) *100, 2) ."%</td><td>".$delayCount."</td><td>".$overdueCount ."</td></tr>";
     	echo "</tbody></table>";
   }


   public static function ajax_track_self_controller() {   
   	  // GRAB THE NODES THAT WE ARE ALREADY TRACKING FOR CHOICE SELECTION IN THE FORM.
   	  $query = "SELECT SiteName, Task, Status, Progress, DateDue FROM SiteStatus WHERE AssignedTo = '".$_SESSION['user']['username']."' ORDER BY SiteName ASC";
      $status = \Portal\DB::fetch_array($query);
   
      if(count($status) == 0) {
         echo "<p id='message'>All tasks assigned to you are complete!</p>";
      } else {
         echo "<table class='table table-bordered'><thead><th>Site Name</th><th>Task</th><th>Status</th><th>Progress</th><th>Due Date</th></thead><tbody>";
         foreach ($status as $row) {
            // Make sure the task isn't complete or N/A
            if (($row['Status'] != 'Complete') && ($row['Status'] != 'N/A')) {
               echo "<tr><td><a href='#' onclick='open_output_popup(\"".$row['SiteName']."\")'>".$row['SiteName']."</a></td><td>".$row['Task']."</td><td>".$row['Status']."</td><td>".$row['Progress']."%</td><td>".$row['DateDue']."</td></tr>";
            }
         }
         echo "</tbody></table>";
      }
   }
   
   public static function ajax_site_photos_mkzip_controller() {
     	// Grab the SLIM app.
     	global $app;
     
     	// create zip of all site photos that are not a thumbnail
     	$photos = rglob(VZP_FILES . '/node/'.$_SESSION['siteName'].'/photos/full/*', 0, true);
     	$temp_file = tempnam(VZP_TMP, $_SESSION['siteName']);
     	$zip = create_zip($photos, $temp_file, true);
     
     	// return the location of the zip file.
     	if ($zip) {
     		  echo basename($temp_file);
     	}
   } // ajax_site_photos_mkzip_controller


   public static function ajax_site_photos_delete_controller() {
   
       extract($_POST, EXTR_PREFIX_ALL, "post");
   
       $audit_log = VZP_LOG . '/' . $post_site . '_photo_audit.log';
   
       $image_full = VZP_FILES . "/node/" . $post_site . "/photos/full/" . $post_image;
       $image_thumb = VZP_FILES . "/node/" . $post_site . "/photos/thumb/" . $post_image;
   
       if (unlink($image_full)) {   
           // remove the thumbnail if it exists
           @unlink($image_thumb);
   
           // log the action
           $log_entry = date("Y-m-d G:i e") . " | " . $_SERVER['REMOTE_ADDR'] . " | " . $_SESSION['user']['username'] . " deleted photo '" . $post_image . "'\n";
           file_put_contents($audit_log, $log_entry, FILE_APPEND | LOCK_EX);
   
           echo "success";
       } else {
           echo "error";
       }
   }

}
