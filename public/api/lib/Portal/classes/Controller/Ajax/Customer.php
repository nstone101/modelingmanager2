<?php

Namespace Portal\Controller\Ajax;

require_once VZP_LIB . '/DataTables/DataTables.php';

use \DataTables\Editor,
    \DataTables\Editor\Field,
    \DataTables\Editor\Format,
    \DataTables\Editor\Join,
    \DataTables\Editor\Validate;

class Customer {

   public static function ajax_customeraddressing_controller() {
   
       $db = $GLOBALS['db'];
   
       $editor = \DataTables\Editor::inst( $db, 'CustomerAddressing',"id" ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'SPIP' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'HostOffset' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'IPAddress' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::ip' ),
               \DataTables\Editor\Field::inst( 'Device' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Description' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       if ($_SESSION['VSN'] == 'all') {
           $out = $editor
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       } else {
           $out = $editor
               ->where("VSN",$_SESSION['VSN'])
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       }
   }

   public static function ajax_custckt_controller() {
   
       $db = $GLOBALS['db'];;
   
       $editor = \DataTables\Editor::inst( $db, 'CustomerCircuits' ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'SiteName' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'VSN' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'CustomerCktType' )->validator( '\DataTables\Editor\Validate::required' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Speed' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'CDR' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'CircuitID' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'MX960Router' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' ),
               \DataTables\Editor\Field::inst( 'RemoteRouter' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       if ($_SESSION['VSN'] == 'all') {
           $out = $editor
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       } else {
           $out = $editor
               ->where("VSN",$_SESSION['VSN'])
               ->where("SiteName",$_SESSION['siteName'])
               ->process( $_POST )
               ->json();
       }
   }

}