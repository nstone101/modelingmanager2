<?php

Namespace Portal\Controller\Ajax;

require_once VZP_LIB . '/DataTables/DataTables.php';

use \DataTables\Editor,
    \DataTables\Editor\Field,
    \DataTables\Editor\Format,
    \DataTables\Editor\Join,
    \DataTables\Editor\Validate;

class Tags {

   public static function ajax_mastertags_controller() {
   
       $db = $GLOBALS['db'];
       
       $editor = \DataTables\Editor::inst( $db, 'MasterTags' ) // these are the db connection, the table name, and the primary key for that table.
           ->fields(
               \DataTables\Editor\Field::inst( 'id' )->set( false ),
               \DataTables\Editor\Field::inst( 'Tag' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'UpdatedTime' )->set( false ),
               \DataTables\Editor\Field::inst( 'EditedBy' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } ),
               \DataTables\Editor\Field::inst( 'Comment' )->getFormatter( function ( $val, $data, $opts ) { return html_encode($val); } )->validator( '\DataTables\Editor\Validate::required' )
           );
   
       $out = $editor
           ->process( $_POST )
           ->json();
   } // ajax_mastertags_controller
   
   
   // 
   // MQ : Doesn't seem to be used, found no refs in source code
   //
   public static function ajax_master_tags_suggest_controller( $query ) {
      // This is the controller that returns a JSON array of suggestions from the master tags table.
     	// explode the query on spaces to search for them individually
     	$query = explode(" ", $query);
     	$tags = "";
     	// Loops over each word in the query
     	foreach ($query as $key => $value) {
     		// When running after the first word, pull out instances 
     		// that don't have the additional word in the tag.
     		if ($key > 0) {
     			foreach ($tags as $k => $v) {
     				if (strpos($v["Tag"], $value) == false) {
     					unset($tags[$k]);
     				}
     			}
     			$tags = array_values($tags);
     		} else { // on first run (first word of query), find all like tags in the master tags table
     			$tags = \ORM::for_table('MasterTags')
     			->select('Tag')
     			->where_like('Tag', '%'.$value.'%')
     			->find_array();
     		}
     	}
     	print json_encode($tags);
   } // ajax_master_tags_suggest_controller

}