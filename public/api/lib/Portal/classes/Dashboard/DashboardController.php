<?php

namespace Portal\Dashboard;

use Portal\Views;

class DashboardController{
	function __construct(){

    }

    public function index()
    {
        // setup a view
        $view = new Views\View();

        $_user = new \stdClass();

        foreach($_SESSION['user'] as $key => $val){
            $_user->{$key} = $val;
            if($val == null){
                $_user->{$key} = '';
            }
        }

        $view->set('user', json_encode($_user));

        // render
        $view->render('dashboard/index');
    }
}