<?php

namespace Portal\Dashboard\Widgets;

class WidgetModel extends RedBean_SimpleModel {
	public $sizeX;
	public $sizeY;
	public $row;
	public $col;
	public $title;
	public $name;
	public $description;
	public $front;
	public $back;
}