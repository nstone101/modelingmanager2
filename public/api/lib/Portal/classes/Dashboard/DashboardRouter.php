<?php

namespace Portal\Dashboard;

use \Portal\Services;

class DashboardRouter {
	public static function routes($app){
		$app->map('/dashboard', function () {
            $controller = new DashboardController();
            $controller->index();
        })->via('GET');

		$app->group('/api', function () use ($app) {
			$app->group('/dashboard', function() use ($app){
				$app->get('getAvailableWidgets()', function() {
					
				});

				$app->get('getUserWidgets(/:user)', function($user = null) {

				});
				
				$app->post('addWidget', function() use ($app){

				});
				
				$app->post('deleteWidget', function() use($app){

				});
			});
		});
	}
}