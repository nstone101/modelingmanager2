<?php
Namespace Portal;

class DB {
    private $_connection;
    private static $_instance;

    public static function getInstance() {
        if(!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct() {
        $this->_connection = new \mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

        if(mysqli_connect_error()) {
            trigger_error("Failed to conencton to MySQL: " . mysqli_connect_error(), E_USER_ERROR);
        }
    }

    // clone is empty to prevent dupe of connection
    private function __clone() { }

    public function getConnection() {
        return $this->_connection;
    }

    public function __call($name, $arguments) {
        $method = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $name));

        return call_user_func_array(array($this, $method), $arguments);
    }

    public static function __callStatic($name, $arguments) {
        $method = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $name));

        return call_user_func_array(array('DB', $method), $arguments);
    }

    public static function escape($str) {
        return self::getInstance()->getConnection()->real_escape_string($str);
    }

    public static function run($sql) {
        return self::getInstance()->getConnection()->query($sql);
    }

    public static function fetch_array($sql) {
        $return = array();
        if ($result = self::run($sql)) {
            while($row = $result->fetch_array()) {
                $return[] = $row;
            }
        }
        return $return;
    }

}

