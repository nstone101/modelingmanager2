<?php

namespace Portal\Users;

use Model, \Portal\Models\Users;

class UserService {

	/**
	 * provide a queryable interface for users.
	 * @return UserModel IdiOrm result set
	 */
	public function users(){
		return  Model::factory('\Portal\Models\Users\User');
	}

	public function roles(){
		return Model::factory('\Portal\Models\Users\Role');
	}

	public function groups(){
		return Model::factory('\Portal\Models\Users\UserGroup');
	}

    public function getUserList(){
        $retVal = array();
        $users = $this->users()->findMany();
        foreach($users as $user) {
            $retVal[] = $this->getUserDetails($user);
        }
        return $retVal;
    }

    public function getUserByEmail($email) {
        $user = $this->users()->where('email', $email)->findOne();
        return $this->getUserDetails($user);
    }

    public function getUserByUsername($username) {
        $user = $this->users()->where('username', $username)->findOne();
        return $this->getUserDetails($user);
    }

    public function getUserById($id) {
        $user = $this->users()->findOne($id);
        return $this->getUserDetails($user);
    }

    private function getUserDetails($user){
        if($user == null) return;
        $roles = $user->roles()->findMany();
        $_roles = array();
        $role = 0;
        foreach($roles as $role){
            $_roles[] =  $role->asArray()['role_name'];
            $role = $role->asArray()['role_id'];
        };

        $item = $user->asArray();
        unset($item['password']);
        unset($item['salt']);
        unset($item['hash']);
        //$item['sessid'] = session_id();
        $item['roles'] = $_roles;
        $item['role'] = $role;

        return $item;
    }

	public function create($initialValues){
		global $template;
		try{
			$retVal = new \stdClass();
			$user = $this->users()->create();
            /*
             * add on initial password, salt, hash
             */
            $salt = gen_salt();
            // The temp password is kept non-hashed so it can be sent to the user in the verifcation email.
            $temp_password = rand(10000000,99999999);

            $password = hash('sha256', $temp_password . $salt);
            for($round = 0; $round < 65536; $round++)
            {
                $password = hash('sha256', $password . $salt);
            }

            try {
                $user->password = $password;
                $user->salt = $salt;
                $user->hash = md5(rand(0, 1000));

                $role = $initialValues['user']['role'];

                unset($initialValues['user']['role']);

                if ($this->saveOrmObject($user, $initialValues['user']) === true) {
                    $retVal->result = $this->users()->findOne($user->id());
                    if($role != null) {
                        $userRole = Model::factory('\Portal\Models\Users\UserRole')->create();
                        $userRole->role_id = $role;
                        $userRole->user_id =  $user->id();
                        $userRole->save();
                    }
                } else {
                    $retVal->result = array();
                }
            }
            catch(\Exception $e){
                var_dump($e);
                exit('exiting..  user create');
            }

            $mail = new \PHPMailer;
            $mail->IsSMTP();
            $mail->Host = SMTP_HOST;
            $mail->SMTPAuth = SMTP_AUTH;
            $mail->Username = SMTP_USERNAME;
            $mail->Password = SMTP_PASSWORD;
            $mail->SMTPSecure = SMTP_ENCRYPTION;

            $mail->From = SMTP_FROM;
            $mail->FromName = APP_NAME;
            $mail->AddAddress($initialValues['user']['email']);
            $mail->IsHTML(true);
            $mail->Subject = APP_NAME . ' Account Verification';

            $activate_url = 'http://' . $_SERVER['SERVER_NAME'] . '/auth/verify/' . $initialValues['user']['email'] . '/' . $user->hash;
            $mail->Body = $template->render('email/new-user.html.twig', array('username' => $initialValues['user']['username'], 'temp_password' => $temp_password, 'activate_url' => $activate_url));
            if (!$mail->Send()) {
              //$message2 = 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo;
//			    echo json_encode({'code': 500, 'message': 'unble to email the wellcome email to the new user. ' . $e->getMessage()});
            } // if the mail does send, send them back to manage.php
            else {
              //$message2 = 'A verification email and temporary password has been sent to: ' . $initialValues['user']['email'];
            }

			if(count($retVal->result) < 1) {
				return $retVal->result;
			}
			else {
				return $this->getUserDetails($retVal->result);
			}

		}
		catch(\Exception $e){
//			echo json_encode({'code': 500, 'message': 'create the username. ' . $e->getMessage()});
		}
	}

	public function update($id, $updatedValues) {
		$user = $this->users()->findOne($id);
		try {
			if($updatedValues['user']['role'] != null) {
				$userRole = Model::factory('\Portal\Models\Users\UserRole')->where('user_id', $id)->findOne();

				if($userRole == false){
					$userRole = Model::factory('\Portal\Models\Users\UserRole')->create();
					$userRole->role_id = $updatedValues['user']['role'];
					$userRole->user_id =  $id;
					$userRole->save();
					return;
				}
				$userRole->role_id = $updatedValues['user']['role'];
				$userRole->save();
			}

            unset($updatedValues['user']['role']);
            unset($updatedValues['user']['roles']);

            if ($this->saveOrmObject($user, $updatedValues['user']) === true) {
                return $this->getUserDetails($user);
            } else {
               return array();
            }
		}
		catch(\Exception $e){
//			echo json_encode({'code': 500, 'message': 'update the username. ' . $e->getMessage()});
		}
		return;
	}

	public function update2($userValues){

	try {

          $sqlStr = "update users  " .
                    "set firstname = '" .  $userValues["firstname"] . "', " .
                    "    lastname = '" . $userValues["lastname"] . "', " .
                    "    Address1 = '" . $userValues["addr1"] . "', " .
                    "    Address2 = '" . $userValues["addr2"] . "', " .
                    "    workphone = '" . $userValues["workphone"] . "', " .
                    "    cellphone = '" . $userValues["cellphone"] . "', " .
                    "    email = '" . $userValues["email"] . "', " .
                    "    City = '" . $userValues["city"] . "', " .
                    "    State = '" . $userValues["state"] . "', " .
                    "    Zip = '" . $userValues["zip"] . "', " .
                    "    FullName = '" . $userValues["firstname"] . " " . $userValues["lastname"] . "' "  .
                    " where id = " . $userValues["id"] . "; "

          ;

          $testit =  \ORM::for_table( 'DUAL' )
                     ->raw_execute( $sqlStr );


                /* 
                  
                 no error, the findOne below simply returns empty, 
                 will research later, for now
                 update the user with the embedded sql above
        

		$user2 = $this->users()->findOne($userValues["id"]); // try to find one
                $user3 = \ORM::for_table('users')->find_one($userValues["id"]);  // try to find one

                if ($this->saveOrmObject($user, $userValues) === true) {
                    return $this->getUserDetails($user);
                } else {
                   return array();
                }
                 */
	} catch(\Exception $e){
//             echo json_encode({'code': 500, 'message': 'user profile update' . $e->getMessage()});
	}

        }

	public function updateUserPassword($userValues){


	try {

        if (strcmp($userValues["updatedPassword"], $userValues["confirmPassword"]) != 0) {
            return 500;
        }

        $user = $this->users()->findOne($userValues["id"]);

        $user_details_password = $user->get('password');
        $user_salt = $user->get('salt');

        $check_password = hash('sha256', $userValues["currentPassword"] . $user_salt);

        for($round = 0; $round < 65536; $round++) {
          $check_password = hash('sha256', $check_password . $user_salt);
        }

        if($check_password === $user_details_password) {
              $passwordCheckOK = true;
        } else {
              return 500;
        }

         $new_salt = gen_salt();
         $new_hash = md5(rand(0, 1000));

         $new_password = hash('sha256', $userValues["confirmPassword"] . $new_salt);
         for($round = 0; $round < 65536; $round++)
         {
            $new_password = hash('sha256', $new_password . $new_salt);
         }


         $sqlStr = "update users  " .
                   "set salt = '" . $new_salt . "', " .
                   "hash = '" . $new_hash . "', " .
                   "password = '" . $new_password . "' " .
                   " where id = " . $userValues["id"] . "; "
         ;
         $testit =  \ORM::for_table( 'DUAL' )
                     ->raw_execute( $sqlStr );


	} catch(\Exception $e){
//             echo json_encode({'code': 500, 'message': 'user profile update' . $e->getMessage()});
	}

        }

	public function createGroup($initialValues) {
		try {
			unset($initialValues['group_id']);
			var_dump($initialValues);
			$group = $this->groups()->create();

			$this->saveOrmObject($group, $initialValues);
		}
		catch(\Exception $e){
//			echo json_encode({'code': 500, 'message': 'create the group. ' . $e->getMessage()});
		}
	}

	public function updateGroup($updatedValues) {
		try {
			$group = \ORM::for_table('user_groups')->find_one($updatedValues['group_id']);
			//$group = $this->groups()->where('group_id', $updatedValues['group_id']);
			unset($updatedValues['group_id']);
			var_dump($group);
			$this->saveOrmObject($group, $updatedValues);
		}
		catch(\Exception $e){
//			echo json_encode({'code': 500, 'message': 'update the group. ' . $e->getMessage()});
		}
	}

    public function deleteGroup($ids){
    	try{
    		$groups = $this->groups()->whereIn('group_id', $ids)->findMany();
    		foreach($groups as $group){
    			$group->delete();
    		}
		}
		catch(\Exception $e){
//			echo json_encode({'code': 500, 'message': 'unable delete the groups. ' . $e->getMessage()});
		}
	}

    public function batchDeleteGroups($ids){
        try{
            $groups = $this->groups()->whereIn('group_id', $ids)->findMany();
            foreach($groups as $group){
                $group->delete();
            }
        }
        catch(\Exception $e){
            throw $e;
        }
    }

    public function deleteGroupId($id) {
        $query = $this->groups()->findOne($id);
        if ($query !== false) {
            $query->delete();
        }
    }

	public function batchDelete($ids){
    	try{
    		$users = $this->users()->whereIn('id', $ids)->findMany();
    		foreach($users as $user){
    			$user->delete();
    		}
		}
		catch(\Exception $e){
            throw $e;
		}
	}

    public function delete($id) {
        $query = $this->users()->findOne($id);
        if ($query !== false) {
            $query->delete();
        }
    }

    /**
     * @param $object
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    protected function saveOrmObject($object, $params){
        try {
            $params = (array)$params;
            $object->set($params);
            return $object->save();
        }
        catch(\Exception $e){
            var_dump($e);
            throw $e;
        }
    }
}
