<?php

namespace Portal\Users;

use \Portal\Services;

class UserRouter {
	public static function routes($app){

		// setup an api for the user management code to use

			// User REST Interface
			$app->group('/user', function() use($app){
				$app->get('/', function() use($app) {
					$byEmail = $app->request()->params('email');
					$byUsername =  $app->request()->params('username');
					try {
						$retVal = array();
						$userService = new UserService();

						if($byEmail == null && $byUsername == null){
							echo json_encode($userService->getUserList());
						}
						else if($byEmail != null) {
							echo json_encode($userService->getUserByEmail($byEmail), true);
						}
						else if($byUsername != null) {
							echo json_encode($userService->getUserByUsername($byUsername), true);
						}
						else {
							echo json_encode($userService->getUserById($id), true);
						}
					}
					catch(\Exception $e) {
						echo json_encode("{'code': 500, 'message': 'unable to get the user(s). '" . $e->getMessage() . "}");
					}
				});

				$app->get('/:id', function($id) {
					try {
						$userService = new UserService();

						echo json_encode($userService->getUserById($id), true);
					}
					catch(\Exception $e) {
						echo json_encode("{'code': 500, 'message': 'unable to get the user(s). '" . $e->getMessage() . "}");
					}
				});

				$app->post('/', function() use ($app) {
					try {
						$json = json_decode($app->request->getBody(), true);
						$userService = new UserService();
						if(array_key_exists("user", $json)){
							echo json_encode($userService->create((array)$json));
						}
						else {
							echo json_encode("{'code': 500, 'message': 'unable to create the user. '}");
						}
					}
					catch(\Exception $e) {
						echo json_encode("{'code': 500, 'message': 'unable to create the user. '" . $e->getMessage() . "}");
					}
				});

				$app->post('/updateUserProfile', function() use ($app) {
					try {
                                                $json = json_decode($app->request->getBody(), true);

                                                $userService = new UserService();

                                                // if we get here, should hava a params.user
                                                // if not, let it catch an error
                                                echo json_encode($userService->update2($json["params"]["user"]));
					}
					catch(\Exception $e) {
						echo json_encode("{'code': 500, 'message': 'unable to create the user. '" . $e->getMessage() . "}");
					}
				});

				$app->post('/updateUserPassword', function() use ($app) {
					try {
                                                $json = json_decode($app->request->getBody(), true);

                                                $userService = new UserService();


                                                // if we get here, should hava a params.user
                                                // if not, let it catch an error
                                                $result = $userService->updateUserPassword($json["params"]["user"]);
                                                if ($result === 500) {
						   echo json_encode('{status: 500, code: 500, message: "unable to update password. " }');
                                                } else {
                                                    echo $result;
                                                }
                                                

//                                                echo json_encode($userService->updateUserPassword($json["params"]["user"]));
					}
					catch(\Exception $e) {
//						echo json_encode("{'code': 500, 'message': 'unable to create the user. '" . $e->getMessage() . "}");
					}
				});


                $app->post('/user-activate', function() use($app) {
                    try {
                        $json = json_decode($app->request->getBody());

                        $userService = new UserService();
                        $user = $userService->users()->findOne($json->id);
                        $user->activated = true;
                        $user->save();
                    }
                    catch(\Exception $e){
                        echo json_encode("{'code': 500, 'message': 'unable to activate the user. '" . $e->getMessage() . "}");
                    }
                });

				$app->post('/batch-delete', function() use($app){
					$json = json_decode($app->request->getBody());
					$userService = new UserService();
					$userService->batchDelete($json);
				});

				$app->put('/:id', function($id) use($app){
					try {
						$json = json_decode($app->request->getBody(), true);
						$userService = new UserService();
						if(array_key_exists("user", $json)){
							echo json_encode($userService->update($id, $json));
						}
						else {
							echo json_encode("{'code': 500, 'message': 'unable to update the user. '}");
						}
					}
					catch(\Exception $e) {
						echo json_encode("{'code': 500, 'message': 'unable to create the user. '" . $e->getMessage() . "}");
					}
				});

				$app->delete('/:id', function($id) use($app){
					try {
						$userService = new UserService();
						$userService->delete($id);
					}
					catch(\Exception $e){
						echo json_encode("{'code': 500, 'message': 'unable to delete the user. '" . $e->getMessage() . "}");
					}
				});

			});
            // end User REST

            // Group REST Interface
            $app->group('/group', function() use ($app){

                $app->get('/', function(){
                    try {
                        $userService = new UserService();
                        echo json_encode($userService->groups()->findArray());
                    }
                    catch(\Exception $e){
                        echo json_encode("{'code': 500, 'message': 'unable to list the groups.'" . $e->getMessage() . "}");
                    }
                });

                $app->get('/:id', function($id){
                    try {
                        $userService = new UserService();
                        echo json_encode($userService->groups()->where('group_id', $id)->findArray());
                    }
                    catch(\Exception $e){
                        echo json_encode("{'code': 500, 'message': 'unable to get get group id.'" . $e->getMessage() . "}");
                    }
                });

                $app->post('/', function() use($app){
                    try {
                        $json = json_decode($app->request->getBody(), true);
                        $userService = new UserService();
                        $userService->createGroup($json);
                    }
                    catch(\Exception $e){
                        echo json_encode("{'code': 500, 'message': 'unable to create group id.'" . $e->getMessage() . "}");
                    }
                });

                $app->post('/batch-delete', function() use($app){
                    try {
                        $json = json_decode($app->request->getBody(), true);
                        $userService = new UserService();
                        $userService->batchDeleteGroups($json['ids']);
                    }
                    catch(\Exception $e){
                        echo json_encode("{'code': 500, 'message': 'unable to delete group ids.'" . $e->getMessage() . "}");
                    }
                });

                $app->put('/:id', function($id) use($app){
                    try {
                        $json = json_decode($app->request->getBody(), true);
                        $userService = new UserService();
                        $userService->updateGroup($json);
                    }
                    catch(\Exception $e){
                        echo json_encode("{'code': 500, 'message': 'unable to update group id.'" . $e->getMessage() . "}");
                    }
                });

                $app->delete('/:id', function($id) use($app){
                    try {
                        $userService = new UserService();
                        $userService->deleteGroupId($id);
                    }
                    catch(\Exception $e){
                        echo json_encode("{'code': 500, 'message': 'unable to delete the group. '" . $e->getMessage() . "}");
                    }
                });

            });
            // end Group REST

			// Role REST Interface
			$app->group('/role', function() use ($app){

				$app->get('/', function(){
					try {
						$userService = new UserService();
						echo json_encode($userService->roles()->findArray());
					}
					catch(\Exception $e){
						echo json_encode("{'code': 500, 'message': 'unable to list the roles.'" . $e->getMessage() . "}");
					}
				});

				$app->get('/:id', function($id){
					try {
						$userService = new UserService();
						echo json_encode($userService->groups()->where('role_id', $id)->findArray());
					}
					catch(\Exception $e){
						echo json_encode("{'code': 500, 'message': 'unable to get get role id.'" . $e->getMessage() . "}");
					}
				});

			});
			// end Role REST

			$app->group('/validators', function() use ($app){
					$app->post('/username', function() use ($app) {
						try {
		                	$body = json_decode($app->request->getBody());
			            	
			            	$validator = new \Portal\Services\ValidationService();
				            $validator->validateUsername($body->username);
						}
						catch(\Exception $e){
//							echo json_encode({'code': 500, 'message': 'unable to validate the username. ' . $e->getMessage()});
						}
					});

				$app->post('/email', function() use ($app) {
					try {
	                	$body = json_decode($app->request->getBody());
		            	
		            	$validator = new \Portal\Services\ValidationService();
			            $validator->validateEmail($body->email);
					}
					catch(\Exception $e){
//						echo json_encode({'code': 500, 'message': 'unable to validate the email. ' . $e->getMessage()});
					}
				});
			});
	}
}
