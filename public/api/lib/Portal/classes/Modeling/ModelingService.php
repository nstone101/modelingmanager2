<?php

namespace Portal\Modeling;

class ModelingService
{
    public function getMICTable($params) {
        $params = (object)$params;
        $retVal = new \stdClass();
        $conditions = array();

        if(isset($params->siteName) && $params->siteName != null) {
            $conditions['SiteName'] = strtoupper($params->siteName);
        }

        $query = \ORM::for_table($params->tableName);

        return $query->findArray();
    }
    public function getMICPATable($params) {
        $params = (object)$params;
        $retVal = new \stdClass();
        $conditions = array();

        if(isset($params->siteName) && $params->siteName != null) {
            $conditions['SiteName'] = strtoupper($params->siteName);
        }

        $query = \ORM::for_table($params->tableName);

        return $query->findArray();
    }
    public function getMPCTable($params) {
        $params = (object)$params;
        $retVal = new \stdClass();
        $conditions = array();

        if(isset($params->siteName) && $params->siteName != null) {
            $conditions['SiteName'] = strtoupper($params->siteName);
        }

        $query = \ORM::for_table($params->tableName);

        return $query->findArray();
    }


}
