<?php

namespace Portal\Modeling;

use \Portal\Services;

class ModelingRouter {
    public static function routes($app){

            $app->group('/modeling', function() use($app){


                $app->group('/mic', function() use($app){
                    $app->get('/', function() use($app) {
                        $siteName = $app->request()->params('siteName');

                        $node = new ModelingService();
                        echo json_encode($node->getMICTable(array('tableName'=>'Model_MIC', 'siteName' => $siteName )));
                    });

                    $app->get('/:id', function($id) {
                        $node = new ModelingService();
                        echo json_encode($node->getMICTable(array('tableName'=>'Model_MIC', 'id' => $id)));
                    });


                });
                $app->group('/mic-portassignments', function() use($app){
                    $app->get('/', function() use($app) {
                        $siteName = $app->request()->params('siteName');

                        $node = new ModelingService();
                        echo json_encode($node->getMICPATable(array('tableName'=>'Model_MIC_PortAssignments', 'siteName' => $siteName )));
                    });

                    $app->get('/:id', function($id) {
                        $node = new ModelingService();
                        echo json_encode($node->getMICPATable(array('tableName'=>'Model_MIC_PortAssignments', 'id' => $id)));
                    });


                });
                $app->group('/mpc', function() use($app){
                    $app->get('/', function() use($app) {
                        $siteName = $app->request()->params('siteName');

                        $node = new ModelingService();
                        echo json_encode($node->getMPCTable(array('tableName'=>'Model_MPC', 'siteName' => $siteName )));
                    });

                    $app->get('/:id', function($id) {
                        $node = new ModelingService();
                        echo json_encode($node->getMPCTable(array('tableName'=>'Model_MPC', 'id' => $id)));
                    });


                });

            });
    }
}
