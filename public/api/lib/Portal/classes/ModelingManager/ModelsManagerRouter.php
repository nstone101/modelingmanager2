<?php

namespace Portal\ModelsManager;

use \Portal\Services;

class ModelsManagerRouter {
    public static function routes($app){

        // model-data/cards

        //$app->group('/api', function() use($app){
            $app->group('/model-data', function() use($app){

                // cstomer circuits
                $app->group('/cards', function() use($app){
                    $app->get('/', function() use($app) {

                        $node = new ModelsManagerService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'Model_ICard')));
                    });

                });



            });

                        
            $app->group('/modelers', function () use ($app) {

 


                $app->put('/manager', function ($site) use ($app) {
                    $json = $app->request->getBody();
                    // get portal controller
                    if($json)
                        $json = json_decode($json);
                    try {
                            $service = new ModelsManagerService($site, 'all');

                            $retVal =  "0"; // $service->updateNode($site, $json);
                            echo json_encode($retVal);
                        }
                        catch(\Exception $e) {
                            echo json_encode("{'code': 500, 'message': 'unable to update the site info. '" . $e->getMessage() . "}");
                        }
                });
            });




    }
}
