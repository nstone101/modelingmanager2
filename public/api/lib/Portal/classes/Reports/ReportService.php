<?php

namespace Portal\Reports;

class ReportService {
    public function getUtilityServerIps(){
        $results = \ORM::for_table('MX960PortAssignments')->raw_query(
            "select
              SiteName, ConnectedDeviceName, AddressTag, IPAddress
            from
              MX960PortAssignments
            where
              ConnectedDeviceName REGEXP '(^|[^[:digit:]])[[:digit:]]{2}$' and RIGHT (ConnectedDeviceName, 4) like 'cm%'
            and (IPAddress != '#N/A' and IPAddress != '' and IPAddress != 'N/A')")->findArray();
        return $results;
    }

    public function getMx960PortsRequiringScan() {
        $results = \ORM::for_table('MX960PortAssignments')->raw_query(
            "select
              SiteName, Interface, ConnectedDeviceType, ConnectedDeviceName, ConnectedDevicePort from MX960PortAssignments
            where
              VLANTag = 'phy'
            and Layer like '%L2%'
            group by SiteName, Interface, ConnectedDeviceType, ConnectedDeviceName, ConnectedDevicePort, IPAddress")->findArray();
        return $results;
    }

    public function getLatestNodeStateChanges() {
        $results = \ORM::for_table('SiteInfo_Audit')->raw_query(
            "select
              a1.SiteName, a1.SiteState, a1.EditedBy, a1.UpdatedTime
                from SiteInfo_Audit a1
                inner join
                (
                  select *
                  from SiteInfo_Audit
                  where Action = 'updatestate'
                  group by date(UpdatedTime)
                ) a2
                  on a1.SiteName = a2.SiteName")->findArray();
        return $results;
    }

    public function getNodes(){
        return ORM::for_table('SiteInfo')->order_by_asc('SiteName')->find_array();
    }

    public function getNodeTable($params) {
        $params = (object)$params;
        $retVal = new \stdClass();
        $conditions = array();

        if(isset($params->siteName) && $params->siteName != null) {
            $conditions['SiteName'] = strtoupper($params->siteName);
        }

        if (isset($params->vsn) && ($params->vsn != 'all' && $params->vsn != null)) {
            $conditions['VSN'] = $params->vsn;
        }

        $query = \ORM::for_table($params->tableName);

        if(isset($params->id) && $params->id != null) {

            return $query->findOne($params->id)->asArray();
        }

        if(count($conditions) > 0) {
            $result = $query->where($conditions)->findArray();
        }
        else {
            $result = $query->findArray();
        }

        return $result;
    }
}