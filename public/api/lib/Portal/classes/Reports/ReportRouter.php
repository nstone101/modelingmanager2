<?php

namespace Portal\Reports;

use \Portal\Services;

class ReportRouter
{
    public static function routes($app)
    {
        $app->group('/reports', function() use($app){

            $app->group('/node-data', function() use($app){
                // cstomer addressing
                $app->group('/customer-addressing', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'CustomerAddressing_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });
                // cstomer circuits
                $app->group('/customer-circuits', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'CustomerCircuits_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // node device name
                $app->group('/node-device-names', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'NodeDeviceNames_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // ip schema
                $app->group('/ip-schema', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'IPSchema_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // MX960 Port Assignments
                $app->group('/mx960-port-assignments', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'MX960PortAssignments_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // ns5400 port assignments
                $app->group('/ns5400-port-assignments', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'NS5400PortAssignments_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // Network Connections
                $app->group('/network-connections', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'NetworkConnections_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // CardSlotting
                $app->group('/card-slotting', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'CardSlotting_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // IP_Private
                $app->group('/ip-private', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'IP_Private_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // IP IDN
                $app->group('/ip-idn', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'IP_IDN_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // IP Public
                $app->group('/ip-public', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'IP_Public_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // DNS Trap Log
                $app->group('/dns-trap-log', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'DNSTrapLog_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // Subnet Adv
                $app->group('/subnet-information', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'SubnetAdv_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });

                });

                // Subnet Adv
                $app->group('/subnet-adv', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'SubnetAdv_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // Async
                $app->group('/async', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'Async_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // PingPoller
                $app->group('/ping-poller', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'PingPoller_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // TDR
                $app->group('/tdr', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'TDR_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // FWGoldenConfig
                $app->group('/fw-golden-config', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'FWGoldenConfig_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // FWSetup
                $app->group('/fw-setup', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'FWSetup_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // NodeNotes
                $app->group('/node-notes', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'NodeNotes_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

                // vpn-routing
                $app->group('/vpn-routing', function() use($app){
                    $app->get('/', function() use($app) {
                        $vsn = $app->request()->params('vsn');
                        $type = $app->request()->params('type');
                        $siteName = $app->request()->params('siteName');

                        $node = new ReportService();
                        echo json_encode($node->getNodeTable(array('tableName'=>'VPNRouting_Audit', 'siteName' => $siteName,  'vsn' => $vsn, 'type' => $type)));
                    });
                });

            });

            $app->get('/utilityServerIps', function(){
                try {
                    $service = new ReportService();
                    echo json_encode($service->getUtilityServerIps());
                }
                catch(\Exception $e){
                    echo json_encode("{'code': 500, 'message': 'unable to get report: getUtilityServerIps.'" . $e->getMessage() . "}");
                }
            });

            $app->get('/mx960PortsRequiringScanning', function(){
                try {
                    $service = new ReportService();
                    echo json_encode($service->getMx960PortsRequiringScan());
                }
                catch(\Exception $e){
                    echo json_encode("{'code': 500, 'message': 'unable to get report: mx960PortsRequiringScan.'" . $e->getMessage() . "}");
                }
            });

            $app->get('/latestNodeStateChanges', function() {
                try {
                    $service = new ReportService();
                    echo json_encode($service->getLatestNodeStateChanges());
                }
                catch(\Exception $e){
                    echo json_encode("{'code': 500, 'message': 'unable to get report: latestNodeStateChanges.'" . $e->getMessage() . "}");
                }
            });

            $app->get('/getNodeFilter', function() {
                try {
                    $service = new ReportService();
                    echo json_encode($service->getLatestNodeStateChanges());
                }
                catch(\Exception $e){
                    echo json_encode("{'code': 500, 'message': 'unable to get report: get.'" . $e->getMessage() . "}");
                }
            });

            $app->group('/filters', function() use($app){
                $app->get('/nodes', function(){
                    try {
                        $service = new ReportService();
                        echo json_encode($service->getNodes());
                    }
                    catch(\Exception $e){
                        echo json_encode("{'code': 500, 'message': 'unable to get report filters: node filters.'" . $e->getMessage() . "}");
                    }
                });
            });
        });
    }
}