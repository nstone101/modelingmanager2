<?php

Namespace Portal\SiteValidation;

class ValidateBase {

    protected $sitename;

    // MQ : when altering this, please alter the $col_func_mappings struct as well
    protected $db_tables = array ('IPSchema', 'IP_IDN', 'IP_Private', 'IP_Public');

    // MQ: Dang! Can't use  $db_tables[0] instead of 'IPSchema' nor can we use $this->db_tables[0]
    // so we have to hardcode the same values again; hey C language, where are you ?
    protected $col_func_mappings = array ('IPSchema'   => array('NetworkType' => '_validateNetworkTypeCol',
                                                                'Subnet'      => '_validateSubnetCol',
                                                                'Netmask'     => '_validateNetmaskCol'
                                                               ),
                                          'IP_IDN'     => array('Subnet'   => array('_validateSubnetCol', '_ipCheck'),
                                                                'Netmask'  => '_validateNetmaskCol',
                                                                'IPOffset' => '_validateOffsetandIPCol'
                                                               ),
                                          'IP_Private' => array('Subnet'   => array('_validateSubnetCol', '_ipCheck'),
                                                                'Netmask'  => '_validateNetmaskCol',
                                                                'IPOffset' => '_validateOffsetandIPCol'
                                                               ),
                                          'IP_Public'  => array('Subnet'   => array('_validateSubnetCol', '_ipCheck'),
                                                                'Netmask'  => '_validateNetmaskCol',
                                                                'IPOffset' => '_validateOffsetandIPCol'
                                                               )
                                         );

    function __construct($sitename)
    {
        // MQ : should I test for valid sitename, length>0, <>'', not null ???
        $this->sitename = $sitename;
    }

    public function getValidationTables() {
        return $this->db_tables;
    }

    public function getValidationCols($table) {
        if ($table){
            return array_keys($this->col_func_mappings[$table]);
        } else {
            $all = array();
            foreach($this->db_tables as $table) {
                $all[] = array_keys($this->col_func_mappings[$table]);
            }
            return $all;
        }
    }

    public function getColFuncMappings($table) {
        if ($table) {
            return $this->col_func_mappings[$table];
        } else {
            $all = array();
            foreach($this->db_tables as $table) {
                $all[] = $this->col_func_mappings[$table];
            }
            return $all;
        }
    }
}
