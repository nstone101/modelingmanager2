<?php

Namespace Portal\SiteValidation;

class DataEntryController extends ValidateBase {

    function __construct($sitename) {
        parent::__construct($sitename);
        //$this->model = new \Portal\Models\ViewModels\IpSchemaViewModel($type, $siteName);
    }

    /****************************/
    /* helper functions/methods */
    /****************************/

    private function _getNetworkTypes($tableName) {
        $table = $tableName ? $tableName : 'NetworkTypes';
        $network_types = \ORM::for_table($table)->find_array();

        //foreach ($network_types as $network_type) {
        //    echo $network_type['network_type'].'<br>';
        //}

        return $network_types;
    }

    private function _getSiteName() {
        return $this->sitename;
    }

    /******************************************/
    /* functions/methods for finding bad data */
    /******************************************/

    private function _ipCheck( $table, $cols ) {
        //required cols : Subnet, Netmask, IPAddress
        $additional_cols = array('IPAddress');
        $columns = array_merge($cols, $additional_cols);

        // let's check IP addresses (typos and what not)
        $sql = 'SELECT ' . implode(",", $columns) . ' FROM ' . $table . " WHERE SiteName='" . $this->_getSitename() . "'";
        //echo $sql.'<br>';
        $records = \ORM::for_table($table)->raw_query($sql)->find_array();
        if ($records) {
           $found_issues = 0;
           ?>
           <p>Empty fields are <span style="color:red;">NOT OK</span>.</p>
           <table width="70%">
              <thead>
              <tr>
                 <?php
                 foreach ($columns as $heading) {
                     echo '<th>' . $heading . '</th>';
                 } ?>
              </tr>
              </thead>
              <tbody>
              <?php
              // check if the given IP address is in the given subnet/netmask
              foreach ($records as $record) {
                  list ($net, $mask) = array($record['Subnet'], $record['Netmask']);

                  $ip_net = ip2long($net);
                  $ip_mask = ~((1 << (32 - $mask)) - 1);

                  $ip_ip = ip2long($record['IPAddress']);
                  $ip_ip_net = $ip_ip & $ip_mask;

                  if ($ip_ip_net != $ip_net) {
                     $found_issues = 1;
                     echo '<tr>';
                     foreach ($cols as $column) {
                        switch ($column) {
                           case 'Subnet'    : echo '<td style="color:red;">'; break;
                           case 'Netmask'   : echo '<td style="color:red;">'; break;
                           case 'IPAddress' : echo '<td style="color:red;">'; break;
                           default          : echo '<td>'; break;
                        }
                        echo $record[$column] . '</td>';
                     }
                     echo '</tr>';
                  }
            }
            if (!$found_issues) {
               echo '<p>IPAddress vs Subnet/CIDR validates <span style="color:green;">OK</span></p>';
            }
            ?>
            </tbody>
            </table>
            <br>
            <?php
        }
    }

    private function _validateSubnetCol($table, $cols) {

        $sql = 'SELECT '. implode(",", $cols) . ' FROM ' . $table .
               " WHERE SiteName='" . $this->_getSitename(). "' AND Subnet LIKE '%-%' OR Subnet LIKE '%/%'";
        //echo $sql.'<br>';
        $flagged = \ORM::for_table($table)->raw_query($sql)->find_array();
        
        if ($flagged) {
            ?>
            <p>Empty fields are <span style="color:red;">NOT OK</span>.</p>
            <table width="70%">
            <thead><tr>
                <?php
                foreach ($cols as $heading) {
                    echo '<th>'.$heading.'</th>';
                }
                ?>
            </tr></thead>
            <tbody>
            <?php
            foreach ($flagged as $val) {
                echo '<tr>';
                foreach ($cols as $column) {
                    echo ($column == 'Subnet') ? '<td style="color:red;">' : '<td>';
                    echo $val[$column] . '</td>';
                }
                echo '</tr>';
            }
            ?>
            </tbody>
            </table>
            <br>
            <?php
        } else {
            echo '<p>Subnet data validates <span style="color:green;">OK</span></p>';
        }
    }

    private function _validateNetmaskCol($table, $cols) {

       $allowed_netmasks = explode(",", $_SESSION['rules']['AllowedNetmasks']);
       $quoted_netmasks = array_map("addQuotes", $allowed_netmasks);   //functions::addQuotes

       $sql = 'SELECT ' . implode(",", $cols) . ' FROM ' . $table .
              " WHERE SiteName='" . $this->_getSitename() .
              "' AND (Netmask NOT REGEXP '^/[0-9][0-9]$' OR Netmask NOT IN (".implode(",", $quoted_netmasks)."))";
       //echo $sql.'<br>';
       $flagged = \ORM::for_table($table)->raw_query($sql)->find_array();

       if ($flagged) {
           ?>
           <p>Empty fields are <span style="color:red;">NOT OK</span>.</p>
           <p>Valid netmasks : <?php echo $_SESSION['rules']['AllowedNetmasks']; ?></p>
            <table width="70%">
            <thead><tr>
                <?php
                foreach ($cols as $heading) {
                    echo '<th>'.$heading.'</th>';
                }
                ?>
            </tr></thead>
            <tbody>
            <?php
            foreach ($flagged as $has_issue) {
                ?>
                <tr>
                    <td><?=$has_issue['id'];?></td>
                    <td><?=$has_issue['VSN'];?></td>
                    <td style="color:red;"><?=strlen($has_issue['Netmask']) ? $has_issue['Netmask'] : '';?></td>
                </tr>
                <?php
                }
                ?>
            </tbody>
            </table>
            <br>
            <?php
       } else {
           echo '<p>Netmask data validates <span style="color:green;">OK</span></p>';
       }

    }

    private function _validateNetworkTypeCol($table, $cols) {

        // list up the allowed network types
        $networktypes = $this->_getNetworkTypes();

        // we only want the network_type column
        $netwks = array();
        foreach ($networktypes as $networktype) {
            $netwks[] = "'".$networktype['network_type']."'";
        }

        $sql = 'SELECT '. implode(",", $cols) . ' FROM ' . $table . " WHERE SiteName='" . $this->_getSitename() . "' AND NetworkType NOT IN (" . implode(",", $netwks) . ')';
        //echo $sql.'<br>';
        $flagged = \ORM::for_table($table)->raw_query($sql)->find_array();

        if ($flagged) {
            echo '<br>Valid <strong>NetworkType</strong> choices :<br>';
            foreach($netwks as $netwk) {
                echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$netwk.'<br>';
            }
            ?>
            <br>
            <p>Empty fields are <span style="color:red;">NOT OK</span>.</p>
            <table width="70%">
                <thead><tr>
                    <?php
                    foreach ($cols as $heading) {
                        echo '<th>'.$heading.'</th>';
                    }
                    ?>
                </tr></thead>
                <tbody>
                <?php
                foreach ($flagged as $has_issue) {
                    ?>
                    <tr>
                       <td><?=$has_issue['id'];?></td>
                       <td><?=$has_issue['VSN'];?></td>
                       <td style="color:red;"><?=strlen($has_issue['NetworkType'])?$has_issue['NetworkType']:'';?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <br>
            <?php
        } else {
            echo '<p>NetworkType data validates <span style="color:green;">OK</span></p>';
        }
    }

    private function _validateOffsetandIPCol($table, $cols) {
        $issue_reported = 0;
        // additional columns needed
        $additional_cols = array('IPAddress');
        $columns = array_merge($cols, $additional_cols);

        $sql = 'SELECT ' . implode(",", $columns) . ' FROM ' . $table . " WHERE SiteName='" . $this->_getSitename() . "'";
        //echo $sql.'<br>';
        $records = \ORM::for_table($table)->raw_query($sql)->find_array();

        if ($records) {
            foreach ($records as $record) {
                $octets = explode('.', $record['Subnet']);
                $subnet = new \Portal\SiteValidation\IPv4Subnet($record['Subnet'].$record['Netmask']);
                if (!$subnet->contains($record['IPAddress'])) {
                   $issue_reported = 1;
                   ?>
                   <br>
                   <p>Empty fields are <span style="color:red;">NOT OK</span>.</p>
                   <table width="70%">
                      <thead><tr>
                      <?php
                      foreach ($columns as $heading) {
                         echo '<th>'.$heading.'</th>';
                      }
                      ?>
                      </tr></thead>
                      <tbody>
                         <tr>
                            <td><?=$record['id'];?></td>
                            <td><?=$record['VSN'];?></td>
                            <td style="color:red;"><?=$record['Subnet'];?></td>
                            <td style="color:red;"><?=$record['Netmask'];?></td>
                            <td style="color:red;"><?=$record['IPAddress'];?></td>
                         </tr>
                      </tbody>
                   </table>
                   <br>
                   <?php
                }
                unset($subnet);
            }
            if (!$issue_reported){
                echo '<p>IPOffset and IPAddress data validate <span style="color:green;">OK</span></p>';
            }
        } else {
            echo '<p>This node has no data available for validation.</p>';
        }
    }

    public function validateDataEntry($table) {
        $keys = array('id', 'VSN');
        $cols = $this->getValidationCols($table);
        $columns = array_merge($keys, $cols);

        $funcs = $this->getColFuncMappings($table);

        foreach($funcs as $column => $validateFunc) {
            //echo 'column '.$column.' - function '.$validateFunc.'<br>';
            if (is_array($validateFunc)) {
                foreach($validateFunc as $iterValidateFunc) {
                    //echo "calling array func ".$iterValidateFunc.'<br>';
                    call_user_func(array('\Portal\SiteValidation\DataEntryController', $iterValidateFunc), $table, $columns);
                }
            } else {
                call_user_func(array('\Portal\SiteValidation\DataEntryController', $validateFunc), $table, $columns);
            }
        }
    }

    /*************************************************************/
    /* functions/methods for finding dupes, missing and overlaps */
    /*************************************************************/

    public function findDuplicateSubnets($node) {
       //No duplicates in IPSchema due to db definition : UNIQUE KEY `uq_IPSchema_Subnet` (`Name`,`Subnet`,`Netmask`)
       // Inner joins return the rows that all specified tables have in common - those are duplicates, right ?
       $sql = "SELECT DISTINCT Priv.Subnet, Priv.VSN FROM IP_Private Priv
               INNER JOIN IP_Public Pub ON Priv.Subnet = Pub.Subnet
               INNER JOIN IP_IDN Idn    ON Pub.Subnet  = Idn.Subnet
               WHERE Priv.SiteName='" . $this->_getSitename() . "'" .
               "AND Priv.Subnet is not null and Priv.Subnet not like '%-%' and Priv.Subnet not like '%/%' and Priv.Subnet <>''";
       //echo $sql.'<br>';
       $records = \ORM::for_table($tbl)->raw_query($sql)->find_array();
       if ($records) {
           ?>
           <p>Empty fields are <span style="color:red;">NOT OK</span>.</p>
           <table width="80%">
           <thead>
           <tr>
               <th>VSN</th>
               <th>Subnet</th>
           </tr>
           </thead>
           <tbody>
           <?php
           foreach ($records as $record) {
               $duplicates = 1;
               ?>
               <tr>
                   <td><?=$record['VSN']; ?></td>
                   <td style="color:red;"><?=$record['Subnet']; ?></td>
               </tr>
               <?php
           }
           ?>
           </tbody>
           </table>
           <?php
       } else {
          ?>
          <table width="80%"><thead>
              <tr><th>VSN</th><th>Subnet</th></tr>
              </thead><tbody>
              <tr><td colspan="2"><span style="color:green;">No duplicate</span> subnets found for this node.</td></tr>
              </tbody>
          </table>
          <?php
       }
    }

    // Subnets specified in either IP_Public, IP_Private or IP_IDN, should exist in IPSchema
    // if that's not the case -> sound the alarm and call 911
    // the other way around is handled by findUnallocatedSubnets()
    public function findMissingSubnets($node) {
        $netw_type_mappings = array (   //map tables to network types (possibly unclean data, hence regexp)
            'IP_Public'  => '/^Public/i',
            'IP_Private' => '/^Private/i',
            'IP_IDN'     => '/^IDN/i'
        );
        while (list($tbl, $preg) = each($netw_type_mappings)) {
            $missing = $wrong_networktype = 0;
            $sql = 'SELECT DISTINCT Subnet, id, VSN FROM ' . $tbl .
                   " WHERE SiteName='" . $this->_getSitename() . "'" .
                   " AND Subnet is not null and Subnet not like '%-%' and Subnet not like '%/%' and Subnet <>''";
            //echo $sql.'<br>';
            $records = \ORM::for_table($tbl)->raw_query($sql)->find_array();
            if ($records) {
                ?>
                <table width="80%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>VSN</th>
                    <th><?=$tbl;?>::Subnet</th>
                    <th>IPSchema::NetworkType</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($records as $record) {
                    // find given Subnet in IPSchema and confirm it has been allocated to the correct network type
                    $not_missing = \ORM::for_table('IPSchema')->where(array('SiteName' => $this->_getSitename(),'Subnet' => $record['Subnet']))
                                                              ->find_one();

                    if ($not_missing) {
                       // we found something, we're good IF networktype is correct
                       // if we're scanning the IP_Public table, networktype has to be like '/^public/i' (data can be dirty) in IPSchema
                       if (! preg_match($preg, $not_missing->NetworkType)) {
                          $wrong_networktype = 1;
                          ?>
                          <tr>
                             <td><?=$record['id']; ?></td>
                             <td><?=$record['VSN']; ?></td>
                             <td><?=$record['Subnet']; ?></td>
                             <td style="color:red;"><?=$not_missing->NetworkType;?></td>
                          </tr>
                          <?php
                       }
                    } else {
                       // Subnet not in IPSchema -> ring alarm bell
                       $missing = 1;
                       ?>
                       <tr>
                          <td><?=$record['id']; ?></td>
                          <td><?=$record['VSN']; ?></td>
                          <td><?=$record['Subnet']; ?></td>
                          <td style="color:red;"><?=$not_missing->NetworkType;?></td>
                       </tr>
                       <?php
                    }
                }
                if (!$missing && !$wrong_networktype) {
                   echo '<tr><td colspan="4">Subnet data validated <span style="color:green;">OK</span> for this node.</td></tr>';
                }
                ?>
                </tbody>
                </table>
                <?php
            } else {
                ?>
                <br>
                <table width="80%"><thead>
                <tr><th>Id</th><th>VSN</th><th><?=$tbl;?>::Subnet</th><th>IPSchema::NetworkType</th></tr>
                </thead><tbody>
                    <tr><td colspan="4">No data found for this node.</td></tr>
                </tbody>
                </table>
                <br>
                <?php
            }
            unset($records);
        }
    }

    public function findOverlappingSubnets($node) {
       $netw_type_mappings = array (   //map tables to network types (possibly unclean data, hence regexp)
           'IP_Public'  => '/^Public/i',
           'IP_Private' => '/^Private/i',
           'IP_IDN'     => '/^IDN/i'
       );

       $sql = "SELECT DISTINCT Subnet, id, VSN, NetworkType FROM IPSchema
              WHERE SiteName='" . $this->_getSitename() .
              "' AND Subnet is not null and Subnet not like '%-%' and Subnet not like '%/%' and Subnet <>''";
       //echo $sql.'<br>';
       $records = \ORM::for_table($table)->raw_query($sql)->find_array();
       if ($records) {
           ?>
           <table width="80%">
           <thead>
           <tr>
               <th>VSN</th>
               <th>Network Type</th>
               <th>Subnet</th>
               <th>Overlaps with</th>
           </tr>
           </thead>
           <tbody>
           <?php
           $overlap = 0;
           foreach ($records as $record) {
              while (list($tbl, $preg) = each($netw_type_mappings)) {

                 $sql = 'SELECT DISTINCT Subnet, VSN FROM ' . $tbl .
                        " WHERE SiteName='" . $this->_getSitename() . "'" .
                        " AND Subnet is not null and Subnet not like '%-%' and Subnet not like '%/%' and Subnet <>''";
                 //echo $sql.'<br>';
                 $networks = \ORM::for_table($tbl)->raw_query($sql)->find_array();
                 if ($networks) {
                    foreach ($networks as $network) {
                        if ($record['Subnet'] != $network['Subnet']) {
                            $subnet = new \Portal\SiteValidation\IPv4Subnet( $record['Subnet'] );
                            if ( $subnet->within( $network['Subnet'] ) ) {
                                $overlap = 1;
                                ?>
                                <tr>
                                    <td><?=$record['VSN'];?></td>
                                    <td><?=$record['Networktype'];?></td>
                                    <td><?=$network['Subnet'];?></td>
                                    <td><?=$record['Subnet'];?></td>
                                </tr>
                                <?php
                            }
                            unset($subnet);
                        }
                    }
                 }

              }
           }
           if (!$overlap) {
               ?>
               <tr><td colspan="4"><span style="color:green;">No overlapping</span> subnets found for this node.</td></tr>
               <?php
           }
           ?>
           </tbody>
           </table>
           <?php
       } else {
           ?>
           <table width="80%"><thead>
               <tr><th>VSN</th><th>Network Type</th><th>Subnet</th><th>Overlaps with</th></tr>
               </thead><tbody>
               <tr><td colspan="4">No data found in IPSchema table for this node. No overlapping subnets.</td></tr>
               </tbody>
           </table>
           <?php
       }
    }

    // Report subnets defined in IPSchema, but cannot be found in IP_Public, IP_Private nor IP_IDN
    // the other way around is handled by findMissingSubnets()
    public function findUnallocatedSubnets($node) {
        $netw_type_mappings = array (   //map networktypes to tables
            '/^Public/i'  => 'IP_Public',
            '/^Private/i' => 'IP_Private',
            '/^IDN/i'     => 'IP_IDN'
        );

        $sql = "SELECT DISTINCT Subnet, id, VSN, NetworkType FROM IPSchema
                WHERE SiteName='" . $this->_getSitename() .
                "' AND Subnet is not null and Subnet not like '%-%' and Subnet not like '%/%' and Subnet <>''";
        //echo $sql.'<br>';
        $records = \ORM::for_table($table)->raw_query($sql)->find_array();
        if ($records) {
            $table = '';
            ?>
            <p>Empty fields are <span style="color:red;">NOT OK</span>.</p>
            <table width="100%">
            <thead>
              <tr>
                <th>Id</th>
                <th>VSN</th>
                <th>Subnet</th>
                <th>Network Type</th>
                <th>Table OK</th>
                <th>Table Missing</th>
              </tr>
            </thead>
            <tbody>
            <?php
            foreach ($records as $record) {

                // since data (networkType) might not be very clean (Private vs PRIVATE vs Private IP vs ..)
                // I have to code a mapping between db table and networktype
                while (list($preg, $tbl) = each($netw_type_mappings)) {
                    if (preg_match($preg, $record['NetworkType'])) { //empty networktype is handled at display time
                        $table = $tbl;
                        break;
                    }
                }

                reset($netw_type_mappings); // without this the above while loop continues where broken off previously

                $allocated = \ORM::for_table($table)->select('id')->select('VSN')
                                                    ->where('Subnet', $record['Subnet'])
                                                    ->find_array();
                if (!$allocated) {
                    // Subnet not found in $table -> report it
                    ?>
                    <tr>
                        <td><?=$record['id'];?></td>
                        <td><?=$record['VSN'];?></td>
                        <td><?=$record['Subnet'];?></td>
                        <td style="color:red;"><?=$record['NetworkType'];?></td>
                        <td>IPSchema</td>
                        <td style="color:red;"><?=strlen($record['NetworkType']) ? $table : 'IP_Public / IP_Private / IP_IDN';?></td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
            </table>
            <?php
        } else {
            echo '<p>No data found in IPSchema table for this node. No un-allocated subnets.</p>';
        }
    }

    /**********************************************************/
    /* functions/methods for testing implemented code/classes */
    /**********************************************************/

    // test base class implementation code
    public function testBaseValidation() {

        $tabs = $this->getValidationTables();
        $table = array();
        foreach ($tabs as $key => $val) {
            echo $key . '=' . $val . '<br>';
            $table[] = $val;
        }
        $col_arr = $this->getValidationCols($table[0]);
        //print_r($col_arr);
        //echo '<br>';
        foreach ($col_arr as $key => $val) {
            echo 'Table "' . $table[0] . '" has column "' . $val . '"<br>';
        }

        $funcs = $this->getColFuncMappings($table[0]);
        //print_r($funcs);
        //echo '<br>';
        foreach ($funcs as $key => $val) {
            echo 'To validate "' . $key . '" use function "' . $val . '()"<br>';
        }
    }

}