<?php
namespace Portal\Controllers;

/**
 * Class DnsTrapLogApiController
 * @package Portal\Controllers
 */
class DnsTrapLogApiController extends ControllerBase
{
    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\DnsTrapLogViewModel($type, $siteName);
    }
}