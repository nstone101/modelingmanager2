<?php
namespace Portal\Controllers;

/**
 * Class IpPublicApiController
 * @package Portal\Controllers
 */
class IpPublicApiController extends ControllerBase
{
    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\IpPublicViewModel($type, $siteName);
    }

}