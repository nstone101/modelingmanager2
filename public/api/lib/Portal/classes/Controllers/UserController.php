<?php
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 7/25/2015
 * Time: 1:11 PM
 */

namespace Portal\Controllers;
use Portal\Views;

class UserController
{
    function __construct(){

    }

    public function index()
    {
        // setup a view
        $view = new Views\View();

        $_user = new \stdClass();

        foreach($_SESSION['user'] as $key => $val){
            $_user->{$key} = $val;
            if($val == null){
                $_user->{$key} = '';
            }
        }

        $view->set('user', json_encode($_user));

        // render
        $view->render('users/index');
    }
}