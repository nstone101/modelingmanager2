<?php
namespace Portal\Controllers;

/**
 * Class IpIdnApiController
 * @package Portal\Controllers
 */
class IpIdnApiController extends ControllerBase
{
    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\IpIdnViewModel($type, $siteName);
    }
}