<?php
namespace Portal\Controllers;

use Portal\Models\ViewModels as Vm,
    Portal\Views as Views;
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 6/19/2015
 * Time: 9:44 PM
 */
class NodeController
{

    function __construct()
    {
        // init stuff here

        // read business rules
        get_business_rules();

        $_SESSION['user']['image'] = get_user_image();

        date_default_timezone_set($_SESSION['rules']['DefaultTimeZone']);
    }

    public function index($site, $type='all')
    {
        $_SESSION['help']['context'] = 1;
        // setup a viewmodel
        $model = new Vm\SiteViewModel('SiteInfo', $site, $type);

        $_SESSION['siteName'] = $site;
        $_SESSION['VSN'] = $type;

        // setup a view
        $view = new Views\View();
        $view->set('vm', $model);

        // render
        $view->render('node/index');
    }

    // Site Browser Controller for browsing the different types of sites
    // to navigate to the one the user wants.
	public function browser($filter = "")
    {
        global $template;

        $_SESSION['help']['context'] = 2;

        $nodes['prod'] = get_node_names("Production");
        $nodes['prodAddVSN'] = get_node_names("Production-Add-VSN");
        $nodes['prodNoTraffic'] = get_node_names("Pre-Production");
        $nodes['lab'] = get_node_names("Lab");
        $nodes['deactivated'] = get_node_names("Deactivated");
        $nodes['filter'] = $filter;
        print $template->render('html/node/browser.html.twig', $nodes);
        /*var_dump($nodes);*/
    }

    public function docManipulation($action)
    {
        // craft the proper log message and perform the proper actions.
        switch ($action) {
            case 'upload':
                $copy_dir = VZP_FILES . '/node/' .$_POST['siteName'] .'/' .$_POST['directory'];
                // ensure the destination directories exist
                if (!file_exists($copy_dir)) {
                    mkdir($copy_dir, 0777, true);
                }

                if (!file_exists(VZP_LOG . '/' . $_POST['siteName'])) {
                    mkdir(VZP_LOG . '/' . $_POST['siteName'], 0777, true);
                }
                
                $log_file = fopen(VZP_LOG . '/' . $_POST['siteName'] .$_POST['logFile'], 'a+');

                for ($x = 0; $x <= count($_FILES['file']['tmp_name'])-1; $x++) {
                    if (!move_uploaded_file($_FILES['file']['tmp_name'][$x], $copy_dir . "/" . $_FILES['file']['name'][$x])) {
                        fputs($log_file, $_POST['docType'] ." upload failed\n");
                        echo "error";
                    } else {
                        $log_string = date("Y-m-d G:i e") . " | " . $_SERVER['REMOTE_ADDR'] . " | " . $_SESSION['user']['username'] . " uploaded " . $_POST['name'] ." '" . $_FILES['file']['name'][$x] . "'\n";
                        fputs($log_file, $log_string);
                        fputs($log_file, $_POST['docType'] ." upload complete\n");
                        echo "success";
                    }
                }
                break;

            // TODO: This isn't being used. Leaving here in case we need to verify over-write ok.
            /*case 'replace':
                if (!empty($_FILES['uploadID'])) {
                    if (!move_uploaded_file($_FILES['uploadID']['tmp_name'], $copy_dir . "/" . $_FILES['uploadID']['name'])) {
                        $log_string = date("Y-m-d G:i e") . " | " . $_SERVER['REMOTE_ADDR'] . " | " . $_SESSION['user']['username'] . " replaced " . $_POST['name'] ." '" . basename($_POST['replace']) . "' with '" . $_FILES['uploadID']['name'] . "'\n";
                        fputs($log_file, $log_string);
                        // TODO: need some sort of error back to the user here.
                    }
                }
                break;*/
            case 'delete':  // delete the file for 'replace' or 'delete'.
                $postdata = file_get_contents("php://input");
                $request = json_decode($postdata);
                //var_dump($request);

                $log_file = fopen(VZP_LOG . '/' . $request->siteName .$request->logFile, 'a+');

                if (!unlink($request->doc)) {
                    echo "error";
                } else {
                    $log_string = date("Y-m-d G:i e") . " | " . $_SERVER['REMOTE_ADDR'] . " | " . $_SESSION['user']['username'] . " deleted " . $request->name ." '" . basename($request->doc) . "'\n";
                    fputs($log_file, $log_string);
                    echo "success";
                }
                break;
        }

        // Log their action and redirect them back to the node page.
        //fputs($log_file, $log_string);
        //global $app;
        //$app->redirect('/node/' . $_POST['siteName'] . '/all#t_sitedocs');
    } // end doc_manipulation()

    public function genConfigfileManipulation($action)
    {
        $siteTypes = array();  // hash of templates, key is site/node type
        //$log_file = fopen(VZP_FILES . 'node/' . $_POST['SiteName'] . '/logs/' . date('m.d.y') . '.log', 'a+');
        //echo $log_file;

        //fputs($log_file, $action);

        switch ($action) {
            case 'generate':
                // /files/templates/<vsn>/*.txt
//                $template_dir = VZP_FILES . $_SESSION['rules']['GenConfigTemplates'];
//                foreach (glob($template_dir.'/*', GLOB_ONLYDIR) as $key => $type) {
//                    $siteTypes[$key][] = basename($type);     // only pipnni for now, but let's assume 'they' want more
//                    // /files/configfiles/templates/pipnni/*.txt
//                    foreach(glob($type . '/*.txt') as $template) {
//                        $siteTypes[$key][] = $template;
//                    }
//                }

                // /files/node/<current_node/config/
//                $generated_dir = VZP_FILES . '/node/' . $_POST['SiteName'] . '/configs/';

//                foreach (glob($template_dir.'/*', GLOB_ONLYDIR) as $key => $type) {
//                    $str = '<h2>Generating the following config files</h2>';
//                    $str .= '<h4>'.$type.'</h4>';
//                    $str .= '<ul>';
//                    foreach (glob($type . '/*.txt') as $template) {
//                        $str .= '<li>'.$template.'</li>';
//                    }
//                    $str .= '</ul>';
//                }
//                echo $str;
                break;
            default:
                echo 'configfileManipulation: No action given.';
                break;
        }

        //echo 'success';
        //fclose($log_file);

        global $app;
        $app->redirect('/node/' . $_POST['SiteName'] . '/');

    }

}