<?php
namespace Portal\Controllers;

/**
 * Class NodeDeviceNameApiController
 * @package Portal\Controllers
 */
class NodeDeviceNameApiController extends ControllerBase
{
    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\NodeDeviceNameViewModel($type, $siteName);
    }
}