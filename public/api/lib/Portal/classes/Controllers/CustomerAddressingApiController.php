<?php
namespace Portal\Controllers;

/**
 * Class CustomerAddressingApiController
 * @package Portal\Controllers
 */
class CustomerAddressingApiController extends ControllerBase
{
    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\CustomerAddressingViewModel($type, $siteName);
    }
}