<?php
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 6/27/2015
 * Time: 3:53 PM
 */

namespace Portal\Controllers;


class ControllerBase {

    protected $type;
    protected $model;

    function __construct($type){
        $this->type = $type;
    }

    /**
     * @param $json
     * @return mixed
     */
    public function create($json)
    {
        $params = json_decode($json, true);
        var_dump($this->model->create($params));
    }

    /**
     * @param array $params
     */
    public function read(array $params)
    {
        echo json_encode($this->model->read($params));
    }

    /**
     * @param $id
     * @param $json
     * @return mixed
     */
    public function update($id, $json)
    {
        $params = json_decode($json, true);
        var_dump($this->model->update($params, $id));
    }

    /**
     * @param $json
     */
    public function delete($json)
    {
        $ids = json_decode($json);
        foreach ($ids as $id) {
            $this->model->delete($id);
        }
        var_dump(true);
    }
}