<?php
namespace Portal\Controllers;

/**
 * Class UserApiController
 * @package Portal\Controllers
 */
class UserApiController extends ControllerBase
{
    /**
     */
    function __construct(){
        parent::__construct(null);
        $this->model = new \Portal\Models\ViewModels\UserViewModel();
    }
}