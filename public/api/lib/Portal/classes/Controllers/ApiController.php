<?php

/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 6/29/2015
 * Time: 9:25 PM
 */

namespace Portal\Controllers;

/**
 * Class ApiController
 * provide any functions needed for the app.
 * @package Portal\Controllers
 */
class ApiController extends ControllerBase
{

    function __construct($type) {
        parent::__construct($type);
    }

    /**
     * get the master tags for typeahead
     * @param string $val
     */
    public function getMasterTags($val) {
        $tableName = 'MasterTags';
        $tableName.= ($this->type == 'audit') ? '_Audit' : '';
        $tags = \ORM::for_table($tableName)->select('Tag')->where_like('Tag', '%' . $val . '%')->findArray();
        echo json_encode($tags);
    }

    /**
     * validate the current user and the password they initally provided.
     * @param  string $username username to look up their information
     * @param  string $password password we need to verify. it is not the one stored in the database.
     * @return Object object prviding the status and the message from the function.
     */
    public function validateCurrentPassword($username, $password) {
        $user = \ORM::for_table('UsersNext')->select('Password')->whereEqual('Username', $username)->findArray();

        if ($user == null) {
            $retVal = new StdCLass();
            $retVal->status = 'error';
            $retVal->message = 'User not found.';
            return json_encode($retVal);
        }

        $check_password = hash('sha256', $password . $user->salt);
        for ($round = 0; $round < 65536; $round++) {
            $check_password = hash('sha256', $check_password . $user->salt);
        }

        if ($check_password === $user->password) {

            // If they their entered pw matches the database, then we flip this to true, signifying they can log in.
            $retVal = new StdCLass();
            $retVal->status = 'ok';
            $retVal->message = 'password matches';
            return json_encode($retVal);
        }
        else {

            // sets the variable so that we know the user failed the password check.
            $retVal = new StdCLass();
            $retVal->status = 'error';
            $retVal->message = 'password doesn\'t match';
            return json_encode($retVal);
        }
    }

    /**
     * generate a storng password for the user based on the provided password.
     * @param  string $password current password
     * @return Object  privide teh new strong password with the salt
     */
    public function generatePassword($password) {
        $salt = gen_salt();
        $temp_password = rand(10000000, 99999999);
        $password = hash('sha256', $temp_password . $salt);

        for ($round = 0; $round < 65536; $round++) {
            $password = hash('sha256', $password . $salt);
        }

        $hash = md5(rand(0, 1000));

        $retVal = new \StdCLass();
        $retVal->status = 'ok';
        $retVal->password = $password;
        $retVal->salt = $salt;
        $retVal->hash = $hash;
        echo json_encode($retVal);
        return;
    }
}
