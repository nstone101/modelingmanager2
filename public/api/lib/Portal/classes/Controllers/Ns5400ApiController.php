<?php

namespace Portal\Controllers;


class Ns5400ApiController extends ControllerBase
{
    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\Ns5400ViewModel($type, $siteName);
    }
}