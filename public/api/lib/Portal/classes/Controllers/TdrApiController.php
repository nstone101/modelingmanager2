<?php
namespace Portal\Controllers;

/**
 * Class TdrApiController
 * @package Portal\Controllers
 */
class TdrApiController extends ControllerBase
{
    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\TdrViewModel($type, $siteName);
    }
}