<?php

namespace Portal\Controllers;

/**
 * Class IpSchemaApiController
 * @package Portal\Controllers
 */
class IpSchemaApiController extends ControllerBase
{
    protected $tableName;

    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\IpSchemaViewModel($type, $siteName);
    }

}