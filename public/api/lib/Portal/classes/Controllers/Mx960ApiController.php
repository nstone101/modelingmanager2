<?php

namespace Portal\Controllers;

/**
 * Class Mx960ApiController
 * @package Portal\Controllers
 */
class Mx960ApiController extends ControllerBase
{
    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\Mx960ViewModel($type, $siteName);
    }
}