<?php
namespace Portal\Controllers;

/**
 * Class CardSlottingApiController
 * @package Portal\Controllers
 */
class CardSlottingApiController extends ControllerBase
{
    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\CardSlottingViewModel($type, $siteName);
    }
}