<?php
namespace Portal\Controllers;

/**
 * Class PingPollerApiController
 * @package Portal\Controllers
 */
class PingPollerApiController extends ControllerBase
{
    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\PingPollerViewModel($type, $siteName);
    }
}
