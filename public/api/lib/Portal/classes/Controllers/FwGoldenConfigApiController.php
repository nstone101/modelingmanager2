<?php
namespace Portal\Controllers;

/**
 * Class FwGoldenConfigApiController
 * @package Portal\Controllers
 */
class FwGoldenConfigApiController extends ControllerBase
{
    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\FwGoldenConfigViewModel($type, $siteName);
    }
}
