<?php
namespace Portal\Controllers;

/**
 * Class SubnetAdvApiController
 * @package Portal\Controllers
 */
class SubnetAdvApiController extends ControllerBase
{
    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\SubnetAdvViewModel($type, $siteName);
    }
}