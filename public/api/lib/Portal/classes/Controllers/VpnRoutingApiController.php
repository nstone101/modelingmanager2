<?php
namespace Portal\Controllers;

/**
 * Class VpnRoutingApiController
 * @package Portal\Controllers
 */
class VpnRoutingApiController extends ControllerBase
{
    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\VpnRoutingViewModel($type, $siteName);
    }
}
