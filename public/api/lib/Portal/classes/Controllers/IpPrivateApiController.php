<?php
namespace Portal\Controllers;

/**
 * Class IpPrivateApiController
 * @package Portal\Controllers
 */
class IpPrivateApiController extends ControllerBase
{
    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\IpPrivateViewModel($type, $siteName);
    }

}