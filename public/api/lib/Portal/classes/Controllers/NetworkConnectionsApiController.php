<?php

namespace Portal\Controllers;

/**
 * Class NetworkConnectionsApiController
 * @package Portal\Controllers
 */
class NetworkConnectionsApiController extends ControllerBase
{
    protected $tableName;

    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\NetworkConnectionsViewModel($type, $siteName);
    }

}