<?php
namespace Portal\Controllers;

/**
 * Class AsyncApiController
 * @package Portal\Controllers
 */
class AsyncApiController extends ControllerBase
{
    /**
     * @param $type
     * @param string $siteName
     */
    function __construct($type, $siteName = ''){
        parent::__construct($type);
        $this->model = new \Portal\Models\ViewModels\AsyncViewModel($type, $siteName);
    }
}