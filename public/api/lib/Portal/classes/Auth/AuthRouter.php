<?php

namespace Portal\Auth;

use \Portal\Services;
use \Firebase\JWT\JWT;

class AuthRouter {
    public static function routes($app){
            $app->group('/auth', function() use($app){
                $app->map('/login', function() use($app) {
                    // will be provided for a get request
                    if($app->request()->params('data')) {
                        $json = (array)json_decode(base64_decode(rawurldecode($app->request()->params('data'))));
                    }

                    // // will be provided for a post request
                    if($app->request()->getBody()) {
                        $json = json_decode(base64_decode($app->request()->getBody()));
                    }

                    $authService = new AuthService();
                    $_response = $authService->login($json);

                    // here is the slim php response method
                    $res = $app->response;
                    $res->setStatus($_response->code);


                    // build the JWT token
/*                    $key = "jma-vz-portal";
                    $token = array(
                        "iss" => "http://jmait.com",
                        "aud" => "http://vzportal.com",
                        "iat" => time(),
                        "exp" => time() + (4 * 60 * 60),    // 4 hour exp.
                        "sub" => "portal-token",
                        "context" => json_encode($_response->data)
                    );*/

                    $res->write(
                        json_encode(
                            array(
                                'message'=>$_response->message,
                                'data' => $_response->data,
                                'token' => $authService->token(json_encode($_response->data))
                            )
                        )
                    );

                    $res->headers->set('Content-Type', 'application/vnd.portal+json;version=1');

                    return $res;
                })->via('GET', 'POST');

                $app->get('/token', function() use($app){
                    $service = new AuthService();
                    return $service->token($app->jwt->context);
                });

                $app->post('/logout', function() use($app) {
                    $authService = new AuthService();
                    $_response = $authService->logout();

                    // here is the slim php response method
                    $res = $app->response;
                    $res->setStatus($_response->code);
                    $res->write(json_encode(array('message'=>$_response->message, 'data' => $_response->data)));
                    $res->headers->set('Content-Type', 'application/vnd.portal+json;version=1');
                    return $res;
                });

                $app->get('/verify/:email/:hash', function($email, $hash) use($app) {
                    $authService = new AuthService();
                    $_response = $authService->verifyEmail($email, $hash);

                    // here is the slim php response method
                    $res = $app->response;
                    $res->setStatus($_response->code);
                    $res->write(json_encode(array('message'=>$_response->message, 'data' => $_response->data)));
                    $res->headers->set('Content-Type', 'application/vnd.portal+json;version=1');

                    return $res;
                });

                $app->get('/recoverPassword', function() use($app) {
                    $json = json_decode(base64_decode(rawurldecode($app->request()->params('data'))));
                    $authService = new AuthService();
                    $_response = $authService->recoverPassword($json->email);

                    // here is the slim php response method
                    $res = $app->response;
                    $res->setStatus($_response->code);
                    $res->write(json_encode(array('message'=>$_response->message)));
                    $res->headers->set('Content-Type', 'application/vnd.portal+json;version=1');

                    return $res;
                });
            });

    }
}