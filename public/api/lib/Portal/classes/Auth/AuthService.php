<?php

namespace Portal\Auth;

use Model;
use \Firebase\JWT\JWT;
/**
 * AuthService Class
 * designed to handle login services
 */
class AuthService {

    /**
     * login users
     * @param  mixed $credentials username and password array
     * @return stdClass response providing a status code, message and data
     */
    public function login($credentials) {
        $resp = new \stdClass();

        $login_ok = false;
        $pwd_fail = false;

        // Retrieve the user data from the database.  If $row is false, then the username
        // they entered was not found in the database.
        $row = \ORM::for_table('users')->where('username', $credentials['username'])->find_one();

        if ($row) {
            $row_obj = $row;
            $row = $row->as_array();
            // makes sure their account has been activated.
            if ($row['activated'] == "1"){
                // Using the password submitted by the user and the salt stored in the database,
                // we now check to see whether the passwords match by hashing the submitted password
                // and comparing it to the hashed version already stored in the database.
                $check_password = hash('sha256', $credentials['password'] . $row['salt']);
                for($round = 0; $round < 65536; $round++) {
                    $check_password = hash('sha256', $check_password . $row['salt']);
                }

                if($check_password === $row['password']) {
                    // If they their entered pw matches the database, then we flip this to true, signifying they can log in.
                    $login_ok = true;
                }
                else {
                    // sets the variable so that we know the user failed the password check.
                    $pwd_fail = true;
                }
            }

            // If the user logged in successfully, then we send them to the private members-only page
            // Otherwise, we display a login failed message and show the login form again
            if ($login_ok) {
                // Here I am preparing to store the $row array into the $_SESSION by
                // removing the salt and password values from it. There is no reason to
                // store sensitive values in it unless you have to.  Thus, it is best
                // practice to remove these sensitive values first.
                unset($row['salt']);
                unset($row['password']);

                // Show date & time in a human readable format
                $last_login_yr = date('Y', strtotime($row['login_time']));
                // If last_login = 0, the year value will be negative on conversion
                if ($last_login_yr < 0) {
                    // let's not display zero's or odd strings
                    $row['login_time'] = 'Unknown';
                }
                else {
                    $row['login_time'] = date('M j, Y g:i A T', strtotime($row['login_time']));
                }
                $row_obj->set_expr('login_time', 'now()');
                $row_obj->save();

                // This stores the user's data into the session at the index 'user'.
                // We will check this index on the private members-only page to determine whether
                // or not the user is logged in.  We can also use it to retrieve
                // the user's details.
                $userService = new \Portal\Users\UserService();
                $_SESSION['user'] = $row;
                $_SESSION['newuser'] = $userService->getUserById($row['id']);
                $_SESSION['role'] = $_SESSION['newuser']['roles'];
                $_SESSION['current_user'] = $userService->getUserById($row['id']);
                $rules = \ORM::for_table('business_rules')->findMany();
                $_rules =new \stdClass();
                foreach($rules as $rule){
                    $item = $rule->asArray();
                    $_rules->{$item['rule_key']} = $item['rule_value'];
                };
                $_SESSION['current_user']['rules'] = $_rules;
                $_SESSION['current_user']['sessid'] = session_id();

                $resp->code = 200;
                $resp->message = 'Ok';
                $resp->data = $_SESSION['current_user'];

            }
            else {
                // Show them their username again so all they have to do is enter a new
                // password.  The use of htmlentities prevents XSS attacks.  You should
                // always use htmlentities on user submitted values before displaying them
                // to any users (including the user that submitted them).  For more information:
                // http://en.wikipedia.org/wiki/XSS_attack
                $user = new \stdClass();
                $user->username = htmlentities($credentials['username'], ENT_QUOTES, 'UTF-8');
                $resp->code = '401';
                if (!$row) {
                    // there would only be no row if the username they entered was not found.
                    $resp->code = '404';
                    $resp->message = 'Username not found.';
                }
                else if ($pwd_fail) {
                    // checks to see if the password they entered did not work.
                    $resp->message = 'Invalid password.';
                }
                else if ($row['activated'] == "0"){
                    $resp->message = 'Your account is not active.';
                }
                else if (!defined($_POST['message'])) {
                    // sets the login failed message for any other case, this should never show.
                    $resp->message = 'Login failed.';
                }

                $resp->data = $user;
            }
            return $resp;
        }
        else {
            $user = new \stdClass();
            $user->username = htmlentities($credentials['username'], ENT_QUOTES, 'UTF-8');
            $resp->code = '401';
            $resp->message = 'User doesn\'t exist.';
            $resp->data = $user;
            return $resp;
        }
    }

    public function logout() {
        unset($_SESSION['user']);
        unset($_SESSION['newuser']);
        unset($_SESSION['role']);
        unset($_SESSION['current_user']);
        $resp = new \stdClass();
        $resp->code = 200;
        $resp->message = 'Successfully logged out.';
        $resp->data = new \stdClass();
        return $resp;
    }

    public function token($user){
        // build the JWT token
        $token = array(
            "iss" => "http://jmait.com",
            "aud" => "http://vzportal.com",
            "iat" => time(),
            "exp" => time() + (.5 * 60 * 60),    // 30 minute inactivty timer
            "sub" => "portal-token",
            "context" => $user
        );
        return JWT::encode($token, $_ENV['JWT_SECRET']);
    }


    public function verifyEmail($email, $hash) {
        $user = \ORM::for_table('users')->where(array('email' => $email,'hash' => $hash))->find_one();

        $resp = new \stdClass();
        $resp->code = 200;

        if (!empty($user)) {
          $user->activated = true;
          $user->save();
          $resp->message = 'User with email address ' . $email . ' found. Account activated.';
          $resp->data = (object)array('username' => $user->username);
      } else {
          $resp->message = 'User with email address '.$email.' not found. Nothing to verify or activate.';
      }
        unset($_SESSION['user']);
        unset($_SESSION['newuser']);
        unset($_SESSION['role']);
        unset($_SESSION['current_user']);
        return $resp;
    }

    public function recoverPassword($email) {
        global $template;
        $user = \ORM::for_table('users')->where('email', $email)->find_one();
        $resp = new \stdClass();
        $resp->code = 200;
        $resp->message = 'Email send successfully.';

       if ($user) {
           if ($user->activated == true) {
               // Generate the new salt.
               $salt = gen_salt();

               // The temp password is kept non-hashed so it can be sent to the user in the verifcation email.
               $temp_password = rand(10000000,99999999);

               // We then hash the password temp password.
               $password = hash('sha256', $temp_password . $salt);

               // And again another 65536 times.
               for($round = 0; $round < 65536; $round++)
               {
                   $password = hash('sha256', $password . $salt);
               }

               $user->password = $password;
               $user->salt = $salt;
               $user->save();

               // Once the user entry is updated, we can send them the email with the temporary password.
               $mail = new \PHPMailer;

               $mail->IsSMTP();
               $mail->Host = SMTP_HOST;
               $mail->SMTPAuth = SMTP_AUTH;
               $mail->Username = SMTP_USERNAME;
               $mail->Password = SMTP_PASSWORD;
               $mail->SMTPSecure = SMTP_ENCRYPTION;
               $mail->From = SMTP_FROM;

               $mail->FromName = APP_NAME;
               $mail->AddAddress($email);
               $mail->IsHTML(true);                                  // Set email format to HTML
               $mail->Subject = APP_NAME . ' Password Recovery';

               // This is the body for HTML capable email clients
               $mail->Body = $template->render('email/password-recovery.html.twig', array('username' => $user->username, 'temp_password' => $temp_password));

               // This is the alternate body for those without HTML capable email clients.
               $mail->AltBody = $template->render('email/password-recovery.text.twig', array('username' => $user->username, 'temp_password' => $temp_password));

               // This tries to send the message and generates an error if it couldn't be sent.
               if (!$mail->Send()) {
                    $resp->code = 401;
                    $resp->message = 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo;
               }
               $resp->code = 200;
               $resp->message = 'A temporary password has been sent to: ' . $email;
            } else {
                $resp->code = 401;
                $resp->message = 'We cannot send you a temporary password because your account has not been activated.';
           }
        } else {
            $resp->code = 401;
            $resp->message = 'The email you entered for password recovery could not be found.';
        }
        return $resp;
    }
}