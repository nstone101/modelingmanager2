<?php
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 12/6/2015
 * Time: 8:33 PM
 */

namespace Portal\Admin;

class AdminService
{
    public function getFilesByGen($gen){

        if(is_dir(VZP_DIR . "/files/mops/" .$gen)){
            $docs = array(
                "docs" => array(
                    "mops" => glob(VZP_DIR . "/files/mops/" .$gen ."/*"),
                    "vsnmops" => glob(VZP_DIR . "/files/vsnmops/" .$gen ."/*"),
                    "lld" => glob(VZP_DIR . "/files/lld/" .$gen ."/*")
                ),
                "paths" => array(
                    "mops" => "/files/mops/" .$gen ."/",
                    "vsnmops" => "/files/vsnmops/" .$gen ."/",
                    "lld" => "/files/lld/" .$gen ."/"
                )
            );
            echo json_encode($docs);
        }
        else {
            $app->response->setStatus(204);
            echo json_encode("{'message': 'No directory or files found.'}");
        }
    }

    public function fileUpload($params){
        $params = json_decode($params);
        $retVal = new \stdClass();


        $copy_dir = VZP_DIR . '/files/' . $params->type . '/' . $params->gen;

        if(!is_dir($copy_dir)){
            $retVal->status = 204;
            $retVal->message = 'Directory not found';
            return $retVal;
        }

        if (!file_exists(VZP_LOG . '/files/' . $params->type . '/' . $params->gen)) {
            mkdir(VZP_LOG . '/files/' . $params->type . '/' . $params->gen, 0777, true);
        }

        $log_file = fopen(VZP_LOG . '/files/' . $params->type . '/' . $params->gen . '/logfile.log', 'a+');
        if(count($_FILES['file']['tmp_name']) > 0) {
            for ($x = 0; $x <= count($_FILES['file']['tmp_name'])-1; $x++) {
                if (!move_uploaded_file($_FILES['file']['tmp_name'][$x], $copy_dir . "/" . $_FILES['file']['name'][$x])) {
                    fputs($log_file, $copy_dir . "/" . $_FILES['file']['name'][$x] ." upload failed\n");
                    $retVal->status = 500;
                    $retVal->message = 'Unable to upload the file.';
                    return $retVal;
                } else {
                    $log_string = date("Y-m-d G:i e") . " | " . $_SERVER['REMOTE_ADDR'] . " | " . $_SESSION['user']['username'] . " uploaded " . $_POST['name'] ." '" . $_FILES['file']['name'][$x] . "'\n";
                    fputs($log_file, $log_string);
                    fputs($log_file, $copy_dir . "/" . $_FILES['file']['name'][$x] ." upload complete\n");
                    $retVal->status = 200;
                    $retVal->message = 'File upload successful.';
                    return $retVal;
                }
            }
        }
        else {
            $retVal->status = 204;
            $retVal->message = 'No files to upload.';
            return $retVal;
        }
    }
}