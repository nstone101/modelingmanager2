<?php
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 12/6/2015
 * Time: 8:33 PM
 */

namespace Portal\Admin;

use \Portal\Services;

class AdminRouter
{
    public static function routes($app){
        $app->group('/admin-tools', function() use($app){
            $app->group('/gen-docs', function() use($app){
                $app->get('(/:gen)', function($gen = '1.0') use($app) {
                    $service = new AdminService();
                    echo $service->getFilesByGen($gen);
                });

                $app->post('/', function() use($app){
                    $service = new AdminService();
                    $resp = $service->fileUpload($app->request->getBody());
                    echo json_encode($resp);
                    var_dump($resp);
                    $response = $app->response;
                    $response->status($resp->status);
                    $response->body(json_encode($resp, true));
                    $app->stop();
                });
            });
        });
    }
}