<?php
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 12/6/2015
 * Time: 8:33 PM
 */

namespace Portal\Files;

class FileService
{
    public function getConfig($gen, $site){
        //if($gen)
            return $this->getGenConfig();
    }

    protected function getGenConfig(){
        return array(
            'types' => array(
                'mops',
                'vsnmops',
                'lld'
            ),
            'gens' => array(
                '1.0',
                '2.0',
                '3.0'
            )
        );
    }

    protected function getSiteConfig($site){

    }

    public function getFiles($gen, $site){
        $docs = array(
            "files" => array(
                "mops" => array(
                    array(
                        "1.0" => glob(VZP_DIR . "/files/mops/1.0/*"),
                        "2.0" =>  glob(VZP_DIR . "/files/mops/2.0/*"),
                        "3.0" =>  glob(VZP_DIR . "/files/mops/3.0/*")
                    )
                ),
                "vsnmops" => array(
                    array(
                        "1.0" => glob(VZP_DIR . "/files/vsnmops/1.0/*"),
                        "2.0" =>  glob(VZP_DIR . "/files/vsnmops/2.0/*"),
                        "3.0" =>  glob(VZP_DIR . "/files/vsnmops/3.0/*")
                    )
                ),
                "lld" => array(
                    array(
                        "1.0" => glob(VZP_DIR . "/files/lld/1.0/*"),
                        "2.0" =>  glob(VZP_DIR . "/files/lld/2.0/*"),
                        "3.0" =>  glob(VZP_DIR . "/files/lld/3.0/*")
                    )
                )
            ),
            "paths" => array(
                "mops" => array(
                    array(
                        "1.0" => "/files/mops/1.0/",
                        "2.0" => "/files/mops/2.0/",
                        "3.0" => "/files/mops/3.0/"
                    )
                ),
                "vsnmops" => array(
                    array(
                        "1.0" => "/files/vsnmops/1.0/",
                        "2.0" => "/files/vsnmops/2.0/",
                        "3.0" => "/files/vsnmops/3.0/"
                    )
                ),
                "lld" => array(
                    array(
                        "1.0" => "/files/lld/1.0/",
                        "2.0" => "/files/lld/2.0/",
                        "3.0" => "/files/lld/3.0/"
                    )
                )
            ),
            "configs" =>array(
                'types' => array(
                    'mops' => array(
                        'title' => 'MOPs',
                        'type' => 'MOPs',
                        'gens' => array(
                        "1.0",
                        "2.0",
                        "3.0")
                    ),
                    'vsnmops' => array(
                        'title' => 'VSNMOPs',
                        'type' => 'VSNMOPs',
                        'gens' => array(
                            "1.0",
                            "2.0",
                            "3.0")
                    ),
                    'lld' => array(
                        'title' => 'LLD',
                        'type' => 'LLD',
                        'gens' => array(
                            "1.0",
                            "2.0",
                            "3.0")
                    )
                )
            )
        );

        return $docs;
    }

    protected function getDocumentsByGen($gen){

        if(is_dir(VZP_DIR . "/files/mops/" .$gen)){
            $docs = array(
                "files" => array(
                    "mops" => glob(VZP_DIR . "/files/mops/" .$gen ."/*"),
                    "vsnmops" => glob(VZP_DIR . "/files/vsnmops/" .$gen ."/*"),
                    "lld" => glob(VZP_DIR . "/files/lld/" .$gen ."/*")
                ),
                "paths" => array(
                    "mops" => "/files/mops/" .$gen ."/",
                    "vsnmops" => "/files/vsnmops/" .$gen ."/",
                    "lld" => "/files/lld/" .$gen ."/"
                ),
                "configs" =>array(
                    'types' => array(
                        'mops',
                        'vsnmops',
                        'lld'
                    ),
                    'gens' => array(
                        '1.0',
                        '2.0',
                        '3.0'
                    )
                )
            );
            return $docs;
        }
        else {
            return array();
        }
    }

    protected function getDocumentsBySite($site){
        if(is_dir(VZP_DIR . "/files/node/" . $site . "/mops")){
            $docs = array(
                "docs" => array(
                    "ispecs" => glob(VZP_DIR . "/files/node/" . $site . "/ispec/*.*"),
                    "mops" => glob(VZP_DIR . "/files/node/" . $site . "/mops/*.*"),
                    "layouts" => glob(VZP_DIR . "/files/node/" . $site . "/layouts/*.*"),
                    "failovers" => glob(VZP_DIR . "/files/node/" . $site . "/failovers/*.*"),
                    "sitevisits" => glob(VZP_DIR . "/files/node/" . $site . "/sitevisits/*.*"),
                    "miscellaneous" => glob(VZP_DIR . "/files/node/" . $site . "/miscellaneous/*.*")
                ),
                "paths" => array(
                    "ispecs" => "/files/node/" . $site . "/ispec/",
                    "mops" => "/files/node/" . $site . "/mops/",
                    "layouts" => "/files/node/" . $site . "/layouts/",
                    "failovers" => "/files/node/" . $site . "/failovers/",
                    "sitevisits" => "/files/node/" . $site . "/sitevisits/",
                    "miscellaneous" => "/files/node/" . $site . "/miscellaneous/"
                )
            );
            return $docs;
        }
        else {
            return array();
        }
    }

    public function fileUpload(array $params){
        $params = (object)$params;

        if($params->gen)
            return $this->saveDocumentByGen($params->gen, $params->type);
    }

    public function deleteFile($file){
        var_dump($file);
        $file = str_replace('^', '/', $file);
        var_dump($file);

        //$log_file = fopen(VZP_LOG . '/' . $request->siteName .$request->logFile, 'a+');

        if (!unlink(VZP_DIR . $file)) {
            echo "error";
        } else {
            //$log_string = date("Y-m-d G:i e") . " | " . $_SERVER['REMOTE_ADDR'] . " | " . $_SESSION['user']['username'] . " deleted " . $request->name ." '" . basename($request->doc) . "'\n";
            //fputs($log_file, $log_string);
            echo "success";
        }
    }

    protected function saveDocumentByGen($gen, $type){
        $saveDir = VZP_DIR . '/files/' . $type . '/' . $gen;
        $logDir = VZP_LOG . '/files/' . $type . '/' . $gen;


        $saveDir = VZP_DIR . '/files/' . $type . '/' . $gen;

        if(!is_dir($saveDir)){
            return array('status' => 204, 'message' => 'Directory not found.');
        }

        if (!file_exists($logDir)) {
            mkdir($logDir, 0777, true);
        }

        $log_file = fopen($logDir . '/logfile.log', 'a+');
        if(count($_FILES['file']['tmp_name']) > 0) {
            for ($x = 0; $x <= count($_FILES['file']['tmp_name'])-1; $x++) {
                if (!move_uploaded_file($_FILES['file']['tmp_name'][$x], $saveDir . "/" . $_FILES['file']['name'][$x])) {
                    fputs($log_file, $saveDir . "/" . $_FILES['file']['name'][$x] ." upload failed\n");
                    return array('status' => 500, 'message' => 'Unable to upload the file.');
                } else {
                    $log_string = date("Y-m-d G:i e") . " | " . $_SERVER['REMOTE_ADDR'] . " | " . $_SESSION['user']['username'] . " uploaded " . $type ." '" . $_FILES['file']['name'][$x] . "'\n";
                    fputs($log_file, $log_string);
                    fputs($log_file, $saveDir . "/" . $_FILES['file']['name'][$x] ." upload complete\n");
                    return array('status' => 200, 'message' => 'File upload successful.');
                }
            }
        }
        else {
            return array('status' => 204, 'message' => 'No file to upload.');
        }
    }

    protected function saveDocumentBySite($siteName, $name, $logfile){

    }



}