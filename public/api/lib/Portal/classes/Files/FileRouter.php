<?php
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 12/6/2015
 * Time: 8:33 PM
 */

namespace Portal\Files;

use \Portal\Services;

class FileRouter
{
    public static function routes($app){
        $app->group('/files', function() use($app){
            $app->get('/', function() use($app){
                $gen = $app->request()->params('gen');
                $site = $app->request()->params('site');

                $service = new FileService();
                $documents = $service->getFiles($gen, $site);

                if(count($documents) == 0){
                    $app->response->status(204);
                    echo 'No documents found.';
                }
                else {
                    echo json_encode($documents);
                }
            });

            $app->get('/config', function() use($app){
                $gen = $app->request()->params('gen');
                $site = $app->request()->params('site');

                $service = new FileService();
                $config = $service->getConfig($gen, $site);

                if(count($config) == 0){
                    $app->response->status(204);
                    echo 'No documents found.';
                }
                else {
                    echo json_encode($config);
                }
            });

            $app->post('/', function() use($app){
                $service = new FileService();
                $resp = $service->fileUpload(array(
                    'gen'=>$app->request()->params('gen'),
                    'type' => $app->request()->params('type')));
                //echo json_encode($resp);
               // var_dump($app->request);
                $response = $app->response;
                $response->status($resp->status);
                $response->body(json_encode($resp, true));
            });

            $app->delete('(/:id)', function($id=null) use($app){
                var_dump($id);
                $service = new FileService();
                echo json_encode($service->deleteFile($id));
            });
        });
    }
}