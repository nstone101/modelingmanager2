<?php
/**
 * Created by PhpStorm.
 * User: bpowell
 * Date: 12/20/2015
 * Time: 8:45 PM
 */
namespace Portal\Files;

class FileDbService {

    protected function files(){
        return  Model::factory('\Portal\Models\File');
    }

    public function getFiles($filters = null){
        if($filters != null) {
            $filters = $filters;

            return $this->files()->where($filters)->findArray();
        }
        return $this->files()->findArray();
    }

    public function getFile($id){
        return $this->files()->findOne($id);
    }

    public function save($file)
    {
        $retVal = null;
        if ($file->id == null) {
            $retVal = $this->create($file);
        } else {
            $retVal = $this->update($file);
        }

        return $retVal;
    }

    protected function create($file){
        try {
            $newFile = $this->files()->create();
            return $this->saveOrmObject($newFile, $file);
        }
        catch(\Exception $e){
            throw $e;
        }
    }

    protected function update($file){
        try {
            $currentFile = $this->files()->findOne($file->id);
            return $this->saveOrmObject($currentFile, $file);
        }
        catch(\Exception $e){
            throw $e;
        }
    }

    public function delete($id){
        $file = $this->files()->findOne($id);
        $file->delete();
    }

    /**
     * @param $object
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    protected function saveOrmObject($object, $params){
        try {
            $params = (array)$params;
            $object->set($params);
            $object->save();
            return $object->id();
        }
        catch(\Exception $e){
            var_dump($e);
            throw $e;
        }
    }
}