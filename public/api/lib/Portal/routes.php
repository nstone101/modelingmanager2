<?php
// Landing Page
$app->map('/', '\Portal\Controller\Home::default_controller')->via('GET');

// misc routes
$app->notFound('\Portal\Controller\Misc::misc_notfound_controller');

$app->map('/phpinfo', function () {
    phpinfo();
})->via('GET');
$app->map('/debug', '\Portal\Controller\Misc::misc_debug_controller')->via('GET');

if (isJMA()) {
    $app->map('/phpinfo', function () {
        phpinfo();
    })->via('GET');
    $app->map('/debug', '\Portal\Controller\Misc::misc_debug_controller')->via('GET');
}

// User Routes
\Portal\Users\UserRouter::routes($app);

// Auth Routes
\Portal\Auth\AuthRouter::routes($app);

// Node Routes
\Portal\Nodes\NodeRouter::routes($app);

/*** ##ANNOTATED: (Added by Nick Stone #2016:02:19#14:32:09) with JADEATOMJS PLUGIN in WebStorm v11.2 ***/    
// Modeling Routes -- MIC/MPC/MIC-PA 
\Portal\Modeling\ModelingRouter::routes($app);

// Reporter Routes
\Portal\Reports\ReportRouter::routes($app);

// Admin Routes
\Portal\Admin\AdminRouter::routes($app);

// File Routes
\Portal\Files\FileRouter::routes($app);

//  Admin Tools group
if (isAdmin()) {
    $app->group('/admin-tools', function () use ($app) {
        $app->map('/delete-node', '\Portal\Controller\AdminTools::delete_node')->via('GET', 'POST');
        $app->map('/delete-node2', '\Portal\Controller\AdminTools::delete_node2')->via('GET', 'POST');
        $app->map('/master-tags(/:type)', '\Portal\Controller\AdminTools::master_tags_controller')->via('GET');
        $app->map('/vsn-editor', '\Portal\Controller\AdminTools::vsn_editor_controller')->via('GET');
        $app->map('/del-vsn', '\Portal\Controller\AdminTools::remove_vsn')->via('GET', 'POST');

        // Services Health check group
        /*
        $app->group('/health-check', function () use ($app) {
            $app->map('/apc-cache', '\Portal\HealthCheck\APC::do_check')->via('GET');
        });
        */
    });
}

// Config Tools group
if (isJMA()) {
    $app->group('/config', function () use ($app) {
        $app->map('/', '\Portal\Controller\ConfigTools::config_mgr_controller')->via('GET', 'POST');
        $app->map('/mx_builder', '\Portal\Controller\ConfigTools::mx_builder_controller')->via('POST');
    });
}

// reports group
$app->group('/reports', function () use ($app) {
    $app->map('/port-usage', '\Portal\Controller\Reports::port_usage_controller')->via('GET');
    $app->map('/ip-subnet-usage', '\Portal\Controller\Reports::ip_subnet_usage_controller')->via('GET');
    $app->map('/node-vsns', '\Portal\Controller\Reports::node_vsns_controller')->via('GET');
    $app->map('/node-mop', '\Portal\Controller\Reports::node_mop_controller')->via('GET');
    $app->map('/node-validation', '\Portal\Controller\Reports::node_validation_controller')->via('GET', 'POST');
    $app->map('/ter', '\Portal\Controller\Reports::test_exit_report')->via('GET');
    $app->map('/globalsearch', '\Portal\Controller\Reports::global_search_controller')->via('GET');
});

