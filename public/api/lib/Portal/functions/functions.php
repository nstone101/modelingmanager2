<?php
/*
 *    Magic Quotes = BAD
 */
if(function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) {
    function undo_magic_quotes_gpc(&$array) {
        foreach($array as &$value) {
            if(is_array($value)) {
                undo_magic_quotes_gpc($value);
            } else {
                $value = stripslashes($value);
            }
        }
    }
    undo_magic_quotes_gpc($_POST);
    undo_magic_quotes_gpc($_GET);
    undo_magic_quotes_gpc($_COOKIE);
}

// Tag validator function is used for datatables to validate that the tag exists in the Master Tags table.
// If not, it'll print the error the user.
function tag_validate($val) {
    $tag = \ORM::for_table('MasterTags')->where('Tag', $val)->find_one();
    return $tag == false ? "This tag was not found in Master Tags table. Choose one that exists in the table, or consult an administrator if it needs to be added." : true;
}

// vsn_to_dir() turns a VSN name into it's corresponding directory name in the file structure.
function vsn_to_dir($vsn) {
    return strtolower(str_replace(' ', '_', $vsn));
}

// dir_to_vsn() turns a directory name in the file structure into a pretty-print view for displaying the user.
function dir_to_vsn($dir) {
    return strtoupper(str_replace('_', ' ', $dir));
}


function array_filter_recursive($input) {
  foreach ($input as &$value) {
    if (is_array($value)) {
      $value = array_filter_recursive($value);
    }
  }
  return array_filter($input);
}

function dot2form_name($input) {
    $arr_name = '';
    $input = str_replace(array("{","}"), "", $input);
    $input = trim($input);
    $keys = explode('.', $input);
    while ($key = array_shift($keys)) {
      if(($key != 'php') && ($key != 'wizard')) {
        $arr_name .= "[$key]";
      }
    }
    return $arr_name;
}

function assign_array_by_path(&$arr, $path, $value) {
    $keys = explode('.', $path);
    while ($key = array_shift($keys)) {
        $arr = &$arr[$key];
    }
    $arr = $value;
}


function page_header() {
    global $template;
    print $template->render('html/partials/header.html.twig');
    print $template->render('html/partials/menu.html.twig');
}

function page_footer() {
    global $template;
    print $template->render('html/partials/footer.html.twig');
}

/**
 * Execute a PHP template file and return the result as a string.
 */
function render_template($template, $vars = array()) {
    extract($vars);
    extract($GLOBALS, EXTR_SKIP);

    ob_start();
    require $template;
    $applied_template = ob_get_contents();
    ob_end_clean();

    return $applied_template;
}

/* Function create_zip
$files = array - an array of the file pathnames
$destination = string - the pathname of output zip file (ex. /var/home/asd/output.zip)
$overwrite = boolean - whether or not to overwrite the destination file, if it exists already
*/
function create_zip($files = array(),$destination = '',$overwrite = false) {
    // if the zip file already exists and overwrite is false, return false
    if(file_exists($destination) && !$overwrite) { return false; }

    // initializes an array of the valid files,
    // which will be a list of the files that actually exist,
    // and can be added to the zip file output.
    $valid_files = array();

    // if files were passed in...
    if(is_array($files)) {
        // cycle through each file
        foreach($files as $file) {
            // make sure the file exists
            if(file_exists($file)) {
                $valid_files[] = $file; // adds existing files to the array of valid files.
            }
        }
    }
    // if we have good files...
    if(count($valid_files)) {
        // create the archive
        $zip = new ZipArchive();
        if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
            return false;
        }

        foreach($valid_files as $file) {
            $zip->addFile($file,basename($file));
        }
        // unused debug code
        //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

        // close the zip -- done!
        $zip->close();

        // check to make sure the file exists
        return file_exists($destination);
    }
    else {
        return false;
    }
}

// make php.* available in templates
$template->addGlobal('php', $GLOBALS);


// if there is not a twig function then call the php function
$template->registerUndefinedFunctionCallback(function ($name) {
    if (function_exists($name)) {
        return new Twig_Function_Function($name);
    }
    return false;
});

$template->addExtension(new Twig_Extension_Debug());

// logic for creating the node menus
function nodes_menu() {
    $rows = get_node_names();
    if($rows) {
        $rows = array_chunk($rows, (count($rows) / 5));

        foreach ($rows as $key => $chunk) {

            $chunk_title = $chunk[0]["SiteName"] . " - " . end($chunk)["SiteName"];
            $chunk_icon = upper(substr($chunk[0]["SiteName"], 0, 1));

            echo "\t<li class='submenu'>\n";
            echo "\t\t<a href='#'>";
            echo "<icon class='label menu-icon' style='float: none; margin-right: 10px;' title='".$chunk_title."' data-toggle='tooltip' data-placement='right'>" . $chunk_icon ."</icon>";
            echo "<span class='node-title'>" . $chunk_title . "</span>";
            echo "<span class='label node-count'>" . count($chunk) . "</span>";
            echo "</a>\n";
            echo "\t\t<ul>\n";
            foreach ($chunk as $index => $siteName) {
                echo "\t\t\t<li><a href='/node/" . strtolower($siteName["SiteName"]) ."'>" . $siteName["SiteName"] . "</a></li>\n";
            }
            echo "\t\t</ul>\n";
            echo "\t</li>\n";

        }
    }
}

// logic for creating the lab node menus
function lab_nodes_menu() {
    $nodes = get_node_names(true);
    if($nodes) {
        echo "\t<li class='submenu'>\n";
        echo "\t\t<a href='#'><icon class='fa fa-flask menu-icon' title='Lab Nodes' data-toggle='tooltip' data-placement='right'></icon> <span>Lab Nodes</span><span class='label'>" . count($nodes) . "</span></a>\n";
        echo "\t\t<ul>\n";
        foreach ($nodes as $key => $node) {
            echo "\t\t<li><a href='/node/" . strtolower($node["SiteName"]) . "'>" . $node["SiteName"] . "</a></li>\n";
        }
        echo "\t\t</ul>\n";
        echo "\t</li>\n";
    }
}

// check to see if the user is logged in
function check_login() {
    global $app;

    /*if (strpos($_SERVER['REQUEST_URI'], "/verify/")) {
        $_SESSION['user'] = 'admin';
    } else {
        $at_login = strpos($_SERVER['REQUEST_URI'], "/admin/login");
    }
    if (!isset($_SESSION['user']) && $at_login === false)
       $app->redirect('/admin/login?req=' . $_SERVER['REQUEST_URI']);*/
}

// check to see if the user is an admin
function check_admin() {
    global $app;
    if (isset($_SESSION['role']) && $_SESSION['role'] != 'VZ-Admin' && $_SESSION['role'] != 'JMA-Admin') {
       $app->redirect('/');
    }
}

function isRWUser() {
    return (!empty($_SESSION['role']) && $_SESSION['role'] == 'Read-Write') ? true : false;
}

function isROUser() {
    return (!empty($_SESSION['role']) && $_SESSION['role'] == 'Read-Only') ? true : false;
}

function isAdmin() {
    return (!empty($_SESSION['role']) && ($_SESSION['role'] == 'VZ-Admin' || $_SESSION['role'] == 'JMA-Admin')) ? true : false;
}

function isJMA() {
    return (!empty($_SESSION['role']) && $_SESSION['role'] == 'JMA-Admin') ? true : false;
}


function isRWOrAdmin() {
    return (!empty($_SESSION['role']) && ($_SESSION['role'] == 'VZ-Admin' || $_SESSION['role'] == 'Read-Write' || $_SESSION['role'] == 'JMA-Admin')) ? true : false;

}



function gen_salt() {
    return dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647));
}

function get_business_rules() {
    $businessRules = ORM::for_table('business_rules')->raw_query('select rule_key, rule_value from business_rules order by id asc')->find_many();
    foreach($businessRules as $rule) {
        $_SESSION['rules'][$rule->rule_key] = $rule->rule_value;
    }
}

function get_user_image() {
    $user = ORM::for_table('users')->find_one($_SESSION['user']['id']);
    return $user['image'] ? $user['image'] : 0;
}

// get all sitenames
function get_node_names($filter = "") {
    if ($filter == "") {
        return ORM::for_table('SiteInfo')->order_by_asc('SiteName')->find_array();
    } else {
        return ORM::for_table('SiteInfo')->where('SiteGroup', $filter)->order_by_asc('SiteName')->find_array();
    }
}

// get total number of sites
function total_sites() {
    return ORM::for_table('SiteInfo')->count();;
}

// delete a site
function delete_site($node_name){
    $node = ORM::for_table('SiteInfo')->where('SiteName', $node_name)->find_one();
    $node->delete();
}

function html_decode($value) {
    return html_entity_decode($value, ENT_QUOTES);
}

function html_encode($value) {
    return htmlentities($value, ENT_QUOTES);
}

function upper($str) {
    return strtoupper($str);
}

function lower($str) {
    return strtolower($str);
}

function strip_dirty_json($str) {
    return preg_replace('@(?<!http:)//.*@','',$str);
}

function vz_error_handler($errno, $errstr, $errfile, $errline) {

    switch ($errno) {
        case E_USER_ERROR:
            echo "<b>ERROR</b> [$errno] $errstr<br />\n";
            echo "Fatal error on line $errline in file $errfile";
            break;
        case E_USER_WARNING:
            break;
        case E_USER_NOTICE:
            break;
        default:
            break;
    }
    return true;
}

function mq_debug($str) {
    file_put_contents('marcel.txt', $str."\n", FILE_APPEND);
}

function outputJSON($msg, $status = 'error') {
    header('Content-Type: application/json');
    die( json_encode( array('data' => $msg, 'status' => $status) ) );
}

function addQuotes($str) {
    return "'$str'";
}
